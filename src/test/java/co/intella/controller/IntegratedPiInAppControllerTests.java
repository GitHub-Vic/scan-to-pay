package co.intella.controller;

import co.intella.crypto.AesCryptoUtil;
import co.intella.model.IntegratedRequest;
import co.intella.net.Constant;
import co.intella.utility.Cryption;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.SecretKey;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Miles
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegratedPiInAppControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testPiInAppSingleOrderQuery() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header

        requestData.put("Method", "60300");
        requestData.put("ServiceType", "SingleOrderQuery");
        requestData.put("MchId", "InApp");
        requestData.put("CreateTime", "20180103080000");
        // setting data
        requestData.put("StoreOrderNo", "18010300019");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());

    }

    @Test
    public void testPiInAppRefund() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header

        requestData.put("Method", "60300");
        requestData.put("ServiceType", "Refund");
        requestData.put("MchId", "InApp");
        requestData.put("CreateTime", getNowTime());
        // setting data
        requestData.put("StoreOrderNo", "17092900002");

        requestData.put("RefundKey", "0000");
        requestData.put("StoreRefundNo", "17092900002");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());

    }

    private String getNowTime() {
        SimpleDateFormat sdf1 = new SimpleDateFormat(SystemInstance.DATE_PATTERN);
        String str = "";
        str = sdf1.format(new Date());
        return str;
    }
}
