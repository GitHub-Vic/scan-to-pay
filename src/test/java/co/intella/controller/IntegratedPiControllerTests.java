package co.intella.controller;

import co.intella.crypto.AesCryptoUtil;
import co.intella.model.IntegratedRequest;
import co.intella.net.Constant;
import co.intella.utility.Cryption;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.SecretKey;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author Andy Lin
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegratedPiControllerTests {
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testPiTransaction() throws Exception {
        Random rand = new Random();

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10300");
        requestData.put("ServiceType", SystemInstance.TYPE_MICROPAY);
        requestData.put("MchId", "super");
        requestData.put("CreateTime", getNowTime());
        // setting data
        requestData.put("StoreOrderNo", "PiTest" + Integer.toString(rand.nextInt(9999)));
        requestData.put("DeviceInfo", "P1");
        requestData.put("Body", "body");
        requestData.put("TotalFee", "1");
        requestData.put("FeeType", "TWD");
        requestData.put("Detail", "detail");
        requestData.put("Attach", "A123456789");
        requestData.put("GoodsTag", "food");
        requestData.put("StoreInfo", "888811");
        requestData.put("Cashier", "Cashier");
        requestData.put("AuthCode", "PISNYOC3JQAGZIV3P1");
        requestData.put("Extended", "JSON");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testPiRefund() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10300");
        requestData.put("ServiceType", "Refund");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", getNowTime());
        // setting data
        requestData.put("StoreRefundNo", "RPiTest4657");
        requestData.put("StoreOrderNo", "PiTest4657");
        requestData.put("SysOrderNo", "");
        requestData.put("DeviceInfo", "P1");
        requestData.put("StoreInfo", "S1");
        requestData.put("Cashier", "C1");
//        requestData.put("TotalFee", "1");
        requestData.put("RefundFeeType", "TWD");
        requestData.put("RefundFee", "1");
        requestData.put("RefundedMsg", "bad");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("7137", header.get("StatusCode").getAsString());


        requestData.put("Method", "10300");
        requestData.put("ServiceType", "Refund");
        requestData.put("MchId", "support");  //wrong
        requestData.put("CreateTime", "20170402080000");
        // setting data
        requestData.put("StoreRefundNo", "defined");
        requestData.put("StoreOrderNo", "99991125");
        requestData.put("SysOrderNo", "");
        requestData.put("DeviceInfo", "P1");
        requestData.put("StoreInfo", "S1");
        requestData.put("Cashier", "C1");
        requestData.put("TotalFee", "1");
        requestData.put("RefundFeeType", "TWD");
        requestData.put("RefundFee", "1");
        requestData.put("RefundedMsg", "bad");

        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();

        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());


        requestData.put("Method", "10300");
        requestData.put("ServiceType", "Refund");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "201704020859");//wrong
        // setting data
        requestData.put("StoreRefundNo", "defined");
        requestData.put("StoreOrderNo", "99991122");
        requestData.put("SysOrderNo", "");
        requestData.put("DeviceInfo", "P1");
        requestData.put("StoreInfo", "S1");
        requestData.put("Cashier", "C1");
        requestData.put("TotalFee", "1");
        requestData.put("RefundFeeType", "TWD");
        requestData.put("RefundFee", "1");
        requestData.put("RefundedMsg", "bad");

        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();

        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("8002", header.get("StatusCode").getAsString());


        requestData.put("Method", "10300");
        requestData.put("ServiceType", "Refund");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170402085900");
        // setting data
        requestData.put("StoreRefundNo", "defined");
        requestData.put("StoreOrderNo", "");//wrong
        requestData.put("SysOrderNo", "");
        requestData.put("DeviceInfo", "P1");
        requestData.put("StoreInfo", "S1");
        requestData.put("Cashier", "C1");
        requestData.put("TotalFee", "1");
        requestData.put("RefundFeeType", "TWD");
        requestData.put("RefundFee", "1");
        requestData.put("RefundedMsg", "bad");

        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();

        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("8002", header.get("StatusCode").getAsString());


        requestData.put("Method", "10300");
        requestData.put("ServiceType", "Refund");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170402085900");
        // setting data
        requestData.put("StoreRefundNo", "defined");
        requestData.put("StoreOrderNo", "");
        requestData.put("SysOrderNo", "");//wrong
        requestData.put("DeviceInfo", "P1");
        requestData.put("StoreInfo", "S1");
        requestData.put("Cashier", "C1");
        requestData.put("TotalFee", "1");
        requestData.put("RefundFeeType", "TWD");
        requestData.put("RefundFee", "1");
        requestData.put("RefundedMsg", "bad");

        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();

        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("8002", header.get("StatusCode").getAsString());


        requestData.put("Method", "10300");
        requestData.put("ServiceType", "Refund");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170402085900");
        // setting data
        requestData.put("StoreRefundNo", "defined");
        requestData.put("StoreOrderNo", "99991122");
        requestData.put("SysOrderNo", "");
        requestData.put("DeviceInfo", "P1");
        requestData.put("StoreInfo", "S1");
        requestData.put("Cashier", "C1");
        requestData.put("TotalFee", "1");
        requestData.put("RefundFeeType", "TWD");
        requestData.put("RefundFee", "");//wrong
        requestData.put("RefundedMsg", "bad product");

        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();

        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("8002", header.get("StatusCode").getAsString());


        requestData.put("Method", "10300");
        requestData.put("ServiceType", "Refund");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170402085900");
        // setting data
        requestData.put("StoreRefundNo", "defined");
        requestData.put("StoreOrderNo", "99991122");
        requestData.put("SysOrderNo", "");
        requestData.put("DeviceInfo", "P1");
        requestData.put("StoreInfo", "S1");
        requestData.put("Cashier", "C1");
        requestData.put("TotalFee", "1");
        requestData.put("RefundFeeType", "TWD");
        requestData.put("RefundFee", "1");
        requestData.put("RefundedMsg", "");//wrong

        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();

        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("8002", header.get("StatusCode").getAsString());
    }

    @Test
    public void testPiCancel() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10300");
        requestData.put("ServiceType", "Cancel");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170313080000");
        // setting data
        requestData.put("StoreOrderNo", "99991123");
        requestData.put("StoreInfo", "888811");
        requestData.put("DeviceInfo", "P1");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("7132", header.get("StatusCode").getAsString());

        requestData.put("Method", "10300");
        requestData.put("ServiceType", "Cancel");
        requestData.put("MchId", "test");//wrong
        requestData.put("CreateTime", "20170313080000");
        // setting data
        requestData.put("StoreOrderNo", "99991123");
        requestData.put("StoreInfo", "888811");
        requestData.put("DeviceInfo", "P1");

        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();

        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("7002", header.get("StatusCode").getAsString());


        requestData.put("Method", "10300");
        requestData.put("ServiceType", "Cancel");
        requestData.put("MchId", "test");
        requestData.put("CreateTime", "20170313080000");
        // setting data
        requestData.put("StoreOrderNo", "");//wrong
        requestData.put("StoreInfo", "888811");
        requestData.put("DeviceInfo", "P1");

        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();

        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("8002", header.get("StatusCode").getAsString());
    }

    @Test
    public void testPiMicropay() throws Exception {
        Random rand = new Random();

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10300");
        requestData.put("ServiceType", SystemInstance.TYPE_MICROPAY);
        requestData.put("MchId", "super");
        requestData.put("CreateTime", getNowTime());
        // setting data
        requestData.put("StoreOrderNo", "PiTest" + rand.nextInt(9999));
        requestData.put("DeviceInfo", "P1");
        requestData.put("Body", "body");
        requestData.put("TotalFee", "1");
        requestData.put("FeeType", "TWD");
        requestData.put("Detail", "detail");
        requestData.put("Attach", "A123456789");
        requestData.put("GoodsTag", "food");
        requestData.put("StoreInfo", "888811");
        requestData.put("Cashier", "Cashier");
        requestData.put("AuthCode", "PI38KTTKONwEmSa2nw");
        requestData.put("Extended", "JSON");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("7000", header.get("StatusCode").getAsString());


        requestData.put("Method", "10300");
        requestData.put("ServiceType", SystemInstance.TYPE_MICROPAY);
        requestData.put("MchId", "test");//wrong
        requestData.put("CreateTime", "20170401080000");
        // setting data
        requestData.put("StoreOrderNo", "99991125");
        requestData.put("DeviceInfo", "P1");
        requestData.put("Body", "body");
        requestData.put("TotalFee", "1");
        requestData.put("FeeType", "TWD");
        requestData.put("Detail", "detail");
        requestData.put("Attach", "A123456789");
        requestData.put("GoodsTag", "food");
        requestData.put("StoreInfo", "888811");
        requestData.put("Cashier", "Cashier");
        requestData.put("AuthCode", "PI38KTTKONwEmSa2nw");
        requestData.put("Extended", "JSON");

        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();

        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("8002", header.get("StatusCode").getAsString());


        requestData.put("Method", "10300");
        requestData.put("ServiceType", SystemInstance.TYPE_MICROPAY);
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "201704010859");// wrong
        // setting data
        requestData.put("StoreOrderNo", "99991125");
        requestData.put("DeviceInfo", "P1");
        requestData.put("Body", "body");
        requestData.put("TotalFee", "1");
        requestData.put("FeeType", "TWD");
        requestData.put("Detail", "detail");
        requestData.put("Attach", "A123456789");
        requestData.put("GoodsTag", "food");
        requestData.put("StoreInfo", "888811");
        requestData.put("Cashier", "Cashier");
        requestData.put("AuthCode", "PI38KTTKONwEmSa2nw");
        requestData.put("Extended", "JSON");

        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();

        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

//        Assert.assertEquals("8002", header.get("StatusCode").getAsString());
        Assert.assertEquals("8002", header.get("StatusCode").getAsString());


        requestData.put("Method", "10300");
        requestData.put("ServiceType", SystemInstance.TYPE_MICROPAY);
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170401080000");
        // setting data
        requestData.put("StoreOrderNo", "");//wrong
        requestData.put("DeviceInfo", "P1");
        requestData.put("Body", "body");
        requestData.put("TotalFee", "1");
        requestData.put("FeeType", "TWD");
        requestData.put("Detail", "detail");
        requestData.put("Attach", "A123456789");
        requestData.put("GoodsTag", "food");
        requestData.put("StoreInfo", "888811");
        requestData.put("Cashier", "Cashier");
        requestData.put("AuthCode", "PI38KTTKONwEmSa2nw");
        requestData.put("Extended", "JSON");

        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();

        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("8002", header.get("StatusCode").getAsString());


        requestData.put("Method", "10300");
        requestData.put("ServiceType", SystemInstance.TYPE_MICROPAY);
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170401080000");
        // setting data
        requestData.put("StoreOrderNo", "99991125");
        requestData.put("DeviceInfo", "P1");
        requestData.put("Body", "");//wrong
        requestData.put("TotalFee", "1");
        requestData.put("FeeType", "TWD");
        requestData.put("Detail", "detail");
        requestData.put("Attach", "A123456789");
        requestData.put("GoodsTag", "food");
        requestData.put("StoreInfo", "888811");
        requestData.put("Cashier", "Cashier");
        requestData.put("AuthCode", "PI38KTTKONwEmSa2nw");
        requestData.put("Extended", "JSON");

        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();

        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("8002", header.get("StatusCode").getAsString());


        requestData.put("Method", "10300");
        requestData.put("ServiceType", SystemInstance.TYPE_MICROPAY);
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170401080000");
        // setting data
        requestData.put("StoreOrderNo", "99991125");
        requestData.put("DeviceInfo", "P1");
        requestData.put("Body", "body");
        requestData.put("TotalFee", "1");
        requestData.put("FeeType", "TWD");
        requestData.put("Detail", "detail");
        requestData.put("Attach", "A123456789");
        requestData.put("GoodsTag", "food");
        requestData.put("StoreInfo", "888811");
        requestData.put("Cashier", "Cashier");
        requestData.put("AuthCode", "");//wrong
        requestData.put("Extended", "JSON");

        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();

        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("8002", header.get("StatusCode").getAsString());


        requestData.put("Method", "10300");
        requestData.put("ServiceType", SystemInstance.TYPE_MICROPAY);
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170401080000");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("StoreOrderNo", "99991125");
        requestData.put("DeviceInfo", "P1");
        requestData.put("Body", "body");
        requestData.put("TotalFee", "1");
        requestData.put("FeeType", "TWD");
        requestData.put("Detail", "detail");
        requestData.put("Attach", "A123456789");
        requestData.put("GoodsTag", "food");
        requestData.put("StoreInfo", "888811");
        requestData.put("Cashier", "Cashier");
        requestData.put("AuthCode", "PI38KTTKONwEmSa2nw");
        requestData.put("Extended", "JSON");

        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();

        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("7000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testPiSingleOrderQuery() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header

        requestData.put("Method", "10300");
        requestData.put("ServiceType", "SingleOrderQuery");
        requestData.put("MchId", "S2PT00003");
        requestData.put("CreateTime", getNowTime());
        requestData.put("TradeKey","9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");
        // setting data
        requestData.put("StoreOrderNo", "1112223330043");
        requestData.put("SysOrderNo", "");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://s.intella.co/allpaypass/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("7141", header.get("StatusCode").getAsString());


        requestData.put("Method", "10300");
        requestData.put("ServiceType", "SingleOrderQuery");
        requestData.put("MchId", "test");//wrong
        requestData.put("CreateTime", "20170313080000");
        // setting data
        requestData.put("StoreOrderNo", "99991124");
        requestData.put("SysOrderNo", "");

        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();

        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("7002", header.get("StatusCode").getAsString());


        requestData.put("Method", "10300");
        requestData.put("ServiceType", "SingleOrderQuery");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170313080000");
        // setting data
        requestData.put("StoreOrderNo", "");//wrong
        requestData.put("SysOrderNo", "");

        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();

        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("8002", header.get("StatusCode").getAsString());


        requestData.put("Method", "10300");
        requestData.put("ServiceType", "SingleOrderQuery");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170313080000");
        // setting data
        requestData.put("StoreOrderNo", "");
        requestData.put("SysOrderNo", "");//wrong

        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();

        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("8002", header.get("StatusCode").getAsString());
    }

    @Test
    public void testPiInApp() throws Exception {
        Random rand = new Random();

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header

        requestData.put("Method", "60300");
        requestData.put("ServiceType", "InApp");
        requestData.put("MchId", "InApp");
        requestData.put("CreateTime", getNowTime());
        // setting data
        requestData.put("StoreOrderNo", "InAppTest" + Integer.toString(rand.nextInt(9999)));
        //requestData.put("UserId", "bensoni");
        requestData.put("TotalFee", "1");
        requestData.put("MchName", "MAA");
        requestData.put("MchKey", "KrZBOCkYo3b18MdR2JrcxJFzfoVdycgh");
        requestData.put("Body", "test body");
        requestData.put("Detail", "test detail");


        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("7141", header.get("StatusCode").getAsString());

    }

    @Test
    public void testPiRefund2() throws Exception {

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header

        requestData.put("Method", "60300");
        requestData.put("ServiceType", "Refund");
        requestData.put("MchId", "InApp");
        requestData.put("CreateTime", "20170520080000");
        // setting data
        requestData.put("StoreOrderNo", "InAppTest002");
        //requestData.put("UserId", "bensoni");
        requestData.put("TotalFee", "1");
        //requestData.put("MchName", "麥味登");
        requestData.put("MchKey", "KrZBOCkYo3b18MdR2JrcxJFzfoVdycgh");
        //requestData.put("Body", "test body");
        //requestData.put("Detail", "test detail");


        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("7141", header.get("StatusCode").getAsString());
    }

    private String getNowTime() {
        SimpleDateFormat sdf1 = new SimpleDateFormat(SystemInstance.DATE_PATTERN);
        String str = "";
        str = sdf1.format(new Date());
        return str;
    }
}
