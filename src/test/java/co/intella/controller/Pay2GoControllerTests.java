package co.intella.controller;

import co.intella.crypto.AesCryptoUtil;
import co.intella.crypto.KeyReader;
import co.intella.crypto.RsaCryptoUtil;
import co.intella.model.IntegratedRequest;
import co.intella.model.PaymentAccount;
import co.intella.net.Constant;
import co.intella.service.Pay2GoService;
import co.intella.service.PaymentAccountService;
import co.intella.utility.Cryption;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import javax.crypto.SecretKey;
import java.io.File;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Alex
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class Pay2GoControllerTests {
    @Autowired
    private TestRestTemplate restTemplate;

    @Resource
    private Pay2GoService pay2GoService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Test
    public void testCreateOrder() throws Exception {
        DateTime now = DateTime.now();

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("token", "FoUVLZMYSll");


        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("key/P2GTointella-Stage-public.der").getFile());
        PublicKey publicKey = KeyReader.loadPublicKeyFromDER(file);

        byte[] bbb = RsaCryptoUtil.encrypt(new Gson().toJson(requestData),publicKey);

        String encryptRequest = Base64.encode(bbb);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://s.intella.co/allpaypass/api/Pay2Go/createOrder", encryptRequest, String.class);
        String result = responseEntity.getBody();


    }

    @Test
    public void testNotify() throws Exception {
        DateTime now = DateTime.now();

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Status", "SUCCESS");
        requestData.put("Version", "1.0");
        requestData.put("MerchantID", "PG300000004668");
        requestData.put("TradeInfo", "f9e15055bc35c78d583277acfc39c05605d4c6c97c8c1eba741d991f529fc1a7e69d066131f6116a805f79e23899e664bf4d5f455b50dbc8060f512671557489db03bb348149cd777cd4e00301c1665709aa92edcc4abd8557ce48f4ebef74943aa7bc77aa4f1a84138ef16a42bb55432a4686796893d27f93f2fcb988e4d5361c27729521c32a48aa1293ab4bbc99206ef62d74b88174fcdadef5396a894b2813568973c13828590f57072e50bf55707e8eb77082006836f7d22ed9766419edc3db6b970b69dca26dcc1fe6cb437e42cb216f4563907cfd7f6c4162fc8119067c1646e46a0b1b333c3716b85db8c24da095090c3adf462f280d6d65b31443ba76642211260be631b18781dd922d9a99ff0c2ff44c5e5dddd96626fef51db666bc6e68f5ce9bda93a1ec0e2cb77a3d0977a853192b9b5bf72bae0211afd144107fa343d2267d452e35c84bd6a26a55f68a659e9916af0a5eb8144d0ee4c51df5347a151cce9837f827801e921c1dff192240c0bd2c6dba49416729dfbbf71ec218834cf890c8b0cd2ca6e72620e436cc4b147249bc22e364faef79421b8d1676abd7c900406da6e6584709de129a95b8041bbd2b185fcfae38e0d16614742c65742e4daa74dbcddda587ee3d96a1259c6847f0fcce2cde24f729ebd05c79c0d9e1850cf1e1b62f0dd076ec8f7711be4851107372eedac10c2b679a51f9159dfb65c4eb3c02162ddc3f39a95620c61be3d8f329c9a044994ad6bb2058964921ea8e2efd3d731877a0965ff9b42ea54699e731a514f9ead0d6cc20fd90e22a5160");
        requestData.put("TradeSha", "C127E2B15631C3550DC9F0E82F46FA09BE227932BA3055D31A1C1CC2F7654614");

///https://s.intella.co/allpaypass
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/Pay2Go/notify", new Gson().toJson(requestData), String.class);
        String result = responseEntity.getBody();


    }

    @Test
    public void testVerify() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("TradeInfo", "f9e15055bc35c78d583277acfc39c05605d4c6c97c8c1eba741d991f529fc1a7e69d066131f6116a805f79e23899e664bf4d5f455b50dbc8060f512671557489db03bb348149cd777cd4e00301c1665709aa92edcc4abd8557ce48f4ebef74943aa7bc77aa4f1a84138ef16a42bb55432a4686796893d27f93f2fcb988e4d5361c27729521c32a48aa1293ab4bbc99206ef62d74b88174fcdadef5396a894b2813568973c13828590f57072e50bf55707e8eb77082006836f7d22ed9766419edc3db6b970b69dca26dcc1fe6cb437e42cb216f4563907cfd7f6c4162fc8119067c1646e46a0b1b333c3716b85db8c24da095090c3adf462f280d6d65b31443ba76642211260be631b18781dd922d9a99ff0c2ff44c5e5dddd96626fef51db666bc6e68f5ce9bda93a1ec0e2cb77a3d0977a853192b9b5bf72bae0211afd144107fa343d2267d452e35c84bd6a26a55f68a659e9916af0a5eb8144d0ee4c51df5347a151cce9837f827801e921c1dff192240c0bd2c6dba49416729dfbbf71ec218834cf890c8b0cd2ca6e72620e436cc4b147249bc22e364faef79421b8d1676abd7c900406da6e6584709de129a95b8041bbd2b185fcfae38e0d16614742c65742e4daa74dbcddda587ee3d96a1259c6847f0fcce2cde24f729ebd05c79c0d9e1850cf1e1b62f0dd076ec8f7711be4851107372eedac10c2b679a51f9159dfb65c4eb3c02162ddc3f39a95620c61be3d8f329c9a044994ad6bb2058964921ea8e2efd3d731877a0965ff9b42ea54699e731a514f9ead0d6cc20fd90e22a5160");
        requestData.put("TradeSha", "C127E2B15631C3550DC9F0E82F46FA09BE227932BA3055D31A1C1CC2F7654614");

        paymentAccountService.getOne("super",24);

        pay2GoService.verify(requestData,paymentAccountService.getOne("super",24));
    }
    @Test
    public void testRefund() throws Exception {

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "00000");
        requestData.put("ServiceType", "Refund");
        //requestData.put("MchId", "04230623201");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss
        requestData.put("TradeKey","9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");

        // setting data
        requestData.put("StoreRefundNo", "R4BF0018071700004");//UUID.randomUUID().toString()
        requestData.put("SysOrderNo", "");
        requestData.put("StoreOrderNo", "4BF0018071700004");

        requestData.put("DeviceInfo", "skb0001");
        requestData.put("TotalFee", "1");
        requestData.put("RefundFeeType", "TWD");
        requestData.put("RefundFee", "1");
        requestData.put("RefundedMsg", "product");
        requestData.put("RefundKey", "9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");



        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
//        https://s.intella.co/allpaypass
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }
}

