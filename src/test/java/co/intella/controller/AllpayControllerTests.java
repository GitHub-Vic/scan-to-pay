package co.intella.controller;

import co.intella.crypto.AesCryptoUtil;
import co.intella.domain.allpay.AllpayMicropayRequestData;
import co.intella.model.IntegratedRequest;
import co.intella.net.Constant;
import co.intella.utility.Cryption;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.SecretKey;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Alex
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AllpayControllerTests {
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testAllpayMicropay() throws Exception {
        DateTime now = DateTime.now();

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10500");
        requestData.put("ServiceType", "Micropay");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20180116120000");    // format must be yyyyMMddHHmmss

        requestData.put("StoreOrderNo", "AllPT" + now.toString("yyMMddHHmmss"));
        requestData.put("DeviceInfo", "23412342");
        requestData.put("Body", "1323");
        requestData.put("TotalFee", "1");
        requestData.put("AuthCode", "AP191A0XYW6CQ2TNZQ");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
//https://a.intella.co/allpaypass
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
        AllpayMicropayRequestData allpayMicropayRequestData =new AllpayMicropayRequestData();
    }

    @Test
    public void testAllpayRefund() throws Exception {

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "00000");
        requestData.put("ServiceType", "Refund");
        //requestData.put("MchId", "04230623201");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("StoreRefundNo", "RllPT180116152215");//UUID.randomUUID().toString()
        requestData.put("SysOrderNo", "");
        requestData.put("StoreOrderNo", "AllPT180116152215");

        requestData.put("DeviceInfo", "23412342");
        requestData.put("TotalFee", "1");
        requestData.put("RefundFeeType", "TWD");
        requestData.put("RefundFee", "1");
        requestData.put("RefundedMsg", "product");
        requestData.put("RefundKey", "0000");



        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testAllpayQuery() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();

        requestData.put("Method", "00000");
        requestData.put("ServiceType", "SingleOrderQuery");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20171117120000");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("DeviceInfo", "23412342");
        requestData.put("StoreOrderNo", "51OL2171116182745");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());

    }
}
