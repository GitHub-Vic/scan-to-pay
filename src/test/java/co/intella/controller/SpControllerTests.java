package co.intella.controller;

import co.intella.crypto.AesCryptoUtil;
import co.intella.model.IntegratedRequest;
import co.intella.net.Constant;
import co.intella.utility.Cryption;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.SecretKey;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Andy Lin
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public class SpControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testSpCreditBg() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "20600");
        requestData.put("ServiceType", "Payment");
//        requestData.put("MchId", "MS314968777");  //formal store
        requestData.put("MchId", "MS31538025");   //test store

        requestData.put("CreateTime", "1493807228");
        // setting data
        requestData.put("StoreOrderNo", "1493807228");
        requestData.put("TotalFee", "1");
        requestData.put("ver", "1.0");
        requestData.put("Body", "test~");
        requestData.put("Email", "andy@intella.co");
        requestData.put("CardId", "4000221111111111");
        requestData.put("ExpireDate", "1712");
        requestData.put("ExtenNo", "111");
        requestData.put("TransMode", "0");
        requestData.put("Installment", "60");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());

    }

    @Test
    public void testSpRefundAndPay() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "20600");
        requestData.put("ServiceType", "Refund");
//        requestData.put("MchId", "MS314968777");  //formal store
        requestData.put("MchId", "MS31538025");   //test store

        requestData.put("CreateTime", "1493195760");
        // setting data
        requestData.put("RespondType", "JSON");
        requestData.put("StoreOrderNo", "1493195760");
        requestData.put("SysOrderNo", "1493195766");
        requestData.put("TotalFee", "1");
        requestData.put("ver", "1.2");
        requestData.put("IndexType", "2");  // StoreOrderNo: 1, SysOrderNo: 2
        requestData.put("CloseType", "2");  // pay:1, refund: 2

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("7325", header.get("StatusCode").getAsString());

    }

    @Test
    public void testSpSingleQuery() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "20600");
        requestData.put("ServiceType", "SingleQuery");
//        requestData.put("MchId", "MS314968777");  //formal store
        requestData.put("MchId", "MS31538025");   //test store

        requestData.put("CreateTime", "1493195760");
        // setting data
        requestData.put("RespondType", "JSON");
        requestData.put("StoreOrderNo", "1493195760");
        requestData.put("TotalFee", "1");
        requestData.put("ver", "1.2");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());

    }


}
