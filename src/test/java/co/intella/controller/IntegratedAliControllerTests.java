package co.intella.controller;

import co.intella.crypto.AesCryptoUtil;
import co.intella.model.IntegratedRequest;
import co.intella.net.Constant;
import co.intella.utility.Cryption;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.SecretKey;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Andy Lin
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegratedAliControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testAliMicropay() throws Exception {
        DateTime now = DateTime.now();

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10220");
        requestData.put("ServiceType", SystemInstance.TYPE_MICROPAY);
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170826094945");    // format must be yyyyMMddHHmmss

        requestData.put("StoreOrderNo", "AliT" + now.toString(SystemInstance.DATE_PATTERN));
        requestData.put("DeviceInfo", "SCTW05");
        requestData.put("Body", "Item name");
        requestData.put("TotalFee", "1");
        requestData.put("AuthCode", "282928312595383646");
        //        requestData.put("AuthCode", "http://www.intella.co");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://a.intella.co/allpaypass/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();
//        ClassLoader classLoader = getClass().getClassLoader();
//        File file = new File(classLoader.getResource("key/pub.der").getFile());
//        PublicKey publicKey = null;
//        if(publicKey == null) {
//            publicKey =  KeyReader.loadPublicKeyFromDER(file);
//        }
//        String result = HttpRequestUtil.httpsPost("https://www.intellapay.org/allpaypass/api/general",requestData, publicKey, secretKey);
        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testAliRefund() throws Exception {

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10220");
        requestData.put("ServiceType", "Refund");
        //requestData.put("MchId", "04230623201");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170826094945");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("StoreRefundNo", "Rqi3cwdism80000000016");//UUID.randomUUID().toString()
        requestData.put("SysOrderNo", "");
        requestData.put("StoreOrderNo", "qi3cwdism80000000016");

        requestData.put("DeviceInfo", "SCTW05");
        requestData.put("TotalFee", "1");
        requestData.put("RefundFeeType", "TWD");
        requestData.put("RefundFee", "1");
        requestData.put("RefundedMsg", "product");
        requestData.put("RefundKey", "0000");



        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

         String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testAliCurrency() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10220");
        requestData.put("ServiceType", "Currency");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170419120000");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("DeviceInfo", "23412342");
        requestData.put("Amount", "100");
        //requestData.put("Currency", "USD");


        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testAliQueryAction() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10220");
        requestData.put("ServiceType", "QueryAction");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170419120000");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("DeviceInfo", "23412342");
        requestData.put("StoreOrderNo", "2017041900000001");
        //requestData.put("Amount", "100");
        //requestData.put("Currency", "USD");


        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://s.intella.co/allpaypass/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testAliQuery() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();

        requestData.put("Method", "10220");
        requestData.put("ServiceType", "Query");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170419120000");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("DeviceInfo", "23412342");
        requestData.put("StoreOrderNo", "OL320170616171402");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://s.intella.co/allpaypass/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());

    }

    @Test
    public void testAliCancel() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();

        requestData.put("Method", "10220");
        requestData.put("ServiceType", "Cancel");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170826094945");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("DeviceInfo", "SCTW05");
        requestData.put("StoreOrderNo", "AliT170802161313");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }


    @Test
    public void testAliOLRefund() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();

        requestData.put("Method", "10220");
        requestData.put("ServiceType", "Cancel");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170419120000");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("DeviceInfo", "23412342");
        requestData.put("StoreOrderNo", "AliTest20170419002");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void rnadom() throws Exception {

        SecureRandom random = new SecureRandom();
        String  a =  new BigInteger(130, random).toString(32);
    }
}