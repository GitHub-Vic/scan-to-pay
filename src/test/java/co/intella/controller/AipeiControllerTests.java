package co.intella.controller;

import co.intella.crypto.AesCryptoUtil;
import co.intella.crypto.KeyReader;
import co.intella.domain.allpay.AllpayMicropayRequestData;
import co.intella.model.IntegratedRequest;
import co.intella.net.Constant;
import co.intella.utility.Cryption;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

import static co.intella.crypto.RsaCryptoUtil.decrypt;

/**
 * @author Alex
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AipeiControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testGetOrderDetail() throws Exception {

    }

    @Test
    public void testMicropay() throws Exception {
        DateTime now = DateTime.now();

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "11400");
        requestData.put("ServiceType", SystemInstance.TYPE_MICROPAY);
        requestData.put("MchId", "super");
        requestData.put("CreateTime", now.toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss
        requestData.put("TradeKey","9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");

        requestData.put("StoreOrderNo", "AiT" + now.toString(SystemInstance.DATE_PATTERN));
        requestData.put("DeviceInfo", "23412342");
        requestData.put("Body", "TestItem");
        requestData.put("TotalFee", "1");
        requestData.put("AuthCode", "888888888888888888");
        requestData.put("Detail", "888888888888888888");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
//https://a.intella.co/allpaypass
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://s.intella.co/allpaypass/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
        AllpayMicropayRequestData allpayMicropayRequestData =new AllpayMicropayRequestData();
    }

    @Test
    public void testQuery() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();

        requestData.put("Method", "00000");
        requestData.put("ServiceType", "SingleOrderQuery");
        requestData.put("MchId", "S2PT00359");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss
        requestData.put("TradeKey","9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");

        // setting data
//        requestData.put("PiAppUrl", "onAidlSuccessResult orderId:IntellaA180123000001");
        requestData.put("StoreOrderNo", "S2PT0035900077");

        SecretKey secretKey = generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://s.intella.co/allpaypass/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());

    }

    public static SecretKey generateSecreteKey() throws NoSuchAlgorithmException {
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(128, new SecureRandom());
        return keyGen.generateKey();
    }

    @Test
    public void testRefund() throws Exception {

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "00000");
        requestData.put("ServiceType", "Refund");
        //requestData.put("MchId", "04230623201");
        requestData.put("MchId", "S2PT00359");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss
        requestData.put("TradeKey","9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");

        // setting data
        requestData.put("StoreRefundNo", "RS2PT0035900076");//UUID.randomUUID().toString()
        requestData.put("SysOrderNo", "");
        requestData.put("StoreOrderNo", "S2PT0035900076");

        requestData.put("DeviceInfo", "skb0001");
        requestData.put("TotalFee", "1");
        requestData.put("RefundFeeType", "TWD");
        requestData.put("RefundFee", "1");
        requestData.put("RefundedMsg", "product");
        requestData.put("RefundKey", "9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");



        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://s.intella.co/allpaypass/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }


    @Test
    public void testOrderQuery() throws Exception {

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "00000");
        requestData.put("ServiceType", "OrderQuery");
        requestData.put("MchId", "S2PT00355");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss
        requestData.put("TradeKey","9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");

        // setting data
        requestData.put("EndDate", "20180313000000");
        requestData.put("StartDate", "20180312000000");
        requestData.put("OrderStatus", "1");



        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://s.intella.co/allpaypass/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }


    @Test
    public void testfff() throws Exception {
        decryptSecretKey("MTcHGxtAlBdUHiwtUXxKlHxM+sMVkYffbNBoTKNhMVCgE7sxH8Y0fhI/c/Z2k/gtvSf08vUiiE7FPzaLw0VuKBuVC2tfwmmLu+nGGFfuQRghNiz/qFXbQi/Y9+3V97oQ+7XAlp+HrxYBmjxmIr4JiIYgJItSHk3fMD3y+FBEDB/+eTBXJ5nZXA6rCirbsMKZ7MTtl8QNOmEHdi4k/QjJL4O5hvrFXPebJfs7HJ7rJvdhksUMkIt9T50vhN1h5LGs/6BdThDqXjBooOMkfVpCt87jkw09I2O3HB4RFGrQxUhsCsLIfv93hE2dv7x0MU5y1yjyaRbRKY6FFMOAT/A3dw==");
    }
    private SecretKey decryptSecretKey(String encryptApiKey) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("key/stage-pri.der").getFile());
        PrivateKey privateKey = KeyReader.loadPrivateKeyFromDER(file);
        byte[] byteKeys = Base64.decode(encryptApiKey);
        String aesKey = decrypt(byteKeys, privateKey);
        return AesCryptoUtil.convertSecretKey(Base64.decode(aesKey));
    }
}
