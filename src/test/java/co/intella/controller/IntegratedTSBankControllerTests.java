package co.intella.controller;

import co.intella.crypto.AesCryptoUtil;
import co.intella.model.IntegratedRequest;
import co.intella.net.Constant;
import co.intella.utility.Cryption;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.SecretKey;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Alex
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegratedTSBankControllerTests {
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testTsbPayment() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));
        requestData.put("MchId", "super");
        requestData.put("Method", "20000");
        requestData.put("ServiceType", "Payment");
        requestData.put("TradeKey","9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");


        requestData.put("Body", "Body");
        requestData.put("StoreOrderNo", generateSerialNumber());
        requestData.put("DeviceInfo", "T0000000");
        requestData.put("TotalFee", "3");
        requestData.put("CardId", "5241150340018200");
        requestData.put("ExtenNo", "833");
        requestData.put("ExpireDate", "2303");
        requestData.put("StoreInfo", "intella-taipei");
        requestData.put("StoreName", "intella-taipei");
        requestData.put("StoreType", "intella-taipei");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testTsbQuery() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("MchId", "super");
        requestData.put("Method", "20800");
        requestData.put("ServiceType", "SingleOrderQuery");
        requestData.put("StoreOrderNo", "2bf9bb477da3d1f_1");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));
        requestData.put("TradeKey","9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");

        //requestData.put("DeviceInfo", "A6113004");
//        requestData.put("Body", "body");
//        requestData.put("TotalFee", "1000");
//        requestData.put("FeeType", "TWD");
//        requestData.put("Detail", "trade detail");
//        requestData.put("StoreInfo", "intella-taipei");
//        requestData.put("Cashier", "miles");
//        requestData.put("BarcodeMode", "barcodeMode");
//        requestData.put("Extended", "");
//        requestData.put("TransMode", "0");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        //https://y.intella.co/allpaypass
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }
    @Test
    public void testTsbCharge() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("MchId", "topear");
        requestData.put("Method", "20800");
        requestData.put("ServiceType", "Charge");
        requestData.put("StoreOrderNo", "2bf9e9f54102647_2");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));
        requestData.put("TradeKey","1f1ca67da4612af25e6015bcb4f26729800a736815027d02312d3e7e5805f84e");

        //requestData.put("DeviceInfo", "A6113004");
//        requestData.put("Body", "body");
        requestData.put("TotalFee", "1000");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        //https://y.intella.co/allpaypass
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://a.intella.co/allpaypass/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testTsbCancel() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("MchId", "super");
//        requestData.put("MchId", "0100829354");
        requestData.put("Method", "00000");
        requestData.put("ServiceType", "Refund");
        requestData.put("StoreOrderNo", "ABABE18011800001");
        requestData.put("StoreRefundNo", "RABABE18011800001");
        requestData.put("CreateTime", "20180118140000");
        requestData.put("DeviceInfo", "T0000000");
        requestData.put("TotalFee", "1");
        requestData.put("FeeType", "TWD");
        requestData.put("Detail", "trade detail");
        requestData.put("StoreInfo", "intella-taipei");
        requestData.put("Cashier", "miles");
        requestData.put("BarcodeMode", "barcodeMode");
        requestData.put("Extended", "");
        requestData.put("CardId", "6241150340018200");
        requestData.put("ExtenNo", "833");
        requestData.put("ExpireDate", "2303");
        requestData.put("TransMode", "0");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://a.intella.co/allpaypass/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testTsbChargeBack() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("MchId", "super");
//        requestData.put("MchId", "0100829354");
        requestData.put("Method", "20800");
        requestData.put("ServiceType", "Return");
        requestData.put("StoreOrderNo", "tstest8660251753");
        requestData.put("StoreRefundNo", "Atstest8660251753");
        requestData.put("CreateTime", "20170801140000");
        requestData.put("TimeExpire", "20170803140000");
        requestData.put("DeviceInfo", "T0000000");
        requestData.put("Body", "");
        requestData.put("TotalFee", "1");
        requestData.put("FeeType", "TWD");
        requestData.put("Detail", "trade detail");
        requestData.put("StoreInfo", "intella-taipei");
        requestData.put("Cashier", "miles");
        requestData.put("BarcodeMode", "barcodeMode");
        requestData.put("Extended", "");
        requestData.put("CardId", "6241150340018200");
        requestData.put("ExtenNo", "833");
        requestData.put("ExpireDate", "2303");
        requestData.put("TransMode", "0");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        //https://a.intella.co/allpaypass
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://a.intella.co/allpaypass/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testTsbPartialRefund() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("MchId", "super");
//        requestData.put("MchId", "0100829354");
        requestData.put("Method", "20800");
        requestData.put("ServiceType", "PartialRefund");
        requestData.put("StoreOrderNo", "0758364842");
        requestData.put("StoreRefundNo", "PR0758364842");
        requestData.put("CreateTime", "20171026140000");
        requestData.put("TimeExpire", "20170803140000");
        requestData.put("DeviceInfo", "T0000000");
        requestData.put("RefundFee", "2");
        requestData.put("RefundFeeType", "TWD");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        //https://a.intella.co/allpaypass
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://y.intella.co/allpaypass/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    private String generateSerialNumber() {

        char seeds[] = {'0','1', '2', '3', '4', '5', '6', '7', '8', '9'};
        char randStr[] = new char[10];
        for (int i=0;i< randStr.length;i++) {
            randStr[i] = seeds[(int)Math.round(Math.random() * (seeds.length - 1))];
        }

        return new String(randStr);
    }
}
