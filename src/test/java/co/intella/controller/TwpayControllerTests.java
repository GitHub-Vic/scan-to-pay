package co.intella.controller;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang3.StringEscapeUtils;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import co.intella.AllPayApplication;
import co.intella.crypto.AesCryptoUtil;
import co.intella.domain.allpay.AllpayMicropayRequestData;
import co.intella.model.IntegratedRequest;
import co.intella.net.Constant;
import co.intella.net.HttpRequestUtil;
import co.intella.utility.Cryption;
import co.intella.utility.SystemInstance;

/**
 * @author Alex
 */


@RunWith(SpringJUnit4ClassRunner.class)  
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes=AllPayApplication.class) //, classes=AllPayApplication.class
//@WebAppConfiguration
//@ContextConfiguration
public class TwpayControllerTests {
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testAllpayMicropay() throws Exception {
        DateTime now = DateTime.now();

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "12000");
        requestData.put("ServiceType", "Micropay");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", now.toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss
        requestData.put("TradeKey","9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");

        requestData.put("StoreOrderNo", "TW5566" + now.toString("yyMMddHHmmss"));
        requestData.put("DeviceInfo", "skb0001");
        requestData.put("Body", "1323");
        requestData.put("TotalFee", "1");
        //requestData.put("AuthCode", "YWhPCKAAAAFylQABwVwyNTQxMjAxODEwMTcyMTAyNTEwMTEwMDAwMDk5OTk5OTEwMDMwMDAwMDBjUBgRERiVNy6T/KzJADk5OTk5OTAxMjAwMDAwMjQIMDAwMDAwNDEACCjn7B/OfyX6MQ==");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        //https://a.intella.co/allpaypass
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
        AllpayMicropayRequestData allpayMicropayRequestData =new AllpayMicropayRequestData();
    }
    
    private static void testMicropay(Map<String, String> requestData) throws Exception {
        
       encryptAndDecrypt(requestData);
   }
    
    private static void testOLPay(Map<String, String> requestData) throws Exception {
    
    	 // OLPay
        requestData.put("ServiceType", "OLPay");
        encryptAndDecrypt(requestData);
    }
    
    private static void testRefund(Map<String, String> requestData) throws Exception {
        
        requestData.put("ServiceType", "Refund");
        requestData.put("StoreOrderNo", "000000001297");
        requestData.put("StoreRefundNo", "000000001297");
        requestData.put("RefundKey", "f944d874b1cc3f3450b52d6486c13f5855a1e6dc2dcfef47cce618a89d887548");
        requestData.put("RefundFee", "1");
        requestData.put("RefundedMsg", "Broken");

        encryptAndDecrypt(requestData);
    }
    
    private static void encryptAndDecrypt(Map<String, String> requestData) throws Exception {
    	
    	SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        String encodedKey = Base64.getEncoder().encodeToString(secretKey.getEncoded());

        System.out.println(encodedKey);
        String apiKey = StringEscapeUtils.escapeJson(encryptRequest.getApiKey().replace("\n", ""));
        String request = StringEscapeUtils.escapeJson(encryptRequest.getRequestBody().replace("\n", ""));
        
        String requestBody = String.format("{\"ApiKey\": \"%s\",\"Request\": \"%s\"}", apiKey, request);
        System.out.println("---------- request body ----------");
        System.out.println(requestBody);
        
        System.out.println("---------- decrypt response ----------");
        
        // String response = "{\"Response\":\"zFbUi2p8HhJXB06K6gukZMvbAcHR/TaPGY5KDcA0JQJ/HvU4NC9OBvL/vX9oGknP3k/Jqkx027vi\\nXJLIlyQ6JQn2He/ckRehqWUJCD2AizHOUGHyOEi3OoxCKbBkF4ZYPj9I5g7X3yBc7F2bZRSNnPSG\\nmm0B7pI5posRq4d1z59HhRpaf8ynj3k/7GV+NSR3vEUlItFzXziUHpHzL7mBQXmkou1GdWVmNKMS\\nxcpT99CNj1crVhbNVLdP0oXyKMBBjk8GePEC+am/MvI5x/GDFuzBShAp5CeKJ6lmGJzXML2sFieD\\nr56FzAtk8pEajnTU\"}"; 
        // mBXT4YcuBx2MyW4Bn0MtAw==
        String response = HttpRequestUtil.post("http://localhost:8081/allpaypass/api/general", requestBody);

        // restore key from encodedKey
        byte[] decodedKey = Base64.getDecoder().decode(encodedKey);
        secretKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES"); 
        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, response);
        System.out.println(decryptResponse);
    	
    }
    
    public static void main(String[] args) throws Exception {
    	
    	DateTime now = DateTime.now();

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "00000");
        requestData.put("ServiceType", "Micropay");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", now.toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss
        requestData.put("TradeKey","9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");

        requestData.put("StoreOrderNo", "TW5566" + now.toString("yyMMddHHmmss"));
        requestData.put("DeviceInfo", "skb0001");
        requestData.put("Body", "1323");
        requestData.put("TotalFee", "1");
        requestData.put("AuthCode", "YWhPCKAAAAFylQABwVwyNTQxMjAxOTAxMTYxNTM0MTAwMTEwMDAwMDk5OTk5OTEwMDMwMDAwMDBjUBkCExeJbo1g6bJiAzk5OTk5OTAxMjAwMDAxMTMIMDAwMDAwMjcACAr6smXwrhlYMQ==");

        testMicropay(requestData);
	}
}
