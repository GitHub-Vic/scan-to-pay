package co.intella.controller;

import co.intella.crypto.AesCryptoUtil;
import co.intella.model.IntegratedRequest;
import co.intella.net.Constant;
import co.intella.utility.Cryption;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.bouncycastle.util.encoders.Base64;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Miles
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegratedEzCardControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    private final String HOST = "https://dev.intella.co/allpaypass/api/general";

    @Test
    public void testEzCardSignOn() throws Exception {

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "31800");
        requestData.put("ServiceType", "SignOn");
        requestData.put("MchId", "super");
        requestData.put("TradeKey", "0000");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss

        requestData.put("DeviceId", "01304098");

//        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        SecretKey secretKey = new SecretKeySpec(Base64.decode("Y3UJ147HKIYRT8Ovrsik0A=="), 0, Base64.decode("Y3UJ147HKIYRT8Ovrsik0A==").length, "AES");
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(HOST, encryptRequest, String.class);
        String result = responseEntity.getBody();
        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testEzCardPayment() throws Exception {

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "31800");
        requestData.put("ServiceType", "Payment");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss

        requestData.put("Amount", "1");
        requestData.put("Retry", "0");
        requestData.put("DeviceId", "01304098");
        requestData.put("StoreOrderNo", "TEST" + DateTime.now().toString(SystemInstance.DATE_PATTERN));
        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(HOST, encryptRequest, String.class);
        String result = responseEntity.getBody();
        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testEzCardBalance() throws Exception {

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "31800");
        requestData.put("ServiceType", "BalanceQuery");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss

        requestData.put("DeviceId", "01304098");
        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(HOST, encryptRequest, String.class);
        String result = responseEntity.getBody();
        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testEzCardDeviceQuery() throws Exception {

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "31800");
        requestData.put("ServiceType", "DeviceQuery");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss

        requestData.put("DeviceId", "00000000");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(HOST, encryptRequest, String.class);
        String result = responseEntity.getBody();
        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testEzCardReserve() throws Exception {

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "31800");
        requestData.put("ServiceType", "Reserve");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss

        requestData.put("DeviceId", "01304098");
        requestData.put("Amount", "1");
        requestData.put("Retry", "0");
        requestData.put("StoreOrderNo", "TEST" + DateTime.now().toString(SystemInstance.DATE_PATTERN));

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(HOST, encryptRequest, String.class);
        String result = responseEntity.getBody();
        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testEzCardRefund() throws Exception {

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "31800");
        requestData.put("ServiceType", "Refund");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss

        requestData.put("DeviceId", "01304121");
        requestData.put("Amount", "1");
        requestData.put("StoreOrderNo", "");
        requestData.put("Retry", "0");
        requestData.put("RefundKey", "0000");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(HOST, encryptRequest, String.class);
        String result = responseEntity.getBody();
        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testEzCardCheckout() throws Exception {

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "31800");
        requestData.put("ServiceType", "SettlementPatch");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss

        requestData.put("DeviceId", "01304098");

        //17122037
        requestData.put("BatchNo", "17122037");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(HOST, encryptRequest, String.class);
        String result = responseEntity.getBody();
        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testEzCardOrderQuery() throws Exception {

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "31800");
        requestData.put("ServiceType", "OrderQuery");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss

        requestData.put("DeviceId", "01304098");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(HOST, encryptRequest, String.class);
        String result = responseEntity.getBody();
        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testEzCardReserveOrderQuery() throws Exception {

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "31800");
        requestData.put("ServiceType", "ReserveOrderQuery");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss

        requestData.put("DeviceId", "01304098");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(HOST, encryptRequest, String.class);
        String result = responseEntity.getBody();
        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testIdQuery() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "31800");
        requestData.put("ServiceType", "IdQuery");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss

        requestData.put("DeviceId", "01304098");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(HOST, encryptRequest, String.class);
        String result = responseEntity.getBody();
        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testEzcOrderQuery() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "31800");
        requestData.put("ServiceType", "EZCOrderQuery");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss

        requestData.put("DeviceId", "01304098");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(HOST, encryptRequest, String.class);
        String result = responseEntity.getBody();
        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testEzcRefund() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "31800");
        requestData.put("ServiceType", "EZCRefund");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss

        requestData.put("DeviceId", "01304098");
        requestData.put("Amount", "1");
        requestData.put("Retry", "0");
        requestData.put("RefundKey", "0000");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(HOST, encryptRequest, String.class);
        String result = responseEntity.getBody();
        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testEzcCancelQuery() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "31800");
        requestData.put("ServiceType", "CancelOrderQuery");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss

        requestData.put("DeviceId", "01304098");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(HOST, encryptRequest, String.class);
        String result = responseEntity.getBody();
        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testEzcCancel() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "31800");
        requestData.put("ServiceType", "Cancel");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss

        requestData.put("DeviceId", "01304098");
        requestData.put("Retry", "0");
        requestData.put("RefundKey", "0000");

        requestData.put("StoreOrderNo", "TEST20171227154623");
        requestData.put("ActionType", "CANCEL_RESERVE"); //CANCEL_RESERVE //REFUND_TRADE

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(HOST, encryptRequest, String.class);
        String result = responseEntity.getBody();
        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

}
