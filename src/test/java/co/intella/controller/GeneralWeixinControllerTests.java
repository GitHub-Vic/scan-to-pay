package co.intella.controller;

import co.intella.crypto.AesCryptoUtil;
import co.intella.model.IntegratedRequest;
import co.intella.net.Constant;
import co.intella.utility.Cryption;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.SecretKey;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Miles
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GeneralWeixinControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testSingleOrderQuery() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "00000");
        requestData.put("ServiceType", "SingleOrderQuery");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170703103212");
        // setting data
        requestData.put("StoreOrderNo", "OL220170815205439");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity("http://localhost:8080/allpaypass/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testOrderQuery() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "00000");
        requestData.put("ServiceType", "OrderQuery");
        requestData.put("MchId", "super");
        // setting data
        requestData.put("StartDate", "20170601000000");
        requestData.put("EndDate", "20170630235959");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity("http://localhost:8080/allpaypass/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testWeixinMicropay() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10110");
        requestData.put("ServiceType", SystemInstance.TYPE_MICROPAY);
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170705142500");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("StoreOrderNo", "weixin070500001");
        requestData.put("DeviceInfo", "skb0001");
        requestData.put("Body", "Item name");
        requestData.put("TotalFee", "1");
        requestData.put("AuthCode", "1300001142368816706");
        //requestData.put("AuthCode", "http://www.intella.co");


        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("7000", header.get("StatusCode").getAsString());

        requestData.put("Method", "101");
        requestData.put("ServiceType", SystemInstance.TYPE_MICROPAY);
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170313080000");    // format must be yyyyMMddHHmmss
        // setting data
        //requestData.put("StoreOrderNo", "super100");
        requestData.put("DeviceInfo", "skb0001");
        requestData.put("Body", "Item name");
        requestData.put("TotalFee", "1");
        requestData.put("AuthCode", "1300001142368816706");

        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();

         decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
         json = new Gson().fromJson(decryptResponse, JsonObject.class);
         header = json.getAsJsonObject("Header");

        Assert.assertEquals("8002", header.get("StatusCode").getAsString());


        requestData.put("Method", "10110");
        requestData.put("ServiceType", SystemInstance.TYPE_MICROPAY);
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170313080000");    // format must be yyyyMMddHHmmss
        // setting data
        //requestData.put("StoreOrderNo", "super100");
        requestData.put("DeviceInfo", "skb0001");
        requestData.put("Body", "Item name");
        requestData.put("TotalFee", "1");
        requestData.put("AuthCode", "1300001142368816706");

        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();

        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("8002", header.get("StatusCode").getAsString());
    }

    @Test
    public void testWeixinJsapi() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10220");
        requestData.put("ServiceType", "OLPay");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170517080000");    // format must be yyyyMMddHHmmss
        // setting data
        //requestData.put("TimeExpire", "20170427230000");
        requestData.put("DeviceInfo", "skb0001");
        requestData.put("StoreOrderNo", "aliOLpay0006");
        requestData.put("Body", "Item name");
        //requestData.put("FeeType", "TWD");
        requestData.put("TotalFee", "1");
        //requestData.put("AuthCode", "http://www.intella.co");


        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("7000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testCreateQrcode() throws Exception {
        String no;
        for (int i=1;i<=50;i++) {
            if (i < 10) no = "int0" + String.valueOf(i);
            else no = "int" + String.valueOf(i);
            Map<String, String> requestData = new HashMap<String, String>();
            // setting header
            requestData.put("Method", "10110");
            requestData.put("ServiceType", "OLPay");
            requestData.put("MchId", no);
            requestData.put("CreateTime", "20170816080000");    // format must be yyyyMMddHHmmss
            // setting data
            //requestData.put("TimeExpire", "20170427230000");
            requestData.put("DeviceInfo", "skb0001");
            requestData.put("StoreOrderNo", "int");
            requestData.put("Body", "Test item");
            //requestData.put("FeeType", "TWD");
            //requestData.put("TotalFee", "1");
            requestData.put("Permanent", "Y");

            //requestData.put("AuthCode", "http://www.intella.co");


            SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
            IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
            ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
            String result = responseEntity.getBody();

        }

//            String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
//            JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
//            JsonObject header = json.getAsJsonObject("Header");
//
//            Assert.assertEquals("7000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testWeixinCurrency() throws Exception {

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10110");
        requestData.put("ServiceType", "Currency");
        requestData.put("MchId", "super");
        //requestData.put("MchId", "103543903790001");
        requestData.put("CreateTime", "20170313080000");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("Currency", "20161130120000");
        requestData.put("DeviceInfo", "device0001");
        requestData.put("Body", "Item name");
        requestData.put("TotalFee", "100");
        requestData.put("AuthCode", "1300001142368816706");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testWeixixRefund() throws Exception{
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10110");
        requestData.put("ServiceType", "Refund");
        requestData.put("MchId", "super");
        //requestData.put("MchId", "103543903790001");
        requestData.put("CreateTime", "20170313080000");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("StoreRefundNo", "BISNT507008D0000000089");
        requestData.put("StoreOrderNo", "BISNT507008D0000000089");
        requestData.put("SysOrderNo", "BISNT507008D0000000089");
        requestData.put("DeviceInfo", "skb0001");
        requestData.put("TotalFee", "1");
        requestData.put("RefundFeeType", "TWD");
        requestData.put("RefundFee", "1");
        requestData.put("RefundedMsg", "product suck");


        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testWeixixSingleQuery() throws Exception{

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10110");
        requestData.put("ServiceType", "SingleOrderQuery");
        requestData.put("MchId", "super");
        //requestData.put("MchId", "103543903790001");
        requestData.put("CreateTime", "20170313080000");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("StoreOrderNo", "0wwof5zmj30000000130");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();
//        ClassLoader classLoader = getClass().getClassLoader();
//        File file = new File(classLoader.getResource("key/pub.der").getFile());
//        PublicKey publicKey = null;
//        if(publicKey == null) {
//            publicKey =  KeyReader.loadPublicKeyFromDER(file);
//        }
//        String result = HttpRequestUtil.httpsPost("https://www.intellapay.org/allpaypass/api/general",requestData, publicKey, secretKey);

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);

        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());


        requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10110");
        requestData.put("ServiceType", "SingleOrderQuery");
        requestData.put("MchId", "super");
        //requestData.put("MchId", "103543903790001");
        requestData.put("CreateTime", "20170313080000");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("StoreOrderNo", "BISNT507008D0000000121");
        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();
//        ClassLoader classLoader = getClass().getClassLoader();
//        File file = new File(classLoader.getResource("key/pub.der").getFile());
//        PublicKey publicKey = null;
//        if(publicKey == null) {
//            publicKey =  KeyReader.loadPublicKeyFromDER(file);
//        }
//        String result = HttpRequestUtil.httpsPost("https://www.intellapay.org/allpaypass/api/general",requestData, publicKey, secretKey);
        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");
        Assert.assertEquals("8002", header.get("StatusCode").getAsString());



        requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "1011");
        requestData.put("ServiceType", "SingleOrderQuery");
        requestData.put("MchId", "super");
        //requestData.put("MchId", "103543903790001");
        requestData.put("CreateTime", "20170313080000");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("StoreOrderNo", "BISNT507008D0000000121");
        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();
//        ClassLoader classLoader = getClass().getClassLoader();
//        File file = new File(classLoader.getResource("key/pub.der").getFile());
//        PublicKey publicKey = null;
//        if(publicKey == null) {
//            publicKey =  KeyReader.loadPublicKeyFromDER(file);
//        }
//        String result = HttpRequestUtil.httpsPost("https://www.intellapay.org/allpaypass/api/general",requestData, publicKey, secretKey);
        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("8002", header.get("StatusCode").getAsString());


        requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10110");
        requestData.put("ServiceType", "SingleMan");
        requestData.put("MchId", "super");
        //requestData.put("MchId", "103543903790001");
        requestData.put("CreateTime", "20170313080000");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("StoreOrderNo", "BISNT507008D0000000121");
        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();
//        ClassLoader classLoader = getClass().getClassLoader();
//        File file = new File(classLoader.getResource("key/pub.der").getFile());
//        PublicKey publicKey = null;
//        if(publicKey == null) {
//            publicKey =  KeyReader.loadPublicKeyFromDER(file);
//        }
//        String result = HttpRequestUtil.httpsPost("https://www.intellapay.org/allpaypass/api/general",requestData, publicKey, secretKey);
        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("8002", header.get("StatusCode").getAsString());


        requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10110");
        requestData.put("ServiceType", "SingleOrderQuery");
        requestData.put("MchId", "MWD");
        //requestData.put("MchId", "103543903790001");
        requestData.put("CreateTime", "20170313080000");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("StoreOrderNo", "BISNT507008D0000000121");
        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();
//        ClassLoader classLoader = getClass().getClassLoader();
//        File file = new File(classLoader.getResource("key/pub.der").getFile());
//        PublicKey publicKey = null;
//        if(publicKey == null) {
//            publicKey =  KeyReader.loadPublicKeyFromDER(file);
//        }
//        String result = HttpRequestUtil.httpsPost("https://www.intellapay.org/allpaypass/api/general",requestData, publicKey, secretKey);
        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("8002", header.get("StatusCode").getAsString());



        requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10110");
        requestData.put("ServiceType", "SingleOrderQuery");
        requestData.put("MchId", "super");
        //requestData.put("MchId", "103543903790001");
        requestData.put("CreateTime", "201703130808089999");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("StoreOrderNo", "BISNT507008D0000000121");
        secretKey = AesCryptoUtil.generateSecreteKey();
        encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        result = responseEntity.getBody();
//        ClassLoader classLoader = getClass().getClassLoader();
//        File file = new File(classLoader.getResource("key/pub.der").getFile());
//        PublicKey publicKey = null;
//        if(publicKey == null) {
//            publicKey =  KeyReader.loadPublicKeyFromDER(file);
//        }
//        String result = HttpRequestUtil.httpsPost("https://www.intellapay.org/allpaypass/api/general",requestData, publicKey, secretKey);
        decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        json = new Gson().fromJson(decryptResponse, JsonObject.class);
        header = json.getAsJsonObject("Header");

        Assert.assertEquals("8002", header.get("StatusCode").getAsString());
    }


    @Test
    public void testAllpayMicropay() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10500");
        requestData.put("ServiceType", SystemInstance.TYPE_MICROPAY);
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170513080000");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("StoreOrderNo", "asdasddaallpay");
        requestData.put("DeviceInfo", "skb0001");
        requestData.put("Body", "Item name");
        requestData.put("TotalFee", "1");
        requestData.put("AuthCode", "AP171A0CGIUD6IPW4A");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");
    }

    @Test
    public void testAllpaySinglequery() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10500");
        requestData.put("ServiceType", "SingleOrderQuery");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170513080000");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("StoreOrderNo", "co4txk9dtm0000000001");
        //requestData.put("DeviceInfo", "skb0001");
        //requestData.put("Body", "Item name");
        //requestData.put("TotalFee", "1");
        //requestData.put("AuthCode", "AP163A02SSXRHLI15V");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");
    }

    @Test
    public void testAllpayRefund() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10500");
        requestData.put("ServiceType", "Refund");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170513080000");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("StoreOrderNo", "OL220170713152953");
        requestData.put("StoreRefundNo", "ROL220170713152953");
        requestData.put("DeviceInfo", "skb0001");
        requestData.put("Body", "Item name");

        //requestData.put("AuthCode", "AP160A0AULNZCT3DBN");
        requestData.put("RefundFee", "1");
        requestData.put("RefundedMsg", "product suck");


        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");
    }
    @Test
    public void testAllpayCreateOrder() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10500");
        requestData.put("ServiceType", "CreateOrder");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170513080000");    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("StoreOrderNo", "allpa0f4g3y002");
        //requestData.put("DeviceInfo", "skb0001");
        requestData.put("Body", "Item name");
        requestData.put("TotalFee", "1");
        //requestData.put("AuthCode", "AP181A0DRJPWF6RZF3");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");
    }


}
