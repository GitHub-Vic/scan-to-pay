package co.intella.controller;

import co.intella.crypto.AesCryptoUtil;
import co.intella.domain.weixin.WeixinCurrencyRequestData;
import co.intella.domain.weixin.WeixinMicropayRequestData;
import co.intella.model.IntegratedRequest;
import co.intella.net.Constant;
import co.intella.utility.Cryption;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.SecretKey;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Miles
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WeixinControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testRatequery() {
        WeixinCurrencyRequestData requestData = new WeixinCurrencyRequestData();
        requestData.setVer("101");
        requestData.setMethod("10");
        requestData.setCurrency("TWD");
        requestData.setMchId("103543903790001");
        requestData.setNonceStr("nonce_str");
        requestData.setExchangeCurrency("USD");
        requestData.setTradeType("EXCHANGECURRENCY");

        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://y.intella.co/allpaypass/api/weixin/ratequery", requestData,String.class);

        responseEntity.getBody();
    }

    @Test
    public void testMicropay() {

        WeixinMicropayRequestData requestData = new WeixinMicropayRequestData();
        requestData.setVer("101");
        requestData.setNonceStr("111123123");
        requestData.setMethod("10");
        requestData.setFeeType("TWD");
        requestData.setMchId("103543903790001");
        requestData.setBody("chicken");
        requestData.setAuthCode("130050957315345656");
        requestData.setOutTradeNo("20161130165117");
        requestData.setDeviceInfo("allpaydev0001");
        requestData.setGoodsTag("food");
        requestData.setTotalFee("123");
        requestData.setTradeType("MICROPAY");

        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/weixin/micropay", requestData,String.class);

        responseEntity.getBody();
    }

    @Test
    public void testRefund() throws Exception {

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "10110");
        requestData.put("ServiceType", "Refund");
        //requestData.put("MchId", "04230623201");
        requestData.put("MchId", "TPCK01");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss
        // setting data
        requestData.put("StoreRefundNo", "R69TP01171220094318");//UUID.randomUUID().toString()
        requestData.put("SysOrderNo", "");
        requestData.put("StoreOrderNo", "69TP01171220094318");

        requestData.put("DeviceInfo", "skb0001");
        requestData.put("TotalFee", "1");
        requestData.put("RefundFeeType", "TWD");
        requestData.put("RefundFee", "1");
        requestData.put("RefundedMsg", "product");
        requestData.put("RefundKey", "0000");



        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://a.intella.co/allpaypass/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

}
