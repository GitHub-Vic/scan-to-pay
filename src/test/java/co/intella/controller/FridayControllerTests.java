package co.intella.controller;

import co.intella.crypto.AesCryptoUtil;
import co.intella.crypto.KeyReader;
import co.intella.crypto.RsaCryptoUtil;
import co.intella.domain.friDay.FridayNotifyRequestData;
import co.intella.model.IntegratedRequest;
import co.intella.net.Constant;
import co.intella.utility.Cryption;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.SecretKey;
import java.io.File;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Alex
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FridayControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testCreateOrder() throws Exception {
        DateTime now = DateTime.now();

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("token", "FoUVLZMYSll");


        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("key/friDayTointella-Stage-public.der").getFile());
        PublicKey publicKey = KeyReader.loadPublicKeyFromDER(file);

        byte[] bbb = RsaCryptoUtil.encrypt(new Gson().toJson(requestData),publicKey);

        String encryptRequest = Base64.encode(bbb);
//https://s.intella.co/allpaypass
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/friDay/createOrder", encryptRequest, String.class);
        String result = responseEntity.getBody();


    }

    @Test
    public void testNotify() throws Exception {


        String encryptRequest = "Dta/ai97x2/sPbQzV1Nv1PYKsIbKykcUX82qbZU4BEuWv5Ie+yN/bNpQGnukP3l7nVqsBrDk2mW6O2hojJxCtkZS05X6timeIaFUOozwaOu56tQ1kgUuooMSQW0iu3mZtHSKfFQBZtl3Gs//Gr8TRUglcsiCGnX9fkuo0vWmn8YmH/7A/0XXFrpKO7wI+iWkZzak9RJLcVpqvbShrz1nzMGjLrYmuXK0JI7tLrxAbdqZ/qbh9TQSfx5TDdzJjdiWi6q0UG+5q+CLQ+86M1HxELVuOIzSGUeJjpY4ak59Gp+TxUZWuCHRMosR9qP9USyxZW9pVgpbxhXAY4jHtN30hA==";
//
//        String a = "{\"orderId\":\"4BF0018071000016\",\"txOrderNo\":\"LAC000000000174\",\"orderDateTime\":\"2018/07/10 11:13:03\",\"orderStatus\":\"S\",\"totalAmt\":\"10\",\"payments\":[{\"paymentMethod\":\"CC\",\"amount\":\"10\"}]}";
//
//        FridayNotifyRequestData data = new FridayNotifyRequestData();
//        data.setOrderDateTime("2018/07/10 11:13:03");
//        data.setOrderId("4BF0018071000016");
//        data.setOrderStatus("S");
//        data.setTotalAmt("10");
//        data.setTxOrderNo("LAC000000000174");
//
//
//        ClassLoader classLoader = getClass().getClassLoader();
//        File file = new File(classLoader.getResource("key/friDayTointella-Stage-public.der").getFile());
//        PublicKey publicKey = KeyReader.loadPublicKeyFromDER(file);

//        byte[] bbb = RsaCryptoUtil.encrypt(new Gson().toJson(data),publicKey);

//        String encryptRequest = Base64.encode(bbb);



        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/friDay/notify", encryptRequest, String.class);
        String result = responseEntity.getBody();


    }

    @Test
    public void testRefund() throws Exception {

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "00000");
        requestData.put("ServiceType", "Refund");
        //requestData.put("MchId", "04230623201");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss
        requestData.put("TradeKey","9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");

        // setting data
        requestData.put("StoreRefundNo", "R4BF0018071300057");//UUID.randomUUID().toString()
        requestData.put("SysOrderNo", "");
        requestData.put("StoreOrderNo", "4BF0018071300057");

        requestData.put("DeviceInfo", "skb0001");
        requestData.put("TotalFee", "1");
        requestData.put("RefundFeeType", "TWD");
        requestData.put("RefundFee", "1");
        requestData.put("RefundedMsg", "product");
        requestData.put("RefundKey", "9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");



        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
//        https://s.intella.co/allpaypass
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

}
