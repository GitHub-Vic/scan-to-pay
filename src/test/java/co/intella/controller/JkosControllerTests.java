package co.intella.controller;

import co.intella.crypto.AesCryptoUtil;
import co.intella.model.IntegratedRequest;
import co.intella.net.Constant;
import co.intella.utility.Cryption;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.SecretKey;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Alex
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class JkosControllerTests {
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testJkosMicropay() throws Exception {
        DateTime now = DateTime.now();

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "00000");
        requestData.put("ServiceType", SystemInstance.TYPE_MICROPAY);
        requestData.put("MchId", "super");
        requestData.put("CreateTime", now.toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss
        requestData.put("TradeKey","9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");

        requestData.put("StoreOrderNo", "JK" + now.toString("yyMMddhhmmss"));
        requestData.put("DeviceInfo", "skb0001");
        requestData.put("StoreInfo", "0001");
        requestData.put("Body", "Item name");
        requestData.put("TotalFee", "1");
        requestData.put("AuthCode", "22J111111111111111");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testRefund() throws Exception{
        DateTime now = DateTime.now();

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "00000");
        requestData.put("ServiceType", "Refund");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", now.toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss
        requestData.put("TradeKey","9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");

        // setting data
        requestData.put("StoreRefundNo", "R0te15tfjjz0000000005");
        requestData.put("StoreOrderNo", "0te15tfjjz0000000005");
        requestData.put("DeviceInfo", "skb0001");
        requestData.put("TotalFee", "1");
        requestData.put("RefundFeeType", "TWD");
        requestData.put("RefundFee", "1");
        requestData.put("RefundedMsg", "product suck");
        requestData.put("RefundKey", "9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");



        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://s.intella.co/allpaypass/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }
}
