package co.intella.controller;

import co.intella.crypto.AesCryptoUtil;
import co.intella.domain.ecpay.ECPayDoAction;
import co.intella.domain.ecpay.ECPayDoActionResponse;
import co.intella.model.IntegratedRequest;
import co.intella.net.Constant;
import co.intella.service.ECPayService;
import co.intella.utility.Cryption;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.SecretKey;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author Andy Lin
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ECPayControllerTests {
    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ECPayService ecPayService;

    @Test
    public void testECCancel() throws Exception {
        ECPayDoAction ecPayDoAction = new ECPayDoAction();
        ecPayDoAction.setMerchantId("3027720");
        ecPayDoAction.setMerchantTradeNo("OL220170807171654");
        ecPayDoAction.setTradeNo("1708071716555953");
        ecPayDoAction.setTotalAmount("1");
        ecPayDoAction.setAction("R");
        ecPayDoAction.setPlatformID("");

        ECPayDoActionResponse ecPayDoActionResponse = ecPayService.doAction(ecPayDoAction);

        String sss = ecPayDoActionResponse.getRtnMsg();
        byte[] binary = sss.getBytes("x-windows-950");
        String ans = new String(binary, "BIG5");

        int a =1;
    }

    @Test
    public void testECpayCancel() throws Exception {

        Map<String, String> requestData = new HashMap<String, String>();
        requestData.put("Method", "21100");
        requestData.put("ServiceType", "Cancel");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", getNowTime());    // format must be yyyyMMddHHmmss
        requestData.put("StoreOrderNo", "OL220170815213104");
        requestData.put("SysOrderNo", "1708152131046787");
        requestData.put("TotalFee", "1");
        requestData.put("PlatformId", "下午");
        //paymentType, checkMacValue, returnURL, encryptType

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("7138", header.get("StatusCode").getAsString());
        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testECpayQuery() throws Exception {
        Random rand = new Random();

        Map<String, String> requestData = new HashMap<String, String>();
        requestData.put("Method", "21100");
        requestData.put("ServiceType", "SingleQuery");
        requestData.put("MchId", "2000132");
        requestData.put("CreateTime", getNowTime());    // format must be yyyyMMddHHmmss

        requestData.put("StoreOrderNo", "ECTest8698");
        requestData.put("StoreInfo", "relative001");
        requestData.put("Body", "ItemName");
        requestData.put("TotalFee", "30");
        requestData.put("TransMode", "0");
        requestData.put("Detail", "TradeDesc");
        //paymentType, checkMacValue, returnURL, encryptType

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testECpayCapture() throws Exception {
        Random rand = new Random();

        Map<String, String> requestData = new HashMap<String, String>();
        requestData.put("Method", "21100");
        requestData.put("ServiceType", "Capture");
        requestData.put("MchId", "2000132");
        requestData.put("CreateTime", getNowTime());    // format must be yyyyMMddHHmmss
// + Integer.toString(rand.nextInt(9999))
        requestData.put("StoreOrderNo", "ECTest2809");
        requestData.put("TotalFee", "30");
        requestData.put("TransMode", "0");
        requestData.put("Detail", "Remark");
        //paymentType, checkMacValue, returnURL, encryptType

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testECpayPayment() throws Exception {
        Random rand = new Random();

        Map<String, String> requestData = new HashMap<String, String>();
        requestData.put("Method", "21100");
        requestData.put("ServiceType", "Payment");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", getNowTime());    // format must be yyyyMMddHHmmss

        requestData.put("StoreOrderNo", "ECTest" + Integer.toString(rand.nextInt(9999)));
        requestData.put("StoreInfo", "relative001");
        requestData.put("Body", "ItemName");
        requestData.put("TotalFee", "30");
        requestData.put("TransMode", "0");
        requestData.put("Detail", "TradeDesc");
        requestData.put("DeviceInfo", "2000132999");
        //paymentType, checkMacValue, returnURL, encryptType

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    private String getNowTime() {
        SimpleDateFormat sdf1 = new SimpleDateFormat(SystemInstance.DATE_PATTERN);
        String str = "";
        str = sdf1.format(new Date());
        return str;
    }
}

