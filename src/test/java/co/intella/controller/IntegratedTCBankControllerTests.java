package co.intella.controller;

import co.intella.crypto.AesCryptoUtil;
import co.intella.model.IntegratedRequest;
import co.intella.net.Constant;
import co.intella.utility.Cryption;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.SecretKey;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Andy Lin
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegratedTCBankControllerTests {


    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testTcbPayment() throws Exception {

        //{"Data":"{",\StoreOrderNo\":\"20170603095632abc\"}","Header":{"MchId":"lafresh","Method":"20400","ServiceType":"Payment"}}

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("MchId", "super");
//        requestData.put("MchId", "0100829354");
        requestData.put("Method", "20400");
        requestData.put("ServiceType", "Payment");
        requestData.put("StoreOrderNo", "test" + generateSerialNumber());
        requestData.put("CreateTime", "20170330120000");
        requestData.put("TimeExpire", "20170330123456789");
        requestData.put("DeviceInfo", "T0000000");
        requestData.put("Body", "");
        requestData.put("TotalFee", "1");
        requestData.put("FeeType", "TWD");
        requestData.put("Detail", "trade detail");
        requestData.put("StoreInfo", "intella-taipei");
        requestData.put("Cashier", "miles");
        requestData.put("BarcodeMode", "barcodeMode");
        requestData.put("Extended", "");
        requestData.put("CardId", "3566520000000031");
        requestData.put("ExtenNo", "457");
        requestData.put("ExpireDate", "2010");
        requestData.put("TransMode", "0");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://s.intella.co/allpaypass/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());

        String serialNumber = json.get("Data").getAsJsonObject().get("serialNumber").getAsString();
        String token = json.get("Data").getAsJsonObject().get("token").getAsString();

        Map<String, String> requestData2 = new HashMap<String, String>();
        // setting header
        requestData2.put("MchId", "super");
        requestData2.put("Method", "20400");
        requestData2.put("ServiceType", "Payment");
        requestData2.put("StoreOrderNo", "test" + generateSerialNumber());
        requestData2.put("CreateTime", "20170330120000");
        requestData2.put("TimeExpire", "20170330123456789");
        requestData2.put("DeviceInfo", "T0000000");
        requestData2.put("Body", "body?test");
        requestData2.put("TotalFee", "1");
        requestData2.put("FeeType", "TWD");
        requestData2.put("Detail", "trade detail");
        requestData2.put("StoreInfo", "intella-taipei");
        requestData2.put("Cashier", "miles");
        requestData2.put("BarcodeMode", "barcodeMode");
        requestData2.put("Extended", "miles1=bowni&mile2=asd");
        requestData2.put("ExtenNo", "226");
        requestData2.put("ExpireDate", "2101");
        requestData2.put("TransMode", "0");
        requestData2.put("NotifyURL", "");
        requestData2.put("Installment", "");
        requestData2.put("Token", token);
        requestData2.put("TokenSeq", serialNumber);

        SecretKey secretKey2 = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest2 = new Cryption().getEncryptRequest(requestData2, secretKey2);
        ResponseEntity<String> responseEntity2 = restTemplate.postForEntity("https://s.intella.co/allpaypass/api/general", encryptRequest2, String.class);
        String result2 = responseEntity2.getBody();

        String decryptResponse2 = AesCryptoUtil.decryptResponse(secretKey2, Constant.IV, result2);
        JsonObject json2 = new Gson().fromJson(decryptResponse2, JsonObject.class);
        JsonObject header2 = json2.getAsJsonObject("Header");

        Assert.assertEquals("0000", header2.get("StatusCode").getAsString());
    }

    @Test
    public void testTcbCancel() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("MchId", "super");
        requestData.put("Method", "20400");
        requestData.put("ServiceType", "Cancel");

        requestData.put("StoreOrderNo", "17062900006");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://s.intella.co/allpaypass/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testTcbSingleOrderQuery() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("MchId", "lafresh");
        requestData.put("Method", "20400");
        requestData.put("ServiceType", "SingleOrderQuery");
        requestData.put("StoreOrderNo", "17062900006");


        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testTcbOrderQuery() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("MchId", "lafresh");
        requestData.put("Method", "20400");
        requestData.put("ServiceType", "OrderQuery");
        requestData.put("StoreOrderNo", "13003210301230031207");
        requestData.put("DeviceInfo", "A6113002");
        requestData.put("Body", "body?");
        requestData.put("TimeExpire", "20170330123456789");
        requestData.put("FeeType", "TWD");
        requestData.put("Detail", "trade detail");
        requestData.put("StoreInfo", "intella-taipei");
        requestData.put("Cashier", "miles");
        requestData.put("CardId", "1234123412341234");
        requestData.put("ExtenNo", "226");
        requestData.put("ExpireDate", "2101");
        requestData.put("TransMode", "0");
        requestData.put("NotifyURL", "");
        requestData.put("Installment", "");


        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    @Test
    public void testTcbCapture() throws Exception {
        Map<String, String> requestData = new HashMap<String, String>();
        requestData.put("MchId", "0100829354");
        requestData.put("Method", "20400");
        requestData.put("ServiceType", "Capture");
        requestData.put("CaptureTime", "20170531185959");
        requestData.put("CreateTime", getNowTime());
        requestData.put("SerialNo", generateSerialNumber());
        requestData.put("TotalCount", "150");
        requestData.put("NumberSign", "+");
        requestData.put("TotalFee", "350080");

        requestData.put("DeviceInfo", "machine001");
        requestData.put("StoreOrderNo", "test001");
        requestData.put("TotalFee", "350080");
        requestData.put("AuthCode", "machine001");
        requestData.put("TransCode", "02");
        requestData.put("TradeDate", "19950810");
        requestData.put("Extended", "extended column");
        requestData.put("CardOwnerInfo", "Alex Wu");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    private String getNowTime() {
        SimpleDateFormat sdf1 = new SimpleDateFormat(SystemInstance.DATE_PATTERN);
        String str = "";
        str = sdf1.format(new Date());
        return str;
    }

    private String generateSerialNumber() {

        char seeds[] = {'0','1', '2', '3', '4', '5', '6', '7', '8', '9'};
        char randStr[] = new char[10];
        for (int i=0;i< randStr.length;i++) {
            randStr[i] = seeds[(int)Math.round(Math.random() * (seeds.length - 1))];
        }

        return new String(randStr);
    }

}
