package co.intella.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Miles
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PaymentAccountControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testSave() {

        Map<String, String> requestData = new HashMap<String, String>();

        requestData.put("merchantId", "super");
        requestData.put("bankId", "4");
        requestData.put("account", "test");

        String update = "testUpdate" + generateSerialNumber();
        requestData.put("description", update);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/paymentaccount/save", requestData, String.class);

        JsonObject result = new Gson().fromJson(responseEntity.getBody(), JsonObject.class);
        JsonObject entityResult = new Gson().fromJson(result.get("message").getAsString(), JsonObject.class);
        Assert.assertEquals(update, entityResult.get("description").getAsString());

    }

    @Test
    public void testSave2() {
        Map<String, String> requestData = new HashMap<String, String>();

        requestData.put("merchantId", "miles1");
        requestData.put("bankId", "1");
        requestData.put("account", "account2bank1");

        String update = "testUpdate" + generateSerialNumber();
        requestData.put("description", update);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/paymentaccount/save", requestData, String.class);

        JsonObject result = new Gson().fromJson(responseEntity.getBody(), JsonObject.class);
        JsonObject entityResult = new Gson().fromJson(result.get("message").getAsString(), JsonObject.class);
        Assert.assertEquals(update, entityResult.get("description").getAsString());
    }

    @Test
    public void testGetOne() {
        ResponseEntity<String> responseEntity = restTemplate.getForEntity("/api/paymentaccount/get/super/1", String.class);
        JsonObject result = new Gson().fromJson(responseEntity.getBody(), JsonObject.class);
        JsonObject entityResult = new Gson().fromJson(result.get("message").getAsString(), JsonObject.class);
        Assert.assertEquals("103543903790001", entityResult.get("account").getAsString());
    }

    private String generateSerialNumber() {

        char seeds[] = {'0','1', '2', '3', '4', '5', '6', '7', '8', '9'};
        char randStr[] = new char[10];
        for (int i=0;i< randStr.length;i++) {
            randStr[i] = seeds[(int)Math.round(Math.random() * (seeds.length - 1))];
        }

        return new String(randStr);
    }

}
