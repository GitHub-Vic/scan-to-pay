package co.intella.controller;

import co.intella.crypto.AesCryptoUtil;
import co.intella.crypto.KeyReader;
import co.intella.model.IntegratedRequest;
import co.intella.net.Constant;
import co.intella.net.HttpRequestUtil;
import co.intella.utility.Cryption;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.SecretKey;
import java.io.File;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Alex
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EzcControllerTests {
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testSettlementPatch() throws Exception {
        DateTime now = DateTime.now();

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "31800");
        requestData.put("ServiceType", "SettlementPatch");
        requestData.put("MchId", "epoint05");
        requestData.put("CreateTime", now.toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss
        requestData.put("TradeKey", "9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");

        requestData.put("DeviceId", "01304110");
        requestData.put("BatchNo", "18062102");


        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = new Cryption().getEncryptRequest(requestData, secretKey);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://a.intella.co/allpaypass/api/general", encryptRequest, String.class);
        String result = responseEntity.getBody();
        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);

        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
        JsonObject header = json.getAsJsonObject("Header");

        Assert.assertEquals("0000", header.get("StatusCode").getAsString());
    }

    public IntegratedRequest getEncryptRequest(Map<String, String> requestData, SecretKey secretKey) throws Exception {

        String keyPath =  "pub.der";

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("key/" + keyPath).getFile());
        PublicKey publicKey = KeyReader.loadPublicKeyFromDER(file);

        String requestString = HttpRequestUtil.getEncryptRequestJson(requestData, publicKey, secretKey);
        return new Gson().fromJson(requestString, IntegratedRequest.class);
    }
}
