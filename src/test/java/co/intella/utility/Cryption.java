package co.intella.utility;

import co.intella.crypto.KeyReader;
import co.intella.model.IntegratedRequest;
import co.intella.net.HttpRequestUtil;
import com.google.gson.Gson;

import javax.crypto.SecretKey;

import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.security.PublicKey;
import java.util.Map;

/**
 * @author Andy Lin
 */
public class Cryption {
	
	@Value("${execution.mode}")
    private String EXEC_MODE;

    public IntegratedRequest getEncryptRequest(Map<String, String> requestData, SecretKey secretKey) throws Exception {
        
    	ClassLoader classLoader = getClass().getClassLoader();
        // String keyPath = !EXEC_MODE.equals("pord") ? "key/stage-pub.der" : "key/pub.der";

    	File file = new File(classLoader.getResource("key/pub.der").getFile());
        PublicKey publicKey = KeyReader.loadPublicKeyFromDER(file);

        String requestString = HttpRequestUtil.getEncryptRequestJson(requestData, publicKey, secretKey);
        return new Gson().fromJson(requestString, IntegratedRequest.class);
    }
}
