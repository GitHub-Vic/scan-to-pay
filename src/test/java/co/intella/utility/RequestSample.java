package co.intella.utility;

import co.intella.crypto.AesCryptoUtil;
import co.intella.crypto.KeyReader;
import co.intella.net.HttpRequestUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.SecretKey;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Miles
 */
@RunWith(SpringRunner.class)
public class RequestSample {

    @Test
    public void onlinePayRequest() throws Exception {

        Map<String, String> requestMap = new HashMap<String, String>();

        // header
        requestMap.put("Method", "00000");
        requestMap.put("ServiceType", "OLPay");
        requestMap.put("MchId", "");    // merchant account
        requestMap.put("TradeKey", "9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0"); //SHA256 hash
        requestMap.put("CreateTime", "20180305120000");   // format yyyyMMddHHmmss

        //data
        requestMap.put("DeviceInfo", "skb0001");
        requestMap.put("StoreOrderNo", "20180305AA0000000001");
        requestMap.put("Body", "Test");
        requestMap.put("TotalFee", "0");    // use string instead.

        PublicKey publicKey = KeyReader.loadPublicKeyFromDER("D:\\key\\scan2pay-stage-key\\stage-pub.der");
        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        String result = HttpRequestUtil.doRequest("https://s.intella.co/allpaypass/api/general", requestMap, secretKey, publicKey);

        System.out.print(result);
    }

    @Test
    public void httpPostTest() throws Exception {
        //https://ezc.intella.co/ezc/icer2/ICERService.py?data={"ServiceType":"SignON","TerminalID":"08300000","DeviceID":"01304097","TerminalTXNNumber":"093252","HostSerialNumber":"093252","Time":"093252","Date":"20180320","AESKey":"7395bd9417ca40b0fe34b01482c1bc6a"}

        String url = "http://35.187.147.210/ezc/ICERService.py?data={\"ServiceType\":\"SignON\",\"TerminalID\":\"08300000\",\"DeviceID\":\"01304097\",\"TerminalTXNNumber\":\"093252\",\"HostSerialNumber\":\"093252\",\"Time\":\"093252\",\"Date\":\"20180320\",\"AESKey\":\"7395bd9417ca40b0fe34b01482c1bc6a\"}";
        String result = HttpRequestUtil.httpGet(url);
        System.out.print(result);
    }

}
