package co.intella.service;

import static co.intella.crypto.RsaCryptoUtil.decrypt;
import static co.intella.service.TripleDESUtil.convertTripleDESSecretKey;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.Resource;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fet.mobilewallet.utils.CipherUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import co.intella.crypto.AesCryptoUtil;
import co.intella.crypto.KeyReader;
import co.intella.crypto.RsaCryptoUtil;
import co.intella.domain.friDay.FridayBasic;
import co.intella.domain.pi.PiOLRefundResponseData;
import co.intella.model.IntegratedRequestBody;
import co.intella.model.PaymentAccount;
import co.intella.model.RefundDetail;
import co.intella.model.RequestHeader;
import co.intella.model.TestUUID;
import co.intella.model.TradeDetail;
import co.intella.net.Constant;
import co.intella.service.CRM.TicketHistoryService;
import co.intella.service.CRM.TicketOperatorService;
import co.intella.service.CRM.TicketService;
import co.intella.service.CRM.WorkflowService;
import co.intella.utility.GeneralUtil;
import co.intella.utility.SystemInstance;

/**
 * @author Alex
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class GeneralTests {

//    @Autowired
//    private CacheService cacheService;

    @Autowired
    private WorkflowService workflowService;

    @Autowired
    private SaleService saleService;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private TicketHistoryService ticketHistoryService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private TicketOperatorService ticketOperatorService;

    @Autowired
    private TradeDetailService tradeDetailService;
    @Autowired
    private Environment env;

    @Resource
    private CaptureOrderDetailService captureOrderetailService;

    @Resource
    private CaptureDetailService captureDetailService;

    private SessionFactory sessionFactory;

    @Resource
    private BankService bankService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private TestService testService;

    @Resource
    private EzCardService ezCardService;

    @Resource
    private RefundDetailService refundDetailService;

    @Resource
    private Pay2GoService pay2GoService;

    private static String decrypt4(SecretKey secretKey, byte[] iv, byte[] encryptText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
        cipher.init(2, secretKey, new IvParameterSpec(iv));
        byte[] decryptedText = cipher.doFinal(encryptText);
        return new String(decryptedText);
    }

    public static String decryptByPrivateKey(byte[] encryptedData, PrivateKey privateKey)
            throws Exception {
        int MAX_DECRYPT_BLOCK = 128;

        Cipher cipher = Cipher.getInstance(privateKey.getAlgorithm());
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        int inputLen = encryptedData.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] cache;
        int i = 0;
        // 对数据分段解密
        while (inputLen - offSet > 0) {
            if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
                cache = cipher.doFinal(encryptedData, offSet, MAX_DECRYPT_BLOCK);
            } else {
                cache = cipher.doFinal(encryptedData, offSet, inputLen - offSet);
            }
            out.write(cache, 0, cache.length);
            i++;
            offSet = i * MAX_DECRYPT_BLOCK;
        }
        byte[] decryptedData = out.toByteArray();
        out.close();
        return new String(decryptedData);
    }

    @Resource
    private TwPayService twPayService;

    @Resource
    private FridayService fridayService;


    @Test
    public void fsfsf() throws Exception{

        FridayBasic ba= new FridayBasic();
        fridayService.refund(ba);



        String publick = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCtsS/Do6PMqy3pVdv45cxpbQEQbfjPI6ClRWCFVuaFVnVBgU3fCrNKRM9Xpsq5LtccTE4P/fLUdKiifn1sd//kpGe7ITA40tBpPrc03iLu6M6Cr9TthYhpLwMxTARqZ/Whv3NV8iJjiUf7/3pXnLk7bG7A0nZx+f3ceAFT91svLwIDAQAB";
        byte[] b33 = new BigInteger("5E8B6E1998F421204C6576544FE1A26B44FC775982D8CE2E",16).toByteArray();

        PublicKey publicKey4= KeyReader.loadPublicKeyFromString(publick);


        String ency3 = "DQ8BiPA3pve//lwo3cZKnZ4R+xqZF3ske2FRDOrAQfSTaroCBo0vr8refjfkjiluKGFYPPbqkRW3IRu4kIrlkX6EaUGxBDGFPgF3K48aqvij/lSs//JKuV6WwmksG5h3f8ymTzVjNJNE5cS4SrCMGNZB7boHeauUJK4MwiylMls=";

        String fu8fu = CipherUtil.decrypt(ency3,"RSA_PUBLIC",publick);

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("key/friDayTointella-Stage-public.der").getFile());
        PublicKey publicKey = KeyReader.loadPublicKeyFromDER(file);

        byte[] bbb = RsaCryptoUtil.encrypt("ss",publicKey);

        classLoader = getClass().getClassLoader();
        file = new File(classLoader.getResource("key/friDayTointella-Stage-private.der").getFile());
        PrivateKey privateKey = KeyReader.loadPrivateKeyFromDER(file);

        String fuif8f = decryptByPrivateKey( com.sun.org.apache.xml.internal.security.utils.Base64.decode("QCqcQ8lMlYXpkKFzPurlawhYxY+rvO1wlhNPFSfuO1XWfUJtMPfPGcVPdP3YuotJn3QSiHAhSpbBAgvMcnMeq1auFqKMj/JDJ9VNjQSEQlaFv+VWjexS2sqIvOoUINlhL1jLuPLIh8k0BlMFkfuXhXTq1mNTzKK4MUB3iH+JvrWL/0tS8+34lmS0AMixiLEta3kuEiEMFdsShmZOX42EbZPHojhTDcduG34Hol+djzDl8ltWmXmfGZ5z1c2sjCkNavsljo3pk3mFMD7Yxh6Ex0Lxl+lcvf0yh+jCfMqr2Vb+q40UwKNrPRY5CDNQPOv0yQa78qpdZjzI8wPogei5pQ=="),privateKey);

//        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(qrcode);

        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "31800");
        requestData.put("ServiceType", "SignOn");
        requestData.put("MchId", "xxx");
        requestData.put("TradeKey", "0000");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss

        requestData.put("DeviceId", "01304098");

        IntegratedRequestBody body = new IntegratedRequestBody();
        String rawJson = (new Gson()).toJson(requestData);
        RequestHeader header = (RequestHeader)(new Gson()).fromJson(rawJson, RequestHeader.class);
        Map<String, String> dataMap = new HashMap();
        ArrayList<String> ignoreList = new ArrayList<String>() {
            {
                this.add("Method");
                this.add("ServiceType");
                this.add("MchId");
                this.add("CreateTime");
                this.add("TradeKey");
            }
        };
        Iterator var10 = requestData.keySet().iterator();

        String key;
        while(var10.hasNext()) {
            key = (String)var10.next();
            if(!ignoreList.contains(key)) {
                dataMap.put(key, requestData.get(key));
            }
        }

        body.setRequestData((new Gson()).toJson(dataMap));
        body.setRequestHeader(header);

        JsonObject jsonObject333 = new Gson().fromJson("{\"Header\":{\"StatusCode\":\"0000\",\"StatusDesc\":\"STATUS_SUCCESS\",\"Method\":\"31800\",\"ServiceType\":\"DeviceQuery\",\"MchId\":\"super\",\"ResponseTime\":\"20180214134253\"},\"Data\":{\"DeviceList\":[{\"description\":\"ezc\",\"type\":\"ezc\",\"name\":\"super-01304097\",\"ownerDeviceId\":\"01304097\",\"batchNumber\":35,\"ezcDongleId\":\"01100DE0A57E\",\"aesKey\":\"72737475767778797a31323334353637\"},{\"description\":\"ezc\",\"type\":\"ezc\",\"name\":\"super-01304096\",\"ownerDeviceId\":\"01304096\",\"batchNumber\":30,\"ezcDongleId\":\"00100DE0A57E\",\"aesKey\":\"72737475767778797a31323334353637\"},{\"description\":\"ezc\",\"type\":\"ezc\",\"name\":\"super-01304098\",\"ownerDeviceId\":\"01304098\",\"batchNumber\":64,\"ezcDongleId\":\"02100DE0A57E\",\"aesKey\":\"72737475767778797a31323334353637\"}]}}",JsonObject.class);

        byte[] b334 =  GeneralUtil.hex2Byte("A247E99D41C73529A01335FA0D793225");
        byte[] key4;
        if (b33.length == 16) {
            key4 = new byte[24];
            System.arraycopy(b33, 0, key4, 0, 16);
            System.arraycopy(b33, 0, key4, 16, 8);
        } else {
            key4 = b33;
        }

        SecretKey secretKey222 = convertTripleDESSecretKey(key4);
        String afff = Hex.encodeHexString (TripleDESUtil.des3EncodeCBCNoPadding(secretKey222, env.getProperty("twpay.3des.iv").getBytes(),"000059004126563540001201804259001000100583300400".getBytes("UTF-8")));

        byte[] samle = new BigInteger(afff,16).toByteArray();

        byte[] fkefw= TripleDESUtil.des3DecodeCBC(secretKey222,env.getProperty("twpay.3des.iv").getBytes(),samle);





        Document doc = DocumentHelper.createDocument();

        Element root = doc.addElement( "root" );

        Element author1 = root.addElement( "author" )
                .addAttribute( "name", "James" )
                .addAttribute( "location", "UK" )
                .addText( "James Strachan" );

        Element author2 = root.addElement( "author" )
                .addAttribute( "name", "Bob" )
                .addAttribute( "location", "US" )
                .addText( "Bob McWhirter" );

        XMLWriter writer = new XMLWriter(
                new FileWriter( "D:\\output.xml" )
        );
        writer.write( doc );
        writer.close();

        OutputFormat format = OutputFormat.createPrettyPrint();
        writer = new XMLWriter( System.out, format );
        writer.write( doc );




        String opay = "YWhPCKAAAAFylQABwVwyNTQxMjAxODAzMTkxMTM3MzcwMTEwMDAwMDk5OTk5OTEwMDMwMDAwMDBjUBgDGRLTs2V16xb2szk5OTk5OTAwMjAwMDAyMTIIMDAwMDAwMDEACJEAPmIW6V71MQ==";
        byte[] b =com.sun.org.apache.xml.internal.security.utils.Base64.decode(opay);
        byte[] c = Arrays.copyOfRange(b,14,18);

           String date=  new String(Arrays.copyOfRange(b,18,32), StandardCharsets.UTF_8);

        String bankcode=  new String(Arrays.copyOfRange(b,32,40), StandardCharsets.UTF_8);
        String comment=  new String(Arrays.copyOfRange(b,40,70), StandardCharsets.UTF_8);
        String card=  new String(Arrays.copyOfRange(b,70,86), StandardCharsets.UTF_8);

//        String fqf = Hex.encodeHex(b);
        SecretKey secretKey =  AesCryptoUtil.convertSecretKey("fbBn0u9JbvMXJNu9khcQzA==".getBytes());

        Integer.valueOf(String.valueOf(Hex.encodeHex(com.sun.org.apache.xml.internal.security.utils.Base64.decode(opay)),0,2));


        //final Base64.Decoder decoder = Base64.getDecoder();
        String decryptData = AesCryptoUtil.decrypt(secretKey,"yxnKaQWtWWKLscCq".getBytes(), com.sun.org.apache.xml.internal.security.utils.Base64.decode(opay));


        Map<String,String> result =
                new ObjectMapper().readValue(opay, HashMap.class);

        result.remove("CheckMacValue");

        getCheckMacValue(result,paymentAccountService.getOne("super",5));

//        refundDetailService.getTopOneByStoreRefundId("super");

        String enc = "rMP0Lb2wcuyxYqHKSkSiNlcMa0h4vjpmwuND0PusqkRrF5P6CkPnZyjqq14c1BxxZZkVG+NCDTyYp5afOw1EvKpn90wmn6IwvNGyMTLwj+vtNKJoUyqXnYvcDb2l/YCbSHMwuBvkSXMo/5RZov8/sQ94N/dj/uPYISPvU4NHBYdTgJG7OmbHPCKykRsr+6yl5W5rfPWAZKFdmCQoEtP5McSj2bsL+Yw3Wkztmb0RJ3N5DOdMOFJGn60bWEmqYKk7qHeWABaUwGX4E6MkoI9yeGd6TMfJxLpZnuILrAsXlRQGeiodSLg2bS7mFcMWnPHj0Fqh7lw+BDeGkc9QYZwopw==";

        SecretKey secretKey2 =  AesCryptoUtil.convertSecretKey("vC6jBU2iFH7FbNUB".getBytes());
        //final Base64.Decoder decoder = Base64.getDecoder();
        String decryptData3 = AesCryptoUtil.decrypt(secretKey2,"yxnKaQWtWWKLscCq".getBytes(), com.sun.org.apache.xml.internal.security.utils.Base64.decode(enc));


        Boolean add = ezCardService.bootDevice("013040198","08300000");

        TestUUID t = testService.getOne("2c9180825e5a6283015e5a6395760001");

        String aff = new ObjectMapper().writeValueAsString(t);

        TestUUID tt = new Gson().fromJson(aff,TestUUID.class);


        String afsf= "{\"result\":\"failure\",\"order_id\":\"\",\"pi_tx_id\":\"\",\"refund_amount\":\"\",\"refund_at\":\"\",\"error_code\":1022}";
        String piResponse ="{\"result\":\"success\",\"order_id\":\"4BF0018013000002\",\"pi_tx_id\":\"TX1807095048268706\",\"refund_amount\":1,\"refund_at\":{\"date\":\"2018-01-3013:52:01.000000\",\"timezone_type\":3,\"timezone\":\"Asia\\/Taipei\"}}";

        JsonObject jsonObject = new JsonParser().parse(afsf).getAsJsonObject();

        PiOLRefundResponseData data = new Gson().fromJson(piResponse,PiOLRefundResponseData.class);


        qrcodeService.getRandomBody(qrcodeService.getOneByShortId("intella"));

        List<String> paymentGateway = Arrays.asList("pi_wallet","credit_card");

        paymentGateway.indexOf("pi_wallet");

        String a = "{\"refundDetailRandomId\":\"8a8080b16107423d01610743a82e0000\",\"deviceRandomId\":{\"deviceRandomId\":\"0e347854-5a19-4b8f-9360-65da3750bbdb\",\"owner\":{\"merchantSeqId\":31,\"name\":\"▒q▒αb▒▒\",\"email\":\"miles@intella.co\",\"type\":null,\"creator\":1,\"pin\":\"ef51306214d9a6361ee1d5b452e6d2bb70dc7ebb85bf9e02c3d4747fb57d6bec\",\"accountId\":\"super\",\"comId\":\"99999999\",\"phone\":\"02-33225780-109\",\"signDate\":\"20170412160704\",\"status\":\"active\",\"paycodeToken\":\"xxx\",\"activateToken\":\"a759e8c1-fb6a-4\",\"refundPin\":\"0000\",\"recipients\":[{\"id\":\"befc1b1c-03e5-473f-b7a0-0425fecb41b8\",\"serialId\":\"0000\"},{\"id\":\"3986bcd1-3351-42df-aab3-dd9404195630\",\"serialId\":\"0000\"},{\"id\":\"1b3157d2-0f3a-4cab-8abf-8832d1f30261\",\"serialId\":\"0000\"},{\"id\":\"4eb30a15-841f-4f2e-9699-56cf291cfd33\",\"serialId\":\"0000\"},{\"id\":\"fdf252ec-db19-418c-afae-1838df36d27d\",\"serialId\":\"2ED71F99-31C9-4B6A-A4A1-EF4E99ADA27B\"},{\"id\":\"1f0e3c43-ddeb-4d6d-aa42-0821d1057bc7\",\"serialId\":\"61BBF41A-27E8-4947-8B20-0D6FC6E8CFCC\"},{\"id\":\"aa75e3b8-cd7b-4452-9479-43eddda5110a\",\"serialId\":\"0F417487-550D-4D1C-A3B4-0EB483806CD9\"},{\"id\":\"3460d815-8ee0-488d-a643-7d855b9426cd\",\"serialId\":\"876445F2-0AB8-41BE-B1DE-CDB068268F88\"},{\"id\":\"d636059b-64f6-448c-bbc2-8963a850b8fe\",\"serialId\":\"50EAB236-49A8-4B3F-A103-53ABE83DD465\"},{\"id\":\"95d98329-4a9d-4fc5-a37d-7c7b2b981744\",\"serialId\":\"9887a8314b31594849\"},{\"id\":\"c56958d7-df96-4dc7-9d6f-e001c4f291ea\",\"serialId\":\"0000\"},{\"id\":\"3994c8ad-2812-49cf-b672-108e86216cb2\",\"serialId\":\"E35818F0-7A10-4305-9669-CAFDE518B56A\"},{\"id\":\"c8789cc5-d403-4a51-8f02-11941e9da6a0\",\"serialId\":\"562cf44a\"},{\"id\":\"c8fc62e8-7eeb-4575-9c19-d64925c076ec\",\"serialId\":\"0000\"},{\"id\":\"5475944e-ccbd-4098-a2e2-8510ad52cd6b\",\"serialId\":\"52003661fe1f5429\"},{\"id\":\"b4abb460-410a-4362-9a94-174d241e952b\",\"serialId\":\"41002cca44f09115\"},{\"id\":\"1dddfed3-0149-4861-b168-005f46f44137\",\"serialId\":\"2707c4de\"},{\"id\":\"f65ff051-085a-46c2-8fe0-b7263e9ba88b\",\"serialId\":\"281370F7-EC2F-4B86-8697-7513F96FCFE2\"},{\"id\":\"e7f1b0ff-235a-4b02-92fd-05af137e6950\",\"serialId\":\"BISNT507008D\"},{\"id\":\"a18f9e1b-d2e4-4b2c-9927-c67dc979b28d\",\"serialId\":\"HT463WM06101\"},{\"id\":\"c672adf2-7a74-4ccc-b246-efb4e3aca051\",\"serialId\":\"0000\"}],\"saleId\":{\"id\":6,\"accountId\":\"admin\",\"pin\":\"0000\",\"isEnable\":1,\"type\":1,\"name\":\"administrator\",\"mobile\":\"0988777999\",\"email\":\"cs@intella.co\",\"createTime\":\"20170821171900\",\"lastLogin\":\"00000000000000\",\"modifiedTime\":\"00000000000000\"},\"notifyUrl\":\"dev.intella.co/allpaypass/api/test\",\"randomId\":\"3B194\",\"tradePin\":\"9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0\"},\"description\":null,\"type\":\"Allpay-001\",\"name\":null,\"ownerDeviceId\":\"23412342\",\"batchNumber\":null,\"ezcDongleId\":\"-1\"},\"orderId\":\"AllPT180116152215\",\"amount\":1,\"accountId\":\"super\",\"storeRefundId\":\"RllPT180116152215\",\"systemRefundId\":null,\"cashier\":null,\"status\":\"Refund fail\",\"createTime\":\"20180118111526\",\"reason\":\"product\",\"method\":\"10500\",\"platformRefundDate\":null,\"reconciliation\":false,\"sysTime\":1516245329000,\"batchNo\":null,\"rrn\":null,\"partialRefund\":false}";

        RefundDetail r = new Gson().fromJson(a,RefundDetail.class);

        PaymentAccount paymentAccount = paymentAccountService.getOneByDao("super", 15);


        TradeDetail tradeDetail = new TradeDetail();

        tradeDetail.setAccountId("super");
        tradeDetail.setMethod("10110");

        tradeDetail.setPaymentAccount(paymentAccount);
        tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        tradeDetail.setStatus("Creating order");
        tradeDetail.setOrderId("fsfsdfjkjksd");
        tradeDetail.setDeviceRandomId(deviceService.getOne(merchantService.getOne("super"), "APP-001"));
        tradeDetail.setPayment(1L);
        tradeDetail.setServiceType("OLPay");
        tradeDetail.setQrcodeToken("gkjdskgsg");
        tradeDetail.setStoreInfo("gggggg");
        tradeDetail.setDescription("fdsfsdfs");
        tradeDetail.setOnSale(false);
        tradeDetail.setOriginalPrice(1L);

        tradeDetail = tradeDetailService.save(tradeDetail);

        tradeProcedureService.captureOrderSave(tradeDetail);





    }

    public String getCheckMacValue(Map<String, String> map, PaymentAccount paymentAccount) throws Exception {
        StringBuilder checkStr = new StringBuilder();
        Map<String, String> treeMap = new TreeMap<String, String>(map);

        for(Map.Entry<String,String> entry : treeMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            checkStr.append(key).append("=").append(value).append("&");
        }
        checkStr = new StringBuilder("HashKey=" + paymentAccount.getHashKey() + "&" + checkStr + "HashIV=" + paymentAccount.getHashIV());
        checkStr = new StringBuilder(URLEncoder.encode(checkStr.toString(), "UTF-8").toLowerCase());
        return GeneralUtil.getHash(checkStr.toString()).toUpperCase();
    }

    @Test
    public void testAes() throws Exception{
        String b = decryptSecretKey("am8584ZY8Lal3HzlpOQsYORyBfagC4BCDZWRlWSokHWVkd6dfEL1xJeELz1B/4SJIkBQhdiHCDr5j2hFLKj5qz2CmP6gPRBd9Q/rBQP0ZgaDQrdoThQzP3eM3fqkNG3PbMq/qQAKFrCmnuUiDDUsr7rWVYOvJkrkAd5Q6ElvYA5THiiAFopmdLlXxsbUxomhBaeEShskkD3JzDQ4hctEDUU/2vIeAv7Sgr6d4tUO3YJnbBUV4mLoQJDVMCCBmfN2dlf2Ovq2lTIZK+4Gdm3mE4nXu2jmo1+paDgJLYREoFK3QpZPHBBf5JXZ1fnwzYMH9L0aNkoAoLFNgcgZDTIZCA==");
//gtCCipYdbU73aPh3/mfAGkis6Nu5PKgXwAuF/F081VjRJtHIEwyD55VM44wPTXXICXzAllYSk0nd5qrimnqdlV7tCpu7+pb49JPN/f5QWfcnnPKgUfXUOAy3RrFAmaq8LplX0x6ROlNGxl4Iu44r4fs6G+0/LWaGHDelbcbRiVra5cLsnFkw9uvYUZDqDuyosNzslgWPBHlJGZ8RtmVbHx9CuvnJUXgdDU4PA5qGaEWK7Cu2RwYzVL1lL4X8h3n69gmt0e9RqMzaP0AP2Ey2IY3kIH7xfrPyaO9lI9h2RTJsn/xQ+CzNcP6S7ls/HejkBOzgNcKA5nEgIPGdErmhqQ==

        SecretKey secretKey =AesCryptoUtil.convertSecretKey(com.sun.org.apache.xml.internal.security.utils.Base64.decode(b.getBytes()));
        String a = AesCryptoUtil.decrypt(secretKey, Constant.IV, com.sun.org.apache.xml.internal.security.utils.Base64.decode("74OWztbD9FtTEHbjspczKzyX/g6NZ1mV/Y70+69P2id8xowXBCwf/rCCPwShYgWhUH9XLkt4SR01mlTcZ7eY8p6j2ALoiHFWLQwTnW26+zINFNurRV8UKhy8pjJUXU04dJKwgsbyDkrJpvZtS8W+BT0V+QVQ4Ietdfdw6sKbKC+JNupoeYcV81a0FCtbHlPG6Ii9OaXSUzqRMVBPs+h1Ty5lF12LCq92z5ZsXS/OXB4ju7NK3fd2zdiCgXx2B8B5vV3ihcDEF3OXcD6XU0cUGerkFY7vHoXHMibY6gHgSRE5nc+IkWANQSid9ZPxDJHP".getBytes()));
        File fileDir = new File("D:\\test.txt");


        File fileDir2 = new File("D:\\test.txt");

    }

    private String decryptSecretKey(String encryptApiKey) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("key/stage-pri.der").getFile());
        PrivateKey privateKey = KeyReader.loadPrivateKeyFromDER(file);
        byte[] byteKeys = com.sun.org.apache.xml.internal.security.utils.Base64.decode(encryptApiKey);
        return decrypt(byteKeys, privateKey);

    }


    class EncryptAES {
        private static final String ALGORITHM = "AES";
        private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
        private  final Base64.Decoder decoder = Base64.getDecoder();
        private  final Base64.Encoder encoder = Base64.getEncoder();

        private Key key;
        private IvParameterSpec iv;
        private Cipher cipher;
        public EncryptAES(String IV, String KEY){
            this.key = new SecretKeySpec(decoder.decode(KEY), ALGORITHM);
            this.iv = new IvParameterSpec(decoder.decode(IV));
            try {
                cipher = Cipher.getInstance(TRANSFORMATION);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            }
        }
        public String encrypt(final String str) {
            byte[] bytes = new byte[0];
            try {
                bytes = str.getBytes("UTF-16LE");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return encrypt(bytes);
        }
        private String encrypt(final byte[] data) {
            try {
                cipher.init(Cipher.ENCRYPT_MODE, key, iv);
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (InvalidAlgorithmParameterException e) {
                e.printStackTrace();
            }
            try {
                final byte[] encryptData;

                encryptData = cipher.doFinal(data);
                return new String( encoder.encode(getHash("SHA-256", encoder.encodeToString(encryptData))), "UTF-8");

            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();

            } catch (BadPaddingException e) {
                e.printStackTrace();
            }catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return null;
        }
        private  byte[] getHash(final String algorithm, final String text) {
            try {
                return getHash(algorithm, text.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return null;
            }
        }
        private  byte[] getHash(final String algorithm, final byte[] data) {
            try {
                final MessageDigest digest = MessageDigest.getInstance(algorithm);
                digest.update(data);
                return digest.digest();
            } catch (final Exception ex) {
                throw new RuntimeException(ex.getMessage());
            }
        }
    }
}