package co.intella.service;

import co.intella.model.CaptureDetail;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Miles Wu
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CaptureDetailServiceTests {

    @Autowired
    private CaptureDetailService captureDetailService;

    @Test
    public void testGetOne(){

        CaptureDetail entity = captureDetailService.getOne("4ee47203-a3a4-4bb1-8302-fd1687aca995");
        Assert.assertEquals("super", entity.getMerchantId());

    }

    @Test
    public void testSettle() {
        CaptureDetail entity = captureDetailService.settle("super", "20400", "20170601000000", "20170619235959");
        Assert.assertEquals("super", entity.getMerchantId());
    }

}
