package co.intella.service;

import co.intella.crypto.KeyReader;
import co.intella.model.Merchant;
import co.intella.model.Recipient;
import co.intella.model.TradeDetail;
import co.intella.net.HttpRequestUtil;
import com.google.gson.Gson;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.PrivateKey;
import java.util.*;

import static org.springframework.http.HttpHeaders.USER_AGENT;

/**
 * @author Miles
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AppNotificationServiceTests {

    private static PrivateKey PRIVATE_KEY;

    @Value("${host.request.weixin.php}")
    private String HOST;

    @Resource
    private MerchantService merchantService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private RecipientService recipientService;

    @Test
    public void sendGet() throws Exception {

        String url = "http://a1.intella.co/intella";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());

    }

    @Test
    public void testSnsRegister() {
        String serialId="bensonSnsTest";
        Merchant merchantPid=merchantService.getOne("benson");
        List<Recipient> list = new ArrayList<Recipient>(merchantPid.getRecipients());
        for(Recipient element : list)
        {
            if (element.getSerialId().equals(serialId))
            {
                System.out.println("serialId already exists.");
                return;
            }
        }
        Recipient recipient = new Recipient();
        recipient.setSerialId(serialId);
        recipient.setMerchant(merchantPid);
        recipientService.save(recipient);

        // todo assertion
    }


    @Test
    public void testSnsRemoveByAccount() {
        Merchant merchantPid=merchantService.getOne("benson");
        List<Recipient> list = new ArrayList<Recipient>(merchantPid.getRecipients());
        for(Recipient element : list) {
            String serialId = element.getSerialId();
            recipientService.delete(serialId);
        }
        // todo assertion
    }


    @Test
    public void WeixinSnsPushTest()  throws Exception {
        TradeDetail tradeDetail = tradeDetailService.getOne("super", "BISNT507008D0000000241");
        System.out.println("tradeDetail  status: "+tradeDetail.getStatus());
        //commom
        Map<String, String> map = new HashMap<String, String>();
        map.put("ver", "101");
        map.put("method", "10");
        map.put("mch_id", "103543903790001");
        map.put("nonce_str", "nonce_str");
        //WeixinOrderQueryRequestData
        map.put("trade_type", "ORDERQUERY");
        map.put("out_trade_no", "BISNT507008D0000000241");

        System.out.println("[TRANSACTION] weixin singleQuery start. " + new Gson().toJson(map));
        String result = HttpRequestUtil.post(HOST, map, getPrivateKey());
        // todo assertion
    }

    private PrivateKey getPrivateKey() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("key/php_private.der").getFile());
        if(PRIVATE_KEY == null) {
            PRIVATE_KEY =  KeyReader.loadPrivateKeyFromDER(file);
        }
        return PRIVATE_KEY;
    }
}
