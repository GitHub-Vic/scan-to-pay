package co.intella.service;

import co.intella.crypto.AesCryptoUtil;
import co.intella.crypto.KeyReader;
import co.intella.domain.allpay.AllpayMicropayRequestData;
import co.intella.domain.crm.TicketReviewData;
import co.intella.model.*;
import co.intella.model.SalesManage.Status;
import co.intella.model.SalesManage.WorkflowConfig;
import co.intella.net.Constant;
import co.intella.net.HttpRequestUtil;
import co.intella.service.CRM.TicketHistoryService;
import co.intella.service.CRM.TicketOperatorService;
import co.intella.service.CRM.TicketService;
import co.intella.service.CRM.WorkflowService;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import io.netty.handler.codec.http.HttpUtil;
import org.apache.commons.lang.StringEscapeUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import javax.annotation.Resource;
import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.*;
import java.sql.PreparedStatement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import static co.intella.crypto.RsaCryptoUtil.decrypt;

/**
 * @author Alex
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class WorkflowTests {

//    @Autowired
//    private CacheService cacheService;

    @Autowired
    private WorkflowService workflowService;

    @Autowired
    private SaleService saleService;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private TicketHistoryService ticketHistoryService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private TicketOperatorService ticketOperatorService;

    @Autowired
    private TradeDetailService tradeDetailService;
    @Autowired
    private Environment env;

    @Resource
    private CaptureOrderDetailService captureOrderDetailService;

    private SessionFactory sessionFactory;

    @Resource
    private BankService bankService;

    @Test
    public void testAes() throws Exception{
        String b = decryptSecretKey("VylVy6/z+2dgylqBL010tWH/YVpii7xLstSZaY0unpj/Mw9dj0vZYFJxps8RpctGYYRLZEBMgsfPm9NKsZPRui1payDay6ykzX/gPb6ZAQkMneuiTJYfQmiJDCtX0VGllZIpIHUF1ZfiRD7vBU3pBjfTovgCZs4ozrzovFyuLRxJp2hz+lXbXRexg487ioyxcF2jNJBdAbfdKAssjP17IO+Pt6lhitbcPaRlxE20GaAQSjsJiAZEeFaN2tW9/oBpV7oGUMsi60Khwu/R62aYpe8h3fD2gvwkGbszuTL0z0/LNwxI5EbG+D0092ZdGhj4VuOYdSH8a95eWsPn1wySyQ==");


        SecretKey secretKey =AesCryptoUtil.convertSecretKey(com.sun.org.apache.xml.internal.security.utils.Base64.decode(b.getBytes()));
        String a = AesCryptoUtil.decrypt(secretKey, Constant.IV, com.sun.org.apache.xml.internal.security.utils.Base64.decode("74OWztbD9FtTEHbjspczKzyX/g6NZ1mV/Y70+69P2id8xowXBCwf/rCCPwShYgWhUH9XLkt4SR01mlTcZ7eY8p6j2ALoiHFWLQwTnW26+zINFNurRV8UKhy8pjJUXU04dJKwgsbyDkrJpvZtS8W+BT0V+QVQ4Ietdfdw6sKbKC+JNupoeYcV81a0FCtbHlPG6Ii9OaXSUzqRMVBPs+h1Ty5lF12LCq92z5ZsXS/OXB4ju7NK3fd2zdiCgXx2B8B5vV3ihcDEF3OXcD6XU0cUGerkFY7vHoXHMibY6gHgSRE5nc+IkWANQSid9ZPxDJHP".getBytes()));
        File fileDir = new File("D:\\test.txt");


        File fileDir2 = new File("D:\\test.txt");

    }

    private String decryptSecretKey(String encryptApiKey) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("key/stage-pri.der").getFile());
        PrivateKey privateKey = KeyReader.loadPrivateKeyFromDER(file);
        byte[] byteKeys = com.sun.org.apache.xml.internal.security.utils.Base64.decode(encryptApiKey);
        return decrypt(byteKeys, privateKey);

    }

    @Test
    public void testLoda() throws Exception{
        File fileDir = new File("D:\\test.txt");

        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(fileDir), "Big5"));

        String str;

        Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("D:\\filename.txt"), "utf-8"));

        while ((str = in.readLine()) != null) {
            System.out.println(StringEscapeUtils.escapeJava(str));
            writer.write( StringEscapeUtils.unescapeCsv( StringEscapeUtils.escapeJava(str)));

        }

        in.close();
        writer.close();




    }

    @Test
    public void testCreate() throws Exception {
       // workflowService.loadConfig("D:\\workflow.config");

        Sale sales = saleService.getOneById(11);

        Merchant merchant =  merchantService.getOne(28);
        Ticket ticket= ticketService.getOneIsActive(merchant);
        if (ticket!=null){
            throw new Exception("Ticket already exists");
        }

        ticket= workflowService.createTicket(merchant, sales);

        System.out.print(ticket);
    }

    @Test
    public void testTransfer() throws Exception{

        Merchant merchant =  merchantService.getOne(11);
        Ticket ticket= ticketService.getOneIsActive(merchant);

        if (ticket==null){
            throw new Exception("Ticket is not existed");
        }


        Sale sales1 = saleService.getOneById(9);
//        //first pass
//        ticket = workflowService.nextStep(ticket, WorkflowAction.APPROVE, sales1);
//        Sale sales2 = saleService.getOneById(8);
//        //second return
//        ticket = workflowService.nextStep(ticket,WorkflowAction.APPROVE,sales2);

        //reset ticket
        ticket = workflowService.resetTicket(ticket,sales1);

        //first reject
        //ticket = workflowService.nextStep(ticket, WorkflowAction.REJECT, sales1);



        System.out.print(ticket);
    }

    @Test
    public void redis(){
//        cacheService.addMessage("test","555");
//        List<String> messages = cacheService.listMessages("test");
//        cacheService.listMessages("test");
    }

    @Test
    public void testList(){
//        Merchant merchant =  merchantService.getOne(3);
//
//        Ticket ticket= ticketService.getOneIsActive(merchant);
//
//        ticketOperatorService.listAllByTicket(ticket);
//        System.out.print(ticket);

//        List<Ticket> tickets = ticketService.listAll();
//
//        Sale sales = saleService.getOneById(6);
//        List<Ticket> tickets2 = ticketService.listAllByCreator(sales);
//
//        Merchant merchant = merchantService.getOne(2);
//        List<Ticket> tickets3 = ticketService.listAllByMerchant(merchant);
        Sale sales = saleService.getOneById(9);

        List<Ticket> tickets = ticketService.listForReview(sales);
        System.out.print(tickets);


    }
    @Test
    public void testHistoryList(){
        Sale sales = saleService.getOneById(8);

        List<TicketHistory> a =ticketHistoryService.listBySales(sales);

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try {
            //String b = new Gson().toJson(a);
            String requestJson = ow.writeValueAsString(a);
            System.out.print(requestJson);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


       // List<Ticket> tickets = ticketService.listHistory(sales);
//        System.out.print(tickets);
    }


    @Test
    public void testStroe()throws Exception{



    }
    @Test
    public void  test() throws Exception {

//        String body = "{\"Data\":\"{\\\"CardId\\\":\\\"5408360100001705\\\",\\\"ExpireDate\\\":\\\"3112\\\",\\\"DeviceInfo\\\":\\\"A6113004\\\",\\\"DeviceOS\\\":\\\"2\\\",\\\"TotalFee\\\":\\\"1\\\",\\\"StoreOrderNo\\\":\\\"IntellaA170824000001\\\",\\\"ExtenNo\\\":\\\"165\\\"}\",\"Header\":{\"CreateTime\":\"20170824175739\",\"MchId\":\"super\",\"Method\":\"20000\",\"ServiceType\":\"Payment\"}}";
//
//        JsonObject jsonObject = new Gson().fromJson(body,JsonObject.class);
//        JsonObject header = jsonObject.getAsJsonObject("Header");
//        JsonPrimitive data5 = jsonObject.getAsJsonPrimitive("Data");
//
//        RequestHeader requestHeader = new Gson().fromJson(header.toString(), RequestHeader.class);
//        JsonObject requestData3 = new Gson().fromJson(data5.getAsString(), JsonObject.class);
//
//



        String data = "fwfwefwefw";

        Signature signature = Signature.getInstance("SHA256withRSA");

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(env.getProperty("intella.private.key")).getFile());
        PrivateKey privateKey = KeyReader.loadPrivateKeyFromDER(file);

        signature.initSign(privateKey);

        signature.update(data.getBytes());

        byte[] sign = signature.sign();
        String signData = Base64.getEncoder().encodeToString(sign);

        String adad = Base64.getEncoder().encodeToString("Hello".getBytes("utf-8"));


        Signature signature1 = Signature.getInstance("SHA256withRSA");

        File file1 = new File(classLoader.getResource("key/stage-pub.der").getFile());
        PublicKey publicKey = KeyReader.loadPublicKeyFromDER(file1);

        signature1.initVerify(publicKey);
        signature1.update(data.getBytes());



        boolean bool = signature1.verify(Base64.getDecoder().decode(signData.getBytes()));



        Sale sales = saleService.getOneById(8);

        List<TicketReviewData> ticketReviewDataList = new ArrayList<TicketReviewData>();

        List<Ticket> tickets = ticketService.listForReview(sales);

        WorkflowConfig config =workflowService.getCONFIG();

        List<Status> statuses = config.getStatuses();


        for(Ticket t: tickets){
            TicketReviewData ticketReviewData = new TicketReviewData();
            ticketReviewData.setTicketId(t.getId());
            ticketReviewData.setCreator(t.getCreator().getName());
            ticketReviewData.setMerchantName(t.getMerchant().getName());
            ticketReviewData.setStatus(statuses.get(t.getStatus()).getName());
            ticketReviewData.setSystime( new DateTime(t.getCreateDate()).toString("yyyy/MM/dd HH:mm:ss"));
            ticketReviewDataList.add(ticketReviewData);

        }

        try {
            String result = new ObjectMapper().writeValueAsString(ticketReviewDataList);
            System.out.print(result);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        System.out.print(tickets);


    }


    @Test
    public void XXX(){


        TradeDetail t = tradeDetailService.getOne("OL2170912142734");

        t.setTradeToken("1111");

//        tradeDetailService.saveByDao(t);

        CaptureOrderDetail c = captureOrderDetailService.getOne("lafresh","20170524170723abc");

        String ffff = null;
        try {
            ffff = new ObjectMapper().writeValueAsString(c);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        ffff = "f";


        String fsdfsd = "2017-11-23T02:26:30Z";

        DateTime dt = DateTime.parse(fsdfsd);

        String strMacData = "1900gamga0002TWD00000000000200wPPVeQCv4/tOskXIqhBcSg==";
        String iv = "wPPVeQCv4/tOskXIqhBcSg==";
        String key = "K5biAb1s2vgEsap9WbKK8RJRE4r3wSgIcWV1okqUIUg=";


        EncryptAES encryptAES = new EncryptAES(iv, key);

        String mac = encryptAES.encrypt(strMacData);

        System.out.println(mac);


        String ff= "1200";
        String fewf = String.format("%014d",Integer.valueOf(ff));
//        tradeDetailService.set
        AllpayMicropayRequestData allpayMicropayRequestData = new AllpayMicropayRequestData();
        allpayMicropayRequestData.setTradeDesc("123");
        String a3 = new Gson().toJson(allpayMicropayRequestData);
        String b3  = new Gson().toJson(a3);

        String nDate;
        String action;

        DateTime createDateTime = DateTime.parse("20170921210000", DateTimeFormat.forPattern(SystemInstance.DATE_PATTERN));
        if (createDateTime.getHourOfDay() < 20 ){
            nDate = createDateTime.toString("yyyyMMdd").concat("200000");
        }
        else {
            nDate = createDateTime.plusDays(1).toString("yyyyMMdd").concat("200000");
        }
        if (nDate.compareTo( DateTime.now().toString(SystemInstance.DATE_PATTERN)) ==-1){
            action = "R";
        }
        else {
            action="N";
        }
        String a  = DateTime.now().toString("yyyyMMdd").concat("200000");
        String b ="20170921190000";
        b.compareTo(a);

//        responseGeneralHeader.setStatusCode(errCode);
//        responseGeneralHeader.setStatusDesc(temp[1]);

//        DateTime a = DateTime.parse("20170919",DateTimeFormat.forPattern("yyyyMMdd"));
//
//        DateTime qDateTime= DateTime.parse("2017-09-1517:42:56", DateTimeFormat.forPattern("yyyy-MM-ddHH:mm:ss"));
//
//        DateTime cDateTime= DateTime.parse("20170912103053", DateTimeFormat.forPattern("yyyyMMddhhmmss"));
//
//        if (qDateTime.getMillis() > cDateTime.plusMinutes(15).getMillis()){
//            System.out.print("fff");
//
//        }
    }
    class EncryptAES {
        private static final String ALGORITHM = "AES";
        private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
        private  final Base64.Decoder decoder = Base64.getDecoder();
        private  final Base64.Encoder encoder = Base64.getEncoder();

        private Key key;
        private IvParameterSpec iv;
        private Cipher cipher;
        public EncryptAES(String IV, String KEY){
            this.key = new SecretKeySpec(decoder.decode(KEY), ALGORITHM);
            this.iv = new IvParameterSpec(decoder.decode(IV));
            try {
                cipher = Cipher.getInstance(TRANSFORMATION);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            }
        }
        public String encrypt(final String str) {
            byte[] bytes = new byte[0];
            try {
                bytes = str.getBytes("UTF-16LE");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return encrypt(bytes);
        }
        private String encrypt(final byte[] data) {
            try {
                cipher.init(Cipher.ENCRYPT_MODE, key, iv);
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (InvalidAlgorithmParameterException e) {
                e.printStackTrace();
            }
            try {
                final byte[] encryptData;

                encryptData = cipher.doFinal(data);
                return new String( encoder.encode(getHash("SHA-256", encoder.encodeToString(encryptData))), "UTF-8");

            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();

            } catch (BadPaddingException e) {
                e.printStackTrace();
            }catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return null;
        }
        private  byte[] getHash(final String algorithm, final String text) {
            try {
                return getHash(algorithm, text.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return null;
            }
        }
        private  byte[] getHash(final String algorithm, final byte[] data) {
            try {
                final MessageDigest digest = MessageDigest.getInstance(algorithm);
                digest.update(data);
                return digest.digest();
            } catch (final Exception ex) {
                throw new RuntimeException(ex.getMessage());
            }
        }
    }
}