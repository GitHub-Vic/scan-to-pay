package co.intella.service;

import co.intella.domain.aipei.*;
import co.intella.domain.line.LineReserveRequestData;
import co.intella.model.PaymentAccount;
import co.intella.utility.GeneralUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Alex
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AipeiServiceTests {
    @Autowired
    private AipeiService aipeiService;

    @Autowired
    private PaymentAccountService paymentAccountService;

    @Autowired
    private Environment env;

    @Test
    public void testPayment() throws Exception{

        PaymentAccount paymentAccount = paymentAccountService.getOneByDao("super",17);

        AipeiPaymentParaData aipeiPaymentParaData = new AipeiPaymentParaData();

        aipeiPaymentParaData.setPartnerTransId("AiTest000001");
        aipeiPaymentParaData.setBuyerCodeType("barcode");
        aipeiPaymentParaData.setBuyerCode("888888888888888888");
        aipeiPaymentParaData.setTransName("test");
        aipeiPaymentParaData.setTransAmount(100);
        aipeiPaymentParaData.setCurrency("TWD");
        aipeiPaymentParaData.setMerchantType("5995");
        aipeiPaymentParaData.setMerchantId("320008");
        aipeiPaymentParaData.setMerchantName("intella");
        aipeiPaymentParaData.setTerminalId("T00000000");
        aipeiPaymentParaData.setTerminalName("POS01");

        aipeiPaymentParaData.setTransCreateTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        AipeiPaymentRequestData aipeiPaymentRequestData = new AipeiPaymentRequestData();

        aipeiPaymentRequestData.setPartnerId("320008");
        aipeiPaymentRequestData.setService("ap.pay");
        aipeiPaymentRequestData.setData(new ObjectMapper().writeValueAsString(aipeiPaymentParaData));
        aipeiPaymentRequestData.setSignType("MD5");
        aipeiPaymentRequestData.setSign(getSign(aipeiPaymentRequestData,paymentAccount));

        String result = aipeiService.payment(aipeiPaymentRequestData);




    }

    private String getSign(AipeiPaymentRequestData request,PaymentAccount paymentAccount) throws JsonProcessingException {

        return GeneralUtil.getMD5(request.getPartnerId()+request.getService()+request.getData()+request.getSignType()+"qvrRihhtwYXludxsxf3iJw1V0Dx8bVdW");

    }

    @Test
    public void testQuery() throws Exception {
        PaymentAccount paymentAccount = paymentAccountService.getOneByDao("super",17);

        AipeiPaymentParaData aipeiPaymentParaData = new AipeiPaymentParaData();

        aipeiPaymentParaData.setPartnerTransId("AiT171206164354");

        AipeiPaymentRequestData aipeiPaymentRequestData = new AipeiPaymentRequestData();

        aipeiPaymentRequestData.setPartnerId("320008");
        aipeiPaymentRequestData.setService("ap.query");
        aipeiPaymentRequestData.setData(new ObjectMapper().writeValueAsString(aipeiPaymentParaData));
        aipeiPaymentRequestData.setSignType("MD5");
        aipeiPaymentRequestData.setSign(getSign(aipeiPaymentRequestData,paymentAccount));

        String result = aipeiService.query(aipeiPaymentRequestData);
    }

    @Test
    public void testRefundPayment() throws Exception {
        PaymentAccount paymentAccount = paymentAccountService.getOneByDao("super",17);

        AipeiPaymentParaData aipeiPaymentParaData = new AipeiPaymentParaData();

        aipeiPaymentParaData.setPartnerTransId("AiT171206161410");
        aipeiPaymentParaData.setPartnerRefundId("RAiT171206161410");
        aipeiPaymentParaData.setRefundAmount(100);
        aipeiPaymentParaData.setCurrency("TWD");

        AipeiPaymentRequestData aipeiPaymentRequestData = new AipeiPaymentRequestData();

        aipeiPaymentRequestData.setPartnerId("320008");
        aipeiPaymentRequestData.setService("ap.refund");
        aipeiPaymentRequestData.setData(new ObjectMapper().writeValueAsString(aipeiPaymentParaData));
        aipeiPaymentRequestData.setSignType("MD5");
        aipeiPaymentRequestData.setSign(getSign(aipeiPaymentRequestData,paymentAccount));

        String result = aipeiService.refundPayment(aipeiPaymentRequestData);
    }


    @Test
    public void teststate() throws Exception{
        Map<String,String> map = new HashMap<String, String>();

        map.put("a","1.0");

        Integer a = Double.valueOf(map.get("a")).intValue();

        context context = new context();

        AipeiPaymentResponseParaData data = new AipeiPaymentResponseParaData();
        if (data.getApTransStatus().equals("0000")){
            successState su = new successState();
            su.getResponse(data);
        }




    }
}

