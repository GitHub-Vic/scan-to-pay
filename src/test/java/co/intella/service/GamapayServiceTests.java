package co.intella.service;

import co.intella.domain.gama.*;
import co.intella.model.PaymentAccount;
import co.intella.utility.JsonUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Alex
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class GamapayServiceTests {
    @Autowired
    private GamaPayService gamaPayService;

    @Autowired
    private PaymentAccountService paymentAccountService;

    @Autowired
    private Environment env;

    @Test
    public void testMicropay() throws Exception{
        PaymentAccount paymentAccount =paymentAccountService.getOne("super",13);
        GamaCreateOrderRequestData gamaCreateOrderRequestData =new GamaCreateOrderRequestData();

        gamaCreateOrderRequestData.setMerchantId(paymentAccount.getAccount());
        gamaCreateOrderRequestData.setMerchantOrderId("gama0008");
        gamaCreateOrderRequestData.setTransType(10);
        gamaCreateOrderRequestData.setTradingContent("test001");
//        gamaCreateOrderRequestData.setTradingToken("688153333161268399");
        gamaCreateOrderRequestData.setCurrencyCode("TWD");
        gamaCreateOrderRequestData.setBusinessDate(DateTime.now().toString("yyyyMMdd"));
        gamaCreateOrderRequestData.setTransAmount(1);

        String result = gamaPayService.createOrder(gamaCreateOrderRequestData,paymentAccount);

        GamaCreateOrderResponseData gamaCreateOrderResponseData = new Gson().fromJson(result,GamaCreateOrderResponseData.class);

        GamaQrcodeData qrcodeData = new Gson().fromJson(gamaCreateOrderResponseData.getQrCodeData(),GamaQrcodeData.class);




    }

    @Test
    public void testRefund() throws Exception{
        PaymentAccount paymentAccount =paymentAccountService.getOne("super",13);
        GamaCreateOrderRequestData gamaCreateOrderRequestData =new GamaCreateOrderRequestData();

        gamaCreateOrderRequestData.setMerchantId(paymentAccount.getAccount());
        gamaCreateOrderRequestData.setMerchantOrderId("GMT171129142539");
        gamaCreateOrderRequestData.setTransactionID("20171129100079422028");
        gamaCreateOrderRequestData.setCurrencyCode("TWD");
        gamaCreateOrderRequestData.setTransAmount(1);

        String result = gamaPayService.refund(gamaCreateOrderRequestData,paymentAccount);
        GamaResponseData gamaCreateOrderResponseData = new Gson().fromJson(result,GamaResponseData.class);

    }

    @Test
    public void testSingleOrderQuery() throws Exception{
        PaymentAccount paymentAccount =paymentAccountService.getOne("super",13);
        GamaCreateOrderRequestData gamaCreateOrderRequestData =new GamaCreateOrderRequestData();

        gamaCreateOrderRequestData.setMerchantId(paymentAccount.getAccount());
        gamaCreateOrderRequestData.setMerchantOrderId("GMT171129142539");
        gamaCreateOrderRequestData.setTransactionID("ReversalTrading");
        gamaCreateOrderRequestData.setCurrencyCode("TWD");
        gamaCreateOrderRequestData.setTransAmount(1);
        gamaCreateOrderRequestData.setTransType(10);
        String result = gamaPayService.singleOrderQuery(gamaCreateOrderRequestData,paymentAccount);
        GamaResponseData gamaResponseData = new Gson().fromJson(result,GamaResponseData.class);
        gamaResponseData.getTransFinallyAmount();

    }

//    @Test
//    public void testInApp() throws Exception{
//       String a = "{\"TransactionToken\":\"cf7d5583a6c3403194b55cded99d3dc7\",\"TransactionID\":\"20171130100079498783\",\"MerchantID\":\"1900\",\"SubMerchantID\":null,\"MerchantOrderID\":\"gama0008\",\"TransAmount\":1.0,\"CurrencyCode\":\"TWD\",\"TransType\":\"10\",\"ExpiredDateTime\":\"2017/11/30 12:15:19\",\"MAC\":\"uM13L3t66XFo7oqtBQg+f0ZcyaOnyAXXSvHJX7v1HXo=\",\"QRCodeData\":\"{\\\"Type\\\":3,\\\"ReceiverLoginID\\\":\\\"intella\\\",\\\"TransactionToken\\\":\\\"cf7d5583a6c3403194b55cded99d3dc7\\\",\\\"Source\\\":\\\"Vender\\\",\\\"TransType\\\":\\\"10\\\",\\\"SubMerchantID\\\":null,\\\"CashierSN\\\":null}\",\"ResultCode\":\"0000\",\"ResultMessage\":\"執行成功\",\"PayerID\":\"1900\",\"LoveCode\":null,\"CarrierID\":null,\"TransFinallyAmount\":0.0,\"TransFee\":0.0,\"PerformanceBondDateTime\":null,\"CreatedDate\":\"2017/11/30 11:45:19\",\"TransDate\":null,\"StatusCode\":\"0\",\"Relation_ID_Trans\":null,\"CashierSN\":null}";
//        String result = gamaPayService.getInAppUrl(a,"https://?&hj");
//        GamaResponseData gamaResponseData = new Gson().fromJson(result,GamaResponseData.class);
//        gamaResponseData.getTransFinallyAmount();
//
//    }



}


