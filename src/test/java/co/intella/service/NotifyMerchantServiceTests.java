package co.intella.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author Alex
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class NotifyMerchantServiceTests {

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private NotifyService notifyService;


    @Test
    public void test(){


        try {
            notifyService.notifyMerchant(tradeDetailService.getOne("OL2170918182732"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
