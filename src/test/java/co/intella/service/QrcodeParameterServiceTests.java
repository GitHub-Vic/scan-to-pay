package co.intella.service;

import co.intella.model.QrcodeParameter;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Miles Wu
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class QrcodeParameterServiceTests {

    @Autowired
    private QrcodeService qrcodeService;

    @Test
    public void testSave() {
        QrcodeParameter qrcodeParameter = new QrcodeParameter();
        qrcodeParameter.setBody("test2017");
        qrcodeParameter.setShortId("int05");
        qrcodeParameter.setDetail("detail2017");
        qrcodeParameter.setMchId("int05");
        qrcodeParameter.setCodeType("3");

        QrcodeParameter entity = qrcodeService.save(qrcodeParameter);

        Assert.assertEquals("test2017", entity.getBody());
    }

}
