package co.intella.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author Alex
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderServiceTests {

    @Resource
    private OrderService orderService;
    @Test
    public void testReserve() throws Exception{

        String order = orderService.createOrderId("super");
        System.out.print(order);
    }
}
