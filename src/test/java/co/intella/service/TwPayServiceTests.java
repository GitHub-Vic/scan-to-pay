package co.intella.service;

import co.intella.domain.twpay.TwPayRequestData;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.xml.sax.InputSource;

import javax.annotation.Resource;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;


/**
 * @author Alex
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TwPayServiceTests {

    @Resource
    private TwPayService twPayService;
    @Test
    public void testMicropay() throws Exception {


        TwPayRequestData twPayRequestData = new TwPayRequestData();

//        twPayRequestData.setAcqBank("004");
//        twPayRequestData.setAmt("000000001200");
//        twPayRequestData.setCardNumber("0000983004020293");
//        twPayRequestData.setChipData("MjU0MTAwNDAwMDAwUVIxMDAwMDAwMDAwMDAwMDAxMjM0NTY3ODAwMDAwMDAwMDk4MzAwNDAyMDI5MwgxMjM0NTY3OAAK6qsSqr3eO6wVMDAwMDAwNDAgICAgICAg");
//        twPayRequestData.setCountryCode("158");
//        twPayRequestData.setHostId("00401035573110100000001");
//        twPayRequestData.setLocalDate("20180425");
//        twPayRequestData.setLocalTime("005833");
//        twPayRequestData.setMerchantId("004126563540001");
//        twPayRequestData.setMti("0200");
//        twPayRequestData.setOrderNumber("Order20180425000059");
//        twPayRequestData.setOtherInfo("{\"tag05\":\"FFDFE84D\",\"tag12\":\"659\"}");
//        twPayRequestData.setPosConditionCode("77");
//        twPayRequestData.setPosEntryMode("091");
//        twPayRequestData.setProcessingCode("002541");
//        twPayRequestData.setTerminalId("90010001");
//        twPayRequestData.setTraceNumber("000059");
//        twPayRequestData.setTxnCurrencyCode("901");
//        twPayRequestData.setVerifyCode(twPayService.getVerifyCode(twPayRequestData));

        twPayRequestData.setOrderNumber("intellatest043");
        twPayRequestData.setAmt("1");
        twPayRequestData.setAuthCode("YWhPCKAAAAFylQABwVwyNTQxMjAxODA3MTMxNzM2MzEwMTEwMDAwMDk5OTk5OTEwMDMwMDAwMDBjUBgHExhjpC4b9Jl4zTk5OTk5OTAwMjAwMDAzNzcIMDAwMDAwNjUACAXIXD0gHDuHMQ==");
//        twPayService.prepareRequestData(twPayRequestData);

        Document document = twPayService.createMicorpayXml(twPayRequestData,"auth2541",null,null);
//        XMLWriter writer = new StandaloneWriter(
//                new FileWriter( "D:\\output.xml" )
//        );
//        writer.write( document );
//        writer.close();
//        document.toString();
//        OutputFormat format = OutputFormat.createPrettyPrint();
//        StringWriter a = new StringWriter();
//        StandaloneWriter writer = new StandaloneWriter(a , format );
//        writer.write( document );


        String result = twPayService.xmlPost("https://www.focas-test.fisc.com.tw/FOCAS_WS/API20/V1/FISCII/auth2541",document.asXML().replace("\n",""));
        Document aaaa = DocumentHelper.parseText(result);
        Element root = aaaa.getRootElement();
        Map<String,String> map = new HashMap<String, String>();
        for (Iterator<Element> it = root.elementIterator(); it.hasNext();) {
            Element element = it.next();
            map.put(element.getName(),element.getText());

        }

        TwPayRequestData bbbb = new ObjectMapper().convertValue(map, TwPayRequestData.class);


        result.getBytes();
    }


}
