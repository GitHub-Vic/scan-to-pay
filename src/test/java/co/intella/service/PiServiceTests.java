package co.intella.service;

import co.intella.domain.pi.*;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author Andy Lin
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PiServiceTests {

    @Resource
    private PiService piService;

    @Test
    public void testReconciliation() throws Exception {
        PiReconciliationRequest piReconciliationRequest = new PiReconciliationRequest();
        piReconciliationRequest.setPartnerId("UE00000000000019");
        piReconciliationRequest.setPartnerKey("KrZBOCkYo3b18MdR2JrcxJFzfoVdycgh");
        piReconciliationRequest.setDate("20170801");

        String result = piService.reconciliation(piReconciliationRequest.getPartnerId(),piReconciliationRequest.getPartnerKey(),piReconciliationRequest.getDate());
        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("0000", jsonObject.get("errorCode").getAsString());
    }

    @Test
    public void testMicropay() throws Exception {

        PiMicropayRequestData piMicropayRequestData = new PiMicropayRequestData();

        piMicropayRequestData.setStoreOrderNo("PiT"+ DateTime.now().toString(SystemInstance.DATE_YYPATTERN));
        piMicropayRequestData.setAuthCode("PISNYOC3JQAGZIV3P1");
        piMicropayRequestData.setAttach("0000000001");
        piMicropayRequestData.setTotalFee("1");
        piMicropayRequestData.setMchId("INTELLA");
        piMicropayRequestData.setStoreInfo("1490520397");
        piMicropayRequestData.setBody("test item");
        piMicropayRequestData.setDeviceInfo("2");
        piMicropayRequestData.setCreateTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        String result = piService.micropay(piMicropayRequestData);

        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("0000", jsonObject.get("errorCode").getAsString());

    }

    @Test
    public void testRefund() throws Exception {

        PiRefundRequestData piRefundRequestData = new PiRefundRequestData();

        piRefundRequestData.setStoreOrderNo("76ibvw3jx40000000020");
        piRefundRequestData.setMchId("INTELLA");
        piRefundRequestData.setRefundFee("1");
        piRefundRequestData.setCreateTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));



        String result = piService.refund(piRefundRequestData);

        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("0000", jsonObject.get("errorCode").getAsString());

    }

    @Test
    public void refundOLPay() throws Exception{
        PiOLRefundRequestData req = new PiOLRefundRequestData();
        req.setPartnerId("UE000MILES");
        req.setCreatedAt(DateTime.now().toString(SystemInstance.DATE_PI_PATTERN));
        req.setOrderId("4BF0018013000002");
        req.setTxId("TX1807095048268706");
        req.setRefundmount(1);

        String result = piService.refundOLPay(req);

        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("0000", jsonObject.get("errorCode").getAsString());
    }

    @Test
    public void testSingleOrderQuery() throws Exception {

        PiSingleOrderQueryRequestData piSingleOrderQueryRequestData= new PiSingleOrderQueryRequestData();


        piSingleOrderQueryRequestData.setStoreOrderNo("76ibvw3jx40000000020");
        piSingleOrderQueryRequestData.setMchId("INTELLA");

        String result = piService.singleOrderQueryMicropay(piSingleOrderQueryRequestData);

        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("0000", jsonObject.get("errorCode").getAsString());

    }

    @Test
    public void testSingleOrderQueryOL() throws Exception {

        PiOLSingleOrderQueryRequestData req = new PiOLSingleOrderQueryRequestData();
        req.setPaymentGateway("pi_wallet");
        req.setPartnerId("UE000MILES");
        req.setPartnerKey("INTELLA");
        req.setOrderId("4BF0018012500001");

        String result = piService.singleOrderQueryOLpay(req);

        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("0000", jsonObject.get("errorCode").getAsString());

    }
}
