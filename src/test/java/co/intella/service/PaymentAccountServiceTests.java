package co.intella.service;

import co.intella.model.PaymentAccount;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author Miles
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PaymentAccountServiceTests {

    @Autowired
    private PaymentAccountService paymentAccountService;

    @Test
    public void testUpdateOrder() throws Exception {
        List<PaymentAccount> list = paymentAccountService.listByQrcode("intella");
        Assert.assertEquals(12, list.size());
    }

}
