package co.intella.service;

import co.intella.domain.contratStore.OperationHistory;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author Miles
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class OperationHistoryServiceTests {

    @Autowired
    private OperationHistoryService operationHistoryService;

    @Test
    public void testList(){
        List<OperationHistory> list = operationHistoryService.list("super");
        Assert.assertEquals(1, list.size());
    }

    @Test
    public void testSave(){
        OperationHistory operationHistory = new OperationHistory();
        operationHistory.setMerchantId("tt");
        operationHistory.setMethod("318");
        operationHistory.setServiceType("PP");

        operationHistoryService.save(operationHistory);
    }

}
