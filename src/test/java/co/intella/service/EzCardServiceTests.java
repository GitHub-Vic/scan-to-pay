package co.intella.service;

import co.intella.domain.easycard.*;
import com.google.gson.Gson;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Miles
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class EzCardServiceTests {

    @Autowired
    private EzCardService ezCardService;

    @Test
    public void testSignOn() throws Exception{

        EzCardSignOn query = new EzCardSignOn();
        query.setTerminalId("09900000");
        query.setDeviceId("01304121");

        query.setTerminalTXNNumber(getTime());
        query.setHostSerialNumber(getTime());
        query.setTime(getTime());
        query.setDate(getDate());

        EzCardSignOnResponse response = new Gson().fromJson(ezCardService.doRequest(query, ""), EzCardSignOnResponse.class);

        Assert.assertEquals("Success", response.getTxnResult());
    }

    @Test
    public void testPayment() throws Exception {

        EzCardPayment query = new EzCardPayment();
        // 099, 083
        query.setTerminalId("09900000");

        // 01304121, 01304096
        query.setDeviceId("01304121");

        String time = getTime();
        String date = getDate();

        query.setTerminalTXNNumber(time);
        query.setHostSerialNumber(time);
        query.setTime(time);
        query.setDate(date);

        query.setAmount("1");
        query.setRetry("0");
        query.setBatchNumber(getBatchNumber(date));

        EzCardPaymentResponse response = new Gson().fromJson(ezCardService.doRequest(query, ""), EzCardPaymentResponse.class);

        Assert.assertEquals("Success", response.getTxnResult());
    }

    @Test
    public void testRefund() throws Exception {

        EzCardRefund query = new EzCardRefund();
        // 099, 083
        query.setTerminalId("09900000");

        // 01304121, 01304096
        query.setDeviceId("01304121");

        String time = getTime();
        String date = getDate();

        query.setTerminalTXNNumber(time);
        query.setHostSerialNumber(time);
        query.setTime(time);
        query.setDate(date);

        query.setAmount("1");
        query.setRetry("0");
        query.setBatchNumber(getBatchNumber(date));

        EzCardPaymentResponse response = new Gson().fromJson(ezCardService.doRequest(query, ""), EzCardReserveResponse.class);

        Assert.assertEquals("Success", response.getTxnResult());
    }

    @Test
    public void testReserve() throws Exception {

        EzCardReserve query = new EzCardReserve();
        // 099, 083
        query.setTerminalId("09900000");

        // 01304121, 01304096
        query.setDeviceId("01304121");

        String time = getTime();
        String date = getDate();

        query.setTerminalTXNNumber(time);
        query.setHostSerialNumber(time);
        query.setTime(time);
        query.setDate(date);

        query.setAmount("1");
        query.setRetry("0");
        query.setBatchNumber(getBatchNumber(date));

        EzCardReserveResponse response = new Gson().fromJson(ezCardService.doRequest(query, ""), EzCardReserveResponse.class);

        Assert.assertEquals("Success", response.getTxnResult());
    }

    @Test
    public void testAutoReserve() throws Exception {

        EzCardAutoReserve query = new EzCardAutoReserve();
        // 099, 083
        query.setTerminalId("09900000");

        // 01304121, 01304096
        query.setDeviceId("01304121");

        String time = getTime();
        String date = getDate();

        query.setTerminalTXNNumber(time);
        query.setHostSerialNumber(time);
        query.setTime(time);
        query.setDate(date);

        query.setAmount("1");
        query.setRetry("0");
        query.setBatchNumber(getBatchNumber(date));

        EzCardAutoReserveResponse response = new Gson().fromJson(ezCardService.doRequest(query, ""), EzCardAutoReserveResponse.class);

        Assert.assertEquals("Success", response.getTxnResult());
    }

    @Test
    public void testCheckout() throws Exception {

        EzCardBatchCheckout query = new EzCardBatchCheckout();
        // 099, 083
        query.setTerminalId("09900000");

        // 01304121, 01304096
        query.setDeviceId("01304121");

        String time = getTime();
        String date = getDate();

        query.setTerminalTXNNumber(time);
        query.setHostSerialNumber(time);
        query.setTime(time);
        query.setDate(date);
        query.setTotalTXNCount("10");
        query.setTotalAmount("10");

        query.setBatchNumber(getBatchNumber(date));

        EzCardBatchCheckoutResponse response = new Gson().fromJson(ezCardService.doRequest(query, ""), EzCardBatchCheckoutResponse.class);

        Assert.assertEquals("Success", response.getTxnResult());
    }

    @Test
    public void testGetCardInfo() throws Exception {

        EzCardInfoQuery query = new EzCardInfoQuery();
        // 099, 083
        query.setTerminalId("09900000");

        // 01304121, 01304096
        query.setDeviceId("01304121");

        String time = getTime();
        String date = getDate();

        query.setTerminalTXNNumber(time);
        query.setHostSerialNumber(time);
        query.setTime(time);
        query.setDate(date);

        EzCardInfoQueryResponse response = new Gson().fromJson(ezCardService.doRequest(query, ""), EzCardInfoQueryResponse.class);

        Assert.assertEquals("Success", response.getTxnResult());
    }

    @Test
    public void testGetTradeInfo() throws Exception {

        EzCardTradeQuery query = new EzCardTradeQuery();
        // 099, 083
        query.setTerminalId("09900000");

        // 01304121, 01304096
        query.setDeviceId("01304121");

        String time = getTime();
        String date = getDate();

        query.setTerminalTXNNumber(time);
        query.setHostSerialNumber(time);
        query.setTime(time);
        query.setDate(date);
        query.setBeforeOrAfter("0");

        EzCardTradeQueryResponse response = new Gson().fromJson(ezCardService.doRequest(query, ""), EzCardTradeQueryResponse.class);

        Assert.assertEquals("Success", response.getTxnResult());
    }

    @Test
    public void testGetCardId() throws Exception {

        EzCardIdQuery query = new EzCardIdQuery();
        // 099, 083
        query.setTerminalId("09900000");

        // 01304121, 01304096
        query.setDeviceId("01304121");

        String time = getTime();
        String date = getDate();

        query.setTerminalTXNNumber(time);
        query.setHostSerialNumber(time);
        query.setTime(time);
        query.setDate(date);

        EzCardIdQueryResponse response = new Gson().fromJson(ezCardService.doRequest(query, ""), EzCardIdQueryResponse.class);

        Assert.assertEquals("Success", response.getTxnResult());
    }

    @Test
    public void testGetLogs() throws Exception {

        EzCardLogQuery query = new EzCardLogQuery();
        // 099, 083
        query.setTerminalId("09900000");

        // 01304121, 01304096
        query.setDeviceId("01304121");

        String time = getTime();
        String date = getDate();

        query.setTerminalTXNNumber(time);
        query.setHostSerialNumber(time);
        query.setTime(time);
        query.setDate(date);
        query.setQueryTime(time);
        query.setQueryDate(date);

        EzCardLogQueryResponse response = new Gson().fromJson(ezCardService.doRequest(query, ""), EzCardLogQueryResponse.class);

        Assert.assertEquals("Success", response.getTxnResult());
    }

    private String getDate() {
        DateTime dateTime = DateTime.now();
        return String.valueOf(dateTime.getYear())
                + String.format("%02d", dateTime.getMonthOfYear())
                + String.format("%02d", dateTime.getDayOfMonth());
    }

    private String getTime() {
        DateTime dateTime = DateTime.now();
        return String.format("%02d", dateTime.getHourOfDay())
                + String.format("%02d", dateTime.getMinuteOfHour())
                + String.format("%02d", dateTime.getSecondOfMinute());
    }

    private String getBatchNumber(String date) {
        return date.substring(2, date.length()) + "00";
    }
}
