package co.intella.service;

import co.intella.model.Merchant;
import co.intella.model.QrcodeParameter;
import co.intella.model.Recipient;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author Miles Wu
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RecipientServiceTests {

    @Resource
    private RecipientService recipientService;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private MerchantService merchantService;
    @Test
    public void testSave() {
        Recipient recipient = new Recipient();
        recipient.setSerialId("27369481698372694871");
        recipientService.save(recipient);

        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId("intella");

        recipient = new Recipient();
        recipient.setSerialId("BISNT507008D");
        recipient.setQrcodeParameter(qrcodeParameter);
        Recipient entity = recipientService.save(recipient);

        Assert.assertEquals("BISNT507008F", entity.getSerialId());
    }
    @Test
    public void bensonTestSave() {
        Recipient recipient = new Recipient();
        recipient.setSerialId("27369481698372694875");
        recipientService.save(recipient);

        Merchant merchantPid=merchantService.getOne("u001");

        recipient = new Recipient();
        recipient.setMerchant(merchantPid);
        recipient.setSerialId("BISNT507008F");
        Recipient entity = recipientService.save(recipient);

        Assert.assertEquals("BISNT507008F", entity.getSerialId());
    }
    @Test
    public void testMchSave() {
        Merchant merchant = new Merchant();
        merchant.setName("benson");
        merchant.setEmail("benson@test.com");
        merchant.setCreator((long)1);
        merchant.setPin("test");
        merchant.setAccountId("hello");
        merchant.setComId("888888");
        merchant.setPhone("12345678");
        merchant.setPaycodeToken("9ujfjwiope");
//        merchant.setRefundPin("0000");
        merchant.setStatus("active");

        Merchant entity = merchantService.save(merchant);

        Assert.assertEquals("benson", entity.getName());
    }
    @Test
    public void testGetOne(){

    }
}
