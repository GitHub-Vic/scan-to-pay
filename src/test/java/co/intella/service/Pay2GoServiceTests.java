package co.intella.service;

import co.intella.crypto.AesCryptoUtil;
import co.intella.domain.pay2go.Pay2GoRefundResult;
import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import co.intella.utility.GeneralUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

/**
 * @author Alex
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class Pay2GoServiceTests {
    @Autowired
    private Pay2GoService pay2GoService;

    @Autowired
    private PaymentAccountService paymentAccountService;

    @Autowired
    private TradeDetailService tradeDetailService;

    @Autowired
    private Environment env;

    @Test
    public void testReserve() throws Exception{

        PaymentAccount paymentAccount = paymentAccountService.getOne("super",24);
        TradeDetail tradeDetail = tradeDetailService.getOne("super","4BF0018071700004");

        String result = pay2GoService.prepareRefund(paymentAccount,tradeDetail);
//        result = pay2GoService.refund(result);
//
//        result.getBytes();
        result="{\"Status\":\"SUCCESS\",\"Version\":\"2.0\",\"MerchantID\":\"PG300000004668\",\"RefundInfo\":\"f9e15055bc35c78d583277acfc39c0563e10f6c8d6d04064f8a8c2016b8720a93cbfc05493999889062c4804661904565596799db8cde9ef4c7582dd45c206c166d7ceb68f7b60ba34b629b36107d0088211ae842ce8b528e0b3db4ac9ed58e481873fb824cf1fb53a9a4a244fb26e2b3782a3e99108fece6fe578d994a353fdda4b2df027760aba8bbe246ca4c81b3272da4d7028c9d03e58ec95b0753cd97ff03307522c21061b4e184e9f14b95abcdd889ac1a96764b36fee9437741ad98a06e51f7a4adfc5f0240f6b0933d9055eba89f627c7a7d827077b5c39c2670b823ebb0ea361e41e242bf9d96a57da63c7edc809555928854ad0bdefd05459b653a69bb3bd6b0fa07b4feacd562a1642f5aec8eb8e6c6574fd05b66a61be5f9ac104f4ebeca5828bcdb8fa17a55510cb80c69defc5cb0ba98ebd23e000a42517d0aef6d6aaef15369ff1e28a702bcd9f1475c6bfe383f2db9c2b08d360bbf776ceaaaffcc85ecd0a7c850efaba65e399965ff93ea996fc2a0a2b0990478b14cdde\",\"RefundSha\":\"408141E725EB5A4FA8CADECF24AF710A2E1E90692A65E4542A7C57CDF8DB5171\"}";
        JsonObject jsonObject = new Gson().fromJson(result,JsonObject.class);

        String status = jsonObject.get("Status").getAsString();

        String refundInfo= decryptRefund(jsonObject.get("RefundInfo").getAsString(),paymentAccount);

        JsonObject jsonObject2 = new Gson().fromJson(refundInfo,JsonObject.class);


        Pay2GoRefundResult refundResult1 = new Gson().fromJson(jsonObject2.get("Result").getAsString(),Pay2GoRefundResult.class);



    }
    private String decryptRefund(String refundInfo, PaymentAccount paymentAccount) {
        SecretKey secretKey =  AesCryptoUtil.convertSecretKey(paymentAccount.getHashKey().getBytes());
        try {
            return aesDecrypt(secretKey, paymentAccount.getHashIV().getBytes(), GeneralUtil.hexStringToByteArray(refundInfo));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String aesDecrypt(SecretKey secretKey, byte[] iv, byte[] encryptText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
        cipher.init(2, secretKey, new IvParameterSpec(iv));
        byte[] decryptedText = cipher.doFinal(encryptText);
        return new String(decryptedText).trim();
    }
}
