package co.intella.service;

import co.intella.model.Merchant;
import co.intella.model.PaymentAccount;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author Andy Lin
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DataBaseUtillityTest {

    @Autowired
    private PaymentAccountService paymentAccountService;

    @Test
    public void testPaymentAccountListByQrcode() {
        List<PaymentAccount> list = paymentAccountService.listByQrcode("vhenq1oi65a");

        int a = 1;
    }
}
