package co.intella.service;

import co.intella.domain.jkos.JkosMicropayRequestData;
import co.intella.domain.jkos.JkosRefundRequestData;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Alex
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class JkosServiceTests {
    @Autowired
    private JkosService jkosService;

    @Test
    public void testMicropay() throws Exception {
        JkosMicropayRequestData requestData = new JkosMicropayRequestData();

        requestData.setMerchantId("54390379");
        requestData.setStoreId("000000000000001");
        requestData.setStoreName("Intella");
        requestData.setGatewayTradeNo("");
        requestData.setMerchantTradeNo("intellajk001");
        requestData.setPosId("1");
        requestData.setPosTradeTime(DateTime.now().toString("yyyy/MM/dd HH:mm:ss"));
        requestData.setCardToken("22J111111111111111");
        requestData.setTradeAmount(200);
        //requestData.setUnRedeem(0);
        requestData.setRemark("remark");
        requestData.setExtra1("abc");
        requestData.setExtra2("");
        requestData.setExtra3("");
        requestData.setSendTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        requestData.setUnRedeem(0);
        requestData.setSign("");

//        requestData.setMerchantId("54390379");
//        requestData.setStoreId("000000000000001");
//        requestData.setStoreName("Welcome");
//        requestData.setGatewayTradeNo("");
//        requestData.setMerchantTradeNo("2343565453423421");
//        requestData.setPosId("1");
//        requestData.setPosTradeTime("2016/05/08 12:01:01");
//        requestData.setCardToken("22J111111111111111");
//        requestData.setTradeAmount(200);
//        //requestData.setUnRedeem(0);
//        requestData.setRemark("remark");
//        requestData.setExtra1("abc");
//        requestData.setExtra2("");
//        requestData.setExtra3("");
//        requestData.setSendTime("20160805120103");
//        requestData.setSign("");


        String result = jkosService.micropay(requestData,"50673DE464A74B828FAA5E1FB400A1B8");

        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("7000", jsonObject.get("Header").getAsJsonObject().get("StatusCode").getAsString());

    }

    @Test
    public void testRefund() throws Exception {
        JkosRefundRequestData requestData = new JkosRefundRequestData();

        requestData.setMerchantId("9999999");
        requestData.setStoreId("1");
        requestData.setStoreName("Intella");
        requestData.setGatewayTradeNo("");
        requestData.setMerchantTradeNo("intellajk001");
        requestData.setPosId("1");
        requestData.setPosTradeTime("2017/03/13 08:00:00");
        requestData.setTradeNo("P000031011707110001");
        requestData.setTradeAmount(200);
        //requestData.setUnRedeem(0);
        requestData.setRemark("remark");
        requestData.setExtra1("abc");
        requestData.setExtra2("");
        requestData.setExtra3("");
        requestData.setSendTime("20170712160000");
        requestData.setSign("");


        String result = jkosService.refund(requestData,"50673DE464A74B828FAA5E1FB400A1B8");

        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("7000", jsonObject.get("Header").getAsJsonObject().get("StatusCode").getAsString());

    }
}
