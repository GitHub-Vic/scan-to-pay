package co.intella.service;

import co.intella.domain.contratStore.MerchantIncome;
import co.intella.model.Device;
import co.intella.model.Merchant;
import co.intella.model.PaymentAccount;
import co.intella.utility.GeneralUtil;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.util.Random;
import java.util.UUID;

/**
 * @author Alex
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CreateMerchantTests {

    @Resource
    private MerchantService merchantService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private BankService bankService;

    @Test
    public void createMerchant() {

        Merchant newAccount = new Merchant();

        newAccount.setName("Batch"+new Random().nextInt(10000));
        newAccount.setEmail("miles@intella.co");
        newAccount.setCreator(Long.getLong("0"));
        newAccount.setPin(GeneralUtil.getHash("0000"));
        newAccount.setAccountId("tttttttt");
        newAccount.setComId("123456");
        newAccount.setPhone("001");
        newAccount.setSignDate(DateTime.now().toString("yyyyMMddHHmmss"));
        newAccount.setStatus("active");
        newAccount.setPaycodeToken("XXXX");
        String activateToken = UUID.randomUUID().toString().substring(0, 15);
        newAccount.setActivateToken(activateToken);
        newAccount.setRefundPin("0000");
        newAccount = merchantService.create(newAccount);


        savePayment(newAccount,"103543903790001",1);
        savePayment(newAccount,"54390379101",2);
        savePayment(newAccount,"MWD",3);
        savePayment(newAccount,"0100829354",4);
        savePayment(newAccount,"1812526",5);
        savePayment(newAccount,"000812770060201",8);
        savePayment(newAccount,"3027720",10);
        savePayment(newAccount,"42343041201",11);

        savaDevice(newAccount,"2000132999","ECpay-001");
        savaDevice(newAccount,"23412342","Allpay-001");
        savaDevice(newAccount,"T0000000","TS-001");
//        savaDevice(newAccount,"A6113004","Ali");
//        savaDevice(newAccount,"P1","super-001");
        savaDevice(newAccount,"skb0001","super-001");






        //return new ResponseEntity<String>("Hello Intella", HttpStatus.OK);

    }

    private void savaDevice(Merchant newAccount, String deviceId, String type) {
        Device device = new Device();

        device.setName(null);
        device.setDescription(null);
        device.setOwner(newAccount);
        device.setOwnerDeviceId(deviceId);
        device.setType(type);


        deviceService.save(device);
    }

    private void savePayment(Merchant newAccount, String account, int i) {
        PaymentAccount paymentAccount  = new PaymentAccount();

        paymentAccount.setDescription("");
        paymentAccount.setAccount(account);
        paymentAccount.setBank(bankService.getOne(i));
        paymentAccount.setMerchant(newAccount);
        paymentAccount.setDefault(true);

        PaymentAccount entity = paymentAccountService.save(paymentAccount);
    }
}
