package co.intella.service;

import co.intella.model.ConsumerToken;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Miles
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ConsumerTokenServiceTests {

    @Autowired
    private ConsumerTokenService consumerTokenService;

    @Test
    public void testGetOne(){
        ConsumerToken consumerToken = consumerTokenService.getOne((long) 123000001);
        Assert.assertNotNull(consumerToken);
        Assert.assertEquals("1234123412341234", consumerToken.getToken());
    }

    @Test
    public void testCreateOrUpdate() {
        ConsumerToken consumerToken = new ConsumerToken();
        consumerToken.setAccountId("miles1");
        consumerToken.setAlgorithm("001001001");
        consumerToken.setToken("sdkldfhaskdfkajsgdfjk");
        consumerToken.setKeys("21a3s54d65a4ds654a6sdasd");
        ConsumerToken result = consumerTokenService.createOrUpdate(consumerToken);

        Assert.assertNotNull(result);
        Assert.assertEquals("sdkldfhaskdfkajsgdfjk", result.getToken());
    }
}
