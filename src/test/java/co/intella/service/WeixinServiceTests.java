package co.intella.service;

import co.intella.domain.ali.AliRequestBody;
import co.intella.domain.weixin.*;
import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author Miles
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class WeixinServiceTests {

    @Autowired
    private WeixinService weixinService;

    @Resource
    private AliService aliService;

    @Test
    public void AliJSAPITest() throws Exception{


        WeixinJsapiRequestData weixinJsapiRequestData = new WeixinJsapiRequestData();

        weixinJsapiRequestData.setVer("101");
        weixinJsapiRequestData.setMchId("54390379101");
        weixinJsapiRequestData.setMethod("12");
        weixinJsapiRequestData.setNonceStr(null);

        weixinJsapiRequestData.setTimeExpire("20180419144734");
        weixinJsapiRequestData.setTradeType("JSAPI");
        weixinJsapiRequestData.setOutTradeNo("dgh7jyeh8000");
        weixinJsapiRequestData.setBody("aaa");
        weixinJsapiRequestData.setFeeType("TWD");
        weixinJsapiRequestData.setTotalFee("1");
        weixinJsapiRequestData.setDetail("zzzz");
        weixinJsapiRequestData.setDeviceInfo("skb0001");


        String platformURL = aliService.callJsapi(weixinJsapiRequestData, AliRequestBody.REFUND);

        platformURL.getBytes();

    }

    @Test
    public void testUpdateOrder() throws Exception {

        WeixinSingleQueryRequestData requestData = new WeixinSingleQueryRequestData();
        requestData.setVer("101");
        requestData.setMethod("10");
        requestData.setMchId("103543903790001");
        requestData.setNonceStr("nonce_str");
        //WeixinOrderQueryRequestData
        requestData.setTradeType("UPDATESKORDER");
        requestData.setOutTradeNo("mock001");

        String result = weixinService.updateOrder(requestData);
        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("0000", jsonObject.get("Header").getAsJsonObject().get("StatusCode").getAsString());
    }

    @Test
    public void testSingleOrderQuery() throws Exception {
        WeixinSingleQueryRequestData requestData = new WeixinSingleQueryRequestData();
        requestData.setVer("101");
        requestData.setMethod("10");
        requestData.setMchId("103543903790001");
        requestData.setNonceStr("nonce_str");
        requestData.setTradeType("ORDERQUERY");
        requestData.setOutTradeNo("mock001");

        String result = weixinService.singleQuery(requestData);
        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("0000", jsonObject.get("Header").getAsJsonObject().get("StatusCode").getAsString());
    }

    @Test
    public void testRefund() throws Exception {

        WeixinRefundRequestData requestData = new WeixinRefundRequestData();
        requestData.setVer("101");
        requestData.setMethod("10");
        requestData.setMchId("103543903790001");
        requestData.setNonceStr("nonce_str");

        requestData.setTradeType("REFUND");
        requestData.setOutTradeNo("mock001");
        requestData.setDeviceInfo("skb0001");
        requestData.setTotalFee("1");
        requestData.setRefundFee("1");
        requestData.setTransactionId("mock001");
        requestData.setOutRefundNo("mock001");
        requestData.setRefundFeeType("TWD");

        String result = weixinService.refund(requestData);
        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("7113", jsonObject.get("Header").getAsJsonObject().get("StatusCode").getAsString());
    }

    @Test
    public void testOrderQuery() throws Exception {

        WeixinOrderQueryRequestData requestData = new WeixinOrderQueryRequestData();
        requestData.setVer("101");
        requestData.setMethod("10");
        requestData.setMchId("103543903790001");
        requestData.setNonceStr("nonce_str");
        requestData.setTradeType("ORDERQUERY");
        requestData.setDeviceInfo("skb0001");

        requestData.setStartDate("2017-07-01");
        requestData.setEndDate("2017-07-02");

        String result = weixinService.orderQuery(requestData);
        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("7002", jsonObject.get("Header").getAsJsonObject().get("StatusCode").getAsString());
    }

    @Test
    public void testMicroPay() throws Exception {
        WeixinMicropayRequestData requestData = new WeixinMicropayRequestData();
        requestData.setVer("101");
        requestData.setMethod("10");
        requestData.setMchId("103543903790001");
        requestData.setNonceStr("nonce_str");
        requestData.setOutTradeNo("mock001");
        requestData.setTradeType("MICROPAY");
        requestData.setDeviceInfo("skb0001");
        requestData.setTotalFee("1");
        requestData.setAuthCode("1300001142368816706");
        requestData.setBody("mock");

        String result = weixinService.micropay(requestData);
        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("7000", jsonObject.get("Header").getAsJsonObject().get("StatusCode").getAsString());
    }

    @Test
    public void testCallJsApiNoReturn() throws Exception {
        WeixinJsapiRequestData requestData = new WeixinJsapiRequestData();
        requestData.setVer("101");
        requestData.setMethod("10");
        requestData.setMchId("103543903790001");
        requestData.setNonceStr("nonce_str");
        requestData.setTimeExpire("2017070612000000");

        requestData.setTradeType("JSAPI");
        requestData.setOutTradeNo("mock001");
        requestData.setTotalFee("1");
        requestData.setDeviceInfo("deviceinfo1");

        String result = weixinService.callJsApiNoReturn(requestData);
        Assert.assertEquals("https://o2oapi.sk2pay.com/pay/JsapiGateway.aspx?s=103543903790001&m=OL&b=201612070080&c=TWD&a=1&p=&d=3qou08suTZ9ICxig7fuZmjC6efXq9MD5k8ViGxcCoPQ9iPXm0SI4MGDy7RPmeSN4ZNeG7%2bl3GWndFacPIOsxIVmj8TCpUFGQdJ%2fKqWIa60mwagzmiH%2fLrFEiIIJ%2bJjjoGUON%2fcdCorpIxYizhc5mAkYxDQzX9zY9DHgKjAF9HfKdBgtcfwfOlOFIcDdaCeyuJjsAaQGcORrlago2VASq4hJVTQB0GotPVdDNz6DzgInNRVmlVxd2uXL18UIzD8CL&h=tQ6MlAPPajrFBJcZ%2fZ3r5hFkDRllYywdOdIfCC18F2c%3d&r=Y&t=2017-07-06+12%3a00%3a00&", result);

    }

    @Test
    public void testCallJsApi() throws Exception {
        WeixinJsapiRequestData requestData = new WeixinJsapiRequestData();
        requestData.setVer("101");
        requestData.setMethod("10");
        requestData.setMchId("103543903790001");
        requestData.setNonceStr("nonce_str");
        requestData.setTimeExpire("2017070612000000");
        requestData.setTradeType("JSAPI");
        requestData.setOutTradeNo("mock001");
        requestData.setBody("mock1");
        requestData.setFeeType("TWD");
        requestData.setTotalFee("1");
        requestData.setDetail("detail");
        requestData.setDeviceInfo("deviceinfo1");

        String result = weixinService.callJsApi(requestData);
        Assert.assertEquals("<br/><fontsize='1'><tableclass='xdebug-errorxe-warning'dir='ltr'border='1'cellspacing='0'cellpadding='1'><tr><thalign='left'bgcolor='#f57900'colspan=\"5\"><spanstyle='background-color:#cc0000;color:#fce94f;font-size:x-large;'>(!)</span>Warning:Invalidargumentsuppliedforforeach()in/home/alex/Sites/intella-allpaypass/lib/AllPay.Shinkong.phponline<i>183</i></th></tr><tr><thalign='left'bgcolor='#e9b96e'colspan='5'>CallStack</th></tr><tr><thalign='center'bgcolor='#eeeeec'>#</th><thalign='left'bgcolor='#eeeeec'>Time</th><thalign='left'bgcolor='#eeeeec'>Memory</th><thalign='left'bgcolor='#eeeeec'>Function</th><thalign='left'bgcolor='#eeeeec'>Location</th></tr><tr><tdbgcolor='#eeeeec'align='center'>1</td><tdbgcolor='#eeeeec'align='center'>0.0000</td><tdbgcolor='#eeeeec'align='right'>230936</td><tdbgcolor='#eeeeec'>{main}()</td><tdtitle='/home/alex/Sites/intella-allpaypass/allpaypass/index.php'bgcolor='#eeeeec'>.../index.php<b>:</b>0</td></tr><tr><tdbgcolor='#eeeeec'align='center'>2</td><tdbgcolor='#eeeeec'align='center'>0.0175</td><tdbgcolor='#eeeeec'align='right'>855632</td><tdbgcolor='#eeeeec'>ShinKongApi::jsapiPay()</td><tdtitle='/home/alex/Sites/intella-allpaypass/allpaypass/index.php'bgcolor='#eeeeec'>.../index.php<b>:</b>263</td></tr></table></font>https://o2oapi.sk2pay.com/pay/JsapiGateway.aspx?s=103543903790001&m=OL&b=201612070080&c=TWD&a=1&p=mock1&d=3qou08suTZ9ICxig7fuZmjC6efXq9MD5k8ViGxcCoPQ9iPXm0SI4MGDy7RPmeSN4ZNeG7%2bl3GWndFacPIOsxIVmj8TCpUFGQdJ%2fKqWIa60mwagzmiH%2fLrFEiIIJ%2bJjjoGUON%2fcdCorpIxYizhc5mAkYxDQzX9zY9DHgKjAF9HfKdBgtcfwfOlOFIcDdaCeyuJjsAaQGcORrlago2VASq4j3j%2fCG%2bgz97kfVVuRMgMN%2b6GeSIlKNP50eATdPi%2fjhVDqAVjcXMvwW9shkhbJ5omA%3d%3d&h=dREcgHigmlt1yEyA1FTbJGtf2EDV2%2fT0W%2bF8PXDUVKI%3d&r=Y&t=2017-07-06+12%3a00%3a00&", result);

    }

    @Test
    public void testFtf() throws Exception {
        WeixinFtfRequestData requestData = new WeixinFtfRequestData();
        requestData.setVer("101");
        requestData.setMethod("10");
        requestData.setMchId("103543903790001");
        requestData.setNonceStr("nonce_str");
        requestData.setTime_expire("20170706000000");
        requestData.setTrade_type("FTF");

        String result = weixinService.faceToFace(requestData);
        Assert.assertEquals("https://o2oapi.sk2pay.com/pay/JsapiGateway.aspx?s=103543903790001&m=FTF&b=201612130090&d=JZ5WvuJbbaqW%2b7lOv6AclA%2bntnhhlSdwYlvUqVizrcGsnJkK89zS0V%2bcg4dr1ljnELRiDr35KiRnN5r9dc8UyA%3d%3d&h=3HfUG9xb77a%2f9%2fvhvaxuXsg3vJRY5TMkMHvo78h2BJk%3d&r=Y&", result);
    }

    @Test
    public void testPayment() throws Exception {

        WeixinRequestParameter requestData = new WeixinRequestParameter();
        requestData.setP("103543903790001");
        requestData.setR("nonce_str");
        requestData.setP2("outtradeno00001");
        requestData.setP1("deviceinfo0001");
        requestData.setP3("1");

        String result = weixinService.payment(requestData);
        Assert.assertEquals("https://o2oapi.sk2pay.com/pay/JsapiGateway.aspx?s=103543903790001&m=OL&b=201612070080&c=TWD&a=1&p=&d=3qou08suTZ9ICxig7fuZmoi3XjXSdlb0n64q78Stj1BM8KgCBCb26RSqih5UQrlMCxJl7Pg0ARFfnSIi4o%2bExd9y%2bpigxng75L%2boSiFE4B%2fOxazURFpOopsAsktTRS0tp4O5zIMKyiDeGgEr4SItp%2bag0UYl%2fQf%2fovXmdfQSQA16a7ILEb1G%2bKdCnHXSk0K1OcUmimErv%2bOPYwnPpupayh%2btaBbmi5ULfr6V%2fvE%2fIvRaHhSo3XMZio8jsoJIyuRG&h=6DTi5BDaT%2bdAhhufAbsH6t1e9Xh5Y60Tpuhd45vNeXo%3d&r=Y&t=--+%3a%3a&", result);
    }

    @Test
    public void testRateQuery() throws Exception {

        WeixinCurrencyRequestData requestData = new WeixinCurrencyRequestData();
        requestData.setVer("101");
        requestData.setMethod("10");
        requestData.setCurrency("TWD");
        requestData.setMchId("103543903790001");
        requestData.setNonceStr("nonce_str");
        requestData.setExchangeCurrency("USD");
        requestData.setTradeType("EXCHANGECURRENCY");

        String result = weixinService.rateQuery(requestData);
        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("0000", jsonObject.get("Header").getAsJsonObject().get("StatusCode").getAsString());
    }


}
