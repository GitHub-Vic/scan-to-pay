package co.intella.service;

import co.intella.model.Device;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

/**
 * @author Miles
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DeviceServiceTests {

    @Autowired
    private DeviceService deviceService;

    @Test
    public void testSave() {
        Device device = deviceService.getOne(UUID.fromString("1CFF7379-E0A5-11E7-B71C-42010A8C0126"));

        device.setDescription("ezc-test");
        device.setBan("1");
        deviceService.save(device);
    }

}
