package co.intella.service;

import co.intella.model.CaptureOrderDetail;
import co.intella.utility.SystemInstance;
import com.ibm.icu.util.Calendar;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Miles
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CaptureOrderServiceTests {

    @Resource
    private  CaptureService captureService;

    @Autowired
    private CaptureOrderDetailService captureOrderDetailService;

    @Test
    public void testAlipayCapture() throws Exception {
        String mchId = "super";
        String endDate = convertDateFormat(getNowTime(), 8);
        String startDate = convertDateFormat(dateCalculate(-60, endDate), 8);

        List<CaptureOrderDetail> list = captureOrderDetailService.listSettleCaptureOrder(mchId, startDate+ "230000", endDate+ "225959", "10220");
        for (CaptureOrderDetail elem : list) {
            elem.setStatus("wait");
            captureOrderDetailService.save(elem);
        }

        String rsp = captureService.alipayCapture(mchId, startDate, endDate);

//        Assert.assertEquals("2000", rsp);
        Assert.assertEquals("0000", rsp);
    }

    @Test
    public void testAllpayCapture() throws Exception {
        String mchId = "super";
        String endDate = convertDateFormat(getNowTime(), 8);
        String startDate = convertDateFormat(dateCalculate(-30, endDate), 8);

        List<CaptureOrderDetail> list = captureOrderDetailService.listSettleCaptureOrder(mchId, startDate+ "230000", endDate+ "225959", "10500");
        for (CaptureOrderDetail elem : list) {
            elem.setStatus("wait");
            captureOrderDetailService.save(elem);
        }

        String rsp = captureService.allpayCapture(mchId, startDate, endDate);

//        Assert.assertEquals("2000", rsp);
        Assert.assertEquals("0000", rsp);
    }

    @Test
    public void testTsbCapture() throws Exception {
        String mchId = "super";
        String endDate = convertDateFormat(getNowTime(), 8);
        String startDate = convertDateFormat(dateCalculate(-120, endDate), 8);

        List<CaptureOrderDetail> list = captureOrderDetailService.listSettleCaptureOrder(mchId, startDate+ "230000", endDate+ "225959", "20800");
        for (CaptureOrderDetail elem : list) {
            elem.setStatus("wait");
            captureOrderDetailService.save(elem);
        }

        String rsp = captureService.tsbCapture(mchId, startDate, endDate);

//        Assert.assertEquals("2000", rsp);
        Assert.assertEquals("0000", rsp);
    }

    @Test
    public void testWeixinCapture() throws Exception {
        String mchId = "super";
        String endDate = convertDateFormat(getNowTime(), 8);
        String startDate = convertDateFormat(dateCalculate(-14, endDate), 8);

        List<CaptureOrderDetail> list = captureOrderDetailService.listSettleCaptureOrder(mchId, startDate+ "230000", endDate+ "225959", "10110");
        for (CaptureOrderDetail elem : list) {
            elem.setStatus("wait");
            captureOrderDetailService.save(elem);
        }

        String rsp = captureService.weixinCapture(mchId, startDate, endDate);

//        Assert.assertEquals("2000", rsp);
        Assert.assertEquals("0000", rsp);
    }

    @Test
    public void testTcbCapture() throws Exception {
        String mchId = "lafresh";
        String endDate = convertDateFormat(getNowTime(), 8);
        String startDate = convertDateFormat(dateCalculate(-14, endDate), 8);

        List<CaptureOrderDetail> list = captureOrderDetailService.listSettleCaptureOrder(mchId, startDate+ "230000", endDate+ "225959", "20400");
        for (CaptureOrderDetail elem : list) {
            elem.setStatus("wait");
            captureOrderDetailService.save(elem);
        }

        String rsp = captureService.tcbCapture(mchId, startDate, endDate);

//        Assert.assertEquals("2000", rsp);
        Assert.assertEquals("0000", rsp);
    }

    @Test
    public void testTcbCaptureResult() throws Exception {
        String mchId = "0100829354";
        String endDate = convertDateFormat(getNowTime(), 8);
//        String startDate = convertDateFormat(dateCalculate(-14, endDate), 8);
        String startDate = "20170616";
        String rsp = captureService.tcbCaptureResult(mchId, startDate, endDate);

//        Assert.assertEquals("2000", rsp);
        Assert.assertEquals("0000", rsp);
    }

    @Test
    public void testList(){
        List<CaptureOrderDetail> list = captureOrderDetailService.listByMerchantIdAndDate("super", "20170605170000", "20170605180254", "10500");
    }

    @Test
    public void testListAll(){
        List<CaptureOrderDetail> list = captureOrderDetailService.listAll();
    }

    private String convertDateFormat(String dateTime, int to) {
        String pattern1 = dateFormatParameter(dateTime.length());
        String pattern2 = dateFormatParameter(to);

        SimpleDateFormat sdf1 = new SimpleDateFormat(pattern1);
        SimpleDateFormat sdf2 = new SimpleDateFormat(pattern2);
        String str = "";
        try {
            Date d = sdf1.parse(dateTime);
            str = sdf2.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private String dateCalculate(int days, String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date dt = sdf.parse(date);
        Calendar cd = Calendar.getInstance();
        cd.setTime(dt);
        cd.add(Calendar.DAY_OF_YEAR,days);
        Date dt1 = cd.getTime();
        String str = sdf.format(dt1);

        return str;
    }

    private String dateFormatParameter(int ref) {
        String pattern = "";
        if(ref == 14) {
            pattern = SystemInstance.DATE_PATTERN;
        }else if(ref == 12) {
            pattern = "yyyyMMddhhmm";
        }else if(ref == 8) {
            pattern = "yyyyMMdd";
        }else if(ref == 6) {
            pattern = "yyMMdd";
        }
        return pattern;
    }

    private String getNowTime() {
        SimpleDateFormat sdf1 = new SimpleDateFormat(SystemInstance.DATE_PATTERN);
        String str = "";
        str = sdf1.format(new Date());
        return str;
    }
}
