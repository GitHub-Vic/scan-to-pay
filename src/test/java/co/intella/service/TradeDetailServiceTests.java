package co.intella.service;

import co.intella.domain.contratStore.CustomizePageRequest;
import co.intella.domain.contratStore.TradeDetailVo;
import co.intella.model.TradeDetail;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author Miles Wu
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TradeDetailServiceTests {


    @Autowired
    private TradeDetailService tradeDetailService;

    @Test
    public void testCountBy() {
        CustomizePageRequest request = new CustomizePageRequest();
        request.setAccountId("super");
        request.setStartDate("20170401000000");
        request.setEndDate("20170631000000");
        long sum = tradeDetailService.countPaymentByCreateDate(request);

        Assert.assertEquals(1623, sum);
    }

    @Test
    public void testPageable() {
        CustomizePageRequest request = new CustomizePageRequest();
        request.setAccountId("super");
        request.setStartDate("20171001000000");
        request.setEndDate("20171111000000");
        request.setPageSize(10);
        request.setIndex(0);
        List<TradeDetail> list = tradeDetailService.pageList(request);

        Assert.assertEquals(10, list.size());
    }

    @Test
    public void testListByUserId() {
        List<TradeDetailVo> list = tradeDetailService.listByUserId("super", "1467704304");

        Assert.assertEquals(10, list.size());
    }

    @Test
    public void testListByBatch() {
        List<TradeDetailVo> list = tradeDetailService.listByBatchNo("super", "01304098", "17122140");

        Assert.assertEquals(2, list.size());
    }

}
