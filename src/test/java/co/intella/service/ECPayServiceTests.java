package co.intella.service;

import co.intella.domain.ecpay.*;
import co.intella.utility.StatusManipulation;
import co.intella.utility.SystemInstance;
import com.google.common.base.Utf8;
import org.joda.time.LocalDateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.servlet.ModelAndView;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Miles
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ECPayServiceTests {

    @Autowired
    private ECPayService ecPayService;

    @Autowired
    private Environment env;

    @Test
    public void testVerifyNotification() throws Exception {
//        String response = "CustomField1=&CustomField2=&CustomField3=&CustomField4=&MerchantID=2000132"+
//                "&MerchantTradeNo=OL220170817143016&PaymentDate=2017%2F08%2F17+14%3A30%3A45"+
//                "&PaymentType=Credit_CreditCard&PaymentTypeChargeFee=1&RtnCode=1"+
//                "&RtnMsg=%E4%BA%A4%E6%98%93%E6%88%90%E5%8A%9F&SimulatePaid=0&StoreID=&TradeAmt=1"+
//                "&TradeDate=2017%2F08%2F17+14%3A30%3A17&TradeNo=1708171430178200"+
//                "&CheckMacValue=164C55D6758444BD25232E11031E9BAD96BD4EA0F5BD144C793B2B43218FD433";
//        Map<String, String> result = ecPayService.verifyNotification(response);

//          String responseFullInfo = "CustomField1=&CustomField2="+
//                "&CustomField3=&CustomField4=&MerchantID=2000132"+
//                "&MerchantTradeNo=OL220170817113802&PaymentDate=2017%2F08%2F17+11%3A38%3A33"+
//                "&PaymentType=Credit_CreditCard&PaymentTypeChargeFee=1"+
//                ""+
//                "&RtnCode=1&RtnMsg=%E4%BA%A4%E6%98%93%E6%88%90%E5%8A%9F&SimulatePaid=0"+
//                "&StoreID="+
//                "&TradeAmt=1&TradeDate=2017%2F08%2F17+11%3A38%3A02&TradeNo=1708171138027939"+
//                "&CheckMacValue=9D8F78255C7893AB95CE5C6DBB80C9EBD795855C20BC4448148F4015004E613D";
        String responseFullInfo = "AlipayID=&AlipayTradeNo=&amount=1&ATMAccBank=&ATMAccNo="+
                "&auth_code=777777&card4no=2222&card6no=431195&CustomField1=&CustomField2="+
                "&CustomField3=&CustomField4=&eci=0&ExecTimes=&Frequency=&gwsr=10572691&MerchantID=2000132"+
                "&MerchantTradeNo=OL220170817113802&PayFrom=&PaymentDate=2017%2F08%2F17+11%3A38%3A33"+
                "&PaymentNo=&PaymentType=Credit_CreditCard&PaymentTypeChargeFee=1&PeriodAmount="+
                "&PeriodType=&process_date=2017%2F08%2F17+11%3A38%3A33&red_dan=0&red_de_amt=0&red_ok_amt=0"+
                "&red_yet=0&RtnCode=1&RtnMsg=%E4%BA%A4%E6%98%93%E6%88%90%E5%8A%9F&SimulatePaid=0"+
                "&staed=0&stage=0&stast=0&StoreID=&TenpayTradeNo=&TotalSuccessAmount=&TotalSuccessTimes="+
                "&TradeAmt=1&TradeDate=2017%2F08%2F17+11%3A38%3A02&TradeNo=1708171138027939&WebATMAccBank="+
                "&WebATMAccNo=&WebATMBankName=&CheckMacValue=9D8F78255C7893AB95CE5C6DBB80C9EBD795855C20BC4448148F4015004E613D";
        Map<String, String> result2 = ecPayService.verifyNotification(responseFullInfo);

        Assert.assertEquals("0000", StatusManipulation.getCodeOfECPay(result2.get("returnToECpay"), ""));
    }

    @Test
    public void testDoTransaction() throws UnsupportedEncodingException, NoSuchAlgorithmException {
        ECPayOrderResponse ecPayOrderResponse = new ECPayOrderResponse();
        ecPayOrderResponse.setMerchantId("2000132");
        ecPayOrderResponse.setSpToken("63UQEHRU252ETW");
        String paymentType = "CREDIT";

        ModelAndView modelAndView = ecPayService.doTransaction(ecPayOrderResponse.getMerchantId(), ecPayOrderResponse.getSpToken());

    }

    @Test
    public void testCapture() {
        ECPayCapture ecPayCapture = new ECPayCapture();
        ecPayCapture.setMerchantId("2000132");
        ecPayCapture.setMerchantTradeNo("ECTest2809");
        ecPayCapture.setCaptureAMT("30");
        ecPayCapture.setUserRefundAMT("0");
        ecPayCapture.setPlatformID("");
        ecPayCapture.setRemark("memo");

        ECPayCaptureResponse ecPayCaptureResponse = ecPayService.capture(ecPayCapture);

    }
    
    @Test
    public void testQueryOrder() throws UnsupportedEncodingException, NoSuchAlgorithmException {
        ECPayQuery ecPayQuery = new ECPayQuery();
        ecPayQuery.setMerchantId("2000132");
        ecPayQuery.setMerchantTradeNo("OL220170718155106");
        ecPayQuery.setTimeStamp(Long.toString(new Date().getTime()));
        ecPayQuery.setPlatformID("");

        ECPayQueryResponse ecPayQueryResponse = ecPayService.queryOrder(ecPayQuery);
    }

    @Test
    public void testDoActionOrder() throws UnsupportedEncodingException, NoSuchAlgorithmException {

    }

    @Test
    public void testCreateOrder() throws UnsupportedEncodingException, NoSuchAlgorithmException {

        ECPayOrder ecPayOrder = new ECPayOrder();
        ecPayOrder.setMerchantId("2000132");
        ecPayOrder.setMerchantTradeNo("strong12346");
        ecPayOrder.setStoreId("sttore001");
        ecPayOrder.setMerchantTradeDate("2017/07/03 17:09:00");
        ecPayOrder.setPaymentType("aio"); //aio
        ecPayOrder.setTotalAmount("30");
        ecPayOrder.setTradeDesc("test111");
        ecPayOrder.setItemName("sausage");
        ecPayOrder.setReturnURL("https://s.intella.co/allpaypass/api/notify/ecpay");
        ecPayOrder.setChoosePayment("ALL");
        ecPayOrder.setNeedExtraPaidInfo("N");
        ecPayOrder.setPlatformId("");
        ecPayOrder.setInvoiceMark("N");
        ecPayOrder.setHoldTradeAMT("0");

        ecPayOrder.setCustomField1("");
        ecPayOrder.setCustomField2("");
        ecPayOrder.setCustomField3("");
        ecPayOrder.setCustomField4("");

        //atm
        ecPayOrder.setExpireDate("1");
        ecPayOrder.setPaymentInfoURL("https://s.intella.co/allpaypass/api/notify/ecpay");

        //cvs
        ecPayOrder.setStoreExpireDate("7");
        ecPayOrder.setDesc1("");
        ecPayOrder.setDesc2("");
        ecPayOrder.setDesc3("");
        ecPayOrder.setDesc4("");

        //credit
        ecPayOrder.setBindingCard("0");
        ecPayOrder.setBindingCard("");


        ecPayOrder.setEncryptType("1");

        /////////////////////////////////////

        ECPayOrderResponse ecPayOrderResponse = ecPayService.createOrder(ecPayOrder);
    }

    private String getCheckMacValue(Map<String, String> map) throws Exception {
        StringBuilder checkStr = new StringBuilder();
        Map<String, String> treeMap = new TreeMap<String, String>(map);

        for(Map.Entry<String,String> entry : treeMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            checkStr.append(key).append("=").append(value).append("&");
        }
        checkStr = new StringBuilder("HashKey=" + env.getProperty("allpay.hashkey") + "&" + checkStr + "HashIV=" + env.getProperty("allpay.hashIV"));
        checkStr = new StringBuilder(URLEncoder.encode(checkStr.toString(), "UTF-8").toLowerCase());
        return getHash(checkStr.toString()).toUpperCase();
    }

    private String geturlform(Map<String, String> map) throws Exception {
        StringBuilder checkStr = new StringBuilder();
        Map<String, String> treeMap = new TreeMap<String, String>(map);

        for(Map.Entry<String,String> entry : treeMap.entrySet()) {
            String key = entry.getKey();
            String value = URLEncoder.encode(entry.getValue());

            checkStr.append(key).append("=").append(value).append("&");
        }
        checkStr = checkStr.delete(checkStr.length()-1,checkStr.length());
        //checkStr = new StringBuilder("HashKey=" + env.getProperty("allpay.hashkey") + "&" + checkStr + "HashIV=" + env.getProperty("allpay.hashIV"));
        //checkStr = new StringBuilder(URLEncoder.encode(checkStr.toString(), "UTF-8").toLowerCase());
        return checkStr.toString();
    }

    private String getHash(String pin){
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return byte2Hex(digest.digest(pin.getBytes()));

    }
    private String byte2Hex(byte[] inputBytes) {
        String result = "";
        for (byte currentByte : inputBytes) result += Integer.toString((currentByte & 0xff) + 0x100, 16).substring(1);
        return result;
    }

    private static String ecpayPost(String url, String query) throws Exception {
        URL requestUrl = new URL(url);
        HttpURLConnection con = (HttpsURLConnection) requestUrl.openConnection();
        con.setRequestMethod("POST");

        con.setRequestProperty("Content-length", String.valueOf(query.length()));
        con.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
        con.setDoOutput(true);
        con.setDoInput(true);

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();
        DataInputStream input = new DataInputStream( con.getInputStream() );

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(input));
        String result;

        StringBuilder buffer = new StringBuilder();
        while((result = bufferedReader.readLine()) != null) {
            buffer.append(result);
        }

        input.close();

        return buffer.toString();
    }


    private String getCheckValue3(ECPayOrder ecPayOrder) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        Map<String, String> requestMap = new HashMap<String, String>();
        requestMap.put("MerchantID", ecPayOrder.getMerchantId());
        requestMap.put("MerchantTradeNo", ecPayOrder.getMerchantTradeNo());
//        requestMap.put("StoreID", ecPayOrder.getStoreId());
        requestMap.put("MerchantTradeDate", ecPayOrder.getMerchantTradeDate());
        requestMap.put("PaymentType", ecPayOrder.getPaymentType());
        requestMap.put("TotalAmount", String.valueOf(ecPayOrder.getTotalAmount()));
        requestMap.put("TradeDesc", ecPayOrder.getTradeDesc());
        requestMap.put("ItemName", ecPayOrder.getItemName());
        requestMap.put("ReturnURL", ecPayOrder.getReturnURL());
        requestMap.put("ChoosePayment", ecPayOrder.getChoosePayment());
//        requestMap.put("NeedExtraPaidInfo", ecPayOrder.getNeedExtraPaidInfo());
//        requestMap.put("PlatformID", ecPayOrder.getPlatformId());
//        requestMap.put("HoldTradeAMT", String.valueOf(ecPayOrder.getHoldTradeAMT()));
        requestMap.put("EncryptType", String.valueOf(ecPayOrder.getEncryptType()));
//        requestMap.put("ExpireDate", String.valueOf(ecPayOrder.getExpireDate()));
//        requestMap.put("BindingCard", String.valueOf(ecPayOrder.getBindingCard()));

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("HashKey=5294y06JbISpM5x9&");
        for(String entry : requestMap.keySet()) {
            stringBuilder.append(entry);
            stringBuilder.append("=");
            stringBuilder.append(requestMap.get(entry));
            stringBuilder.append("&");
        }
        stringBuilder.append("HashIV=v77hoKGq4kWxNNIS");

        String checkValue = stringBuilder.toString();
        String encodeValue = URLEncoder.encode(checkValue, "UTF-8");

        String lowerCaseValue = encodeValue.toLowerCase();

        MessageDigest sha = MessageDigest.getInstance("SHA-256");
        sha.update(lowerCaseValue.getBytes());

        String sha256Value = byte2hex(sha.digest());

        String upperCaseValue = sha256Value.toUpperCase();
        return upperCaseValue;
    }


    private static String byte2hex(byte[] b){
        String hs="";
        String stmp="";
        for (int n=0;n<b.length;n++){
            stmp=(java.lang.Integer.toHexString(b[n] & 0XFF));
            if (stmp.length()==1) hs=hs+"0"+stmp;
            else hs=hs+stmp;
        }
        return hs.toUpperCase();
    }

    private String getNowTime() {
        SimpleDateFormat sdf1 = new SimpleDateFormat(SystemInstance.DATE_PATTERN);
        String str = "";
        str = sdf1.format(new Date());
        return str;
    }
}
