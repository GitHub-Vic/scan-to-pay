package co.intella.service;

import co.intella.domain.tcbank.TCBCreditCardCancelRequestData;
import co.intella.domain.tcbank.TCBCreditCardCaptureRequestHeader;
import co.intella.domain.tcbank.TCBCreditCardOrderQueryRequestData;
import co.intella.domain.tcbank.TCBCreditCardRequestData;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Andy Lin
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TcbankServiceTests {

    @Resource
    private  CaptureService captureService;

    @Resource
    private TcBankService tcBankService;

    @Test
    public void testPayment() throws Exception {
        TCBCreditCardRequestData requestData = new TCBCreditCardRequestData();
        requestData.setMerchantId("0100829354");
        requestData.setTerminalId("T0000000");
        requestData.setOrderId("17062300172");
        requestData.setTransCode("00");
        requestData.setPan("3566520000000031");
        requestData.setExpireData("0123");
        requestData.setExtenNo("123");
        requestData.setTransMode("0");
        requestData.setInstall("0");
        requestData.setTransAmt("1");
        requestData.setNotifyUrl("https://s.intella.co/allpaypass/api/notify?mchId=test&orderId=123123213");

        String result = tcBankService.payment(requestData);
        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("911", jsonObject.get("responseCode").getAsString());
    }

    @Test
    public void testCancel() throws Exception {
        TCBCreditCardCancelRequestData requestData = new TCBCreditCardCancelRequestData();
        requestData.setMerchantId("0100829354");
        requestData.setOrderId("17062300172");
        requestData.setTransCode("01");
        String result = tcBankService.cancel(requestData);
        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("17062300172", jsonObject.get("orderId").getAsString());
        Assert.assertEquals("00", jsonObject.get("responseCode").getAsString());
    }

    @Test
    public void testQueryOrder() throws Exception {
        TCBCreditCardOrderQueryRequestData requestData = new TCBCreditCardOrderQueryRequestData();
        requestData.setMerchantId("0100829354");
        requestData.setOrderId("17062300172");
        String result = tcBankService.queryOrder(requestData);
        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("17062300172", jsonObject.get("orderId").getAsString());
        Assert.assertEquals("00", jsonObject.get("responseCode").getAsString());
    }

    @Test
    public void testDoRequest() {

    }

    private String getNowTime() {
        SimpleDateFormat sdf1 = new SimpleDateFormat(SystemInstance.DATE_PATTERN);
        return sdf1.format(new Date());
    }
}
