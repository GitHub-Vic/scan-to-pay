package co.intella.service;

import co.intella.domain.tsbank.*;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Miles
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TsBankServiceTests {

    @Autowired
    private TsBankService tsBankService;

    @Test
    public void testVirtualAccount() {
        String account = tsBankService.getVirtualAccount("334567", "6242", 121);
        Assert.assertEquals("9638933456762427", account);

        account = tsBankService.getVirtualAccount("001298", "6242", 2050);
        Assert.assertEquals("9638900129862427", account);

        account = tsBankService.getVirtualAccount("123456", "6243", 10011);
        Assert.assertEquals("9638912345662431", account);

        account = tsBankService.getVirtualAccount("654320", "6244", 332541);
        Assert.assertEquals("9638965432062448", account);

        account = tsBankService.getVirtualAccount("101022", "6245", 1234563);
        Assert.assertEquals("9638910102262458", account);
    }

    @Test
    public void testPayment() throws Exception {

        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setMethod("20800");
        requestHeader.setServiceType("Payment");
        requestHeader.setMerchantId("super");
        requestHeader.setCreateTime("20170707141300");

        String data = "{\"BarcodeMode\":\"barcodeMode\",\"TimeExpire\":\"20170330123456789\",\"FeeType\":\"TWD\",\"ExpireDate\":\"3509\",\"Extended\":\"\",\"StoreOrderNo\":\"tstest2442871398\",\"StoreInfo\":\"intella-taipei\",\"CardId\":\"4162050100018705\",\"Cashier\":\"miles\",\"ExtenNo\":\"871\",\"TransMode\":\"0\",\"TotalFee\":\"1\",\"DeviceInfo\":\"A6113004\",\"Body\":\"\",\"Detail\":\"trade detail\"}";
        JsonPrimitive jsonPrimitive = new JsonPrimitive(data);
        String integratedMchId = "super";

        String result = tsBankService.doRequest(requestHeader, jsonPrimitive, integratedMchId, "127.0.0.1");
        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("0000", jsonObject.get("Header").getAsJsonObject().get("StatusCode").getAsString());
    }

    @Test
    public void testSingleOrderQuery() throws Exception {

        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setMethod("20800");
        requestHeader.setServiceType("SingleOrderQuery");
        requestHeader.setMerchantId("super");

        String data = "{\"BarcodeMode\":\"barcodeMode\",\"TimeExpire\":\"20170330123456789\",\"FeeType\":\"TWD\",\"ExpireDate\":\"3509\",\"Extended\":\"\",\"StoreOrderNo\":\"intella012\",\"StoreInfo\":\"intella-taipei\",\"CardId\":\"4162050100018705\",\"Cashier\":\"miles\",\"ExtenNo\":\"871\",\"TransMode\":\"0\",\"TotalFee\":\"1\",\"DeviceInfo\":\"A6113004\",\"Body\":\"\",\"Detail\":\"trade detail\"}";
        JsonPrimitive jsonPrimitive = new JsonPrimitive(data);
        String integratedMchId = "super";

        String result = tsBankService.doRequest(requestHeader, jsonPrimitive, integratedMchId,"127.0.0.1");
        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("0000", jsonObject.get("Header").getAsJsonObject().get("StatusCode").getAsString());
    }

    @Test
    public void testCharge() throws Exception {

        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setMethod("20800");
        requestHeader.setServiceType("Charge");
        requestHeader.setMerchantId("super");

        String data = "{\"BarcodeMode\":\"barcodeMode\",\"TimeExpire\":\"20170330123456789\",\"FeeType\":\"TWD\",\"ExpireDate\":\"3509\",\"Extended\":\"\",\"StoreOrderNo\":\"intella012\",\"StoreInfo\":\"intella-taipei\",\"CardId\":\"4162050100018705\",\"Cashier\":\"miles\",\"ExtenNo\":\"871\",\"TransMode\":\"0\",\"TotalFee\":\"1\",\"DeviceInfo\":\"A6113004\",\"Body\":\"\",\"Detail\":\"trade detail\"}";
        JsonPrimitive jsonPrimitive = new JsonPrimitive(data);
        String integratedMchId = "super";

        String result = tsBankService.doRequest(requestHeader, jsonPrimitive, integratedMchId, "127.0.0.1");
        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("0000", jsonObject.get("Header").getAsJsonObject().get("StatusCode").getAsString());
    }

    @Test
    public void testCancel() throws Exception {

        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setMethod("20800");
        requestHeader.setServiceType("Cancel");
        requestHeader.setMerchantId("super");

        String data = "{\"BarcodeMode\":\"barcodeMode\",\"TimeExpire\":\"20170330123456789\",\"FeeType\":\"TWD\",\"ExpireDate\":\"3509\",\"Extended\":\"\",\"StoreOrderNo\":\"intella012\",\"StoreInfo\":\"intella-taipei\",\"CardId\":\"4162050100018705\",\"Cashier\":\"miles\",\"ExtenNo\":\"871\",\"TransMode\":\"0\",\"TotalFee\":\"1\",\"DeviceInfo\":\"A6113004\",\"Body\":\"\",\"Detail\":\"trade detail\"}";
        JsonPrimitive jsonPrimitive = new JsonPrimitive(data);
        String integratedMchId = "super";

        String result = tsBankService.doRequest(requestHeader, jsonPrimitive, integratedMchId, "127.0.0.1");
        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("0000", jsonObject.get("Header").getAsJsonObject().get("StatusCode").getAsString());
    }

    @Test
    public void testRefund() throws Exception {

        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setMethod("20800");
        requestHeader.setServiceType("Refund");
        requestHeader.setMerchantId("super");

        String data = "{\"BarcodeMode\":\"barcodeMode\",\"TimeExpire\":\"20170330123456789\",\"FeeType\":\"TWD\",\"ExpireDate\":\"3509\",\"Extended\":\"\",\"StoreOrderNo\":\"intella012\",\"StoreInfo\":\"intella-taipei\",\"CardId\":\"4162050100018705\",\"Cashier\":\"miles\",\"ExtenNo\":\"871\",\"TransMode\":\"0\",\"TotalFee\":\"1\",\"DeviceInfo\":\"A6113004\",\"Body\":\"\",\"Detail\":\"trade detail\"}";
        JsonPrimitive jsonPrimitive = new JsonPrimitive(data);
        String integratedMchId = "super";

        String result = tsBankService.doRequest(requestHeader, jsonPrimitive, integratedMchId, "127.0.0.1");
        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("0000", jsonObject.get("Header").getAsJsonObject().get("StatusCode").getAsString());
    }

    @Test
    public void testCancelCharge() throws Exception {

        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setMethod("20800");
        requestHeader.setServiceType("CancelCharge");
        requestHeader.setMerchantId("super");

        String data = "{\"BarcodeMode\":\"barcodeMode\",\"TimeExpire\":\"20170330123456789\",\"FeeType\":\"TWD\",\"ExpireDate\":\"3509\",\"Extended\":\"\",\"StoreOrderNo\":\"intella012\",\"StoreInfo\":\"intella-taipei\",\"CardId\":\"4162050100018705\",\"Cashier\":\"miles\",\"ExtenNo\":\"871\",\"TransMode\":\"0\",\"TotalFee\":\"1\",\"DeviceInfo\":\"A6113004\",\"Body\":\"\",\"Detail\":\"trade detail\"}";
        JsonPrimitive jsonPrimitive = new JsonPrimitive(data);
        String integratedMchId = "super";

        String result = tsBankService.doRequest(requestHeader, jsonPrimitive, integratedMchId, "127.0.0.1");
        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("0000", jsonObject.get("Header").getAsJsonObject().get("StatusCode").getAsString());
    }

    @Test
    public void testCancelRefund() throws Exception {

        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setMethod("20800");
        requestHeader.setServiceType("CancelRefund");
        requestHeader.setMerchantId("super");

        String data = "{\"BarcodeMode\":\"barcodeMode\",\"TimeExpire\":\"20170330123456789\",\"FeeType\":\"TWD\",\"ExpireDate\":\"3509\",\"Extended\":\"\",\"StoreOrderNo\":\"intella012\",\"StoreInfo\":\"intella-taipei\",\"CardId\":\"4162050100018705\",\"Cashier\":\"miles\",\"ExtenNo\":\"871\",\"TransMode\":\"0\",\"TotalFee\":\"1\",\"DeviceInfo\":\"A6113004\",\"Body\":\"\",\"Detail\":\"trade detail\"}";
        JsonPrimitive jsonPrimitive = new JsonPrimitive(data);
        String integratedMchId = "super";

        String result = tsBankService.doRequest(requestHeader, jsonPrimitive, integratedMchId, "127.0.0.1");
        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("0000", jsonObject.get("Header").getAsJsonObject().get("StatusCode").getAsString());
    }



    @Test
    public void testUnionPayment() throws Exception {

        TSBCreditCardRequestData requestData = new TSBCreditCardRequestData();
        TSBCreditCardRequestDataPara para = new TSBCreditCardRequestDataPara();


        para.setOrderNo("TSUnion0003");
        para.setAmt("1");
        para.setCur("NTD");
        para.setOrderDesc("test");
        para.setCaptFlag("0");
        para.setResultFlag("1");
        para.setPostBackUrl("https://s.intella.co");
        para.setResultUrl("https://s.intella.co");
        para.setCardholderIp("211.22.90.181");
        para.setOrderTimeout("5");

        requestData.setSender("rest");
        requestData.setVer("1.0.0");
        requestData.setMid("999812666555047");
        requestData.setTid("T0000000");
        requestData.setPayType(2);
        requestData.setTxType(1);
        requestData.setParams(para);

        String result = tsBankService.payment(requestData,"super","union");


    }

    @Test
    public void testUnionOther() throws Exception {

        TSBOtherRequestData requestData = new TSBOtherRequestData();
        TSBOtherRequestDataPara para = new TSBOtherRequestDataPara();


        para.setOrderNo("tstest8660251753");
        //para.setAmt("1");


        requestData.setSender("rest");
        requestData.setVer("1.0.0");
        requestData.setMid("000812770060201");
        requestData.setTid("T0000000");
        requestData.setPayType(2);
        requestData.setTxType(8);
        requestData.setParams(para);

        String result = tsBankService.unionOther(requestData,"super");


    }


    @Test
    public void XX() throws Exception{


        String orderId = "xxxx";
        Double amount = 1.0;
        String createDate = "20171011210000";

        //TradeDetail tradeDetail = tradeDetailService.getOne(orderId);

        Map<String, String> requestMap = new HashMap<String, String>();
        requestMap.put("orderId",orderId);
        requestMap.put("DeviceInfo","T0000000");
        JsonPrimitive jsonPrimitive = new JsonPrimitive(new Gson().toJson(requestMap));

        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setServiceType("SingleOrderQuery");

        tsBankService.doRequest(requestHeader, jsonPrimitive, "super","127.0.0.1");
    }

}
