package co.intella.service;

import co.intella.domain.line.LineReserveRequestData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Alex
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class LineServiceTests {
    @Autowired
    private LineService lineService;

    @Autowired
    private PaymentAccountService paymentAccountService;

    @Autowired
    private Environment env;

    @Test
    public void testReserve() throws Exception{

        LineReserveRequestData reserveRequestData = new LineReserveRequestData();

        reserveRequestData.setProductName("testitem002");
        reserveRequestData.setProductImageUrl("https://dev.intella.co/intella-allpaypass/images.png");
        reserveRequestData.setAmount(1);
        reserveRequestData.setCurrency("TWD");
        reserveRequestData.setConfirmUrl("https://dev.intella.co/allpaypass/api/linepay/confirm");
        reserveRequestData.setOrderId("linetest00003");
//        reserveRequestData.setOneTimeKey("822983242303");

        String result = lineService.reserve(reserveRequestData,paymentAccountService.getOne("super",15));


    }
}
