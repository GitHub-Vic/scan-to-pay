package co.intella.service;

import co.intella.domain.allpay.AllpayAio;
import co.intella.domain.allpay.AllpayMicropayRequestData;
import co.intella.domain.allpay.AllpayOLRefundRequest;
import co.intella.model.PaymentAccount;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * @author Alex
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AllpayServiceTests {
    @Autowired
    private AllpayService allpayService;

    @Resource
    private PaymentAccountService paymentAccountService;
    @Resource
    private TradeProcedureService tradeProcedureService;

    @Test
    public void testMicropay() throws Exception{
        PaymentAccount paymentAccount = paymentAccountService.getOne("super",5);

        AllpayMicropayRequestData allpayMicropayRequestData = new AllpayMicropayRequestData();
        allpayMicropayRequestData.setTradeDesc(SystemInstance.EMPTY_STRING);
        allpayMicropayRequestData.setMerchantId(paymentAccount.getAccount());
        allpayMicropayRequestData.setPlatformId(paymentAccount.getAccount());
        if (allpayMicropayRequestData.getStoreId()==null)
            allpayMicropayRequestData.setStoreId("00001");
        if (allpayMicropayRequestData.getPosId()==null)
            allpayMicropayRequestData.setPosId("00001");
        allpayMicropayRequestData.setMerchantTradeDate("20170925");
        allpayMicropayRequestData.setMerchantTradeTime("160000");
        allpayMicropayRequestData.setCheckOutType("1");
        allpayMicropayRequestData.setRemark(SystemInstance.EMPTY_STRING);
        allpayMicropayRequestData.setUseRedeem("N");
        allpayMicropayRequestData.setUseRedeemAmount("0");
        String result = allpayService.micropay(allpayMicropayRequestData,paymentAccount);


    }

    @Test
    public void testOLRefund() throws Exception {

        PaymentAccount paymentAccount = paymentAccountService.getOne("super",5);
        AllpayOLRefundRequest allpayOLRefundRequest = new AllpayOLRefundRequest();

        allpayOLRefundRequest.setMerchantId("1812526");
        allpayOLRefundRequest.setPlatformId("1812526");
        allpayOLRefundRequest.setChargeBackTotalAmount("1");
        allpayOLRefundRequest.setMerchantTradeNo("OL220170809164917");
        allpayOLRefundRequest.setRemark("");
        allpayOLRefundRequest.setTradeNo("1708091649278240");
        String result = allpayService.aioRefund(allpayOLRefundRequest,paymentAccount);

        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

        Assert.assertEquals("7000", jsonObject.get("Header").getAsJsonObject().get("StatusCode").getAsString());

    }

    @Test
    public void testOLcreditRefund() throws Exception {
        PaymentAccount paymentAccount = paymentAccountService.getOne("super",5);

        AllpayOLRefundRequest allpayOLRefundRequest = new AllpayOLRefundRequest();

        allpayOLRefundRequest.setMerchantId("1812526");
        allpayOLRefundRequest.setPlatformId("1812526");
        allpayOLRefundRequest.setChargeBackTotalAmount("1");
        allpayOLRefundRequest.setMerchantTradeNo("0pjdk2t0gz0000000034");
        allpayOLRefundRequest.setRemark("");
        allpayOLRefundRequest.setTradeNo("1708161618565708");
        String result = allpayService.creditCardRefund(allpayOLRefundRequest,paymentAccount);

        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);

//        tradeProcedureService.setTradeDetailResponse("", "int29", result, data);


        Assert.assertEquals("7000", jsonObject.get("Header").getAsJsonObject().get("StatusCode").getAsString());

    }

    @Test
    public void testCreateOrder() throws UnsupportedEncodingException, NoSuchAlgorithmException {
        PaymentAccount paymentAccount = paymentAccountService.getOne("super",5);

        AllpayAio allpayAio = new AllpayAio();
        allpayAio.setMerchantId("2000132");
        allpayAio.setMerchantTradeNo("sfw"+ DateTime.now().toString(SystemInstance.DATE_PATTERN));
        allpayAio.setStoreId("sttore001");
        allpayAio.setMerchantTradeDate("2017/07/03 17:09:00");
        allpayAio.setPaymentType("aio"); //aio
        allpayAio.setTotalAmount("30");
        allpayAio.setTradeDesc("test111");
        allpayAio.setItemName("sausage");
        allpayAio.setReturnURL("https://s.intella.co/allpaypass/api/notify/ecpay");
        allpayAio.setChoosePayment("ALL");
        allpayAio.setNeedExtraPaidInfo("N");
        allpayAio.setPlatformId("");
        allpayAio.setInvoiceMark("N");
        allpayAio.setHoldTradeAMT("0");

        allpayAio.setCustomField1("");
        allpayAio.setCustomField2("");
        allpayAio.setCustomField3("");
        allpayAio.setCustomField4("");

        //atm
        allpayAio.setExpireDate("1");
        allpayAio.setPaymentInfoURL("https://s.intella.co/allpaypass/api/notify/ecpay");

        //cvs
        allpayAio.setStoreExpireDate("7");
        allpayAio.setDesc1("");
        allpayAio.setDesc2("");
        allpayAio.setDesc3("");
        allpayAio.setDesc4("");

        //credit
        allpayAio.setBindingCard("0");
        allpayAio.setBindingCard("");


        allpayAio.setEncryptType("1");

        /////////////////////////////////////

        try {
            String ecPayOrderResponse = allpayService.AioAll(allpayAio,paymentAccount);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
