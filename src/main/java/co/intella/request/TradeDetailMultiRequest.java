package co.intella.request;

import java.util.List;

public class TradeDetailMultiRequest {
    private String startDate;
    private String endDate;
    private String accountId;
    private String storeType;
    private String mchId;
    private String tradeKey;
    private List<Integer> merchantSeqIds;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getStoreType() {
        return storeType;
    }

    public void setStoreType(String storeType) {
        this.storeType = storeType;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public List<Integer> getMerchantSeqIds() {
        return merchantSeqIds;
    }

    public void setMerchantSeqIds(List<Integer> merchantSeqIds) {
        this.merchantSeqIds = merchantSeqIds;
    }

    public String getTradeKey() {
        return tradeKey;
    }

    public void setTradeKey(String tradeKey) {
        this.tradeKey = tradeKey;
    }

    public String toString() {
        return "TradeDetailMultiRequest{" +
                "startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", accountId='" + accountId + '\'' +
                ", storeType='" + storeType + '\'' +
                ", mchId='" + mchId + '\'' +
                ", tradeKey='" + tradeKey + '\'' +
                ", merchantSeqIds=" + merchantSeqIds +
                '}';
    }
}
