package co.intella.request;

public class QrcodeGetImageRequest {
	private String shortId;
	private String body;
	private String qrcodeImageBack;
	private String qrcodeImage;

	public String getShortId() {
		return shortId;
	}

	public void setShortId(String shortId) {
		this.shortId = shortId;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getQrcodeImageBack() {
		return qrcodeImageBack;
	}

	public void setQrcodeImageBack(String qrcodeImageBack) {
		this.qrcodeImageBack = qrcodeImageBack;
	}

	public String getQrcodeImage() {
		return qrcodeImage;
	}

	public void setQrcodeImage(String qrcodeImage) {
		this.qrcodeImage = qrcodeImage;
	}
	
}