package co.intella.auth;

import co.intella.model.Merchant;
import co.intella.model.Sale;
import co.intella.service.MerchantService;
import co.intella.service.SaleService;
import co.intella.utility.SystemInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collection;

/**
 * @author Miles
 */
@Component
public class IntellaAuthProvider implements AuthenticationProvider {

    private final Logger LOGGER = LoggerFactory.getLogger(IntellaAuthProvider.class);

    @Resource
    private SaleService saleService;

    @Resource
    private MerchantService merchantService;

    public Authentication authenticate(Authentication authentication) {

        try {
            String username = authentication.getName();
            String password = (String) authentication.getCredentials();

            boolean isSales = false;

            for (GrantedAuthority g : authentication.getAuthorities()) {
                LOGGER.info("[ROLE] " + g.getAuthority());
                if(SystemInstance.ROLE_CRM_USER.equals(g.getAuthority())) {
                    isSales = true;
                }
            }

            if(isSales) {
                Sale sale = saleService.login(username, password);

                if(sale != null) {
                    User user = new User(sale.getAccountId(), sale.getPin(), authentication.getAuthorities());

                    LOGGER.info("[AUTH][CRM][PASS]" + sale.getAccountId());
                    Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
                    return new UsernamePasswordAuthenticationToken(user, password, authorities);
                }

                LOGGER.error("[AUTH][CRM][YOU CAN NOT PASS]" + username);
                return null;
            } else {
                Merchant merchant = merchantService.getOne(username);

                if(merchant != null && merchant.getPin().equals(password)) {

                    User user = new User(merchant.getAccountId(), merchant.getPin(), authentication.getAuthorities());

                    LOGGER.info("[AUTH][APP][PASS]" + merchant.getAccountId());
                    Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
                    return new UsernamePasswordAuthenticationToken(user, password, authorities);
                }

                LOGGER.error("[AUTH][APP][YOU CAN NOT PASS]" + username);
                return null;
            }

        } catch (Exception e) {

            e.printStackTrace();
            LOGGER.error("[AUTH][EXCEPTION][YOU CAN NOT PASS]" + e.toString());
            return null;
        }
    }

    public boolean supports(Class<?> arg0) {
        return true;
    }
}
