package co.intella.service;

import co.intella.model.ImotoToken;

public interface ImotoTokenService {

    Integer insert(ImotoToken imotoToken);

    ImotoToken getOneForEffectiveByShortId(String shortId);

    ImotoToken getOneForEffectiveByOutcode(String Outcode);
}
