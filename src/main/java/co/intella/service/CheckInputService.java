package co.intella.service;

import co.intella.domain.tsbank.TSBVirtualAccountData;
import co.intella.exception.*;
import co.intella.model.Merchant;
import co.intella.model.RequestHeader;
import co.intella.utility.SystemInstance;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Alex
 */
@Service
public class CheckInputService {

    private final Logger LOGGER = LoggerFactory.getLogger(CheckInputService.class);

    @Autowired
    private MerchantService merchantService;

    @Resource
    private TradeDetailService tradeDetailService;

    public void maskCradId(String sessionId, String clientIP, JsonObject requestData) throws Exception {

        String cardId = null;
        String extendNo = null;
        String expireDate = null;
        if (requestData.has("CardId")) {
            cardId = requestData.get("CardId").getAsString();
            requestData.remove("CardId");
            requestData.addProperty("CardId", cardId.substring(0, 6) + "**************" + cardId.substring(cardId.length() - 4));
        }
        if (requestData.has("ExtenNo")) {
            extendNo = requestData.get("ExtenNo").getAsString();
            requestData.remove("ExtenNo");
            requestData.addProperty("ExtenNo", "***");
        }
        if (requestData.has("ExpireDate")) {
            expireDate = requestData.get("ExpireDate").getAsString();
            requestData.remove("ExpireDate");
            requestData.addProperty("ExpireDate", "****");
        }

        LOGGER.info("[REQUEST][GENERAL][" + sessionId + "][" + clientIP + "][DATA]" + requestData);

        if (cardId != null) {
            requestData.remove("CardId");
            requestData.addProperty("CardId", cardId);
        }
        if (extendNo != null) {
            requestData.remove("ExtenNo");
            requestData.addProperty("ExtenNo", extendNo);
        }
        if (expireDate != null) {
            requestData.remove("ExpireDate");
            requestData.addProperty("ExpireDate", expireDate);
        }

    }

    public void doRequest(TSBVirtualAccountData data) throws Exception {

        if (merchantService.getOne(data.getMchId()) == null) {
            throw new NoSuchMerchantException("Merchant error");
        }
        if (tradeDetailService.getOne(data.getOrderId()) != null) {
            throw new OrderAlreadyExistedException("");
        }
        if (data.getFee() == 0 || data.getMchId() == null || data.getCreateTime() == null || data.getItemName() == null || data
                .getOrderId() == null) {
            throw new InvalidDataFormatException("");
        }


    }

    public void doRequest(RequestHeader requestHeader, JsonObject requestData) throws Exception {

        boolean headerError = requestHeader.getMerchantId() == null || requestHeader.getServiceType() == null || requestHeader.getCreateTime() == null || requestHeader.getMethod() == null;
        if (headerError) {
            throw new InvalidDataFormatException("InvalidDataFormat");
        }

        Merchant merchant = merchantService.getOne(requestHeader.getMerchantId());

        if (merchant == null) {
            throw new NoSuchMerchantException("Merchant error");
        }

        if (!requestHeader.getCreateTime().startsWith(DateTime.now().toString("yyyyMMdd")) || requestHeader.getCreateTime().length() != 14) {
            throw new CreateTimeErrorException("Create time error");
        }


        if (requestHeader.getServiceType().equals(SystemInstance.TYPE_MICROPAY)) {
            if (!requestData.has("AuthCode")) {
                throw new AuthCodeErrorException("Auth code error");
            }
        }

        if (requestHeader.getServiceType().equals(SystemInstance.TYPE_OLPAY)) {
            if (requestData.has("TimeExpire")) {
                try {
                    DateTime.parse(requestData.get("TimeExpire").getAsString(), DateTimeFormat.forPattern(SystemInstance.DATE_PATTERN));
                } catch (Exception e) {
                    throw new TimeExpireException("TimeExpire format error");
                }
            }

        }

        if (requestData.has("StoreOrderNo") && !TicketLogicService.ticketMethodList.contains(requestHeader.getMethod())) {
            if (StringUtils.isBlank(requestData.get("StoreOrderNo").getAsString())) {
                throw new OrderNoIsBlankException("StoreOrderNo is blank");
            }
        }

        boolean excludeAccount = merchant.getAccountId().equals("mwd");

        if (!excludeAccount && !merchant.getTradePin().equals(requestHeader.getTradeKey())) {
            LOGGER.error("Trade key error" + merchant.getTradePin());
            throw new TradeKeyErrorException("Trade key error");
        }
    }
}
