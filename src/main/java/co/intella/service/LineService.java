package co.intella.service;

import co.intella.domain.line.LineConfirmRequestData;
import co.intella.domain.line.LinePaymentRequestData;
import co.intella.domain.line.LineRefundRequestData;
import co.intella.domain.line.LineReserveRequestData;
import co.intella.model.PaymentAccount;
import co.intella.model.QrcodeParameter;
import co.intella.model.RequestHeader;
import com.google.gson.JsonPrimitive;

import javax.persistence.criteria.CriteriaBuilder;

/**
 * @author Alex
 */

public interface LineService {

    String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;

    String reserve(LineReserveRequestData request, PaymentAccount paymentAccount) throws Exception;

    String confirm(LineConfirmRequestData request, PaymentAccount paymentAccount, String transactionId) throws Exception;

    String refundReserve(LineRefundRequestData request, PaymentAccount paymentAccount, String transactionId) throws Exception;

    String singleOrderQuery(PaymentAccount paymentAccount, String transactionId) throws Exception;

    String payment(LinePaymentRequestData request, PaymentAccount paymentAccount,String device) throws Exception;

    String refundPayment(LineRefundRequestData request, PaymentAccount paymentAccount, String transactionId) throws Exception;

    LineReserveRequestData prepareLineReserveRequestData(QrcodeParameter qrcodeParameter, Integer totalFee, String orderId);


}
