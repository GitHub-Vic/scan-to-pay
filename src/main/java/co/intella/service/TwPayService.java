package co.intella.service;

import co.intella.domain.twpay.*;
import co.intella.exception.OrderAlreadyExistedException;
import co.intella.exception.OtherAPIException;
import co.intella.model.Device;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import com.google.gson.JsonPrimitive;
import org.dom4j.Document;

import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;

/**
 * @author Alex
 */

public interface TwPayService {

    String getPaymentUrl(String amount, String shortId, String agent) throws OtherAPIException, UnsupportedEncodingException, OrderAlreadyExistedException;

    String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;

    Document createMicorpayXml(TwPayRequestData twPayRequestData, String type, PaymentAccount paymentAccount, Device device);

    String xmlPost(String url, String query) throws Exception;

    String getTerminalVerifyCode(String data, String code);

    boolean prepareTwPayRequestData(TwPayRequestData twPayRequestData, PaymentAccount paymentAccount, TradeDetail tradeDetail);

    String getVerifyCode(TwPayRequestData twPayRequestData, String verifyCode);

    QrpNotifyResp getQrpNotifyResp(QrpNotifyReq qrpNotifyReq);

    QrpEmcqrNotifyResp getQrpEmvqrNotifyResp(QrpEmvqrNotifyReq qrpNotifyReq);

    void updateTradeDetailByQrpNotifyReq(QrpNotifyReq qrpNotifyReq, TradeDetail tradeDetail,String cardType);

    void updateTradeDetailByQrpEmvqrNotifyReq(QrpEmvqrNotifyReq qrpNotifyReq, TradeDetail tradeDetail);

    void webToAppReq(String shortId, String amount, HttpServletResponse response) throws Exception;

//    void updateTradeDetailByWebToApp(WebToAppNotify webToAppNotify, TradeDetail tradeDetail);
    void updateTradeDetailByWebToApp( QrpNotifyResp qrpNotifyResp , TradeDetail tradeDetail);

    void setTradeDetailCreditCardType(TradeDetail tradeDetail);

    void saveTradeDetailCardLog(TradeDetail tradeDetail);
}
