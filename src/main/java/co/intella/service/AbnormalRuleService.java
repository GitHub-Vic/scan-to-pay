package co.intella.service;


import java.util.List;
import co.intella.model.IntTbAbnormalRule;

public interface AbnormalRuleService {
	
	List<IntTbAbnormalRule> findAllByArcSeq(Integer arcSeq);
	List<IntTbAbnormalRule> findAll();



}
