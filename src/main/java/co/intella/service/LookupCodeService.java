package co.intella.service;

import co.intella.model.IntTbLookupCode;
import co.intella.model.IntTbLookupCodeNoFK;

import java.util.List;

public interface LookupCodeService {

    List<IntTbLookupCode> findAllByLookType(String type);

    IntTbLookupCode findOne(String type, String code);

    boolean isTicketType(String bankId);

    IntTbLookupCode save(IntTbLookupCode intTbLookupCode);
    
    List<IntTbLookupCodeNoFK> findAllByLookTypeQuery(String type);

    boolean isTsmcAccount(String accountId);

    boolean isFGSAccount (String accountId);

    boolean isIMotoAccount (String accountId);

    boolean isIParkingAccount (String accountId);
}
