package co.intella.service.validation;

import co.intella.request.TradeDetailMultiRequest;

public interface TradeDetailMultiValidationService {
    String doValidate (TradeDetailMultiRequest request);
}
