package co.intella.service.validation.impl;

import co.intella.model.Merchant;
import co.intella.request.TradeDetailMultiRequest;
import co.intella.service.LookupCodeService;
import co.intella.service.MerchantService;
import co.intella.service.validation.TradeDetailMultiValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
public class TradeDetailMultiValidationServiceImpl implements TradeDetailMultiValidationService {

    @Resource
    private MerchantService merchantService;

    @Resource
    private LookupCodeService lookupCodeService;

    @Resource
    private MessageSource messageSource;

    private Locale locale = new Locale("zh_TW");

    @Override
    public String doValidate(TradeDetailMultiRequest request) {
        List<String> validateResult = new ArrayList<>();
        customFormatDate(request);
        String result = null;
        validateMchId(request, validateResult);
        validateTradeKey(request, validateResult);
        validateStoreType(request, validateResult);
        validateAccountId(request, validateResult);
        validateStartDate(request, validateResult);
        validateEndDate(request, validateResult);
        if (!CollectionUtils.isEmpty(validateResult)) {
            result = String.join(",", validateResult);
        }
        return result;
    }

    private void customFormatDate(TradeDetailMultiRequest request) {
        String startDate = request.getStartDate();
        if (!StringUtils.isEmpty(startDate) && startDate.length() == 8) {
            request.setStartDate(startDate + "000000");
        }
        String endDate = request.getEndDate();
        if (!StringUtils.isEmpty(endDate) && endDate.length() == 8) {
            request.setEndDate(endDate + "235959");
        }
    }

    private void validateEndDate(TradeDetailMultiRequest request, List<String> validateResult) {
        String value = request.getEndDate();
        if (StringUtils.isEmpty(value)) {
            validateResult.add(messageSource.getMessage("co.intella.tradeDetailMulti.endDate.notEmpty", null, locale));
        } else if (!StringUtils.isEmpty(request.getStartDate())) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
            formatter = formatter.withLocale(Locale.TAIWAN);
            LocalDateTime endDate;
            LocalDateTime startDate;
            try {
                endDate = LocalDateTime.parse(value, formatter);
                try {
                    startDate = LocalDateTime.parse(request.getStartDate(), formatter);
                    if (endDate.isBefore(startDate)) {
                        validateResult.add(messageSource.getMessage("co.intella.tradeDetailMulti.endDate.not.before.startDate", null , locale));
                    }
                } catch (DateTimeParseException ignored) {}
            } catch (DateTimeParseException e) {
                validateResult.add(messageSource.getMessage("co.intella.tradeDetailMulti.endDate.incorrect", null, locale));
            }
        }
    }

    private void validateStartDate(TradeDetailMultiRequest request, List<String> validateResult) {
        String value = request.getStartDate();
        if (StringUtils.isEmpty(value)) {
            validateResult.add(messageSource.getMessage("co.intella.tradeDetailMulti.startDate.notEmpty", null, locale));
        } else if (!StringUtils.isEmpty(request.getEndDate())) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
            formatter = formatter.withLocale(Locale.TAIWAN);
            LocalDateTime endDate;
            LocalDateTime startDate;
            try {
                startDate = LocalDateTime.parse(value, formatter);
                try {
                    endDate = LocalDateTime.parse(request.getEndDate(), formatter);
                    if (startDate.isBefore(endDate)) {
                        long months = startDate.until(endDate, ChronoUnit.DAYS);
                        if (months > 31) {
                            validateResult.add(messageSource.getMessage("co.intella.tradeDetailMulti.startDate.not.bigger.one.month", null, locale));
                        }
                    }
                } catch (DateTimeParseException ignored) {}
            } catch (DateTimeParseException e) {
                validateResult.add(messageSource.getMessage("co.intella.tradeDetailMulti.startDate.incorrect", null, locale));
            }
        }
    }

    private void validateAccountId(TradeDetailMultiRequest request, List<String> validateResult) {
        String value = request.getAccountId();
        if (!StringUtils.isEmpty(value))  {
            Merchant merchant = merchantService.getOne(value);
            if (ObjectUtils.isEmpty(merchant)) {
                validateResult.add(messageSource.getMessage("co.intella.tradeDetailMulti.accountId.notFound", null, locale));
            } else {
                if (!request.getMchId().equals(merchant.getParentMerchantAccount()) && !request.getAccountId().equals(request.getMchId())) {
                    validateResult.add(messageSource.getMessage("co.intella.tradeDetailMulti.accountId.notSub", null, locale));
                }
            }
        }
    }

    private void validateStoreType(TradeDetailMultiRequest request, List<String> validateResult) {
        String value = request.getStoreType();
        if (!StringUtils.isEmpty(value)) {
            boolean anyMatch = lookupCodeService.findAllByLookType("storeType").stream().filter(item -> "1".equals(item.getType2())).anyMatch(item -> item.getLookupCode().equals(value));
            if (!anyMatch) {
                validateResult.add(messageSource.getMessage("co.intella.tradeDetailMulti.storeType.incorrect", null, locale));
            }
        }
    }

    private void validateTradeKey(TradeDetailMultiRequest request, List<String> validateResult) {
        String value  = request.getTradeKey();
        if (StringUtils.isEmpty(value)) {
            validateResult.add(messageSource.getMessage("co.intella.tradeDetailMulti.tradeKey.notEmpty", null, locale));
        } else if (!StringUtils.isEmpty(request.getMchId())) {
            Merchant merchant = merchantService.getOne(request.getMchId());
            if (!ObjectUtils.isEmpty(merchant) && !value.equals(merchant.getTradePin())) {
                validateResult.add(messageSource.getMessage("co.intella.tradeDetailMulti.tradeKey.incorrect", null, locale));
            }
        }
    }

    private void validateMchId(TradeDetailMultiRequest request, List<String> validateResult) {
        String value = request.getMchId();
        if (StringUtils.isEmpty(value)) {
            validateResult.add(messageSource.getMessage("co.intella.tradeDetailMulti.mchId.notEmpty", null, locale));
        } else {
            Merchant merchant = merchantService.getOne(value);
            if (ObjectUtils.isEmpty(merchant)) {
                validateResult.add(messageSource.getMessage("co.intella.tradeDetailMulti.mchId.incorrect", null, locale));
            }
        }
    }
}
