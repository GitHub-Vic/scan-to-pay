package co.intella.service;

import co.intella.model.TradeDetailOtherInfo;

public interface TradeDetailOtherInfoService {

    TradeDetailOtherInfo getOneByUNHEXUuid(String uuid);

    String getCardIdByCardNumberForPrint(String cardNumberForPrint);

    TradeDetailOtherInfo save(TradeDetailOtherInfo tradeDetailOtherInfo);

    String getCardIdByLikeCardNumberForPrint(String cardNumberForPrint);
}
