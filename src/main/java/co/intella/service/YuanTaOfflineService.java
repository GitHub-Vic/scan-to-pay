package co.intella.service;

import co.intella.model.PaymentAccount;
import co.intella.model.QrcodeParameter;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import com.google.gson.JsonPrimitive;

public interface YuanTaOfflineService {

    String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;

    String sendRequest(String method, String url, String request) throws Exception;

    String prepareAliPayShangHaiReserveRequestData(String totalFee, String orderId, PaymentAccount paymentAccount) throws Exception;

    String createOrder(TradeDetail tradeDetail, QrcodeParameter qrcodeParameter, String totalFee, String storeOrderNoNew, PaymentAccount paymentAccount) throws Exception;

    String encryptSHA256(String content) throws Exception;

}
