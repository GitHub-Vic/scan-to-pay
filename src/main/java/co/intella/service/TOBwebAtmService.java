package co.intella.service;

import co.intella.domain.tobWebAtm.TOBwebATMCreateOrderRequestData;

import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import com.google.gson.JsonPrimitive;

import javax.servlet.http.HttpServletResponse;


public interface TOBwebAtmService {
    String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;

    void createOrder(TOBwebATMCreateOrderRequestData request, PaymentAccount paymentAccount, HttpServletResponse response) throws Exception;

    void getPaymentUrl(String amount, String shortId, HttpServletResponse response) throws Exception;

    String backUrl(String res) throws Exception;

    void notify(String res) throws Exception;

    String schedulingQueryMerchantOrder(String orderId)throws Exception;
}
