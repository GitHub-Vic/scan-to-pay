package co.intella.service;

import com.google.gson.JsonPrimitive;

import co.intella.domain.fubon.FuBonParamBean;
import co.intella.model.RequestHeader;

public interface FuBonBankService {
	
	public String creditAuth(RequestHeader requestHeader, JsonPrimitive data, String integrateMchId, String clientIP) throws Exception;

	public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;
	
	public String fubonBankNotify(String orderId) throws Exception;

}
