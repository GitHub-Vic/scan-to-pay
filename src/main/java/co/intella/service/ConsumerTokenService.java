package co.intella.service;

import co.intella.model.ConsumerToken;

/**
 * @author Miles
 */
public interface ConsumerTokenService {

    ConsumerToken getOne(Long id);

    ConsumerToken createOrUpdate(ConsumerToken entity);

}
