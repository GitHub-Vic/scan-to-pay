package co.intella.service;

import co.intella.model.INT_VW_IpassCheckOut;

import java.util.List;

public interface IpassCheckOutService {

    List<INT_VW_IpassCheckOut> findAll();
}
