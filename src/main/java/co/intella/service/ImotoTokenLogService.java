package co.intella.service;

import co.intella.model.ImotoTokenLog;

import java.util.List;

public interface ImotoTokenLogService {

    Integer insert(ImotoTokenLog ntuImotoTokenLog);

    List<ImotoTokenLog> getListByOutercodeAndTokenSeq(String tokenOutercode , long imotoTokenSeq);

    ImotoTokenLog getLastLogByOutercodeAndTokenSeq(String tokenOutercode , long imotoTokenSeq);
}
