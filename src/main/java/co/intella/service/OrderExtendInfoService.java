package co.intella.service;

import co.intella.domain.aliShangHai.AliShangHaiRequestExtendParams;

public interface OrderExtendInfoService {

    AliShangHaiRequestExtendParams findOne(String randomId);
}
