package co.intella.service;

import com.google.gson.JsonPrimitive;

import co.intella.model.RequestHeader;

public interface ScsbService {

	String creditAuth(RequestHeader requestHeader, JsonPrimitive data, String integrateMchId, String clientIP) throws Exception;
	
	String creditAuth3D(RequestHeader requestHeader, JsonPrimitive data, String integrateMchId, String clientIP) throws Exception;
	
	String scsb3DNotify(String orderId, String errorCode) throws Exception;

	String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;

}
