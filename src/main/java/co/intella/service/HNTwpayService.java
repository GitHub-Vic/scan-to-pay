package co.intella.service;

import co.intella.model.RequestHeader;
import com.google.gson.JsonPrimitive;

public interface HNTwpayService {


    String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;

    String getPaymentUrl(String amount, String shortId, String agent) throws Exception;
}
