package co.intella.service;

import co.intella.domain.icash.IcashBaseResponse;
import co.intella.domain.icash.IcashTradeRefundResponse;
import co.intella.domain.icash.IcashTradeSaleRequest;
import co.intella.domain.icash.IcashTradeSaleResponse;
import co.intella.exception.InvokeAPIException;
import co.intella.model.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.JsonObject;

public interface ICashService {

    String ezRequest(RequestHeader requestHeader, JsonObject requestData, Merchant merchant) throws Exception;

    String doBlcDownload() throws JsonProcessingException, InvokeAPIException;

    void settlementMain(String createDate,String batchEndStr);

    String doStoreMain() throws JsonProcessingException, InvokeAPIException;

    void settlementMain(String createDate, String endDate,String batchEndStr);

    String updateDoTradeSale(RequestHeader requestHeader, JsonObject requestData, Device device, Merchant merchant, IcashTradeSaleRequest icashTradeSaleRequest, IcashTradeSaleResponse response, TradeDetail tradeDetail, PaymentAccount paymentAccount);

    String updateDoRefund(RequestHeader requestHeader, JsonObject requestData, Device device, Merchant merchant, IcashTradeRefundResponse response, TradeDetail tradeDetail, PaymentAccount paymentAccount);

    String getGeneralResponse(RequestHeader requestHeader, IcashBaseResponse response, String errorCode);
}
