package co.intella.service;

import co.intella.domain.tcbank.*;
import co.intella.model.RequestHeader;
import com.google.gson.JsonPrimitive;

/**
 * @author Miles
 */
public interface TcBankService {

    String payment(TCBCreditCardRequestData request) throws Exception;

    String cancel(TCBCreditCardCancelRequestData request) throws Exception;

    String queryOrder(TCBCreditCardOrderQueryRequestData request) throws Exception;

    String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;
}
