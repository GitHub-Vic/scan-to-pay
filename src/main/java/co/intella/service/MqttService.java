package co.intella.service;

import co.intella.domain.notify.ApnsMessage;

/**
 * @author Miles
 */
public interface MqttService {

    void publish(String clientId, String message, String topic) throws Exception;

    void publishAPNS(String token, ApnsMessage payload, String appId) throws Exception;

}
