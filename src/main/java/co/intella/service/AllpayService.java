package co.intella.service;

import co.intella.domain.allpay.*;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import com.google.gson.JsonPrimitive;

import java.util.Map;

/**
 * @author Alex
 */

public interface AllpayService {

    String micropay(AllpayMicropayRequestData request, PaymentAccount paymentAccount) throws Exception;

    String singleQuery(AllpaySingleQueryRequestData request, PaymentAccount paymentAccount)throws Exception ;

    String commit(AllpayCommitRequestData requestData, PaymentAccount paymentAccount) throws Exception;

    String refund(AllpayRefundRequestData requestData, PaymentAccount paymentAccount) throws Exception;

    String createOrder(AllpayCreateOrderRequestData requestData, PaymentAccount paymentAccount) throws Exception;

    String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;

    String getCheckMacValue(Map<String, String> map, PaymentAccount paymentAccount) throws Exception;

    String allpayPost(String url, String query) throws Exception;

    String aioRefund(AllpayOLRefundRequest requestData, PaymentAccount paymentAccount) throws Exception;

    String creditCardRefund(AllpayOLRefundRequest requestData, PaymentAccount paymentAccount) throws Exception;

    String AioAll(AllpayAio requestData, PaymentAccount paymentAccount) throws Exception;



}
