package co.intella.service;

import java.util.List;
import co.intella.model.ReserveCancel;

/**
 * @author Miles
 */
public interface ReserveCancelService {

    ReserveCancel save(ReserveCancel reserveCancelVo);

    ReserveCancel getOne(String orderId);

    List<ReserveCancel> listByMerchantIdAndDeviceIdAndBatchNo(String merchantId, String deviceId, String batchNo);

    List<ReserveCancel> listGroupEZC(String terminalId, String deviceId, String batchNo) ;
}
