package co.intella.service;


import co.intella.model.DiscountMch;

import java.util.List;

public interface DiscountMchService {

    List<DiscountMch> getDiscountMchList(String accountId);
}
