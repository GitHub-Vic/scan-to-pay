package co.intella.service;

import co.intella.model.TradeDetail;
import org.springframework.stereotype.Service;

/**
 * @author Alex
 */
public interface NotifyService {
    Boolean notifyMerchant(TradeDetail tradeDetail) throws Exception;

    Boolean notifyMerchantTest(String merchantId) throws Exception;
}
