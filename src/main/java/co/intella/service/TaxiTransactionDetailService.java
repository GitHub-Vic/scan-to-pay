package co.intella.service;

import co.intella.model.TaxiTransactionDetail;

import java.util.List;

public interface TaxiTransactionDetailService {


    int countAbnormalRule001(String mins, String licensePlate, String cardId);

    int countAbnormalRule002(String licensePlate, String cardId);

    int countAbnormalRule003(String cycleTime, String licensePlate, String cardId);

    List<TaxiTransactionDetail> getCycleList(String startDate, String endDate, String licensePlate, String cardId, String terminalId);

    TaxiTransactionDetail save(String taxiTransactionDetail);
}
