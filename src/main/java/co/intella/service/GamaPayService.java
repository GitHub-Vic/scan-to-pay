package co.intella.service;

import co.intella.domain.gama.GamaCreateOrderRequestData;
import co.intella.domain.gama.GamaRefundRequestData;
import co.intella.domain.gama.GamaSingleOrderQueryRequestData;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import com.google.gson.JsonPrimitive;

/**
 * @author Alex
 */

public interface GamaPayService {

    String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;

    String createOrder(GamaCreateOrderRequestData request, PaymentAccount paymentAccount) throws Exception;

    String refund(GamaRefundRequestData request, PaymentAccount paymentAccount) throws Exception;

    String refundQuery(GamaSingleOrderQueryRequestData request, PaymentAccount paymentAccount) throws Exception;

    String singleOrderQuery(GamaSingleOrderQueryRequestData request, PaymentAccount paymentAccount) throws Exception;

    String getPaymentUrl(String amount, String shortId, String agen) throws Exception;

}

