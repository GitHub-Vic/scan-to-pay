package co.intella.service;

import co.intella.domain.TSMC.QueryForCardIdRequest;
import co.intella.domain.contratStore.CustomizePageRequest;
import co.intella.domain.contratStore.TradeDetailVo;
import co.intella.domain.integration.IntegratedOrderQueryRequestData;
import co.intella.model.TradeDetail;

import java.util.List;
import java.util.UUID;

/**
 * Created by Alexchiu on 2017/3/20.
 */
public interface TradeDetailService {

    TradeDetail save(TradeDetail tradeDetail);

    TradeDetail getOne(String orderId);

    TradeDetail getOne(UUID uuid);

    TradeDetail getOneBySystemOrderId(String systemOrderId);

    TradeDetail getOneByIntellaOrderId(String systemOrderId);

    TradeDetail getOneBySysOrder(String accountId, String sysOrderId);

    TradeDetail getOne(String accountId, String orderId);

    TradeDetail getOneByPartnerUserIdAndOrderId(String partnerUserId, String orderId);

    TradeDetail getOneByOrderIdAndMethod(String orderId, String method);

    TradeDetail getOneByTradeToken(String method, String tradeToken);

    TradeDetail getOneByShortId(String shortId);

    List<TradeDetail> listByMerchants(CustomizePageRequest request);

    List<TradeDetail> pageList(CustomizePageRequest request);

    long countPaymentByCreateDate(CustomizePageRequest request);

    long pageCount(CustomizePageRequest request);

    List<TradeDetail> listByAccountId(String accountId);

    List<TradeDetail> listByMerchantIdAndDate(String merchantId, String startDate, String endDate);

    List<TradeDetail> list(IntegratedOrderQueryRequestData requestData, String merchantId);

    List<TradeDetailVo> listByBatchNo(String merchantId, String deviceId, String batchNo);

    List<TradeDetailVo> listGroupEZC(String terminalId, String deviceId, String batchNo);

    List<TradeDetailVo> listByUserId(String merchantId, String userId);

    List<TradeDetailVo> listByUserId(String merchantId, String deviceId, String userId);

    List<TradeDetailVo> listTopOneByUserId(String merchantId, String deviceId, String userId);

    List<TradeDetailVo> listByBatchNoAndMethod(String accountId, String deviceId, String batchNo, String method);

    /**
     * 取得富邦 apple pay 以及 富邦3D   的訂單，以去做請款動作
     *
     * @return
     */
    List<TradeDetail> getFubonOrderList();

    /**
     * (使用總店/子店帳號, 搜下面所有直營/加盟店子帳號下該card id的交易
     *
     * @return
     */
    List<TradeDetail> queryForCardIdMchId(QueryForCardIdRequest queryForCardIdRequest, String mchId);

    List<String> queryTobWebAtmOrder();
}
