package co.intella.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import co.intella.model.MultiQrcode;


public interface MultiQrcodeService {
	
	public List<MultiQrcode> findMutiQRCodeByPayer(String shortId, String email, String payerName, String mobileNumber) throws Exception;
	
	public List<MultiQrcode> findMutiQRCodeByPayer(String shortId, String email, String payerName, String mobileNumber, String payerId, String token, HttpServletResponse response) throws Exception;
	
	public List<MultiQrcode> findMutiQRCode(String shortId) throws Exception;
	
	public Map<String, Object> findMutiQRCode(String shortId, String payerId, String token, HttpServletResponse response) throws Exception;
	
	public String mergeQrcode(String accountId, String shortIdsData, String payerId, HttpServletResponse response) throws Exception;
	
	public String sendCookie(Map<String, String> request, HttpServletResponse response) throws Exception;

	public void sendToken(Map<String, String> request)throws Exception;

}
