package co.intella.service;

import co.intella.model.Merchant;
import co.intella.model.Recipient;

import java.util.List;
import java.util.UUID;

/**
 * @author Miles
 */
public interface RecipientService {

    Recipient save(Recipient entity);

    void delete(String serialId, String appId);

    Recipient getOne(UUID id);

    Recipient getOne(String udid, String appId);

    Recipient getOneAppIdNull(String udid);

    List<Recipient> list();

    List<Recipient> listByMerchant(Merchant merchant);
    
    public List<Recipient> listByUserId(String userId);

	void deleteNullAppId(String serialId);

}
