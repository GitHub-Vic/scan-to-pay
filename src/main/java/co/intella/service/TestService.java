package co.intella.service;

import co.intella.model.TestUUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author Alex
 */

public interface TestService {
    Page findAll(Pageable pageable);

    public void save(TestUUID testUUID);

    TestUUID getOne(String id);


}
