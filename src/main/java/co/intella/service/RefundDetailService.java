package co.intella.service;

import co.intella.domain.contratStore.RefundDetailVo;
import co.intella.model.RefundDetail;

import java.util.List;

/**
 * @author Alex
 */

public interface RefundDetailService {

    RefundDetail save(RefundDetail refundDetail);

    RefundDetail getOne(String orderId);

    RefundDetail getOneByStoreRefundId(String accountId, String orderId);

    RefundDetail getTopOneByStoreRefundId(String accountId, String orderId);

    RefundDetail getOneByOrderIdAndMethod(String orderId, String method);

    List<RefundDetailVo> listByBatchNo(String merchantId, String deviceId, String batchNo);
    
    List<RefundDetailVo> listGroupEZC(String terminalId, String deviceId, String batchNo) ;

    RefundDetail getOneByOrderId(String orderId);
}
