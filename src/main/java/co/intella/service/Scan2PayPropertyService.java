package co.intella.service;

import java.io.IOException;

/**
 * @author Miles
 */
public interface Scan2PayPropertyService {

    String get(String property) throws IOException;

}
