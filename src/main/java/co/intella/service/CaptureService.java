package co.intella.service;

import co.intella.domain.tcbank.TCBCreditCardCaptureRequestData;
import co.intella.domain.tcbank.TCBCreditCardCaptureRequestHeader;
import co.intella.model.CaptureDetail;
import co.intella.model.CaptureOrderDetail;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

/**
 * @author Andy Lin
 */
public interface CaptureService {

    CaptureDetail generalCapture(String merchantId, String method, String from, String to);

    String tcbCapture(String mchId, String startDate, String endDate) throws IOException, ParseException;

    String tcbCaptureResult(String mchId, String startDate, String endDate) throws IOException, ParseException;

    String weixinCapture(String mchId, String from, String to) throws IOException, ParseException;

    String weixinCaptureResult() throws IOException, ParseException;

    String allpayCapture(String mchId, String startDate, String endDate) throws IOException, ParseException;

    String allpayCaptureResult() throws IOException, ParseException;

    String alipayCapture(String mchId, String startDate, String endDate);

    String tsbCapture(String mchId, String startDate, String endDate) throws IOException, ParseException;
}
