package co.intella.service;

import co.intella.domain.ali.*;
import co.intella.domain.weixin.WeixinJsapiRequestData;
import co.intella.model.RequestHeader;
import com.google.gson.JsonPrimitive;

/**
 * @author Miles Wu
 */
public interface AliService {

    String doRequest(AliBasicRequestData request, String requestType) throws Exception;

    String callJsapi(WeixinJsapiRequestData request, String requestType) throws Exception;

    String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;
}
