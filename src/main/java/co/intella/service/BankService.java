package co.intella.service;

import co.intella.model.Bank;

import java.util.List;

/**
 * Created by Alexchiu on 2017/3/20.
 */
public interface BankService {

    Bank getOne(long bankId);

    List<Bank> listAll();

    void delete(long bankId);

    void save(Bank bank);
}
