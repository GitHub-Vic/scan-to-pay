package co.intella.service;

import co.intella.model.PaymentAccount;

import java.util.List;
import java.util.Map;

/**
 * Created by Alexchiu on 2017/3/20.
 */
public interface PaymentAccountService {

    PaymentAccount getOne(String merchantId,long bankId);

    PaymentAccount getOneByDao(String merchantId,long bankId);

    List<PaymentAccount> listByBankId(long bankId);

    List<PaymentAccount> listAllByAccount(String accountId);

    List<PaymentAccount> listByQrcode(String qrcode);
    
    List<PaymentAccount> groupAcerTexiIpass();

    PaymentAccount save(PaymentAccount paymentAccount);

    String paymentAccountExist(String terminalId);

    List<PaymentAccount> listByPayAccount(String payAccount);

	Map<String, Object> checkQrcodePayment(String merchantId);

	Map<String, Object> checkTicketPayment(String merchantId);

}
