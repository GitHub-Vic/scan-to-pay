package co.intella.service;

import com.google.gson.JsonPrimitive;

import co.intella.domain.PaymentResponse;
import co.intella.domain.tsbank.TSBCreditCardBasicRequestData;
import co.intella.domain.tsbank.TSBCreditCardRequestData;
import co.intella.domain.tsbank.TSBOtherResponseData;
import co.intella.model.RequestHeader;

/**
 * @author Alex
 */

public interface TsBankService {

    String payment(TSBCreditCardRequestData request, String integrateMchId, String type) throws Exception;

    String other(TSBCreditCardBasicRequestData request, String integrateMchId) throws Exception;

    String unionPayment(TSBCreditCardRequestData request, String integrateMchId) throws Exception;

    String unionOther(TSBCreditCardBasicRequestData request, String integrateMchId) throws Exception;

    String doRequest(RequestHeader requestHeader, JsonPrimitive data, String integrateMchId, String clientIP) throws Exception;

    String getVirtualAccount(String virtualAccountNo, String date, long price);
    
    PaymentResponse mapToPaymentResponse(TSBOtherResponseData tsbOtherResponseData);
}
