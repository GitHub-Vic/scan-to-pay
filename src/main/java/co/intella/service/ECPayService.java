package co.intella.service;

import co.intella.domain.ecpay.*;
import co.intella.model.RequestHeader;
import com.google.gson.JsonPrimitive;
import org.springframework.web.servlet.ModelAndView;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * @author Miles
 */
public interface ECPayService {

    String doRequest(RequestHeader requestHeader, JsonPrimitive data, String mchId) throws Exception;
    // todo create order
    ECPayOrderResponse createOrder(ECPayOrder ecPayOrder);

    ECPayQueryResponse queryOrder(ECPayQuery ecPayQuery);

    ECPayCaptureResponse capture(ECPayCapture ecPayCapture);

    ECPayDoActionResponse doAction(ECPayDoAction ecPayDoAction);

    ModelAndView doTransaction(String merchantId, String spToken);

    String getCheckMacValue(Map<String, String> map) throws Exception;

    Map<String, String> verifyNotification(String response) throws Exception;

    String queryTransaction(String orderId);
}
