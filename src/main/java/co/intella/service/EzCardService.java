package co.intella.service;

import co.intella.domain.acertaxi.AcerTaxiPaymentResponse;
import co.intella.domain.acertaxi.EzCardEDCAPaymentReq;
import co.intella.domain.easycard.*;
import co.intella.exception.*;
import co.intella.model.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.JsonObject;

import java.io.UnsupportedEncodingException;

/**
 * @author Miles
 */
public interface EzCardService {

    String doRequest(EzCardBasicRequest ezCardBasicRequest, String aesKey);

    String ezRequest(RequestHeader requestHeader, JsonObject requestData, Merchant merchant) throws JsonProcessingException, DeviceNotFoundException, InvalidDataFormatException, OrderNotFoundException, OrderAlreadyRefundedException, ServiceTypeNotSupportException, RefundKeyException, UnsupportedEncodingException, UnsupportedEncodingException, OtherAPIException;

    Boolean bootDevice(String deviceId, String terminalId);

    EzCardEDCAPaymentReq getEzCardEDCAPaymentReq(Device device, PaymentAccount paymentAccount, String amount, String time, String date, String batchNo, String retry);

    EzCardPayment getEzCardPayment(Device device, PaymentAccount paymentAccount, String amount, String time, String date, String batchNo, String retry, String terminalTXNNumber);

    String updateDoPayment(RequestHeader requestHeader, JsonObject requestData, Device device, Merchant merchant, EzCardPayment ezCardPayment, EzCardPaymentResponse response, TradeDetail tradeDetail, PaymentAccount paymentAccount);

    String updateEDCAPayment(RequestHeader requestHeader, JsonObject requestData, Device device, Merchant merchant, EzCardEDCAPaymentReq ezCardEDCAPaymentReq, AcerTaxiPaymentResponse response, TradeDetail tradeDetail, PaymentAccount paymentAccount);

    EzCardRefund getEzCardRefund(Device device, PaymentAccount paymentAccount, String time, String date, String batchNo, String retry, String terminalTXNNumber);

    EzCardInfoQuery getEzCardInfoQuery(Device device, PaymentAccount paymentAccount, String date, String time);

    String upodateBalanceQuery(Device device, RequestHeader requestHeader, EzCardInfoQuery ezCardInfoQuery, EzCardInfoQueryResponse response);

    String updateDoRefund(RequestHeader requestHeader, JsonObject requestData, Device device, Merchant merchant, EzCardRefund ezCardRefund, EzCardRefundResponse response, TradeDetail tradeDetail, PaymentAccount paymentAccount, String batchNo);

    String getUserId(PaymentAccount paymentAccount, Device device);

}
