package co.intella.service;

import co.intella.domain.contratStore.DeviceVo;
import co.intella.model.Device;
import co.intella.model.Merchant;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * Created by Alexchiu on 2017/3/21.
 */
public interface DeviceService {

    List<Device> listAllByMerchant(Merchant owner);

    List<DeviceVo> listAllByMerchantAndType(String merchantId, String type);

    Device getOne(UUID deviceId);

    Device getOne(Merchant merchant,String ownerDeviceId);

    Device getOne(String license);

    DeviceVo getOneByMerchantAndDeviceId(String deviceId, String merchantId);
    
    Device getOneByTerminal(String deviceId, String terminalId);

    String deviceExist(String deviceId);

    void delete(String deviceId);

    void save(Device deviceId);
    
    public Device getOne(String type, String ownerDeviceId);
    
    public Device getOneByTDOrderId (String orderId);
    
    public Device getOneByRDOrderId (String orderId);
    
    public Device getOneByRefundOrderId (String orderId);
}
