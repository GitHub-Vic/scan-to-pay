package co.intella.service;

import co.intella.model.Permission;

import java.util.List;

/**
 * @author Andy Lin
 */
public interface PermissionService {

    Permission getOneById(long id);

    List<Permission> getListByRoleId(long roleId);

    List<Permission> listAll();

    Permission save(Permission permission);

    void delete(Permission permission);
}
