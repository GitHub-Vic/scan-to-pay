package co.intella.service;

import co.intella.domain.friDay.FridayBasic;
import co.intella.domain.friDay.FridayNotifyRequestData;
import co.intella.model.RequestHeader;
import com.google.gson.JsonPrimitive;

import java.security.PrivateKey;

/**
 * @author Alex
 */

public interface FridayService {

    Boolean updateNotify(FridayNotifyRequestData requestData);

    String RSADecrypt(byte[] encryptedData, PrivateKey privateKey) throws Exception;

    String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;

    String refund(FridayBasic fridayBasic);


}
