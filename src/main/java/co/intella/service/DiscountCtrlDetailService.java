package co.intella.service;

import co.intella.model.DiscountCtrlDetail;

import java.util.List;

public interface DiscountCtrlDetailService {

    List<DiscountCtrlDetail> getDetailList(long dicountCtrlSeq);
}
