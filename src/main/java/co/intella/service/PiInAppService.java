package co.intella.service;

import co.intella.domain.pi.PiBasicRequestData;
import co.intella.model.RequestHeader;
import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * @author Miles
 */
public interface PiInAppService {

    String doRequest(PiBasicRequestData request, RequestHeader header) throws JsonProcessingException;

}
