package co.intella.service;

import co.intella.model.IntTbAbnormalRuleCtrl;

public interface AbnormalRuleCtrlService {

    IntTbAbnormalRuleCtrl findOneMaxVersionByLocation(String merchantSeqId, String location);
}
