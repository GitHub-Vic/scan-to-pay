package co.intella.service;

import co.intella.model.RequestHeader;
import com.google.gson.JsonObject;

public interface TsmcService {
    String doRequest(RequestHeader requestHeader, JsonObject data) throws Exception;

}
