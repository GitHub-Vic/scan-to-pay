package co.intella.service;

import java.util.Map;

public abstract class CheckoutCustomService {
	public abstract void computeCheckout(Map<String, Object> checkOutObj);
	
	public abstract String executeCheckout(Map<String, Object> checkOutObj);
	
}
