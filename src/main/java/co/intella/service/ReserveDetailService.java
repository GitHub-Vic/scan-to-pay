package co.intella.service;

import java.util.List;
import co.intella.model.ReserveDetail;

/**
 * @author Miles
 */
public interface ReserveDetailService {

    ReserveDetail save(ReserveDetail reserveDetail);

    ReserveDetail getOne(String merchantId, String orderId);

    List<ReserveDetail> listByBatchNumber(String merchantId, String batchNumber);

    List<ReserveDetail> listByDeviceId(String merchantId, String deviceId);

    List<ReserveDetail> listByBatchNo(String merchantId, String deviceId, String batchNo);

    List<ReserveDetail> listByUserId(String merchantId, String deviceId, String userId);

    List<ReserveDetail> listTopOneByUserId(String merchantId, String deviceId, String userId);
    
    List<ReserveDetail> listGroupEZC(String terminalId, String deviceId, String batchNo) ;

}
