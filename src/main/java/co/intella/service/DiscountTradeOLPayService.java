package co.intella.service;

import co.intella.model.DiscountTradeOLPay;

public interface DiscountTradeOLPayService {

    DiscountTradeOLPay save(DiscountTradeOLPay discountTrade);

    DiscountTradeOLPay getOne(String accountId, String shortId);

    DiscountTradeOLPay getOneByOrderId(String orderId, String accountId);
}
