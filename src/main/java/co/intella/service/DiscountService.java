package co.intella.service;

import co.intella.model.*;
import co.intella.utility.JsonUtil;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DiscountService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());


    @Autowired
    private MerchantService merchantService;

    @Autowired
    private LookupCodeService lookupCodeService;

    @Autowired
    private DiscountMchService discountMchService;

    @Autowired
    private DiscountCtrlDetailService discountCtrlDetailService;

    public DiscountTrade checkoutTotalFree(String accountId, Long totalFee, String method) {
        LOGGER.info("[checkoutTotalFree] accountId : {}\t totalFee :{}\t method :{}", accountId, totalFee, method);
        final BigDecimal oriAmount = BigDecimal.valueOf(totalFee);
        DiscountTrade discountTrade = null;
        //查出該mchId 參加的活動
        List<DiscountMch> discountMchList = this.getDiscountMchList(accountId);
        LOGGER.info("[checkoutTotalFree] discountMchList : {}", JsonUtil.toJsonStr(discountMchList));
        if (CollectionUtils.isNotEmpty(discountMchList)) {
            List<DiscountCtrl> discountCtrlList = this.getDiscountCtrlList(discountMchList, method);
            LOGGER.info("[checkoutTotalFree] discountCtrlList : {}", JsonUtil.toJsonStr(discountCtrlList));
            if (CollectionUtils.isNotEmpty(discountCtrlList)) {            // 有符合的設定
                discountTrade = this.valuation(discountCtrlList, oriAmount);   //計算各活動後的金額明細
                LOGGER.info("[checkoutTotalFree] discountTrade : {}", JsonUtil.toJsonStr(discountTrade));
                if (Objects.nonNull(discountTrade)) {
                    discountTrade.setAccountId(accountId);
                }
            }
        }
        return discountTrade;
    }

    private List<DiscountMch> getDiscountMchList(String accountId) {

        List<DiscountMch> discountMchList = discountMchService.getDiscountMchList(accountId);
        final LocalDateTime now = LocalDateTime.now();
        final ZoneId zoneId = ZoneId.systemDefault();
        return discountMchList.stream().filter(discountMch -> {
            LocalDateTime startDateTime = Objects.isNull(discountMch.getStartDate()) ? null : LocalDateTime.ofInstant(discountMch.getStartDate().toInstant(), zoneId);
            LocalDateTime endDateTime = Objects.isNull(discountMch.getEndDate()) ? null : LocalDateTime.ofInstant(discountMch.getEndDate().toInstant(), zoneId);

            boolean check;
            if (Objects.isNull(startDateTime) && Objects.isNull(endDateTime)) {
                check = true;
            } else if (Objects.nonNull(startDateTime) && Objects.nonNull(endDateTime)) {
                check = startDateTime.isBefore(now) && now.isBefore(endDateTime);
            } else if (Objects.nonNull(startDateTime)) {
                check = startDateTime.isBefore(now);
            } else {
                check = now.isBefore(endDateTime);
            }
            return check;
        }).collect(Collectors.toList());
    }


    private List<DiscountCtrl> getDiscountCtrlList(List<DiscountMch> discountMchList, String method) {
        final LocalDateTime now = LocalDateTime.now();
        final ZoneId zoneId = ZoneId.systemDefault();

        return discountMchList.stream().map(DiscountMch::getDicountCtrl)
                .filter(discountCtrl -> Arrays.asList(discountCtrl.getMethods().split(",")).contains(method))
                .filter(discountCtrl -> discountCtrl.getBudget().subtract(discountCtrl.getGrandTotal()).compareTo(new BigDecimal(0)) > 0)
                .filter(discountCtrl -> {
                    LocalDateTime startDateTime = LocalDateTime.ofInstant(discountCtrl.getStartDate().toInstant(), zoneId);
                    LocalDateTime endDateTime = Objects.isNull(discountCtrl.getEndDate()) ? null : LocalDateTime.ofInstant(discountCtrl.getEndDate().toInstant(), zoneId);

                    boolean check;
                    if (Objects.isNull(endDateTime)) {
                        check = startDateTime.isBefore(now);
                    } else {
                        check = startDateTime.isBefore(now) && now.isBefore(endDateTime);
                    }
                    return check;
                }).collect(Collectors.toList());
    }

    /**
     * 活動設定明細 ( 由上限排序  小到大)
     *
     * @param dicountCtrlSeq
     * @return
     */
    private List<DiscountCtrlDetail> getDiscountCtrlDetaillList(long dicountCtrlSeq) {
        List<DiscountCtrlDetail> discountCtrlDetailList = discountCtrlDetailService.getDetailList(dicountCtrlSeq);
        discountCtrlDetailList = discountCtrlDetailList.stream().peek(discountCtrlDetail -> {
            if (Objects.isNull(discountCtrlDetail.getCalLimit())) {
                discountCtrlDetail.setCalLimit(new BigDecimal(999999999));
            }
        }).collect(Collectors.toList());
        // 先排序 由上限 小到大
        Comparator<DiscountCtrlDetail> comparator = Comparator.comparing(discountCtrlDetail -> discountCtrlDetail.getCalLimit().intValue());
        discountCtrlDetailList.sort(comparator);
        return discountCtrlDetailList;
    }

    /**
     * 計算活動金額
     *
     * @param discountCtrlList
     * @return
     */
    private DiscountTrade valuation(List<DiscountCtrl> discountCtrlList, final BigDecimal oriAmount) {

        List<DiscountTrade> discountTradeList = discountCtrlList.stream().map(discountCtrl -> {
            //取得活動明細
            List<DiscountCtrlDetail> discountCtrlDetailList = this.getDiscountCtrlDetaillList(discountCtrl.getDicountCtrlSeq());
            LOGGER.info("[valuation] discountCtrlDetailList : {}", JsonUtil.toJsonStr(discountCtrlDetailList));
            DiscountTrade discountTrade = new DiscountTrade();

            switch (discountCtrl.getDiscountCode()) {
                case "D1":  //每%s元，折扣%s元
                    discountTrade = this.valuation_D1(discountCtrlDetailList, oriAmount);
                    break;
                case "D21":
                case "D22":
                case "D23":  //每%s元，打%s折
                    discountTrade = this.valuation_D2(discountCtrlDetailList, oriAmount, this.getRoundTypeInt(discountCtrl));
                    break;
                case "D31":
                case "D32":
                case "D33":  //每%s元，%s OFF
                    discountTrade = this.valuation_D3(discountCtrlDetailList, oriAmount, this.getRoundTypeInt(discountCtrl));
                    break;
            }
            LOGGER.info("[valuation] {}  discountTrade:{}", discountCtrl.getDiscountCode(), JsonUtil.toJsonStr(discountTrade));
            return discountTrade;
        }).collect(Collectors.toList());


        LOGGER.info("[valuation] discountTradeList : {}", JsonUtil.toJsonStr(discountTradeList));

        return discountTradeList.stream()
                .filter(discountTrade -> Objects.nonNull(discountTrade.getTradeAmount()))
                .min(Comparator.comparing(DiscountTrade::getTradeAmount)).orElse(null);
    }

    private DiscountTrade valuation_D1(List<DiscountCtrlDetail> discountCtrlDetailList, final BigDecimal oriAmount) {
        DiscountTrade discountTrade = new DiscountTrade();
        BigDecimal calculatedLimit = new BigDecimal(0); //已計算的上限額度
        BigDecimal discountAmount = new BigDecimal(0);  // 累計優惠金額
        DiscountCtrl dicountCtrl = null;

        for (int i = 0; i < discountCtrlDetailList.size(); i++) {
            DiscountCtrlDetail discountCtrlDetail = discountCtrlDetailList.get(i);
            LOGGER.info("[valuation_D1] discountCtrlDetail : {}", JsonUtil.toJsonStr(discountCtrlDetail));
            dicountCtrl = discountCtrlDetail.getDicountCtrl();
            if (oriAmount.compareTo(discountCtrlDetail.getCalLimit()) <= 0) {
                //若原始金額  小於 等於 設定上限 則算到此
                BigDecimal lastAmount = oriAmount.subtract(calculatedLimit);
                Integer intervals = lastAmount.intValue() / discountCtrlDetail.getCalUnit().intValue();
                discountAmount = discountAmount.add(new BigDecimal(intervals).multiply(discountCtrlDetail.getCalRatio()));
                break;
            } else {
                BigDecimal lastAmount = discountCtrlDetail.getCalLimit().subtract(calculatedLimit);
                calculatedLimit = discountCtrlDetail.getCalLimit();
                Integer intervals = lastAmount.intValue() / discountCtrlDetail.getCalUnit().intValue();
                discountAmount = discountAmount.add(new BigDecimal(intervals).multiply(discountCtrlDetail.getCalRatio()));
            }
        }
        LocalDateTime localDateTime = LocalDateTime.now();
        discountTrade.setDicountCtrl(dicountCtrl);
        discountTrade.setDiscountAmount(discountAmount);
        discountTrade.setOriAmount(oriAmount);
        discountTrade.setTradeAmount(oriAmount.subtract(discountAmount));
        discountTrade.setCreateDate(Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant()));

        return discountTrade;
    }

    private DiscountTrade valuation_D2(List<DiscountCtrlDetail> discountCtrlDetailList, final BigDecimal oriAmount, final int roundTypeInt) {

        return new DiscountTrade();
    }

    private DiscountTrade valuation_D3(List<DiscountCtrlDetail> discountCtrlDetailList, final BigDecimal oriAmount, final int roundTypeInt) {

        return new DiscountTrade();
    }

    private int getRoundTypeInt(DiscountCtrl discountCtrl) {
        IntTbLookupCode lookupCode = lookupCodeService.findOne("discountType", discountCtrl.getDiscountCode());
        int typeInt = BigDecimal.ROUND_HALF_UP;     //預設四捨五入
        if (Objects.nonNull(lookupCode)) {
            if (1 == lookupCode.getValue()) //無條件捨去
                typeInt = BigDecimal.ROUND_DOWN;
            else if (3 == lookupCode.getValue()) //無條件進位
                typeInt = BigDecimal.ROUND_UP;
        }
        return typeInt;
    }
}
