package co.intella.service;

import com.google.gson.JsonPrimitive;

import co.intella.model.RequestHeader;

public interface UUPayService {
    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;
	public String doOLPay(String totalFee, String token) throws Exception;
	public void doNotify(String json,String authorization) throws Exception;
}
