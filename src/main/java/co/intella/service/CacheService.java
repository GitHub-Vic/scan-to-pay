package co.intella.service;

import java.util.List;

/**
 * @author Alex
 */

public interface  CacheService {

    public void addMessage(String user,String message);

    public List<String> listMessages(String user);
}
