package co.intella.service;

import co.intella.domain.contratStore.OperationHistory;

import java.util.List;

/**
 * @author Miles
 */
public interface OperationHistoryService {

    List<OperationHistory> list(String merchantId);

    void save(OperationHistory operationHistory);

}
