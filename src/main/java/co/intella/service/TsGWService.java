package co.intella.service;

import com.google.gson.JsonPrimitive;

import co.intella.model.RequestHeader;

public interface TsGWService {

	public String doRequest(RequestHeader requestHeader, JsonPrimitive data, String gw) throws Exception;
	
}
