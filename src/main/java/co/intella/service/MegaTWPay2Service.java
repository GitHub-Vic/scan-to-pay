package co.intella.service;

import co.intella.model.RequestHeader;
import com.google.gson.JsonPrimitive;

/**
 * 新兆豐台灣pay
 */
public interface MegaTWPay2Service {

    String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;
}
