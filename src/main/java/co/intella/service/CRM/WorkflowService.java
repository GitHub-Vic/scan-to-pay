package co.intella.service.CRM;

import co.intella.model.Merchant;
import co.intella.model.Sale;
import co.intella.model.Ticket;
import co.intella.model.SalesManage.WorkflowConfig;
import co.intella.utility.WorkflowAction;

/**
 * @author Alex
 */

public interface WorkflowService {

    WorkflowConfig loadConfig();

    void reloadConfig(String workflowJson);

    Ticket nextStep(Ticket ticket, WorkflowAction action, Sale reviewer, String message) throws Exception;

    Ticket createTicket(Merchant merchant, Sale sales);

    Ticket resetTicket(Ticket ticket, Sale sales);

    WorkflowConfig getCONFIG();



}
