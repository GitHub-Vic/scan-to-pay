package co.intella.service.CRM.impl;

import co.intella.model.QTicketHistory;
import co.intella.model.Sale;
import co.intella.model.Ticket;
import co.intella.model.TicketHistory;
import co.intella.repository.TicketHistoryRepository;
import co.intella.service.CRM.TicketHistoryService;
import com.google.common.collect.Lists;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Alex
 */
@Service
@Transactional
public class TicketHistoryServiceImpl implements TicketHistoryService {

    @Resource
    private TicketHistoryRepository ticketHistoryRepository;

    public void delete(long id) {
        ticketHistoryRepository.delete(id);
    }

    public TicketHistory save(TicketHistory ticketHistory) {
        return ticketHistoryRepository.save(ticketHistory);

    }

    public List<TicketHistory> listBySales(Sale sales) {

        QTicketHistory qTicketHistory = QTicketHistory.ticketHistory;
        BooleanExpression predicate = qTicketHistory.actorId.eq(sales);

        return Lists.newArrayList( ticketHistoryRepository.findAll(predicate));

    }

    public List<TicketHistory> listByTicket(Ticket ticket) {
        //todo
        return null;
    }
}
