package co.intella.service.CRM.impl;

import co.intella.model.*;
import co.intella.model.SalesManage.WorkflowConfig;
import co.intella.model.Ticket;
import co.intella.repository.TicketRepository;
import co.intella.service.CRM.TicketHistoryService;
import co.intella.service.CRM.TicketOperatorService;
import co.intella.service.CRM.TicketService;
import co.intella.service.CRM.WorkflowService;
import co.intella.service.RoleService;
import com.google.common.collect.Lists;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.security.krb5.internal.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Alex
 */
@Service
@Transactional
public class TicketServiceImpl implements TicketService {

    @Resource
    private TicketRepository ticketRepository;

    @Autowired
    private WorkflowService workflowService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private TicketOperatorService ticketOperatorService;

    @Autowired
    private TicketHistoryService ticketHistoryService;


    public Ticket save(Ticket ticket) {

        return ticketRepository.save(ticket);
    }

    public Ticket getOneById(long id) {
        QTicket qTicket = QTicket.ticket;
        BooleanExpression predicate = qTicket.ticketId.eq(id);
        return ticketRepository.findOne(predicate);
    }

    public Ticket getOne(Merchant merchant) {
        QTicket qTicket = QTicket.ticket;
        BooleanExpression predicate = qTicket.merchant.eq(merchant);
        return ticketRepository.findOne(predicate);
    }

    public Ticket getOneIsActive(Merchant merchant) {
        QTicket qTicket = QTicket.ticket;
        BooleanExpression predicate = qTicket.merchant.eq(merchant).and(
                qTicket.status.lt(3)
        );
        return ticketRepository.findOne(predicate);
    }

    public List<Ticket> listAll() {
        return ticketRepository.findAll();
    }

    public List<Ticket> listAllByMerchant(Merchant merchant) {
        QTicket qTicket = QTicket.ticket;
        BooleanExpression predicate = qTicket.merchant.eq(merchant);

        return Lists.newArrayList( ticketRepository.findAll(predicate));
    }

    public List<Ticket> listAllByCreator(Sale sales) {
        QTicket qTicket = QTicket.ticket;
        BooleanExpression predicate = qTicket.creator.eq(sales);

        return Lists.newArrayList( ticketRepository.findAll(predicate));
    }

    public List<Ticket> listForReview(Sale sales) {

//        Role role = roleService.getOneById(Long.valueOf("4"));
        List<Ticket> tickets = new  ArrayList<Ticket>();

        for (Role role:sales.getRoles()){
            List<TicketOperator> ticketOperators = ticketOperatorService.listAllByRole(role);
            for(TicketOperator ticketOperator : ticketOperators){
                tickets.add(ticketOperator.getTicketId());
            }

        }




//        WorkflowConfig config = workflowService.getCONFIG();

        return tickets;
//        return null;
    }

    public List<Ticket> listHistory(Sale sales) {

        List<TicketHistory> ticketHistoryList =ticketHistoryService.listBySales(sales);

        List<Ticket> tickets = new ArrayList<Ticket>();

        for (TicketHistory t:ticketHistoryList){

        }

        return null;
    }
}
