package co.intella.service.CRM.impl;

import co.intella.model.*;
import co.intella.model.SalesManage.Task;
import co.intella.model.SalesManage.WorkflowConfig;
import co.intella.service.CRM.TicketHistoryService;
import co.intella.service.CRM.TicketOperatorService;
import co.intella.service.CRM.TicketService;
import co.intella.service.CRM.WorkflowService;
import co.intella.service.RoleService;
import co.intella.utility.SystemInstance;
import co.intella.utility.WorkflowAction;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Files;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * @author Alex
 */
@Service
@Transactional
public class WorkflowServiceImpl implements WorkflowService {

    private final Logger LOGGER = LoggerFactory.getLogger(WorkflowServiceImpl.class);

    @Resource
    private TicketService ticketService;

    @Autowired
    private TicketOperatorService ticketOperatorService;

    @Autowired
    private TicketHistoryService ticketHistoryService;

    @Autowired
    private RoleService roleService;

    @Value("${workflow.config.path}")
    private String configPath;

    private static WorkflowConfig CONFIG;

    public WorkflowServiceImpl() {


        // todo load default from db
//        CONFIG = loadConfig("/opt/config_scan2pay/workflow.conf");
        CONFIG = loadConfig();

    }

    public WorkflowConfig getCONFIG(){
        return CONFIG;
    }

    public WorkflowConfig loadConfig() {
        try {

            //todo fix UTF-8 problem. Now, chinese should be encoded to unicode first.
            //LOGGER.info("[config][path]:"+filePath);
            ClassLoader classLoader = getClass().getClassLoader();
            //File file = new File(classLoader.getResource("key/php_private.der").getFile());

            byte[] jsonData = Files.toByteArray(new File(classLoader.getResource("key/workflow.conf").getFile()));//.readAllBytes(new File(filePath).toPath());

            ObjectMapper objectMapper = new ObjectMapper();

            return objectMapper.readValue(jsonData, WorkflowConfig.class);
        }
        catch (Exception e){
            LOGGER.info("[LoadConfig][Error]"+e.getMessage());
            return null;
        }

    }

    public void reloadConfig(String filePath){
        CONFIG = loadConfig();
    }

    public Ticket nextStep(Ticket ticket, WorkflowAction action, Sale reviewer, String message) throws Exception{

        Task nextTask = getNextTask(ticket,action);

        if (nextTask==null){
            return null;
        }

        if (ticket.getStatus().equals(0)){
            ticket.setFirstReviewer(reviewer);
            ticket.setFirstReviewDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        }
        else if (ticket.getStatus().equals(1) ){
            ticket.setSecondReviewer(reviewer);
            ticket.setSecondReviewDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        }

        ticket.setStatus(nextTask.getTo());

        setOperator(ticket);

        setHistory(ticket,action,reviewer,message);

        return ticketService.save(ticket);

    }

    private void setHistory(Ticket ticket, WorkflowAction action, Sale reviewer, String message) {
        TicketHistory ticketHistory = new TicketHistory();

        ticketHistory.setAction(action);
        ticketHistory.setTicketId(ticket);
        ticketHistory.setActorId(reviewer);
        ticketHistory.setMessage(message);
        ticketHistoryService.save(ticketHistory);
    }

    private Task getNextTask(Ticket ticket, WorkflowAction action) {
        List<Task> tasks_config = CONFIG.getTasks();

        for (Task a:tasks_config){
            for (String status:a.getStatus()){
                if (String .valueOf(ticket.getStatus()).equals(status)) {
                    if (action.ordinal()==a.getAction()){
                        return a;
                    }
                }
            }
        }
        return null;
    }

    private Task getCurrentTask(Ticket ticket) {
        List<Task> tasks_config = CONFIG.getTasks();

        for (Task a:tasks_config){
            if (ticket.getStatus().equals(a.getTo())) {
                return a;
            }
        }
        return null;
    }

    public Ticket createTicket(Merchant merchant, Sale sales) {
        Ticket ticket = new Ticket();

        ticket.setTitle("test");
        ticket.setMerchant(merchant);
        ticket.setStatus(0);
        ticket.setCreator(sales);
        ticket.setFirstReviewer(null);
        ticket.setSecondReviewer(null);
        ticket= ticketService.save(ticket);

        setOperator(ticket);

        return ticket;
    }

    private void setOperator(Ticket ticket) {

        clearOperator(ticket);

//        if (ticket.getStatus()!=4) {


        Task task = getCurrentTask(ticket);

        if (task != null && task.getAssignment()!=null) {
            for (String role : task.getAssignment()) {
                if (role.equals("creator")) {
                    //todo name is not unique
                    TicketOperator ticketOperator = new TicketOperator();
                    ticketOperator.setTicketId(ticket);
                    ticketOperator.setAvailableRole(roleService.getOneByName(ticket.getCreator().getAccountId()));
                    ticketOperatorService.save(ticketOperator);

                }
                if (roleService.getOneByName(role) != null) {
                    TicketOperator ticketOperator = new TicketOperator();
                    ticketOperator.setTicketId(ticket);
                    ticketOperator.setAvailableRole(roleService.getOneByName(role));
                    ticketOperatorService.save(ticketOperator);
                }
            }
        }

//        }
    }

    private void clearOperator(Ticket ticket) {

        List<TicketOperator> ticketOperators = ticketOperatorService.listAllByTicket(ticket);

        for(TicketOperator t:ticketOperators){
            ticketOperatorService.delete(t.getId());
        }

    }

    public Ticket resetTicket(Ticket ticket, Sale sales) {
        ticket.setStatus(0);
        ticket.setCreateDate(DateTime.now().toDate());
        ticket.setFirstReviewer(null);
        ticket.setFirstReviewDate(null);
        ticket.setSecondReviewer(null);
        ticket.setSecondReviewDate(null);
        ticket= ticketService.save(ticket);

        setOperator(ticket);

        setHistory(ticket,WorkflowAction.RESET,sales,null);

        return ticket;
    }




}
