package co.intella.service.CRM.impl;

import co.intella.model.*;
import co.intella.repository.TicketOperatorRepository;
import co.intella.service.CRM.TicketOperatorService;
import com.google.common.collect.Lists;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Alex
 */
@Service
@Transactional
public class TicketOperatorServiceImpl implements TicketOperatorService {

    @Resource
    private TicketOperatorRepository ticketOperatorRepository;

    public void delete(long id) {
        ticketOperatorRepository.delete(id);
    }

    public List<TicketOperator> listAllByTicket(Ticket ticket) {
        QTicketOperator qTicketOperator = QTicketOperator.ticketOperator;
        BooleanExpression predicate = qTicketOperator.ticketId.eq(ticket);
        return Lists.newArrayList( ticketOperatorRepository.findAll(predicate));

    }

    public List<TicketOperator> listAllByRole(Role role) {
        QTicketOperator qTicketOperator = QTicketOperator.ticketOperator;
        BooleanExpression predicate = qTicketOperator.availableRole.eq(role);
        return Lists.newArrayList( ticketOperatorRepository.findAll(predicate));
    }

    public TicketOperator save(TicketOperator ticketOperator) {
        return ticketOperatorRepository.save(ticketOperator);
    }
}
