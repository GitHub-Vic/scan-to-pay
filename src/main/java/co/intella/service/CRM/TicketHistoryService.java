package co.intella.service.CRM;

import co.intella.model.Sale;
import co.intella.model.Ticket;
import co.intella.model.TicketHistory;
import co.intella.model.TicketOperator;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Alex
 */

public interface TicketHistoryService {

    void delete(long id);

    TicketHistory save(TicketHistory ticketHistory);

    List<TicketHistory> listBySales(Sale sales);

    List<TicketHistory> listByTicket(Ticket ticket);

}
