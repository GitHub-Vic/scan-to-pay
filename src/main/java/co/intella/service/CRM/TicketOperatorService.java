package co.intella.service.CRM;

import co.intella.model.Bank;
import co.intella.model.Role;
import co.intella.model.SalesManage.Task;
import co.intella.model.Ticket;
import co.intella.model.TicketOperator;

import java.util.List;

/**
 * @author Alex
 */

public interface TicketOperatorService {

    void delete(long id);

    List<TicketOperator> listAllByTicket(Ticket ticket);

    List<TicketOperator> listAllByRole(Role role);

    TicketOperator save(TicketOperator ticketOperator);
}
