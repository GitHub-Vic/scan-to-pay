package co.intella.service.CRM;

import co.intella.model.Merchant;
import co.intella.model.Role;
import co.intella.model.Sale;
import co.intella.model.Ticket;

import java.util.List;

/**
 * @author Alex
 */

public interface TicketService {

    Ticket save(Ticket ticket);

    Ticket getOne(Merchant merchant);

    Ticket getOneById(long id);

    Ticket getOneIsActive(Merchant merchant);

    List<Ticket> listAll();

    List<Ticket> listAllByMerchant(Merchant merchant);

    List<Ticket> listAllByCreator(Sale sales);

    List<Ticket> listForReview(Sale sales);

    List<Ticket> listHistory(Sale sales);

}
