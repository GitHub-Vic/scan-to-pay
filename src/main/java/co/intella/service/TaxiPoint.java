package co.intella.service;

import co.intella.model.*;
import com.google.gson.JsonObject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 計程車 票證邏輯
 */

public abstract class TaxiPoint {
    private final Logger LOGGER = LoggerFactory.getLogger(TaxiPoint.class);
    @Resource
    private TaxiTransactionDetailService taxiTransactionDetailService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private AbnormalRuleService abnormalRuleService;

    @Resource
    private AbnormalRuleCtrlService abnormalRuleCtrlService;

    /**
     * 取得ＰＯＳ機本地代碼 (scan2Pay)
     *
     * @param device
     * @return
     */
    public abstract String getScan2PayLcation(Device device);

    /**
     * 取得ＰＯＳ機本地使用點數限制金額
     *
     * @param location
     * @return
     */
    public abstract String getlLcationUsePointLimit(String location);

    /**
     * 查詢 組合卡片資料
     *
     * @param device
     * @param queryCardJson
     * @return
     */
    public abstract IntTbLookupCode isCanDeductionPoint(Device device, JsonObject queryCardJson);

    /**
     * 主要邏輯入口
     *
     * @param device
     * @param queryCardJson
     * @param requestData
     * @return
     */
    public Map<String, String> mainLogic(Device device, JsonObject queryCardJson, JsonObject requestData) {
        Map<String, String> resultMap = new HashMap<>();

        String scan2PayLcation = getScan2PayLcation(device);

        // 查詢該卡是否設定為 社福卡
        IntTbLookupCode intTbLookupCode = isCanDeductionPoint(device, queryCardJson);

        // 取得該區營運規則錯誤信息
        Map<String, String> verificationRuleMap = getVerificationRuleMap(device, queryCardJson, scan2PayLcation);
        String verificationRuleResult = verificationRuleMap.get("result");

        boolean verificationRule = StringUtils.isEmpty(verificationRuleResult);

        // 分配金額（現金,錢包,社福點）
        Map<String, String> allocationMap = allocation(queryCardJson, requestData, scan2PayLcation,
                intTbLookupCode, verificationRule, device);

        resultMap.put("Code", StringUtils.isBlank(verificationRuleResult) ? "0000" : "9999");
        resultMap.put("Msg", verificationRuleResult.trim());
        resultMap.put("Total", requestData.get("Total").getAsString());
        resultMap.putAll(allocationMap);
        resultMap.put("arcSeq", verificationRuleMap.get("arcSeq"));
        return resultMap;
    }


    /**
     * 取得營運規則的流水號以期驗證結果
     *
     * @param device
     * @param queryCardJson
     * @param location
     * @return
     */
    private Map<String, String> getVerificationRuleMap(Device device, JsonObject queryCardJson, String location) {
        Map<String, String> map = new HashMap<>();

        String result = " "; // 故意空格,做來判斷是否沒有營運規則
        Merchant parentMch = merchantService.getOne(device.getOwner().getParentMerchantAccount());
        IntTbAbnormalRuleCtrl abCtrl = abnormalRuleCtrlService.findOneMaxVersionByLocation(String.valueOf(parentMch.getMerchantSeqId()), location);
        LOGGER.info("[verificationRule] IntTbAbnormalRuleCtrl is null ? " + Objects.isNull(abCtrl));
        if (Objects.nonNull(abCtrl)) {
            List<IntTbAbnormalRule> abRList = abnormalRuleService.findAllByArcSeq(abCtrl.getArcSeq());
            LOGGER.info("[verificationRule] List<IntTbAbnormalRule> is not empty ? " + CollectionUtils.isNotEmpty(abRList));
            if (CollectionUtils.isNotEmpty(abRList)) {
                map.put("arcSeq", abCtrl.getArcSeq().toString());
                result = verificationRule(device, queryCardJson, abRList);
            }
        }
        map.put("result", result);
        return map;
    }

//    /**
//     * 取得 營運規則的結果字串
//     *
//     * @param abRList
//     * @param licensePlate
//     * @param cardId
//     * @return
//     */
//    public String getverificationRuleSult(List<IntTbAbnormalRule> abRList, String licensePlate, String cardId) {
//        String result = "";
//        for (IntTbAbnormalRule abr : abRList) {
//            int conunt;
//            switch (abr.getAbnormalRule().trim()) {
//                case "RULE001":
//                    conunt = taxiTransactionDetailService.countAbnormalRule001(abr.getParameter1(), licensePlate, cardId);
//                    if (conunt > 0) {
//                        result = abr.getType1().replace("{parameter1}", abr.getParameter1());
//                    }
//                    break;
//                case "RULE002":
//                    conunt = taxiTransactionDetailService.countAbnormalRule002(licensePlate, cardId);
//                    if (conunt >= Integer.parseInt(abr.getParameter1())) {
//                        result = abr.getType1().replace("{parameter1}", abr.getParameter1());
//                    }
//                    break;
//                case "RULE003":
//                    conunt = taxiTransactionDetailService.countAbnormalRule003(abr.getParameter1(), licensePlate, cardId);
//                    if (conunt >= Integer.parseInt(abr.getParameter2())) {
//                        result = abr.getType1().replace("{parameter2}", abr.getParameter2());
//                    }
//                    break;
//            }
//            if (StringUtils.isNotBlank(result)) {
//                break;
//            }
//        }
//
//        return result;
//    }

    /**
     * 驗證營運規則
     *
     * @param device
     * @param queryCardJson
     * @param abRList
     * @return {'cdoe'=false,'msg'='同一車輛，同一卡號，連續刷卡間隔小於5分鐘'}
     * {'cdoe'=true,'msg'=''}
     */
    public abstract String verificationRule(Device device, JsonObject queryCardJson, List<IntTbAbnormalRule> abRList);


    /**
     * 分配錢包 點數 現金，預設台中
     *
     * @param queryCardJson
     * @param requestData
     * @param location
     * @param intTbLookupCode
     * @return { "cash": "100", "money": "10", "point": "50"}
     */
    private Map<String, String> allocation(JsonObject queryCardJson, JsonObject requestData, String location,
                                           IntTbLookupCode intTbLookupCode, boolean verificationRule, Device device) {
        Map<String, String> map = new HashMap<String, String>();

        try {// 新增地區，method請按照命名allocationFor+lookupCode.Type1
            Method method = this.getClass().getDeclaredMethod("allocationFor" + (StringUtils.isBlank(location) ? "TC" : location),
                    new Class[]{JsonObject.class, JsonObject.class, IntTbLookupCode.class, Boolean.class, Device.class});
            method.setAccessible(true);
            map = (Map<String, String>) method.invoke(this,
                    new Object[]{queryCardJson, requestData, intTbLookupCode, verificationRule, device});
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return map;
    }

}
