package co.intella.service;

import java.util.Map;

import org.springframework.security.core.Authentication;

public interface UserService {
	Map<String, Object> getToken(String clientId, String code) throws Exception;
}
