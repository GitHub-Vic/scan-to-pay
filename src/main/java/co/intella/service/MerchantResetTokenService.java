package co.intella.service;

import co.intella.model.MerchantResetToken;

/**
 * @author Alex
 */

public interface MerchantResetTokenService {
    void save(MerchantResetToken merchantResetToken);

    MerchantResetToken getOne(String accountId, String restToken);

}
