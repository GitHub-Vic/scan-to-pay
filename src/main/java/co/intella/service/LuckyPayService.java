package co.intella.service;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.domain.luckypay.LuckyPayNotifyRequestData;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;

public interface LuckyPayService {

    String signValueGenerate(String requestData) throws Exception;

    String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;
    
    String orderReverse(PaymentAccount luckyPayAccount, TradeDetail tradeDetail, String [] hashIV) throws Exception;
    
    String orderQuery(PaymentAccount luckyPayAccount, TradeDetail tradeDetail, String[] hashIV)throws Exception;
    
    String orderRefund(PaymentAccount luckyPayAccount, TradeDetail tradeDetail, String[] hashIV) throws Exception;
    
    PaymentAccount updateKeyExpiry(PaymentAccount luckyPayAccount);
    
    PaymentAccount terminalRegister(PaymentAccount luckyPayAccount)throws Exception;
    
    void terminalUnregister(PaymentAccount luckyPayAccount)throws Exception;
    
    void terminalRegisterConfirm(PaymentAccount luckyPayAccount) throws Exception;
    
}
