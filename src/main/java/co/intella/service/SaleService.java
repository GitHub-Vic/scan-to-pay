package co.intella.service;

import co.intella.model.Sale;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;

/**
 * @author Andy Lin
 */
public interface SaleService {

    Sale login(String accountId, String pin) throws JsonProcessingException;

    List<Sale> queryWithLike(String accountId, String name);

    Sale getOneByAccountId(String accountId);

    Sale getOneById(long id);

    List<Sale> getListByType(int type);

    List<Sale> getListAll();

    Sale save(Sale sale);

    void delete(Sale sale);

    Sale update(Sale sale);
}
