package co.intella.service;

import co.intella.domain.weixin.*;

/**
 * @author Miles Wu
 */
public interface WeixinService {

    String payment(WeixinRequestParameter request) throws Exception;

    String faceToFace(WeixinFtfRequestData request) throws Exception;

    String callJsApi(WeixinJsapiRequestData request) throws Exception;

    String callJsApiNoReturn(WeixinJsapiRequestData request) throws Exception;

    String micropay(WeixinMicropayRequestData request) throws Exception;

    String orderQuery(WeixinOrderQueryRequestData request) throws Exception;

    String refund(WeixinRefundRequestData request) throws Exception;

    String singleQuery(WeixinSingleQueryRequestData input) throws Exception;

    String rateQuery(WeixinCurrencyRequestData input) throws Exception;

    String getPayCode(String merchantId, String orderId) throws Exception;

    String updateOrder(WeixinSingleQueryRequestData request) throws Exception;


}
