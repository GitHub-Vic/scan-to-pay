package co.intella.service.impl;

import co.intella.model.Permission;
import co.intella.model.QPermission;
import co.intella.repository.PermissionRepository;
import co.intella.service.PermissionService;
import com.google.common.collect.Lists;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Andy Lin
 */
@Service
@Transactional
public class PermissionServiceImpl implements PermissionService{
    @Resource
    private PermissionRepository permissionRepository;

    public Permission getOneById(long id) {
        QPermission qPermission = QPermission.permission;
        BooleanExpression predicate = qPermission.id.eq(id);
        return permissionRepository.findOne(predicate);
    }

    public List<Permission> getListByRoleId(long roleId) {
        QPermission qPermission = QPermission.permission;
        BooleanExpression predicate = qPermission.roleId.eq(roleId);
        return Lists.newArrayList(permissionRepository.findAll(predicate));
    }

    public List<Permission> listAll() {
        return permissionRepository.findAll();
    }

    public Permission save(Permission permission) {
        return permissionRepository.save(permission);
    }

    public void delete(Permission permission) {
        permissionRepository.delete(permission);
    }
}
