package co.intella.service.impl;

import co.intella.constant.Bank;
import co.intella.domain.HNCBTwpay.*;
import co.intella.domain.integration.IntegratedMicropayResponseData;
import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.exception.InvokeAPIException;
import co.intella.exception.ServiceTypeNotSupportException;
import co.intella.model.*;
import co.intella.service.*;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.xml.bind.DatatypeConverter;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class HNTwpayServiceImpl implements HNTwpayService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private OrderService orderService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Autowired
    private AppNotificationService appNotificationService;

    @Value("${hn.twpay.url}")
    String HN_TWPAY_URL;

    @Value("${api.url}")
    private String API_URL;

    private Locale locale = new Locale("zh_TW");

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private Environment env;

    @Override
    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {
        String generalResponseData = SystemInstance.EMPTY_STRING;

        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        Merchant merchant = merchantService.getOne(requestHeader.getMerchantId());
        PaymentAccount paymentAccount = paymentAccountService.getOneByDao(integrateMchId, bankId);
        if (Objects.isNull(paymentAccount)) {
            throw new ServiceTypeNotSupportException("Exception: The store's account isn't opened.");
        }

        String serviceType = requestHeader.getServiceType();
        switch (serviceType) {
            case SystemInstance.TYPE_MICROPAY:
                generalResponseData = this.doMicropay(requestHeader, data, merchant, paymentAccount);
                break;
            case "Refund":
                generalResponseData = this.doRefund(requestHeader, data, merchant, paymentAccount);
                break;
            case "SingleOrderQuery":
                this.queryMerchantOrder(requestHeader, data, merchant, paymentAccount); //由IntegratedController 那邊重新抓取訂單狀態回復
                break;
            default:
                LOGGER.error("Service type not support. " + serviceType);
                generalResponseData = "Service type not support.";
        }

        return generalResponseData;
    }

    @Override
    public String getPaymentUrl(String amount, String shortId, String agent) throws Exception {
        String result = "";
        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(shortId);

        if (Objects.nonNull(qrcodeParameter)) {
            Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());
            PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), Bank.HNTWPAY.getId());

            if (Objects.isNull(paymentAccount)) {
                throw new ServiceTypeNotSupportException("Exception: The store's account isn't opened.");
            }

            TradeDetail tradeDetail = this.setOnlineTradeDetailRequest(amount, merchant, paymentAccount, qrcodeParameter);
            PreOrderRequest request = new PreOrderRequest(paymentAccount, tradeDetail);
            request.setNoticeURL(API_URL + "allpaypass/api/hnTwPay/notify");
            request.setSign(this.getSign(JsonUtil.toJsonStr(request), paymentAccount));

            String requestJsonStr = JsonUtil.toJsonStr(request);
            LOGGER.info("[HNTwpay][getPaymentUrl]  requestJsonStr" + requestJsonStr);
            String resultStr = this.send(requestJsonStr, "preOrder");
            LOGGER.info("[HNTwpay][getPaymentUrl]  resultStr" + resultStr);

            PreOrderResponse preOrderResponse = JsonUtil.parseJson(resultStr, PreOrderResponse.class);

            if (Objects.nonNull(preOrderResponse)) {
                result = preOrderResponse.getQrCodeURL();
            }
        } else {
            LOGGER.error("[REQUEST][GamaPay] qrcode not found");
        }
        return result;
    }

    private TradeDetail setOnlineTradeDetailRequest(String amount, Merchant merchant,
                                                    PaymentAccount paymentAccount, QrcodeParameter qrcodeParameter) {
        String systemOrderId = orderService.createOrderIdBySequence(qrcodeParameter.getMchId());
        String storeOrderNo = "1".equals(qrcodeParameter.getCodeType()) ?
                qrcodeParameter.getStoreOrderNo() : systemOrderId;

        TradeDetail tradeDetail = tradeDetailService.getOne(merchant.getAccountId(), storeOrderNo);
        if (tradeDetail != null) {
            return tradeDetail;
        } else {
            tradeDetail = new TradeDetail();
        }
        tradeDetail.setAccountId(merchant.getAccountId());
        tradeDetail.setMethod("14500");
        tradeDetail.setPaymentAccount(paymentAccount);
        tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        tradeDetail.setStatus("Creating order");

        tradeDetail.setOrderId(storeOrderNo);
        tradeDetail.setSystemOrderId(systemOrderId);
        tradeDetail.setDeviceRandomId(deviceService.getOne(merchant, "APP-001"));
        tradeDetail.setPayment(Long.valueOf(amount));
        tradeDetail.setServiceType("OLPay");
        tradeDetail.setQrcodeToken(qrcodeParameter.getShortId());
        tradeDetail.setDescription(qrcodeParameter.getBody());
        tradeDetail.setDetail(qrcodeParameter.getDetail());
        tradeDetail.setOnSale(false);
        tradeDetail.setOriginalPrice(qrcodeParameter.getTotalFee());

        tradeDetail = tradeDetailService.save(tradeDetail);

        return tradeDetail;
    }


    private String getSign(String requestJsonStr, PaymentAccount paymentAccount) throws Exception {
        Map<String, String> requestMap = JsonUtil.parseJson(requestJsonStr, Map.class);
        Map<String, String> signMap = new TreeMap<>();
        signMap.putAll(requestMap);
        String signStr = signMap.entrySet().stream().filter(es -> StringUtils.isNotBlank(es.getValue()))
                .map(es -> es.getKey() + "=" + es.getValue()).collect(Collectors.joining("&"));

        LOGGER.info("signStr =>" + signStr);
        String base64SignStr = Base64.getEncoder().encodeToString(signStr.getBytes());
        LOGGER.info("base64SignStr =>" + base64SignStr);
        return this.encryptSHA256(base64SignStr + StringUtils.defaultIfBlank(paymentAccount.getHashKey(), ""));
    }

    private String encryptSHA256(String content) throws Exception {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(content.getBytes(StandardCharsets.UTF_8));

            return DatatypeConverter.printHexBinary(hash).toLowerCase();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new RuntimeException("encryptSHA256 fail", e);
        }
    }

    private String send(String requestJsonStr, String serviceName) throws InvokeAPIException {
        String result = "";
        try {
            result = HttpUtil.doPostJson(HN_TWPAY_URL + serviceName, requestJsonStr);
        } catch (Exception e) {
            LOGGER.error("[HN_TWPAY][EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            throw new InvokeAPIException(this.getClass().getSimpleName(), e.getMessage());
        }
        if (StringUtils.isEmpty(result)) {
            throw new InvokeAPIException(this.getClass().getSimpleName(), messageSource.getMessage("response.7350", null, locale));
        }
        return result;
    }

    private String doMicropay(RequestHeader requestHeader, JsonPrimitive data, Merchant merchant, PaymentAccount paymentAccount) throws Exception {

        TradeDetail tradeDetail = this.getTradeDetail(requestHeader, data);

        OrderRequest orderRequest = new OrderRequest(paymentAccount, tradeDetail);
        orderRequest.setSign(this.getSign(JsonUtil.toJsonStr(orderRequest), paymentAccount));

        String requestJsonStr = JsonUtil.toJsonStr(orderRequest);
        LOGGER.info("[HNTwpay][doMicropay]  requestJsonStr" + requestJsonStr);
        String resultStr = this.send(requestJsonStr, "order");
        LOGGER.info("[HNTwpay][doMicropay]  resultStr" + resultStr);

        OrderResponse response = JsonUtil.parseJson(resultStr, OrderResponse.class);

        if ("0000".equals(response.getRespCode())) {
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetail.setSystemOrderId(response.getTxnSeqno());
            appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
        } else {
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
        }
        tradeDetail.setTxParams(resultStr);
        tradeDetailService.save(tradeDetail);

        ResponseGeneralBody responseGeneralBody = this.getResponseBody(requestHeader, tradeDetail, response.getRespCode());
        IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();
        integratedMicropayResponseData.setPlatformRsp(response.getTxnDateTime());
        integratedMicropayResponseData.setAuthCode(tradeDetail.getBarcode());
        integratedMicropayResponseData.setStoreOrderNo(tradeDetail.getOrderId());
        integratedMicropayResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
        integratedMicropayResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
        responseGeneralBody.setData(integratedMicropayResponseData);

        return JsonUtil.toJsonStr(responseGeneralBody);
    }

    private String doRefund(RequestHeader requestHeader, JsonPrimitive data, Merchant merchant, PaymentAccount paymentAccount) throws Exception {

        TradeDetail tradeDetail = this.getTradeDetail(requestHeader, data);

        RefundResquest refundResquest = new RefundResquest(tradeDetail, paymentAccount);
        refundResquest.setSign(this.getSign(JsonUtil.toJsonStr(refundResquest), paymentAccount));
        String requestJsonStr = JsonUtil.toJsonStr(refundResquest);

        LOGGER.info("[HNTwpay][doRefund]  requestJsonStr" + requestJsonStr);
        String resultStr = this.send(requestJsonStr, "refund");
        LOGGER.info("[HNTwpay][doRefund]  resultStr" + resultStr);

        RefundResponse refundResponse = JsonUtil.parseJson(resultStr, RefundResponse.class);

        if ("4001".equals(refundResponse.getRespCode())) {
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
            tradeDetail.setSystemRefundId(refundResponse.getTxnSeqno());
        } else {
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
        }
        tradeDetail = tradeDetailService.save(tradeDetail);
        tradeProcedureService.updateRefundDeatil(tradeDetail, new Gson().fromJson(data.getAsString(), JsonObject.class), tradeDetail.getRefundStatus());

        ResponseGeneralBody responseGeneralBody = this.getResponseBody(requestHeader, tradeDetail, refundResponse.getRespCode());
        IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();
        integratedRefundResponseData.setPlatformRsp(refundResponse.getTxnDateTime());
        integratedRefundResponseData.setRefundedAt(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        integratedRefundResponseData.setStoreOrderNo(tradeDetail.getOrderId());
        integratedRefundResponseData.setStoreRefundNo(tradeDetail.getStoreRefundId());
        integratedRefundResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
        integratedRefundResponseData.setSysRefundNo(tradeDetail.getSystemRefundId());
        responseGeneralBody.setData(integratedRefundResponseData);

        return JsonUtil.toJsonStr(responseGeneralBody);
    }

    private void queryMerchantOrder(RequestHeader requestHeader, JsonPrimitive data, Merchant merchant, PaymentAccount paymentAccount) throws Exception {

        TradeDetail tradeDetail = this.getTradeDetail(requestHeader, data);

        InqueryRequest inqueryRequest = new InqueryRequest(tradeDetail, paymentAccount);
        inqueryRequest.setSign(this.getSign(JsonUtil.toJsonStr(inqueryRequest), paymentAccount));

        String requestJsonStr = JsonUtil.toJsonStr(inqueryRequest);
        LOGGER.info("[HNTwpay][queryMerchantOrder]  requestJsonStr" + requestJsonStr);
        String resultStr = this.send(requestJsonStr, "order");
        LOGGER.info("[HNTwpay][queryMerchantOrder]  resultStr" + resultStr);

        InqueryResponse inqueryResponse = JsonUtil.parseJson(resultStr, InqueryResponse.class);

        if ("2541".equals(inqueryResponse.getTxnType())
                && SystemInstance.STATUS_SUCCESS.equals(this.changeScan2payErrCode(inqueryResponse.getRespCode()))) {
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
        } else if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())
                && "2541".equals(inqueryResponse.getTxnType())
                && !SystemInstance.STATUS_SUCCESS.equals(this.changeScan2payErrCode(inqueryResponse.getRespCode()))) {
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
        } else if ("2543".equals(inqueryResponse.getTxnType())
                && SystemInstance.STATUS_SUCCESS.equals(this.changeScan2payErrCode(inqueryResponse.getRespCode()))) {
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
            tradeProcedureService.updateRefundDeatil(tradeDetail, new Gson().fromJson(data.getAsString(), JsonObject.class), SystemInstance.TRADE_REFUND_SUCCESS);
        }
        tradeDetailService.save(tradeDetail);
    }

    private String changeScan2payErrCode(String hnErrCode) {
        String scan2PayCode;
        switch (hnErrCode) {
            case "0000":
            case "4001":
                scan2PayCode = SystemInstance.STATUS_SUCCESS;
                break;
            case "9999":
                scan2PayCode = SystemInstance.STATUS_FAIL;
                break;
            case "0018":
            case "0302":
                scan2PayCode = "7202";
                break;
            case "0101":
                scan2PayCode = "8002";
                break;
            case "0601":
                scan2PayCode = "7305";
                break;
            default:
                scan2PayCode = "9998";
                break;
        }
        return scan2PayCode;
    }

    private TradeDetail getTradeDetail(RequestHeader requestHeader, JsonPrimitive data) throws Exception {
        String mchId = requestHeader.getMerchantId();
        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
        requestHeader.setMerchantId(mchId);

        return tradeDetail;
    }

    private ResponseGeneralBody getResponseBody(RequestHeader requestHeader, TradeDetail tradeDetail, String returnCode) {
        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        ResponseGeneralHeader header = new ResponseGeneralHeader();

        String intellaCode = this.changeScan2payErrCode(returnCode);
        header.setStatusCode(intellaCode);
        header.setStatusDesc(messageSource.getMessage("response." + intellaCode, null, locale));
        header.setMethod(tradeDetail.getMethod());
        header.setMchId(tradeDetail.getPaymentAccount().getMerchant().getAccountId());
        header.setServiceType(requestHeader.getServiceType());
        header.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        responseGeneralBody.setHeader(header);

        return responseGeneralBody;
    }
}
