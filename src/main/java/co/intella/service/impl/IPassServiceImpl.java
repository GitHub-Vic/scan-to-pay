package co.intella.service.impl;

import co.intella.domain.CheckoutCtrlVo;
import co.intella.domain.contratStore.*;
import co.intella.domain.easycard.EzCardReceipt;
import co.intella.domain.iPass.*;
import co.intella.domain.icash.IcashDeviceQueryResponse;
import co.intella.domain.icash.IcashOrderQueryResponse;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralData;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.exception.*;
import co.intella.model.*;
import co.intella.net.HttpRequestUtil;
import co.intella.repository.CheckoutRepository;
import co.intella.service.*;
import co.intella.utility.HttpUtil;
import co.intella.utility.IPassUtil;
import co.intella.utility.JsonUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Strings;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class IPassServiceImpl implements IPassService {

    private final Logger LOGGER = LoggerFactory.getLogger(IPassServiceImpl.class);

    @Value("${host.iPass}")
    private String HOST;

    @Value("${host.nonTaxi.iPass}")
    private String NON_TAXI_HOST;

    @Value("${host.request.ezc}")
    private String EZC_HOST;

    @Value("${host.iPass.Blacklist}")
    private String IPASS_BLACKLIST_URL;

    @Value("${host.iPass.Upload}")
    private String IPASS_UPLOAD_URL;

    @Value("${host.iPass.Checkout}")
    private String IPASS_CHECKOUT_URL;

    @Value("${host.welfarepoint.query}")
    private String WELFAREPOINT_QUERY_URL;

    @Value("${host.taxi.record}")
    private String TAXI_RECORD_INSERT_API_URL;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private ReserveDetailService reserveDetailService;

    @Resource
    private RefundDetailService refundDetailService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private ReserveCancelService reserveCancelService;

    @Resource
    private OperationHistoryService operationHistoryService;

    @Resource
    private LookupCodeService lookupCodeService;

    @Resource
    private CheckoutRepository checkoutRepository;

    @Resource
    private INT_TB_CheckoutCtrlService INT_TB_CheckoutCtrlService;

    @Resource
    private INT_TB_CheckoutLogService INT_TB_CheckoutLogService;

    @Resource
    private TradeDetailOtherInfoService tradeDetailOtherInfoService;

    @Resource
    private TaxiTransactionDetailService taxiTransactionDetailService;

    @Resource
    private MerchantService merchantService;

    private Locale locale = new Locale("zh_TW");

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private Environment env;

    @Value("${iPass.TestMode}")
    private String testMode;
    // Prod = 0
    // TestMode = 1

    @Value("${iPass.ClientIP}")
    private String TAXI_CLIENT_IP;// test ip = 35.236.129.13

    @Value("${iPass.NONTaxi.ClientIP}")
    private String NON_TAXI_CLIENT_IP;

    @Resource(type = co.intella.service.impl.IpassTaxiPoint.class)
    private TaxiPoint taxiPoint;

    private String systemID = "A4";

    private String multiElectronicTicket = "TEST_IPASS";

    final static String uartMode = "0";

    private String samSlotNumber = "2";

    private String fareFlag = "0";

    public String NonTransactionRequest(IPassGeneralRequest iPassGeneralRequest) {

        HttpURLConnection conn = null;

        LOGGER.info("[IPASS] doRequest(). " + iPassGeneralRequest.getServiceType());
        if (iPassGeneralRequest.getServiceType().equals("Blacklist")
                || iPassGeneralRequest.getServiceType().equals("Upload")
                || iPassGeneralRequest.getServiceType().equals("Checkout")) {
            try {
                if (iPassGeneralRequest.getServiceType().equals("Blacklist")) {
                    conn = (HttpURLConnection) (new URL(IPASS_BLACKLIST_URL)).openConnection();
                    LOGGER.info("[IPASS] doRequest(). " + IPASS_BLACKLIST_URL);
                } else if (iPassGeneralRequest.getServiceType().equals("Upload")) {
                    conn = (HttpURLConnection) (new URL(IPASS_UPLOAD_URL)).openConnection();
                    LOGGER.info("[IPASS] doRequest(). " + IPASS_UPLOAD_URL);
                } else {
                    conn = (HttpURLConnection) (new URL(IPASS_CHECKOUT_URL)).openConnection();
                    LOGGER.info("[IPASS] doRequest(). " + IPASS_CHECKOUT_URL);
                }
            } catch (Exception e) {

                LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
                IPassGeneralResponse connError = new IPassGeneralResponse();
                connError.setErrorCode("909995");
                connError.setTxnResult("Fail");
                return new Gson().toJson(connError);
            }

            iPassGeneralRequest.setServiceType("");
        }

        // ObjectMapper mapper = new ObjectMapper();
        String requestData = new Gson().toJson(iPassGeneralRequest);

        LOGGER.info("[IPASS][Request]: " + requestData);

        try {

            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.getOutputStream().write(requestData.getBytes("UTF-8"));
            conn.getOutputStream().flush();
            conn.getOutputStream().close();
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            StringBuilder text = new StringBuilder();

            String line;
            while ((line = rd.readLine()) != null) {
                text.append(line);

                // if (line.length() < 1 && line.charAt(0) == '\ufeff') {
                // text.deleteCharAt(0);
                // }
            }
            // System.out.println(text);

            LOGGER.info("[IPASS][RESPONSE] doRequest(). " + text.toString().replaceAll("\\s", ""));

            return text.toString().replaceAll("\\s", "");

            // String response = HttpRequestUtil.post(HOST, requestData);
            //
            // LOGGER.info("[IPASS][RESPONSE] doRequest(). " + response);
            //
            // return response;
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
            IPassGeneralResponse error = new IPassGeneralResponse();
            error.setErrorCode("909995");
            error.setTxnResult("Fail");
            return new Gson().toJson(error);
        }
    }

    @Override
    public String doRequest(IPassGeneralRequest iPassGeneralRequest, String aesKey) {
        String result;
        try {
            iPassGeneralRequest.setAesKey(aesKey);
            result = this.sendPost(HOST, new Gson().toJson(iPassGeneralRequest));
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
            IPassGeneralResponse connError = new IPassGeneralResponse();
            connError.setErrorCode("909995");
            connError.setTxnResult("Fail");
            result = new Gson().toJson(connError);
        }
        return result;
    }

    @Override
    public String ezRequest(RequestHeader requestHeader, JsonObject requestData, Merchant merchant)
            throws Exception {

        String deviceId = requestData.get("DeviceId").getAsString();
        Device device = deviceService.getOne(merchant, deviceId);
        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        PaymentAccount paymentAccount = paymentAccountService.getOne(integrateMchId, bankId);
        LOGGER.info("[REQUEST][IPASS] " + merchant.getAccountId() + ", " + deviceId);

        if (Objects.isNull(device) && !deviceId.equals("00000000")) {
            LOGGER.error("[REQUEST][IPASS] device not found. " + deviceId + ", " + merchant.getAccountId());
            throw new DeviceNotFoundException("device not found");
        } else if (!deviceId.equals("00000000") && "1".equals(device.getBan())) {
            LOGGER.error("[REQUEST][IPASS] device lock. " + deviceId + ", " + merchant.getAccountId());
            throw new DeviceNotFoundException("Device Locked");
        }

        //TODO  有新增serviceType 如果必須要有卡片的相關serviceType，請去TicketLogicServiceImpl 也增加到相對的 private static final String[]
        String resultStr;
        String serviceType = requestHeader.getServiceType();
        switch (serviceType) {
            case "QueryCardInfo":
                resultStr = this.queryCard(device, paymentAccount, requestHeader);
                break;
            case "FindCard":
                resultStr = this.findCard(device, paymentAccount, requestHeader);
                break;
            case "TradeSale":
                resultStr = this.tradeSale(requestHeader, requestData, device, paymentAccount, merchant);
                break;
            case "Blacklist":
                resultStr = this.blacklist();
                break;
            case "Upload":
                resultStr = this.uploadFile();
                break;
            case "Checkout":
                resultStr = this.checkout(requestData.get("FileSN").getAsString());
                break;
            case "TaxiLogin":
                resultStr = this.termAuth(device, paymentAccount, requestHeader);
                break;
            case SystemInstance.ServiceType.TICKET_LOGIC:
                resultStr = this.ticketLogic(requestHeader, requestData, device, paymentAccount, merchant);
                break;

            //       TODO *************************       以上為計程車 相關 ， 以下為新增ipass小額(含停車場)用        *************************

            case "TermAuth":    //登入
                resultStr = this.doTermAuth(requestHeader, requestData, device, paymentAccount, merchant);
                break;
            case "Payment": //扣款(含自動加值)
                resultStr = this.doTradeSale(requestHeader, requestData, device, paymentAccount, merchant);
                break;
            case "Refund":    //退款
                resultStr = this.doTradeRefund(device, paymentAccount, requestHeader, requestData, merchant);
                break;
            case "Reserve":    //手動加值             // TODO
                resultStr = this.doCashTopUp(device, paymentAccount, requestHeader, requestData, merchant);
                break;
            case "TradeChargeCancel":    //手動加值取消             // TODO
                resultStr = this.doCancelCashTopUp(device, paymentAccount, requestHeader, requestData, merchant);
                break;
            case "OrderQuery":    // TODO  ezc抄來的
                resultStr = this.doOrderQuery(requestHeader, requestData, merchant, device, paymentAccount);
                break;
            case "DeviceQuery": // TODO  ezc抄來的
                resultStr = this.doDeviceQuery(merchant, requestHeader);
                break;
            case "IdQuery":
                resultStr = this.doIdQuery(device, paymentAccount, requestHeader, requestData);
                break;
            case "BalanceQuery":  //讀卡 (線上)
                resultStr = this.doQueryCardInfo(device, paymentAccount, requestHeader, requestData);
                break;
            default:
                throw new ServiceTypeNotSupportException("ServiceType Not Support.");
        }
        return resultStr;
    }

    private String getDate() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    }

    private String getTime() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
    }

    private String getBatchNumber(String date, String batch) {
        return date.substring(2, date.length()) + batch;
    }

    private String termAuth(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader) {

        LOGGER.info(
                "[REQUEST][IPASS][TermAuth][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");

        String date = getDate();
        String time = getTime();

        IPassAuthRequest termAuthRequest = new IPassAuthRequest();

        termAuthRequest.setTestMode(testMode);
        termAuthRequest.setUartMode(uartMode);
        termAuthRequest.setSystemID(paymentAccount.getSystemID());
        termAuthRequest.setSpID(paymentAccount.getSpID());
        if (!Strings.isNullOrEmpty(device.getLastusedate())) {
            if (device.getLastusedate().equals(date)) {
                termAuthRequest.setPosTradeSN(String.valueOf(Integer.valueOf(device.getPostradesn()) + 1));
                device.setPostradesn(String.valueOf(Integer.valueOf(device.getPostradesn()) + 1));
            } else {
                termAuthRequest.setPosTradeSN("1");
                device.setLastusedate(date);
                device.setPostradesn("1");
            }
        } else {
            termAuthRequest.setPosTradeSN("1");
            device.setLastusedate(date);
            device.setPostradesn("1");
        }
        // termAuthRequest.setPosTradeSN(String.valueOf(Integer.valueOf(paymentAccount.getPlatformCheckCode())+1));
        termAuthRequest.setShopNumber("0");
        termAuthRequest.setSamSlotNumber(samSlotNumber);
        termAuthRequest.setTime(time);
        termAuthRequest.setDate(date);
        termAuthRequest.setTerminalID(paymentAccount.getAccount());
        termAuthRequest.setDeviceID(device.getOwnerDeviceId());
        termAuthRequest.setClientIP(TAXI_CLIENT_IP);
        termAuthRequest.setMultiElectronicTicket(multiElectronicTicket);

        System.out.println(new Gson().toJson(termAuthRequest));

        // termAuthRequest.setAesKey(device.getAesKey());

        IPassAuthResponse response = new Gson().fromJson(doRequest(termAuthRequest, device.getAesKey()),
                IPassAuthResponse.class);

        if (!Strings.isNullOrEmpty(response.getNewAESKey())) {
            device.setAesKey(response.getNewAESKey());
        }
        deviceService.save(device);

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    private String queryCard(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader) {

        LOGGER.info("[REQUEST][IPASS][Query][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");

        String date = getDate();
        String time = getTime();

        IPassQueryRequest queryRequest = new IPassQueryRequest();

        queryRequest.setTestMode(testMode);
        queryRequest.setUartMode(uartMode);
        queryRequest.setSystemID(paymentAccount.getSystemID());
        queryRequest.setSpID(paymentAccount.getSpID());
        queryRequest.setTime(time);
        queryRequest.setDate(date);
        queryRequest.setTerminalID(paymentAccount.getAccount());
        queryRequest.setDeviceID(device.getOwnerDeviceId());
        queryRequest.setMultiElectronicTicket(multiElectronicTicket);
        queryRequest.setLicensePlate(device.getLicensePlate());

        // termAuthRequest.setAesKey(device.getAesKey());
        String result = doRequest(queryRequest, device.getAesKey());
        IPassQueryResponse response = new Gson().fromJson(result,
                IPassQueryResponse.class);

        String discount = "1";
        String maxPointPerTrip = "0";
        String usePointLimit = "0";
        try {
            maxPointPerTrip = StringUtils.defaultIfBlank(response.getSpecialIdentityUsagePointLimit(), "0");
            usePointLimit = taxiPoint.getlLcationUsePointLimit(device.getIpasslocation());

        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
        }

        response.setSpecialIdentityPointRatio(discount.replaceAll("\"", ""));
        response.setMaxPointPerTrip(maxPointPerTrip.replaceAll("\"", ""));
        response.setUsePointLimit(usePointLimit.replaceAll("\"", ""));

        if (!Strings.isNullOrEmpty(response.getNewAESKey())) {
            device.setAesKey(response.getNewAESKey());
            deviceService.save(device);
        }

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    /**
     * 原本的尋卡寫在這，後來有新的尋卡API，因票證共用，寫在TicketLogicServiceImpl 內
     *
     * @param device
     * @param paymentAccount
     * @param requestHeader
     * @return
     */
    @Deprecated
    private String findCard(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader) {

        LOGGER.info(
                "[REQUEST][IPASS][FindCard][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");

        String date = getDate();
        String time = getTime();

        IPassFindCardRequest queryRequest = new IPassFindCardRequest();

        queryRequest.setTestMode(testMode);
        queryRequest.setUartMode(uartMode);
        queryRequest.setSystemID(paymentAccount.getSystemID());
        queryRequest.setSpID(paymentAccount.getSpID());
        queryRequest.setTime(time);
        queryRequest.setDate(date);
        queryRequest.setTerminalID(paymentAccount.getAccount());
        queryRequest.setDeviceID(device.getOwnerDeviceId());
        queryRequest.setMultiElectronicTicket(multiElectronicTicket);

        // termAuthRequest.setAesKey(device.getAesKey());

        IPassFindCardResponse response = new Gson().fromJson(doRequest(queryRequest, device.getAesKey()),
                IPassFindCardResponse.class);

        if (!Strings.isNullOrEmpty(response.getNewAESKey())) {
            device.setAesKey(response.getNewAESKey());
            deviceService.save(device);
        }
        // paymentAccount.setPlatformCheckCode(String.valueOf(Integer.valueOf(paymentAccount.getPlatformCheckCode())+1));
        // paymentAccountService.save(paymentAccount);

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    private String tradeSale(RequestHeader requestHeader, JsonObject requestData, Device device,
                             PaymentAccount paymentAccount, Merchant merchant) throws OtherAPIException {

        LOGGER.info(
                "[REQUEST][IPASS][TradeSale][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");

        String date = getDate();
        String time = getTime();

        long isRetry = requestData.get("Retry").getAsLong();

        String orderId = requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString();
        String batchNo = device.getBatchNumber().toString();
        if (!Strings.isNullOrEmpty(batchNo)) {
            batchNo = getBatchNumber(date, String.format("%02d", device.getBatchNumber()));
        }

        TradeDetail tradeDetail;
        String userId = "-1";

        if (isRetry > 0 && isRetry < 4) {
            tradeDetail = tradeDetailService.getOne(orderId);
            userId = tradeDetail.getUserId();
        } else if (isRetry >= 4) {

            LOGGER.error("[REQUEST][IPASS] STOP RETRY. " + device.getOwnerDeviceId() + ", " + merchant.getAccountId());
            IPassErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Already retry 3 times.",
                    "900003");
            return getErrorResponse(requestHeader, errorResponse, "Already retry 3 times.");
        } else {
            tradeDetail = tradeDetailService.getOne(orderId);

            if (tradeDetail != null) {
                IPassErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Store Order No duplicated",
                        "7000");
                return getErrorResponse(requestHeader, errorResponse, "Store Order No duplicated.");
            }

            userId = requestData.get("CardSNFull").getAsString();

            if (Strings.isNullOrEmpty(userId) || userId.equals("-1")) {
                LOGGER.error("[REQUEST][IPASS] Card id not found. " + device.getOwnerDeviceId() + ", "
                        + merchant.getAccountId());
                IPassErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Card id not found.",
                        "0462xx");
                return getErrorResponse(requestHeader, errorResponse, "Can not read card info.");
            }

            tradeDetail = new TradeDetail();
            tradeDetail.setStatus(SystemInstance.TRADE_CREATING_ORDER);
            tradeDetail.setOrderId(orderId);
            tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
            tradeDetail.setAccountId(requestHeader.getMerchantId());
            tradeDetail.setPayment(Long.valueOf(requestData.get("PaidAmount").getAsString()));
            tradeDetail.setDeviceRandomId(device);
            tradeDetail.setPaymentAccount(paymentAccount);
            tradeDetail.setMethod("32100");
            tradeDetail.setSystemOrderId(time);
            tradeDetail.setBatchNo(batchNo);
            tradeDetail.setUserId(userId);
            tradeDetail.setEzcTxnDate(date + time);
            tradeDetail.setCarePoint(Long.valueOf(requestData.get("SpecialIdentityTxnPoint").getAsString()));
            tradeDetail.setRemainCarePoint(Long.valueOf(requestData.get("PointBalance").getAsString()));
            tradeDetail.setComment(paymentAccount.getAccount() + device.getOwnerDeviceId());
            tradeDetail.setDescription("扣點: " + Long.valueOf(requestData.get("SpecialIdentityTxnPoint").getAsString())
                    + " 點, 扣款: " + Long.valueOf(requestData.get("PaidAmount").getAsString()) + " 元");

            tradeDetail = tradeDetailService.save(tradeDetail);
            if (Objects.isNull(tradeDetail)) {
                throw new OtherAPIException(messageSource.getMessage("response.7142", null, locale));
            }
        }

        IPassTradeRequest tradeRequest = new IPassTradeRequest();

        tradeRequest.setTestMode(testMode);
        tradeRequest.setUartMode(uartMode);
        tradeRequest.setSystemID(paymentAccount.getSystemID());
        tradeRequest.setSpID(paymentAccount.getSpID());
        if (!Strings.isNullOrEmpty(device.getLastusedate())) {
            if (device.getLastusedate().equals(date)) {
                tradeRequest.setPosTradeSN(String.valueOf(Integer.valueOf(device.getPostradesn()) + 1));
                device.setPostradesn(String.valueOf(Integer.valueOf(device.getPostradesn()) + 1));
            } else {
                tradeRequest.setPosTradeSN("1");
                device.setLastusedate(date);
                device.setPostradesn("1");
            }
        } else {
            tradeRequest.setPosTradeSN("1");
            device.setLastusedate(date);
            device.setPostradesn("1");
        }

        if (Long.valueOf(requestData.get("PaidAmount").getAsString()) == 0 && tradeDetail.getCarePoint() == 0) {
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetail.setEzcTxnDate(date + time);
            tradeDetailService.save(tradeDetail);
            IPassTradeResponse response = new IPassTradeResponse();
            response.setCardSN(userId);
            response.setSpecialCardType(requestData.get("SpecialCardType").getAsString());
            response.setSpecialIdentityType(requestData.get("SpecialIdentityType").getAsString());
            response.setSpecialIdentityProvider(requestData.get("SpecialIdentityProvider").getAsString());
            response.setIdentityType(requestData.get("IdentityType").getAsString());
            this.insertTaxiTransactionDetail(tradeDetail, merchant, paymentAccount, device, response, requestData, SystemInstance.TRADE_SUCCESS);

            throw new OtherAPIException(messageSource.getMessage("response.7354", null, locale));
        }

        tradeRequest.setAmount(requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString());
        tradeRequest.setSpecialIdentityTxnPoint(requestData.get("SpecialIdentityTxnPoint").getAsString());
        tradeRequest.setSpecialIdentityPointRatio(
                String.format("%02d", Integer.valueOf(requestData.get("SpecialIdentityPointRatio").getAsString())));
        tradeRequest.setSpecialIdentityDiscountAmount(requestData.get("SpecialIdentityDiscountAmount").getAsString());
        tradeRequest.setPaidAmount(requestData.get("PaidAmount").getAsString());
        tradeRequest.setFareFlag(fareFlag);
        tradeRequest.setNeedCash(requestData.get("NeedCash").getAsString());
        tradeRequest.setTransactionType(requestData.get("TransactionType").getAsString());
        tradeRequest.setSpecialIdentityResetFlag(requestData.get("SpecialIdentityResetFlag").getAsString());
        tradeRequest.setTime(time);
        tradeRequest.setDate(date);
        tradeRequest.setTerminalID(paymentAccount.getAccount());
        tradeRequest.setDeviceID(device.getOwnerDeviceId());
        tradeRequest.setMultiElectronicTicket(multiElectronicTicket);
        tradeRequest.setLicensePlate(device.getLicensePlate());

        // termAuthRequest.setAesKey(device.getAesKey());

        IPassTradeResponse response = new Gson().fromJson(doRequest(tradeRequest, device.getAesKey()),
                IPassTradeResponse.class);
        response.setTime(getTime());
        response.setDate(getDate());
        response.setOrderId(orderId);
//        response.setRequest(tradeRequest);
        response.setCarFleetName(merchant.getCompanyName());
        response.setLicensePlate(device.getLicensePlate());
        response.setDriverNumber(requestHeader.getMerchantId());
        response.setSpecialIdentityTxnPoint(requestData.get("SpecialIdentityTxnPoint").getAsString());
        response.setPointBalance(requestData.get("PointBalance").getAsString());
        response.setPaidAmount(requestData.get("PaidAmount").getAsString());

        response.setBeforepoint(String.valueOf(Integer.valueOf(requestData.get("PointBalance").getAsString()) + Integer.valueOf(requestData.get("SpecialIdentityTxnPoint").getAsString())));
        response.setDiscountAmount(requestData.get("SpecialIdentityDiscountAmount").getAsString());
        response.setNeedBalance(requestData.get("NeedCash").getAsString());

        IntTbLookupCode lookupCode = lookupCodeService.findOne("ticketType", "21");
        response.setCardId("01");
        response.setCardIdDscr(lookupCode.getDscr());
        response.setTotal(requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString());

        if (!Strings.isNullOrEmpty(response.getNewAESKey())) {
            device.setAesKey(response.getNewAESKey());

        }
        deviceService.save(device);
        // paymentAccount.setPlatformCheckCode(String.valueOf(Integer.valueOf(paymentAccount.getPlatformCheckCode())+1));
        // paymentAccountService.save(paymentAccount);

        // 20190416 edit:Sol 不管是否扣款成功,都寫自動充值紀錄
        if (StringUtils.isNotBlank(response.getAutoTopUpAmount()) && !"0".equals(response.getAutoTopUpAmount())) {
            this.saveReserveDetail(batchNo, requestData, requestHeader, orderId, response, device, userId, date, 0, 1, time);
        }
        Map<String, String> taxiTransactionDetail = new HashMap<String, String>();
        String taxiTransactionDetailStatus = "";
        tradeDetail = tradeDetailService.getOne(tradeDetail.getAccountId(), tradeDetail.getOrderId());
        if (response.getTxnResult().equals("Success")) {
            LOGGER.info("[RESPONSE][ACER][Success] " + new Gson().toJson(response));
            tradeDetail.setMethod("32100");
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetail.setRrn(date + time);
            tradeDetail.setEzcTxnDate(date + time);
            tradeDetailService.save(tradeDetail);
            taxiTransactionDetailStatus = SystemInstance.TRADE_SUCCESS;
            appNotificationService.notifyApp(tradeDetail, merchant);

            // printReceipt(acerTaxiPaymentResponse, device, paymentAccount)

        } else {
            LOGGER.error("[RESPONSE][ACER][Fail] " + new Gson().toJson(response));
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            tradeDetailService.save(tradeDetail);
            taxiTransactionDetailStatus = SystemInstance.TRADE_FAIL;
        }
        this.insertTaxiTransactionDetail(tradeDetail, merchant, paymentAccount, device, response, requestData, taxiTransactionDetailStatus);
        response.setCardSNFull(response.getCardSN());
        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    private String checkout(String filesn) {

        LOGGER.info("[REQUEST][IPASS] Checkout");

        List<PaymentAccount> ipasscheckout = paymentAccountService.groupAcerTexiIpass();
        // PaymentAccount paymentAccount =
        // paymentAccountService.getOne(requestHeader.getMerchantId(), 21);

        ArrayList<Map<String, String>> checkoutdata = new ArrayList<>();
        ArrayList<Map<String, String>> uploadData = new ArrayList<>();

        if (ipasscheckout != null) {

            for (PaymentAccount pACheckout : ipasscheckout) {
                Map<String, String> checkout = new HashMap<>();
                Map<String, String> upload = new HashMap<>();
                checkout.put("SystemID", pACheckout.getSystemID());
                checkout.put("SPID", pACheckout.getSpID());

                upload.put("SystemID", pACheckout.getSystemID());
                upload.put("SPID", pACheckout.getSpID());
                upload.put("User", pACheckout.getSftpUser());
                upload.put("Password", pACheckout.getSftpPasswd());

                checkoutdata.add(checkout);
                uploadData.add(upload);
            }

        } else {
            LOGGER.info("[REQUEST][IPASS] Checkout ERROR! This Merchant DON'T HAVE THIS PAYMENT: 21 IPASS)");
        }

        IPassCheckOutReq iPassCheckOutReq = new IPassCheckOutReq();

        iPassCheckOutReq.setDate(getDate());
        iPassCheckOutReq.setTime(getTime());
        // iPassCheckOutReq.setCount(requestData.get("Count").getAsString());
        iPassCheckOutReq.setCount(String.valueOf(checkoutdata.size()));
        iPassCheckOutReq.setData(checkoutdata);
        iPassCheckOutReq.setFileSN(filesn);

        IPassNonTransactionReq iPassNonTransactionReq = new IPassNonTransactionReq();

        iPassNonTransactionReq.setServiceType("Upload");
        iPassNonTransactionReq.setDate(getDate());
        iPassNonTransactionReq.setTime(getTime());
        iPassNonTransactionReq.setCount(String.valueOf(uploadData.size()));
        iPassNonTransactionReq.setData(uploadData);

        System.out.println(new Gson().toJson(iPassCheckOutReq));
        System.out.println(new Gson().toJson(iPassNonTransactionReq));

        try {

            // String response = HttpRequestUtil.post(IPASS_CHECKOUT_URL, new
            // Gson().toJson(iPassCheckOutReq));
            IPassGeneralResponse generalResponse = new Gson().fromJson(NonTransactionRequest(iPassCheckOutReq),
                    IPassGeneralResponse.class);

            if (generalResponse.getTxnResult().equals("Success")) {

                IPassGeneralResponse uploadFinalResponse = new Gson()
                        .fromJson(NonTransactionRequest(iPassNonTransactionReq), IPassGeneralResponse.class);
                return new Gson().toJson(uploadFinalResponse);
            } else {
                return new Gson().toJson(generalResponse);
            }

        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
            return new Gson().toJson(e.getMessage());

        }
    }

    private String blacklist() {

        LOGGER.info("[REQUEST][IPASS] Download Blacklist");

        List<PaymentAccount> ipassblackList = paymentAccountService.groupAcerTexiIpass();
        // PaymentAccount paymentAccount =
        // paymentAccountService.getOne(requestHeader.getMerchantId(), 21);
        ArrayList<Map<String, String>> blacklistdata = new ArrayList<>();

        if (ipassblackList != null) {

            Map<String, String> downloadBlacklist = new HashMap<>();
            downloadBlacklist.put("SystemID", ipassblackList.get(1).getSystemID());
            downloadBlacklist.put("SPID", ipassblackList.get(1).getSpID());
            downloadBlacklist.put("User", ipassblackList.get(1).getSftpUser());
            downloadBlacklist.put("Password", ipassblackList.get(1).getSftpPasswd());

            blacklistdata.add(downloadBlacklist);
        } else {
            LOGGER.info("[REQUEST][IPASS] Blacklist Fetch FAIL");
        }

        IPassNonTransactionReq iPassNonTransactionReq = new IPassNonTransactionReq();

        iPassNonTransactionReq.setServiceType("Blacklist");
        iPassNonTransactionReq.setDate(getDate());
        iPassNonTransactionReq.setTime(getTime());
        iPassNonTransactionReq.setCount(String.valueOf(blacklistdata.size()));
        iPassNonTransactionReq.setData(blacklistdata);

        System.out.println(new Gson().toJson(iPassNonTransactionReq));
        try {

            // String response = HttpRequestUtil.post(IPASS_BLACKLIST_URL, new
            // Gson().toJson(iPassNonTransactionReq));
            // LOGGER.info("[IPASS][RESPONSE] doRequest(). " +
            // IPASS_BLACKLIST_URL);

            IPassGeneralResponse generalResponse = new Gson().fromJson(NonTransactionRequest(iPassNonTransactionReq),
                    IPassGeneralResponse.class);

            return new Gson().toJson(generalResponse);

        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
            return new Gson().toJson(e.getMessage());

        }
    }

    /**
     * stream 去重複用
     *
     * @param keyExtractor
     * @param <T>
     * @return
     */
    private <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return object -> seen.putIfAbsent(keyExtractor.apply(object), Boolean.TRUE) == null;
    }

    private String uploadFile() {

        LOGGER.info("[REQUEST][IPASS] Upload File");

        List<PaymentAccount> ipassUpload = paymentAccountService.groupAcerTexiIpass();    //spID
        // PaymentAccount paymentAccount =
        // paymentAccountService.getOne(requestHeader.getMerchantId(), 21);
        ArrayList<Map<String, String>> uploadData = new ArrayList<>();

        if (ipassUpload != null) {
            for (PaymentAccount pAupload : ipassUpload) {

                Map<String, String> uploadList = new HashMap<>();
                uploadList.put("SystemID", pAupload.getSystemID());
                uploadList.put("SPID", pAupload.getSpID());
                uploadList.put("User", pAupload.getSftpUser());
                uploadList.put("Password", pAupload.getSftpPasswd());

                uploadData.add(uploadList);
            }
        } else {
            LOGGER.info("[REQUEST][IPASS] No Blacklist");
        }

        IPassNonTransactionReq iPassNonTransactionReq = new IPassNonTransactionReq();

        iPassNonTransactionReq.setServiceType("Upload");
        iPassNonTransactionReq.setDate(getDate());
        iPassNonTransactionReq.setTime(getTime());
        iPassNonTransactionReq.setCount(String.valueOf(uploadData.size()));
        iPassNonTransactionReq.setData(uploadData);

        System.out.println(new Gson().toJson(iPassNonTransactionReq));

        // String sendRequest = null;
        // String response = null;
        // try{
        // sendRequest = new Gson().toJson(iPassNonTransactionReq);
        // System.out.println("send!!!!" + sendRequest);
        // } catch (Exception e) {
        // e.printStackTrace();
        // }

        try {

            // String response = HttpRequestUtil.post(IPASS_CHECKOUT_URL, new
            // Gson().toJson(iPassNonTransactionReq));
            // LOGGER.info("[IPASS][RESPONSE] doRequest(). " +
            // IPASS_CHECKOUT_URL);

            IPassGeneralResponse generalResponse = new Gson().fromJson(NonTransactionRequest(iPassNonTransactionReq),
                    IPassGeneralResponse.class);

            return new Gson().toJson(generalResponse);

        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
            return new Gson().toJson(e.getMessage());

        }
    }

    private void saveReserveDetail(String batchNo, JsonObject requestData, RequestHeader requestHeader, String orderId,
                                   IPassTradeResponse response, Device device, String userId, String date, long retry, int isAuto,
                                   String time) {
        LOGGER.info("[EZC][RESERVE][RETRY] saveReserveDetail > " + response.getTxnResult());
        ReserveDetail reserveDetail = this.getReserveDetail(batchNo, requestData, requestHeader, orderId, device, userId, date, 0, 1, time);
        if (retry > 0) {
            reserveDetail.setStatus(response.getTxnResult().equals("Success") ? "0" : "1");
        } else {
            reserveDetail.setAmount(isAuto == 1 ? Long.valueOf(response.getAutoTopUpAmount())
                    : requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsLong());
            reserveDetail.setStatus(response.getTxnResult().equals("Success") ? "0" : "1");
//            reserveDetail.setRrn(response.getRrn());
        }

        ReserveDetail entity = reserveDetailService.save(reserveDetail);
        if (entity == null) {
            LOGGER.error("[REQUEST][IPASS] write reserve detail failed. " + new Gson().toJson(response));
        }
    }

    private ReserveDetail getReserveDetail(String batchNo, JsonObject requestData, RequestHeader requestHeader, String orderId, Device device, String userId, String date, long retry, int isAuto,
                                           String time) {
        ReserveDetail reserveDetail;
        if (retry > 0) {
            reserveDetail = reserveDetailService.getOne(requestHeader.getMerchantId(), orderId);
            reserveDetail.setRrn(date + time);
            reserveDetail.setEzcTxnDate(date + time);
        } else {
            reserveDetail = new ReserveDetail();
            reserveDetail.setAccountId(requestHeader.getMerchantId());
            reserveDetail.setBatchNumber(batchNo);
            reserveDetail.setCashier(requestData.has("Cashier") ? requestData.get("Cashier").getAsString()
                    : SystemInstance.EMPTY_STRING);
            reserveDetail.setDeviceId(device.getOwnerDeviceId());
            reserveDetail.setReserveOrderId(orderId);

            reserveDetail.setMethod("32100");
            reserveDetail.setUserId(userId);
            reserveDetail.setRrn(date + time);
            reserveDetail.setEzcTxnDate(date + time);
            reserveDetail.setIsAuto(isAuto);
            reserveDetail.setDeviceRandomId(device);

        }

        return reserveDetail;
    }

    @Override
    public String getGeneralResponse(RequestHeader requestHeader, ResponseGeneralData response, String errorCode) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setStatusCode(errorCode); // 0001
        responseGeneralHeader.setStatusDesc("STATUS_SUCCESS");
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setMchId(requestHeader.getMerchantId());
        responseGeneralHeader.setMethod(requestHeader.getMethod());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        responseGeneralBody.setData(response);
        responseGeneralBody.setHeader(responseGeneralHeader);

        return new Gson().toJson(responseGeneralBody);
    }

    private String getErrorResponse(RequestHeader requestHeader, IPassGeneralResponse response, String statusDesc) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setStatusCode(SystemInstance.STATUS_FAIL);
        responseGeneralHeader.setStatusDesc(statusDesc);
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setMchId(requestHeader.getMerchantId());
        responseGeneralHeader.setMethod(requestHeader.getMethod());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        responseGeneralBody.setData(response);
        responseGeneralBody.setHeader(responseGeneralHeader);

        return new Gson().toJson(responseGeneralBody);
    }

    private IPassErrorResponse getErrorResponse(RequestHeader requestHeader, Device device, String message,
                                                String errorCode) {
        IPassErrorResponse errorResponse = new IPassErrorResponse();
        errorResponse.setErrorCode(errorCode);
        errorResponse.setTxnResult("Fail");
        errorResponse.setErrorMessage(message);
        errorResponse.setMerchantId(requestHeader.getMerchantId());
        errorResponse.setServiceType(requestHeader.getServiceType());
        errorResponse.setDeviceId(device.getOwnerDeviceId());
        return errorResponse;
    }

    /**
     * 將ＡＰＰ邏輯搬來 各自使用
     *
     * @param requestHeader
     * @param requestData
     * @param device
     * @param paymentAccount
     * @param merchant
     * @return
     */
    private String ticketLogic(RequestHeader requestHeader, JsonObject requestData, Device device,
                               PaymentAccount paymentAccount, Merchant merchant) throws UnsupportedEncodingException, OtherAPIException {
        long isRetry = requestData.get("Retry").getAsLong();
        String orderId = requestData.has(SystemInstance.FIELD_NAME_STORE_ORDER_NO) ?
                StringUtils.defaultIfEmpty(requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString(), merchant.getRandomId() + new SimpleDateFormat("yyMMddHHmmssSS").format(new Date()))
                : merchant.getRandomId() + new SimpleDateFormat("yyMMddHHmmssSS").format(new Date());
        TradeDetail tradeDetail = tradeDetailService.getOne(orderId);
        JsonElement queryCardInfoSultDataJE = requestData.get("QueryCardInfoSultData");
        String queryCardInfoSult = Objects.isNull(queryCardInfoSultDataJE) || queryCardInfoSultDataJE.isJsonNull() || StringUtils.isBlank(queryCardInfoSultDataJE.getAsString()) ?
                this.queryCard(device, paymentAccount, requestHeader) : queryCardInfoSultDataJE.getAsString();

        if (Objects.nonNull(tradeDetail) && isRetry == 0) {
            queryCardInfoSult = this.queryCard(device, paymentAccount, requestHeader);
            orderId = merchant.getRandomId() + new SimpleDateFormat("yyMMddHHmmssSS").format(new Date());
        }
        JsonObject queryCardInfoSultData = new Gson().fromJson(URLDecoder.decode(queryCardInfoSult, "UTF-8"), JsonObject.class).get("Data")
                .getAsJsonObject();

        if ("000000".equals(queryCardInfoSultData.get("ErrorCode").getAsString())
                && "Success".equals(queryCardInfoSultData.get("TXNResult").getAsString())) {
            Map<String, String> resultMap = taxiPoint.mainLogic(device, queryCardInfoSultData, requestData);

            resultMap.put(SystemInstance.FIELD_NAME_STORE_ORDER_NO, orderId);
            resultMap.put("QueryCardInfoSultData", queryCardInfoSult);
            LOGGER.info("[IPASS][ticketLogic] resultMap =>" + resultMap);
            LOGGER.info("[IPASS][ticketLogic] requestData =>" + requestData);
            if (resultMap.get("NeedBalance").equals(requestData.get("NeedBalance").getAsString())
                    && resultMap.get("Cash").equals(requestData.get("Cash").getAsString())
                    && resultMap.get("Point").equals(requestData.get("Point").getAsString())) {

                this.deductionForTaxi(requestData, queryCardInfoSultData, merchant, orderId);
                requestData.addProperty("arcSeq", resultMap.get("arcSeq"));
                requestData.addProperty("transMsg", resultMap.get("Msg"));
                requestHeader.setServiceType("TradeSale");

                // 分配與傳入的相同,直接送單
                return tradeSale(requestHeader, requestData, device, paymentAccount, merchant);
            } else {
                return getGeneralResponse(requestHeader,
                        new Gson().fromJson(new Gson().toJson(resultMap), IPassSicketLogicResponse.class), SystemInstance.STATUS_SUCCESS);
            }
        } else {
            IPassErrorResponse errorResponse = getErrorResponse(requestHeader, device, IPassUtil.getErrorMsg(queryCardInfoSultData.get("ErrorCode").getAsString()),
                    queryCardInfoSultData.get("ErrorCode").getAsString());
            errorResponse.setTotal(requestData.get("Total").getAsString());
            return getErrorResponse(requestHeader, errorResponse, IPassUtil.getErrorMsg(queryCardInfoSultData.get("ErrorCode").getAsString()));
        }


    }

    private void deductionForTaxi(JsonObject requestData, JsonObject queryCardInfoSultData, Merchant merchant, String orderId) {

        String transactionType = Integer.parseInt(requestData.get("Point").getAsString()) > 0 ? "12" : "11";
        String usedate = queryCardInfoSultData.get("SpecialIdentityResetDate").getAsString();
        String now = new SimpleDateFormat("yyyyMM").format(new Date());

        requestData.addProperty("StoreOrderNo", orderId);
        requestData.addProperty("SpecialIdentityDiscountAmount", String.valueOf(Integer.valueOf(queryCardInfoSultData.get("SpecialIdentityPointRatio").getAsString()) *
                Integer.valueOf(requestData.get("Point").getAsString())));  //交易點數折抵金額( 優惠金額 )
        requestData.addProperty("Amount", requestData.get("Total").getAsString());     //總車資
        requestData.addProperty("PaidAmount", requestData.get("Cash").getAsString()); // 實際扣款金額

        requestData.addProperty("CardSNFull", queryCardInfoSultData.get("CardSN").getAsString());
        requestData.addProperty("NeedCash", requestData.get("NeedBalance").getAsString());
        requestData.addProperty("TransactionType", transactionType);   //  交易類型
        requestData.addProperty("SpecialIdentityTxnPoint", requestData.get("Point").getAsString());     // 點數扣點數量
        requestData.addProperty("PointBalance", String.valueOf(Integer.valueOf(queryCardInfoSultData.get("SpecialIdentityUsagePointLimit").getAsString()) -
                (!now.equals(usedate.substring(0, 6)) || "00000000".equals(usedate) ? 0 : Integer.valueOf(queryCardInfoSultData.get("UsageAccumulatedPointBeforeTxn").getAsString())) -
                Integer.valueOf(requestData.get("Point").getAsString())));   // 剩餘可扣點數

        requestData.addProperty("SpecialIdentityPointRatio", queryCardInfoSultData.get("SpecialIdentityPointRatio").getAsString());    // 社福點數與現金轉換比
        requestData.addProperty("SpecialIdentityDiscountAmount", String.valueOf(Integer.valueOf(queryCardInfoSultData.get("SpecialIdentityPointRatio").getAsString()) *
                Integer.valueOf(requestData.get("Point").getAsString())));  //交易點數折抵金額( 優惠金額 )

        String SpecialIdentityResetFlag = "00000000".equals(usedate) || !now.equals(usedate.substring(0, 6)) ? "01" : "00"; //只要上次更新日期非當月或第一次使用 就更新 特種票旗標
        requestData.addProperty("SpecialIdentityResetFlag", SpecialIdentityResetFlag); // 特種票重置旗標

        requestData.addProperty("SpecialCardType", queryCardInfoSultData.get("SpecialCardType").getAsString());
        requestData.addProperty("SpecialIdentityType", queryCardInfoSultData.get("SpecialIdentityType").getAsString());
        requestData.addProperty("SpecialIdentityProvider", queryCardInfoSultData.get("SpecialIdentityProvider").getAsString());
        requestData.addProperty("IdentityType", queryCardInfoSultData.get("IdentityType").getAsString());

    }

    /**
     * 發送請求
     *
     * @param url
     * @param jsonStr
     * @return
     */
    private String sendPost(String url, String jsonStr) throws InvokeAPIException {
        String result = "";
        try {
            result = HttpUtil.doPostJson(url, jsonStr);
        } catch (Exception e) {
            LOGGER.error("[IPASS][EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            throw new InvokeAPIException(this.getClass().getSimpleName(), e.getMessage());
        }
        if (StringUtils.isEmpty(result)) {
            throw new InvokeAPIException(this.getClass().getSimpleName(), messageSource.getMessage("response.7350", null, locale));
        }
        return result;
    }

    private String doTermAuth(RequestHeader requestHeader, JsonObject requestData, Device device,
                              PaymentAccount paymentAccount, Merchant merchant) throws InvalidDataFormatException, JsonProcessingException, InvokeAPIException {
        if ("taxi".equals(device.getType())) {
            // 舊款 計程車APP登入也是傳這個service type，怕還是有司機未更新，直接擋住不給登入
            throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
        }

        IPassTermAuthRequest iPassTermAuthRequest = new IPassTermAuthRequest(testMode, device, paymentAccount, NON_TAXI_CLIENT_IP, merchant);
        String requestJsonStr = new ObjectMapper().writeValueAsString(iPassTermAuthRequest);
        LOGGER.info("[IPASS][doTermAuth]request => " + requestJsonStr);
        String result = this.sendPost(NON_TAXI_HOST, requestJsonStr);
        LOGGER.info("[ICASH][doTermAuth]response => " + result);
        IPassBaseResponse response = new Gson().fromJson(result, IPassBaseResponse.class);

        if (StringUtils.isNotEmpty(response.getNewAESKey())) {
            device.setAesKey(response.getNewAESKey());
        }

        if ("D00000".equals(response.getErrorCode()) && "90000083C8".equals(response.getMcErrorCode())) {
            //端末開機已完成，不需再次進行端末開機
            response.settXNResult("Success");
            response.setErrorCode("000000");
        }

        deviceService.save(device);

        return this.getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    private String doTradeSale(RequestHeader requestHeader, JsonObject requestData, Device device,
                               PaymentAccount paymentAccount, Merchant merchant) throws Exception {

        LOGGER.info("[IPASS][TradeSale][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");

        String userId = this.verificationForTradeSale(device, requestData, merchant, paymentAccount);
        String batchNo = this.getDate();

        TradeDetail tradeDetail = this.createOrder(device, paymentAccount, requestHeader, requestData, batchNo, userId);
        IPassTradeSaleRequest iPassTradeSaleRequest = new IPassTradeSaleRequest(testMode, device, paymentAccount);
        iPassTradeSaleRequest.setAmount(requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString());
        if (requestData.has("Cashier") && StringUtils.isNotEmpty(requestData.get("Cashier").getAsString())) {
            iPassTradeSaleRequest.setPosOperatorID(requestData.get("Cashier").getAsString());
        }
        String requestJsonStr = new ObjectMapper().writeValueAsString(iPassTradeSaleRequest);
        LOGGER.info("[IPASS][TradeSale]request => " + requestJsonStr);
        String result = this.sendPost(NON_TAXI_HOST, requestJsonStr);
        LOGGER.info("[IPASS][TradeSale]response => " + result);

        IPassBaseResponse response = new Gson().fromJson(result, IPassBaseResponse.class);

        return this.updateDoTradeSale(requestHeader, requestData, device, merchant, iPassTradeSaleRequest, response, tradeDetail, paymentAccount);
    }

    /**
     * 驗證 交易 資料，並回傳 userId
     *
     * @param device
     * @param requestData
     * @param merchant
     * @throws AlreadyRetryException
     * @throws OrderAlreadyExistedException
     * @throws TicketReadCardException
     */
    private String verificationForTradeSale(Device device, JsonObject requestData, Merchant merchant, PaymentAccount paymentAccount) throws Exception {

        String orderId = requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString();
        long isRetry = requestData.get("Retry").getAsLong();
        TradeDetail tradeDetail;
        String userId;
        if (isRetry > 0 && isRetry < 4) {
            tradeDetail = tradeDetailService.getOne(orderId);
            userId = tradeDetail.getUserId();
        } else if (isRetry >= 4) {
            LOGGER.error("[REQUEST][IPASS_NON_TAXI] STOP RETRY. " + device.getOwnerDeviceId() + ", " + merchant.getAccountId());
            throw new AlreadyRetryException("Already retry 3 times.");
        } else {
            tradeDetail = tradeDetailService.getOne(orderId);
            if (Objects.nonNull(tradeDetail)) {
                throw new OrderAlreadyExistedException("Trade exist");
            }
            userId = this.getUserId(merchant, requestData, device, paymentAccount);
        }
        return userId;
    }

    /**
     * @param merchant
     * @param requestData
     * @return
     * @throws Exception
     */
    private String getUserId(Merchant merchant, JsonObject requestData, Device device, PaymentAccount paymentAccount) throws Exception {
        LOGGER.info("[IPASS_NON_TAXI] getUserId : " + requestData.get("DeviceId").getAsString() + ", " + merchant.getAccountId());
        String userId = "-1", errMsg = "Can not read card info";

        int tryCount = 0;
        do {
            IPassOfflineInquiryCardRequest iPassOfflineInquiryCardRequest = new IPassOfflineInquiryCardRequest(testMode, device, paymentAccount);
            String requestJsonStr = new ObjectMapper().writeValueAsString(iPassOfflineInquiryCardRequest);

            LOGGER.info("[IPASS_NON_TAXI][getUserId][" + (++tryCount) + "]request => " + requestJsonStr);
            String result = this.sendPost(NON_TAXI_HOST, requestJsonStr);
            LOGGER.info("[IPASS_NON_TAXI][getUserId][" + tryCount + "]response => " + result);
            IPassBaseResponse response = new Gson().fromJson(result, IPassBaseResponse.class);
            if (StringUtils.isNotEmpty(response.getNewAESKey())) {
                device.setAesKey(response.getNewAESKey());
            }
            if ("Success".equals(response.gettXNResult())) {
                userId = StringUtils.defaultIfBlank(response.getCardNumberForPrint(), "");
                userId = userId.substring(0, Math.min(8, userId.length()));
            }
            deviceService.save(device);
        } while (("-1".equals(userId) || StringUtils.isEmpty(userId)) && tryCount < 10);

        if (StringUtils.isEmpty(userId) || userId.equals("-1")) {
            throw new TicketReadCardException(errMsg);
        }

        return userId;
    }

    private TradeDetail createOrder(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader, JsonObject requestData, String batchNo, String userId) {
        String time = getTime();
        TradeDetail tradeDetail = new TradeDetail();
        tradeDetail.setStatus(SystemInstance.TRADE_CREATING_ORDER);
        tradeDetail.setOrderId(requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString());
        tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        tradeDetail.setAccountId(requestHeader.getMerchantId());
        tradeDetail.setPayment(Long.valueOf(requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString()));
        tradeDetail.setDeviceRandomId(device);
        tradeDetail.setPaymentAccount(paymentAccount);
        tradeDetail.setMethod("32100");
        tradeDetail.setSystemOrderId(time);
        tradeDetail.setBatchNo(batchNo);
        tradeDetail.setUserId(userId);
        tradeDetail.setEzcTxnDate(getDate() + time);
        tradeDetail.setComment(paymentAccount.getAccount() + device.getOwnerDeviceId());
        if (requestData.has("Body")) {
            tradeDetail.setDescription(requestData.get("Body").getAsString());
        }
        tradeDetail = tradeDetailService.save(tradeDetail);
        return tradeDetail;
    }

    @Override
    public String updateDoTradeSale(RequestHeader requestHeader, JsonObject requestData, Device device, Merchant merchant, IPassTradeSaleRequest iPassTradeSaleRequest, IPassBaseResponse response, TradeDetail tradeDetail, PaymentAccount paymentAccount) {
        response.setOrderId(tradeDetail.getOrderId());
        response.setCardDesc("iPass");
        response.setEzCardId(tradeDetail.getUserId());

        if (StringUtils.isNotEmpty(response.getNewAESKey())) {
            device.setAesKey(response.getNewAESKey());
        }
        deviceService.save(device);

        this.writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(),
                requestHeader.getMethod(), "Payment | " + merchant.getMerchantSeqId() + " | "
                        + device.getOwnerDeviceId() + " | " + tradeDetail.getPayment() + " | " + tradeDetail.getUserId(),
                response.getOrderId() + "|" + response.gettXNResult());

//        tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());
        tradeDetail.setBatchNo(iPassTradeSaleRequest.getPosInvoiceNumber());

        if (StringUtils.isNotBlank(response.getAutoTopUpAmount()) && !"0".equals(response.getAutoTopUpAmount())) {
            this.saveReserveDetail(tradeDetail.getBatchNo(), requestData, requestHeader, tradeDetail.getOrderId(), response, device, tradeDetail.getUserId(), iPassTradeSaleRequest.getDate(), 0, 1, iPassTradeSaleRequest.getTime());
        }
        if ("Success".equals(response.gettXNResult())) {
            LOGGER.info("[RESPONSE][IPASS][TradeSale][Success] " + new Gson().toJson(response));
            tradeDetail.setMethod("32100");
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetail.setEzcTxnDate(iPassTradeSaleRequest.getDate() + iPassTradeSaleRequest.getTime());
            tradeDetail.setRrn(response.getRrn());
            appNotificationService.notifyApp(tradeDetail, merchant);
            //列印收據
            this.printReceipt(response, device, paymentAccount, response.getAmount(), response.getCardNumberForPrint());
            this.saveTradeDetailOtherInfo(tradeDetail, response);
        } else {
            LOGGER.error("[RESPONSE][IPASS][TradeSale][Fail] " + new Gson().toJson(response));
            response.settXNResult(StringUtils.defaultIfEmpty(response.gettXNResult(), SystemInstance.STATUS_DESCRIPTION_FAIL2));
            String errCode = StringUtils.defaultIfEmpty(response.getMcErrorCode(), "900000");
            errCode = errCode.length() > 6 ? errCode.substring(errCode.length() - 4) : errCode;
            response.setErrorCode(errCode + " - " + response.getChineseErrorMessage());
            if (!lookupCodeService.isTsmcAccount(requestHeader.getMerchantId()) && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            } else if ("14900".equals(tradeDetail.getMethod())) {
                if (SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    requestHeader.setMethod("14900");
                    response.settXNResult(SystemInstance.STATUS_DESCRIPTION_SUCCESS);
                    response.setErrorCode("000000");
                } else {
                    tradeDetail.setMethod("32100");
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }
        }
        tradeDetailService.save(tradeDetail);
        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    @Async
    private void writeDBLog(String merchantId, String serviceType, String method, String instruction, String response) {

        OperationHistory operationHistory = new OperationHistory();
        operationHistory.setServiceType(serviceType);

        operationHistory.setMethod(method);
        operationHistory.setMerchantId(merchantId);
        operationHistory.setInstruction(instruction);
        operationHistory.setResponse(response);

        operationHistoryService.save(operationHistory);

    }

    /**
     * 列印發票，因不改POS機，直接抄EZC來用
     *
     * @param response
     * @param device
     * @param paymentAccount
     * @param amount
     * @param cardId
     * @throws Exception
     */
    private void printReceipt(IPassBaseResponse response, Device device, PaymentAccount paymentAccount, String amount, String cardId) {
        LOGGER.info("[IPASS][Receipt][Print][input] " + new Gson().toJson(response));
        EzCardReceipt ezCardReceipt = new Gson().fromJson(new Gson().toJson(response), EzCardReceipt.class);
        ezCardReceipt.setDeviceId(device.getOwnerDeviceId());
        ezCardReceipt.setTerminalId(paymentAccount.getAccount());
        ezCardReceipt.setPrintId("01");
        ezCardReceipt.setDate(response.getDate());
        ezCardReceipt.setTime(response.getTime());
        ezCardReceipt.setEzCardId(cardId);
        ezCardReceipt.setCpuPurseVersion("0");
        ezCardReceipt.setAmount(amount);
        ezCardReceipt.setBatchNumber(this.getDate());

        LOGGER.info("[IPASS][Receipt][Print][REQ] " + new Gson().toJson(ezCardReceipt));

        if (StringUtils.isNotBlank(response.getAutoTopUpAmount()) && !"0".equals(response.getAutoTopUpAmount())) {
            ezCardReceipt.setAutoTopUpAmount(response.getAutoTopUpAmount());
        }

        String printResponse = null;
        try {
            String requestUrl = EZC_HOST + URLEncoder.encode(new Gson().toJson(ezCardReceipt), "UTF-8");
            printResponse = HttpRequestUtil.httpsGet(requestUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
        LOGGER.info("[IPASS][Receipt][Print][response] " + printResponse);
    }


    /**
     * 非計程車 交易
     *
     * @param batchNo
     * @param requestData
     * @param requestHeader
     * @param orderId
     * @param response
     * @param device
     * @param userId
     * @param date
     * @param retry
     * @param isAuto
     * @param time
     */
    private ReserveDetail saveReserveDetail(String batchNo, JsonObject requestData, RequestHeader requestHeader, String orderId,
                                            IPassBaseResponse response, Device device, String userId, String date, long retry, int isAuto,
                                            String time) {
        LOGGER.info("[IPASS][RESERVE][RETRY] saveReserveDetail > " + response.gettXNResult());
        ReserveDetail reserveDetail = this.getReserveDetail(batchNo, requestData, requestHeader, orderId, device, userId, date, 0, 0, time);
        if (retry > 0) {
            reserveDetail.setStatus(response.gettXNResult().equals("Success") ? "0" : "1");
        } else {
            reserveDetail.setAmount(isAuto == 1 ? Long.valueOf(response.getAutoTopUpAmount())
                    : requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsLong());
            reserveDetail.setStatus(response.gettXNResult().equals("Success") ? "0" : "1");
        }

        ReserveDetail entity = reserveDetailService.save(reserveDetail);
        if (entity == null) {
            LOGGER.error("[REQUEST][IPASS] write reserve detail failed. " + new Gson().toJson(response));
        }
        return entity;
    }

    /**
     * 讀卡  (線上)
     *
     * @param device
     * @param paymentAccount
     * @param requestHeader
     * @return
     */
    private String doQueryCardInfo(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader, JsonObject requestData) throws JsonProcessingException, InvokeAPIException {

        LOGGER.info("[IPASS_NON_TAXI][QueryCardInfo][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");
        IPassOfflineInquiryCardRequest iPassOfflineInquiryCardRequest = new IPassOfflineInquiryCardRequest(testMode, device, paymentAccount);
        String requestJsonStr = new ObjectMapper().writeValueAsString(iPassOfflineInquiryCardRequest);

        LOGGER.info("[IPASS_NON_TAXI][doQueryCardInfo]  request => " + requestJsonStr);
        String result = this.sendPost(NON_TAXI_HOST, requestJsonStr);
        LOGGER.info("[IPASS_NON_TAXI][doQueryCardInfo]  response => " + result);
        IPassBaseResponse response = new Gson().fromJson(result, IPassBaseResponse.class);

        if (StringUtils.isNotEmpty(response.getNewAESKey())) {
            device.setAesKey(response.getNewAESKey());
        }

        deviceService.save(device);

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    /**
     * 查卡號
     * 查卡號，因悠遊卡都可以查所以直接走悠遊卡查卡ID
     *
     * @param device
     * @param paymentAccount
     * @param requestHeader
     * @param requestData
     * @return
     * @throws Exception
     */
    private String doIdQuery(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader, JsonObject requestData) throws JsonProcessingException, InvokeAPIException {

        String result = this.doQueryCardInfo(device, paymentAccount, requestHeader, requestData);
        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);
        IPassOfflineInquiryCardResponse iPassBaseResponse = JsonUtil.parseJson(jsonObject.get("Data").toString(), IPassOfflineInquiryCardResponse.class);

        String userId = StringUtils.defaultIfBlank(iPassBaseResponse.getCardNumberForPrint(), "");
        userId = userId.substring(0, Math.min(8, userId.length()));
        iPassBaseResponse.setEzCardId(userId);
        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                new Gson().toJson(iPassBaseResponse), userId + "|" + iPassBaseResponse.gettXNResult());

        return getGeneralResponse(requestHeader, iPassBaseResponse, SystemInstance.STATUS_SUCCESS);

    }

    private String doDeviceQuery(Merchant merchant, RequestHeader requestHeader) {
        LOGGER.info("[REQUEST][IPASS_NON_TAXI] list device by merchantId : " + merchant.getAccountId());
        List<DeviceVo> deviceList = deviceService.listAllByMerchantAndType(merchant.getAccountId(), "ezc");
        List<DeviceVo> trafficDeviceList = deviceService.listAllByMerchantAndType(merchant.getAccountId(), "traffic");
        deviceList = CollectionUtils.isEmpty(deviceList) ? new ArrayList<>() : deviceList;
        if (CollectionUtils.isNotEmpty(trafficDeviceList)) {
            deviceList.addAll(trafficDeviceList);
        }
        IcashDeviceQueryResponse response = new IcashDeviceQueryResponse();
        response.setDeviceList(deviceList);

        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                "DeviceQuery:" + merchant.getAccountId(), response.getOrderId() + "|" + response.getTxnResult());

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    /**
     * 取得交易紀錄(退款看到的清單)
     *
     * @param requestHeader
     * @param requestData
     * @return
     * @throws JsonProcessingException
     * @throws InvokeAPIException
     */
    private String doOrderQuery(RequestHeader requestHeader, JsonObject requestData, Merchant merchant, Device device, PaymentAccount paymentAccount) throws Exception {

        String userId = "-1";
        if ("1.0".equals(device.getSwVersion()) || Objects.isNull(device.getSwVersion())) {
            userId = this.getUserId(merchant, requestData, device, paymentAccount);
        } else {
            userId = requestData.has("UserId") ? requestData.get("UserId").getAsString() : this.getUserId(merchant, requestData, device, paymentAccount);
        }

        LOGGER.info("[doOrderQuery][ipass] list order by user cardId : " + userId);
        List<TradeDetailVo> tradeDetailVos = tradeDetailService.listByUserId(merchant.getAccountId(), userId);

        IcashOrderQueryResponse response = new IcashOrderQueryResponse();
        List<EzcTradeDetailVo> ezcTradeDetailVos = new ArrayList<EzcTradeDetailVo>();
        for (TradeDetailVo tx : tradeDetailVos) {

            boolean isRefund = !Strings.isNullOrEmpty(tx.getRefundStatus())
                    && tx.getRefundStatus().equals("Refund success");
            if (tx.getPayment() > 0 && !isRefund) {
                EzcTradeDetailVo ezcTx = new EzcTradeDetailVo();
                ezcTx.setCreateDate(tx.getCreateDate());
                ezcTx.setOrderId(tx.getOrderId());
                ezcTx.setPayment(tx.getPayment());

                ezcTradeDetailVos.add(ezcTx);
            }
        }

        response.setList(ezcTradeDetailVos);
        response.setErrorCode("00000");

        this.writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                "OrderQuery : " + userId + "|" + merchant.getAccountId(), "N/A");
        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    /**
     * 退款退貨
     *
     * @param device
     * @param paymentAccount
     * @param requestHeader
     * @return
     */
    private String doTradeRefund(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader, JsonObject requestData, Merchant merchant) throws Exception {

        LOGGER.info("[IPASS_NON_TAXI][TradeRefund][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");
        long isRetry = requestData.get("Retry").getAsLong();
        if (isRetry >= 4) {
            LOGGER.error("[IPASS_NON_TAXI][TradeRefund]. " + device.getOwnerDeviceId() + ", " + requestHeader.getMerchantId());
            throw new RuntimeException("Already retry 3 times.");
        }

        String orderId = requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString();
        TradeDetail tradeDetail = tradeDetailService.getOne(orderId);
        if (Objects.isNull(tradeDetail)) {
            throw new OrderNotFoundException(SystemInstance.ORDER_NOT_FOUND);
        } else if (tradeDetail.getRefundStatus() != null
                && tradeDetail.getRefundStatus().equals(SystemInstance.TRADE_REFUND_SUCCESS)) {
            LOGGER.error("[IPASS_NON_TAXI][TradeRefund] tradeDetail already refund.");
            throw new OrderAlreadyRefundedException("tradeDetail already refund.");
        } else if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
            LOGGER.info("tradeDetail not TRADE_SUCCESS \t" + tradeDetail.getOrderId() + "\t" + tradeDetail.getStatus());
            throw new OtherAPIException(messageSource.getMessage("response.7351", null, locale));
        } else {
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_INIT);
            tradeDetailService.save(tradeDetail);
            LOGGER.info("[IPASS_NON_TAXI][TradeRefund] update tradeDetail and create refund detail.");
        }

        IPassTradeRefundRequest request = new IPassTradeRefundRequest(testMode, device, paymentAccount);
        request.setAmount(String.valueOf(tradeDetail.getPayment()));
        request.setCancelAddRrn(tradeDetail.getRrn());
        String requestJsonStr = new ObjectMapper().writeValueAsString(request);

        LOGGER.info("[IPASS_NON_TAXI][TradeRefund]request => " + requestJsonStr);
        String result = this.sendPost(NON_TAXI_HOST, requestJsonStr);
        LOGGER.info("[IPASS_NON_TAXI][TradeRefund]response => " + result);

        IPassBaseResponse response = new Gson().fromJson(result, IPassBaseResponse.class);
        if (!"Success".equals(response.gettXNResult())) {
            String errCode = StringUtils.defaultIfEmpty(response.getMcErrorCode(), "900000");
            errCode = errCode.length() > 6 ? errCode.substring(errCode.length() - 4) : errCode;
            response.setErrorCode(errCode + " - " + response.getChineseErrorMessage());
        }
        return this.updateDoRefund(requestHeader, requestData, device, merchant, response, tradeDetail, paymentAccount);
    }

    @Override
    public String updateDoRefund(RequestHeader requestHeader, JsonObject requestData, Device device, Merchant merchant, IPassBaseResponse response, TradeDetail tradeDetail, PaymentAccount paymentAccount) {

        String userId = StringUtils.defaultIfBlank(response.getCardNumberForPrint(), "");
        userId = userId.substring(0, Math.min(8, userId.length()));
        response.setEzCardId(userId);

        this.writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(),
                requestHeader.getMethod(), "REFUND(LIMIT) | " + paymentAccount.getAccount() + "|"
                        + device.getOwnerDeviceId() + "|" + response.getAmount(),
                response.getOrderId() + "|" + response.gettXNResult());
        this.saveOrUpdateTransactionDetails(tradeDetail, merchant, response, device, this.getDate(),
                tradeDetail.getPayment(), requestData.get("Retry").getAsLong(), this.getTime(), false);

        if (StringUtils.isNotEmpty(response.getNewAESKey())) {
            device.setAesKey(response.getNewAESKey());
        }
        deviceService.save(device);
        if (SystemInstance.STATUS_DESCRIPTION_SUCCESS.equals(response.gettXNResult())) {
            //列印收據
            this.printReceipt(response, device, paymentAccount, response.getAmount(), response.getCardNumberForPrint());
        }
        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    private void saveOrUpdateTransactionDetails(TradeDetail tradeDetail, Merchant merchant,
                                                IPassBaseResponse response, Device device, String batchNo, long amount, long isRetry, String txnTime,
                                                boolean isCancel) {

        RefundDetail refundDetail = new RefundDetail();
        refundDetail.setMethod(tradeDetail.getMethod());
        refundDetail.setOrderId(tradeDetail.getOrderId());
        refundDetail.setAmount(amount);
        refundDetail.setTradeDetailRandomId(tradeDetailService.getOne(tradeDetail.getOrderId()));
        refundDetail.setCreateTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        refundDetail.setStoreRefundId(tradeDetail.getOrderId());
        refundDetail.setSystemRefundId(txnTime);
        LOGGER.info(
                "[IPASS_NON_TAXI] REFUND success and update refund information." + new Gson().toJson(response));
        tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());
        if (response.gettXNResult().equals("Success")) {
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
            refundDetail.setStatus(SystemInstance.TRADE_REFUND_SUCCESS);
            refundDetail.setPartialRefund(false);
            refundDetail.setBatchNo(batchNo);
            refundDetail.setDeviceRandomId(device);
            refundDetail.setAccountId(merchant.getAccountId());
            refundDetail.setReason(isRetry > 0 ? "Retry" : "");
            refundDetail.setEzcTxnDate(response.getDate() + response.getTime());
            refundDetail.setEzcCancel(isCancel);
            refundDetail.setRrn(response.getRrn());
        } else {
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
            refundDetail.setStatus(SystemInstance.TRADE_REFUND_FAIL);
            refundDetail.setPartialRefund(false);
            refundDetail.setDeviceRandomId(device);
            refundDetail.setAccountId(merchant.getAccountId());
            refundDetail.setReason(isRetry > 0 ? "Retry" : response.getErrorCode());
            refundDetail.setEzcTxnDate(response.getDate() + response.getTime());
        }
        tradeDetailService.save(tradeDetail);
        refundDetailService.save(refundDetail);
    }

    @Override
    public void settlementMain(String createDate, String deviceId, String mchAccount) {
        this.settlementMain(createDate, "", deviceId, mchAccount);
    }

    @Override
    public void settlementMain(String createDate, String endDate, String deviceId, String mchAccount) {
        /**
         *
         *  節班 只需DeviceID、TerminalID
         *   所以每次結帳都是全部結帳 (線上)
         */
        String createDateStart = "", createDateEnd = "";
        try {
            createDateStart = LocalDate.parse(createDate, DateTimeFormatter.ofPattern("yyyyMMdd")).format(DateTimeFormatter.ofPattern("yyyyMMdd000000"));
            createDateEnd = LocalDate.parse(StringUtils.defaultIfEmpty(endDate, createDate), DateTimeFormatter.ofPattern("yyyyMMdd")).format(DateTimeFormatter.ofPattern("yyyyMMdd235959"));
        } catch (Exception e) {
            LOGGER.debug("[settlementMain][ERROR] createDate => " + createDate);
        }

        // ipass小額 沒有自動結帳，每次都是POS打API，故明確知道哪台卡機要結帳
        List<CheckoutCtrlVo> checkoutCtrlVos = checkoutRepository.getCheckoutList(mchAccount, deviceId, "", createDateStart, createDateEnd, "21");

        List<Map<String, Object>> resultMapList = this.transformCheckoutCtrlVoList(checkoutCtrlVos);
        try {
            this.doSettlement(resultMapList, deviceId, mchAccount);
        } catch (Exception e) {
            resultMapList.parallelStream().forEach(stringObjectMap -> this.updateCheckoutCtrl("ERR", resultMapList));
            e.printStackTrace();
        }


    }

    /**
     * 轉換資料，並同時建立INT_TB_CheckoutCtrl  、 INT_TB_CheckoutLog
     *
     * @param CheckoutCtrlVoList
     * @return
     */
    private List<Map<String, Object>> transformCheckoutCtrlVoList(List<CheckoutCtrlVo> CheckoutCtrlVoList) {
        return CollectionUtils.isEmpty(CheckoutCtrlVoList) ? new ArrayList<>() : CheckoutCtrlVoList.parallelStream().map(checkoutCtrlVo -> {
            //初始化 INT_TB_CheckoutCtrl 、INT_TB_CheckoutLog
            INT_TB_CheckoutCtrl checkoutCtrl = this.getCheckoutCtrl(checkoutCtrlVo);
            INT_TB_CheckoutCtrlService.save(checkoutCtrl);
            checkoutCtrlVo.setCcSeqId(BigInteger.valueOf(checkoutCtrl.getCcSeqId()));
            INT_TB_CheckoutLog checkoutLog = this.getCheckoutLog(checkoutCtrlVo);
            INT_TB_CheckoutLogService.save(checkoutLog);

            Map<String, Object> orderMap = this.getDetails(checkoutCtrlVo.getOwnerDeviceId(), checkoutCtrlVo.getAccountId(), checkoutCtrlVo.getBatchNo());

            List<TradeDetailVo> tdList = (List<TradeDetailVo>) orderMap.get("tradeDetailList");
            List<ReserveDetail> reDList = (List<ReserveDetail>) orderMap.get("reserveDetailList");
            List<RefundDetailVo> refundDList = (List<RefundDetailVo>) orderMap.get("refundDetailList");
            List<ReserveCancel> reserveCancels = (List<ReserveCancel>) orderMap.get("reserveCancelList");

            Device device = null;
            if (tdList.size() > 0) {
                device = deviceService.getOneByTDOrderId(tdList.get(0).getOrderId());
            } else if (reDList.size() > 0) {
                device = deviceService.getOneByRDOrderId(reDList.get(0).getReserveOrderId());
            } else if (refundDList.size() > 0) {
                device = deviceService.getOneByRefundOrderId(refundDList.get(0).getOrderId());
            }

            checkoutCtrl.setStatus("P");
            checkoutCtrl.setDateStamp(new Date());
            checkoutCtrl.setTradeAmount(tdList.parallelStream().filter(tradeDetailVo -> SystemInstance.TRADE_SUCCESS.equals(tradeDetailVo.getStatus())).mapToLong(TradeDetailVo::getPayment).sum());
            checkoutCtrl.setRefundAmount(refundDList.parallelStream().filter(refundDetailVo -> SystemInstance.TRADE_REFUND_SUCCESS.equals(refundDetailVo.getStatus())).mapToLong(RefundDetailVo::getAmount).sum());
            checkoutCtrl.setReserveAmount(reDList.parallelStream().filter(reserveDetail -> "0".equals(reserveDetail.getStatus())).mapToLong(ReserveDetail::getAmount).sum());
            checkoutCtrl.setReserveCancelAmount(reserveCancels.parallelStream().filter(reserveCancel -> "0".equals(reserveCancel.getStatus())).mapToLong(ReserveCancel::getAmount).sum());
            INT_TB_CheckoutCtrlService.save(checkoutCtrl);
            checkoutLog.setStatus("P");
            checkoutLog.setCrDate(new Date());
            checkoutLog.setDateStamp(new Date());
            INT_TB_CheckoutLogService.save(checkoutLog);

            orderMap.put("checkoutCtrlVo", checkoutCtrlVo);
            orderMap.put("device", device);
            orderMap.put("checkoutCtrl", checkoutCtrl);
            orderMap.put("checkoutLog", checkoutLog);

            return orderMap;
        }).collect(Collectors.toList());
    }

    private INT_TB_CheckoutCtrl getCheckoutCtrl(CheckoutCtrlVo checkoutCtrlVo) {
        INT_TB_CheckoutCtrl checkoutCtrl = new INT_TB_CheckoutCtrl();
        checkoutCtrl.setCcSeqId(checkoutCtrlVo.getCcSeqId().intValue());
        checkoutCtrl.setAccountId(checkoutCtrlVo.getAccountId());
        checkoutCtrl.setMethod("32100");
        checkoutCtrl.setBatchNo(checkoutCtrlVo.getBatchNo());
        checkoutCtrl.setType(checkoutCtrlVo.getType());
        checkoutCtrl.setComment(checkoutCtrlVo.getComment());
        checkoutCtrl.setOwnerDeviceId(checkoutCtrlVo.getOwnerDeviceId());
        checkoutCtrl.setStatus(checkoutCtrlVo.getStatus());
        checkoutCtrl.setCrDate(checkoutCtrlVo.getCrDate());
        checkoutCtrl.setDateStamp(new Date());

        return checkoutCtrl;
    }

    /**
     * 新增ctrl log 檔 (status 為 I or R)
     *
     * @param checkoutCtrlVo
     */
    private INT_TB_CheckoutLog getCheckoutLog(CheckoutCtrlVo checkoutCtrlVo) {
        INT_TB_CheckoutLog checkoutLog = new INT_TB_CheckoutLog();
        checkoutLog.setCcSeqId(checkoutCtrlVo.getCcSeqId().intValue());
        checkoutLog.setAccountId(checkoutCtrlVo.getAccountId());
        checkoutLog.setBatchNo(checkoutCtrlVo.getBatchNo());
        checkoutLog.setComment(checkoutCtrlVo.getComment());
        checkoutLog.setOwnerDeviceId(checkoutCtrlVo.getOwnerDeviceId());
        checkoutLog.setStatus(checkoutCtrlVo.getStatus());
        checkoutLog.setCrDate(new Date());
        checkoutLog.setDateStamp(new Date());
        return checkoutLog;
    }

    private Map<String, Object> getDetails(String deviceId, String account, String batchNo) {

        List<TradeDetailVo> tradeDetailList = tradeDetailService.listByBatchNo(account,
                deviceId, batchNo);
        List<ReserveDetail> reserveDetailList = reserveDetailService.listByBatchNo(account,
                deviceId, batchNo);
        List<RefundDetailVo> refundDetailList = refundDetailService.listByBatchNo(account,
                deviceId, batchNo);
        List<ReserveCancel> reserveCancelList = reserveCancelService.listByMerchantIdAndDeviceIdAndBatchNo(
                account, deviceId, batchNo);

        Map<String, Object> map = new HashMap<>();
        map.put("tradeDetailList", tradeDetailList);
        map.put("reserveDetailList", reserveDetailList);
        map.put("refundDetailList", refundDetailList);
        map.put("reserveCancelList", reserveCancelList);

        return map;
    }

    private void updateCheckoutCtrl(final String statusStr, List<Map<String, Object>> resultMapList) {

        resultMapList.parallelStream().forEach(map -> {
            INT_TB_CheckoutCtrl checkoutCtrl = (INT_TB_CheckoutCtrl) map.get("checkoutCtrl");
            INT_TB_CheckoutLog checkoutLog = (INT_TB_CheckoutLog) map.get("checkoutLog");
            checkoutCtrl.setStatus(statusStr);
            checkoutCtrl.setDateStamp(new Date());
            checkoutLog.setStatus(statusStr);
            checkoutLog.setCrDate(new Date());
            checkoutLog.setDateStamp(new Date());
            INT_TB_CheckoutCtrlService.save(checkoutCtrl);
            INT_TB_CheckoutLogService.save(checkoutLog);
        });
    }

    /**
     * 結帳
     *
     * @param resultMapList
     * @return
     * @throws JsonProcessingException
     * @throws InvokeAPIException
     */
    private String doSettlement(List<Map<String, Object>> resultMapList, String deviceId, String mchAccount) throws JsonProcessingException, InvokeAPIException {

        LOGGER.info("[IPASS_NON_TAXI][doSettlement] start ");
        LOGGER.info("[IPASS_NON_TAXI][doSettlement] resultMapList => " + resultMapList);
        Merchant merchant = merchantService.getOne(mchAccount);
        Device device = CollectionUtils.isEmpty(resultMapList) ? deviceService.getOne(merchant, deviceId) : (Device) resultMapList.get(0).get("device");
        long bankId = Long.valueOf(env.getProperty("bank.id.32100"));
        PaymentAccount paymentAccount = paymentAccountService.getOne(merchant.getAccountId(), bankId);

        IPassSettlementRequest iPassSettlementRequest = new IPassSettlementRequest(testMode, device, paymentAccount);
        String requestJsonStr = new Gson().toJson(iPassSettlementRequest);
        LOGGER.info("[IPASS_NON_TAXI][doSettlement]  request => " + requestJsonStr);
        String result = this.sendPost(NON_TAXI_HOST, requestJsonStr);
        LOGGER.info("[IPASS_NON_TAXI][doSettlement]  response => " + result);
        IPassBaseResponse response = new Gson().fromJson(result, IPassBaseResponse.class);

        String checkoutStatus = SystemInstance.STATUS_DESCRIPTION_SUCCESS.equals(response.gettXNResult()) ? "COMP" : "FAIL";
        this.updateCheckoutCtrl(checkoutStatus, resultMapList);

        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setMerchantId(mchAccount);
        requestHeader.setMethod("32100");
        requestHeader.setServiceType("Settlement");

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    /**
     * 手動加值
     *
     * @param device
     * @param paymentAccount
     * @param requestHeader
     * @return
     */
    private String doCashTopUp(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader, JsonObject requestData, Merchant merchant) throws Exception {

        LOGGER.info("[IPASS_NON_TAXI][CashTopUp][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");

        long isRetry = requestData.get("Retry").getAsLong();
        String orderId = requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString();
        String batchNum = this.getDate();
        String userId;
        if (isRetry > 0 && isRetry < 4) {
            ReserveDetail reserveDetail = reserveDetailService.getOne(requestHeader.getMerchantId(), orderId);
            userId = reserveDetail.getUserId();
        } else if (isRetry >= 4) {
            LOGGER.error("[ICASH][TradeCharge]. " + device.getOwnerDeviceId() + ", " + requestHeader.getMerchantId());
            throw new RuntimeException("Already retry 3 times.");
        } else {
            userId = this.getUserId(merchant, requestData, device, paymentAccount);
        }

        IPassCashTopUpRequest request = new IPassCashTopUpRequest(testMode, device, paymentAccount);
        request.setAmount(requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString());

        String requestJsonStr = new ObjectMapper().writeValueAsString(request);
        LOGGER.info("[IPASS_NON_TAXI][CashTopUp]  request => " + requestJsonStr);
        String result = this.sendPost(NON_TAXI_HOST, requestJsonStr);
        LOGGER.info("[IPASS_NON_TAXI][CashTopUp]  response => " + result);

        IPassBaseResponse response = new Gson().fromJson(result, IPassBaseResponse.class);

        this.writeDBLog(requestHeader.getMerchantId(),
                requestHeader.getServiceType(), requestHeader.getMethod(), "RESERVE | " + paymentAccount.getAccount()
                        + "|" + device.getOwnerDeviceId() + "|" + response.getAmount(),
                response.getOrderId() + "|" + response.gettXNResult());

        if (StringUtils.isNotEmpty(response.getNewAESKey())) {
            device.setAesKey(response.getNewAESKey());
        }
        deviceService.save(device);
        ReserveDetail reserveDetail = this.saveReserveDetail(batchNum, requestData, requestHeader, orderId, response, device, userId, getDate(), 0, 0, getTime());

        if (SystemInstance.STATUS_DESCRIPTION_SUCCESS.equals(response.gettXNResult())) {
            reserveDetail.setRrn(response.getRrn());
            reserveDetailService.save(reserveDetail);
            //列印收據
            this.printReceipt(response, device, paymentAccount, requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString(), response.getChineseErrorMessage());
        } else {
            String errCode = StringUtils.defaultIfEmpty(response.getMcErrorCode(), "900000");
            errCode = errCode.length() > 6 ? errCode.substring(errCode.length() - 4) : errCode;
            response.setErrorCode(errCode + " - " + response.getChineseErrorMessage());
        }
        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    /**
     * 加值取消
     *
     * @param device
     * @param paymentAccount
     * @param requestHeader
     * @return
     */
    private String doCancelCashTopUp(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader, JsonObject requestData, Merchant merchant) throws Exception {

        LOGGER.info("[IPASS_NON_TAXI][CancelCashTopUp][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");

        long isRetry = requestData.get("Retry").getAsLong();
        if (isRetry >= 4) {
            LOGGER.error("[IPASS_NON_TAXI][CancelCashTopUp]. " + device.getOwnerDeviceId() + ", " + requestHeader.getMerchantId());
            throw new RuntimeException("Already retry 3 times.");
        }
        String userId = this.getUserId(merchant, requestData, device, paymentAccount);
        List<ReserveDetail> listTopOneByUserId = reserveDetailService.listTopOneByUserId(merchant.getAccountId(), device.getOwnerDeviceId(), userId);
        ReserveDetail reserveDetail = CollectionUtils.isNotEmpty(listTopOneByUserId) ? listTopOneByUserId.get(0) : null;
        if (Objects.isNull(reserveDetail)) {
            throw new OrderNotFoundException(SystemInstance.ORDER_NOT_FOUND);
        }

        String amount = String.valueOf(reserveDetail.getAmount());
        if ("0".equals(amount)) {
            LOGGER.error("[IPASS_NON_TAXI][CancelCashTopUp] Tx Amount is 0. ");
            throw new RuntimeException("order reserve Amount is 0.");
        }

        IPassCancelCashTopUpRequest request = new IPassCancelCashTopUpRequest(testMode, device, paymentAccount);
        request.setAmount(amount);
        request.setCancelAddRrn(reserveDetail.getRrn());
        String requestJsonStr = new ObjectMapper().writeValueAsString(request);
        LOGGER.info("[IPASS_NON_TAXI][CancelCashTopUp]request => " + requestJsonStr);
        String result = this.sendPost(NON_TAXI_HOST, requestJsonStr);
        LOGGER.info("[IPASS_NON_TAXI][CancelCashTopUp]response => " + result);

        IPassBaseResponse response = new Gson().fromJson(result, IPassBaseResponse.class);

        String batchNum = this.getDate();

        this.writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                "Cancel(NOT DO): " + merchant.getAccountId() + "|" + device.getOwnerDeviceId(), "N/A");

        if (StringUtils.isNotEmpty(response.getNewAESKey())) {
            device.setAesKey(response.getNewAESKey());
        }
        deviceService.save(device);

        if (SystemInstance.STATUS_DESCRIPTION_SUCCESS.equals(response.gettXNResult())) {
            ReserveCancel reserveCancel = new ReserveCancel();
            reserveCancel.setAccountId(requestHeader.getMerchantId());
            reserveCancel.setBatchNo(batchNum);
            reserveCancel.setReserveDetail(reserveDetail);
            reserveCancel.setCashier(requestHeader.getMerchantId());
            reserveCancel.setAmount(Long.valueOf(amount));
            reserveCancel.setDeviceId(device.getOwnerDeviceId());
            reserveCancel.setMethod(requestHeader.getMethod());
            reserveCancel.setUserId(userId);
            reserveCancel.setOrderId(reserveDetail.getReserveOrderId());
            reserveCancel.setRrn(response.getRrn());
            reserveCancel.setStatus("Success".equals(response.gettXNResult()) ? "0" : "1");
            reserveCancel.setEzcTxnDate(getDate() + getTime());
            reserveCancel.setDeviceRandomId(device);
            reserveCancel.setCreateTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
            reserveCancelService.save(reserveCancel);
            //列印收據
            this.printReceipt(response, device, paymentAccount, amount, response.getCardNumberForPrint());
        } else {
            String errCode = StringUtils.defaultIfEmpty(response.getMcErrorCode(), "900000");
            errCode = errCode.length() > 6 ? errCode.substring(errCode.length() - 4) : errCode;
            response.setErrorCode(errCode + " - " + response.getChineseErrorMessage());
        }

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    private void insertTaxiTransactionDetail(TradeDetail tradeDetail, Merchant merchant, PaymentAccount paymentAccount, Device device, IPassTradeResponse response, JsonObject requestData, String taxiTransactionDetailStatus) {
        Map<String, String> taxiTransactionDetail = new HashMap<>();
        taxiTransactionDetail.put("orderId", tradeDetail.getOrderId());
        taxiTransactionDetail.put("accountId", merchant.getAccountId());
        taxiTransactionDetail.put("terminalId", paymentAccount.getAccount());
        taxiTransactionDetail.put("deviceId", device.getOwnerDeviceId());
        taxiTransactionDetail.put("licensePlate", device.getLicensePlate());
        taxiTransactionDetail.put("cash", String.valueOf(tradeDetail.getPayment()));
        taxiTransactionDetail.put("carePoint", String.valueOf(tradeDetail.getCarePoint()));
        taxiTransactionDetail.put("transactionDetail", tradeDetail.getDescription());
        taxiTransactionDetail.put("transactionType", "21");
        taxiTransactionDetail.put("cardId", StringUtils.defaultIfBlank(response.getCardSN(), requestData.get("CardSNFull").getAsString()));
        taxiTransactionDetail.put("cardType", StringUtils.defaultIfBlank(response.getSpecialCardType(), requestData.get("SpecialCardType").getAsString()));
        taxiTransactionDetail.put("identityInfo", StringUtils.defaultIfBlank(response.getSpecialIdentityType(), requestData.get("SpecialIdentityType").getAsString()));
        taxiTransactionDetail.put("cardArea", StringUtils.defaultIfBlank(response.getSpecialIdentityProvider(), requestData.get("SpecialIdentityProvider").getAsString()));
        taxiTransactionDetail.put("needBalance", requestData.get("NeedCash").getAsString());
        taxiTransactionDetail.put("arcSeq", Objects.isNull(requestData.get("arcSeq")) || requestData.get("arcSeq").isJsonNull() ? null : requestData.get("arcSeq").getAsString());
        taxiTransactionDetail.put("transMsg", Objects.isNull(requestData.get("transMsg")) || requestData.get("transMsg").isJsonNull() ? null : requestData.get("transMsg").getAsString());
        taxiTransactionDetail.put("personalIdentity", response.getIdentityType());
        taxiTransactionDetail.put("status", taxiTransactionDetailStatus);
        try {
            LOGGER.info("[REQ][TAXI_RECORD_INSERT_API_URL] object = " + JsonUtil.toJsonStr(taxiTransactionDetail));
            TaxiTransactionDetail taxiTransactionDetail1 = taxiTransactionDetailService.save(JsonUtil.toJsonStr(taxiTransactionDetail));
            LOGGER.info("[RESPONSE][TAXI_RECORD_INSERT_API_URL] " + JsonUtil.toJsonStr(taxiTransactionDetail1));
        } catch (Exception e) {
            LOGGER.error("[REQUEST][TAXIRECORD] INSERT ERROR. " + tradeDetail.getOrderId() + ", "
                    + merchant.getAccountId() + ", " + device.getOwnerDeviceId());
            LOGGER.error(e.getMessage());
        }

    }

    private void saveTradeDetailOtherInfo(TradeDetail tradeDetail, IPassBaseResponse response) {
        TradeDetailOtherInfo tradeDetailOtherInfo = new TradeDetailOtherInfo();
        tradeDetailOtherInfo.setTradeDetailRandomId(tradeDetail.getTradeDetailRandomId());
        tradeDetailOtherInfo.setExeTime(1L);
        tradeDetailOtherInfo.setCardNumberForPrint(StringUtils.defaultString(response.getCardNumberForPrint(), response.getEzCardId()));
        tradeDetailOtherInfo.setCardId(response.getEzCardId());
        tradeDetailOtherInfoService.save(tradeDetailOtherInfo);
    }
}
