package co.intella.service.impl;


import co.intella.model.IntTbLookupCode;
import co.intella.model.IntTbLookupCodeNoFK;
import co.intella.net.HttpRequestUtil;
import co.intella.repository.INT_TB_LookupCodeRepository;
import co.intella.service.LookupCodeService;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class LookupCodeServiceImpl implements LookupCodeService {

    private final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(LookupCodeServiceImpl.class);

    @Value("${host.dao}")
    private String DAO_URL;

    @Resource
    private INT_TB_LookupCodeRepository intTbLookupCodeRepository;

    @Override
    public List<IntTbLookupCode> findAllByLookType(String type) {
        List<IntTbLookupCode> intTbLookupCodeList = null;
        try {
            String responseEntity = HttpRequestUtil.httpGet(DAO_URL + "api/lookupCode/findAllByLookupType/" + type);
            intTbLookupCodeList = new Gson().fromJson(responseEntity, new TypeToken<List<IntTbLookupCode>>() {
            }.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return intTbLookupCodeList;
    }

    @Override
    public IntTbLookupCode findOne(String type, String code) {
        IntTbLookupCode lookupCode = null;
        try {

            String responseEntity = HttpRequestUtil.httpGet(DAO_URL + "api/lookupCode/findOne/" + type + "/" + code);

            lookupCode = new Gson().fromJson(responseEntity, IntTbLookupCode.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lookupCode;
    }

    @Override
    public boolean isTicketType(String bankId) {
        IntTbLookupCode lookupCode = findOne("ticketType", bankId);
        return !Objects.isNull(lookupCode) && !Objects.isNull(lookupCode.getLookupType());
    }

    @Override
    public IntTbLookupCode save(IntTbLookupCode intTbLookupCode) {
        try {
            String response = HttpUtil.post(DAO_URL + "api/lookupCode/update", new ObjectMapper().writeValueAsString(intTbLookupCode));
            LOGGER.info("[CHECKOUT] response for CheckoutLog" + response);
            Gson gson = JsonUtil.getGson();
            return gson.fromJson(response, IntTbLookupCode.class);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] save() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    @Override
    public List<IntTbLookupCodeNoFK> findAllByLookTypeQuery(String lookType) {
        List<IntTbLookupCodeNoFK> intTbLookupCodeList = null;
        try {
            intTbLookupCodeList = intTbLookupCodeRepository.findAllByLookType(lookType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return intTbLookupCodeList;
    }

    @Override
    public boolean isTsmcAccount(String accountId) {
        IntTbLookupCode lookupCode = findOne("TSMC", "accountList");
        return !Objects.isNull(lookupCode) && Arrays.stream(lookupCode.getType1().split(",")).anyMatch(s -> s.trim().equals(accountId));
    }

    @Override
    public boolean isFGSAccount(String accountId) {
        IntTbLookupCode lookupCode = findOne("FGS", "accountList");
        return !Objects.isNull(lookupCode) && Arrays.stream(lookupCode.getType1().split(",")).anyMatch(s -> s.trim().equals(accountId));
    }

    @Override
    public boolean isIMotoAccount(String accountId) {
        IntTbLookupCode lookupCode = findOne("imoto", "accountList");
        return !Objects.isNull(lookupCode) && Arrays.stream(lookupCode.getType1().split(",")).anyMatch(s -> s.trim().equals(accountId));
    }

    @Override
    public boolean isIParkingAccount(String accountId) {
        IntTbLookupCode lookupCode = findOne("iParking", "accountList");
        return !Objects.isNull(lookupCode) && Arrays.stream(lookupCode.getType1().split(",")).anyMatch(s -> s.trim().equals(accountId));
    }
}
