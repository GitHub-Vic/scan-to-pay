package co.intella.service.impl;

import co.intella.domain.contratStore.CustomizePageRequest;
import co.intella.exception.NoSuchMerchantException;
import co.intella.exception.OrderAlreadyExistedException;
import co.intella.model.QQrcodeParameter;
import co.intella.model.QrcodeParameter;
import co.intella.repository.QrcodeParameterRepository;
import co.intella.service.*;
import co.intella.utility.SystemInstance;
import co.intella.utility.URLMediator;
import com.google.common.collect.Lists;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Strings;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @author Alex
 */
@Service
@Transactional
public class QrcodeServiceImpl implements QrcodeService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Resource
    private QrcodeParameterRepository qrcodeParameterRepository;

    @Autowired
    private EntityManager entityManager;

    private URLMediator urlMediator = new URLMediator();

    @Resource
    private DiscountService discountService;

    @Resource
    private OrderService orderService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private DiscountTradeOLPayService discountTradeOLPayService;

    //    @Cacheable("qrcode")
    public QrcodeParameter getOne(UUID uuid) {
        QQrcodeParameter qQrcodeParameter = QQrcodeParameter.qrcodeParameter;
        BooleanExpression predicate = qQrcodeParameter.id.eq(uuid).and(
                qQrcodeParameter.status.eq(1)
        );
        return qrcodeParameterRepository.findOne(predicate);
    }

    public QrcodeParameter getOneByOrderId(String orderId) {
        QQrcodeParameter qQrcodeParameter = QQrcodeParameter.qrcodeParameter;
        BooleanExpression predicate = qQrcodeParameter.storeOrderNo.eq(orderId).and(
                qQrcodeParameter.status.eq(1)
        ).and(
                qQrcodeParameter.codeType.eq("1")
        );
        return qrcodeParameterRepository.findOne(predicate);
    }

    //    @Cacheable("qrcode")
    public QrcodeParameter getOneByShortId(String shortId) {
        QQrcodeParameter qQrcodeParameter = QQrcodeParameter.qrcodeParameter;
        BooleanExpression predicate = qQrcodeParameter.shortId.eq(shortId).and(
                qQrcodeParameter.status.eq(1));
        return qrcodeParameterRepository.findOne(predicate);

//        String sql = "SELECT * FROM allpaypass_new.QrcodeParameter where shortId = :shortId and status = 1 and timeExpire >= sysdate()";
//        Query query = entityManager.createNativeQuery(sql).setParameter("shortId", shortId);
//        query.unwrap(SQLQuery.class).addScalar("shortId", StandardBasicTypes.STRING)
//                .setResultTransformer(Transformers.aliasToBean(QrcodeParameter.class));
//        return (QrcodeParameter) query.getSingleResult();
    }

    public QrcodeParameter getOriginOneByShortId(String shortId) {
        QQrcodeParameter qQrcodeParameter = QQrcodeParameter.qrcodeParameter;
        BooleanExpression predicate = qQrcodeParameter.shortId.eq(shortId);
        return qrcodeParameterRepository.findOne(predicate);
    }

    //    @Cacheable("qrcode")
    public QrcodeParameter getOneByShortIdAndMchId(String mchId, String shortId) {
        QQrcodeParameter qQrcodeParameter = QQrcodeParameter.qrcodeParameter;
        BooleanExpression predicate = qQrcodeParameter.shortId.eq(shortId)
                .and(qQrcodeParameter.mchId.eq(mchId));
        return qrcodeParameterRepository.findOne(predicate);
    }

    /**
     * Not saving shortId while modify
     */
//    @CacheEvict(value = { "qrcode"}, allEntries = true)
    public QrcodeParameter save(QrcodeParameter qrcodeParameter) {

        QrcodeParameter entity = getOneByShortIdAndMchId(qrcodeParameter.getMchId(), qrcodeParameter.getShortId());
        if (entity == null) {
            return qrcodeParameterRepository.save(qrcodeParameter);
        } else {
            entity.setBody(qrcodeParameter.getBody());
            entity.setDetail(qrcodeParameter.getDetail());
            return qrcodeParameterRepository.save(entity);
        }
    }

    //    @CacheEvict(value = { "qrcode" }, allEntries = true)
    public QrcodeParameter modifyShortId(QrcodeParameter qrcodeParameter, String shortId) {
        QrcodeParameter copy = new QrcodeParameter();
        copy.setMchId(qrcodeParameter.getMchId());
        copy.setShortId(shortId);
        copy.setMethod(qrcodeParameter.getMethod());
        copy.setDetail(qrcodeParameter.getDetail());
        copy.setBody(qrcodeParameter.getBody());
        copy.setCodeType(qrcodeParameter.getCodeType());
        copy.setTotalFee(qrcodeParameter.getTotalFee());
        copy.setDeviceInfo(qrcodeParameter.getDeviceInfo());
        copy.setOnSaleStart(qrcodeParameter.getOnSaleStart());
        copy.setOnSaleEnd(qrcodeParameter.getOnSaleEnd());
        copy.setOnSaleTotalFee(qrcodeParameter.getOnSaleTotalFee());
        copy.setParentId(qrcodeParameter);
        return qrcodeParameterRepository.save(copy);
    }

    //    @Cacheable("qrcode")
    public List<QrcodeParameter> listAll(String mchId) {
        QQrcodeParameter qQrcodeParameter = QQrcodeParameter.qrcodeParameter;
        BooleanExpression predicate = qQrcodeParameter.mchId.eq(mchId)
                .and(qQrcodeParameter.codeType.eq("2").or(qQrcodeParameter.codeType.eq("3")));

        return Lists.newArrayList(qrcodeParameterRepository.findAll(predicate));
    }

    public void registerMediator(URLMediator urlMediator) {
        this.urlMediator = urlMediator;
    }

    public String getURLMediator() {

        return this.urlMediator.getUrl();
    }

    //    @Cacheable("qrcode")
    public List<QrcodeParameter> pageList(CustomizePageRequest request) {
        Page<QrcodeParameter> pageableList = getPageList(request);
        return pageableList.getContent();
    }

    public long pageCount(CustomizePageRequest request) {
        Page<QrcodeParameter> pageableList = getPageList(request);
        return pageableList.getTotalElements();
    }

    public boolean checkQrcodeExpired(QrcodeParameter qrcodeParameter) {
        List<String> timeLimitCodeType = Arrays.asList("1", "4");
        if (timeLimitCodeType.contains(qrcodeParameter.getCodeType())) {
            if (DateTime.parse(qrcodeParameter.getTimeExpire(), DateTimeFormat.forPattern(SystemInstance.DATE_PATTERN)).isBeforeNow()) {
                return true;
            }
        }
        return false;
    }

    public String getOrderId(QrcodeParameter qrcodeParameter) {

        if (qrcodeParameter.getCodeType().equals("1")) {
            return qrcodeParameter.getStoreOrderNo();
        } else {
            return orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
        }
    }

    public String getRandomBody(QrcodeParameter qrcodeParameter) {
//        if (qrcodeParameter.getCodeType().equals("1")) {
//            return qrcodeParameter.getBody();
//        }
        String body = StringUtils.defaultIfEmpty(qrcodeParameter.getBody(), "");

        if (body.contains("-")) {
            String[] temp = body.split("-");
            if (body.matches(".*-\\d{4}$")) {
                body = body.substring(0, body.lastIndexOf("-"));
            }
            if (temp.length == 2) {
                body += "-" + String.format("%04d", new Random().nextInt(9999));
            }
        } else {
            body += "-" + String.format("%04d", new Random().nextInt(9999));
        }
        return body;
    }

    @Override
    public QrcodeParameter createQrcode(QrcodeParameter qrcodeParameter) throws NoSuchMerchantException, OrderAlreadyExistedException {

        if (Objects.isNull(merchantService.getOne(qrcodeParameter.getMchId()))) {
            throw new NoSuchMerchantException("Merchant error");
        }

        if (StringUtils.isNotBlank(qrcodeParameter.getStoreOrderNo()) && Objects.nonNull(tradeDetailService.getOne(qrcodeParameter.getStoreOrderNo()))) {
            throw new OrderAlreadyExistedException("Trade exist");
        }

        qrcodeParameter.setVer("101");

        SecureRandom random = new SecureRandom();
        String shortId;
        do {
            shortId = new BigInteger(55, random).toString(32);
        } while (Objects.nonNull(this.getOneByShortId(shortId)));

        qrcodeParameter.setShortId(shortId);
        qrcodeParameter.setDeviceInfo("skb0001");
        qrcodeParameter.setCreateDate(java.time.LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss")));
        qrcodeParameter.setStatus(1);
        if (StringUtils.isBlank(qrcodeParameter.getCodeType())) {
            qrcodeParameter.setCodeType("4");   //預設 4，為樣板
            qrcodeParameter.setStatus(0);       //不能付款
        }

//        if (StringUtils.isNotBlank(qrcodeParameter.getMethod())) {
//            // 正掃現折，建立時有指定支付，直接做好折扣
//            DiscountTrade discountTrade = discountService.checkoutTotalFree(qrcodeParameter.getMchId(),
//                    Double.valueOf(qrcodeParameter.getTotalFee()).longValue(), qrcodeParameter.getMethod());
//            if (Objects.nonNull(discountTrade)) {
//                DiscountTradeOLPay discountTradeOLPay = new DiscountTradeOLPay();
//                BeanUtils.copyProperties(discountTrade, discountTradeOLPay, "discountTradeSeq");
//                discountTradeOLPay.setShortId(qrcodeParameter.getShortId());
//                discountTradeOLPayService.save(discountTradeOLPay);
//
//                qrcodeParameter.setOnSaleTotalFee(qrcodeParameter.getTotalFee());       //紀錄原始金額
//                qrcodeParameter.setTotalFee(discountTrade.getTradeAmount().longValue());
//            }
//
//        }
        LOGGER.debug("[REQUEST][GENERAL] create qrcode ");

        return qrcodeParameterRepository.save(qrcodeParameter);
    }

    @Override
    public void deleteQrcodeTemplate(QrcodeParameter qrcodeParameter) {
        QrcodeParameter _qrcodeParameter = this.getOneByShortIdAndMchId(qrcodeParameter.getMchId(), qrcodeParameter.getShortId());

        qrcodeParameterRepository.delete(_qrcodeParameter);
    }

    @Override
    public List<QrcodeParameter> qrcodeListByMchId(String MchId) {
        QQrcodeParameter qQrcodeParameter = QQrcodeParameter.qrcodeParameter;
        BooleanExpression predicate = qQrcodeParameter.mchId.eq(MchId);

        return Lists.newArrayList(qrcodeParameterRepository.findAll(predicate));
    }

    private Page<QrcodeParameter> getPageList(CustomizePageRequest request) {
        PageRequest pageRequest = new PageRequest(request.getIndex(), request.getPageSize(), Sort.Direction.DESC, "storeOrderNo");
        QQrcodeParameter qQrcodeParameter = QQrcodeParameter.qrcodeParameter;

        BooleanExpression predicate = qQrcodeParameter.mchId.eq(request.getAccountId())
                .and(qQrcodeParameter.codeType.eq("2").or(qQrcodeParameter.codeType.eq("3")));

        if (!Strings.isNullOrEmpty(request.getName())) {
            predicate = predicate.and(qQrcodeParameter.body.eq(request.getName()));
        }
        return qrcodeParameterRepository.findAll(predicate, pageRequest);
    }
}
