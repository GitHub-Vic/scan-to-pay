package co.intella.service.impl;

import co.intella.constant.ServiceType;
import co.intella.domain.gama.GamaRefundRequestData;
import co.intella.domain.integration.IntegratedMicropayResponseData;
import co.intella.domain.integration.IntegratedRefundRequestData;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.exception.OrderAlreadyExistedException;
import co.intella.exception.ServiceTypeNotSupportException;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import co.intella.service.*;
import co.intella.utility.JsonUtil;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Locale;
import java.util.Objects;

@Service
public class CashServiceImpl implements CashService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private AppNotificationService appNotificationService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Autowired
    private Environment env;

    private Locale locale = new Locale("zh_TW");

    @Override
    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {
        String generalResponseData = SystemInstance.EMPTY_STRING;

        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        PaymentAccount paymentAccount = paymentAccountService.getOneByDao(integrateMchId, bankId);
        if (Objects.isNull(paymentAccount)) {
            throw new ServiceTypeNotSupportException("Exception: The store's account isn't opened.");
        }

        String serviceType = requestHeader.getServiceType();
        switch (serviceType) {
            case ServiceType.PAYMENT:
                generalResponseData = this.doPayment(requestHeader, data, paymentAccount);
                break;
            case ServiceType.REFUND:
                generalResponseData = this.doRefund(requestHeader, data, paymentAccount);
                break;
            default:
                LOGGER.error("Service type not support. " + serviceType);
                generalResponseData = "Service type not support.";
        }

        return generalResponseData;
    }

    private String doPayment(RequestHeader requestHeader, JsonPrimitive data, PaymentAccount paymentAccount) throws Exception {

        JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);

        TradeDetail tradeDetail = tradeDetailService.getOne(requestData.get("StoreOrderNo").getAsString());

        if (Objects.isNull(tradeDetail)) {
            tradeDetail = new TradeDetail();
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetail.setOrderId(requestData.get("StoreOrderNo").getAsString());
            tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
            tradeDetail.setAccountId(requestHeader.getMerchantId());
            tradeDetail.setPayment(Long.valueOf(requestData.get("TotalFee").getAsString()));
            tradeDetail.setPaymentAccount(paymentAccount);
            tradeDetail.setSystemOrderId(requestData.get("StoreOrderNo").getAsString());
            tradeDetail.setMethod("15300");
            tradeDetail.setDetail(requestData.get("Body").getAsString());
            tradeDetail.setRemark(requestData.get("Remark").getAsString());
            tradeDetail = tradeDetailService.save(tradeDetail);
            appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
        } else {
            throw new OrderAlreadyExistedException("Trade exist");
        }

        ResponseGeneralBody responseGeneralBody = this.getResponseBody(requestHeader, tradeDetail, messageSource);
        IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();
        integratedMicropayResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
        responseGeneralBody.setData(integratedMicropayResponseData);

        return JsonUtil.toJsonStr(responseGeneralBody);
    }


    private ResponseGeneralBody getResponseBody(RequestHeader requestHeader, TradeDetail tradeDetail, MessageSource messageSource) {
        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        ResponseGeneralHeader header = new ResponseGeneralHeader();

        header.setStatusCode("0000");
        header.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        header.setMethod(tradeDetail.getMethod());
        header.setMchId(tradeDetail.getPaymentAccount().getMerchant().getAccountId());
        header.setServiceType(requestHeader.getServiceType());
        header.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        responseGeneralBody.setHeader(header);

        return responseGeneralBody;
    }

    private String doRefund(RequestHeader requestHeader, JsonPrimitive data, PaymentAccount paymentAccount) throws Exception {

        GamaRefundRequestData gamaRefundRequestData = new Gson().fromJson(data.getAsString(),
                GamaRefundRequestData.class);
        IntegratedRefundRequestData integratedRefundRequestData = new Gson().fromJson(data.getAsString(),
                IntegratedRefundRequestData.class);

        JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

        tradeProcedureService.setTradeDetailResponse(requestHeader.getMerchantId(), requestHeader, "", data);

        tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());

        ResponseGeneralBody responseGeneralBody = this.getResponseBody(requestHeader, tradeDetail, messageSource);
        IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();
        integratedMicropayResponseData.setSysRefundNo(tradeDetail.getSystemOrderId());
        integratedMicropayResponseData.setStoreOrderNo(tradeDetail.getOrderId());
        integratedMicropayResponseData.setStoreRefundNo(tradeDetail.getStoreRefundId());
        responseGeneralBody.setData(integratedMicropayResponseData);

        return JsonUtil.toJsonStr(responseGeneralBody);
    }
}
