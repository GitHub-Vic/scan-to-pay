package co.intella.service.impl;

import co.intella.domain.weixin.ScheduledQueryJob;
import co.intella.domain.weixin.WeixinSingleQueryRequestData;
import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import co.intella.net.HttpRequestUtil;
import co.intella.service.*;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author Miles
 */
@Service
@Transactional
public class QueryJobServiceImpl implements QueryJobService {

    private final Logger LOGGER = LoggerFactory.getLogger(QueryJobServiceImpl.class);

    @Resource
    private WeixinService weixinService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Value("${host.pi.request.inapp}")
    private String PI_API_URL;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private MerchantService merchantService;

    private static List<ScheduledQueryJob> jobList = new ArrayList<ScheduledQueryJob>();

    @Async
    public void addJob(String orderId, String merchantId, String paymentAccount, long bankId) {
        ScheduledQueryJob job = new ScheduledQueryJob();
        job.setMerchantId(merchantId);
        job.setPaymentAccount(paymentAccount);
        job.setOrderId(orderId);
        job.setBankId(bankId);
        job.setStartTime(new Date().getTime());
        jobList.add(job);
    }

    public List<ScheduledQueryJob> list() {
        return jobList;
    }

    public void doJobs() {

        for (Iterator<ScheduledQueryJob> iterator = jobList.iterator(); iterator.hasNext(); ) {

            ScheduledQueryJob job = iterator.next();

            try {
                LOGGER.info("[JOB][" + job.getBankId() + "][" + job.getOrderId() + "]");
                WeixinSingleQueryRequestData requestData = new WeixinSingleQueryRequestData();
                requestData.setVer("101");
                requestData.setMethod("10");
                requestData.setTradeType("ORDERQUERY");
                requestData.setMchId(job.getPaymentAccount());
                requestData.setOutTradeNo(job.getOrderId());

                if (job.getBankId() == 1) {
                    String result = weixinService.singleQuery(requestData);
                    JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);
                    LOGGER.info("[JOB][WX][RESPONSE] " + jsonObject);

                    if ("0000".equals(jsonObject.get("Header").getAsJsonObject().get("StatusCode").getAsString())) {

                        JsonObject dataValueJson = new Gson()
                                .fromJson(jsonObject.get("Data").getAsJsonObject().get("DataValue").getAsString(),
                                        JsonObject.class);

                        String systemStatus = dataValueJson.get("SYS_STATUS").getAsString();
                        String sysorder = Objects.isNull(dataValueJson.get("SYS_ORDER_NO")) || dataValueJson.get("SYS_ORDER_NO").isJsonNull() ? "" : dataValueJson.get("SYS_ORDER_NO").getAsString();

                        if (systemStatus.equals("1")) {
                            TradeDetail tradeDetail = tradeDetailService.getOne(job.getMerchantId(), job.getOrderId());
                            if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                                LOGGER.info("[JOB][WX][NOTIFY]:" + tradeDetail.getOrderId());
                                tradeDetail.setSystemOrderId(sysorder);
                                tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                                tradeDetailService.save(tradeDetail);
                                appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                            }
                            iterator.remove();
                        } else if (systemStatus.equals("2")) {
                            TradeDetail tradeDetail = tradeDetailService.getOne(job.getMerchantId(), job.getOrderId());
                            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                            tradeDetailService.save(tradeDetail);
                            iterator.remove();
                        } else if (systemStatus.equals("4")) {
                            TradeDetail tradeDetail = tradeDetailService.getOne(job.getMerchantId(), job.getOrderId());
                            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                            tradeDetail.setSystemOrderId(sysorder);
                            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                            tradeDetailService.save(tradeDetail);
                            iterator.remove();
                        } else if (systemStatus.equals("15")) {
                            TradeDetail tradeDetail = tradeDetailService.getOne(job.getMerchantId(), job.getOrderId());
                            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                            tradeDetailService.save(tradeDetail);
                            iterator.remove();
                        }
                    }

                } else if (job.getBankId() == 7) { // Pi Inapp

                    PaymentAccount paymentAccount = paymentAccountService.getOne(job.getMerchantId(), 7);
                    TradeDetail tradeDetail = tradeDetailService.getOne(job.getMerchantId(), job.getOrderId());

                    Map<String, String> requestMap = new HashMap<String, String>();
                    requestMap.put("partner_id", paymentAccount.getAccount());
                    requestMap.put("partner_key", tradeDetail.getMchKey());
                    requestMap.put("partner_order_id", job.getOrderId());

                    String piResponse = HttpRequestUtil.post(PI_API_URL + "payments/orders",
                            new Gson().toJson(requestMap));
                    LOGGER.info("[Job][Pi] " + piResponse);
                    JsonObject json = new Gson().fromJson(piResponse, JsonObject.class);

                    if ("success".equals(json.get("result").getAsString())
                            && "3".equals(json.get("status").getAsString())) {
                        LOGGER.info("[Job][Pi][" + job.getOrderId() + "] Complete.");
                        tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);

                        if (json.has("paid_at")) {
                            tradeDetail.setPlatformPaidDate(json.get("paid_at").getAsString());
                        }

                        tradeDetailService.save(tradeDetail);
                        iterator.remove();
                    } else {
                        // todo
                        // request failure.
                    }

                } else {
                    // todo
                }

                // check jobs and remove jobs
                long checkJobTime = new Date().getTime() - job.getStartTime();
                LOGGER.info("[JOB][" + job.getBankId() + "]" + job.getOrderId() + ", " + checkJobTime);
                if (checkJobTime > 10 * 60 * 1000) {
                    LOGGER.info("[JOB][" + job.getBankId() + "]" + job.getOrderId() + "[EXPIRE]");
                    iterator.remove();
                }

            } catch (Exception e) {
                LOGGER.error("[EXCEPTION][JOB][" + job.getBankId() + "] " + job.toString() + ", \n" + e.getMessage()
                        + "\n" + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            }
        }
    }
}
