package co.intella.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.querydsl.core.types.dsl.BooleanExpression;

import co.intella.domain.contratStore.DeviceVo;
import co.intella.model.Device;
import co.intella.model.INT_TB_CheckoutCtrl;
import co.intella.model.Merchant;
import co.intella.model.QDevice;
import co.intella.net.HttpRequestUtil;
import co.intella.repository.DeviceRepository;
import co.intella.service.DeviceService;

/**
 * Created by Alexchiu on 2017/3/21.
 */
@Service
@Transactional
public class DeviceServiceImpl implements DeviceService {

    @Resource
    private DeviceRepository deviceRepository;
    
    @Autowired
	private EntityManager entityManager;

    private final Logger LOGGER = LoggerFactory.getLogger(DeviceServiceImpl.class);

    private static String HOST_DAO = "http://localhost:8080/scan2pay-data/";

    public List<Device> listAllByMerchant(Merchant owner) {
        QDevice qDevice = QDevice.device;
        BooleanExpression predicate = qDevice.owner.eq(owner);

        Iterable<Device> a = deviceRepository.findAll(predicate);

        if(a instanceof List) {
            return (List<Device>) a;
        }
        ArrayList<Device> list = new ArrayList<Device>();
        if(a != null) {
            for(Device e: a) {
                list.add(e);
            }
        }

        return list;

    }

    public List<DeviceVo> listAllByMerchantAndType(String merchantId, String type) {
        LOGGER.info("[REQUEST] listAllByMerchantAndType. " + merchantId + "/" + type);
        try {
            String result = HttpRequestUtil.httpGet(HOST_DAO + "api/device/list/" + merchantId + "/" + type);
            if (result.equals("no data."))
            {
                return null;
            }
            else {
                return new Gson().fromJson(result, new TypeToken<List<DeviceVo>>(){}.getType());
            }

        } catch (Exception e) {
//            DeviceVo dongleForException = new DeviceVo();
//            dongleForException.setDescription("NULLDONGLE");
//            dongleForException.setOwnerDeviceId("XXXXXXXX");
//            List<DeviceVo> deviceList = new ArrayList<>();
//            deviceList.add(dongleForException);
//            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
//            return deviceList;
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public DeviceVo getOneByMerchantAndDeviceId(String deviceId, String merchantId) {
        LOGGER.info("[REQUEST] getOne. " + merchantId + "/" + deviceId);
        try {
            String result = HttpRequestUtil.httpGet(HOST_DAO + "api/device/get?accountId=" + merchantId + "&deviceId=" + deviceId);
            return new Gson().fromJson(result, DeviceVo.class);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }
    
    public Device getOneByTerminal(String deviceId, String terminalId) {
      LOGGER.info("[REQUEST] getOneByTerminal. " + terminalId + "/" + deviceId);
      try {
          String result = HttpRequestUtil.httpGet(HOST_DAO + "api/device/getOneByTerminal?terminalId=" + terminalId + "&deviceId=" + deviceId);
          return new Gson().fromJson(result, Device.class);
      } catch (Exception e) {
          LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
          return null;
      }
  }

    public String deviceExist(String deviceId) {
        LOGGER.info("[REQUEST] deviceExist. " +  deviceId);
        try {
            String result = HttpRequestUtil.httpGet(HOST_DAO + "api/device/deviceExist?deviceId=" + deviceId);
            return result;
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public Device getOne(UUID deviceId) {
        QDevice qDevice = QDevice.device;
        BooleanExpression predicate = qDevice.deviceRandomId.eq(deviceId);
        return deviceRepository.findOne(predicate);
    }

    public Device getOne(Merchant merchant, String ownerDeviceId) {
        QDevice qDevice = QDevice.device;
        BooleanExpression predicate = qDevice.owner.eq(merchant)
                .and(qDevice.ownerDeviceId.eq(ownerDeviceId));
        return deviceRepository.findOne(predicate);
    }

    public Device getOne(String license) {
        QDevice qDevice = QDevice.device;
        BooleanExpression predicate = qDevice.licensePlate.eq(license);
        return deviceRepository.findOne(predicate);
    }

    public void delete(String deviceId) {

    }

    public void save(Device device) {
        deviceRepository.save(device);
    }
    
    public Device getOne(String type, String ownerDeviceId) {
        QDevice qDevice = QDevice.device;
        BooleanExpression predicate = qDevice.type.eq(type)
                .and(qDevice.ownerDeviceId.eq(ownerDeviceId));
        return deviceRepository.findOne(predicate);
    }
    
    public Device getOneByTDOrderId (String orderId) {
    	String sql = "select d from TradeDetail t " + 
    			"inner join t.deviceRandomId d " + 
    			"where t.orderId = :orderId  ";
		Query query = entityManager.createQuery(sql, Device.class)
				.setParameter("orderId", orderId);
		return (Device) query.getSingleResult();
    }
    public Device getOneByRDOrderId (String orderId) {
    	String sql = "select d from ReserveDetail r " + 
    			"inner join r.deviceRandomId d " + 
    			"where r.reserveOrderId = :orderId  ";
    	Query query = entityManager.createQuery(sql, Device.class)
    			.setParameter("orderId", orderId);
    	return (Device) query.getSingleResult();
    }
    public Device getOneByRefundOrderId (String orderId) {
    	String sql = "select d from RefundDetail r " + 
    			"inner join r.deviceRandomId d " + 
    			"where r.orderId = :orderId  ";
    	Query query = entityManager.createQuery(sql, Device.class)
    			.setParameter("orderId", orderId);
    	return (Device) query.getSingleResult();
    }
}
