package co.intella.service.impl;

import java.util.*;

import javax.annotation.Resource;

import co.intella.domain.pi.*;
import co.intella.utility.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;

import co.intella.domain.integration.IntegratedCancelResponseData;
import co.intella.domain.integration.IntegratedMicropayResponseData;
import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.IntegratedSingleOrderQueryResponseData;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.model.MailInformation;
import co.intella.model.RefundDetail;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import co.intella.net.HttpRequestUtil;
import co.intella.service.MailService;
import co.intella.service.PiService;
import co.intella.service.RefundDetailService;
import co.intella.service.TradeDetailService;
import co.intella.utility.PiOLUtil;
import co.intella.utility.StatusManipulation;
import co.intella.utility.SystemInstance;

/**
 * @author Miles
 */
@Service
@Transactional
public class PiServiceImpl implements PiService {

    private final Logger LOGGER = LoggerFactory.getLogger(PiServiceImpl.class);

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private RefundDetailService refundDetailService;

    @Resource
    private MailService mailService;

    @Value("${host.pi.request.inapp}")
    private String PI_ORDER_CHECK_HOST;

    @Value("${emergency.mail.notify.staff}")
    private String[] mailToStaff;

    @Value("${execution.mode}")
    private String EXECUTION_MODE;

    @Value("${host.pi.offline.url}")
    private String PI_OFFLINE_API_URL;

    @Value("${host.pi.online.url}")
    private String PI_ONLINE_URL;

    private Locale locale = new Locale("zh_TW");

    @Autowired
    private MessageSource messageSource;

    List<String> paymentGateway = Arrays.asList("pi_wallet", "credit_card");

    public String reconciliation(String partnerId, String partnerKey, String date) {
        try {
            LOGGER.info("[reconciliation] payment starting ... ");
            PiReconciliationRequest piReconciliationRequest = new PiReconciliationRequest();
            piReconciliationRequest.setPartnerId(partnerId);
            piReconciliationRequest.setPartnerKey(partnerKey);
            piReconciliationRequest.setDate(date);

            LOGGER.info("[QUERY]" + PI_ORDER_CHECK_HOST);

            String requestJson = new ObjectMapper().writeValueAsString(piReconciliationRequest);
            String piResponse = HttpRequestUtil.post(PI_ORDER_CHECK_HOST + "payments/orders/query", requestJson);

            PiReconciliationResponse piReconciliationResponse = new Gson().fromJson(piResponse,
                    PiReconciliationResponse.class);

            if (!"0".equals(piReconciliationResponse.getErrorCode())) {

                LOGGER.info("[Reconciliation] PI Request-data wrong. " + piResponse);

                if (!"dev".equals(EXECUTION_MODE)) {
                    sendEmergencyMail(
                            "[Reconciliation] PI Request-data wrong : [REQ]" + requestJson + ", \n [RSP] " + piResponse,
                            "reconciliation");
                }

            } else if (!piReconciliationResponse.getAcceptRecord().isEmpty()) {
                for (PiReconciliationAcceptRecord element : piReconciliationResponse.getAcceptRecord()) {
                    TradeDetail tradeDetail = tradeDetailService.getOneByOrderIdAndMethod(element.getPartnerOrderId(),
                            "60300");

                    if (tradeDetail != null) {
                        LOGGER.info("[Reconciliation] " + tradeDetail.getOrderId() + ", " + tradeDetail.getStatus());
                        tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                        tradeDetail.setPlatformPaidDate(element.getPaidAt());
                        tradeDetail.setReconciliation(true);
                        // todo: the amt of the tradeDetail does not equal the money of acceptRecord
                        tradeDetailService.save(tradeDetail);
                    } else {
                        LOGGER.info("[Reconciliation] order not found. " + element.getPartnerOrderId());
                    }
                }
            } else {
                LOGGER.info("[Reconcile] PI Request-data do nothing");
            }

            LOGGER.info("[Reconcile] payment end ... ");

            LOGGER.info("[Reconcile] refund starting ... ");

            if (piReconciliationResponse.getRefundRecord() != null
                    && !piReconciliationResponse.getRefundRecord().isEmpty()) {
                for (PiReconciliationRefundRecord element : piReconciliationResponse.getRefundRecord()) {
                    RefundDetail refundDetail = refundDetailService
                            .getOneByOrderIdAndMethod(element.getPartnerOrderId(), "60300");
                    if (refundDetail != null) {

                        LOGGER.info("[Reconciliation] " + refundDetail.getOrderId() + ", " + refundDetail.getStatus());
                        refundDetail.setPlatformRefundDate(element.getRefundedAt());
                        refundDetail.setReconciliation(true);
                        refundDetail.setStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                        refundDetailService.save(refundDetail);

                    } else {
                        LOGGER.info("[Reconciliation] order not found. " + element.getPartnerOrderId());
                    }
                }
            } else {
                LOGGER.info("[Reconciliation] refund order not found. ");
            }

            LOGGER.info("[reconciliation] refund end ... ");

            Map<String, String> map = new HashMap<String, String>();
            map.put("status", piReconciliationResponse.getResult());
            map.put("errorCode",
                    StatusManipulation.getCodeOfPi(piReconciliationResponse.getErrorCode(), "Reconciliate"));
            return new Gson().toJson(map);
        } catch (Exception e) {

            LOGGER.error("[EXCEPTION][reconcile] " + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            if (!"dev".equals(EXECUTION_MODE)) {
                sendEmergencyMail(e.getMessage() + "\n" + Arrays.toString(e.getStackTrace()).replaceAll(",", ",</br>"),
                        "reconciliation");
            }

            Map<String, String> map = new HashMap<String, String>();
            map.put("status", "error");
            map.put("errorCode", "9998");
            return new Gson().toJson(map);
        }
    }

    public String doRequest(PiBasicRequestData request, RequestHeader header, TradeDetail tradeDetail,
                            String integrateMchId) {
        try {
            String requestJson = new ObjectMapper().writeValueAsString(request);
            String result = null;

            String type = header.getServiceType();

            LOGGER.info("[Pi][REQUEST]:" + new Gson().toJson(requestJson));
            if (type.equals("Refund")) {
                if (tradeDetail.getServiceType().equals(SystemInstance.TYPE_MICROPAY)) {

                    String piResponse = refund(request); // HttpRequestUtil.post(
                    // "https://intella.piapp.com.tw/server-api/enterprise/retailer/paybarcode/refund",
                    // requestJson);
                    LOGGER.info("[Pi][Response]" + piResponse);
                    return convertRefundResponse(piResponse, header, integrateMchId);
                } else if (tradeDetail.getServiceType().equals(SystemInstance.TYPE_OLPAY)) {
                    PiOLRefundRequestData req = new PiOLRefundRequestData();
                    req.setPartnerId("UE000MILES");
                    req.setCreatedAt(DateTime.now().toString(SystemInstance.DATE_PI_PATTERN));
                    req.setOrderId(tradeDetail.getOrderId());
                    req.setTxId(tradeDetail.getSystemOrderId());
                    req.setRefundmount(Integer.valueOf(((PiRefundRequestData) request).getRefundFee()));

                    LOGGER.info("[Pi][RefundOLPay][Request]:" + new ObjectMapper().writeValueAsString(req));

                    String piResponse = refundOLPay(req);// HttpRequestUtil.post(
                    // "https://intella.piapp.com.tw/server-api/enterprise/retailer/paybarcode/ask",
                    // requestJson);
//                    String piResponse ="{\"result\":\"failure\",\"order_id\":\"\",\"pi_tx_id\":\"\",\"refund_amount\":\"\",\"refund_at\":\"\",\"error_code\":1022}";
                    LOGGER.info("[Pi][Response]" + piResponse);
                    return convertRefundOLResponse(piResponse, header, integrateMchId);
                } else {
                    return null;
                }

            } else if (type.equals(SystemInstance.TYPE_MICROPAY)) {
                String piResponse = micropay(request);
                LOGGER.info("[Pi][Micropay][Response]" + piResponse);

                PiMicropayResponseData paymentResponseData;
                try {
                    paymentResponseData = new Gson().fromJson(piResponse, PiMicropayResponseData.class);
                } catch (JsonSyntaxException e) {
                    paymentResponseData = new PiMicropayResponseData();
                }

                if (!"success".equals(paymentResponseData.getStatusDesc())) {
                    TradeDetail td = tradeDetailService.getOne(((PiMicropayRequestData) request).getStoreOrderNo());
                    td.setStatus(SystemInstance.TRADE_FAIL);
                    tradeDetailService.save(td);
                }

                return convertMicropayResponse(paymentResponseData, header, integrateMchId);
            } else if (type.equals("SingleOrderQuery")) {
                if (tradeDetail.getServiceType().equals(SystemInstance.TYPE_MICROPAY)) {
                    String piResponse = singleOrderQueryMicropay(request);// HttpRequestUtil.post(
                    // "https://intella.piapp.com.tw/server-api/enterprise/retailer/paybarcode/ask",
                    // requestJson);
                    LOGGER.info("[Pi][SingleOrderQuery][Response]" + piResponse);
                    return convertSingleQueryResponse(piResponse, header, integrateMchId, tradeDetailService.getOne(tradeDetail.getOrderId()));
                } else if (tradeDetail.getServiceType().equals(SystemInstance.TYPE_OLPAY)) {
                    PiOLSingleOrderQueryRequestData req = new PiOLSingleOrderQueryRequestData();
                    req.setPartnerId("UE000MILES");
                    req.setPartnerKey("INTELLA");
                    req.setPaymentGateway(paymentGateway.get(tradeDetail.getType()));
                    req.setOrderId(((PiSingleOrderQueryRequestData) request).getStoreOrderNo());

                    String piResponse = singleOrderQueryOLpay(req);// HttpRequestUtil.post(
                    // "https://intella.piapp.com.tw/server-api/enterprise/retailer/paybarcode/ask",
                    // requestJson);
                    LOGGER.info("[Pi][SingleOrderQueryOLPay][Response]" + piResponse);
                    return convertSingleQueryOLResponse(piResponse, header, integrateMchId, tradeDetailService.getOne(tradeDetail.getOrderId()));

                } else {
                    return null;
                }

            } else if (type.equals("Cancel")) {
                String piResponse = HttpRequestUtil.post(
                        "https://intella.piapp.com.tw/server-api/enterprise/retailer/paybarcode/cancel", requestJson);
                LOGGER.info(piResponse);
                return convertCancelResponse(piResponse, header, integrateMchId);
            } else {
                return result;
            }

        } catch (JsonSyntaxException e) {
            LOGGER.error("[EXCEPTION][Pi] doRequest." + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",</br>"));
            if (!"dev".equals(EXECUTION_MODE)) {
                sendEmergencyMail(Arrays.toString(e.getStackTrace()), "do PiRequest - JsonSyntaxException");
            }

            return "[EXCEPTION] jsonSyntax is wrong";
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION][Pi] doRequest." + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",</br>"));
            if (!"dev".equals(EXECUTION_MODE)) {
                sendEmergencyMail(Arrays.toString(e.getStackTrace()), "do PiRequest - Exception");
            }

            return "[EXCEPTION] post to pi wrong";
        }
    }

    private String convertRefundOLResponse(String piResponse, RequestHeader header, String integrateMchId) {

        JsonObject jsonObject = new JsonParser().parse(piResponse).getAsJsonObject();
        PiOLRefundResponseData piOLRefundResponseData;
        if (jsonObject.get("result").getAsString().equals("success")) {
            piOLRefundResponseData = new Gson().fromJson(piResponse, PiOLRefundResponseData.class);
        } else {
            piOLRefundResponseData = new PiOLRefundResponseData();
            piOLRefundResponseData.setErrorCode(jsonObject.get("error_code").getAsInt());
            piOLRefundResponseData.setResult(jsonObject.get("result").getAsString());

        }

        header.setMerchantId(integrateMchId);

        ResponseGeneralHeader responseHeader = new PiOLUtil().getPiRefundResponseHeader(piOLRefundResponseData, header,
                messageSource);

        IntegratedRefundResponseData responseData = new IntegratedRefundResponseData();
        if (piOLRefundResponseData.getResult().equals("success")) {
            responseData.setSysRefundNo(piOLRefundResponseData.getTxId());
            responseData.setStoreRefundNo("R" + piOLRefundResponseData.getOrderId());
            responseData.setSysOrderNo(piOLRefundResponseData.getTxId());
            responseData.setStoreOrderNo(piOLRefundResponseData.getOrderId());
            PiOLRefundAt piOLRefundAt = piOLRefundResponseData.getRefundAt();
            String time = DateTime.parse(piOLRefundAt.getDate(), DateTimeFormat.forPattern("yyyy-MM-ddHH:mm:ss.000000"))
                    .toString(SystemInstance.DATE_PATTERN);
//        piOLRefundResponseData.getRefundAt().setDate(time);
            responseData.setRefundedAt(time);
        }

        ResponseGeneralBody piResponseBody = new ResponseGeneralBody();
        piResponseBody.setHeader(responseHeader);
        piResponseBody.setData(responseData);

        return new Gson().toJson(piResponseBody);
    }

    private String convertSingleQueryOLResponse(String piResponse, RequestHeader header, String integrateMchId , TradeDetail tradeDetail) {

        PiOLSingleOrderQueryResponseData piSingleOrderQueryResponseData = JsonUtil.getGson().fromJson(piResponse,
                PiOLSingleOrderQueryResponseData.class);
        String statusCode = "0000";

        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setStatusCode(statusCode);
        responseGeneralHeader.setStatusDesc(messageSource.getMessage("response." + statusCode, null, locale));
        responseGeneralHeader.setMethod(header.getMethod());
        responseGeneralHeader.setServiceType(header.getServiceType());
        responseGeneralHeader.setMchId(integrateMchId);
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();
        integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());
        integratedSingleOrderQueryResponseData.setStoreOrderNo(tradeDetail.getOrderId());
        integratedSingleOrderQueryResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
        integratedSingleOrderQueryResponseData.setRefundedMsg(tradeDetail.getRefundStatus());
        integratedSingleOrderQueryResponseData.setPaidAt(tradeDetail.getCreateDate());
        RefundDetail refundDetail = refundDetailService.getOneByOrderIdAndMethod(tradeDetail.getOrderId(),tradeDetail.getMethod());
        if(Objects.nonNull(refundDetail)){
            integratedSingleOrderQueryResponseData.setRefundedAt(refundDetail.getCreateTime());
        }
        integratedSingleOrderQueryResponseData.setSysOrderNo("");
        if (piSingleOrderQueryResponseData.getResult().equals("01")) {

            integratedSingleOrderQueryResponseData.setStoreOrderNo(piSingleOrderQueryResponseData.getOrderId());
            integratedSingleOrderQueryResponseData
                    .setTotalFee(String.valueOf(piSingleOrderQueryResponseData.getTotalAmount()));
            integratedSingleOrderQueryResponseData.setFeeType("TWD");
            integratedSingleOrderQueryResponseData.setOrderStatus("1");
            integratedSingleOrderQueryResponseData.setSysOrderNo(piSingleOrderQueryResponseData.getPiTxId());

            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetail.setSystemOrderId(piSingleOrderQueryResponseData.getPiTxId());
            tradeDetail.setTxParams(piResponse);

//        integratedSingleOrderQueryResponseData.setPlatformRsp(piResponse);
        } else if (piSingleOrderQueryResponseData.getResult().equals("02")) {
            integratedSingleOrderQueryResponseData.setOrderStatus("2");
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
        } else if (piSingleOrderQueryResponseData.getResult().equals("03")) {
            integratedSingleOrderQueryResponseData.setOrderStatus("3");
            tradeDetail.setRefundStatus("Refund success");
        } else {
            integratedSingleOrderQueryResponseData.setOrderStatus("0");
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
        }
        tradeDetailService.save(tradeDetail);
        ResponseGeneralBody piResponseBody = new ResponseGeneralBody();
        piResponseBody.setHeader(responseGeneralHeader);
        piResponseBody.setData(integratedSingleOrderQueryResponseData);

        return new Gson().toJson(piResponseBody);

    }

    public String singleOrderQueryOLpay(PiOLSingleOrderQueryRequestData request) {
        String piResponse = null;
        try {
            piResponse = HttpRequestUtil.post(PI_ONLINE_URL + "query", new ObjectMapper().writeValueAsString(request));
        } catch (Exception e) {
            e.printStackTrace();
        }
        LOGGER.info(piResponse);
        return piResponse;
    }

    public String refundOLPay(PiOLRefundRequestData request) {
        String piResponse = null;
        try {
            piResponse = HttpRequestUtil.post(PI_ONLINE_URL + "refund", new ObjectMapper().writeValueAsString(request));
        } catch (Exception e) {
            e.printStackTrace();
        }
        LOGGER.info(piResponse);
        return piResponse;
    }

    public String refund(PiBasicRequestData request) {
        String piResponse = null;
        try {
            piResponse = HttpRequestUtil.post(PI_OFFLINE_API_URL + "refund",
                    new ObjectMapper().writeValueAsString(request));
        } catch (Exception e) {
            e.printStackTrace();
        }
        LOGGER.info(piResponse);
        return piResponse;
    }

    public String singleOrderQueryMicropay(PiBasicRequestData request) {
        String piResponse = null;

        try {
            piResponse = HttpRequestUtil.post(PI_OFFLINE_API_URL + "ask",
                    new ObjectMapper().writeValueAsString(request));
        } catch (Exception e) {
            e.printStackTrace();
        }
        LOGGER.info(piResponse);
        return piResponse;

    }

    public String doRequest(RequestHeader header, JsonPrimitive data) throws JsonProcessingException {
        return null;
    }

    public String micropay(PiBasicRequestData request) throws JsonProcessingException {

        String piResponse = null;
        try {
            piResponse = HttpRequestUtil.post(PI_OFFLINE_API_URL + "pay",
                    new ObjectMapper().writeValueAsString(request));
        } catch (Exception e) {
            e.printStackTrace();
        }
        LOGGER.info(piResponse);
        return piResponse;
    }

    private void sendEmergencyMail(String input, String emailTitle) {
        MailInformation mailInformation = new MailInformation();

        mailInformation.setMailTo(mailToStaff);
        mailInformation.setSubject("[Scan2 Pay] EMERGENCY MAIL !!! -  " + emailTitle);

        Context ctx = new Context();
        ctx.setVariable("errorMessage", input);

        try {
            mailService.send(mailInformation, ctx, "emergencyMail");
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION][reconciliation] sendEmergencyMail." + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
        }
    }

    private String convertCancelResponse(String piResponse, RequestHeader header, String integrateMchId) {
        PiCancelResponseData piCancelResponseData = new Gson().fromJson(piResponse, PiCancelResponseData.class);
        String statusCode = piCancelResponseData.getStatusDesc().equals("success") ? "0000"
                : StatusManipulation.getCodeOfPi(piCancelResponseData.getStatusCode(), header.getServiceType());

        ResponseGeneralHeader responseHeader = new ResponseGeneralHeader();
        responseHeader.setStatusCode(statusCode);
        responseHeader.setStatusDesc(piCancelResponseData.getStatusDesc());
        responseHeader.setMethod(header.getMethod());
        responseHeader.setServiceType(header.getServiceType());
        responseHeader.setMchId(integrateMchId);
        responseHeader.setResponseTime(header.getCreateTime());

        IntegratedCancelResponseData responseData = new IntegratedCancelResponseData();
        responseData.setStoreOrderNo(piCancelResponseData.getStoreOrderNo());
        responseData.setPlatformRsp(piResponse);

        ResponseGeneralBody responseBody = new ResponseGeneralBody();
        responseBody.setHeader(responseHeader);
        responseBody.setData(responseData);

        return new Gson().toJson(responseBody);
    }

    private String convertRefundResponse(String piResponse, RequestHeader header, String integrateMchId) {

        PiRefundResponseData piRefundResponseData = new Gson().fromJson(piResponse, PiRefundResponseData.class);
        String statusCode = piRefundResponseData.getStatusDesc().equals("success") ? "0000"
                : StatusManipulation.getCodeOfPi(piRefundResponseData.getStatusCode(), header.getServiceType());

        ResponseGeneralHeader responseHeader = new ResponseGeneralHeader();
        responseHeader.setStatusCode(statusCode);
        responseHeader.setStatusDesc(piRefundResponseData.getStatusDesc());
        responseHeader.setMethod(header.getMethod());
        responseHeader.setServiceType(header.getServiceType());
        responseHeader.setMchId(integrateMchId);
        responseHeader.setResponseTime(piRefundResponseData.getRefundedAt());

        IntegratedRefundResponseData responseData = new IntegratedRefundResponseData();
        responseData.setSysRefundNo(piRefundResponseData.getSysOrderNo());
        responseData.setStoreRefundNo("R" + piRefundResponseData.getStoreOrderNo());
        responseData.setSysOrderNo(piRefundResponseData.getSysOrderNo());
        responseData.setStoreOrderNo(piRefundResponseData.getStoreOrderNo());
        responseData.setRefundedAt(piRefundResponseData.getRefundedAt());

        ResponseGeneralBody piResponseBody = new ResponseGeneralBody();
        piResponseBody.setHeader(responseHeader);
        piResponseBody.setData(responseData);

        return new Gson().toJson(piResponseBody);
    }


    private String convertMicropayResponse(PiMicropayResponseData paymentResponseData, RequestHeader header, String integrateMchId) {
        //TODO  錯誤時回應為NULL  先應急改的，後續重購需重新撰寫
        String piResponse = new Gson().toJson(paymentResponseData);

        String statusCode = StringUtils.defaultIfBlank(paymentResponseData.getStatusDesc(), "").equals("success") ? "0000"
                : StatusManipulation.getCodeOfPi(StringUtils.defaultIfBlank(paymentResponseData.getStatusCode(), ""), header.getServiceType());

        ResponseGeneralHeader responseHeader = new ResponseGeneralHeader();
        responseHeader.setStatusCode(statusCode);
        responseHeader.setStatusDesc(messageSource.getMessage("response." + statusCode, null, locale));
        responseHeader.setMethod(header.getMethod());
        responseHeader.setServiceType(header.getServiceType());
        responseHeader.setMchId(integrateMchId);
        responseHeader.setResponseTime(StringUtils.defaultIfBlank(paymentResponseData.getResponseTime(), ""));

        IntegratedMicropayResponseData responseData = new IntegratedMicropayResponseData();
        responseData.setSysOrderNo(StringUtils.defaultIfBlank(paymentResponseData.getSysOrderNo(), ""));
        responseData.setStoreOrderNo(StringUtils.defaultIfBlank(paymentResponseData.getStoreOrderNo(), ""));
        responseData.setTotalFee(StringUtils.defaultIfBlank(paymentResponseData.getTotalFee(), ""));
        responseData.setAuthCode(StringUtils.defaultIfBlank(paymentResponseData.getAuthcode(), ""));
        responseData.setExtended(piResponse);
        responseData.setPlatformRsp(piResponse);

        ResponseGeneralBody piResponseBody = new ResponseGeneralBody();
        piResponseBody.setHeader(responseHeader);
        piResponseBody.setData(responseData);

        return new Gson().toJson(piResponseBody);
    }

    private String convertSingleQueryResponse(String piResponse, RequestHeader header, String integrateMchId, TradeDetail tradeDetail) {
        PiSingleOrderQueryResponseData piSingleOrderQueryResponseData = new Gson().fromJson(piResponse,
                PiSingleOrderQueryResponseData.class);
        String statusCode = piSingleOrderQueryResponseData.getStatusDesc().equals("success") ? "0000"
                : StatusManipulation.getCodeOfPi(piSingleOrderQueryResponseData.getStatusCode(),
                header.getServiceType());

        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setStatusCode(statusCode);
        responseGeneralHeader.setStatusDesc(piSingleOrderQueryResponseData.getStatusDesc());
        responseGeneralHeader.setMethod(header.getMethod());
        responseGeneralHeader.setServiceType(header.getServiceType());
        responseGeneralHeader.setMchId(integrateMchId);
        responseGeneralHeader.setResponseTime(piSingleOrderQueryResponseData.getResponseTime());

        IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();
        integratedSingleOrderQueryResponseData.setSysOrderNo(piSingleOrderQueryResponseData.getSysOrderNo());
        integratedSingleOrderQueryResponseData.setStoreOrderNo(piSingleOrderQueryResponseData.getStoreOrderNo());
        integratedSingleOrderQueryResponseData.setTotalFee(piSingleOrderQueryResponseData.getTotalFee());
        integratedSingleOrderQueryResponseData.setFeeType("TWD");
        integratedSingleOrderQueryResponseData.setStoreInfo(piSingleOrderQueryResponseData.getStoreInfo());
        integratedSingleOrderQueryResponseData.setAuthCode(piSingleOrderQueryResponseData.getAuthCode());
        integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());
        integratedSingleOrderQueryResponseData.setPaidAt(tradeDetail.getCreateDate());
        integratedSingleOrderQueryResponseData.setRefundedMsg(tradeDetail.getRefundStatus());
        RefundDetail refundDetail = refundDetailService.getOneByOrderIdAndMethod(tradeDetail.getOrderId(), tradeDetail.getMethod());
        if (Objects.nonNull(refundDetail)) {
            integratedSingleOrderQueryResponseData.setRefundedAt(refundDetail.getCreateTime());
        }
        if (piSingleOrderQueryResponseData.getTransStatus().equals("accepted")) {
            integratedSingleOrderQueryResponseData.setOrderStatus("1");
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetail.setSystemOrderId(piSingleOrderQueryResponseData.getSysOrderNo());
            tradeDetail.setTxParams(piResponse);
        } else if (piSingleOrderQueryResponseData.getTransStatus().equals("refunded")) {
            integratedSingleOrderQueryResponseData.setOrderStatus("3");
            tradeDetail.setRefundStatus("Refund success");
        } else if (piSingleOrderQueryResponseData.getTransStatus().equals("canceled")) {
            integratedSingleOrderQueryResponseData.setOrderStatus("2");
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
        } else {
            integratedSingleOrderQueryResponseData.setOrderStatus("0");
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
        }

        tradeDetailService.save(tradeDetail);
//        integratedSingleOrderQueryResponseData.setPlatformRsp(piResponse);

        ResponseGeneralBody piResponseBody = new ResponseGeneralBody();
        piResponseBody.setHeader(responseGeneralHeader);
        piResponseBody.setData(integratedSingleOrderQueryResponseData);

        return new Gson().toJson(piResponseBody);
    }

}
