package co.intella.service.impl;

import co.intella.domain.integration.*;
import co.intella.domain.line.*;
import co.intella.exception.ServiceTypeNotSupportException;
import co.intella.model.PaymentAccount;
import co.intella.model.QrcodeParameter;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import co.intella.service.*;
import co.intella.utility.LinePayUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

/**
 * @author Alex
 */
@Service
public class LineServiceImpl implements LineService {

    private final Logger LOGGER = LoggerFactory.getLogger(LineServiceImpl.class);

    @Resource
    private PaymentAccountService paymentAccountService;

    @Autowired
    private Environment env;

    @Autowired
    private MessageSource messageSource;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Value("${host.request.linepay}")
    private String LINEPAY_URL;

    @Value("${api.url}")
    private String API_URL;

    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {
        String generalResponseData = SystemInstance.EMPTY_STRING;

        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        PaymentAccount paymentAccount = paymentAccountService.getOneByDao(integrateMchId, bankId);

        if (requestHeader.getServiceType().equals(SystemInstance.TYPE_MICROPAY)) {

            paymentAccount = paymentAccountService.getOneByDao(integrateMchId, 16);
            if (null == paymentAccount) {
                throw new ServiceTypeNotSupportException("Exception: The store's account isn't opened.");
            }
            LOGGER.info("[Line][Micropay][account]" + paymentAccount.getAccount());
            LinePaymentRequestData paymentRequestData = new Gson().fromJson(data.getAsString(),
                    LinePaymentRequestData.class);
            paymentRequestData.setCurrency("TWD");
            tradeProcedureService.setTradeDetailRequest(requestHeader, data);
            String paymentResult = payment(paymentRequestData, paymentAccount,
                    paymentRequestData.getDevice());
            LOGGER.info("[Line][Micropay][Response]:" + paymentResult);
//paymentResult="{\"returnCode\":\"0000\",\"returnMessage\":\"Success.\",\"info\":{\"transactionId\":2017112200020910810,\"orderId\":\"LineT171122141233\",\"payInfo\":[{\"method\":\"CREDIT_CARD\",\"amount\":1,\"maskedCreditCardNumber\":\"************6437\"}],\"needCheck\":\"N\",\"transactionDate\":\"2017-11-22T06:12:31Z\"}}";
            tradeProcedureService
                    .setTradeDetailResponse(integrateMchId, requestHeader, paymentResult, data);

            LineResponseData lineResponseData = new Gson()
                    .fromJson(paymentResult, LineResponseData.class);

            ResponseGeneralHeader responseGeneralHeader = new LinePayUtil().getResponseHeader(
                    lineResponseData.getReturnCode(), lineResponseData.getReturnMessage(), requestHeader,
                    messageSource);
            requestHeader.setMerchantId(integrateMchId);
            IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();

            if (lineResponseData.getReturnCode().equals("0000")) {
                integratedMicropayResponseData
                        .setSysOrderNo(lineResponseData.getInfo().getTransactionId().toString());
                integratedMicropayResponseData
                        .setTotalFee(
                                String.valueOf(lineResponseData.getInfo().getPayInfos().get(0).getAmount()));
            } else {
                integratedMicropayResponseData.setSysOrderNo(SystemInstance.EMPTY_STRING);
                integratedMicropayResponseData.setTotalFee(SystemInstance.EMPTY_STRING);
            }
            integratedMicropayResponseData.setStoreOrderNo(paymentRequestData.getOrderId());
            integratedMicropayResponseData.setAuthCode(paymentRequestData.getOneTimeKey());
            integratedMicropayResponseData.setPlatformRsp(paymentResult);

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedMicropayResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);
            LOGGER.info("integrate response ready :" + generalResponseData);

        } else if (requestHeader.getServiceType().equals("Refund")) {
            LOGGER.info("[LinePay][Refund][data]" + data.getAsString());
            LineRefundRequestData lineRefundRequestData = new Gson().fromJson(data.getAsString(),
                    LineRefundRequestData.class);

            IntegratedRefundRequestData integratedRefundRequestData = new Gson()
                    .fromJson(data.getAsString(),
                            IntegratedRefundRequestData.class);

//            TradeDetail tradeDetail = tradeDetailService.getOneByDao(integratedRefundRequestData.getStoreOrderNo());

            String refundResult;
            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
            lineRefundRequestData.setRefundAmount(Long.valueOf(tradeDetail.getPayment()).intValue());

            if (tradeDetail.getServiceType().equals(SystemInstance.TYPE_MICROPAY)) {
                paymentAccount = paymentAccountService.getOneByDao(integrateMchId, 16);

                refundResult = refundPayment(lineRefundRequestData, paymentAccount,
                        tradeDetail.getOrderId());
            } else {
                refundResult = refundReserve(lineRefundRequestData, paymentAccount,
                        tradeDetail.getSystemOrderId());
            }
            tradeProcedureService
                    .setTradeDetailResponse(integrateMchId, requestHeader, refundResult, data);
            LineResponseData lineResponseData = new Gson().fromJson(refundResult, LineResponseData.class);

            ResponseGeneralHeader responseGeneralHeader = new LinePayUtil().getResponseHeader(
                    lineResponseData.getReturnCode(), lineResponseData.getReturnMessage(), requestHeader,
                    messageSource);
            responseGeneralHeader.setMchId(integrateMchId);
            IntegratedRefundResponseData integratedRefundResponseData = new LinePayUtil()
                    .getIntegratedRefundResponseData(lineResponseData, requestHeader, tradeDetail,
                            integratedRefundRequestData);
            integratedRefundResponseData.setPlatformRsp(refundResult);

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedRefundResponseData);
            generalResponseData = new Gson().toJson(responseGeneralBody);

            LOGGER.info("integrate response ready :" + generalResponseData);

        } else if (requestHeader.getServiceType().equals("SingleOrderQuery")) {
            LOGGER.info("[LinePay][SingleOrderQuery][data]" + data.getAsString());

            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

            if (tradeDetail.getServiceType().equals(SystemInstance.TYPE_MICROPAY)) {
                paymentAccount = paymentAccountService.getOneByDao(integrateMchId, 16);
            }

            String result = singleOrderQuery(paymentAccount, tradeDetail.getOrderId());
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);

            tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

            if (tradeDetail.getStatus().equals(SystemInstance.TRADE_FAIL)) {
                ResponseGeneralHeader responseGeneralHeader = new LinePayUtil()
                        .getResponseHeader("9998", "",
                                requestHeader, messageSource);
                responseGeneralHeader.setMchId(integrateMchId);
                IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();
                integratedSingleOrderQueryResponseData.setOrderStatus("2");
                integratedSingleOrderQueryResponseData.setStoreOrderNo(tradeDetail.getOrderId());
                integratedSingleOrderQueryResponseData.setPaidAt(tradeDetail.getCreateDate());
                integratedSingleOrderQueryResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
                integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());
                integratedSingleOrderQueryResponseData.setRefundedMsg(tradeDetail.getRefundStatus());
                ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
                responseGeneralBody.setHeader(responseGeneralHeader);
                responseGeneralBody.setData(integratedSingleOrderQueryResponseData);
                generalResponseData = new Gson().toJson(responseGeneralBody);
                LOGGER.info("integrate response ready :" + generalResponseData);
                return generalResponseData;

            }

            LOGGER.info("[LinePay][SingleOrderQuery][result].." + result);

            LineQueryResponseData lineQueryResponseData = new Gson()
                    .fromJson(result, LineQueryResponseData.class);

            ResponseGeneralHeader responseGeneralHeader = new LinePayUtil().getResponseHeader(
                    lineQueryResponseData.getReturnCode(), lineQueryResponseData.getReturnMessage(),
                    requestHeader,
                    messageSource);
            responseGeneralHeader.setMchId(integrateMchId);
            IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new LinePayUtil()
                    .getIntegratedSingleOrderQueryResponseData(lineQueryResponseData, tradeDetailService.getOne(tradeDetail.getOrderId()));

//            integratedSingleOrderQueryResponseData.setPlatformRsp(result);
            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedSingleOrderQueryResponseData);
            generalResponseData = new Gson().toJson(responseGeneralBody);

            LOGGER.info("integrate response ready :" + generalResponseData);

        } else if (requestHeader.getServiceType().equals("InApp")) {

            return getLinePayInApp(data, requestHeader);

        } else if (requestHeader.getServiceType().equals("Confirm")) {
            return getConfirmResult(data, requestHeader);
        }
        return generalResponseData;
    }

    private String getConfirmResult(JsonPrimitive data, RequestHeader requestHeader)
            throws Exception {
        PaymentAccount paymentAccount = paymentAccountService.getOne(requestHeader.getMerchantId(), 15);

        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
        JsonObject jsonObject = new Gson().fromJson(data.getAsString(), JsonObject.class);

        LineConfirmRequestData lineConfirmRequestData = new LineConfirmRequestData();
        lineConfirmRequestData.setCurrency("TWD");
        lineConfirmRequestData.setAmount((int) tradeDetail.getPayment());
        LineResponseData lineResponseData = null;
        LOGGER.info("[LinePay][confirm][id]" + paymentAccount.getAccount());
        try {
            if (!jsonObject.has("SysOrderNo") || jsonObject.get("SysOrderNo").getAsString() == null) {
                lineResponseData = new LineResponseData();
                lineResponseData.setReturnCode("9999");
                setLinePayResponse(lineResponseData, tradeDetail);
            } else {
                String confirmResult = confirm(lineConfirmRequestData, paymentAccount,
                        jsonObject.get("SysOrderNo").getAsString());
                LOGGER.info("[LinePay][confirm][Result]: " + confirmResult);
                lineResponseData = new Gson().fromJson(confirmResult, LineResponseData.class);
                setLinePayResponse(lineResponseData, tradeDetail);
            }
        } catch (Exception e) {
//            e.printStackTrace();
            LOGGER.info("[LinePay][confirm] exception: " + e.getMessage());
        }
        IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();

        // todo line in app confirm return
        ResponseGeneralHeader responseGeneralHeader = new LinePayUtil().getResponseHeader(
                lineResponseData.getReturnCode(), lineResponseData.getReturnMessage(), requestHeader,
                messageSource);
        if (lineResponseData.getReturnCode().equals("0000")) {
            integratedSingleOrderQueryResponseData.setOrderStatus(SystemInstance.TRADE_SUCCESS);
            integratedSingleOrderQueryResponseData.setStoreOrderNo(tradeDetail.getOrderId());
            integratedSingleOrderQueryResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
        } else {
            integratedSingleOrderQueryResponseData.setStoreOrderNo(tradeDetail.getOrderId());
        }
        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        responseGeneralBody.setHeader(responseGeneralHeader);
        responseGeneralBody.setData(integratedSingleOrderQueryResponseData);

        String generalResponseData = new Gson().toJson(responseGeneralBody);
        LOGGER.info("integrate response ready :" + generalResponseData);
        return generalResponseData;

    }

    private String getLinePayInApp(JsonPrimitive data, RequestHeader requestHeader) throws Exception {

        PaymentAccount paymentAccount = paymentAccountService.getOne(requestHeader.getMerchantId(), 15);

        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

        JsonObject jsonObject = new Gson().fromJson(data.getAsString(), JsonObject.class);

        LineReserveRequestData lineReserveRequestData = new Gson().fromJson(data.getAsString(),
                LineReserveRequestData.class);
        lineReserveRequestData.setProductImageUrl(API_URL + "intella-allpaypass/images.png");
        lineReserveRequestData.setCurrency("TWD");
        lineReserveRequestData.setConfirmUrl(jsonObject.get("Scheme").getAsString());
        lineReserveRequestData.setCancelUrl(jsonObject.get("Scheme").getAsString());
//        lineReserveRequestData.setOrderId(lineReserveRequestData);
        String result = reserve(lineReserveRequestData, paymentAccount);

        LineResponseData lineResponseData = new Gson().fromJson(result, LineResponseData.class);

        IntegratedInAppResponseData integratedInAppResponseData = new IntegratedInAppResponseData();

        ResponseGeneralHeader responseGeneralHeader = new LinePayUtil().getResponseHeader(
                lineResponseData.getReturnCode(), lineResponseData.getReturnMessage(), requestHeader,
                messageSource);
        integratedInAppResponseData.setPartnerId("");
        if (lineResponseData.getReturnCode().equals("0000")) {
            integratedInAppResponseData
                    .setTradeToken(lineResponseData.getInfo().getPaymentUrl().getApp());
        } else {
            integratedInAppResponseData.setTradeToken("");
        }
        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        responseGeneralBody.setHeader(responseGeneralHeader);
        responseGeneralBody.setData(integratedInAppResponseData);

        String generalResponseData = new Gson().toJson(responseGeneralBody);
        LOGGER.info("integrate response ready :" + generalResponseData);
        return generalResponseData;

    }

    public String reserve(LineReserveRequestData request, PaymentAccount paymentAccount)
            throws Exception {
        LOGGER.info("[Line][Reserve][request] : " + new ObjectMapper().writeValueAsString(request));
        LOGGER.info("[Line][Reserve][paymentAccount] : " + paymentAccount.getAccount() + ", "
                + paymentAccount.getHashKey());
        String result = linePost(LINEPAY_URL + "request",
                new ObjectMapper().writeValueAsString(request),
                paymentAccount, null);
        LOGGER.info("[Line][Reserve][response] : " + result);
        return result;
    }

    public String confirm(LineConfirmRequestData request, PaymentAccount paymentAccount,
                          String transactionId)
            throws Exception {
        LOGGER.info(
                "[Line][confirm]" + transactionId + " [request] : " + new ObjectMapper()
                        .writeValueAsString(request));

        String result = linePost(LINEPAY_URL + transactionId + "/confirm",
                new ObjectMapper().writeValueAsString(request), paymentAccount, null);
        LOGGER.info("[Line][confirm][response] : " + result);
        return result;
    }

    public String refundReserve(LineRefundRequestData request, PaymentAccount paymentAccount,
                                String transactionId)
            throws Exception {
        LOGGER.info(
                "[Line][refundReserved][request] : " + new ObjectMapper().writeValueAsString(request));

        String result = linePost(LINEPAY_URL + transactionId + "/refund",
                new ObjectMapper().writeValueAsString(request), paymentAccount, null);

        return result;
    }

    public String singleOrderQuery(PaymentAccount paymentAccount, String transactionId)
            throws Exception {
        LOGGER.info("[Line][singleOrderQuery][request] : " + transactionId);
        String result = lineGet(LINEPAY_URL + "?orderId=" + transactionId, paymentAccount);

        return result;
    }

    public String payment(LinePaymentRequestData request, PaymentAccount paymentAccount,
                          String device)
            throws Exception {
        LOGGER.info("[Line][micropay][request] : " + new ObjectMapper().writeValueAsString(request));

        String result = linePost(LINEPAY_URL + "oneTimeKeys/pay",
                new ObjectMapper().writeValueAsString(request),
                paymentAccount, device);
        return result;
    }

    public String refundPayment(LineRefundRequestData request, PaymentAccount paymentAccount,
                                String orderId)
            throws Exception {
        LOGGER
                .info("[Line][refundPayment][request] : " + new ObjectMapper().writeValueAsString(request));

        String result = linePost(LINEPAY_URL + "orders/" + orderId + "/refund",
                new ObjectMapper().writeValueAsString(request), paymentAccount, null);

        return result;
    }

    public LineReserveRequestData prepareLineReserveRequestData(QrcodeParameter qrcodeParameter,
                                                                Integer totalFee,
                                                                String orderId) {
        LineReserveRequestData lineReserveRequestData = new LineReserveRequestData();
        lineReserveRequestData.setProductName(qrcodeParameter.getBody());
        lineReserveRequestData.setProductImageUrl(API_URL + "intella-allpaypass/images.png");
        lineReserveRequestData.setAmount(totalFee);
        lineReserveRequestData.setCurrency("TWD");
        lineReserveRequestData
                .setConfirmUrl(API_URL + "allpaypass/api/linepay/confirm?orderId=" + orderId);
        lineReserveRequestData
                .setCancelUrl(API_URL + "allpaypass/api/linepay/cancel?orderId=" + orderId);
        lineReserveRequestData.setOrderId(orderId);

        return lineReserveRequestData;
    }

    private String linePost(String url, String data, PaymentAccount paymentAccount, String device)
            throws Exception {
        HttpURLConnection conn = (HttpURLConnection) (new URL(url)).openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("X-LINE-ChannelId", paymentAccount.getAccount());
        conn.setRequestProperty("X-LINE-ChannelSecret", paymentAccount.getHashKey());
        if (device != null) {
            conn.setRequestProperty("X-LINE-MerchantDeviceProfileId", device);
        }
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.getOutputStream().write(data.getBytes("UTF-8"));
        conn.getOutputStream().flush();
        conn.getOutputStream().close();
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        StringBuilder text = new StringBuilder();

        String line;
        while ((line = rd.readLine()) != null) {
            text.append(line);
            if (line.length() < 1 && line.charAt(0) == '\ufeff') {
                text.deleteCharAt(0);
            }
        }

        return text.toString().replaceAll("\\s", "");
    }

    public static String lineGet(String requestUrl, PaymentAccount paymentAccount) throws Exception {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }};
        SSLContext context = SSLContext.getInstance("TLS");
        context.init((KeyManager[]) null, trustAllCerts, (SecureRandom) null);
        URL url = new URL(requestUrl);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setSSLSocketFactory(context.getSocketFactory());
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("X-LINE-ChannelId", paymentAccount.getAccount());
        connection.setRequestProperty("X-LINE-ChannelSecret", paymentAccount.getHashKey());
        connection.setReadTimeout(10000);
        connection.setConnectTimeout(15000);
        connection.setDoInput(true);
        connection.setRequestMethod("GET");
        connection.setDoOutput(false);

        StringBuilder text = new StringBuilder();
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(connection.getInputStream(), "UTF-8"));

        String line;
        while ((line = reader.readLine()) != null) {
            text.append(line);
        }

        reader.close();
        return text.toString();
    }

    private void setLinePayResponse(LineResponseData lineResponseData, TradeDetail tradeDetail) {
        if (lineResponseData.getReturnCode().equals("0000")) {
            LOGGER.info("[LinePay][setLinePayResponse][Success]");
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetail.setSystemOrderId(lineResponseData.getInfo().getTransactionId().toString());
            appNotificationService
                    .notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
        } else {
            LOGGER.info("[LinePay][setLinePayResponse][Fail]");
            if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            }
//            tradeDetail.setSystemOrderId(lineResponseData.getInfo().getTransactionId().toString());
        }

        tradeDetailService.save(tradeDetail);

    }

}
