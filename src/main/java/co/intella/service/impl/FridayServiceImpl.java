package co.intella.service.impl;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.crypto.Cipher;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fet.mobilewallet.utils.CipherUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.sun.org.apache.xml.internal.security.utils.Base64;

import co.intella.crypto.KeyReader;
import co.intella.domain.friDay.FridayBasic;
import co.intella.domain.friDay.FridayContentData;
import co.intella.domain.friDay.FridayNotifyRequestData;
import co.intella.domain.friDay.FridayPayloadData;
import co.intella.domain.integration.IntegratedRefundRequestData;
import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import co.intella.service.AppNotificationService;
import co.intella.service.FridayService;
import co.intella.service.MerchantService;
import co.intella.service.PaymentAccountService;
import co.intella.service.TradeDetailService;
import co.intella.service.TradeProcedureService;
import co.intella.utility.FridayUtil;
import co.intella.utility.JsonUtil;
import co.intella.utility.SystemInstance;

/**
 * @author Alex
 */
@Service
public class FridayServiceImpl implements FridayService {

	private final Logger LOGGER = LoggerFactory.getLogger(FridayServiceImpl.class);

	@Resource
	private PaymentAccountService paymentAccountService;

	@Autowired
	private Environment env;

	@Autowired
	private MessageSource messageSource;

	@Resource
	private TradeProcedureService tradeProcedureService;

	@Resource
	private AppNotificationService appNotificationService;

	@Resource
	private MerchantService merchantService;

	@Resource
	private TradeDetailService tradeDetailService;

	@Value("${host.request.friday}")
	private String FRIDAY_URL;

	@Value("${api.url}")
	private String API_URL;

	HashMap<Integer, String> paymentMethodMap = new HashMap<Integer, String>() {
		{
			put(1, "PB");
			put(2, "CC");
			put(3, "HG");
			put(4, "PC");

		}
	};

	public Boolean updateNotify(FridayNotifyRequestData requestData) {
		TradeDetail tradeDetail = tradeDetailService.getOne(requestData.getOrderId());
		if (tradeDetail == null) {
			return false;
		}
		tradeDetail.setMethod("12300");
		if (requestData.getOrderStatus().equals("S")) {
			if(!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
				tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
				tradeDetail.setPlatformPaidDate(requestData.getOrderDateTime());
				tradeDetail.setType(getMethodType(requestData.getPayments().get(0).getPaymentMethod()));
				tradeDetail.setSystemOrderId(requestData.getTxOrderNo());
				tradeDetail.setPayment(Long.parseLong(requestData.getTotalAmt()));
				appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
			}

		} else {
			tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
		}
		Boolean response = tradeDetailService.save(tradeDetail) != null;
		return response;
	}

	public String RSADecrypt(byte[] encryptedData, PrivateKey privateKey) throws Exception {
		int MAX_DECRYPT_BLOCK = 128;

		Cipher cipher = Cipher.getInstance(privateKey.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		int inputLen = encryptedData.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		// separate decrypt
		while (inputLen - offSet > 0) {
			if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
				cache = cipher.doFinal(encryptedData, offSet, MAX_DECRYPT_BLOCK);
			} else {
				cache = cipher.doFinal(encryptedData, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_DECRYPT_BLOCK;
		}
		byte[] decryptedData = out.toByteArray();
		out.close();
		return new String(decryptedData);
	}

	public static String encryptByPublicKey(byte[] data, PublicKey publicKey) throws Exception {
		int MAX_ENCRYPT_BLOCK = 117;

		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		int inputLen = data.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		while (inputLen - offSet > 0) {
			if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
				cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
			} else {
				cache = cipher.doFinal(data, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_ENCRYPT_BLOCK;
		}
		byte[] encryptedData = out.toByteArray();
		out.close();
		return Base64.encode(encryptedData).replace("\n", "");
	}

	public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {
		String generalResponseData = SystemInstance.EMPTY_STRING;

		JsonObject DataValueJson = JsonUtil.convertJsonPrimitiveToJsonObject(data);

		String integrateMchId = requestHeader.getMerchantId();
		long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
		PaymentAccount paymentAccount = paymentAccountService.getOneByDao(integrateMchId, bankId);

		if (requestHeader.getServiceType().equals(SystemInstance.TYPE_MICROPAY)) {

		} else if (requestHeader.getServiceType().equals("Refund")) {
			LOGGER.info("[Friday][Refund][data]" + data.getAsString());

			PublicKey publicKey = KeyReader.loadPublicKeyFromString(paymentAccount.getHashKey());

			FridayBasic fridayBasic = new FridayBasic();
			FridayPayloadData fridayPayloadData = new FridayPayloadData();
			FridayContentData fridayContentData = new FridayContentData();

			fridayContentData.setChannelTerminalId(DataValueJson.get("DeviceInfo").getAsString());
			fridayContentData.setStoreNo(paymentAccount.getHashIV());
			fridayContentData.setOperatorNo("0000001");
			fridayPayloadData.setContent(fridayContentData);
			fridayPayloadData.setApi("T09");

			IntegratedRefundRequestData integratedRefundRequestData = new Gson().fromJson(data.getAsString(),
					IntegratedRefundRequestData.class);

//            TradeDetail tradeDetail = tradeDetailService.getOneByDao(integratedRefundRequestData.getStoreOrderNo());

			String refundResult;
			TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
			fridayPayloadData.getContent().setTxOrderNo(tradeDetail.getSystemOrderId());

			fridayBasic.setPayloadData(new Gson().toJson(fridayPayloadData));
			fridayBasic.setPayloadData(encryptPayloadData2(fridayBasic, publicKey));
			fridayBasic.setServiceId(paymentAccount.getAccount());
			refundResult = CipherUtil.decrypt(refund(fridayBasic), "RSA_PUBLIC", paymentAccount.getHashKey());

//            refundResult ="{\"code\":\"E0000\",\"message\":\"Success\",\"responseData\":\"{\\\"channelTermindId\\\":\\\"skb0001\\\",\\\"refundTXID\\\":\\\"00003\\\",\\\"txOrderNo\\\":\\\"SAC000000007510\\\"}\"}";
			LOGGER.info("[Friday][Refund][Response]" + refundResult);

			JsonObject headerJson = new Gson().fromJson(refundResult, JsonObject.class);

			updateRefund(tradeDetail, headerJson, data);

//            tradeProcedureService.setTradeDetailResponse(integrateMchId,requestHeader,refundResult,data);

			ResponseGeneralHeader responseGeneralHeader = new FridayUtil().getResponseHeader(
					headerJson.get("code").getAsString(), integrateMchId, requestHeader, messageSource);
			IntegratedRefundResponseData integratedRefundResponseData = new FridayUtil()
					.getIntegratedRefundResponseData(headerJson, requestHeader, tradeDetail,
							integratedRefundRequestData);
//            integratedRefundResponseData.setPlatformRsp(refundResult);

			ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
			responseGeneralBody.setHeader(responseGeneralHeader);
			responseGeneralBody.setData(integratedRefundResponseData);
			generalResponseData = new Gson().toJson(responseGeneralBody);

			LOGGER.info("integrate response ready :" + generalResponseData);
		}

		return generalResponseData;
	}

	private void updateRefund(TradeDetail tradeDetail, JsonObject headerJson, JsonPrimitive data) {

		String code = headerJson.get("code").getAsString();
		if (code.equals("E0000")) {
			JsonObject dataJson = JsonUtil
					.convertJsonPrimitiveToJsonObject(headerJson.getAsJsonPrimitive("responseData"));
			tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
			tradeDetail.setSystemRefundId(dataJson.get("refundTXID").getAsString());
			tradeProcedureService.updateRefundDeatil(tradeDetail,
					new Gson().fromJson(data.getAsString(), JsonObject.class), SystemInstance.TRADE_REFUND_SUCCESS);
		} else if (code.equals("E3055") || code.equals("E3003")) {
			tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
		} else {
			tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
			tradeProcedureService.updateRefundDeatil(tradeDetail,
					new Gson().fromJson(data.getAsString(), JsonObject.class), SystemInstance.TRADE_REFUND_FAIL);
		}

		tradeDetailService.save(tradeDetail);
	}

	private String encryptPayloadData2(FridayBasic fridayBasic, PublicKey publicKey) {

		String plainText = new Gson().toJson(fridayBasic.getPayloadData());

		try {
			return encryptByPublicKey(plainText.getBytes(), publicKey);
//            return CipherUtil.encrypt(plainText, CipherUtil.CodeAlgorithm.RSA_PUBLIC.getCode(), publicKey);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String refund(FridayBasic fridayBasic) {

		try {// HttpRequestUtil.post
			return sslNoVerify(FRIDAY_URL, new ObjectMapper().writeValueAsString(fridayBasic));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	private int getMethodType(String paymentMethod) {
		for (Map.Entry<Integer, String> entry : paymentMethodMap.entrySet()) {
			if (entry.getValue().equals(paymentMethod)) {
				return entry.getKey();
			}
		}
		return 0;
	}

	public String sslNoVerify(String FRIDAY_URL, String data)
			throws IOException, KeyManagementException, NoSuchAlgorithmException {
		HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
		SSLContext sc = SSLContext.getInstance("TLS");
		sc.init(null, trustAllCerts, new SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		URL url = new URL(FRIDAY_URL);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
		conn.setConnectTimeout(130000);
		conn.setReadTimeout(130000);
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.getOutputStream().write(data.getBytes("UTF-8"));
		conn.getOutputStream().flush();
		conn.getOutputStream().close();
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
		StringBuilder text = new StringBuilder();

		String line;
		while ((line = rd.readLine()) != null) {
			text.append(line);
			if (line.length() < 1 && line.charAt(0) == '\ufeff') {
				text.deleteCharAt(0);
			}
		}
		return text.toString();
	}

	static TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
//        @Override
		public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			// TODO Auto-generated method stub
		}

//        @Override
		public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			// TODO Auto-generated method stub
		}

//        @Override
		public X509Certificate[] getAcceptedIssuers() {
			// TODO Auto-generated method stub
			return null;
		}
	} };

	public class NullHostNameVerifier implements HostnameVerifier {
		/*
		 * (non-Javadoc)
		 *
		 * @see javax.net.ssl.HostnameVerifier#verify(java.lang.String,
		 * javax.net.ssl.SSLSession)
		 */
//        @Override
		public boolean verify(String arg0, SSLSession arg1) {
			// TODO Auto-generated method stub
			return true;
		}
	}

}
