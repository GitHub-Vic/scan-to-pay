package co.intella.service.impl;

import co.intella.domain.acertaxi.*;
import co.intella.domain.contratStore.*;
import co.intella.domain.easycard.*;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.exception.*;
import co.intella.model.*;
import co.intella.net.HttpRequestUtil;
import co.intella.repository.MerchantRepository;
import co.intella.service.*;
import co.intella.utility.EzcUtil;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Strings;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * @author Miles
 */
@Service
public class EzCardServiceImpl implements EzCardService {

    private final Logger LOGGER = LoggerFactory.getLogger(EzCardServiceImpl.class);

    @Value("${host.request.ezc}")
    private String HOST;

    @Value("${host.request.ezcTaxi}")
    private String taxiServiceHost;

    @Value("${host.request.ezcPark}")
    private String parkServiceHost;

    @Value("${host.welfarepoint.query}")
    private String WELFAREPOINT_QUERY_URL;

    @Value("${host.taxi.record}")
    private String TAXI_RECORD_INSERT_API_URL;

    @Resource
    private MerchantService merchantService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private ReserveDetailService reserveDetailService;

    @Resource
    private OperationHistoryService operationHistoryService;

    @Resource
    private RefundDetailService refundDetailService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private OrderService orderService;

    @Resource
    private MailService mailService;

    @Resource
    private ReserveCancelService reserveCancelService;

    @Resource

    private INT_TB_CheckoutCtrlService INT_TB_CheckoutCtrlService;

    @Resource
    private INT_TB_CheckoutLogService INT_TB_CheckoutLogService;

    @Resource
    private MerchantRepository merchantRepository;

    @Resource
    private LookupCodeService lookupCodeService;

    private Locale locale = new Locale("zh_TW");
    @Autowired
    private MessageSource messageSource;

    @Resource(type = co.intella.service.impl.EZCardTaxiPoint.class)
    private TaxiPoint taxiPoint;

    // 20190418 Edit
    @Resource
    private CheckoutService checkoutService;

    @Resource
    private TradeDetailOtherInfoService tradeDetailOtherInfoService;

    @Resource
    private TaxiTransactionDetailService taxiTransactionDetailService;

    public String doRequest(EzCardBasicRequest ezCardBasicRequest, String aesKey) {

        LOGGER.info("[EZC] doRequest(). " + aesKey);
        ezCardBasicRequest.setAesKey(aesKey);
        String requestUrl = HOST + new Gson().toJson(ezCardBasicRequest);

        LOGGER.info("[EZC] doRequest(). " + ezCardBasicRequest.getServiceType() + " -> " + requestUrl);

        try {

            String response;
            if (requestUrl.startsWith("https")) {
                response = HttpRequestUtil.httpsGet(requestUrl);
            } else {
                response = HttpRequestUtil.httpGet(requestUrl);
            }
            LOGGER.info("[EZC][RESPONSE] doRequest(). " + response);
            return response;
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
            EzCardBasicResponse errorResponse = new EzCardBasicResponse();
            errorResponse.setErrorCode("909995");
            errorResponse.setTxnResult("Fail");
            return new Gson().toJson(errorResponse);
        }
    }

    public String taxiRequest(EzCardBasicRequest ezCardBasicRequest, String aesKey) {

        LOGGER.info("[EZC] doRequest(). " + aesKey);
        ezCardBasicRequest.setAesKey(aesKey);
        String requestUrl = null;
        try {
            requestUrl = taxiServiceHost + URLEncoder.encode(new Gson().toJson(ezCardBasicRequest), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        LOGGER.info("[EZC] doRequest(). " + ezCardBasicRequest.getServiceType() + " -> " + requestUrl);

        try {

            String response = HttpUtil.doGet(requestUrl);

            LOGGER.info("[EZC][RESPONSE] doRequest(). " + response);
            return response;
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
            EzCardBasicResponse errorResponse = new EzCardBasicResponse();
            errorResponse.setErrorCode("909995");
            errorResponse.setTxnResult("Fail");
            return new Gson().toJson(errorResponse);
        }
    }

    public String parkRequest(EzCardBasicRequest ezCardBasicRequest, String aesKey) {

        LOGGER.info("[EZC] doRequest(). " + aesKey);
        ezCardBasicRequest.setAesKey(aesKey);
        String requestUrl = parkServiceHost + new Gson().toJson(ezCardBasicRequest);

        LOGGER.info("[EZC] doRequest(). " + ezCardBasicRequest.getServiceType() + " -> " + requestUrl);

        try {

            String response;
            if (requestUrl.startsWith("https")) {
                response = HttpRequestUtil.httpsGet(requestUrl);
            } else {
                response = HttpRequestUtil.httpGet(requestUrl);
            }
            LOGGER.info("[EZC][RESPONSE] doRequest(). " + response);
            return response;
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
            EzCardBasicResponse errorResponse = new EzCardBasicResponse();
            errorResponse.setErrorCode("909995");
            errorResponse.setTxnResult("Fail");
            return new Gson().toJson(errorResponse);
        }
    }

    public String ezRequest(RequestHeader requestHeader, JsonObject requestData, Merchant merchant)
            throws DeviceNotFoundException, InvalidDataFormatException, OrderNotFoundException,
            OrderAlreadyRefundedException, ServiceTypeNotSupportException, RefundKeyException, UnsupportedEncodingException, OtherAPIException {

        String deviceId = requestData.get("DeviceId").getAsString();
        Device device = deviceService.getOne(merchant, deviceId);

        if (Objects.isNull(device) && !deviceId.equals("00000000")) {
            LOGGER.error("[REQUEST][EZC] device not found. " + deviceId + ", " + merchant.getAccountId());
            throw new DeviceNotFoundException("device not found");
        } else if (!"AutoSettlement".equals(requestHeader.getServiceType()) && !deviceId.equals("00000000") && "1".equals(device.getBan())) {
            // 自動結帳 不驗證device權限
            LOGGER.error("[REQUEST][EZC] device not found. " + deviceId + ", " + merchant.getAccountId());
            throw new DeviceNotFoundException("Device Locked");
        }

        PaymentAccount paymentAccount = paymentAccountService.getOne(merchant.getAccountId(), 18);

        LOGGER.info("[REQUEST][EZC] " + merchant.getAccountId() + ", " + deviceId);
        //TODO  有新增serviceType 如果必須要有卡片的相關serviceType，請去TicketLogicServiceImpl 也增加到相對的 private static final String[]
        if ("AutoSettlement".equals(requestHeader.getServiceType())) {
            return doSettlement(device, paymentAccount, requestHeader, requestData.get("BatchNo").getAsString());
        } else if ("SignOn".equals(requestHeader.getServiceType())) {
            return doSignOn(device, paymentAccount, requestHeader);
        } else if ("TaxiSignOn".equals(requestHeader.getServiceType())) {
            throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);      //TODO  這個ServiceType為舊款APP，直接擋住不給登入
        } else if ("TaxiLogin".equals(requestHeader.getServiceType())) {
            return taxiSignOn(device, paymentAccount, requestHeader);
        } else if ("EDCASignOn".equals(requestHeader.getServiceType())) {
            return parkSignOn(device, paymentAccount, requestHeader);
        } else if ("Payment".equals(requestHeader.getServiceType())) {
            if (!requestData.has(SystemInstance.FIELD_NAME_STORE_ORDER_NO)) {
                throw new InvalidDataFormatException("No StoreOrderNo.");
            }
            return doPayment(requestHeader, requestData, device, paymentAccount, merchant);
        } else if ("Refund".equals(requestHeader.getServiceType())) {
            if (!requestData.has(SystemInstance.FIELD_NAME_STORE_ORDER_NO)
                    || !requestData.has(SystemInstance.FIELD_NAME_AMOUNT)) {
                LOGGER.error("[EZC] Refund Argument error. No StoreOrderNo or No RefundFee.");
                throw new InvalidDataFormatException("Refund Argument error. No StoreOrderNo or No RefundFee.");
            }
            return doRefund(requestHeader, requestData, device, paymentAccount, merchant);
        } else if ("EZCRefund".equals(requestHeader.getServiceType())) {
            return doRefundWithoutTradeDetail(requestHeader, requestData, device, paymentAccount, merchant);
        } else if ("Reserve".equals(requestHeader.getServiceType())) {
            if (!requestData.has(SystemInstance.FIELD_NAME_STORE_ORDER_NO)) {
                throw new InvalidDataFormatException("No StoreOrderNo.");
            }
            return doReserve(requestHeader, requestData, device, paymentAccount);
        } else if ("IdQuery".equals(requestHeader.getServiceType())) {

            LOGGER.info("[REQUEST][EZC] IdQuery : " + device.getOwnerDeviceId() + ", " + paymentAccount.getAccount());

            String userId = "-1";
            String date = getDate();
            String time = getTime();

            EzCardIdQuery ezCardIdQuery = new EzCardIdQuery(device, paymentAccount);

            EzCardIdQueryResponse response = new Gson().fromJson(doRequest(ezCardIdQuery, device.getAesKey()),
                    EzCardIdQueryResponse.class);
            if (response.getTxnResult().equals("Success")) {
                userId = response.getCardNumber();
            }

            if (StringUtils.isNotBlank(response.getNewAESKey())) {
                device.setAesKey(response.getNewAESKey());

                deviceService.save(device);
            }

            writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                    new Gson().toJson(ezCardIdQuery), userId + "|" + response.getTxnResult());

            return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
        } else if ("BalanceQuery".equals(requestHeader.getServiceType())) {

            return getBalanceQuery(device, paymentAccount, requestHeader);
        } else if ("Settlement".equals(requestHeader.getServiceType())) {
//            PaymentAccount paymentAccount = paymentAccountService.getOne(merchant.getAccountId(), 18);
//            return doSettlement(device, merchant, paymentAccount, requestHeader, null);
            String accountId = merchant.getAccountId();
            return checkoutService.checkout(accountId, deviceId, "", requestHeader, null, "");
        } else if ("SettlementForTaxi".equals(requestHeader.getServiceType())) {
            // PaymentAccount paymentAccount =
            // paymentAccountService.getOne(merchant.getAccountId(), 18);
            // return taxiCheckout(device, merchant, paymentAccount,
            // requestHeader, null);
            // 20190418 Edit
            String accountId = merchant.getAccountId();

            return checkoutService.checkout(accountId, deviceId, "", requestHeader, null, "");

        } else if ("EZCardDataInQueryForTaxi".equals(requestHeader.getServiceType())) {

            return taxiCardQuery(device, paymentAccount, requestHeader, requestData);
        } else if ("PaymentForTaxi".equals(requestHeader.getServiceType())) {

            return taxiPayment(requestHeader, requestData, device, paymentAccount, merchant);
        } else if ("SettlementPatch".equals(requestHeader.getServiceType())) {

            return doSettlement(device, merchant, paymentAccount, requestHeader,
                    requestData.get("BatchNo").getAsString());
        } else if ("ParkCheckout".equals(requestHeader.getServiceType())) {
            String accountId = merchant.getAccountId();
            return checkoutService.checkout(accountId, deviceId, "", requestHeader, null, "");
//            return parkCheckout(device, merchant, paymentAccount, requestHeader, null);
        } else if ("LogQuery".equals(requestHeader.getServiceType())) {
            return null;
        } else if ("DeviceQuery".equals(requestHeader.getServiceType())) {
            return getDeviceQuery(merchant, requestHeader);
        } else if ("CheckDevice".equals(requestHeader.getServiceType())) {
            LOGGER.info("[REQUEST][EZC] list device by merchantId : " + merchant.getAccountId());
            List<DeviceVo> deviceList = deviceService.listAllByMerchantAndType(merchant.getAccountId(), "taxi");

            AcerTaxiCheckDeviceResponse response = new AcerTaxiCheckDeviceResponse();

            // List<PaymentAccount> paymentAccountList =
            // paymentAccountService.listByBankId(Long.valueOf("18"));

            if (IsNullOrEmpty(deviceList)) {
                Device newDevice = deviceService.getOne(requestData.get("LicensePlate").getAsString());

                if ((StringUtils.isNotBlank(newDevice.getOwnerDeviceId())) && (!newDevice.getBan().equals("1"))) {
                    // if (paymentAccountService.getOne(merchant.getAccountId(),
                    // Long.valueOf("18")).getAccount().equals(newDevice.getTerminalid())){

                    newDevice.setOwner(merchant);
                    deviceService.save(newDevice);

                    response.setNewDeviceID(newDevice.getOwnerDeviceId());
                    response.setAreaCode(newDevice.getLocation());
                    response.setIpassArea(newDevice.getIpasslocation());
                    response.setLicensePlate(newDevice.getLicensePlate());
                    return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
                } else {
                    EzCardErrorResponse errorResponse = new EzCardErrorResponse();
                    errorResponse.setErrorCode("7106");
                    errorResponse.setTxnResult("Fail");
                    errorResponse.setErrorMessage("Check Device ERROR");
                    errorResponse.setMerchantId(merchant.getAccountId());
                    errorResponse.setServiceType(requestHeader.getServiceType());
                    errorResponse.setDeviceId("ERROR");
                    return getErrorResponse(requestHeader, errorResponse, "Check Device ERROR");
                }
            } else {
                Device newDevice = deviceService.getOne(requestData.get("LicensePlate").getAsString());
                Device oldDevice = deviceService.getOne(merchant, deviceList.get(0).getOwnerDeviceId());
                System.out.println("newDevice" + newDevice.getOwnerDeviceId());
                // String deviceAccount =
                // paymentAccountService.getOne(newDevice.getOwner().getAccountId(),
                // Long.valueOf("18")).getAccount();

                if ((StringUtils.isNotBlank(newDevice.getOwnerDeviceId())) && (!newDevice.getBan().equals("1"))) {
                    // if (paymentAccountService.getOne(merchant.getAccountId(),
                    // Long.valueOf("18")).getAccount().equals(newDevice.getTerminalid())){
                    if (!(newDevice.getOwnerDeviceId().equals(oldDevice.getOwnerDeviceId()))) {
                        newDevice.setOwner(merchant);

                        String parentMerchantID = merchantService.getOneByLoginId(merchant.getAccountId())
                                .getParentMerchantAccount();
                        System.out.println("LOGINID!!!!!" + parentMerchantID);
                        oldDevice.setOwner(merchantService.getOne(parentMerchantID));

                        deviceService.save(oldDevice);
                        deviceService.save(newDevice);

                        response.setNewDeviceID(newDevice.getOwnerDeviceId());
                        response.setAreaCode(newDevice.getLocation());
                        response.setIpassArea(newDevice.getIpasslocation());
                        response.setLicensePlate(newDevice.getLicensePlate());
                    } else {
                        response.setNewDeviceID(newDevice.getOwnerDeviceId());
                        response.setAreaCode(newDevice.getLocation());
                        response.setIpassArea(newDevice.getIpasslocation());
                        response.setLicensePlate(newDevice.getLicensePlate());
                    }

                    // }

                    // paymentAccount.setAccount(deviceAccount);
                    // paymentAccountService.save(paymentAccount);

                    // response.setNewDeviceID(newDevice.getOwnerDeviceId());

                    return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
                } else {
                    EzCardErrorResponse errorResponse = new EzCardErrorResponse();
                    errorResponse.setErrorCode("7106");
                    errorResponse.setTxnResult("Fail");
                    errorResponse.setErrorMessage("Check Device ERROR");
                    errorResponse.setMerchantId(merchant.getAccountId());
                    errorResponse.setServiceType(requestHeader.getServiceType());
                    errorResponse.setDeviceId("ERROR");
                    return getErrorResponse(requestHeader, errorResponse, "Check Device ERROR");
                }

            }

        } else if ("EZCardEDCAPayment".equals(requestHeader.getServiceType())) {
            return EDCAPayment(requestHeader, requestData, device, paymentAccount, merchant);
        } else if ("EZCardEDCADataInQuery".equals(requestHeader.getServiceType())) {
            return EDCACardQuery(device, paymentAccount, requestHeader, requestData);
        } else if ("PrinterPaperStates".equals(requestHeader.getServiceType())) {
            return checkPrinter(requestHeader, device, paymentAccount);
        } else if ("OrderQuery".equals(requestHeader.getServiceType())) {

            String userId = getUserId(paymentAccount, device);

            if (Strings.isNullOrEmpty(userId) || userId.equals("-1")) {
                LOGGER.error("[REQUEST][EZC] Card id not found. " + device.getOwnerDeviceId() + ", "
                        + merchant.getAccountId());
                EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Card id not found.",
                        "0462xx");
                return getErrorResponse(requestHeader, errorResponse, "Can not read card info.");
            }

            return getOrderQuery(userId, merchant, requestHeader);
        } else if ("ReserveOrderQuery".equals(requestHeader.getServiceType())) {
            return getReserveOrderQuery(deviceId, merchant, requestHeader);
        } else if ("EZCOrderQuery".equals(requestHeader.getServiceType())) {

            EzCardTradeQuery query = new EzCardTradeQuery();
            query.setTerminalId(paymentAccount.getAccount());
            query.setDeviceId(device.getOwnerDeviceId());

            String time = getTime();
            String date = getDate();

            query.setTerminalTXNNumber(time);
            query.setHostSerialNumber(getTime()); // 因之前有嚴重延遲，所以改為送出前當下
            query.setTime(time);
            query.setDate(date);
            query.setBeforeOrAfter("0");

            EzCardTradeQueryResponse response = new Gson().fromJson(doRequest(query, device.getAesKey()),
                    EzCardTradeQueryResponse.class);

            if (StringUtils.isNotBlank(response.getNewAESKey())) {
                device.setAesKey(response.getNewAESKey());
                if (StringUtils.isNotBlank(response.getHostSerialNumber())) {
                    device.setHostSerialNumber(response.getHostSerialNumber());
                }
                deviceService.save(device);
            }

            writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                    "EZCOrderQuery: " + merchant.getAccountId() + "|" + deviceId, "N/A");

            return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
        } else if ("CancelOrderQuery".equals(requestHeader.getServiceType())) {

            String userId = getUserId(paymentAccount, device);

            if (Strings.isNullOrEmpty(userId) || userId.equals("-1")) {
                LOGGER.error("[REQUEST][EZC] Card id not found. " + device.getOwnerDeviceId() + ", "
                        + merchant.getAccountId());
                EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Card id not found.",
                        "0462xx");
                return getGeneralResponse(requestHeader, errorResponse, SystemInstance.STATUS_SUCCESS);
            }

            List<TradeDetailVo> tradeDetailVos = tradeDetailService.listTopOneByUserId(requestHeader.getMerchantId(),
                    deviceId, userId);
            List<ReserveDetail> reserveDetails = reserveDetailService.listTopOneByUserId(requestHeader.getMerchantId(),
                    deviceId, userId);

            LOGGER.info("newest trade : " + new Gson().toJson(tradeDetailVos));
            LOGGER.info("newest reserve : " + new Gson().toJson(reserveDetails));

            ReserveCancelInfo reserveCancelInfo = getReserveCancelInfo(tradeDetailVos, reserveDetails, requestHeader,
                    device);
            if (reserveCancelInfo == null) {
                LOGGER.error("[REQUEST][EZC] Tx not found. " + device.getOwnerDeviceId() + ", "
                        + requestHeader.getMerchantId());
                EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Tx not found.", "020076");
                return getGeneralResponse(requestHeader, errorResponse, SystemInstance.STATUS_SUCCESS);
            }

            if (reserveCancelInfo.getReserveDetail() == null && reserveCancelInfo.getTradeDetailVo() == null) {
                LOGGER.error("[REQUEST][EZC] Tx not found. " + device.getOwnerDeviceId() + ", "
                        + requestHeader.getMerchantId());
                EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Tx not found.", "020076");
                return getGeneralResponse(requestHeader, errorResponse, SystemInstance.STATUS_SUCCESS);
            }

            EzCardCancelOrderQueryResponse response = new EzCardCancelOrderQueryResponse();
            response.setEzCardId(userId);
            response.setUserId(userId);
            response.setErrorCode("00000");

            if (reserveCancelInfo.getAction().equals("CANCEL_RESERVE")) {
                response.setActionType("CANCEL_RESERVE");
                response.setAmount(String.valueOf(reserveCancelInfo.getReserveDetail().getAmount()));
                response.setCreateDate(reserveCancelInfo.getReserveDetail().getCreateTime());
                response.setTradeOrderId(reserveCancelInfo.getReserveDetail().getReserveOrderId());
                response.setTxnDeviceId(reserveCancelInfo.getTxnDeviceId());
                response.setOrderStatus(reserveCancelInfo.getReserveDetail().getStatus());

                String isRefund;
                if (StringUtils.isNotBlank(reserveCancelInfo.getReserveDetail().getCancelDate())) {
                    isRefund = reserveCancelInfo.getReserveDetail().getCancelStatus().equals("0") ? "Y" : "N";
                } else {
                    isRefund = "N";
                }

                response.setRefundStatus(isRefund);
            } else if (reserveCancelInfo.getAction().equals("REFUND_TRADE")) {
                response.setActionType("REFUND_TRADE");
                response.setCreateDate(reserveCancelInfo.getTradeDetailVo().getCreateDate());
                response.setTradeOrderId(reserveCancelInfo.getTradeDetailVo().getOrderId());
                response.setTxnDeviceId(reserveCancelInfo.getTxnDeviceId());
                response.setAmount(String.valueOf(reserveCancelInfo.getTradeDetailVo().getPayment()));
                response.setOrderStatus(reserveCancelInfo.getTradeDetailVo().getStatus());

                String isRefund;
                if (StringUtils.isNotBlank(reserveCancelInfo.getTradeDetailVo().getRefundStatus())) {
                    isRefund = reserveCancelInfo.getTradeDetailVo().getRefundStatus().equals("Refund success") ? "Y"
                            : "N";
                } else {
                    isRefund = "N";
                }

                response.setRefundStatus(isRefund);
            }

            LOGGER.info("[EZC][CancelOrderQuery] " + new Gson().toJson(response));
            writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                    "CancelOrderQuery: " + merchant.getAccountId() + "|" + deviceId, "N/A");

            return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);

        } else if ("Cancel".equals(requestHeader.getServiceType()) || "TradeChargeCancel".equals(requestHeader.getServiceType())) {

            String time = getTime();
            String date = getDate();

            String batchNo = device.getBatchNumber().toString();
            // System.out.println("BATCHNO!!!!!!"+batchNo);
            if (StringUtils.isNotBlank(batchNo)) {
                batchNo = getBatchNumber(date, String.format("%02d", device.getBatchNumber()));
            }
            String orderId = requestData.has(SystemInstance.FIELD_NAME_STORE_ORDER_NO) ? requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString() : "";

            String actionType = requestData.get("ActionType").getAsString();
            String amount = "0";

            ReserveDetail reserveDetail = null;
            TradeDetail tradeDetail = null;
            ReserveCancelInfo reserveCancelInfo = new ReserveCancelInfo();

            String userId = requestData.has("UserId") ? requestData.get("UserId").getAsString() : getUserId(paymentAccount, device);
            long isRetry = requestData.get("Retry").getAsLong();
            if (isRetry >= 4) {
                LOGGER.error(
                        "[REQUEST][EZC] STOP RETRY. " + device.getOwnerDeviceId() + ", " + merchant.getAccountId());
                EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Already retry 3 times.",
                        "900003");
                return getErrorResponse(requestHeader, errorResponse, "Already retry 3 times.");
            }

            if (actionType.equals("CANCEL_RESERVE")) {
                LOGGER.info("CANCEL_RESERVE : " + orderId);
                List<ReserveDetail> listTopOneByUserId = reserveDetailService.listTopOneByUserId(merchant.getAccountId(), device.getOwnerDeviceId(), userId);
                reserveDetail = CollectionUtils.isNotEmpty(listTopOneByUserId) ? listTopOneByUserId.get(0) : null;


                if (Strings.isNullOrEmpty(userId) || userId.equals("-1")) {
                    LOGGER.error("[REQUEST][EZC] Card id not found. " + device.getOwnerDeviceId() + ", "
                            + merchant.getAccountId());
                    EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Card id not found.",
                            "0462xx");
                    return getErrorResponse(requestHeader, errorResponse, "Can not read card info.");
                }
                String oldDeviceId = reserveDetail.getDeviceId();
                Device oldDevice = deviceService.getOne(merchant, oldDeviceId);

                reserveCancelInfo.setDongleDeviceId(oldDevice.getEzcDongleId());
                reserveCancelInfo.setRrn(reserveDetail.getRrn());
                reserveCancelInfo.setTxnDate(reserveDetail.getEzcTxnDate());
                amount = String.valueOf(reserveDetail.getAmount());

            } else if (actionType.equals("REFUND_TRADE")) {
                LOGGER.info("REFUND_TRADE : " + orderId);
                tradeDetail = tradeDetailService.getOne(orderId);
                if (isRetry > 0 && isRetry < 4) {
                    userId = tradeDetail.getUserId();
                } else {
                    userId = getUserId(paymentAccount, device);
                }
                if (Strings.isNullOrEmpty(userId) || userId.equals("-1")) {
                    LOGGER.error("[REQUEST][EZC] Card id not found. " + device.getOwnerDeviceId() + ", "
                            + merchant.getAccountId());
                    EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Card id not found.",
                            "0462xx");
                    return getErrorResponse(requestHeader, errorResponse, "Can not read card info.");
                }
                Device oldDevice = tradeDetail.getDeviceRandomId();

                reserveCancelInfo.setDongleDeviceId(oldDevice.getEzcDongleId());
                reserveCancelInfo.setRrn(tradeDetail.getRrn());
                reserveCancelInfo.setTxnDate(tradeDetail.getEzcTxnDate());
                amount = String.valueOf(tradeDetail.getPayment());
            } else {
                LOGGER.error("[REQUEST][EZC][CANCEL] Argument error. No action type.");

            }

            if (reserveDetail == null && tradeDetail == null) {
                LOGGER.error("[REQUEST][EZC] Tx not found. " + device.getOwnerDeviceId() + ", "
                        + requestHeader.getMerchantId());
                EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Tx not found.", "020076");
                return getErrorResponse(requestHeader, errorResponse, "Order not found.");
            }

            if (amount.equals("0")) {
                LOGGER.error("[REQUEST][EZC] Tx Amount is 0. " + device.getOwnerDeviceId() + ", "
                        + requestHeader.getMerchantId());
                EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Tx Amount is 0.",
                        "908002");
                return getErrorResponse(requestHeader, errorResponse, "Order not found.");
            }

            EzCardCancel query = new EzCardCancel();
            query.setTerminalId(paymentAccount.getAccount());
            query.setDeviceId(device.getOwnerDeviceId());
            query.setTerminalTXNNumber(isRetry > 0 ? requestData.get("TerminalTXNNumber").getAsString() : time);
            query.setHostSerialNumber(getTime()); // 因之前有嚴重延遲，所以改為送出前當下
            query.setTime(time);
            query.setDate(date);
            query.setRetry(requestData.get("Retry").getAsString());
            query.setBatchNumber(batchNo);
            query.setOriginalDongleDeviceID(reserveCancelInfo.dongleDeviceId);
            query.setOriginalRRN(reserveCancelInfo.rrn);
            query.setOriginalTXNDate(reserveCancelInfo.txnDate.substring(0, 8));
            query.setAmount(amount);
            query.setSameCard(isRetry > 0 ? "1" : "0");

            EzCardCancelResponse response = new Gson().fromJson(doRequest(query, device.getAesKey()),
                    EzCardCancelResponse.class);

            if (StringUtils.isNotBlank(response.getNewAESKey())) {
                device.setAesKey(response.getNewAESKey());
                if (StringUtils.isNotBlank(response.getHostSerialNumber())) {
                    device.setHostSerialNumber(response.getHostSerialNumber());
                }
                deviceService.save(device);
            }

            // set refundKey for retry
            if (response.getErrorCode().equals("000125") || isRetry > 0) {
//            if (!"000000".equals(response.getErrorCode())) {
                query.setRefundKey(requestData.get("RefundKey").getAsString());
                response.setActionType(actionType);
                response.setOrderId(orderId);
            }
            response.setRequest(query);

            writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                    "Cancel(NOT DO): " + merchant.getAccountId() + "|" + deviceId, "N/A");

            if (response.getTxnResult().equals("Success")) {
                if (actionType.equals("CANCEL_RESERVE") && reserveDetail != null) {
                    LOGGER.info("[EZC][CANCEL_RESERVE]");

                    response.setErrorCode("00000");
                    response.getRequest().setServiceType("CANCEL_RESERVE");

                    ReserveCancel reserveCancel = new ReserveCancel();
                    reserveCancel.setAccountId(requestHeader.getMerchantId());
                    reserveCancel.setBatchNo(batchNo);
                    reserveCancel.setReserveDetail(reserveDetail);
                    reserveCancel.setCashier(requestHeader.getMerchantId());
                    reserveCancel.setAmount(Long.valueOf(amount));
                    reserveCancel.setDeviceId(deviceId);
                    reserveCancel.setMethod(requestHeader.getMethod());
                    reserveCancel.setUserId(userId);
                    reserveCancel.setOrderId(reserveDetail.getReserveOrderId());
                    reserveCancel.setRrn(response.getRrNumber());
                    reserveCancel.setStatus("0");
                    // ReserveCancel.setSysTime(time);
                    reserveCancel.setEzcTxnDate(date + time);
                    reserveCancel.setDeviceRandomId(device);
                    reserveCancel.setCreateTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
                    ReserveCancel entity = reserveCancelService.save(reserveCancel);

                    if (entity == null) {
                        LOGGER.error("[EZC][CANCEL_RESERVE] save reserve cancel failed.");
                    }

                    printReceipt(response, device, paymentAccount);
                }

                if (actionType.equals("REFUND_TRADE") && tradeDetail != null) {
                    LOGGER.info("[EZC][REFUND_TRADE]");
                    // notice : this trade should not have batch no, because
                    // settlement will be false.
                    response.setErrorCode("00000");
                    saveOrUpdateTransactionDetails(tradeDetail, merchant, response, device, batchNo,
                            Long.valueOf(amount), isRetry, time, true);
                    printReceipt(response, device, paymentAccount);
                }
            }

            return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
        } else if (SystemInstance.ServiceType.TICKET_LOGIC.equals(requestHeader.getServiceType())) {
            return ticketLogic(requestHeader, requestData, device, paymentAccount, merchant);
        } else {
            throw new ServiceTypeNotSupportException("ServiceType Not Support.");
        }
    }

    public Boolean bootDevice(String deviceId, String terminalId) {

        String deviceExist = deviceService.deviceExist(deviceId);
        String paymentAccountExist = paymentAccountService.paymentAccountExist(terminalId);

        return deviceExist.equals("true") && paymentAccountExist.equals("true");
    }

    public static <T> boolean IsNullOrEmpty(Collection<T> list) {
        return list == null || list.isEmpty();
    }

    private String doPayment(RequestHeader requestHeader, JsonObject requestData, Device device,
                             PaymentAccount paymentAccount, Merchant merchant) {

        String date = getDate();
        String time = getTime();

        long isRetry = requestData.get("Retry").getAsLong();

        String orderId = requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString();
        String batchNo = device.getBatchNumber().toString();
        // System.out.println("BATCHNO!!!!!!"+batchNo);
        if (StringUtils.isNotBlank(batchNo)) {
            batchNo = getBatchNumber(date, String.format("%02d", device.getBatchNumber()));
        }

        TradeDetail tradeDetail;
        String userId;

        if (isRetry > 0 && isRetry < 4) {
            tradeDetail = tradeDetailService.getOne(orderId);
            userId = tradeDetail.getUserId();
        } else if (isRetry >= 4) {

            LOGGER.error("[REQUEST][EZC] STOP RETRY. " + device.getOwnerDeviceId() + ", " + merchant.getAccountId());
            EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Already retry 3 times.",
                    "900003");
            return getErrorResponse(requestHeader, errorResponse, "Already retry 3 times.");

        } else {
            tradeDetail = tradeDetailService.getOne(orderId);

            if (tradeDetail != null) {
                EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Store Order No duplicated",
                        "7000");
                return getErrorResponse(requestHeader, errorResponse, "Store Order No duplicated.");
            }

            userId = getUserId(paymentAccount, device);

            if (Strings.isNullOrEmpty(userId) || userId.equals("-1")) {
                LOGGER.error("[REQUEST][EZC] Card id not found. " + device.getOwnerDeviceId() + ", "
                        + merchant.getAccountId());
                EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Card id not found.",
                        "0462xx");
                return getErrorResponse(requestHeader, errorResponse, "Can not read card info.");
            }

            tradeDetail = new TradeDetail();
            tradeDetail.setStatus(SystemInstance.TRADE_CREATING_ORDER);
            tradeDetail.setOrderId(orderId);
            tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
            tradeDetail.setAccountId(requestHeader.getMerchantId());
            tradeDetail.setPayment(Long.valueOf(requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString()));
            tradeDetail.setDeviceRandomId(device);
            tradeDetail.setPaymentAccount(paymentAccount);
            tradeDetail.setMethod("31800");
            tradeDetail.setSystemOrderId(time);
            tradeDetail.setBatchNo(batchNo);
            tradeDetail.setUserId(userId);
            tradeDetail.setEzcTxnDate(date + time);
            tradeDetail.setComment(paymentAccount.getAccount() + device.getOwnerDeviceId());
            if (requestData.has("Body")) {
                tradeDetail.setDescription(requestData.get("Body").getAsString());
            }
            tradeDetail = tradeDetailService.save(tradeDetail);
            if (Objects.isNull(tradeDetail) || StringUtils.isBlank(tradeDetail.getOrderId())) {
                EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Store Order create fail",
                        "7142");
                return getErrorResponse(requestHeader, errorResponse, "Store Order create fail.");
            }
        }

        EzCardPayment ezCardPayment = this.getEzCardPayment(device, paymentAccount, requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString(), time, date
                , batchNo, requestData.get("Retry").getAsString(), isRetry > 0 ? requestData.get("TerminalTXNNumber").getAsString() : time);


        EzCardPaymentResponse response = new Gson().fromJson(doRequest(ezCardPayment, device.getAesKey()), EzCardPaymentResponse.class);
        return this.updateDoPayment(requestHeader, requestData, device, merchant, ezCardPayment, response, tradeDetail, paymentAccount);
    }

    private String taxiPayment(RequestHeader requestHeader, JsonObject requestData, Device device,
                               PaymentAccount paymentAccount, Merchant merchant) throws OtherAPIException {

        String date = getDate();
        String time = getTime();

        long isRetry = requestData.get("Retry").getAsLong();

        String orderId = requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString();
        String batchNo = device.getBatchNumber().toString();
        // System.out.println("BATCHNO!!!!!!"+batchNo);
        if (StringUtils.isNotBlank(batchNo)) {
            batchNo = getBatchNumber(date, String.format("%02d", device.getBatchNumber()));
        }

        // System.out.println("NEWBATCHNO!!!!!!"+newBatchNo);
        TradeDetail tradeDetail;
        String userId = "-1";

        if (isRetry > 0 && isRetry < 4) {
            tradeDetail = tradeDetailService.getOne(orderId);
            userId = tradeDetail.getUserId();
        } else if (isRetry >= 4) {

            LOGGER.error("[REQUEST][EZC] STOP RETRY. " + device.getOwnerDeviceId() + ", " + merchant.getAccountId());
            EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Already retry 3 times.",
                    "900003");
            return getErrorResponse(requestHeader, errorResponse, "Already retry 3 times.");

        } else {
            tradeDetail = tradeDetailService.getOne(orderId);

            if (tradeDetail != null) {
                EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Store Order No duplicated",
                        "7000");
                return getErrorResponse(requestHeader, errorResponse, "Store Order No duplicated.");
            }

            userId = requestData.get("EZCardID").getAsString();

            if (Strings.isNullOrEmpty(userId) || userId.equals("-1")) {
                LOGGER.error("[REQUEST][EZC] Card id not found. " + device.getOwnerDeviceId() + ", "
                        + merchant.getAccountId());
                EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Card id not found.",
                        "0462xx");
                return getErrorResponse(requestHeader, errorResponse, "Can not read card info.");
            }

            tradeDetail = new TradeDetail();
            tradeDetail.setStatus(SystemInstance.TRADE_CREATING_ORDER);
            tradeDetail.setOrderId(orderId);
            tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
            tradeDetail.setAccountId(requestHeader.getMerchantId());
            tradeDetail.setPayment(Long.valueOf(requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString()));
            tradeDetail.setDeviceRandomId(device);
            tradeDetail.setPaymentAccount(paymentAccount);
            tradeDetail.setMethod("31800");
            tradeDetail.setSystemOrderId(time);
            tradeDetail.setBatchNo(batchNo);
            tradeDetail.setUserId(userId);
            tradeDetail.setEzcTxnDate(date + time);
            tradeDetail.setCarePoint(Long.valueOf(requestData.get("DeductPoint").getAsString()));
            tradeDetail.setRemainCarePoint(Long.valueOf(requestData.get("PointBalance").getAsString()));
            tradeDetail.setComment(paymentAccount.getAccount() + device.getOwnerDeviceId());
            tradeDetail.setDescription("扣點: " + Long.valueOf(requestData.get("DeductPoint").getAsString()) + " 點, 扣款: "
                    + Long.valueOf(requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString()) + " 元");

            tradeDetail = tradeDetailService.save(tradeDetail);

            if (Objects.isNull(tradeDetail)) {
                throw new OtherAPIException(messageSource.getMessage("response.7142", null, locale));
            }
        }

        if (Long.valueOf(requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString()) == 0 && tradeDetail.getCarePoint() == 0) {
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetail.setEzcTxnDate(date + time);
            tradeDetailService.save(tradeDetail);
            AcerTaxiPaymentResponse acerTaxiPaymentResponse = new AcerTaxiPaymentResponse();
            acerTaxiPaymentResponse.setEzCardId(requestData.get("EZCardID").getAsString());
            acerTaxiPaymentResponse.setEzCardType(requestData.get("EZCardType").getAsString());
            acerTaxiPaymentResponse.setPersonalProfile(requestData.get("PersonalProfile").getAsString());
            acerTaxiPaymentResponse.setAreaCode(requestData.get("AreaCode").getAsString());
            this.insertTaxiTransactionDetail(tradeDetail, merchant, paymentAccount, device, acerTaxiPaymentResponse, requestData);

            throw new OtherAPIException(messageSource.getMessage("response.7354", null, locale));
        }
        AcerTaxiPayment acerTaxiPayment = new AcerTaxiPayment();
        acerTaxiPayment.setTerminalId(paymentAccount.getAccount());
        acerTaxiPayment.setDeviceId(device.getOwnerDeviceId());
        acerTaxiPayment.setTerminalTXNNumber(isRetry > 0 ? tradeDetail.getSystemOrderId() : time);
        acerTaxiPayment.setHostSerialNumber(isRetry > 0 ? tradeDetail.getTxParams() : String.format("%06d", (Long.valueOf(device.getHostSerialNumber()) + 1)));
        acerTaxiPayment.setTime(time);
        acerTaxiPayment.setDate(date);
        acerTaxiPayment.setAmount(requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString());
        acerTaxiPayment.setRetry(requestData.get("Retry").getAsString());
        acerTaxiPayment.setSameCard(isRetry > 0 ? "1" : "0");
        acerTaxiPayment.setBatchNumber(batchNo);
        acerTaxiPayment.setPersonalProfile(requestData.get("PersonalProfile").getAsString());
        acerTaxiPayment.setAreaCode(requestData.get("AreaCode").getAsString());
        acerTaxiPayment.setEzcardType(requestData.get("EZCardType").getAsString());
        acerTaxiPayment.setPersonalDiscount("0");
        acerTaxiPayment.setTransferDiscount(requestData.get("TransferDiscount").getAsString());
        acerTaxiPayment.setTransferGroup("00");
        acerTaxiPayment.setPaymentType("20");
        acerTaxiPayment.setPointID(requestData.get("PointID").getAsString());
        acerTaxiPayment.setPointType("WELFARE");
        acerTaxiPayment.setDeductPoint(requestData.get("DeductPoint").getAsString());
        acerTaxiPayment.setPointBalance(requestData.get("PointBalance").getAsString());
        acerTaxiPayment.setCountOfDeductPoint(requestData.get("CountOfDeductPoint").getAsString());
        acerTaxiPayment.setPointLastUseDate(requestData.get("PointLastUseDate").getAsString());
        acerTaxiPayment.setMaxPointOnMonth(requestData.get("MaxPointOnMonth").getAsString());
        acerTaxiPayment.setNumberPlate(device.getLicensePlate());
        acerTaxiPayment.setDriverNumber(requestHeader.getMerchantId());
        acerTaxiPayment.setTradeKey(requestHeader.getTradeKey());
        acerTaxiPayment.setPointRefillAmount(requestData.get("MaxPointOnMonth").getAsString());
        acerTaxiPayment.setCardNumberForPrint(requestData.get("EZCardID").getAsString());
        acerTaxiPayment.setPersonalProfileAuthorization(requestData.get("PersonalProfileAuthorization").getAsString());

        AcerTaxiPaymentResponse acerTaxiPaymentResponse = new Gson()
                .fromJson(this.taxiRequest(acerTaxiPayment, device.getAesKey()), AcerTaxiPaymentResponse.class);
        acerTaxiPaymentResponse.setOrderId(orderId);
//        acerTaxiPaymentResponse.setRequest(acerTaxiPayment);
        acerTaxiPaymentResponse.setCarFleetName(merchant.getCompanyName());
        acerTaxiPaymentResponse.setLicensePlate(device.getLicensePlate());
        acerTaxiPaymentResponse.setDriverNumber(requestHeader.getMerchantId());

        IntTbLookupCode lookupCode = lookupCodeService.findOne("ticketType", "18");
        acerTaxiPaymentResponse.setCardId("00");
        acerTaxiPaymentResponse.setCardIdDscr(lookupCode.getDscr());
        acerTaxiPaymentResponse.setNeedBalance(Objects.isNull(requestData.get("NeedCash")) || requestData.get("NeedCash").isJsonNull() ? "0" : requestData.get("NeedCash").getAsString());
        acerTaxiPaymentResponse.setBeforepoint(String.valueOf(Integer.valueOf(requestData.get("PointBalance").getAsString()) + Integer.valueOf(requestData.get("DeductPoint").getAsString())));
        acerTaxiPaymentResponse.setDiscountAmount(requestData.get("TransferDiscount").getAsString());
        acerTaxiPaymentResponse.setQueryCardInfoSultData(requestData.has("QueryCardInfoSultData") ? requestData.get("QueryCardInfoSultData").getAsString() : "");

        //edit :sol   20200107  桃園悠遊卡說要顯示卡機地區、卡片地區、卡片身分別
        IntTbLookupCode ezclocationLookupCode = lookupCodeService.findOne("ezclocation", device.getLocation());
        acerTaxiPaymentResponse.setDeviceArea(Objects.nonNull(ezclocationLookupCode) ? ezclocationLookupCode.getDscr() : device.getLocation());
        if (StringUtils.isNotBlank(acerTaxiPaymentResponse.getQueryCardInfoSultData())) {
            JsonObject queryCardInfoSultData = new JsonObject();
            try {
                queryCardInfoSultData = new Gson().fromJson(URLDecoder.decode(acerTaxiPaymentResponse.getQueryCardInfoSultData(), "UTF-8"), JsonObject.class).get("Data")
                        .getAsJsonObject();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            lookupCode = taxiPoint.isCanDeductionPoint(device, queryCardInfoSultData);
            acerTaxiPaymentResponse.setIdentityInfo(lookupCode.getDscr());
            ezclocationLookupCode = lookupCodeService.findOne("ezclocation", acerTaxiPaymentResponse.getAreaCode());
            acerTaxiPaymentResponse.setCardArea(Objects.nonNull(ezclocationLookupCode) ? ezclocationLookupCode.getDscr() : acerTaxiPaymentResponse.getAreaCode());
        }

        if (StringUtils.isNotBlank(acerTaxiPaymentResponse.getNewAESKey())) {
            device.setAesKey(acerTaxiPaymentResponse.getNewAESKey());
        }

        if (StringUtils.isNotBlank(acerTaxiPaymentResponse.getHostSerialNumber())) {
            device.setHostSerialNumber(acerTaxiPaymentResponse.getHostSerialNumber());
        } else {
            device.setHostSerialNumber(String.format("%06d", (Long.valueOf(device.getHostSerialNumber()) + 1)));
        }
        deviceService.save(device);

        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                "Payment | " + merchant.getMerchantSeqId() + " | " + device.getOwnerDeviceId() + " | "
                        + acerTaxiPaymentResponse.getAmount() + " | " + userId,
                acerTaxiPaymentResponse.getOrderId() + "|" + acerTaxiPaymentResponse.getTxnResult());

        if (StringUtils.isNotBlank(acerTaxiPaymentResponse.getAutoTopUpAmount()) && !"0".equals(acerTaxiPaymentResponse.getAutoTopUpAmount())) {
            // fixme should auto up need retry?
            saveReserveDetail(batchNo, requestData, requestHeader, orderId, acerTaxiPaymentResponse, device,
                    userId, date, 0, 1, time);
        }

        tradeDetail = tradeDetailService.getOne(tradeDetail.getAccountId(), tradeDetail.getOrderId());
        if (acerTaxiPaymentResponse.getTxnResult().equals("Success")) {
            LOGGER.info("[RESPONSE][ACER][Success] " + new Gson().toJson(acerTaxiPaymentResponse));
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetail.setRrn(acerTaxiPaymentResponse.getRrNumber());
            tradeDetail.setEzcTxnDate(date + time);
            tradeDetailService.save(tradeDetail);
            appNotificationService.notifyApp(tradeDetail, merchant);

            this.printReceipt(acerTaxiPaymentResponse, device, paymentAccount);

        } else {
            LOGGER.error("[RESPONSE][ACER][Fail] " + new Gson().toJson(acerTaxiPaymentResponse));
            if ("000125".equals(acerTaxiPaymentResponse.getErrorCode())) {
                tradeDetail.setTxParams(acerTaxiPayment.getHostSerialNumber());
            }
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            tradeDetailService.save(tradeDetail);
        }
        if (!"000125".equals(acerTaxiPaymentResponse.getErrorCode())) {
            this.insertTaxiTransactionDetail(tradeDetail, merchant, paymentAccount, device, acerTaxiPaymentResponse, requestData);
        }
        return getGeneralResponse(requestHeader, acerTaxiPaymentResponse, SystemInstance.STATUS_SUCCESS);
    }

    private void saveReserveDetail(String batchNo, JsonObject requestData, RequestHeader requestHeader, String orderId,
                                   EzCardPaymentResponse response, Device device, String userId, String date, long retry, int isAuto,
                                   String time) {

        ReserveDetail reserveDetail;
        if (retry > 0) {
            reserveDetail = reserveDetailService.getOne(requestHeader.getMerchantId(), orderId);
            LOGGER.info("[EZC][RESERVE][RETRY] saveReserveDetail > " + response.getTxnResult());
            reserveDetail.setStatus(response.getTxnResult().equals("Success") ? "0" : "1");
            reserveDetail.setRrn(response.getRrNumber());
            reserveDetail.setEzcTxnDate(date + time);
        } else {
            LOGGER.info("[EZC][RESERVE] saveReserveDetail > " + response.getTxnResult());
            reserveDetail = new ReserveDetail();
            reserveDetail.setAccountId(requestHeader.getMerchantId());
            reserveDetail.setAmount(isAuto == 1 ? Long.valueOf(response.getAutoTopUpAmount())
                    : requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsLong());
            reserveDetail.setBatchNumber(batchNo);
            reserveDetail.setCashier(requestData.has("Cashier") ? requestData.get("Cashier").getAsString()
                    : SystemInstance.EMPTY_STRING);
            reserveDetail.setDeviceId(device.getOwnerDeviceId());
            reserveDetail.setReserveOrderId(orderId);
            reserveDetail.setStatus((isAuto == 1 ? !"0".equalsIgnoreCase(response.getAutoTopUpAmount()) : response.getTxnResult().equals("Success")) ? "0" : "1");
            reserveDetail.setMethod("31800");
            reserveDetail.setUserId(userId);
            reserveDetail.setRrn(response.getRrNumber());
            reserveDetail.setEzcTxnDate(date + time);
            reserveDetail.setIsAuto(isAuto);
            reserveDetail.setDeviceRandomId(device);
        }

        ReserveDetail entity = reserveDetailService.save(reserveDetail);
        if (entity == null) {
            LOGGER.error("[REQUEST][EZC] write reserve detail failed. " + new Gson().toJson(response));
        }
    }

    private String doRefundWithoutTradeDetail(RequestHeader requestHeader, JsonObject requestData, Device device,
                                              PaymentAccount paymentAccount, Merchant merchant) throws OrderAlreadyRefundedException {

        LOGGER.info("doRefundWithoutTradeDetail() " + requestHeader.getServiceType());

        String date = getDate();
        String time = getTime();

        String userId;
        TradeDetail tradeDetail;

        String batchNo = device.getBatchNumber().toString();

        if (StringUtils.isNotBlank(batchNo)) {
            batchNo = getBatchNumber(date, String.format("%02d", device.getBatchNumber()));
        }

        long isRetry = requestData.get("Retry").getAsLong();
        if (isRetry >= 4) {
            LOGGER.error("[REQUEST][EZC] STOP RETRY. " + device.getOwnerDeviceId() + ", " + merchant.getAccountId());
            EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Already retry 3 times.",
                    "900003");
            return getErrorResponse(requestHeader, errorResponse, "Already retry 3 times.");
        } else if (isRetry > 0 && isRetry < 4) {
            tradeDetail = tradeDetailService
                    .getOne(requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString());
            if (Objects.nonNull(tradeDetail) && SystemInstance.TRADE_REFUND_SUCCESS.equals(tradeDetail.getRefundStatus())) {
                LOGGER.error("[EZC] tradeDetail already refund.");
                throw new OrderAlreadyRefundedException("tradeDetail already refund.");
            }
            userId = tradeDetail.getUserId();
        } else {
            userId = getUserId(paymentAccount, device);
            tradeDetail = new TradeDetail();

            if (Strings.isNullOrEmpty(userId) || userId.equals("-1")) {
                LOGGER.error("[REQUEST][EZC] Card id not found. " + device.getOwnerDeviceId() + ", "
                        + merchant.getAccountId());
                EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Card id not found.",
                        "0462xx");
                return getErrorResponse(requestHeader, errorResponse, "Can not read card info.");
            }
        }

        EzCardRefund ezCardRefund = this.getEzCardRefund(device, paymentAccount, time, date, batchNo,
                requestData.get("Retry").getAsString(), isRetry > 0 ? requestData.get("TerminalTXNNumber").getAsString() : time);
        ezCardRefund.setAmount(String.valueOf(tradeDetail.getPayment()));

        EzCardRefundResponse response = new Gson().fromJson(doRequest(ezCardRefund, device.getAesKey()),
                EzCardRefundResponse.class);

        if (StringUtils.isNotBlank(response.getNewAESKey())) {
            device.setAesKey(response.getNewAESKey());
            // if (StringUtils.isNotBlank(response.getHostSerialNumber()))
            // {
            // device.setHostSerialNumber(response.getHostSerialNumber());
            // }
            deviceService.save(device);
        }

        // set refundKey for retry
        if (response.getErrorCode().equals("000125")) {
            ezCardRefund.setRefundKey(requestData.get("RefundKey").getAsString());
        }

        response.setRequest(ezCardRefund);

        if (isRetry == 0) {
            tradeDetail.setStatus(response.getTxnResult().equals("Success") ? SystemInstance.TRADE_SUCCESS
                    : SystemInstance.TRADE_WAITING);
            tradeDetail.setOrderId(orderService.createOrderId(requestHeader.getMerchantId()));
            tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
            tradeDetail.setAccountId(requestHeader.getMerchantId());

            // notice : this trade is fake. just link to refund detail
            tradeDetail.setPayment(0);
            tradeDetail.setDeviceRandomId(device);
            tradeDetail.setPaymentAccount(paymentAccount);
            tradeDetail.setMethod("31800");
            tradeDetail.setSystemOrderId(time);
            // notice : this trade should not have batch no, because settlement
            // will be false.
            tradeDetail.setUserId(userId);
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
            tradeDetail.setEzcTxnDate(date + time);
            tradeDetailService.save(tradeDetail);

        }
        response.setOrderId(tradeDetail.getOrderId());

        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                "REFUND(UNLIMITED) | " + userId + " | " + response.getAmount(),
                response.getOrderId() + "|" + response.getTxnResult());
        saveOrUpdateTransactionDetails(tradeDetail, merchant, response, device, batchNo,
                Long.valueOf(ezCardRefund.getAmount()), isRetry, time, false);

        if (response.getTxnResult().equals("Success")) {
            printReceipt(response, device, paymentAccount);
        }

        LOGGER.info("doRefundWithoutTradeDetail() END " + requestHeader.getServiceType());
        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    private String doRefund(RequestHeader requestHeader, JsonObject requestData, Device device,
                            PaymentAccount paymentAccount, Merchant merchant)
            throws OrderNotFoundException, OrderAlreadyRefundedException, OtherAPIException {
        LOGGER.info("doRefund() START " + requestHeader.getServiceType());
        String date = getDate();
        String time = getTime();

        String orderId = requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString();
        String batchNo = device.getBatchNumber().toString();

        if (StringUtils.isNotBlank(batchNo)) {
            batchNo = getBatchNumber(date, String.format("%02d", device.getBatchNumber()));
        }

        String amount = requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString();
        long isRetry = requestData.get("Retry").getAsLong();

        if (isRetry >= 4) {
            LOGGER.error("[REQUEST][EZC] STOP RETRY. " + device.getOwnerDeviceId() + ", " + merchant.getAccountId());
            EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Already retry 3 times.",
                    "900003");
            return getErrorResponse(requestHeader, errorResponse, "Already retry 3 times.");
        }

        TradeDetail tradeDetail = tradeDetailService.getOne(orderId);
        if (tradeDetail == null) {
            LOGGER.error("[EZC] tradeDetail not found.");
            throw new OrderNotFoundException("tradeDetail not found." + orderId);
        } else if (tradeDetail.getRefundStatus() != null
                && tradeDetail.getRefundStatus().equals(SystemInstance.TRADE_REFUND_SUCCESS)) {
            LOGGER.error("[EZC] tradeDetail already refund.");
            throw new OrderAlreadyRefundedException("tradeDetail already refund.");
        } else if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
            LOGGER.info("tradeDetail not TRADE_SUCCESS \t" + tradeDetail.getOrderId() + "\t" + tradeDetail.getStatus());
            throw new OtherAPIException(messageSource.getMessage("response.7351", null, locale));
        } else {
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_INIT);
            tradeDetailService.save(tradeDetail);
            LOGGER.info("[EZC] update tradeDetail and create refund detail.");
        }

        EzCardRefund ezCardRefund = this.getEzCardRefund(device, paymentAccount, time, date, batchNo,
                requestData.get("Retry").getAsString(), isRetry > 0 ? requestData.get("TerminalTXNNumber").getAsString() : time);
        ezCardRefund.setAmount(String.valueOf(tradeDetail.getPayment()));
        EzCardRefundResponse response = new Gson().fromJson(doRequest(ezCardRefund, device.getAesKey()),
                EzCardRefundResponse.class);
        return this.updateDoRefund(requestHeader, requestData, device, merchant, ezCardRefund, response, tradeDetail, paymentAccount, batchNo);
    }

    private void saveOrUpdateTransactionDetails(TradeDetail tradeDetail, Merchant merchant,
                                                EzCardPaymentResponse response, Device device, String batchNo, long amount, long isRetry, String txnTime,
                                                boolean isCancel) {
        if (response.getTxnResult().equals("Success")) {
            LOGGER.info(
                    "[RESPONSE][EZC] EZC_REFUND success and update refund information." + new Gson().toJson(response));
            tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
            tradeDetailService.save(tradeDetail);

            RefundDetail refundDetail = new RefundDetail();
            refundDetail.setMethod("31800");
            refundDetail.setOrderId(tradeDetail.getOrderId());
            refundDetail.setAmount(amount);
            refundDetail.setTradeDetailRandomId(tradeDetailService.getOne(tradeDetail.getOrderId()));
            refundDetail.setCreateTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
            refundDetail.setStoreRefundId(tradeDetail.getOrderId());
            refundDetail.setSystemRefundId(txnTime);
            refundDetail.setStatus(SystemInstance.TRADE_REFUND_SUCCESS);
            refundDetail.setPartialRefund(false);
            refundDetail.setBatchNo(batchNo);
            refundDetail.setDeviceRandomId(device);
            refundDetail.setAccountId(merchant.getAccountId());
            refundDetail.setReason(isRetry > 0 ? "Retry" : "");
            refundDetail.setEzcTxnDate(response.getDate() + response.getTime());
            refundDetail.setEzcCancel(isCancel);
            refundDetail.setRrn(response.getRrNumber());
            refundDetailService.save(refundDetail);

        } else {
            LOGGER.info("[RESPONSE][EZC] EZC_REFUND fail and update refund information." + new Gson().toJson(response));
            tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
            tradeDetailService.save(tradeDetail);

            RefundDetail refundDetail = new RefundDetail();
            refundDetail.setMethod("31800");
            refundDetail.setOrderId(tradeDetail.getOrderId());
            refundDetail.setAmount(amount);
            refundDetail.setTradeDetailRandomId(tradeDetailService.getOne(tradeDetail.getOrderId()));
            refundDetail.setCreateTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
            refundDetail.setStoreRefundId(tradeDetail.getOrderId());
            refundDetail.setSystemRefundId(txnTime);
            refundDetail.setStatus(SystemInstance.TRADE_REFUND_FAIL);
            refundDetail.setPartialRefund(false);
            refundDetail.setDeviceRandomId(device);
            refundDetail.setAccountId(merchant.getAccountId());
            refundDetail.setReason(isRetry > 0 ? "Retry" : response.getErrorCode());
            refundDetail.setEzcTxnDate(response.getDate() + response.getTime());
            refundDetailService.save(refundDetail);
        }
    }

    private String doReserve(RequestHeader requestHeader, JsonObject requestData, Device device,
                             PaymentAccount paymentAccount) {

        String date = getDate();
        String time = getTime();

        long isRetry = requestData.get("Retry").getAsLong();
        String orderId = requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString();
        String batchNum = device.getBatchNumber().toString();
        if (StringUtils.isNotBlank(batchNum)) {
            batchNum = getBatchNumber(date, String.format("%02d", device.getBatchNumber()));
        }

        String userId;

        if (isRetry > 0 && isRetry < 4) {
            ReserveDetail reserveDetail = reserveDetailService.getOne(requestHeader.getMerchantId(), orderId);
            userId = reserveDetail.getUserId();
        } else if (isRetry >= 4) {
            LOGGER.error(
                    "[REQUEST][EZC] STOP RETRY. " + device.getOwnerDeviceId() + ", " + requestHeader.getMerchantId());
            EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Already retry 3 times.",
                    "900003");
            return getErrorResponse(requestHeader, errorResponse, "Already retry 3 times.");
        } else {
            userId = requestData.has("UserId") ? requestData.get("UserId").getAsString() : getUserId(paymentAccount, device);
        }

        if (Strings.isNullOrEmpty(userId) || userId.equals("-1")) {
            LOGGER.error("[REQUEST][EZC] Card id not found. " + device.getOwnerDeviceId() + ", "
                    + requestHeader.getMerchantId());
            EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Card id not found.", "0462xx");
            return getErrorResponse(requestHeader, errorResponse, "Can not read card info.");
        }

        EzCardReserve ezCardReserve = new EzCardReserve();
        ezCardReserve.setTerminalId(paymentAccount.getAccount());
        ezCardReserve.setDeviceId(device.getOwnerDeviceId());

        ezCardReserve.setTerminalTXNNumber(isRetry > 0 ? requestData.get("TerminalTXNNumber").getAsString() : time);
        ezCardReserve.setHostSerialNumber(getTime()); // 因之前有嚴重延遲，所以改為送出前當下
        ezCardReserve.setTime(time);
        ezCardReserve.setDate(date);
        ezCardReserve.setRetry(requestData.get("Retry").getAsString());
        ezCardReserve.setAmount(requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString());
        ezCardReserve.setBatchNumber(batchNum);
        ezCardReserve.setSameCard(isRetry > 0 ? "1" : "0");

        EzCardReserveResponse response = new Gson().fromJson(doRequest(ezCardReserve, device.getAesKey()),
                EzCardReserveResponse.class);
        response.setRequest(ezCardReserve);
        response.setOrderId(orderId);

        if (StringUtils.isNotBlank(response.getNewAESKey())) {
            device.setAesKey(response.getNewAESKey());
            // if (StringUtils.isNotBlank(response.getHostSerialNumber()))
            // {
            // device.setHostSerialNumber(response.getHostSerialNumber());
            // }
            deviceService.save(device);
        }

        writeDBLog(requestHeader.getMerchantId(),
                requestHeader.getServiceType(), requestHeader.getMethod(), "RESERVE | " + paymentAccount.getAccount()
                        + "|" + device.getOwnerDeviceId() + "|" + response.getAmount(),
                response.getOrderId() + "|" + response.getTxnResult());
        saveReserveDetail(batchNum, requestData, requestHeader, orderId, response, device, userId, date, isRetry, 0,
                time);

        if (response.getTxnResult().equals("Success")) {
            printReceipt(response, device, paymentAccount);
        }

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    private String doSignOn(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader) {

        LOGGER.info("[REQUEST][EZC][SignOn][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");

        String date = getDate();
        String time = getTime();

        EzCardSignOn ezCardSignOn = new EzCardSignOn();
        ezCardSignOn.setTerminalId(paymentAccount.getAccount());
        ezCardSignOn.setDeviceId(device.getOwnerDeviceId());

        ezCardSignOn.setTerminalTXNNumber(time);

        ezCardSignOn.setHostSerialNumber(getTime()); // 因之前有嚴重延遲，所以改為送出前當下

        ezCardSignOn.setTime(time);
        ezCardSignOn.setDate(date);

        EzCardSignOnResponse response = new Gson().fromJson(doRequest(ezCardSignOn, device.getAesKey()),
                EzCardSignOnResponse.class);

        if (StringUtils.isNotBlank(response.getNewAESKey())) {

            if (response.getErrorCode().equals("900002")) {
                device.setBan("1");
            }

            device.setEzcDongleId(response.getDongleDeviceID());
            device.setAesKey(response.getNewAESKey());
            // device.setHostSerialNumber(response.getHostSerialNumber());

            deviceService.save(device);
        }

        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                new Gson().toJson(ezCardSignOn), response.getOrderId() + "|" + response.getTxnResult());
        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    private String taxiSignOn(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader) {

        LOGGER.info(
                "[REQUEST][EZC][TaxiSignOn][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");

        String date = getDate();
        String time = getTime();

        EzCardSignOn ezCardSignOn = new EzCardSignOn();
        ezCardSignOn.setTerminalId(paymentAccount.getAccount());
        ezCardSignOn.setDeviceId(device.getOwnerDeviceId());

        ezCardSignOn.setTerminalTXNNumber(time);

        ezCardSignOn.setHostSerialNumber(getTime()); // 因之前有嚴重延遲，所以改為送出前當下
        // device.setHostSerialNumber(getTime()); // 因之前有嚴重延遲，所以改為送出前當下

        ezCardSignOn.setTime(time);
        ezCardSignOn.setDate(date);

        EzCardSignOnResponse response = new Gson().fromJson(taxiRequest(ezCardSignOn, device.getAesKey()),
                EzCardSignOnResponse.class);

        if (StringUtils.isNotBlank(response.getHostSerialNumber())) {
            device.setHostSerialNumber(response.getHostSerialNumber());
        }

        if (StringUtils.isNotBlank(response.getNewAESKey())) {

            if (response.getErrorCode().equals("900002")) {
                device.setBan("1");
            }

            device.setEzcDongleId(response.getDongleDeviceID());
            device.setAesKey(response.getNewAESKey());
        }

        deviceService.save(device);

        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                new Gson().toJson(ezCardSignOn), response.getOrderId() + "|" + response.getTxnResult());
        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    private String parkSignOn(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader) {

        LOGGER.info(
                "[REQUEST][EZC][ParkSignOn][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");

        String date = getDate();
        String time = getTime();

        EzCardSignOn ezCardSignOn = new EzCardSignOn();
        ezCardSignOn.setTerminalId(paymentAccount.getAccount());
        ezCardSignOn.setDeviceId(device.getOwnerDeviceId());

        ezCardSignOn.setTerminalTXNNumber(time);

        ezCardSignOn.setHostSerialNumber(getTime()); // 因之前有嚴重延遲，所以改為送出前當下
        // device.setHostSerialNumber(getTime()); // 因之前有嚴重延遲，所以改為送出前當下

        ezCardSignOn.setTime(time);
        ezCardSignOn.setDate(date);

        EzCardSignOnResponse response = new Gson().fromJson(parkRequest(ezCardSignOn, device.getAesKey()),
                EzCardSignOnResponse.class);

        if (StringUtils.isNotBlank(response.getHostSerialNumber())) {
            device.setHostSerialNumber(response.getHostSerialNumber());
        }

        if (StringUtils.isNotBlank(response.getNewAESKey())) {

            if (response.getErrorCode().equals("900002")) {
                device.setBan("1");
            }

            device.setEzcDongleId(response.getDongleDeviceID());
            device.setAesKey(response.getNewAESKey());
            device.setHostSerialNumber(response.getHostSerialNumber());

            deviceService.save(device);
        }

        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                new Gson().toJson(ezCardSignOn), response.getOrderId() + "|" + response.getTxnResult());
        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    private String deviceQueryForTaxi(Merchant merchant, RequestHeader requestHeader) {
        LOGGER.info("[REQUEST][EZC] list device by merchantId : " + merchant.getAccountId());
        List<DeviceVo> deviceList = deviceService.listAllByMerchantAndType(merchant.getAccountId(), "ezc");
        PaymentAccount paymentAccount = paymentAccountService.getOne(String.valueOf(merchant.getMerchantSeqId()),
                Long.valueOf("18"));

        EzCardDeviceQueryResponse response = new EzCardDeviceQueryResponse();
        response.setDeviceList(deviceList);

        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                "DeviceQuery:" + merchant.getAccountId(), response.getOrderId() + "|" + response.getTxnResult());

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    private String getBalanceQuery(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader) {

        LOGGER.info(
                "[REQUEST][EZC][BalanceQuery][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");

        String date = getDate();
        String time = getTime();

        EzCardInfoQuery ezCardInfoQuery = this.getEzCardInfoQuery(device, paymentAccount, date, time);

        EzCardInfoQueryResponse response = new Gson().fromJson(doRequest(ezCardInfoQuery, device.getAesKey()),
                EzCardInfoQueryResponse.class);

        return this.upodateBalanceQuery(device, requestHeader, ezCardInfoQuery, response);
    }

    private String taxiCardQuery(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader,
                                 JsonObject requestData) {

        LOGGER.info(
                "[REQUEST][EZC][BalanceQuery][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");

        String date = getDate();
        String time = getTime();

        AcerTaxiQuery acerTaxiQuery = new AcerTaxiQuery();
        acerTaxiQuery.setTerminalId(paymentAccount.getAccount());
        acerTaxiQuery.setDeviceId(device.getOwnerDeviceId());

        acerTaxiQuery.setTerminalTXNNumber(time);

        acerTaxiQuery.setHostSerialNumber(String.format("%06d", (Long.valueOf(device.getHostSerialNumber()) + 1)));
        acerTaxiQuery.setTime(time);
        acerTaxiQuery.setDate(date);
        acerTaxiQuery.setNumberPlate(device.getLicensePlate());
        acerTaxiQuery.setDriverNumber(requestHeader.getMerchantId());
        acerTaxiQuery
                .setAmount(Objects.isNull(requestData.get("Amount")) ? "1" : requestData.get("Amount").getAsString());

        String result = taxiRequest(acerTaxiQuery, device.getAesKey());
        AcerTaxiQueryResponse response = new Gson().fromJson(result,
                AcerTaxiQueryResponse.class);

        if (StringUtils.isNotBlank(response.getNewAESKey())) {
            device.setAesKey(response.getNewAESKey());
            // if (StringUtils.isNotBlank(response.getHostSerialNumber())) {
            // device.setHostSerialNumber(response.getHostSerialNumber());
            // }
        }

        if (StringUtils.isNotBlank(response.getHostSerialNumber())) {
            device.setHostSerialNumber(response.getHostSerialNumber());
        } else {
            device.setHostSerialNumber(String.format("%06d", (Long.valueOf(device.getHostSerialNumber()) + 1)));
        }
        deviceService.save(device);

        System.out.println("AreaCode!!!!!" + response.getAreaCode());
        String getPointforRefill = "0";
        String discount = "1";
        String maxPointPerTrip = "0";
        String usePointLimit = "0";
        if (StringUtils.isNotBlank(response.getEzCardType()) && response.getEzCardType().equals("01")) {
            try {
                usePointLimit = taxiPoint.getlLcationUsePointLimit(device.getLocation());
                IntTbLookupCode cardLookupCode = taxiPoint.isCanDeductionPoint(device, new Gson().fromJson(result, JsonObject.class));
                if (Objects.nonNull(cardLookupCode)) {
                    getPointforRefill = cardLookupCode.getType2();
                    discount = cardLookupCode.getType3();
                    maxPointPerTrip = String.valueOf(cardLookupCode.getValue());
                }
            } catch (Exception e) {
                LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
            }
        }
        response.setPointRefillAmount(getPointforRefill.replaceAll("\"", ""));
        response.setDiscount(discount.replaceAll("\"", ""));
        response.setMaxPointPerTrip(maxPointPerTrip);
        response.setUsePointLimit(usePointLimit);

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    private String checkPrinter(RequestHeader requestHeader, Device device, PaymentAccount paymentAccount) {
        LOGGER.info("[REQUEST][EZC][EDCACheckPrinter][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId()
                + "]");

        EDCACheckDeviceReq checkDeviceReq = new EDCACheckDeviceReq();
        checkDeviceReq.setTerminalId(paymentAccount.getAccount());
        checkDeviceReq.setDeviceId(device.getOwnerDeviceId());

        EDCACheckDeviceResponse checkDeviceResponse = new Gson()
                .fromJson(parkRequest(checkDeviceReq, device.getAesKey()), EDCACheckDeviceResponse.class);

        return getGeneralResponse(requestHeader, checkDeviceResponse, SystemInstance.STATUS_SUCCESS);
    }

    private String EDCAPayment(RequestHeader requestHeader, JsonObject requestData, Device device,
                               PaymentAccount paymentAccount, Merchant merchant) {

        LOGGER.info(
                "[REQUEST][EZC][EDCAPayment][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");

        long isRetry = requestData.get("Retry").getAsLong();
        String date = getDate();
        String time = getTime();

        String orderId = requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString();
        String batchNo = device.getBatchNumber().toString();
        // System.out.println("BATCHNO!!!!!!"+batchNo);
        if (StringUtils.isNotBlank(batchNo)) {
            batchNo = getBatchNumber(date, String.format("%02d", device.getBatchNumber()));
        }

        TradeDetail tradeDetail;
        String userId;

        if (isRetry > 0 && isRetry < 4) {

            tradeDetail = tradeDetailService.getOne(orderId);
            userId = tradeDetail.getUserId();
        } else if (isRetry >= 4) {

            LOGGER.error("[REQUEST][EZC] STOP RETRY. " + device.getOwnerDeviceId() + ", " + merchant.getAccountId());
            EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Already retry 3 times.",
                    "900003");
            return getErrorResponse(requestHeader, errorResponse, "Already retry 3 times.");

        } else {
            tradeDetail = tradeDetailService.getOne(orderId);

            if (tradeDetail != null) {
                EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Store Order No duplicated",
                        "7000");
                return getErrorResponse(requestHeader, errorResponse, "Store Order No duplicated.");
            }

            EzCardEDCAQueryReq edcaQueryReq = new EzCardEDCAQueryReq();
            edcaQueryReq.setTerminalId(paymentAccount.getAccount());
            edcaQueryReq.setDeviceId(device.getOwnerDeviceId());

            edcaQueryReq.setTerminalTXNNumber(time);
            edcaQueryReq.setHostSerialNumber(String.format("%06d", (Long.valueOf(device.getHostSerialNumber()) + 1)));
            edcaQueryReq.setTime(time);
            edcaQueryReq.setDate(date);
            edcaQueryReq.setAmount(requestData.get("Amount").getAsString());

            EzCardEDCAQueryResponse response = new Gson().fromJson(parkRequest(edcaQueryReq, device.getAesKey()),
                    EzCardEDCAQueryResponse.class);

            if (Strings.isNullOrEmpty(response.getEzCardId()) || response.getEzCardId().equals("-1")) {
                LOGGER.error("[REQUEST][EZC] Card id not found. " + device.getOwnerDeviceId() + ", "
                        + merchant.getAccountId());
                EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Card id not found.",
                        response.getErrorCode());
                return getErrorResponse(requestHeader, errorResponse, "Can not read card info.");
            }

            if (StringUtils.isNotBlank(response.getNewAESKey())) {
                device.setAesKey(response.getNewAESKey());
                if (StringUtils.isNotBlank(response.getHostSerialNumber())) {
                    device.setHostSerialNumber(response.getHostSerialNumber());
                }
                deviceService.save(device);
            }

            userId = response.getEzCardId();

            if (Strings.isNullOrEmpty(userId) || userId.equals("-1")) {
                LOGGER.error("[REQUEST][EZC] Card id not found. " + device.getOwnerDeviceId() + ", "
                        + merchant.getAccountId());
                EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, "Card id not found.",
                        "0462xx");
                return getErrorResponse(requestHeader, errorResponse, "Can not read card info.");
            }

            tradeDetail = new TradeDetail();
            tradeDetail.setStatus(SystemInstance.TRADE_CREATING_ORDER);
            tradeDetail.setOrderId(orderId);
            tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
            tradeDetail.setAccountId(requestHeader.getMerchantId());
            tradeDetail.setPayment(Long.valueOf(requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString()));
            tradeDetail.setDeviceRandomId(device);
            tradeDetail.setPaymentAccount(paymentAccount);
            tradeDetail.setMethod("31800");
            tradeDetail.setSystemOrderId(time);
            tradeDetail.setBatchNo(batchNo);
            tradeDetail.setUserId(userId);
            tradeDetail.setEzcTxnDate(date + time);
            // tradeDetail.setCarePoint(Long.valueOf(requestData.get("DeductPoint").getAsString()));
            // tradeDetail.setRemainCarePoint(Long.valueOf(requestData.get("PointBalance").getAsString()));
            tradeDetail.setComment(paymentAccount.getAccount() + device.getOwnerDeviceId());
            tradeDetail.setDescription(
                    "扣款: " + Long.valueOf(requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString()) + " 元");

            tradeDetailService.save(tradeDetail);
        }

        EzCardEDCAPaymentReq edcaPaymentReq = this.getEzCardEDCAPaymentReq(device, paymentAccount, requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString(), time, date, batchNo, requestData.get("Retry").getAsString());

        LOGGER.info("[REQUEST][EZC][EDCAPayment][REQ][" + new Gson().toJson(edcaPaymentReq));
        AcerTaxiPaymentResponse paymentResponse = new Gson().fromJson(parkRequest(edcaPaymentReq, device.getAesKey()),
                AcerTaxiPaymentResponse.class);
        return this.updateEDCAPayment(requestHeader, requestData, device, merchant, edcaPaymentReq, paymentResponse, tradeDetail, paymentAccount);
    }

    private String EDCACardQuery(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader,
                                 JsonObject requestData) {

        LOGGER.info(
                "[REQUEST][EZC][BalanceQuery][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");

        String date = getDate();
        String time = getTime();

        EzCardEDCAQueryReq edcaQueryReq = new EzCardEDCAQueryReq();
        edcaQueryReq.setTerminalId(paymentAccount.getAccount());
        edcaQueryReq.setDeviceId(device.getOwnerDeviceId());

        edcaQueryReq.setTerminalTXNNumber(time);

        edcaQueryReq.setHostSerialNumber(String.format("%06d", (Long.valueOf(device.getHostSerialNumber()) + 1)));
        edcaQueryReq.setTime(time);
        edcaQueryReq.setDate(date);
        edcaQueryReq.setNumberPlate(device.getLicensePlate());
        edcaQueryReq.setAmount(requestData.get("Amount").getAsString());
        // edcaQueryReq.setDriverNumber(requestHeader.getMerchantId());

        String result = parkRequest(edcaQueryReq, device.getAesKey());
        EzCardEDCAQueryResponse response = new Gson().fromJson(result,
                EzCardEDCAQueryResponse.class);

        if (StringUtils.isNotBlank(response.getNewAESKey())) {
            device.setAesKey(response.getNewAESKey());
            if (StringUtils.isNotBlank(response.getHostSerialNumber())) {
                device.setHostSerialNumber(response.getHostSerialNumber());
            }

            deviceService.save(device);
        }

        System.out.println("AreaCode!!!!!" + response.getAreaCode());
        String getPointforRefill = "0";
        String discount = "1";
        if (StringUtils.isNotBlank(response.getEzCardType()) && response.getEzCardType().equals("01")) {

            try {
                IntTbLookupCode cardLookupCode = taxiPoint.isCanDeductionPoint(device, new Gson().fromJson(result, JsonObject.class));
                if (Objects.nonNull(cardLookupCode)) {
                    getPointforRefill = cardLookupCode.getType2();
                    discount = cardLookupCode.getType3();
                }
            } catch (Exception e) {
                LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
            }
        }
        response.setPointRefillAmount(getPointforRefill.replaceAll("\"", ""));
        response.setDiscount(discount.replaceAll("\"", ""));

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    private String doSettlement(Device device, Merchant merchant, PaymentAccount paymentAccount,
                                RequestHeader requestHeader, String batchNum) {

        String date = getDate();
        String time = getTime();

        String deviceId = device.getOwnerDeviceId();
        String batchNo = batchNum == null ? getBatchNumber(date, String.format("%02d", device.getBatchNumber()))
                : batchNum;

        LOGGER.info("[REQUEST][EZC][Settlement][" + merchant.getAccountId() + "][" + deviceId + "][" + batchNo + "]");

        List<TradeDetailVo> tradeDetails = tradeDetailService.listByBatchNo(merchant.getAccountId(), deviceId, batchNo);
        List<ReserveDetail> reserveDetails = reserveDetailService.listByBatchNo(merchant.getAccountId(), deviceId,
                batchNo);
        List<RefundDetailVo> refundDetails = refundDetailService.listByBatchNo(merchant.getAccountId(), deviceId,
                batchNo);
        List<ReserveCancel> ReserveCancels = reserveCancelService
                .listByMerchantIdAndDeviceIdAndBatchNo(merchant.getAccountId(), deviceId, batchNo);

        long txCountTrade = 0;
        long txCountRefund = 0;
        long txCountReserve = 0;
        long txCountCancel = 0;
        long txCountAutoReserve = 0;
        long txCountCashReserve = 0;

        long txAmountTrade = 0;
        long txAmountRefund = 0;
        long txAmountReserve = 0;
        long txAmountCancel = 0;
        long txAmountAutoReserve = 0;
        long txAmountCashReserve = 0;

        List<EzcTXNData> txnDataList = new ArrayList<EzcTXNData>();

        if (tradeDetails != null) {
            LOGGER.info("tradeDetails : " + tradeDetails.size());
            for (TradeDetailVo tx : tradeDetails) {
                if (tx.getStatus().equals(SystemInstance.TRADE_SUCCESS)) {
                    txAmountTrade = txAmountTrade + tx.getPayment();
                    txCountTrade = txCountTrade + 1;

                    EzcTXNData ezcTXNData = new EzcTXNData();
                    ezcTXNData.setTxnType("Payment");
                    ezcTXNData.setAmount(String.valueOf(tx.getPayment()));
                    ezcTXNData.setCardNumber(tx.getUserId());

                    LOGGER.info("tradeDetails : txn " + tx.getEzcTxnDate());
                    ezcTXNData.setDate(
                            Strings.isNullOrEmpty(tx.getEzcTxnDate()) ? "" : tx.getEzcTxnDate().substring(0, 8));
                    ezcTXNData.setTerminalTXNNumber(tx.getSystemOrderId());
                    ezcTXNData.setTime(Strings.isNullOrEmpty(tx.getEzcTxnDate()) || tx.getEzcTxnDate().length() < 14
                            ? "" : tx.getEzcTxnDate().substring(8, 14));
                    txnDataList.add(ezcTXNData);
                }
            }
        }

        if (reserveDetails != null) {
            LOGGER.info("reserveDetails : " + reserveDetails.size());
            for (ReserveDetail tx : reserveDetails) {
                if (tx.getStatus().equals("0")) {
                    txAmountReserve = txAmountReserve + tx.getAmount();
                    txCountReserve = txCountReserve + 1;

                    LOGGER.info("reserveDetails : txn " + tx.getEzcTxnDate());

                    if (tx.getIsAuto() == 1) {
                        txAmountAutoReserve = txAmountAutoReserve + tx.getAmount();
                        txCountAutoReserve = txCountAutoReserve + 1;
                        EzcTXNData ezcTXNData = new EzcTXNData();
                        ezcTXNData.setTxnType("AutoCashTopUp");
                        ezcTXNData.setAmount(String.valueOf(tx.getAmount()));
                        ezcTXNData.setCardNumber(tx.getUserId());

                        String txnDate = Strings.isNullOrEmpty(tx.getEzcTxnDate()) ? ""
                                : tx.getEzcTxnDate().substring(0, 8);
                        String txnTime = Strings.isNullOrEmpty(tx.getEzcTxnDate()) || tx.getEzcTxnDate().length() < 14
                                ? "" : tx.getEzcTxnDate().substring(8, 14);

                        ezcTXNData.setDate(txnDate);
                        ezcTXNData.setTerminalTXNNumber(txnTime);
                        ezcTXNData.setTime(txnTime);
                        txnDataList.add(ezcTXNData);
                    } else {
                        txAmountCashReserve = txAmountCashReserve + tx.getAmount();
                        txCountCashReserve = txCountCashReserve + 1;

                        EzcTXNData ezcTXNDataReserve = new EzcTXNData();
                        ezcTXNDataReserve.setTxnType("CashTopUp");
                        ezcTXNDataReserve.setAmount(String.valueOf(tx.getAmount()));
                        ezcTXNDataReserve.setCardNumber(tx.getUserId());

                        String txnDate = Strings.isNullOrEmpty(tx.getEzcTxnDate()) ? ""
                                : tx.getEzcTxnDate().substring(0, 8);
                        String txnTime = Strings.isNullOrEmpty(tx.getEzcTxnDate()) || tx.getEzcTxnDate().length() < 14
                                ? "" : tx.getEzcTxnDate().substring(8, 14);

                        ezcTXNDataReserve.setDate(txnDate);
                        ezcTXNDataReserve.setTerminalTXNNumber(txnTime);
                        ezcTXNDataReserve.setTime(txnTime);
                        txnDataList.add(ezcTXNDataReserve);
                    }
                }
            }
        }

        if (ReserveCancels != null) {
            LOGGER.info("ReserveCancels : " + ReserveCancels.size());
            for (ReserveCancel tx : ReserveCancels) {
                if (tx.getStatus().equals("0")) {
                    txAmountCancel = txAmountCancel + tx.getAmount();
                    txCountCancel = txCountCancel + 1;

                    EzcTXNData ezcTXNData = new EzcTXNData();
                    ezcTXNData.setTxnType("CancelCashTopUp");
                    ezcTXNData.setAmount(String.valueOf(tx.getAmount()));
                    ezcTXNData.setCardNumber(tx.getUserId());

                    LOGGER.info("ReserveCancels : txn " + tx.getEzcTxnDate());
                    String txnDate = Strings.isNullOrEmpty(tx.getEzcTxnDate()) ? ""
                            : tx.getEzcTxnDate().substring(0, 8);
                    String txnTime = Strings.isNullOrEmpty(tx.getEzcTxnDate()) || tx.getEzcTxnDate().length() < 14 ? ""
                            : tx.getEzcTxnDate().substring(8, 14);

                    ezcTXNData.setDate(txnDate);
                    ezcTXNData.setTerminalTXNNumber(txnTime);
                    ezcTXNData.setTime(txnTime);
                    txnDataList.add(ezcTXNData);
                }
            }
        }

        if (refundDetails != null) {
            LOGGER.info("refundDetails : " + refundDetails.size());
            for (RefundDetailVo tx : refundDetails) {
                if (tx.getStatus().equals(SystemInstance.TRADE_REFUND_SUCCESS)) {
                    txAmountRefund = txAmountRefund + tx.getAmount();
                    txCountRefund = txCountRefund + 1;

                    EzcTXNData ezcTXNData = new EzcTXNData();
                    ezcTXNData.setTxnType(tx.getEzcCancel() ? "CancelPayment" : "Refund");
                    ezcTXNData.setAmount(String.valueOf(tx.getAmount()));

                    TradeDetail tradeDetail = tradeDetailService.getOne(tx.getOrderId());
                    ezcTXNData.setCardNumber(tradeDetail.getUserId());

                    LOGGER.info("refundDetails : txn " + tx.getEzcTxnDate());
                    String txnDate = Strings.isNullOrEmpty(tx.getEzcTxnDate()) ? ""
                            : tx.getEzcTxnDate().substring(0, 8);
                    String txnTime = Strings.isNullOrEmpty(tx.getEzcTxnDate()) || tx.getEzcTxnDate().length() < 14 ? ""
                            : tx.getEzcTxnDate().substring(8, 14);

                    ezcTXNData.setDate(txnDate);
                    ezcTXNData.setTerminalTXNNumber(tx.getSystemRefundId());
                    ezcTXNData.setTime(txnTime);
                    txnDataList.add(ezcTXNData);
                }
            }
        }

        LOGGER.info("[EZC] ---------- ");
        LOGGER.info("[EZC] settlement trade count: " + txCountTrade);
        LOGGER.info("[EZC] settlement trade amount: " + txAmountTrade);
        LOGGER.info("[EZC] ---------- ");
        LOGGER.info("[EZC] settlement refund count: " + txCountRefund);
        LOGGER.info("[EZC] settlement refund amount: " + txAmountRefund);
        LOGGER.info("[EZC] ---------- ");
        LOGGER.info("[EZC] settlement reserve count: " + txCountReserve);
        LOGGER.info("[EZC] settlement reserve amount: " + txAmountReserve);
        LOGGER.info("[EZC] ---------- ");
        LOGGER.info("[EZC] settlement reserve cancel count: " + txCountCancel);
        LOGGER.info("[EZC] settlement reserve cancel amount: " + txAmountCancel);
        LOGGER.info("[EZC] ---------- ");
        String settlementResult = "TXC:" + txCountTrade + " | TX:" + txAmountTrade + " | RFC:" + txCountRefund
                + " | RF:" + txAmountRefund + " | RSC:" + txCountReserve + " | RS:" + txAmountReserve + " | RCC:"
                + txCountCancel + " | RC:" + txAmountCancel;

        if (txCountRefund == 0 && txCountReserve == 0 && txCountTrade == 0 && txCountCancel == 0) {
            EzCardBatchCheckoutResponse response = new EzCardBatchCheckoutResponse();
            response.setTxRefundAmount(0);
            response.setTxReserveAmount(0);
            response.setTxTradeAmount(0);
            response.setTxCancelAmount(0);

            response.setTxRefundCount(0);
            response.setTxReserveCount(0);
            response.setTxTradeCount(0);
            response.setTxCancelCount(0);

            writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                    "Settlement Nothing", "N/A");
            return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS_0001);
        }

        EzCardBatchCheckout ezCardBatchCheckout = new EzCardBatchCheckout();
        ezCardBatchCheckout.setTerminalId(paymentAccount.getAccount());
        ezCardBatchCheckout.setDeviceId(device.getOwnerDeviceId());

        ezCardBatchCheckout.setTerminalTXNNumber(time);
        ezCardBatchCheckout.setHostSerialNumber(getTime()); // 因之前有嚴重延遲，所以改為送出前當下
        ezCardBatchCheckout.setTime(time);
        ezCardBatchCheckout.setDate(date);
        ezCardBatchCheckout
                .setTotalTXNCount(String.valueOf(txCountRefund + txCountReserve + txCountTrade + txCountCancel));
        ezCardBatchCheckout
                .setTotalAmount(String.valueOf(txAmountTrade + txAmountRefund + txAmountReserve + txAmountCancel));

        ezCardBatchCheckout.setBatchNumber(batchNo);
        EzCardBatchCheckoutResponse response = new Gson().fromJson(doRequest(ezCardBatchCheckout, device.getAesKey()),
                EzCardBatchCheckoutResponse.class);
        response.setTxRefundAmount(txAmountRefund);
        response.setTxReserveAmount(txAmountReserve);
        response.setTxTradeAmount(txAmountTrade);
        response.setTxCancelAmount(txAmountCancel);

        response.setTxRefundCount(txCountRefund);
        response.setTxReserveCount(txCountReserve);
        response.setTxTradeCount(txCountTrade);
        response.setTxCancelCount(txCountCancel);

        if (StringUtils.isNotBlank(response.getNewAESKey())) {
            device.setEzcDongleId(response.getTerminalId());
            device.setAesKey(response.getNewAESKey());

            // if (StringUtils.isNotBlank(response.getHostSerialNumber()))
            // {
            // device.setHostSerialNumber(response.getHostSerialNumber());
            // }
            deviceService.save(device);
        }

        LOGGER.info("[EZC][CHECKOUT] " + new Gson().toJson(ezCardBatchCheckout));
        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                settlementResult, response.getTxnResult() + "|" + deviceId + "|" + batchNo);

        if (response.getTxnResult().equals("Success")) {
            LOGGER.info("[RESPONSE][EZC] execute settlement success. " + deviceId + ". " + device.getBatchNumber()
                    + "..." + batchNo + "... update new batchNo.");
            patchBatchNo(device, batchNo);

            List<EzcTXNCount> txnCountsReceipt = new ArrayList<EzcTXNCount>();
            EzcTXNCount ezcTXNCount = new EzcTXNCount();
            ezcTXNCount.setPaymentCount(String.valueOf(txCountTrade));
            ezcTXNCount.setRefundCount(String.valueOf(txCountRefund));
            ezcTXNCount.setAutoTopUpCount(String.valueOf(txCountAutoReserve));
            ezcTXNCount.setPaymentTotal(String.valueOf(txAmountTrade));
            ezcTXNCount.setRefundTotal(String.valueOf(txAmountRefund));
            ezcTXNCount.setAutoTopUpTotal(String.valueOf(txAmountAutoReserve));

            ezcTXNCount.setCashTopUpCount(String.valueOf(txCountCashReserve));
            ezcTXNCount.setCashTopUpTotal(String.valueOf(txAmountCashReserve));
            ezcTXNCount.setCancelReserveCount(String.valueOf(txCountCancel));
            ezcTXNCount.setCancelReserveTotal(String.valueOf(txAmountCancel));

            txnCountsReceipt.add(ezcTXNCount);

            LOGGER.info("[EZC][Settlement][Print][input] " + new Gson().toJson(response));
            EzcSettlementReceive ezcSettlementReceive = new Gson().fromJson(new Gson().toJson(response),
                    EzcSettlementReceive.class);
            ezcSettlementReceive.setDeviceId(device.getOwnerDeviceId());
            ezcSettlementReceive.setTerminalId(paymentAccount.getAccount());
            ezcSettlementReceive.setPrintId("01");
            ezcSettlementReceive.setTxnCount(txnCountsReceipt);
            ezcSettlementReceive.setTxnList(txnDataList);

            ezcSettlementReceive.setDate(date);
            ezcSettlementReceive.setTime(time);
            ezcSettlementReceive.setDeviceNumber(paymentAccount.getAccount() + device.getOwnerDeviceId());
            ezcSettlementReceive.setBatchNumber(batchNo);

            String printResponse = doRequest(ezcSettlementReceive, device.getAesKey());
            LOGGER.info("[EZC][Settlement][Print][output] " + printResponse);
        }
        // todo settlement fail send email
        else {
            MailInformation mailInformation = new MailInformation();
            mailInformation.setMailTo(new String[]{"alex@intella.co"});
            mailInformation.setSubject("ezc settlement failed");

            Context ctx = new Context();
            ctx.setVariable("username", merchant.getAccountId());
            ctx.setVariable("batchNo", batchNo);
            ctx.setVariable("ezc response", new Gson().toJson(response));

            try {
                mailService.send(mailInformation, ctx, "resetPasswordMail");
            } catch (MessagingException e) {
                e.printStackTrace();
            }

        }

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    private String doSettlement(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader,
                                String batchNum) {

        String date = getDate();
        String time = getTime();

        String deviceId = device.getOwnerDeviceId();
        String batchNo = batchNum == null ? getBatchNumber(date, String.format("%02d", device.getBatchNumber()))
                : batchNum;

        LOGGER.info("[REQUEST][EZC][AutoSettlement][" + paymentAccount.getAccount() + "][" + deviceId + "][" + batchNo
                + "]");

        List<TradeDetailVo> tradeDetails = tradeDetailService.listGroupEZC(paymentAccount.getAccount(), deviceId,
                batchNo);
        List<ReserveDetail> reserveDetails = reserveDetailService.listGroupEZC(paymentAccount.getAccount(), deviceId,
                batchNo);
        List<RefundDetailVo> refundDetails = refundDetailService.listGroupEZC(paymentAccount.getAccount(), deviceId,
                batchNo);
        List<ReserveCancel> ReserveCancels = reserveCancelService.listGroupEZC(paymentAccount.getAccount(), deviceId,
                batchNo);

        long txCountTrade = 0;
        long txCountRefund = 0;
        long txCountReserve = 0;
        long txCountCancel = 0;
        long txCountAutoReserve = 0;
        long txCountCashReserve = 0;

        long txAmountTrade = 0;
        long txAmountRefund = 0;
        long txAmountReserve = 0;
        long txAmountCancel = 0;
        long txAmountAutoReserve = 0;
        long txAmountCashReserve = 0;

        List<EzcTXNData> txnDataList = new ArrayList<EzcTXNData>();

        if (tradeDetails != null) {
            LOGGER.info("tradeDetails : " + tradeDetails.size());
            for (TradeDetailVo tx : tradeDetails) {
                if (tx.getStatus().equals(SystemInstance.TRADE_SUCCESS)) {
                    txAmountTrade = txAmountTrade + tx.getPayment();
                    txCountTrade = txCountTrade + 1;

                    EzcTXNData ezcTXNData = new EzcTXNData();
                    ezcTXNData.setTxnType("Payment");
                    ezcTXNData.setAmount(String.valueOf(tx.getPayment()));
                    ezcTXNData.setCardNumber(tx.getUserId());

                    LOGGER.info("tradeDetails : txn " + tx.getEzcTxnDate());
                    ezcTXNData.setDate(
                            Strings.isNullOrEmpty(tx.getEzcTxnDate()) ? "" : tx.getEzcTxnDate().substring(0, 8));
                    ezcTXNData.setTerminalTXNNumber(tx.getSystemOrderId());
                    ezcTXNData.setTime(Strings.isNullOrEmpty(tx.getEzcTxnDate()) || tx.getEzcTxnDate().length() < 14
                            ? "" : tx.getEzcTxnDate().substring(8, 14));
                    txnDataList.add(ezcTXNData);
                }
            }
        }

        if (reserveDetails != null) {
            LOGGER.info("reserveDetails : " + reserveDetails.size());
            for (ReserveDetail tx : reserveDetails) {
                if (tx.getStatus().equals("0")) {
                    txAmountReserve = txAmountReserve + tx.getAmount();
                    txCountReserve = txCountReserve + 1;

                    LOGGER.info("reserveDetails : txn " + tx.getEzcTxnDate());

                    if (tx.getIsAuto() == 1) {
                        txAmountAutoReserve = txAmountAutoReserve + tx.getAmount();
                        txCountAutoReserve = txCountAutoReserve + 1;
                        EzcTXNData ezcTXNData = new EzcTXNData();
                        ezcTXNData.setTxnType("AutoCashTopUp");
                        ezcTXNData.setAmount(String.valueOf(tx.getAmount()));
                        ezcTXNData.setCardNumber(tx.getUserId());

                        String txnDate = Strings.isNullOrEmpty(tx.getEzcTxnDate()) ? ""
                                : tx.getEzcTxnDate().substring(0, 8);
                        String txnTime = Strings.isNullOrEmpty(tx.getEzcTxnDate()) || tx.getEzcTxnDate().length() < 14
                                ? "" : tx.getEzcTxnDate().substring(8, 14);

                        ezcTXNData.setDate(txnDate);
                        ezcTXNData.setTerminalTXNNumber(txnTime);
                        ezcTXNData.setTime(txnTime);
                        txnDataList.add(ezcTXNData);
                    } else {
                        txAmountCashReserve = txAmountCashReserve + tx.getAmount();
                        txCountCashReserve = txCountCashReserve + 1;

                        EzcTXNData ezcTXNDataReserve = new EzcTXNData();
                        ezcTXNDataReserve.setTxnType("CashTopUp");
                        ezcTXNDataReserve.setAmount(String.valueOf(tx.getAmount()));
                        ezcTXNDataReserve.setCardNumber(tx.getUserId());

                        String txnDate = Strings.isNullOrEmpty(tx.getEzcTxnDate()) ? ""
                                : tx.getEzcTxnDate().substring(0, 8);
                        String txnTime = Strings.isNullOrEmpty(tx.getEzcTxnDate()) || tx.getEzcTxnDate().length() < 14
                                ? "" : tx.getEzcTxnDate().substring(8, 14);

                        ezcTXNDataReserve.setDate(txnDate);
                        ezcTXNDataReserve.setTerminalTXNNumber(txnTime);
                        ezcTXNDataReserve.setTime(txnTime);
                        txnDataList.add(ezcTXNDataReserve);
                    }
                }
            }
        }

        if (ReserveCancels != null) {
            LOGGER.info("ReserveCancels : " + ReserveCancels.size());
            for (ReserveCancel tx : ReserveCancels) {
                if (tx.getStatus().equals("0")) {
                    txAmountCancel = txAmountCancel + tx.getAmount();
                    txCountCancel = txCountCancel + 1;

                    EzcTXNData ezcTXNData = new EzcTXNData();
                    ezcTXNData.setTxnType("CancelCashTopUp");
                    ezcTXNData.setAmount(String.valueOf(tx.getAmount()));
                    ezcTXNData.setCardNumber(tx.getUserId());

                    LOGGER.info("ReserveCancels : txn " + tx.getEzcTxnDate());
                    String txnDate = Strings.isNullOrEmpty(tx.getEzcTxnDate()) ? ""
                            : tx.getEzcTxnDate().substring(0, 8);
                    String txnTime = Strings.isNullOrEmpty(tx.getEzcTxnDate()) || tx.getEzcTxnDate().length() < 14 ? ""
                            : tx.getEzcTxnDate().substring(8, 14);

                    ezcTXNData.setDate(txnDate);
                    ezcTXNData.setTerminalTXNNumber(txnTime);
                    ezcTXNData.setTime(txnTime);
                    txnDataList.add(ezcTXNData);
                }
            }
        }

        if (refundDetails != null) {
            LOGGER.info("refundDetails : " + refundDetails.size());
            for (RefundDetailVo tx : refundDetails) {
                if (tx.getStatus().equals(SystemInstance.TRADE_REFUND_SUCCESS)) {
                    txAmountRefund = txAmountRefund + tx.getAmount();
                    txCountRefund = txCountRefund + 1;

                    EzcTXNData ezcTXNData = new EzcTXNData();
                    ezcTXNData.setTxnType(tx.getEzcCancel() ? "CancelPayment" : "Refund");
                    ezcTXNData.setAmount(String.valueOf(tx.getAmount()));

                    TradeDetail tradeDetail = tradeDetailService.getOne(tx.getOrderId());
                    ezcTXNData.setCardNumber(tradeDetail.getUserId());

                    LOGGER.info("refundDetails : txn " + tx.getEzcTxnDate());
                    String txnDate = Strings.isNullOrEmpty(tx.getEzcTxnDate()) ? ""
                            : tx.getEzcTxnDate().substring(0, 8);
                    String txnTime = Strings.isNullOrEmpty(tx.getEzcTxnDate()) || tx.getEzcTxnDate().length() < 14 ? ""
                            : tx.getEzcTxnDate().substring(8, 14);

                    ezcTXNData.setDate(txnDate);
                    ezcTXNData.setTerminalTXNNumber(tx.getSystemRefundId());
                    ezcTXNData.setTime(txnTime);
                    txnDataList.add(ezcTXNData);
                }
            }
        }

        LOGGER.info("[EZC] ---------- ");
        LOGGER.info("[EZC] settlement trade count: " + txCountTrade);
        LOGGER.info("[EZC] settlement trade amount: " + txAmountTrade);
        LOGGER.info("[EZC] ---------- ");
        LOGGER.info("[EZC] settlement refund count: " + txCountRefund);
        LOGGER.info("[EZC] settlement refund amount: " + txAmountRefund);
        LOGGER.info("[EZC] ---------- ");
        LOGGER.info("[EZC] settlement reserve count: " + txCountReserve);
        LOGGER.info("[EZC] settlement reserve amount: " + txAmountReserve);
        LOGGER.info("[EZC] ---------- ");
        LOGGER.info("[EZC] settlement reserve cancel count: " + txCountCancel);
        LOGGER.info("[EZC] settlement reserve cancel amount: " + txAmountCancel);
        LOGGER.info("[EZC] ---------- ");
        String settlementResult = "TXC:" + txCountTrade + " | TX:" + txAmountTrade + " | RFC:" + txCountRefund
                + " | RF:" + txAmountRefund + " | RSC:" + txCountReserve + " | RS:" + txAmountReserve + " | RCC:"
                + txCountCancel + " | RC:" + txAmountCancel;

        if (txCountRefund == 0 && txCountReserve == 0 && txCountTrade == 0 && txCountCancel == 0) {
            EzCardBatchCheckoutResponse response = new EzCardBatchCheckoutResponse();
            response.setTxRefundAmount(0);
            response.setTxReserveAmount(0);
            response.setTxTradeAmount(0);
            response.setTxCancelAmount(0);

            response.setTxRefundCount(0);
            response.setTxReserveCount(0);
            response.setTxTradeCount(0);
            response.setTxCancelCount(0);

            writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                    "Settlement Nothing", "N/A");
            return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS_0001);
        }

        EzCardBatchCheckout ezCardBatchCheckout = new EzCardBatchCheckout();
        ezCardBatchCheckout.setTerminalId(paymentAccount.getAccount());
        ezCardBatchCheckout.setDeviceId(device.getOwnerDeviceId());

        ezCardBatchCheckout.setTerminalTXNNumber(time);
        ezCardBatchCheckout.setHostSerialNumber(getTime()); // 因之前有嚴重延遲，所以改為送出前當下
        ezCardBatchCheckout.setTime(time);
        ezCardBatchCheckout.setDate(date);
        ezCardBatchCheckout
                .setTotalTXNCount(String.valueOf(txCountRefund + txCountReserve + txCountTrade + txCountCancel));
        ezCardBatchCheckout
                .setTotalAmount(String.valueOf(txAmountTrade + txAmountRefund + txAmountReserve + txAmountCancel));

        ezCardBatchCheckout.setBatchNumber(batchNo);
        EzCardBatchCheckoutResponse response = new Gson().fromJson(doRequest(ezCardBatchCheckout, device.getAesKey()),
                EzCardBatchCheckoutResponse.class);
        response.setTxRefundAmount(txAmountRefund);
        response.setTxReserveAmount(txAmountReserve);
        response.setTxTradeAmount(txAmountTrade);
        response.setTxCancelAmount(txAmountCancel);

        response.setTxRefundCount(txCountRefund);
        response.setTxReserveCount(txCountReserve);
        response.setTxTradeCount(txCountTrade);
        response.setTxCancelCount(txCountCancel);

        if (StringUtils.isNotBlank(response.getNewAESKey())) {
            device.setEzcDongleId(response.getTerminalId());
            device.setAesKey(response.getNewAESKey());

            // if (StringUtils.isNotBlank(response.getHostSerialNumber()))
            // {
            // device.setHostSerialNumber(response.getHostSerialNumber());
            // }
            deviceService.save(device);
        }

        LOGGER.info("[EZC][CHECKOUT] " + new Gson().toJson(ezCardBatchCheckout));
        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                settlementResult, response.getTxnResult() + "|" + deviceId + "|" + batchNo);

        if (response.getTxnResult().equals("Success")) {
            LOGGER.info("[RESPONSE][EZC] execute settlement success. " + deviceId + ". " + device.getBatchNumber()
                    + "..." + batchNo + "... update new batchNo.");
            patchBatchNo(device, batchNo);

            List<EzcTXNCount> txnCountsReceipt = new ArrayList<EzcTXNCount>();
            EzcTXNCount ezcTXNCount = new EzcTXNCount();
            ezcTXNCount.setPaymentCount(String.valueOf(txCountTrade));
            ezcTXNCount.setRefundCount(String.valueOf(txCountRefund));
            ezcTXNCount.setAutoTopUpCount(String.valueOf(txCountAutoReserve));
            ezcTXNCount.setPaymentTotal(String.valueOf(txAmountTrade));
            ezcTXNCount.setRefundTotal(String.valueOf(txAmountRefund));
            ezcTXNCount.setAutoTopUpTotal(String.valueOf(txAmountAutoReserve));

            ezcTXNCount.setCashTopUpCount(String.valueOf(txCountCashReserve));
            ezcTXNCount.setCashTopUpTotal(String.valueOf(txAmountCashReserve));
            ezcTXNCount.setCancelReserveCount(String.valueOf(txCountCancel));
            ezcTXNCount.setCancelReserveTotal(String.valueOf(txAmountCancel));

            txnCountsReceipt.add(ezcTXNCount);

            LOGGER.info("[EZC][Settlement][Print][input] " + new Gson().toJson(response));
            EzcSettlementReceive ezcSettlementReceive = new Gson().fromJson(new Gson().toJson(response),
                    EzcSettlementReceive.class);
            ezcSettlementReceive.setDeviceId(device.getOwnerDeviceId());
            ezcSettlementReceive.setTerminalId(paymentAccount.getAccount());
            ezcSettlementReceive.setPrintId("01");
            ezcSettlementReceive.setTxnCount(txnCountsReceipt);
            ezcSettlementReceive.setTxnList(txnDataList);

            ezcSettlementReceive.setDate(date);
            ezcSettlementReceive.setTime(time);
            ezcSettlementReceive.setDeviceNumber(paymentAccount.getAccount() + device.getOwnerDeviceId());
            ezcSettlementReceive.setBatchNumber(batchNo);

            String printResponse = doRequest(ezcSettlementReceive, device.getAesKey());
            LOGGER.info("[EZC][Settlement][Print][output] " + printResponse);
        }
        // todo settlement fail send email
        else {
            MailInformation mailInformation = new MailInformation();
            mailInformation.setMailTo(new String[]{"alex@intella.co"});
            mailInformation.setSubject("ezc settlement failed");

            Context ctx = new Context();
            ctx.setVariable("deviceId", deviceId);
            ctx.setVariable("batchNo", batchNo);
            ctx.setVariable("ezc response", new Gson().toJson(response));

            try {
                mailService.send(mailInformation, ctx, "resetPasswordMail");
            } catch (MessagingException e) {
                e.printStackTrace();
            }

        }

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    private String parkCheckout(Device device, Merchant merchant, PaymentAccount paymentAccount,
                                RequestHeader requestHeader, String batchNum) {

        String date = getDate();
        String time = getTime();

        String deviceId = device.getOwnerDeviceId();
        String batchNo = batchNum == null ? getBatchNumber(date, String.format("%02d", device.getBatchNumber()))
                : batchNum;

        LOGGER.info("[REQUEST][EZC][Settlement][" + merchant.getAccountId() + "][" + deviceId + "][" + batchNo + "]");

        List<TradeDetailVo> tradeDetails = tradeDetailService.listByBatchNo(merchant.getAccountId(), deviceId, batchNo);
        List<ReserveDetail> reserveDetails = reserveDetailService.listByBatchNo(merchant.getAccountId(), deviceId,
                batchNo);
        List<RefundDetailVo> refundDetails = refundDetailService.listByBatchNo(merchant.getAccountId(), deviceId,
                batchNo);
        List<ReserveCancel> ReserveCancels = reserveCancelService
                .listByMerchantIdAndDeviceIdAndBatchNo(merchant.getAccountId(), deviceId, batchNo);

        long txCountTrade = 0;
        long txCountRefund = 0;
        long txCountReserve = 0;
        long txCountCancel = 0;
        long txCountAutoReserve = 0;
        long txCountCashReserve = 0;

        long txAmountTrade = 0;
        long txAmountRefund = 0;
        long txAmountReserve = 0;
        long txAmountCancel = 0;
        long txAmountAutoReserve = 0;
        long txAmountCashReserve = 0;

        List<EzcTXNData> txnDataList = new ArrayList<EzcTXNData>();

        if (tradeDetails != null) {
            LOGGER.info("tradeDetails : " + tradeDetails.size());
            for (TradeDetailVo tx : tradeDetails) {
                if (tx.getStatus().equals(SystemInstance.TRADE_SUCCESS)) {
                    txAmountTrade = txAmountTrade + tx.getPayment();
                    txCountTrade = txCountTrade + 1;

                    EzcTXNData ezcTXNData = new EzcTXNData();
                    ezcTXNData.setTxnType("Payment");
                    ezcTXNData.setAmount(String.valueOf(tx.getPayment()));
                    ezcTXNData.setCardNumber(tx.getUserId());

                    LOGGER.info("tradeDetails : txn " + tx.getEzcTxnDate());
                    ezcTXNData.setDate(
                            Strings.isNullOrEmpty(tx.getEzcTxnDate()) ? "" : tx.getEzcTxnDate().substring(0, 8));
                    ezcTXNData.setTerminalTXNNumber(tx.getSystemOrderId());
                    ezcTXNData.setTime(Strings.isNullOrEmpty(tx.getEzcTxnDate()) || tx.getEzcTxnDate().length() < 14
                            ? "" : tx.getEzcTxnDate().substring(8, 14));
                    txnDataList.add(ezcTXNData);
                }
            }
        }

        if (reserveDetails != null) {
            LOGGER.info("reserveDetails : " + reserveDetails.size());
            for (ReserveDetail tx : reserveDetails) {
                if (tx.getStatus().equals("0")) {
                    txAmountReserve = txAmountReserve + tx.getAmount();
                    txCountReserve = txCountReserve + 1;

                    LOGGER.info("reserveDetails : txn " + tx.getEzcTxnDate());

                    if (tx.getIsAuto() == 1) {
                        txAmountAutoReserve = txAmountAutoReserve + tx.getAmount();
                        txCountAutoReserve = txCountAutoReserve + 1;
                        EzcTXNData ezcTXNData = new EzcTXNData();
                        ezcTXNData.setTxnType("AutoCashTopUp");
                        ezcTXNData.setAmount(String.valueOf(tx.getAmount()));
                        ezcTXNData.setCardNumber(tx.getUserId());

                        String txnDate = Strings.isNullOrEmpty(tx.getEzcTxnDate()) ? ""
                                : tx.getEzcTxnDate().substring(0, 8);
                        String txnTime = Strings.isNullOrEmpty(tx.getEzcTxnDate()) || tx.getEzcTxnDate().length() < 14
                                ? "" : tx.getEzcTxnDate().substring(8, 14);

                        ezcTXNData.setDate(txnDate);
                        ezcTXNData.setTerminalTXNNumber(txnTime);
                        ezcTXNData.setTime(txnTime);
                        txnDataList.add(ezcTXNData);
                    } else {
                        txAmountCashReserve = txAmountCashReserve + tx.getAmount();
                        txCountCashReserve = txCountCashReserve + 1;

                        EzcTXNData ezcTXNDataReserve = new EzcTXNData();
                        ezcTXNDataReserve.setTxnType("CashTopUp");
                        ezcTXNDataReserve.setAmount(String.valueOf(tx.getAmount()));
                        ezcTXNDataReserve.setCardNumber(tx.getUserId());

                        String txnDate = Strings.isNullOrEmpty(tx.getEzcTxnDate()) ? ""
                                : tx.getEzcTxnDate().substring(0, 8);
                        String txnTime = Strings.isNullOrEmpty(tx.getEzcTxnDate()) || tx.getEzcTxnDate().length() < 14
                                ? "" : tx.getEzcTxnDate().substring(8, 14);

                        ezcTXNDataReserve.setDate(txnDate);
                        ezcTXNDataReserve.setTerminalTXNNumber(txnTime);
                        ezcTXNDataReserve.setTime(txnTime);
                        txnDataList.add(ezcTXNDataReserve);
                    }
                }
            }
        }

        if (ReserveCancels != null) {
            LOGGER.info("ReserveCancels : " + ReserveCancels.size());
            for (ReserveCancel tx : ReserveCancels) {
                if (tx.getStatus().equals("0")) {
                    txAmountCancel = txAmountCancel + tx.getAmount();
                    txCountCancel = txCountCancel + 1;

                    EzcTXNData ezcTXNData = new EzcTXNData();
                    ezcTXNData.setTxnType("CancelCashTopUp");
                    ezcTXNData.setAmount(String.valueOf(tx.getAmount()));
                    ezcTXNData.setCardNumber(tx.getUserId());

                    LOGGER.info("ReserveCancels : txn " + tx.getEzcTxnDate());
                    String txnDate = Strings.isNullOrEmpty(tx.getEzcTxnDate()) ? ""
                            : tx.getEzcTxnDate().substring(0, 8);
                    String txnTime = Strings.isNullOrEmpty(tx.getEzcTxnDate()) || tx.getEzcTxnDate().length() < 14 ? ""
                            : tx.getEzcTxnDate().substring(8, 14);

                    ezcTXNData.setDate(txnDate);
                    ezcTXNData.setTerminalTXNNumber(txnTime);
                    ezcTXNData.setTime(txnTime);
                    txnDataList.add(ezcTXNData);
                }
            }
        }

        if (refundDetails != null) {
            LOGGER.info("refundDetails : " + refundDetails.size());
            for (RefundDetailVo tx : refundDetails) {
                if (tx.getStatus().equals(SystemInstance.TRADE_REFUND_SUCCESS)) {
                    txAmountRefund = txAmountRefund + tx.getAmount();
                    txCountRefund = txCountRefund + 1;

                    EzcTXNData ezcTXNData = new EzcTXNData();
                    ezcTXNData.setTxnType(tx.getEzcCancel() ? "CancelPayment" : "Refund");
                    ezcTXNData.setAmount(String.valueOf(tx.getAmount()));

                    TradeDetail tradeDetail = tradeDetailService.getOne(tx.getOrderId());
                    ezcTXNData.setCardNumber(tradeDetail.getUserId());

                    LOGGER.info("refundDetails : txn " + tx.getEzcTxnDate());
                    String txnDate = Strings.isNullOrEmpty(tx.getEzcTxnDate()) ? ""
                            : tx.getEzcTxnDate().substring(0, 8);
                    String txnTime = Strings.isNullOrEmpty(tx.getEzcTxnDate()) || tx.getEzcTxnDate().length() < 14 ? ""
                            : tx.getEzcTxnDate().substring(8, 14);

                    ezcTXNData.setDate(txnDate);
                    ezcTXNData.setTerminalTXNNumber(tx.getSystemRefundId());
                    ezcTXNData.setTime(txnTime);
                    txnDataList.add(ezcTXNData);
                }
            }
        }

        LOGGER.info("[EZC] ---------- ");
        LOGGER.info("[EZC] settlement trade count: " + txCountTrade);
        LOGGER.info("[EZC] settlement trade amount: " + txAmountTrade);
        LOGGER.info("[EZC] ---------- ");
        LOGGER.info("[EZC] settlement refund count: " + txCountRefund);
        LOGGER.info("[EZC] settlement refund amount: " + txAmountRefund);
        LOGGER.info("[EZC] ---------- ");
        LOGGER.info("[EZC] settlement reserve count: " + txCountReserve);
        LOGGER.info("[EZC] settlement reserve amount: " + txAmountReserve);
        LOGGER.info("[EZC] ---------- ");
        LOGGER.info("[EZC] settlement reserve cancel count: " + txCountCancel);
        LOGGER.info("[EZC] settlement reserve cancel amount: " + txAmountCancel);
        LOGGER.info("[EZC] ---------- ");
        String settlementResult = "TXC:" + txCountTrade + " | TX:" + txAmountTrade + " | RFC:" + txCountRefund
                + " | RF:" + txAmountRefund + " | RSC:" + txCountReserve + " | RS:" + txAmountReserve + " | RCC:"
                + txCountCancel + " | RC:" + txAmountCancel;

        if (txCountRefund == 0 && txCountReserve == 0 && txCountTrade == 0 && txCountCancel == 0) {
            EzCardBatchCheckoutResponse response = new EzCardBatchCheckoutResponse();
            response.setTxRefundAmount(0);
            response.setTxReserveAmount(0);
            response.setTxTradeAmount(0);
            response.setTxCancelAmount(0);

            response.setTxRefundCount(0);
            response.setTxReserveCount(0);
            response.setTxTradeCount(0);
            response.setTxCancelCount(0);

            writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                    "Settlement Nothing", "N/A");
            return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS_0001);
        }

        EzCardBatchCheckout ezCardBatchCheckout = new EzCardBatchCheckout();
        ezCardBatchCheckout.setTerminalId(paymentAccount.getAccount());
        ezCardBatchCheckout.setDeviceId(device.getOwnerDeviceId());

        ezCardBatchCheckout.setTerminalTXNNumber(time);
        ezCardBatchCheckout
                .setHostSerialNumber(String.format("%06d", (Long.valueOf(device.getHostSerialNumber()) + 1)));
        ezCardBatchCheckout.setTime(time);
        ezCardBatchCheckout.setDate(date);
        ezCardBatchCheckout
                .setTotalTXNCount(String.valueOf(txCountRefund + txCountReserve + txCountTrade + txCountCancel));
        ezCardBatchCheckout
                .setTotalAmount(String.valueOf(txAmountTrade + txAmountRefund + txAmountReserve + txAmountCancel));

        ezCardBatchCheckout.setBatchNumber(batchNo);

        System.out.println("REQQQQQQQ!!" + ezCardBatchCheckout.toString());

        EzCardBatchCheckoutResponse response = new Gson().fromJson(parkRequest(ezCardBatchCheckout, device.getAesKey()),
                EzCardBatchCheckoutResponse.class);
        response.setTxRefundAmount(txAmountRefund);
        response.setTxReserveAmount(txAmountReserve);
        response.setTxTradeAmount(txAmountTrade);
        response.setTxCancelAmount(txAmountCancel);

        response.setTxRefundCount(txCountRefund);
        response.setTxReserveCount(txCountReserve);
        response.setTxTradeCount(txCountTrade);
        response.setTxCancelCount(txCountCancel);

        if (StringUtils.isNotBlank(response.getNewAESKey())) {
            device.setEzcDongleId(response.getDongleDeviceID());
            device.setAesKey(response.getNewAESKey());

            if (StringUtils.isNotBlank(response.getHostSerialNumber())) {
                device.setHostSerialNumber(response.getHostSerialNumber());
            }
            deviceService.save(device);
        }

        LOGGER.info("[EZC][CHECKOUT] " + new Gson().toJson(ezCardBatchCheckout));
        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                settlementResult, response.getTxnResult() + "|" + deviceId + "|" + batchNo);

        if (response.getTxnResult().equals("Success")) {
            LOGGER.info("[RESPONSE][EZC] execute settlement success. " + deviceId + ". " + device.getBatchNumber()
                    + "..." + batchNo + "... update new batchNo.");
            patchBatchNo(device, batchNo);

            List<EzcTXNCount> txnCountsReceipt = new ArrayList<EzcTXNCount>();
            EzcTXNCount ezcTXNCount = new EzcTXNCount();
            ezcTXNCount.setPaymentCount(String.valueOf(txCountTrade));
            ezcTXNCount.setRefundCount(String.valueOf(txCountRefund));
            ezcTXNCount.setAutoTopUpCount(String.valueOf(txCountAutoReserve));
            ezcTXNCount.setPaymentTotal(String.valueOf(txAmountTrade));
            ezcTXNCount.setRefundTotal(String.valueOf(txAmountRefund));
            ezcTXNCount.setAutoTopUpTotal(String.valueOf(txAmountAutoReserve));

            ezcTXNCount.setCashTopUpCount(String.valueOf(txCountCashReserve));
            ezcTXNCount.setCashTopUpTotal(String.valueOf(txAmountCashReserve));
            ezcTXNCount.setCancelReserveCount(String.valueOf(txCountCancel));
            ezcTXNCount.setCancelReserveTotal(String.valueOf(txAmountCancel));

            txnCountsReceipt.add(ezcTXNCount);

            LOGGER.info("[EZC][Settlement][Print][input] " + new Gson().toJson(response));
            EzcSettlementReceive ezcSettlementReceive = new Gson().fromJson(new Gson().toJson(response),
                    EzcSettlementReceive.class);
            ezcSettlementReceive.setDeviceId(device.getOwnerDeviceId());
            ezcSettlementReceive.setTerminalId(paymentAccount.getAccount());
            ezcSettlementReceive.setPrintId("01");
            ezcSettlementReceive.setTxnCount(txnCountsReceipt);
            ezcSettlementReceive.setTxnList(txnDataList);

            ezcSettlementReceive.setDate(date);
            ezcSettlementReceive.setTime(time);
            ezcSettlementReceive.setDeviceNumber(paymentAccount.getAccount() + device.getOwnerDeviceId());
            ezcSettlementReceive.setBatchNumber(batchNo);

            String printResponse = doRequest(ezcSettlementReceive, device.getAesKey());
            LOGGER.info("[EZC][Settlement][Print][output] " + printResponse);
        }
        // todo settlement fail send email
        else {
            MailInformation mailInformation = new MailInformation();
            mailInformation.setMailTo(new String[]{"alex@intella.co"});
            mailInformation.setSubject("ezc settlement failed");

            Context ctx = new Context();
            ctx.setVariable("username", merchant.getAccountId());
            ctx.setVariable("batchNo", batchNo);
            ctx.setVariable("ezc response", new Gson().toJson(response));

            try {
                mailService.send(mailInformation, ctx, "resetPasswordMail");
            } catch (MessagingException e) {
                e.printStackTrace();
            }

        }

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    private void patchBatchNo(Device device, String batchNo) {

//        if (DateTime.now().toString("yyMMdd").equals(batchNo.substring(0,6))) {
        LOGGER.info("==========Device batchNo===============" + batchNo);
        LOGGER.info("==========Device BatchNum===============" + device.getBatchNumber());
        LOGGER.info("==========Device BatchNum===============" + (DateTime.now().toString("yyMMdd") + String.format("%02d", device.getBatchNumber())));
        String batchno = DateTime.now().toString("yyMMdd") + String.format("%02d", device.getBatchNumber());
        if (batchno.equals(batchNo)) {
            LOGGER.info("==========Device true===============");
            LOGGER.info("==========Device batchNo===============" + batchNo);
            LOGGER.info("==========Device batchno===============" + batchno);
//	        if (DateTime.now().toString("yyMMdd").equals(batchNo.substring(0,6))) {
            long batch = device.getBatchNumber();
            if (batch >= 99) {
                batch = 0;
            } else {
                batch = batch + 1;
            }
            device.setBatchNumber(batch);
            deviceService.save(device);
        }
//        }
    }

    private String getOrderQuery(String userId, Merchant merchant, RequestHeader requestHeader) {
        LOGGER.info("[REQUEST][EZC] list order by user cardId : " + userId);
        List<TradeDetailVo> tradeDetailVos = tradeDetailService.listByUserId(merchant.getAccountId(), userId);
        EzCardOrderQueryResponse response = new EzCardOrderQueryResponse();

        List<EzcTradeDetailVo> ezcTradeDetailVos = new ArrayList<EzcTradeDetailVo>();
        for (TradeDetailVo tx : tradeDetailVos) {

            boolean isRefund = StringUtils.isNotBlank(tx.getRefundStatus())
                    && tx.getRefundStatus().equals("Refund success");
            if (tx.getPayment() > 0 && !isRefund) {
                EzcTradeDetailVo ezcTx = new EzcTradeDetailVo();
                ezcTx.setCreateDate(tx.getCreateDate());
                ezcTx.setOrderId(tx.getOrderId());
                ezcTx.setPayment(tx.getPayment());

                ezcTradeDetailVos.add(ezcTx);
            }
        }

        response.setList(ezcTradeDetailVos);
        response.setErrorCode("00000");

        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                "OrderQuery : " + userId + "|" + merchant.getAccountId(), "N/A");

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    @Override
    public String getUserId(PaymentAccount paymentAccount, Device device) {
        LOGGER.info("[REQUEST][EZC] getUserId : " + device.getOwnerDeviceId() + ", " + paymentAccount.getAccount());
        String userId = "-1";

        EzCardIdQuery ezCardIdQuery = new EzCardIdQuery(device, paymentAccount);

        EzCardIdQueryResponse idQueryResponse = new Gson().fromJson(doRequest(ezCardIdQuery, device.getAesKey()),
                EzCardIdQueryResponse.class);
        if (idQueryResponse.getTxnResult().equals("Success")) {
            userId = idQueryResponse.getCardNumber();
        }

        if (StringUtils.isNotBlank(idQueryResponse.getNewAESKey())) {
            device.setAesKey(idQueryResponse.getNewAESKey());
            deviceService.save(device);
        }

        if (Strings.isNullOrEmpty(userId) || userId.equals("-1")) {
            int tryCount = 0;
            while ((Strings.isNullOrEmpty(userId) || userId.equals("-1")) && tryCount < 10) {

                idQueryResponse = new Gson().fromJson(doRequest(ezCardIdQuery, device.getAesKey()),
                        EzCardIdQueryResponse.class);
                if (idQueryResponse.getTxnResult().equals("Success")) {
                    userId = idQueryResponse.getCardNumber();
                }

                if (StringUtils.isNotBlank(idQueryResponse.getNewAESKey())) {
                    device.setAesKey(idQueryResponse.getNewAESKey());
                    // device.setHostSerialNumber(String.valueOf(Long.valueOf(ezCardIdQuery.getHostSerialNumber())+1));
                    deviceService.save(device);
                }

                if (!(Strings.isNullOrEmpty(userId) || userId.equals("-1"))) {
                    break;
                }
                tryCount++;
                LOGGER.info("[EZC] try to get card id : " + userId + ", " + tryCount + " times...");
            }
        }

        return userId;
    }
    //
    // private String getCardIDForParking(PaymentAccount paymentAccount, Device
    // device) {
    // LOGGER.info("[REQUEST][EZC] getUserId : " + device.getOwnerDeviceId() +
    // ", " + paymentAccount.getAccount());
    // String userId = "-1";
    //
    // String date = getDate();
    // String time = getTime();
    //
    // EzCardEDCAQueryReq edcaQueryReq = new EzCardEDCAQueryReq();
    // edcaQueryReq.setTerminalId(paymentAccount.getAccount());
    // edcaQueryReq.setDeviceId(device.getOwnerDeviceId());
    //
    // edcaQueryReq.setTerminalTXNNumber(time);
    // edcaQueryReq.setHostSerialNumber(String.format("%06d",
    // (Long.valueOf(device.getHostSerialNumber())+1)));
    // edcaQueryReq.setTime(time);
    // edcaQueryReq.setDate(date);
    //
    // EzCardEDCAQueryResponse edcaQueryResponse = new
    // Gson().fromJson(doRequest(edcaQueryReq, device.getAesKey()),
    // EzCardEDCAQueryResponse.class);
    // if(edcaQueryResponse.getTxnResult().equals("Success")) {
    // userId = edcaQueryResponse.getEzCardId();
    // }
    //
    // if(StringUtils.isNotBlank(edcaQueryResponse.getNewAESKey())) {
    // device.setAesKey(edcaQueryResponse.getNewAESKey());
    // deviceService.save(device);
    // }
    //
    // if(Strings.isNullOrEmpty(userId) || userId.equals("-1")) {
    // int tryCount = 0;
    // while((Strings.isNullOrEmpty(userId) || userId.equals("-1")) && tryCount
    // < 10) {
    //
    // edcaQueryResponse = new Gson().fromJson(doRequest(edcaQueryReq,
    // device.getAesKey()), EzCardEDCAQueryResponse.class);
    // if(edcaQueryResponse.getTxnResult().equals("Success")) {
    // userId = edcaQueryResponse.getEzCardId();
    // }
    //
    // if(StringUtils.isNotBlank(edcaQueryResponse.getNewAESKey())) {
    // device.setAesKey(edcaQueryResponse.getNewAESKey());
    // deviceService.save(device);
    // }
    //
    // if(!(Strings.isNullOrEmpty(userId) || userId.equals("-1"))) {
    // break;
    // }
    // tryCount ++;
    // LOGGER.info("[EZC] try to get card id : " + userId + ", " + tryCount + "
    // times...");
    // }
    // }
    //
    // return userId;
    // }

    private String queryIdForTaxi(PaymentAccount paymentAccount, Device device) {
        LOGGER.info(
                "[REQUEST][EZC][BalanceQuery][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");

        String date = getDate();
        String time = getTime();

        AcerTaxiQuery acerTaxiQuery = new AcerTaxiQuery();
        acerTaxiQuery.setTerminalId(paymentAccount.getAccount());
        acerTaxiQuery.setDeviceId(device.getOwnerDeviceId());

        acerTaxiQuery.setTerminalTXNNumber(time);
        acerTaxiQuery.setHostSerialNumber(String.valueOf(Integer.valueOf(device.getHostSerialNumber()) + 1));
        acerTaxiQuery.setTime(time);
        acerTaxiQuery.setDate(date);

        String result = doRequest(acerTaxiQuery, device.getAesKey());
        AcerTaxiQueryResponse response = new Gson().fromJson(result,
                AcerTaxiQueryResponse.class);

        if (StringUtils.isNotBlank(response.getNewAESKey())) {
            device.setAesKey(response.getNewAESKey());
            if (StringUtils.isNotBlank(response.getHostSerialNumber())) {
                device.setHostSerialNumber(response.getHostSerialNumber());
            }

            deviceService.save(device);
        }
        String getPointforRefill = "";
        String discount = "";
        if (StringUtils.isNotBlank(response.getEzCardType()) && response.getEzCardType().equals("01")) {
            try {

                IntTbLookupCode cardLookupCode = taxiPoint.isCanDeductionPoint(device, new Gson().fromJson(result, JsonObject.class));
                if (Objects.nonNull(cardLookupCode)) {
                    getPointforRefill = cardLookupCode.getType2();
                    discount = cardLookupCode.getType3();

                }
            } catch (Exception e) {
                LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
            }
        }
        // LOGGER.info("[REQUEST][EZC] NULL AreaCode");
        response.setPointRefillAmount(getPointforRefill.replaceAll("\"", ""));
        response.setDiscount(discount.replaceAll("\"", ""));
        return response.getEzCardId();
    }

    private String getDeviceQuery(Merchant merchant, RequestHeader requestHeader) {
        LOGGER.info("[REQUEST][EZC] list device by merchantId : " + merchant.getAccountId());
        List<DeviceVo> deviceList = deviceService.listAllByMerchantAndType(merchant.getAccountId(), "ezc");
        List<DeviceVo> trafficDeviceList = deviceService.listAllByMerchantAndType(merchant.getAccountId(), "traffic");
        deviceList = CollectionUtils.isEmpty(deviceList) ? new ArrayList<>() : deviceList;
        if (CollectionUtils.isNotEmpty(trafficDeviceList)) {
            deviceList.addAll(trafficDeviceList);
        }
        EzCardDeviceQueryResponse response = new EzCardDeviceQueryResponse();
        response.setDeviceList(deviceList);

        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                "DeviceQuery:" + merchant.getAccountId(), response.getOrderId() + "|" + response.getTxnResult());

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    private String getReserveOrderQuery(String deviceId, Merchant merchant, RequestHeader requestHeader) {
        LOGGER.info("[REQUEST][EZC] list reserve order by device Id : " + deviceId);
        List<ReserveDetail> list = reserveDetailService.listByDeviceId(merchant.getAccountId(), deviceId);

        EzCardReserveOrderResponse response = new EzCardReserveOrderResponse();

        List<EzcReserveDetailVo> ezcReserveDetailVoList = new ArrayList<EzcReserveDetailVo>();
        for (ReserveDetail tx : list) {
            EzcReserveDetailVo ezcTx = new EzcReserveDetailVo();
            ezcTx.setOrderId(tx.getReserveOrderId());
            ezcTx.setAmount(tx.getAmount());
            ezcTx.setCancelDate(tx.getCancelDate());
            ezcTx.setCancelStatus(tx.getCancelStatus());
            ezcTx.setCreateTime(tx.getCreateTime());
            ezcTx.setStatus(tx.getStatus());
            ezcTx.setUserId(tx.getUserId());

            ezcReserveDetailVoList.add(ezcTx);
        }

        response.setList(ezcReserveDetailVoList);

        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                "ReserveOrderQuery: " + merchant.getAccountId() + "|" + deviceId, "N/A");

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    private String getDate() {
        DateTime dateTime = DateTime.now();
        return String.valueOf(dateTime.getYear()) + String.format("%02d", dateTime.getMonthOfYear())
                + String.format("%02d", dateTime.getDayOfMonth());
    }

    private String getTime() {
        DateTime dateTime = DateTime.now();
        return String.format("%02d", dateTime.getHourOfDay()) + String.format("%02d", dateTime.getMinuteOfHour())
                + String.format("%02d", dateTime.getSecondOfMinute());
    }

    private String getBatchNumber(String date, String batch) {
        return date.substring(2, date.length()) + batch;
    }

    @Async
    private void writeDBLog(String merchantId, String serviceType, String method, String instruction, String response) {

        OperationHistory operationHistory = new OperationHistory();
        operationHistory.setServiceType(serviceType);

        operationHistory.setMethod(method);
        operationHistory.setMerchantId(merchantId);
        operationHistory.setInstruction(instruction);
        operationHistory.setResponse(response);

        operationHistoryService.save(operationHistory);

    }

    private String getGeneralResponse(RequestHeader requestHeader, EzCardBasicResponse response, String errorCode) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setStatusCode(errorCode); // 0001
        responseGeneralHeader.setStatusDesc("STATUS_SUCCESS");
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setMchId(requestHeader.getMerchantId());
        responseGeneralHeader.setMethod(requestHeader.getMethod());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        responseGeneralBody.setData(response);
        responseGeneralBody.setHeader(responseGeneralHeader);

        return new Gson().toJson(responseGeneralBody);
    }

    private String getErrorResponse(RequestHeader requestHeader, EzCardErrorResponse response, String statusDesc) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setStatusCode(SystemInstance.STATUS_FAIL);
        responseGeneralHeader.setStatusDesc(statusDesc);
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setMchId(requestHeader.getMerchantId());
        responseGeneralHeader.setMethod(requestHeader.getMethod());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        responseGeneralBody.setData(response);
        responseGeneralBody.setHeader(responseGeneralHeader);

        return new Gson().toJson(responseGeneralBody);
    }

    private EzCardErrorResponse getErrorResponse(RequestHeader requestHeader, Device device, String message,
                                                 String errorCode) {
        EzCardErrorResponse errorResponse = new EzCardErrorResponse();
        errorResponse.setErrorCode(errorCode);
        errorResponse.setTxnResult("Fail");
        errorResponse.setErrorMessage(message);
        errorResponse.setMerchantId(requestHeader.getMerchantId());
        errorResponse.setServiceType(requestHeader.getServiceType());
        errorResponse.setDeviceId(device.getOwnerDeviceId());
        return errorResponse;
    }

    private ReserveCancelInfo getReserveCancelInfo(List<TradeDetailVo> tradeDetailVos,
                                                   List<ReserveDetail> reserveDetails, RequestHeader requestHeader, Device device) {

        TradeDetailVo tradeDetailVo = null;
        ReserveDetail reserveDetail = null;

        if (tradeDetailVos != null) {
            tradeDetailVo = tradeDetailVos.get(0);
        }

        if (reserveDetails != null) {
            reserveDetail = reserveDetails.get(0);
        }

        ReserveCancelInfo reserveCancelInfo = new ReserveCancelInfo();

        if (tradeDetailVo == null && reserveDetail == null) {
            LOGGER.error("No Trade and Reserve Record.");
            return null;
        } else if (tradeDetailVo == null && reserveDetail != null) {
            LOGGER.error("[REQUEST][EZC] CANCEL_RESERVE. " + device.getOwnerDeviceId() + ", "
                    + requestHeader.getMerchantId());
            reserveCancelInfo.action = "CANCEL_RESERVE";
            reserveCancelInfo.rrn = reserveDetail.getRrn();
            reserveCancelInfo.dongleDeviceId = device.getEzcDongleId();
            reserveCancelInfo.txnDate = reserveDetail.getEzcTxnDate();
            reserveCancelInfo.setReserveDetail(reserveDetail);
        } else if (tradeDetailVo != null && reserveDetail == null) {
            LOGGER.error(
                    "[REQUEST][EZC] REFUND_TRADE. " + device.getOwnerDeviceId() + ", " + requestHeader.getMerchantId());
            reserveCancelInfo.action = "REFUND_TRADE";
            reserveCancelInfo.rrn = tradeDetailVo.getRrn();
            reserveCancelInfo.dongleDeviceId = device.getEzcDongleId();
            reserveCancelInfo.setTradeDetailVo(tradeDetailVo);
            reserveCancelInfo.txnDate = tradeDetailVo.getEzcTxnDate();
        } else {
            if (Long.valueOf(tradeDetailVo.getCreateDate()) > Long.valueOf(reserveDetail.getCreateTime())) {
                LOGGER.error("[REQUEST][EZC] REFUND_TRADE. " + device.getOwnerDeviceId() + ", "
                        + requestHeader.getMerchantId());
                reserveCancelInfo.action = "REFUND_TRADE";
                reserveCancelInfo.rrn = tradeDetailVo.getRrn();
                reserveCancelInfo.dongleDeviceId = device.getEzcDongleId();
                reserveCancelInfo.txnDate = tradeDetailVo.getEzcTxnDate();
                reserveCancelInfo.setTradeDetailVo(tradeDetailVo);

            } else {
                if (reserveDetail.getIsAuto() == 0) {
                    LOGGER.error("[REQUEST][EZC] CANCEL_RESERVE. " + device.getOwnerDeviceId() + ", "
                            + requestHeader.getMerchantId());
                    reserveCancelInfo.action = "CANCEL_RESERVE";
                    reserveCancelInfo.rrn = reserveDetail.getRrn();
                    reserveCancelInfo.dongleDeviceId = device.getEzcDongleId();
                    reserveCancelInfo.setReserveDetail(reserveDetail);
                    reserveCancelInfo.txnDate = reserveDetail.getEzcTxnDate();
                } else {
                    LOGGER.error("[REQUEST][EZC] REFUND_TRADE. " + device.getOwnerDeviceId() + ", "
                            + requestHeader.getMerchantId());
                    reserveCancelInfo.action = "REFUND_TRADE";
                    reserveCancelInfo.rrn = tradeDetailVo.getRrn();
                    reserveCancelInfo.dongleDeviceId = device.getEzcDongleId();
                    reserveCancelInfo.txnDate = tradeDetailVo.getEzcTxnDate();
                    reserveCancelInfo.setTradeDetailVo(tradeDetailVo);
                }
            }
        }

        reserveCancelInfo.txnDeviceId = device.getOwnerDeviceId();

        return reserveCancelInfo;
    }

    private void printReceipt(EzCardPaymentResponse response, Device device, PaymentAccount paymentAccount) {
        LOGGER.info("[EZC][Receipt][Print][input] " + new Gson().toJson(response));
        EzCardReceipt ezCardReceipt = new Gson().fromJson(new Gson().toJson(response), EzCardReceipt.class);
        ezCardReceipt.setDeviceId(device.getOwnerDeviceId());
        ezCardReceipt.setTerminalId(paymentAccount.getAccount());
        ezCardReceipt.setPrintId("01");
        ezCardReceipt.setDate(response.getDate());

        ezCardReceipt.setTime(response.getTime());

        // LOGGER.info("[EZC][Receipt][Print][REQ] " + new
        // Gson().toJson(ezCardReceipt));

        if (Integer.valueOf(response.getCpuPurseVersion()) != 0) {
            ezCardReceipt.setEzCardId(response.getEzCardPurseID());
        }
        String date = getDate();
        ezCardReceipt.setCpuPurseVersion(response.getCpuPurseVersion());
        ezCardReceipt.setAmount(response.getAmount());
        ezCardReceipt.setBatchNumber(getBatchNumber(date, String.format("%02d", device.getBatchNumber())));

        LOGGER.info("[EZC][Receipt][Print][REQ] " + new Gson().toJson(ezCardReceipt));

        if (response.getAutoTopUpAmount() != null && !response.getAutoTopUpAmount().equals("0")) {
            ezCardReceipt.setAutoTopUpAmount(response.getAutoTopUpAmount());
        }

        String printResponse = "";

        if (response instanceof AcerTaxiPaymentResponse) {
            AcerTaxiPaymentResponse acerTaxiPaymentResponse = (AcerTaxiPaymentResponse) response;
            ezCardReceipt.setDeductPoint(acerTaxiPaymentResponse.getDiscountAmount());
            ezCardReceipt.setPointBalance(acerTaxiPaymentResponse.getPointBalance());
            ezCardReceipt.setTransferDiscount(acerTaxiPaymentResponse.getDiscountAmount());
            this.taxiRequest(ezCardReceipt, device.getAesKey());
        } else {
            printResponse = doRequest(ezCardReceipt, device.getAesKey());
        }

        LOGGER.info("[EZC][Receipt][Print][response] " + printResponse);
    }

    /**
     * 將ＡＰＰ邏輯搬來 各自使用
     *
     * @param requestHeader
     * @param requestData
     * @param device
     * @param paymentAccount
     * @param merchant
     * @return
     */
    private String ticketLogic(RequestHeader requestHeader, JsonObject requestData, Device device,
                               PaymentAccount paymentAccount, Merchant merchant) throws UnsupportedEncodingException, OtherAPIException {
        long isRetry = requestData.get("Retry").getAsLong();
        String orderId = requestData.has(SystemInstance.FIELD_NAME_STORE_ORDER_NO) ?
                StringUtils.defaultIfEmpty(requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString(), merchant.getRandomId() + new SimpleDateFormat("yyMMddHHmmssSS").format(new Date()))
                : merchant.getRandomId() + new SimpleDateFormat("yyMMddHHmmssSS").format(new Date());
        TradeDetail tradeDetail = tradeDetailService.getOne(orderId);
        JsonElement queryCardInfoSultDataJE = requestData.get("QueryCardInfoSultData");
        String queryCardInfoSult = (Objects.isNull(queryCardInfoSultDataJE) || queryCardInfoSultDataJE.isJsonNull() || StringUtils.isBlank(queryCardInfoSultDataJE.getAsString())) || isRetry == 0 ?
                this.taxiCardQuery(device, paymentAccount, requestHeader, requestData) : queryCardInfoSultDataJE.getAsString();

        if (Objects.nonNull(tradeDetail) && isRetry == 0) {
            queryCardInfoSult = this.taxiCardQuery(device, paymentAccount, requestHeader, requestData);
            orderId = merchant.getRandomId() + new SimpleDateFormat("yyMMddHHmmssSS").format(new Date());
        }

        JsonObject queryCardInfoSultData = new Gson().fromJson(URLDecoder.decode(queryCardInfoSult, "UTF-8"), JsonObject.class).get("Data")
                .getAsJsonObject();

        if ("000000".equals(queryCardInfoSultData.get("ErrorCode").getAsString())
                && "Success".equals(queryCardInfoSultData.get("TXNResult").getAsString())) {
            Map<String, String> resultMap = taxiPoint.mainLogic(device, queryCardInfoSultData, requestData);

            resultMap.put(SystemInstance.FIELD_NAME_STORE_ORDER_NO, orderId);
            resultMap.put("QueryCardInfoSultData", queryCardInfoSult);
            LOGGER.info("[EZC][ticketLogic] resultMap =>" + resultMap);
            LOGGER.info("[EZC][ticketLogic] requestData =>" + requestData);
            IntTbLookupCode intTbLookupCode = taxiPoint.isCanDeductionPoint(device, queryCardInfoSultData);
            if ((Objects.nonNull(intTbLookupCode) && "TY".equals(intTbLookupCode.getType1()) && StringUtils.isEmpty(resultMap.get("Msg"))) ||
                    (resultMap.get("NeedBalance").equals(requestData.get("NeedBalance").getAsString())
                            && resultMap.get("Cash").equals(requestData.get("Cash").getAsString())
                            && resultMap.get("Point").equals(requestData.get("Point").getAsString()))) {
                requestData.addProperty("Cash", resultMap.get("Cash"));
                requestData.addProperty("NeedBalance", resultMap.get("NeedBalance"));
                requestData.addProperty("Point", resultMap.get("Point"));
                requestData.addProperty("QueryCardInfoSultData", resultMap.get("QueryCardInfoSultData"));

                this.deductionForTaxi(requestData, queryCardInfoSultData, merchant, intTbLookupCode, orderId);
                requestData.addProperty("arcSeq", resultMap.get("arcSeq"));
                requestData.addProperty("transMsg", resultMap.get("Msg"));
                requestHeader.setServiceType("PaymentForTaxi");

                // 分配與傳入的相同,直接送單
                return this.taxiPayment(requestHeader, requestData, device, paymentAccount, merchant);
            } else {
                return getGeneralResponse(requestHeader,
                        new Gson().fromJson(new Gson().toJson(resultMap), EzCardSicketLogicRespons.class), SystemInstance.STATUS_SUCCESS);
            }
        } else {
            EzCardErrorResponse errorResponse = getErrorResponse(requestHeader, device, EzcUtil.getErrorMsg(queryCardInfoSultData.get("ErrorCode").getAsString()),
                    queryCardInfoSultData.get("ErrorCode").getAsString());
            return getErrorResponse(requestHeader, errorResponse, EzcUtil.getErrorMsg(queryCardInfoSultData.get("ErrorCode").getAsString()));
        }
    }

    private void deductionForTaxi(JsonObject requestData, JsonObject queryCardInfoSultData, Merchant merchant, IntTbLookupCode intTbLookupCode, String orderId) {


        boolean isNotWelfareCard = Objects.isNull(intTbLookupCode) || Objects.isNull(intTbLookupCode.getType1());

        requestData.addProperty("StoreOrderNo", orderId);
        requestData.addProperty("EZCardID", queryCardInfoSultData.get("EZCardID").getAsString());
        requestData.addProperty("EZCardType", queryCardInfoSultData.get("EZCardType").getAsString());
        requestData.addProperty("PersonalProfile", queryCardInfoSultData.get("PersonalProfile").getAsString());
        requestData.addProperty("PersonalProfileAuthorization", queryCardInfoSultData.get("PersonalProfileAuthorization").getAsString());
        requestData.addProperty("PointID", queryCardInfoSultData.get("PointID").getAsString());
        requestData.addProperty("PointType", queryCardInfoSultData.get("PointType").getAsString());
        requestData.addProperty("MaxPointOnMonth", isNotWelfareCard ? "0" : intTbLookupCode.getType2()); // 每月最大點數額度
        requestData.addProperty("TransferDiscount", isNotWelfareCard ? "0" : String.valueOf(Integer.valueOf(intTbLookupCode.getType3()) *
                Integer.valueOf(requestData.get("Point").getAsString()))); //交易點數折抵金額( 優惠金額 )
        requestData.addProperty("DeductPoint", requestData.get("Point").getAsString()); // 點數扣點數量
        String usedate = queryCardInfoSultData.get("PointLastUseDate").getAsString();
        String now = new SimpleDateFormat("yyyyMM").format(new Date());

        requestData.addProperty("CountOfDeductPoint", String.valueOf((isNotWelfareCard || now.equals(usedate.substring(0, 6)) ? Integer.valueOf(queryCardInfoSultData.get("CountOfDeductPoint").getAsString()) : 0) +
                Integer.valueOf(requestData.get("Point").getAsString()))); // 本月累計使用點數(社福卡+跨月歸0) ＋ 本次扣點
        requestData.addProperty("PointBalance", isNotWelfareCard ? "0" : String.valueOf(Integer.valueOf(intTbLookupCode.getType2()) -
                Integer.valueOf(requestData.get("CountOfDeductPoint").getAsString())));   // 剩餘可扣點數
        requestData.addProperty("NeedCash", requestData.get("NeedBalance").getAsString()); //須補現金
        requestData.addProperty("AreaCode", queryCardInfoSultData.get("AreaCode").getAsString()); //地區馬
        requestData.addProperty("PointLastUseDate", isNotWelfareCard || Integer.valueOf(requestData.get("Point").getAsString()) <= 0 ? "00000000" : new SimpleDateFormat("yyyyMMdd").format(new Date()));
        requestData.addProperty("Amount", requestData.get("Cash").getAsString());     //實際扣款金額
    }

    private class ReserveCancelInfo {

        private String action;

        private String rrn;

        private String dongleDeviceId;

        private String txnDeviceId;

        private String txnDate;

        private TradeDetailVo tradeDetailVo;

        private ReserveDetail reserveDetail;

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }

        public String getRrn() {
            return rrn;
        }

        public void setRrn(String rrn) {
            this.rrn = rrn;
        }

        public String getDongleDeviceId() {
            return dongleDeviceId;
        }

        public void setDongleDeviceId(String dongleDeviceId) {
            this.dongleDeviceId = dongleDeviceId;
        }

        public String getTxnDeviceId() {
            return txnDeviceId;
        }

        public void setTxnDeviceId(String txnDeviceId) {
            this.txnDeviceId = txnDeviceId;
        }

        public String getTxnDate() {
            return txnDate;
        }

        public void setTxnDate(String txnDate) {
            this.txnDate = txnDate;
        }

        public TradeDetailVo getTradeDetailVo() {
            return tradeDetailVo;
        }

        public void setTradeDetailVo(TradeDetailVo tradeDetailVo) {
            this.tradeDetailVo = tradeDetailVo;
        }

        public ReserveDetail getReserveDetail() {
            return reserveDetail;
        }

        public void setReserveDetail(ReserveDetail reserveDetail) {
            this.reserveDetail = reserveDetail;
        }
    }


    @Override
    public EzCardEDCAPaymentReq getEzCardEDCAPaymentReq(Device device, PaymentAccount paymentAccount, String amount, String time, String date, String batchNo, String retry) {

        EzCardEDCAPaymentReq edcaPaymentReq = new EzCardEDCAPaymentReq();
        edcaPaymentReq.setTerminalId(paymentAccount.getAccount());
        edcaPaymentReq.setDeviceId(device.getOwnerDeviceId());
        edcaPaymentReq.setTerminalTXNNumber(getTime());
        edcaPaymentReq.setHostSerialNumber(String.format("%06d", (Long.valueOf(device.getHostSerialNumber()) + 1)));
        edcaPaymentReq.setTime(time);
        edcaPaymentReq.setDate(date);
        edcaPaymentReq.setAmount(amount);
        edcaPaymentReq.setRetry(retry);
        edcaPaymentReq.setSameCard("0");
        edcaPaymentReq.setBatchNumber(batchNo);
        edcaPaymentReq.setPersonalDiscount("0");
        edcaPaymentReq.setTransferGroup("06");
        edcaPaymentReq.setPaymentType("20");
        edcaPaymentReq.setAesKey(device.getAesKey());

        return edcaPaymentReq;
    }

    @Override
    public EzCardPayment getEzCardPayment(Device device, PaymentAccount paymentAccount, String amount, String time, String date, String batchNo, String retry, String terminalTXNNumber) {
        EzCardPayment ezCardPayment = new EzCardPayment();
        ezCardPayment.setTerminalId(paymentAccount.getAccount());
        ezCardPayment.setDeviceId(device.getOwnerDeviceId());

        ezCardPayment.setTerminalTXNNumber(terminalTXNNumber);
        ezCardPayment.setHostSerialNumber(Long.valueOf(retry) > 0 ? terminalTXNNumber : time); // 因之前有嚴重延遲，所以改為送出前當下
        ezCardPayment.setTime(time);
        ezCardPayment.setDate(date);

        ezCardPayment.setAmount(amount);
        ezCardPayment.setRetry(retry);
        ezCardPayment.setBatchNumber(batchNo);
        ezCardPayment.setSameCard(Integer.valueOf(retry) > 0 ? "1" : "0");
        ezCardPayment.setAesKey(device.getAesKey());
        return ezCardPayment;
    }

    @Override
    public String updateDoPayment(RequestHeader requestHeader, JsonObject requestData, Device device, Merchant merchant, EzCardPayment ezCardPayment, EzCardPaymentResponse response, TradeDetail tradeDetail, PaymentAccount paymentAccount) {

        response.setRequest(ezCardPayment);
        response.setOrderId(tradeDetail.getOrderId());
        response.setCardDesc("悠遊卡");
        LOGGER.info("[RESPONSE][EZC] " + new Gson().toJson(response));

        if (StringUtils.isNotBlank(response.getNewAESKey())) {
            device.setAesKey(response.getNewAESKey());
            // if (StringUtils.isNotBlank(response.getHostSerialNumber()))
            // {
            // device.setHostSerialNumber(response.getHostSerialNumber());
            // }
            deviceService.save(device);
        }

        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(),
                requestHeader.getMethod(), "Payment | " + merchant.getMerchantSeqId() + " | "
                        + device.getOwnerDeviceId() + " | " + tradeDetail.getPayment() + " | " + tradeDetail.getUserId(),
                response.getOrderId() + "|" + response.getTxnResult());

//        tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());
        tradeDetail.setBatchNo(ezCardPayment.getBatchNumber());

        if (StringUtils.isNotBlank(response.getAutoTopUpAmount()) && !"0".equals(response.getAutoTopUpAmount())) {
            // fixme should auto up need retry?
            saveReserveDetail(tradeDetail.getBatchNo(), requestData, requestHeader, tradeDetail.getOrderId(), response, device, tradeDetail.getUserId(), ezCardPayment.getDate(), 0,
                    1, ezCardPayment.getTime());
        }

        if ("Success".equals(response.getTxnResult())) {
            LOGGER.info("[RESPONSE][EZC][Success] " + new Gson().toJson(response));
            tradeDetail.setMethod("31800");
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetail.setRrn(response.getRrNumber());
            tradeDetail.setEzcTxnDate(ezCardPayment.getDate() + ezCardPayment.getTime());
            tradeDetailService.save(tradeDetail);
            appNotificationService.notifyApp(tradeDetail, merchant);
            this.saveTradeDetailOtherInfo(tradeDetail, response);
            printReceipt(response, device, paymentAccount);

        } else {
            LOGGER.error("[RESPONSE][EZC][Fail] " + new Gson().toJson(response));
            //TODO
            response.setTxnResult(StringUtils.defaultIfEmpty(response.getTxnResult(), SystemInstance.STATUS_DESCRIPTION_FAIL2));
            response.setErrorCode(StringUtils.defaultIfEmpty(response.getErrorCode(), "900000"));
            if (!lookupCodeService.isTsmcAccount(requestHeader.getMerchantId()) && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            } else if ("14900".equals(tradeDetail.getMethod())) {
                if (SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    requestHeader.setMethod("14900");
                    response.setTxnResult(SystemInstance.STATUS_DESCRIPTION_SUCCESS);
                    response.setErrorCode("000000");
                } else {
                    tradeDetail.setMethod("31800");
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }
            tradeDetailService.save(tradeDetail);
        }

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    @Override
    public String updateEDCAPayment(RequestHeader requestHeader, JsonObject requestData, Device device, Merchant merchant, EzCardEDCAPaymentReq ezCardEDCAPaymentReq, AcerTaxiPaymentResponse paymentResponse, TradeDetail tradeDetail, PaymentAccount paymentAccount) {
        paymentResponse.setOrderId(tradeDetail.getOrderId());
        paymentResponse.setRequest(ezCardEDCAPaymentReq);
        paymentResponse.setCardDesc("悠遊卡");
        if (StringUtils.isNotBlank(paymentResponse.getNewAESKey())) {
            device.setAesKey(paymentResponse.getNewAESKey());
            device.setHostSerialNumber(paymentResponse.getHostSerialNumber());
            deviceService.save(device);
        }

        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                "Payment | " + merchant.getMerchantSeqId() + " | " + device.getOwnerDeviceId() + " | "
                        + tradeDetail.getPayment() + " | " + tradeDetail.getUserId(),
                paymentResponse.getOrderId() + "|" + paymentResponse.getTxnResult());

        tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());
        tradeDetail.setBatchNo(ezCardEDCAPaymentReq.getBatchNumber());

        if (StringUtils.isNotBlank(paymentResponse.getAutoTopUpAmount()) && !"0".equals(paymentResponse.getAutoTopUpAmount())) {
            // fixme should auto up need retry?
            saveReserveDetail(tradeDetail.getBatchNo(), requestData, requestHeader, tradeDetail.getOrderId(), paymentResponse, device, tradeDetail.getUserId(),
                    ezCardEDCAPaymentReq.getDate(), 0, 1, ezCardEDCAPaymentReq.getTime());
        }

        if (paymentResponse.getTxnResult().equals("Success")) {
            LOGGER.info("[RESPONSE][EDCA][Success] " + new Gson().toJson(paymentResponse));
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetail.setMethod("31800");
            tradeDetail.setRrn(paymentResponse.getRrNumber());
            tradeDetail.setEzcTxnDate(ezCardEDCAPaymentReq.getDate() + ezCardEDCAPaymentReq.getTime());
            tradeDetailService.save(tradeDetail);
            appNotificationService.notifyApp(tradeDetail, merchant);

            printReceipt(paymentResponse, device, paymentAccount);
        } else {
            LOGGER.error("[RESPONSE][ACER][Fail] " + new Gson().toJson(paymentResponse));
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            tradeDetailService.save(tradeDetail);
        }
        // writeDBLog(requestHeader.getMerchantId(),
        // requestHeader.getServiceType(), requestHeader.getMethod(), new
        // Gson().toJson(acerTaxiQuery), response.getOrderId() + "|" +
        // response.getTxnResult());

        return getGeneralResponse(requestHeader, paymentResponse, SystemInstance.STATUS_SUCCESS);
    }

    @Override
    public EzCardRefund getEzCardRefund(Device device, PaymentAccount paymentAccount, String time, String date, String batchNo, String retry, String terminalTXNNumber) {
        EzCardRefund ezCardRefund = new EzCardRefund();
        ezCardRefund.setTerminalId(paymentAccount.getAccount());
        ezCardRefund.setDeviceId(device.getOwnerDeviceId());
        ezCardRefund.setTerminalTXNNumber(terminalTXNNumber);
        ezCardRefund.setHostSerialNumber(getTime()); // 因之前有嚴重延遲，所以改為送出前當下
        ezCardRefund.setTime(time);
        ezCardRefund.setDate(date);
        ezCardRefund.setRetry(retry);
        ezCardRefund.setBatchNumber(batchNo);
        ezCardRefund.setSameCard(Integer.valueOf(retry) > 0 ? "1" : "0");
        ezCardRefund.setAesKey(device.getAesKey());

        return ezCardRefund;
    }

    @Override
    public EzCardInfoQuery getEzCardInfoQuery(Device device, PaymentAccount paymentAccount, String date, String time) {
        EzCardInfoQuery ezCardInfoQuery = new EzCardInfoQuery();
        ezCardInfoQuery.setTerminalId(paymentAccount.getAccount());
        ezCardInfoQuery.setDeviceId(device.getOwnerDeviceId());

        ezCardInfoQuery.setTerminalTXNNumber(time);
        ezCardInfoQuery.setHostSerialNumber(getTime()); // 因之前有嚴重延遲，所以改為送出前當下
        ezCardInfoQuery.setTime(time);
        ezCardInfoQuery.setDate(date);
        ezCardInfoQuery.setAesKey(device.getAesKey());

        return ezCardInfoQuery;
    }

    @Override
    public String upodateBalanceQuery(Device device, RequestHeader requestHeader, EzCardInfoQuery ezCardInfoQuery, EzCardInfoQueryResponse response) {

        if (StringUtils.isNotBlank(response.getNewAESKey())) {
            device.setAesKey(response.getNewAESKey());
            // if (StringUtils.isNotBlank(response.getHostSerialNumber()))
            // {
            // device.setHostSerialNumber(response.getHostSerialNumber());
            // }
            deviceService.save(device);
        }

        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                new Gson().toJson(ezCardInfoQuery), response.getOrderId() + "|" + response.getTxnResult());

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    @Override
    public String updateDoRefund(RequestHeader requestHeader, JsonObject requestData, Device device, Merchant merchant, EzCardRefund ezCardRefund, EzCardRefundResponse response, TradeDetail tradeDetail, PaymentAccount paymentAccount, String batchNo) {
        if (StringUtils.isNotBlank(response.getNewAESKey())) {
            device.setAesKey(response.getNewAESKey());
            // if (StringUtils.isNotBlank(response.getHostSerialNumber()))
            // {
            // device.setHostSerialNumber(response.getHostSerialNumber());
            // }
            deviceService.save(device);
        }

        // set refundKey for retry
        if (response.getErrorCode().equals("000125")) {
            ezCardRefund.setRefundKey(requestData.get("RefundKey").getAsString());
            response.setOrderId(tradeDetail.getOrderId());
        }

        response.setRequest(ezCardRefund);
        LOGGER.info("[RESPONSE][EZC] refund success and update refund information." + new Gson().toJson(response));
        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(),
                requestHeader.getMethod(), "REFUND(LIMIT) | " + paymentAccount.getAccount() + "|"
                        + device.getOwnerDeviceId() + "|" + response.getAmount(),
                response.getOrderId() + "|" + response.getTxnResult());

        saveOrUpdateTransactionDetails(tradeDetail, merchant, response, device, batchNo,
                tradeDetail.getPayment(), requestData.get("Retry").getAsLong(), getTime(), false);

        if (response.getTxnResult().equals("Success")) {
            printReceipt(response, device, paymentAccount);
        }

        LOGGER.info("doRefund() END " + requestHeader.getServiceType());
        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }


    private void insertTaxiTransactionDetail(TradeDetail tradeDetail, Merchant merchant, PaymentAccount paymentAccount, Device device, AcerTaxiPaymentResponse acerTaxiPaymentResponse, JsonObject requestData) {
        Map<String, String> taxiTransactionDetail = new HashMap<String, String>();
        taxiTransactionDetail.put("status", tradeDetail.getStatus());
        taxiTransactionDetail.put("orderId", tradeDetail.getOrderId());
        taxiTransactionDetail.put("accountId", merchant.getAccountId());
        taxiTransactionDetail.put("terminalId", paymentAccount.getAccount());
        taxiTransactionDetail.put("deviceId", device.getOwnerDeviceId());
        taxiTransactionDetail.put("licensePlate", device.getLicensePlate());
        taxiTransactionDetail.put("cash", String.valueOf(tradeDetail.getPayment()));
        taxiTransactionDetail.put("carePoint", String.valueOf(tradeDetail.getCarePoint()));
        taxiTransactionDetail.put("transactionDetail", tradeDetail.getDescription());

        taxiTransactionDetail.put("transactionType", "18");
        taxiTransactionDetail.put("cardId", StringUtils.defaultIfBlank(acerTaxiPaymentResponse.getEzCardId(), requestData.get("EZCardID").getAsString()));
        taxiTransactionDetail.put("cardType", StringUtils.defaultIfBlank(acerTaxiPaymentResponse.getEzCardType(), requestData.get("EZCardType").getAsString()));
        taxiTransactionDetail.put("identityInfo", StringUtils.defaultIfBlank(acerTaxiPaymentResponse.getPersonalProfile(), requestData.get("PersonalProfile").getAsString()));
        taxiTransactionDetail.put("cardArea", StringUtils.defaultIfBlank(acerTaxiPaymentResponse.getAreaCode(), requestData.get("AreaCode").getAsString()));
        taxiTransactionDetail.put("needBalance", Objects.isNull(requestData.get("NeedCash")) || requestData.get("NeedCash").isJsonNull() ? "0" : requestData.get("NeedCash").getAsString());
        taxiTransactionDetail.put("arcSeq", Objects.isNull(requestData.get("arcSeq")) || requestData.get("arcSeq").isJsonNull() ? null : requestData.get("arcSeq").getAsString());
        taxiTransactionDetail.put("transMsg", Objects.isNull(requestData.get("transMsg")) || requestData.get("transMsg").isJsonNull() ? null : requestData.get("transMsg").getAsString());
        taxiTransactionDetail.put("personalIdentity", requestData.get("PersonalProfileAuthorization").getAsString());
        try {
            LOGGER.info("[REQ][TAXI_RECORD_INSERT_API_URL] object = " + JsonUtil.toJsonStr(taxiTransactionDetail));
            TaxiTransactionDetail taxiTransactionDetail1 = taxiTransactionDetailService.save(JsonUtil.toJsonStr(taxiTransactionDetail));
            LOGGER.info("[RESPONSE][TAXI_RECORD_INSERT_API_URL] " + JsonUtil.toJsonStr(taxiTransactionDetail1));
        } catch (Exception e) {
            LOGGER.error("[REQUEST][TAXIRECORD] INSERT ERROR. " + tradeDetail.getOrderId() + ", "
                    + merchant.getAccountId() + ", " + device.getOwnerDeviceId());
            LOGGER.error(e.getMessage());
        }
    }

    private void saveTradeDetailOtherInfo(TradeDetail tradeDetail, EzCardPaymentResponse response) {
        TradeDetailOtherInfo tradeDetailOtherInfo = new TradeDetailOtherInfo();
        tradeDetailOtherInfo.setTradeDetailRandomId(tradeDetail.getTradeDetailRandomId());
        tradeDetailOtherInfo.setExeTime(1L);
        tradeDetailOtherInfo.setCardNumberForPrint(StringUtils.defaultString(response.getCardNumberForPrint(), response.getEzCardId()));
        tradeDetailOtherInfo.setCardId(response.getEzCardId());
        tradeDetailOtherInfoService.save(tradeDetailOtherInfo);
    }
}