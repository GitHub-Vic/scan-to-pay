package co.intella.service.impl;

import co.intella.model.IntTbAbnormalRule;
import co.intella.net.HttpRequestUtil;
import co.intella.service.AbnormalRuleService;
import co.intella.utility.JsonUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AbnormalRuleServiceImpl implements AbnormalRuleService {
    @Value("${host.dao}")
    private String DAO_URL;

    @Override
    public List<IntTbAbnormalRule> findAllByArcSeq(Integer arcSeq) {
        List<IntTbAbnormalRule> list = null;
        try {
            Gson gson = JsonUtil.getGson();

            String responseEntity = HttpRequestUtil.httpGet(DAO_URL + "api/crm/AbnormalRule/getByArcSeq/" + arcSeq);
            list = gson.fromJson(responseEntity, new TypeToken<List<IntTbAbnormalRule>>() {
            }.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public List<IntTbAbnormalRule> findAll() {
        List<IntTbAbnormalRule> list = null;
        try {
            Gson gson = JsonUtil.getGson();

            String responseEntity = HttpRequestUtil.httpGet(DAO_URL + "api/crm/AbnormalRule/getAll");
            list = gson.fromJson(responseEntity, new TypeToken<List<IntTbAbnormalRule>>() {
            }.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

}
