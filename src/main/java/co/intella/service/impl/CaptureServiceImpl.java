package co.intella.service.impl;

import co.intella.domain.tcbank.TCBCreditCardCaptureRequestHeader;
import co.intella.domain.tcbank.TCBCreditCardCaptureResponseData;
import co.intella.model.CaptureDetail;
import co.intella.model.CaptureOrderDetail;
import co.intella.service.CaptureDetailService;
import co.intella.service.CaptureOrderDetailService;
import co.intella.service.CaptureService;
import co.intella.utility.SystemInstance;
import co.intella.utility.TcBankUtil;
import com.ibm.icu.util.Calendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Andy Lin
 */
@Service
@Transactional
public class CaptureServiceImpl implements CaptureService {

    private final Logger LOGGER = LoggerFactory.getLogger(CaptureServiceImpl.class);

    @Value("${file.path.tcb.capture.response}")
    private String FILE_PATH;

    @Resource
    private CaptureOrderDetailService captureOrderDetailService;

    @Resource
    private CaptureDetailService captureDetailService;

    public CaptureDetail generalCapture(String merchantId, String method, String from, String to) {
        return null;
    }

    public String tcbCapture(String mchId, String from, String to) throws IOException, ParseException {

        String response = "";
        List<CaptureOrderDetail> listOfCaptureOrderDetail = captureOrderDetailService.listUnSettleCaptureOrder(mchId, from+ "230000", to+ "225959", "20400");

        if(!listOfCaptureOrderDetail.isEmpty()) {
            CaptureDetail captureDetail = new CaptureDetail();
            captureDetail.setMerchantId(mchId);
            captureDetail.setCaptureDate(to);
            captureDetail.setSequentialNo("8888888888");
            captureDetail.setTotalCount(listOfCaptureOrderDetail.size());
            captureDetail.setCreateTime(getNowTime());
            captureDetail.setMethod("20400");
            captureDetail.setFeeRate(0.25);
            CaptureDetail captureDetail2 = captureDetailService.save(captureDetail);

            TCBCreditCardCaptureRequestHeader requestHeader = new TCBCreditCardCaptureRequestHeader();
            requestHeader.setHeader("H");
            requestHeader.setMchId(mchId);
            requestHeader.setCaptureTime(to);
            requestHeader.setSequentialNo("8888888888");
            requestHeader.setTotalCount(String.valueOf(captureDetail.getTotalCount()));
            requestHeader.setTotalFee("0");
            requestHeader.setCreateTime(captureDetail.getCreateTime());

            Map<String, String> captureInfo = getTcbankRequestInfo(requestHeader, listOfCaptureOrderDetail);

            for (CaptureOrderDetail element : listOfCaptureOrderDetail) {
                element.setCaptureDetail(captureDetail2);
                element.setStatus("done");
                element.setCaptureDate(captureDetail2.getCaptureDate());
                captureOrderDetailService.save(element);
            }

//            String datName = requestHeader.getMchId() + ".dat";
//            String path = "..\\CaptureFiles\\"+ mchId +"\\";
            mchId = "0100829354";  //test
            String datName = mchId + ".dat";
            String path = FILE_PATH + mchId +"/";
            LOGGER.info("File Directory is Creating ...");
            File file = new File(path);
            if(!file.exists())
            {
                file.mkdirs();
            }
            LOGGER.info("File Directory Created.");

            BufferedWriter bufWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path+datName, false),"UTF-8"));
            bufWriter.write(captureInfo.get("requestData"));
            bufWriter.flush();
            bufWriter.close();

            LOGGER.info("[TcBank] capture request sending...");
            response = TcBankUtil.upload("Internet", path + mchId , "tcbank-test.nccc.com.tw", mchId);
        }else {
            LOGGER.info("[TcBank] No more Order to be Captured");
            return "2000";
        }
        LOGGER.info("[TcBank] Done, tcb capture.");
        return "0000";
    }

    public String tcbCaptureResult(String mchId, String from, String to) throws IOException, ParseException {
        //todo : get all merchants registered in TCBank
        String rspName = mchId + "_" + from + ".rsp";
        String path = FILE_PATH + mchId +"/";
//        LOGGER.info(path);
        String response = "";

        LOGGER.info("File Directory is Creating ...");
        File directory = new File(path);
        if(!directory.exists())
        {
            directory.mkdirs();
        }
        LOGGER.info("File Directory Created.");

        response = TcBankUtil.download("Internet", path, mchId,"tcbank-test.nccc.com.tw", from);

        if(!response.equals("99")) {
            FileReader file = new FileReader(path + rspName);


            if(file.ready())
            {
                BufferedReader br = new BufferedReader(file);
                String result = "";

                while(br.ready()) {
                    result += br.readLine();
                }
                file.close();

                String[] array = result.split("\n");
                LOGGER.info("[TcBank] Capture finished!");
            }
            //todo: re-organizes the downloaded-file
            //        BufferedReader br = new BufferedReader(file);
            //        String response = "";
            //
            //        while(br.ready()) {
            //            response += br.readLine();
            //        }
            //        file.close();
            //
            //        String[] array = response.split("\n");
            //        LOGGER.info("Tcb Capture finished!");
            //        List <CaptureOrderDetail> captureOrderDetailList = captureOrderDetailService.listUnSettleCaptureOrder("lafresh", "20170613", "20170613", "20400");
            //        for(String s : array) {
            //            TCBCreditCardCaptureResponseData tcbResponseData = transformToTCBCaptureResponse(s);
            //            for(CaptureOrderDetail element : captureOrderDetailList) {
            //                if(element.getStoreOrderNo().equals(tcbResponseData.getStoreOrderNo().trim()) && element.getId() == 1524) {
            //                    element.setStatus("done");
            //                    captureOrderDetailService.save(element);
            //                }else {
            //                    continue;
            //                }
            //            }
            //        }
            return "0000";
        }else {
            LOGGER.info("[TcBank] Can't Find specific file.");
            return "2005";
        }
    }

    public String weixinCapture(String mchId, String from, String to) throws ParseException, IOException {

        LOGGER.info("[WeiXin] From " + from + ":000000 to "+ to + ":000000");

        String response = "0000";
        List<CaptureOrderDetail> listOfCaptureOrderDetail = captureOrderDetailService.listByMerchantIdAndDate(mchId, from + "000000", to + "000000", "10110");

        CaptureDetail finalCaptureDetail;
        CaptureDetail captureDetail = captureDetailService.getOne(mchId, from, "10110");
        if(captureDetail == null) {
            captureDetail = new CaptureDetail();
            captureDetail.setMerchantId(mchId);
            captureDetail.setCaptureDate(from);
            captureDetail.setSequentialNo("");
            captureDetail.setCreateTime(getNowTime());
            captureDetail.setMethod("10110");
            captureDetail.setFeeRate(0.19);
            captureDetail.setStatus("wait");
            captureDetail = captureDetailService.save(captureDetail);
        }

        if(!listOfCaptureOrderDetail.isEmpty() && captureDetail != null) {

            captureDetail.setTotalCount(listOfCaptureOrderDetail.size());

            long sumFee = 0;
            for (CaptureOrderDetail elem : listOfCaptureOrderDetail) {
                elem.setCaptureDetail(captureDetail);
                elem.setStatus("done");
                elem.setCaptureDate(captureDetail.getCaptureDate());
                captureOrderDetailService.save(elem);
                sumFee += elem.getTotalFee();
            }
            captureDetail.setTotalFee(sumFee);
            finalCaptureDetail = captureDetailService.save(captureDetail);

// todo: call capture api
//            String captureRequest = getWeixinRequest(map, listOfCaptureOrderDetail);
//
//            String datName = ".dat";
//            String path = "./build/resources/main/";
//
//            BufferedWriter bufWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path+datName, false),"UTF-8"));
//            bufWriter.write(captureRequest);
//            bufWriter.flush();
//            bufWriter.close();

        }else if(captureDetail != null){
            LOGGER.info("[WeiXin] No more Order to be Captured");
            captureDetail.setTotalCount(0);
            captureDetail.setTotalFee(0);
            finalCaptureDetail = captureDetailService.save(captureDetail);

            response = "2000";
        }

        LOGGER.info("[WeiXin] Capture done.");
        return response;
    }

    private String getWeixinRequest(Map<String, String> map, List<CaptureOrderDetail> listOfCaptureOrderDetail) {
        return "";
    }

    public String weixinCaptureResult() throws ParseException, IOException {
        String mchId = "super";
        String rsp = "";
        String to = convertDateFormat(getNowTime(), 8);
        String from = convertDateFormat(dateCalculate(-1, to), 8);
        String rspName = mchId + "_" + from + ".rsp";
        String path = "./build/resources/main/";

        //todo: request the data of captured

        FileReader fr2 = new FileReader(path + rspName);
        BufferedReader br = new BufferedReader(fr2);
        String response = "";

        while(br.ready()) {
            response += br.readLine();
        }
        fr2.close();

        String[] array = response.split("\n");
        List <CaptureOrderDetail> captureOrderDetailList = captureOrderDetailService.listUnSettleCaptureOrder("super", from, to, "10110");

        //todo: update the status of orders

        return rsp;
    }

    public String allpayCapture(String mchId, String from, String to) throws ParseException, IOException {

        LOGGER.info("[AllPay] From " + from + ":000000 to "+ to + ":000000");

        String response = "0000";
        List<CaptureOrderDetail> listOfCaptureOrderDetail = captureOrderDetailService.listByMerchantIdAndDate(mchId, from + "000000", to + "000000", "10500");

        CaptureDetail finalCaptureDetail;
        CaptureDetail captureDetail = captureDetailService.getOne(mchId, from, "10500");
        if(captureDetail == null) {
            captureDetail = new CaptureDetail();
            captureDetail.setMerchantId(mchId);
            captureDetail.setCaptureDate(from);
            captureDetail.setSequentialNo("");
            captureDetail.setCreateTime(getNowTime());
            captureDetail.setMethod("10500");
            captureDetail.setFeeRate(0.19);
            captureDetail.setStatus("wait");
            captureDetail = captureDetailService.save(captureDetail);
        }

        if(!listOfCaptureOrderDetail.isEmpty() && captureDetail != null) {

            captureDetail.setTotalCount(listOfCaptureOrderDetail.size());

            long sumFee = 0;
            for (CaptureOrderDetail elem : listOfCaptureOrderDetail) {
                elem.setCaptureDetail(captureDetail);
                elem.setStatus("done");
                elem.setCaptureDate(captureDetail.getCaptureDate());
                captureOrderDetailService.save(elem);
                sumFee += elem.getTotalFee();
            }
            captureDetail.setTotalFee(sumFee);
            finalCaptureDetail = captureDetailService.save(captureDetail);

// todo: call capture api

        }else if(captureDetail != null){
            LOGGER.info("[AllPay] No more Order to be Captured");
            captureDetail.setTotalCount(0);
            captureDetail.setTotalFee(0);
            finalCaptureDetail = captureDetailService.save(captureDetail);

            response = "2000";
        }

        LOGGER.info("[AllPay] Capture done.");
        return response;
    }

    private String getAllpayRequest(Map<String, String> map, List<CaptureOrderDetail> listOfCaptureOrderDetail) {
        return "";
    }

    public String allpayCaptureResult() throws ParseException, IOException {
        String mchId = "super";
        String rsp = "";
        String to = convertDateFormat(getNowTime(), 8);
        String from = convertDateFormat(dateCalculate(-1, to), 8);
        String rspName = mchId + "_" + from + ".rsp";
        String path = "./build/resources/main/";

        //todo: request the data of captured

        FileReader fr2 = new FileReader(path + rspName);
        BufferedReader br = new BufferedReader(fr2);
        String response = "";

        while(br.ready()) {
            response += br.readLine();
        }
        fr2.close();

        String[] array = response.split("\n");
        List <CaptureOrderDetail> captureOrderDetailList = captureOrderDetailService.listUnSettleCaptureOrder("super", from, to, "10500");

        //todo: update the status of capture orders

        return rsp;
    }

    public  String alipayCapture(String mchId, String from, String to) {
        LOGGER.info("[AliPay] From " + from + ":000000 to "+ to + ":000000");

        String response = "0000";
        List<CaptureOrderDetail> listOfCaptureOrderDetail = captureOrderDetailService.listByMerchantIdAndDate(mchId, from + "000000", to + "000000", "10220");

        CaptureDetail finalCaptureDetail;
        CaptureDetail captureDetail = captureDetailService.getOne(mchId, from, "10220");
        if(captureDetail == null) {
            captureDetail = new CaptureDetail();
            captureDetail.setMerchantId(mchId);
            captureDetail.setCaptureDate(from);
            captureDetail.setSequentialNo("");
            captureDetail.setCreateTime(getNowTime());
            captureDetail.setMethod("10220");
            captureDetail.setFeeRate(0.19);
            captureDetail.setStatus("wait");
            captureDetail = captureDetailService.save(captureDetail);
        }

        if(!listOfCaptureOrderDetail.isEmpty() && captureDetail != null) {

            captureDetail.setTotalCount(listOfCaptureOrderDetail.size());

            long sumFee = 0;
            for (CaptureOrderDetail elem : listOfCaptureOrderDetail) {
                elem.setCaptureDetail(captureDetail);
                elem.setStatus("done");
                elem.setCaptureDate(captureDetail.getCaptureDate());
                captureOrderDetailService.save(elem);
                sumFee += elem.getTotalFee();
            }
            captureDetail.setTotalFee(sumFee);
            finalCaptureDetail = captureDetailService.save(captureDetail);

// todo: call capture api

        }else if(captureDetail != null){
            LOGGER.info("[AliPay] No more Order to be Captured");
            captureDetail.setTotalCount(0);
            captureDetail.setTotalFee(0);
            finalCaptureDetail = captureDetailService.save(captureDetail);

            response = "2000";
        }

        LOGGER.info("[AliPay] Capture done.");
        return response;
    }

    public String tsbCapture(String mchId, String from, String to) throws IOException, ParseException {

        String rsp = "";
        List<CaptureOrderDetail> listOfCaptureOrderDetail = captureOrderDetailService.listUnSettleCaptureOrder(mchId, from+ "230000", to+ "225959", "20800");

        if(!listOfCaptureOrderDetail.isEmpty()) {
            CaptureDetail captureDetail = new CaptureDetail();
            captureDetail.setMerchantId(mchId);
            captureDetail.setCaptureDate(to);
            captureDetail.setSequentialNo("");
            captureDetail.setTotalCount(listOfCaptureOrderDetail.size());
            captureDetail.setCreateTime(getNowTime());
            captureDetail.setMethod("20800");
            captureDetail.setFeeRate(0.66);
            CaptureDetail captureDetail2 = captureDetailService.save(captureDetail);

            for (CaptureOrderDetail elem : listOfCaptureOrderDetail) {
                elem.setCaptureDetail(captureDetail2);
                elem.setStatus("done");
                elem.setCaptureDate(captureDetail2.getCaptureDate());
                captureOrderDetailService.save(elem);
            }
            // todo: call capture api

        }else {
            LOGGER.info("[TsBank] No more Order to be Captured");
            return "2000";
        }
        LOGGER.info("[TsBank] Capture done.");
        return "0000";
    }

    private Map<String,String> getTcbankRequestInfo(TCBCreditCardCaptureRequestHeader request, List<CaptureOrderDetail> listOfCaptureOrderDetail) {
        String header = "";
        String data = "";
        long totalCount = 0;
        long totalFee = 0;
        Map<String,String> map= new HashMap<String,String>();

        String testStr = "";
        testStr = "";
        for (int i = 0; i < 40; i++) {
            testStr += " ";
        }
        for (int i = 0; i < 111; i++) {
            testStr += " ";
        }
        testStr += "\n";

        for (CaptureOrderDetail cd : listOfCaptureOrderDetail) {
            // temp added line
            String createDate = cd.getTradeDetail().getCreateDate() == null? request.getCreateTime() : cd.getTradeDetail().getCreateDate();

            data = data + spaceFiller(/*td.getAccountId()*/"0100829354", 10)
                    + spaceFiller(/*td.getDeviceId()*/"88888888", 8)
                    + spaceFiller(cd.getTradeDetail().getOrderId(), 40)
                    + spaceFiller("", 19)
                    + zeroFiller(String.valueOf(cd.getTradeDetail().getPayment()), 8)
                    + spaceFiller(cd.getTradeDetail().getBarcode()== null? "123456": cd.getTradeDetail().getBarcode(), 8)
                    + "02"
                    + convertDateFormat(createDate, 8)
                    + spaceFiller("extended_columns", 16)
//                + convertWidth(spaceFiller("", 40), "Halfwidth-Fullwidth") + "\n";
                    + testStr;
            totalCount++;
            totalFee += cd.getTradeDetail().getPayment();
        }

        header = request.getHeader() + spaceFiller(request.getMchId(), 10) + convertDateFormat(request.getCreateTime(), 8) + request.getSequentialNo()
                + zeroFiller(String.valueOf(totalCount), 12) + (totalFee >= 0 ? "+":"-") + zeroFiller(String.valueOf(totalFee), 12)
                + spaceFiller("", 216) + "\n";

        map.put("totalCount", String.valueOf(totalCount));
        map.put("totalFee", String.valueOf(totalFee));
        map.put("requestData", header + data);

        return map;
    }

    private TCBCreditCardCaptureResponseData transformToTCBCaptureResponse(String response) {
        TCBCreditCardCaptureResponseData result = new TCBCreditCardCaptureResponseData();
        result.setMchId(response.substring(0,10));
        result.setDeviceInfo(response.substring(10,18));
        result.setStoreOrderNo(response.substring(18,58));
        // space 58,77
        result.setTotalFee(response.substring(77,85));
        result.setAuthCode(response.substring(85,93));
        result.setCloseType(response.substring(93,95));
        result.setEstablishedAt(response.substring(95,103));
        result.setExtended(response.substring(103,119));
        result.setCardOwnerInfo(response.substring(119,159));
        result.setAcceptedAt(response.substring(159,165));
        result.setStatusCode(response.substring(165,168));
        result.setStatusMsg(response.substring(168,184));
        result.setBatchAndSeq(response.substring(184,190));
        result.setTransMode(response.substring(190,191));
        result.setInstallment(response.substring(191,193).equals("  ")? 0 : Integer.parseInt(response.substring(191,193)));
        result.setInstFirst(response.substring(193,201));
        result.setInstEach(response.substring(201,209));
        result.setInstFee(response.substring(209,215));
        result.setRedeemUsed(response.substring(215,223));
        result.setBalanceSign(response.substring(223,224));
        result.setRedeemBalance(response.substring(224,232));
        result.setCreditAmt(response.substring(232,242));
        result.setPaidAt(response.substring(242,250));
        result.setSecureStatus(response.substring(250,251));
        result.setForeign(response.substring(251,252));
        result.setReserved(response.substring(252,270));
        return result;
    }

    private String dateCalculate(int days, String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date dt = sdf.parse(date);
        Calendar cd = Calendar.getInstance();
        cd.setTime(dt);
        cd.add(Calendar.DAY_OF_YEAR,days);
        Date dt1 = cd.getTime();
        String str = sdf.format(dt1);

        return str;
    }

    private String getNowTime() {
        SimpleDateFormat sdf1 = new SimpleDateFormat(SystemInstance.DATE_PATTERN);
        String str = "";
        str = sdf1.format(new Date());
        return str;
    }

    private String dateFormatParameter(int ref) {
        String pattern = "";
        if(ref == 14) {
            pattern = SystemInstance.DATE_PATTERN;
        }else if(ref == 12) {
            pattern = "yyyyMMddhhmm";
        }else if(ref == 8) {
            pattern = "yyyyMMdd";
        }else if(ref == 6) {
            pattern = "yyMMdd";
        }
        return pattern;
    }

    private String convertDateFormat(String dateTime, int to) {
        String pattern1 = dateFormatParameter(dateTime.length());
        String pattern2 = dateFormatParameter(to);

        SimpleDateFormat sdf1 = new SimpleDateFormat(pattern1);
        SimpleDateFormat sdf2 = new SimpleDateFormat(pattern2);
        String str = "";
        try {
            Date d = sdf1.parse(dateTime);
            str = sdf2.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private String spaceFiller(String data, int column) {
        int length = column - data.length();
        for (int i = 0; i < length ; i++) {
            data += " ";
        }
        return data;
    }

    private String zeroFiller(String data, int column) {
        int length = column - data.length();
        for (int i = 0; i < length ; i++) {
            data = "0" + data;
        }
        return data;
    }

    public void test(String merchantId, String method, String from, String to) {
        System.out.println("hi");
    }
}
