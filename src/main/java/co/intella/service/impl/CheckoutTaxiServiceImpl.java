package co.intella.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.assertj.core.util.Strings;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import co.intella.domain.CheckoutCtrlVo;
import co.intella.domain.contratStore.OperationHistory;
import co.intella.domain.contratStore.RefundDetailVo;
import co.intella.domain.contratStore.TradeDetailVo;
import co.intella.domain.easycard.EzCardBasicRequest;
import co.intella.domain.easycard.EzCardBasicResponse;
import co.intella.domain.easycard.EzCardBatchCheckoutForTaxi;
import co.intella.domain.easycard.EzCardBatchCheckoutResponse;
import co.intella.domain.easycard.EzcSettlementReceive;
import co.intella.domain.easycard.EzcTXNCount;
import co.intella.domain.easycard.EzcTXNData;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.model.Device;
import co.intella.model.Merchant;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import co.intella.model.ReserveCancel;
import co.intella.model.ReserveDetail;
import co.intella.model.TradeDetail;
import co.intella.net.HttpRequestUtil;
import co.intella.service.CheckoutCustomService;
import co.intella.service.DeviceService;
import co.intella.service.OperationHistoryService;
import co.intella.service.TradeDetailService;
import co.intella.utility.SystemInstance;

@Service
public class CheckoutTaxiServiceImpl  extends CheckoutCustomService{

    private final static String TX_COUNT_POINT = "txCountPoint";

    private final static String TX_AMOUNT_POINT = "txAmountPoint";

    private final static String TX_COUNT_TRIP_FOR_DRIVER = "txCountTripForDriver";

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Value("${host.request.ezcTaxi}")
    private String taxiServiceHost;

    @Value("${host.request.ezc}")
    private String HOST;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private OperationHistoryService operationHistoryService;

    @Override
    public void computeCheckout(Map<String, Object> checkOutObj) {
        long txCountTrade = 0;
        long txCountRefund = 0;
        long txCountReserve = 0;
        long txCountCancel = 0;
        long txCountAutoReserve = 0;
        long txCountCashReserve = 0;
        long txCountPoint = 0;
        long txCountTripForDriver = 0;

        long txAmountTrade = 0;
        long txAmountRefund = 0;
        long txAmountReserve = 0;
        long txAmountCancel = 0;
        long txAmountAutoReserve = 0;
        long txAmountCashReserve = 0;
        long txAmountPoint = 0;

        @SuppressWarnings("unchecked")
        List<TradeDetailVo> tradeDetails = (List<TradeDetailVo>) checkOutObj.get(CheckoutServiceImpl.TRADE_DETAILS);
        @SuppressWarnings("unchecked")
        List<ReserveDetail> reserveDetails = (List<ReserveDetail>) checkOutObj.get(CheckoutServiceImpl.RESERVE_DETAILS);
        @SuppressWarnings("unchecked")
        List<RefundDetailVo> refundDetails = (List<RefundDetailVo>) checkOutObj.get(CheckoutServiceImpl.REFUND_DETAILS);
        @SuppressWarnings("unchecked")
        List<ReserveCancel> reserveCancels = (List<ReserveCancel>) checkOutObj.get(CheckoutServiceImpl.RESERVE_CANCELS);

        List<EzcTXNData> txnDataList = new ArrayList<EzcTXNData>();

        if (tradeDetails != null) {
            for (TradeDetailVo tx : tradeDetails) {
                if (tx.getStatus().equals(SystemInstance.TRADE_SUCCESS)) {
                    LOGGER.info("tradeDetails : txn " + tx.getEzcTxnDate());

                    if (tx.getPayment() > 0 && tx.getCarePoint() > 0) {
                        txAmountTrade = txAmountTrade + tx.getPayment();
                        txCountTrade = txCountTrade + 1;
                        txAmountPoint = txAmountPoint + tx.getCarePoint();
                        txCountPoint = txCountPoint + 1;

                        txCountTripForDriver = txCountTripForDriver + 1;
                    } else if (tx.getCarePoint() > 0 && tx.getPayment() == 0) {
                        txAmountPoint = txAmountPoint + tx.getCarePoint();
                        txCountPoint = txCountPoint + 1;
                        txCountTripForDriver = txCountTripForDriver + 1;
                    } else if (tx.getPayment() > 0 && tx.getCarePoint() == 0) {
                        txAmountTrade = txAmountTrade + tx.getPayment();
                        txCountTrade = txCountTrade + 1;
                        txCountTripForDriver = txCountTripForDriver + 1;
                    } else if (tx.getPayment() == 0 && tx.getCarePoint() == 0) {
                        txCountTripForDriver = txCountTripForDriver + 1;
                    }

                    EzcTXNData ezcTXNData = new EzcTXNData();
                    ezcTXNData.setTxnType("Payment");
                    ezcTXNData.setAmount(String.valueOf(tx.getPayment()));
                    ezcTXNData.setCardNumber(tx.getUserId());

                    ezcTXNData.setDate(
                            Strings.isNullOrEmpty(tx.getEzcTxnDate()) ? "" : tx.getEzcTxnDate().substring(0, 8));
                    ezcTXNData.setTerminalTXNNumber(tx.getSystemOrderId());
                    ezcTXNData
                            .setTime(Strings.isNullOrEmpty(tx.getEzcTxnDate()) || tx.getEzcTxnDate().length() < 14 ? ""
                                    : tx.getEzcTxnDate().substring(8, 14));
                    txnDataList.add(ezcTXNData);
                }
            }
        }

        if (reserveDetails != null) {
            for (ReserveDetail tx : reserveDetails) {
                if (tx.getStatus().equals("0")) {
                    txAmountReserve = txAmountReserve + tx.getAmount();
                    txCountReserve = txCountReserve + 1;
                    LOGGER.info("reserveDetails : txn " + tx.getEzcTxnDate());

                    if (tx.getIsAuto() == 1) {
                        txAmountAutoReserve = txAmountAutoReserve + tx.getAmount();
                        txCountAutoReserve = txCountAutoReserve + 1;
                        EzcTXNData ezcTXNData = new EzcTXNData();
                        ezcTXNData.setTxnType("AutoCashTopUp");
                        ezcTXNData.setAmount(String.valueOf(tx.getAmount()));
                        ezcTXNData.setCardNumber(tx.getUserId());

                        String txnDate = Strings.isNullOrEmpty(tx.getEzcTxnDate()) ? ""
                                : tx.getEzcTxnDate().substring(0, 8);
                        String txnTime = Strings.isNullOrEmpty(tx.getEzcTxnDate()) || tx.getEzcTxnDate().length() < 14
                                ? ""
                                : tx.getEzcTxnDate().substring(8, 14);

                        ezcTXNData.setDate(txnDate);
                        ezcTXNData.setTerminalTXNNumber(txnTime);
                        ezcTXNData.setTime(txnTime);
                        txnDataList.add(ezcTXNData);
                    } else {
                        txAmountCashReserve = txAmountCashReserve + tx.getAmount();
                        txCountCashReserve = txCountCashReserve + 1;

                        EzcTXNData ezcTXNDataReserve = new EzcTXNData();
                        ezcTXNDataReserve.setTxnType("CashTopUp");
                        ezcTXNDataReserve.setAmount(String.valueOf(tx.getAmount()));
                        ezcTXNDataReserve.setCardNumber(tx.getUserId());

                        String txnDate = Strings.isNullOrEmpty(tx.getEzcTxnDate()) ? ""
                                : tx.getEzcTxnDate().substring(0, 8);
                        String txnTime = Strings.isNullOrEmpty(tx.getEzcTxnDate()) || tx.getEzcTxnDate().length() < 14
                                ? ""
                                : tx.getEzcTxnDate().substring(8, 14);

                        ezcTXNDataReserve.setDate(txnDate);
                        ezcTXNDataReserve.setTerminalTXNNumber(txnTime);
                        ezcTXNDataReserve.setTime(txnTime);
                        txnDataList.add(ezcTXNDataReserve);
                    }
                }
            }
        }

        if (reserveCancels != null) {
            for (ReserveCancel tx : reserveCancels) {
                if (tx.getStatus().equals("0")) {
                    LOGGER.info("ReserveCancel : txn " + tx.getEzcTxnDate());
                    txAmountCancel = txAmountCancel + tx.getAmount();
                    txCountCancel = txCountCancel + 1;

                    EzcTXNData ezcTXNData = new EzcTXNData();
                    ezcTXNData.setTxnType("CancelCashTopUp");
                    ezcTXNData.setAmount(String.valueOf(tx.getAmount()));
                    ezcTXNData.setCardNumber(tx.getUserId());

                    String txnDate = Strings.isNullOrEmpty(tx.getEzcTxnDate()) ? ""
                            : tx.getEzcTxnDate().substring(0, 8);
                    String txnTime = Strings.isNullOrEmpty(tx.getEzcTxnDate()) || tx.getEzcTxnDate().length() < 14 ? ""
                            : tx.getEzcTxnDate().substring(8, 14);

                    ezcTXNData.setDate(txnDate);
                    ezcTXNData.setTerminalTXNNumber(txnTime);
                    ezcTXNData.setTime(txnTime);
                    txnDataList.add(ezcTXNData);
                }
            }
        }

        if (refundDetails != null) {
            for (RefundDetailVo tx : refundDetails) {
                if (tx.getStatus().equals(SystemInstance.TRADE_REFUND_SUCCESS)) {
                    LOGGER.info("RefundDetailVo : txn " + tx.getEzcTxnDate());
                    txAmountRefund = txAmountRefund + tx.getAmount();
                    txCountRefund = txCountRefund + 1;

                    EzcTXNData ezcTXNData = new EzcTXNData();
                    ezcTXNData.setTxnType(tx.getEzcCancel() ? "CancelPayment" : "Refund");
                    ezcTXNData.setAmount(String.valueOf(tx.getAmount()));

                    TradeDetail tradeDetail = tradeDetailService.getOne(tx.getOrderId());
                    ezcTXNData.setCardNumber(tradeDetail.getUserId());

                    String txnDate = Strings.isNullOrEmpty(tx.getEzcTxnDate()) ? ""
                            : tx.getEzcTxnDate().substring(0, 8);
                    String txnTime = Strings.isNullOrEmpty(tx.getEzcTxnDate()) || tx.getEzcTxnDate().length() < 14 ? ""
                            : tx.getEzcTxnDate().substring(8, 14);

                    ezcTXNData.setDate(txnDate);
                    ezcTXNData.setTerminalTXNNumber(tx.getSystemRefundId());
                    ezcTXNData.setTime(txnTime);
                    txnDataList.add(ezcTXNData);
                }
            }
        }

        LOGGER.info("[EZC][Taxi] ---------- ");
        LOGGER.info("[EZC][Taxi] settlement trade count: " + txCountTrade);
        LOGGER.info("[EZC][Taxi] settlement trade amount: " + txAmountTrade);
        LOGGER.info("[EZC][Taxi] ---------- ");
        LOGGER.info("[EZC][Taxi] settlement refund count: " + txCountRefund);
        LOGGER.info("[EZC][Taxi] settlement refund amount: " + txAmountRefund);
        LOGGER.info("[EZC][Taxi] ---------- ");
        LOGGER.info("[EZC][Taxi] settlement reserve count: " + txCountReserve);
        LOGGER.info("[EZC][Taxi] settlement reserve amount: " + txAmountReserve);
        LOGGER.info("[EZC][Taxi] ---------- ");
        LOGGER.info("[EZC][Taxi] settlement reserve cancel count: " + txCountCancel);
        LOGGER.info("[EZC][Taxi] settlement reserve cancel amount: " + txAmountCancel);
        LOGGER.info("[EZC][Taxi] ---------- ");
        LOGGER.info("[EZC][Taxi] settlement Point count: " + txCountPoint);
        LOGGER.info("[EZC][Taxi] settlement Point amount: " + txAmountPoint);
        LOGGER.info("[EZC][Taxi] ---------- ");
        LOGGER.info("[EZC][Taxi] settlement Trip count: " + txCountTripForDriver);
        String settlementResult = "TXC:" + txCountTrade + " | TX:" + txAmountTrade + " | RFC:" + txCountRefund
                + " | RF:" + txAmountRefund + " | RSC:" + txCountReserve + " | RS:" + txAmountReserve + " | RCC:"
                + txCountCancel + " | RC:" + txAmountCancel;
        checkOutObj.put(CheckoutServiceImpl.TX_COUNT_TRADE, txCountTrade);
        checkOutObj.put(CheckoutServiceImpl.TX_AMOUNT_TRADE, txAmountTrade);
        checkOutObj.put(CheckoutServiceImpl.TX_COUNT_REFUND, txCountRefund);
        checkOutObj.put(CheckoutServiceImpl.TX_AMOUNT_REFUND, txAmountRefund);
        checkOutObj.put(CheckoutServiceImpl.TX_COUNT_RESERVE, txCountReserve);
        checkOutObj.put(CheckoutServiceImpl.TX_AMOUNT_RESERVE, txAmountReserve);
        checkOutObj.put(CheckoutServiceImpl.TX_COUNT_CANCEL, txCountCancel);
        checkOutObj.put(CheckoutServiceImpl.TX_AMOUNT_CANCEL, txAmountCancel);
        checkOutObj.put(TX_COUNT_POINT, txCountPoint);
        checkOutObj.put(TX_AMOUNT_POINT, txAmountPoint);
        checkOutObj.put(TX_COUNT_TRIP_FOR_DRIVER, txCountTripForDriver);
        checkOutObj.put(CheckoutServiceImpl.SETTLEMENT_RESULT, settlementResult);
        checkOutObj.put(CheckoutServiceImpl.TX_COUNT_AUTO_RESERVE, txCountAutoReserve);
        checkOutObj.put(CheckoutServiceImpl.TX_COUNT_CASH_RESERVE, txCountCashReserve);
        checkOutObj.put(CheckoutServiceImpl.TX_AMOUNT_AUTO_RESERVE, txAmountAutoReserve);
        checkOutObj.put(CheckoutServiceImpl.TX_AMOUNT_CASH_RESERVE, txAmountCashReserve);
        checkOutObj.put(CheckoutServiceImpl.TXN_DATA_LIST, txnDataList);
    }

    @Override
    public String executeCheckout(Map<String, Object> checkOutObj) {
        Merchant merchant = (Merchant) checkOutObj.get(CheckoutServiceImpl.MERCHANT);
        Device device = (Device) checkOutObj.get(CheckoutServiceImpl.DEVICE);
        PaymentAccount paymentAccount = (PaymentAccount) checkOutObj.get(CheckoutServiceImpl.PAYMENT_ACCOUNT);
        long txCountTripForDriver = (long) checkOutObj.get(TX_COUNT_TRIP_FOR_DRIVER);
        long txCountRefund = (long) checkOutObj.get(CheckoutServiceImpl.TX_COUNT_REFUND);
        long txCountReserve = (long) checkOutObj.get(CheckoutServiceImpl.TX_COUNT_RESERVE);
        long txCountCancel = (long) checkOutObj.get(CheckoutServiceImpl.TX_COUNT_CANCEL);
        long txAmountTrade = (long) checkOutObj.get(CheckoutServiceImpl.TX_AMOUNT_TRADE);
        long txAmountRefund = (long) checkOutObj.get(CheckoutServiceImpl.TX_AMOUNT_REFUND);
        long txAmountReserve = (long) checkOutObj.get(CheckoutServiceImpl.TX_AMOUNT_RESERVE);
        long txAmountCancel = (long) checkOutObj.get(CheckoutServiceImpl.TX_AMOUNT_CANCEL);
        long txAmountPoint = (long) checkOutObj.get(TX_AMOUNT_POINT);
        long txCountPoint = (long) checkOutObj.get(TX_COUNT_POINT);
        long txCountTrade = (long) checkOutObj.get(CheckoutServiceImpl.TX_COUNT_TRADE);
        long txCountAutoReserve = (long) checkOutObj.get(CheckoutServiceImpl.TX_COUNT_AUTO_RESERVE);
        long txCountCashReserve = (long) checkOutObj.get(CheckoutServiceImpl.TX_COUNT_CASH_RESERVE);
        long txAmountAutoReserve = (long) checkOutObj.get(CheckoutServiceImpl.TX_AMOUNT_AUTO_RESERVE);
        long txAmountCashReserve = (long) checkOutObj.get(CheckoutServiceImpl.TX_AMOUNT_CASH_RESERVE);
        String settlementResult = (String) checkOutObj.get(CheckoutServiceImpl.SETTLEMENT_RESULT);
        @SuppressWarnings("unchecked")
        List<EzcTXNData> txnDataList = (List<EzcTXNData>) checkOutObj.get(CheckoutServiceImpl.TXN_DATA_LIST);
        RequestHeader requestHeader = (RequestHeader) checkOutObj.get(CheckoutServiceImpl.REQUEST_HEADER);
        CheckoutCtrlVo checkoutCtrlVo = (CheckoutCtrlVo) checkOutObj.get(CheckoutServiceImpl.CHECKOUT_CTRL_VO);
        String date = getDate();
        String time = getTime();

        EzCardBatchCheckoutForTaxi ezCardBatchCheckoutForTaxi = new EzCardBatchCheckoutForTaxi();
        ezCardBatchCheckoutForTaxi.setTerminalId(paymentAccount.getAccount());
        ezCardBatchCheckoutForTaxi.setDeviceId(device.getOwnerDeviceId());

        ezCardBatchCheckoutForTaxi.setTerminalTXNNumber(time);
        ezCardBatchCheckoutForTaxi.setHostSerialNumber(String.format("%06d", (Long.valueOf(device.getHostSerialNumber()) + 1)));
        ezCardBatchCheckoutForTaxi.setTime(time);
        ezCardBatchCheckoutForTaxi.setDate(date);
        ezCardBatchCheckoutForTaxi.setTotalTXNCount(
                String.valueOf(txCountTripForDriver + txCountRefund + txCountReserve + txCountCancel));
        ezCardBatchCheckoutForTaxi
                .setTotalAmount(String.valueOf(txAmountTrade + txAmountRefund + txAmountReserve + txAmountCancel));
        ezCardBatchCheckoutForTaxi.setTotalPoint(String.valueOf(txAmountPoint));
        ezCardBatchCheckoutForTaxi.setTotalPointCount(String.valueOf(txCountPoint));

        ezCardBatchCheckoutForTaxi.setBatchNumber(String.valueOf(checkoutCtrlVo.getBatchNo()));
        EzCardBatchCheckoutResponse response = new Gson().fromJson(
                customRequest(ezCardBatchCheckoutForTaxi, device.getAesKey()), EzCardBatchCheckoutResponse.class);
        response.setTxRefundAmount(txAmountRefund);
        response.setTxReserveAmount(txAmountReserve);
        response.setTxTradeAmount(txAmountTrade);
        response.setTxCancelAmount(txAmountCancel);
        response.setTxPointAmount(txAmountPoint);

        response.setTxRefundCount(txCountRefund);
        response.setTxReserveCount(txCountReserve);
        response.setTxTradeCount(txCountTrade);
        response.setTxCancelCount(txCountCancel);
        response.setTxPointCount(txCountPoint);

        response.setTxCountTripForDriver(txCountTripForDriver);

        response.setCarFleetName(merchant.getCompanyName());
        response.setLicensePlate(device.getLicensePlate());
        response.setDriverNumber(merchant.getAccountId());
        response.setBatchNumber(String.valueOf(checkoutCtrlVo.getBatchNo()));
        response.setDeviceNumber(paymentAccount.getAccount() + device.getOwnerDeviceId());

        if (!Strings.isNullOrEmpty(response.getNewAESKey())) {
            device.setEzcDongleId(response.getDongleDeviceID());
            device.setAesKey(response.getNewAESKey());
        }

        if (!Strings.isNullOrEmpty(response.getHostSerialNumber())) {
            device.setHostSerialNumber(response.getHostSerialNumber());
        } else {
            device.setHostSerialNumber(String.format("%06d", (Long.valueOf(device.getHostSerialNumber()) + 1)));
        }
        deviceService.save(device);

        LOGGER.info("[CHECKOUT][Taxi] " + new Gson().toJson(response));
        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                settlementResult,
                response.getTxnResult() + "|" + device.getOwnerDeviceId() + "|" + checkoutCtrlVo.getBatchNo());

        if (response.getTxnResult().equals(CheckoutServiceImpl.SUCCESS)) {
            List<EzcTXNCount> txnCountsReceipt = new ArrayList<EzcTXNCount>();
            EzcTXNCount ezcTXNCount = new EzcTXNCount();
            ezcTXNCount.setPaymentCount(String.valueOf(txCountTrade));
            ezcTXNCount.setRefundCount(String.valueOf(txCountRefund));
            ezcTXNCount.setAutoTopUpCount(String.valueOf(txCountAutoReserve));
            ezcTXNCount.setPaymentTotal(String.valueOf(txAmountTrade));
            ezcTXNCount.setRefundTotal(String.valueOf(txAmountRefund));
            ezcTXNCount.setAutoTopUpTotal(String.valueOf(txAmountAutoReserve));

            ezcTXNCount.setCashTopUpCount(String.valueOf(txCountCashReserve));
            ezcTXNCount.setCashTopUpTotal(String.valueOf(txAmountCashReserve));
            ezcTXNCount.setCancelReserveCount(String.valueOf(txCountCancel));
            ezcTXNCount.setCancelReserveTotal(String.valueOf(txAmountCancel));

            txnCountsReceipt.add(ezcTXNCount);

            LOGGER.info("[EZC][Taxi][Settlement][Print][input] " + new Gson().toJson(response));
            EzcSettlementReceive ezcSettlementReceive = new Gson().fromJson(new Gson().toJson(response),
                    EzcSettlementReceive.class);
            ezcSettlementReceive.setDeviceId(device.getOwnerDeviceId());
            ezcSettlementReceive.setTerminalId(paymentAccount.getAccount());
            ezcSettlementReceive.setPrintId("01");
            ezcSettlementReceive.setTxnCount(txnCountsReceipt);
            ezcSettlementReceive.setTxnList(txnDataList);

            ezcSettlementReceive.setDate(date);
            ezcSettlementReceive.setTime(time);
            ezcSettlementReceive.setDeviceNumber(paymentAccount.getAccount() + device.getOwnerDeviceId());
            ezcSettlementReceive.setBatchNumber(String.valueOf(checkoutCtrlVo.getBatchNo()));

            String printResponse = doRequest(ezcSettlementReceive, device.getAesKey());
            LOGGER.info("[CHECKOUT][Taxi][Settlement][Print][output] " + printResponse);
        }
        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    private String customRequest(EzCardBasicRequest ezCardBasicRequest, String aesKey) {
        LOGGER.info("[EZC] doRequest(). " + aesKey);
        ezCardBasicRequest.setAesKey(aesKey);
        String requestUrl = taxiServiceHost + new Gson().toJson(ezCardBasicRequest);

        LOGGER.info("[EZC] doRequest(). " + ezCardBasicRequest.getServiceType() + " -> " + requestUrl);

        try {

            String response;
            if (requestUrl.startsWith("https")) {
                response = HttpRequestUtil.httpsGet(requestUrl);
            } else {
                response = HttpRequestUtil.httpGet(requestUrl);
            }
            LOGGER.info("[EZC][RESPONSE] doRequest(). " + response);
            return response;
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
            EzCardBasicResponse errorResponse = new EzCardBasicResponse();
            errorResponse.setErrorCode("909995");
            errorResponse.setTxnResult("Fail");
            return new Gson().toJson(errorResponse);
        }
    }

    private String getGeneralResponse(RequestHeader requestHeader, EzCardBasicResponse response, String errorCode) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setStatusCode(errorCode); // 0001
        responseGeneralHeader.setStatusDesc("STATUS_SUCCESS");
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setMchId(requestHeader.getMerchantId());
        responseGeneralHeader.setMethod(requestHeader.getMethod());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        responseGeneralBody.setData(response);
        responseGeneralBody.setHeader(responseGeneralHeader);

        return new Gson().toJson(responseGeneralBody);
    }

    private String doRequest(EzCardBasicRequest ezCardBasicRequest, String aesKey) {

        LOGGER.info("[EZC] doRequest(). " + aesKey);
        ezCardBasicRequest.setAesKey(aesKey);
        String requestUrl = HOST + new Gson().toJson(ezCardBasicRequest);

        LOGGER.info("[EZC] doRequest(). " + ezCardBasicRequest.getServiceType() + " -> " + requestUrl);

        try {

            String response;
            if (requestUrl.startsWith("https")) {
                response = HttpRequestUtil.httpsGet(requestUrl);
            } else {
                response = HttpRequestUtil.httpGet(requestUrl);
            }
            LOGGER.info("[EZC][RESPONSE] doRequest(). " + response);
            return response;
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
            EzCardBasicResponse errorResponse = new EzCardBasicResponse();
            errorResponse.setErrorCode("909995");
            errorResponse.setTxnResult("Fail");
            return new Gson().toJson(errorResponse);
        }
    }

    @Async
    private void writeDBLog(String merchantId, String serviceType, String method, String instruction, String response) {
        OperationHistory operationHistory = new OperationHistory();
        operationHistory.setServiceType(serviceType);
        operationHistory.setMethod(method);
        operationHistory.setMerchantId(merchantId);
        operationHistory.setInstruction(instruction);
        operationHistory.setResponse(response);
        operationHistoryService.save(operationHistory);
    }

    private String getDate() {
        DateTime dateTime = DateTime.now();
        return String.valueOf(dateTime.getYear()) + String.format("%02d", dateTime.getMonthOfYear())
                + String.format("%02d", dateTime.getDayOfMonth());
    }

    private String getTime() {
        DateTime dateTime = DateTime.now();
        return String.format("%02d", dateTime.getHourOfDay()) + String.format("%02d", dateTime.getMinuteOfHour())
                + String.format("%02d", dateTime.getSecondOfMinute());
    }
}
