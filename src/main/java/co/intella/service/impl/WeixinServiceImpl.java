package co.intella.service.impl;

import co.intella.crypto.KeyReader;
import co.intella.domain.weixin.*;
import co.intella.net.HttpRequestUtil;
import co.intella.service.WeixinService;
import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.security.PrivateKey;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Miles Wu
 */
@Service
@Transactional
public class WeixinServiceImpl implements WeixinService {

    private final Logger LOGGER = LoggerFactory.getLogger(WeixinServiceImpl.class);

    @Value("${host.request.weixin.php}")
    private String HOST;

    private static PrivateKey PRIVATE_KEY;

    public String payment(WeixinRequestParameter request) throws Exception {

        Map<String, String> requestMap = new HashMap<String, String>();
        // general items
        requestMap.put("ver", "101");
        requestMap.put("method", "10");
        requestMap.put("mch_id", request.getP());
        requestMap.put("nonce_str", request.getR());
        // items corresponding to trade_type
        requestMap.put("trade_type", "JSAPI");
        requestMap.put("time_expire", "");
        requestMap.put("out_trade_no", request.getP2());
        requestMap.put("device_info", request.getP1());
        requestMap.put("body", "");
        requestMap.put("total_fee", request.getP3());
        requestMap.put("fee_type", "TWD");
        requestMap.put("detail", "{}");

        LOGGER.info("[TRANSACTION] weixin payment start." + new Gson().toJson(requestMap));
        String response = HttpRequestUtil.post(HOST, requestMap, getPrivateKey());
        LOGGER.info("[TRANSACTION] weixin payment complete." + response);
        return response;
    }

    public String faceToFace(WeixinFtfRequestData request) throws Exception {
        Map<String, String> requestMap = new HashMap<String, String>();

        //commom
        requestMap.put("ver", request.getVer());
        requestMap.put("method", request.getMethod());
        requestMap.put("mch_id", request.getMchId());
        requestMap.put("nonce_str", request.getNonceStr());
        //WeixinFtfRequestData
        requestMap.put("time_expire", request.getTime_expire());
        requestMap.put("trade_type", request.getTrade_type());

        LOGGER.info("[TRANSACTION] weixin faceToFace start. " + new Gson().toJson(requestMap));
        String result = HttpRequestUtil.post(HOST, requestMap, getPrivateKey());
        LOGGER.info("[TRANSACTION] weixin faceToFace complete. " + result);
        return result;
    }

    public String callJsApi(WeixinJsapiRequestData request) throws Exception {
        Map<String, String> requestMap = new HashMap<String, String>();
        //commom
        requestMap.put("ver", request.getVer());
        requestMap.put("method", request.getMethod());
        requestMap.put("mch_id", request.getMchId());
        requestMap.put("nonce_str", request.getNonceStr());

        //JSAPI
        requestMap.put("time_expire", request.getTimeExpire());
        requestMap.put("trade_type", request.getTradeType());
        requestMap.put("out_trade_no", request.getOutTradeNo());
        requestMap.put("body", request.getBody());
        requestMap.put("fee_type", request.getFeeType());
        requestMap.put("total_fee", request.getTotalFee());
        requestMap.put("detail", request.getDetail());
        requestMap.put("device_info", request.getDeviceInfo());

        LOGGER.info("[TRANSACTION] weixin callJsApi start. " + new Gson().toJson(requestMap));
        String result = HttpRequestUtil.post(HOST, requestMap, getPrivateKey());
        LOGGER.info("[TRANSACTION] weixin callJsApi complete. " + result);
        return result;
    }

    public String callJsApiNoReturn(WeixinJsapiRequestData request) throws Exception {
        Map<String, String> map = new HashMap<String, String>();
        //commom
        map.put("ver", request.getVer());
        map.put("method", request.getMethod());
        map.put("mch_id", request.getMchId());
        map.put("nonce_str", request.getNonceStr() == null ? "" : request.getNonceStr());

        //JSAPI
        map.put("time_expire", request.getTimeExpire());
        map.put("trade_type", request.getTradeType());
        map.put("out_trade_no", request.getOutTradeNo());
        map.put("body", request.getBody() == null ? "" : request.getBody());
        map.put("fee_type", request.getFeeType() == null ? "TWD" : request.getFeeType());
        map.put("total_fee", request.getTotalFee());
        if (request.getDetail() != null)
            map.put("detail", request.getDetail() == null ? "" : request.getDetail());
        map.put("device_info", request.getDeviceInfo());

        LOGGER.info("[TRANSACTION] weixin callJsApiNoReturn start. " + new Gson().toJson(map));
        String result = HttpRequestUtil.post(HOST, map, getPrivateKey());
        LOGGER.info("[TRANSACTION] weixin callJsApiNoReturn complete. " + result);
        return result;
    }

    public String micropay(WeixinMicropayRequestData request) throws Exception {
        Map<String, String> requestMap = new HashMap<String, String>();
        // general items
        requestMap.put("ver", request.getVer());
        requestMap.put("method", request.getMethod());
        requestMap.put("mch_id", request.getMchId());
        requestMap.put("nonce_str", request.getNonceStr() == null ? "" : request.getNonceStr());
        // items corresponding to trade_type
        requestMap.put("trade_type", request.getTradeType());
        requestMap.put("out_trade_no", request.getOutTradeNo());
        requestMap.put("device_info", request.getDeviceInfo());
        requestMap.put("body", request.getBody() == null ? "" : request.getBody());
        requestMap.put("total_fee", request.getTotalFee());
        requestMap.put("fee_type", request.getFeeType() == null ? "TWD" : request.getFeeType());
        requestMap.put("detail", request.getDetail() == null ? "" : request.getDetail());
        requestMap.put("attach", request.getAttach() == null ? "" : request.getAttach());
        requestMap.put("goods_tag", request.getGoodsTag() == null ? "" : request.getGoodsTag());
        requestMap.put("auth_code", request.getAuthCode());

        LOGGER.info("[TRANSACTION] weixin micropay start. " + new Gson().toJson(requestMap));
        String result = HttpRequestUtil.post(HOST, requestMap, getPrivateKey());
        LOGGER.info("[TRANSACTION] weixin micropay complete. " + result);
        return result;
    }

    public String orderQuery(WeixinOrderQueryRequestData request) throws Exception {
        Map<String, String> map = new HashMap<String, String>();
        //commom
        map.put("ver", request.getVer());
        map.put("method", request.getMethod());
        map.put("mch_id", request.getMchId());
        map.put("nonce_str", request.getNonceStr());
        //WeixinOrderQueryRequestData
        map.put("trade_type", request.getTradeType());
        map.put("device_info", request.getDeviceInfo());
        map.put("start_date", request.getStartDate());
        map.put("end_date", request.getEndDate());

        LOGGER.info("[TRANSACTION] weixin orderQuery start. " + new Gson().toJson(map));
        String result = HttpRequestUtil.post(HOST, map, getPrivateKey());
        LOGGER.info("[TRANSACTION] weixin orderQuery complete. " + result);
        return result;
    }

    public String refund(WeixinRefundRequestData request) throws Exception {
        Map<String, String> map = new HashMap<String, String>();

        //commom
        map.put("ver", request.getVer());
        map.put("method", request.getMethod());
        map.put("mch_id", request.getMchId());
        map.put("nonce_str", request.getNonceStr() == null ? "" : request.getNonceStr());
        //WeixinOrderQueryRequestData
        map.put("trade_type", request.getTradeType());
        map.put("out_refund_no", request.getOutRefundNo());

        map.put("out_trade_no", request.getOutTradeNo() == null ? "" : request.getOutTradeNo());
        map.put("transaction_id", request.getTransactionId());

        map.put("device_info", request.getDeviceInfo());
        map.put("total_fee", request.getTotalFee());
        map.put("refund_fee_type", StringUtils.defaultIfBlank(request.getRefundFeeType(), "TWD"));
        map.put("refund_fee", request.getRefundFee());

        LOGGER.info("[TRANSACTION] weixin refund start. " + new Gson().toJson(map));
        String result = HttpRequestUtil.post(HOST, map, getPrivateKey());
        LOGGER.info("[TRANSACTION] weixin refund complete. " + result);
        return result;
    }

    public String singleQuery(WeixinSingleQueryRequestData input) throws Exception {
        Map<String, String> map = new HashMap<String, String>();

        //commom
        map.put("ver", input.getVer());
        map.put("method", input.getMethod());
        map.put("mch_id", input.getMchId());
        map.put("nonce_str", input.getNonceStr() == null ? "" : input.getNonceStr());
        //WeixinOrderQueryRequestData
        map.put("trade_type", input.getTradeType());
        map.put("out_trade_no", input.getOutTradeNo());

        LOGGER.info("[TRANSACTION] weixin singleQuery start. " + new Gson().toJson(map));
        String result = HttpRequestUtil.post(HOST, map, getPrivateKey());
        LOGGER.info("[TRANSACTION] weixin singleQuery complete. " + result);
        return result;
    }

    public String rateQuery(WeixinCurrencyRequestData input) throws Exception {
        Map<String, String> map = new HashMap<String, String>();

        //commom
        map.put("ver", input.getVer());
        map.put("method", input.getMethod());
        map.put("mch_id", input.getMchId());
        map.put("nonce_str", input.getNonceStr() == null ? "" : input.getNonceStr());
        //WeixinOrderQueryRequestData
        map.put("trade_type", input.getTradeType());
        map.put("currency", input.getCurrency());
        map.put("exchange_currency", input.getExchangeCurrency());

        return HttpRequestUtil.post(HOST, map, getPrivateKey());
    }


    public String getPayCode(String merchantId, String orderId) throws Exception {

        // Todo : query the payment result to return something according to result
        // todo load order by merchantId and orderId.
        // todo calculate pay code by information in this order.

        String token = "ABCD";
        String randomNumber = "9763981694";

        return generatePaycode(token, randomNumber);
    }

    public String updateOrder(WeixinSingleQueryRequestData input) throws Exception {
        Map<String, String> map = new HashMap<String, String>();
        //commom
        map.put("ver", input.getVer());
        map.put("method", input.getMethod());
        map.put("mch_id", input.getMchId());
        map.put("nonce_str", input.getNonceStr() == null ? "" : input.getNonceStr());
        //WeixinOrderQueryRequestData
        map.put("trade_type", "UPDATESKORDER");
        map.put("out_trade_no", input.getOutTradeNo());

        LOGGER.info("[TRANSACTION] weixin updateOrder start. " + new Gson().toJson(map));
        String result = HttpRequestUtil.post(HOST, map, getPrivateKey());
        LOGGER.info("[TRANSACTION] weixin updateOrder complete. " + result);
        return result;
    }

    private PrivateKey getPrivateKey() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("key/php_private.der").getFile());

        if (PRIVATE_KEY == null) {
            PRIVATE_KEY = KeyReader.loadPrivateKeyFromDER(file);
        }
        return PRIVATE_KEY;
    }

    private String generatePaycode(String token, String random) {
        int a = calculate(token);
        int b = calculate(random);
        int c = a * b;
        int paycodeLen = 6;

        String str = Integer.toString(c);
        LOGGER.info("original paycode:" + str);
        String formatStr = "%06d";
        String formatAns = String.format(formatStr, c);
        int leng = formatAns.length();
        String ans;

        if (leng > 6) {
            ans = formatAns.substring(leng - paycodeLen, leng);
        } else {
            ans = formatAns;
        }
        return str;
    }

    private int calculate(String s) {
        byte[] bytes = s.getBytes();
        int total = 0;
        for (int _i = 0; _i < bytes.length; _i++) {
            total += bytes[_i];
        }
        return total;
    }
}
