package co.intella.service.impl;

import co.intella.model.EdgeToken;
import co.intella.model.EncryptType;
import co.intella.service.EdgeService;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Map;


@Service
@Transactional
public class EdgeServiceImpl implements EdgeService {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Resource
    private EntityManager entityManager;


    @Override
    public EncryptType findEncryptType(String mchId) {

        String sql = " SELECT e.token , e.encryptType , e.key , e.iv " +
                "FROM edge.INT_TB_EncryptType e  LEFT JOIN  edge.INT_TB_Token t  ON  e.token = t.token " +
                "WHERE  t.accountId= :accountId  order BY e.dateStamp DESC  ";

        Query query = entityManager.createNativeQuery(sql)
                .setParameter("accountId", mchId);
        query.unwrap(SQLQuery.class).setResultTransformer(Transformers.aliasToBean(EncryptType.class));

        List<EncryptType> list = query.getResultList();
        return CollectionUtils.isNotEmpty(list) ? list.get(0) : null;
    }


	@Override
	public EdgeToken findToken(String token) {
		 String sql = " SELECT token,serviceNumber,accountId,serviceId,status FROM edge.INT_TB_Token WHERE token= :token ";

	        Query query = entityManager.createNativeQuery(sql)
	                .setParameter("token", token);
	        query.unwrap(SQLQuery.class).setResultTransformer(Transformers.aliasToBean(EdgeToken.class));
	        EdgeToken result=(EdgeToken)query.getSingleResult();
		return result;
	}
}
