package co.intella.service.impl;

import co.intella.domain.TSMC.PaymentNotifyRequest;
import co.intella.domain.TSMC.PaymentRespData;
import co.intella.domain.TSMC.QueryForCardIdRequest;
import co.intella.domain.TSMC.RefundNotifyRequest;
import co.intella.domain.easycard.EzCardPaymentResponse;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.exception.*;
import co.intella.model.Merchant;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import co.intella.service.*;
import co.intella.utility.JsonUtil;
import co.intella.utility.SystemInstance;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * 給台積電員工使用，主要邏輯：
 * 當台積電發起悠遊卡交易時，需額外MQTT 啟動台積電員工卡機 同時感應卡片
 * 看哪張卡有銬上就扣款哪張
 * 同時記錄 訂單狀態更新歷程
 */
@Service
public class TsmcServiceImpl implements TsmcService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Resource
    private MqttService mqttService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private TradeDetailOtherInfoService tradeDetailOtherInfoService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private TsmcUtilService tsmcUtilService;

    @Autowired
    private MessageSource messageSource;

    private static final String MQTT_TITLE = "toTSMC_";

    private static final Locale locale = new Locale("zh_TW");

    @Override
    public String doRequest(RequestHeader requestHeader, JsonObject requestData) throws Exception {

        Merchant merchant = this.doValidate(requestHeader, requestData);
        LOGGER.info("requestHeader : " + JsonUtil.toJsonStr(requestHeader));
        LOGGER.info("requestData : " + JsonUtil.getGson().toJson(requestData));

        String serviceType = requestHeader.getServiceType();
        String result = "";
        switch (serviceType) {
            case "Payment":
                result = this.doPayment(merchant, requestHeader, requestData);
                break;
            case "RefundNotify":
                result = this.doRefundNotify(merchant, requestHeader, requestData);
                break;
            case "PaymentNotify":
                result = this.doPaymentNotify(merchant, requestHeader, requestData);
                break;
            case "QueryForCardId":
                result = this.doQueryForCardId(merchant, requestHeader, requestData);
                break;
            default:
                throw new ServiceTypeNotSupportException(serviceType + " ServiceType Not Support.");
        }
        return result;
    }

    /**
     * 發起MQTT 喚起 台積電員工卡機
     *
     * @param merchant
     * @param requestData
     */
    public void doMqttTSMC(Merchant merchant, JsonObject requestData) {

        Map<String, String> requestMap = this.getMqttMap(merchant, requestData);
        StringBuilder clientIdSb = new StringBuilder();
        clientIdSb.append(MQTT_TITLE).append(merchant.getAccountId());
        StringBuilder requestSb = new StringBuilder();
        requestSb.append(JsonUtil.toJsonStr(requestMap));
        try {
            mqttService.publish("", requestSb.toString(), clientIdSb.toString());
            LOGGER.info("[TSMC][MQTT][" + clientIdSb.toString() + "] -> " + requestSb.toString());
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("[TSMC][MQTT][" + clientIdSb.toString() + "] -> " + requestSb.toString() + "\nError:" + e.getMessage());
        }
    }

    private Map<String, String> getMqttMap(Merchant merchant, JsonObject requestData) {
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("ServiceType", "Payment");
        requestMap.put("MchId", merchant.getAccountId());
        requestMap.put("DeviceId", requestData.get("DeviceId").getAsString());
        requestMap.put("CreateTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        requestMap.put("OrderId", requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString());
        requestMap.put("Amount", requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString());
        requestMap.put("Body", requestData.get("Body").getAsString());

        return requestMap;
    }

    private Merchant doValidate(RequestHeader requestHeader, JsonObject requestData) throws NoSuchMerchantException, OrderAlreadyExistedException, OtherAPIException {
        Merchant merchant = merchantService.getOne(requestHeader.getMerchantId());
        if (Objects.isNull(merchant)) {
            throw new NoSuchMerchantException("Merchant error");
        }
        return merchant;
    }

    private ResponseGeneralBody getGeneralResponse(RequestHeader requestHeader, TradeDetail tradeDetail, String errorCode) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setStatusCode(errorCode); // 0001
        responseGeneralHeader.setStatusDesc((messageSource.getMessage("response." + errorCode, null, locale)));
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setMchId(requestHeader.getMerchantId());
        responseGeneralHeader.setMethod(requestHeader.getMethod());
        responseGeneralHeader.setResponseTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        if ("0000".equals(errorCode) && Objects.nonNull(tradeDetail)) {
            responseGeneralBody.setData(new PaymentRespData(tradeDetail));
        }
        responseGeneralBody.setHeader(responseGeneralHeader);

        return responseGeneralBody;
    }

    /**
     * @param merchant
     * @param requestHeader
     * @param requestData
     * @return
     * @throws Exception
     */
    private String doRefundNotify(Merchant merchant, RequestHeader requestHeader, JsonObject requestData) throws Exception {

        JsonElement orderIdJE = requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO);
        if (orderIdJE.isJsonNull() || StringUtils.isBlank(orderIdJE.getAsString())) {
            throw new OtherAPIException(messageSource.getMessage("response.7142", null, locale));
        }

        RefundNotifyRequest refundNotifyRequest = JsonUtil.parseJson(JsonUtil.getGson().toJson(requestData), RefundNotifyRequest.class);
        String result = "";
        TradeDetail tradeDetail = tradeDetailService.getOne(refundNotifyRequest.getStoreOrderNo());
        if (Objects.isNull(tradeDetail)) {
            throw new OtherAPIException(messageSource.getMessage("response.7002", null, locale));
        } else if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
            throw new OtherAPIException(messageSource.getMessage("response.7351", null, locale));
        }

        if ("14900".equals(tradeDetail.getMethod()) && merchant.getRefundPin().equals(refundNotifyRequest.getRefundKey())) {
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
            tradeDetail.setStoreRefundId(refundNotifyRequest.getStoreRefundNo());
            tradeDetail.setSystemRefundId("r" + refundNotifyRequest.getStoreRefundNo());
            tradeDetailService.save(tradeDetail);
            result = JsonUtil.toJsonStr(this.getGeneralResponse(requestHeader, tradeDetail, "0000"));
        } else {
            result = JsonUtil.toJsonStr(this.getGeneralResponse(requestHeader, tradeDetail, "9998"));
        }

        return result;
    }

    private String doPaymentNotify(Merchant merchant, RequestHeader requestHeader, JsonObject requestData) throws Exception {

        JsonElement orderIdJE = requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO);
        if (orderIdJE.isJsonNull() || StringUtils.isBlank(orderIdJE.getAsString())) {
            throw new OtherAPIException(messageSource.getMessage("response.7142", null, locale));
        }

        PaymentNotifyRequest paymentNotifyRequest = JsonUtil.parseJson(JsonUtil.getGson().toJson(requestData), PaymentNotifyRequest.class);

        TradeDetail tradeDetail = tradeDetailService.getOne(paymentNotifyRequest.getStoreOrderNo());
        PaymentAccount paymentAccount = paymentAccountService.getOne(requestHeader.getMerchantId(), 49);
        if (Objects.isNull(tradeDetail)) {
            tradeDetail = new TradeDetail();
            tradeDetail.setAccountId(merchant.getAccountId());
            tradeDetail.setOrderId(paymentNotifyRequest.getStoreOrderNo());
            tradeDetail.setPaymentAccount(paymentAccount);
            tradeDetail.setUserId(paymentNotifyRequest.getCardId());
//            tradeDetail.setServiceType(SystemInstance.TYPE_OLPAY);
            tradeDetail.setPayment(Long.valueOf(paymentNotifyRequest.getTotalFee()));
            tradeDetail.setDescription(paymentNotifyRequest.getBody());
            tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
            tradeDetail.setDeviceRandomId(deviceService.getOne(merchant, "APP-001"));
            if ("0000".equals(paymentNotifyRequest.getErrorCode())) {
                tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            } else {
                tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            }
        } else {
            if ("0000".equals(paymentNotifyRequest.getErrorCode())) {
                tradeDetail.setPaymentAccount(paymentAccount);
                tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                tradeDetail.setUserId(paymentNotifyRequest.getCardId());
            } else if ("31800".equals(tradeDetail.getStatus()) && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                //非成功 且比悠遊卡(失敗) 晚通知
                tradeDetail.setPaymentAccount(paymentAccount);
                tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                tradeDetail.setUserId(paymentNotifyRequest.getCardId());
            }
        }
        tradeDetail.setMethod("14900");
        tradeDetailService.save(tradeDetail);

        return JsonUtil.toJsonStr(this.getGeneralResponse(requestHeader, tradeDetail, "0000"));
    }

    /**
     * @param merchant
     * @param requestHeader
     * @param requestData
     * @return
     * @throws Exception
     */
    private String doQueryForCardId(Merchant merchant, RequestHeader requestHeader, JsonObject requestData) {
        QueryForCardIdRequest queryForCardIdRequest = JsonUtil.parseJson(JsonUtil.getGson().toJson(requestData), QueryForCardIdRequest.class);
        String result = "";


        if (StringUtils.isNotBlank(queryForCardIdRequest.getCardId())) {

            List<TradeDetail> tradeDetailList = null;

            for (int i = 0; i < 2; i++) {
                if (i == 1) {
                    String cardId = tradeDetailOtherInfoService.getCardIdByLikeCardNumberForPrint(queryForCardIdRequest.getCardId());
                    if (StringUtils.isNotBlank(cardId)) {
                        queryForCardIdRequest.setCardId(cardId);
                    }
                }
                tradeDetailList = tradeDetailService.queryForCardIdMchId(queryForCardIdRequest, requestHeader.getMerchantId());
                if (CollectionUtils.isNotEmpty(tradeDetailList)) {
                    break;
                }
            }

            if (CollectionUtils.isNotEmpty(tradeDetailList)) {
                ResponseGeneralBody responseGeneralBody = this.getGeneralResponse(requestHeader, null, "0000");
                responseGeneralBody.setData(new PaymentRespData(tradeDetailList));
                result = JsonUtil.toJsonStr(responseGeneralBody);
            } else {
                result = JsonUtil.toJsonStr(this.getGeneralResponse(requestHeader, null, "7352"));
            }
        } else {
            result = JsonUtil.toJsonStr(this.getGeneralResponse(requestHeader, null, "7314"));
        }

        return result;
    }

    private String doPayment(Merchant merchant, RequestHeader requestHeader, JsonObject requestData) throws Exception {

        String result;
        String orderId = requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString();

        if (StringUtils.isBlank(orderId)) {
            throw new OrderNoIsBlankException("StoreOrderNo is blank");
        }

        TradeDetail tradeDetail;
        tradeDetail = tradeDetailService.getOne(orderId);
        if (Objects.isNull(tradeDetail)) {
            this.doMqttTSMC(merchant, requestData);
            Future<String> future = tsmcUtilService.asyncTicket(requestHeader, requestData);
            do {
                TimeUnit.SECONDS.sleep(1);
                tradeDetail = tradeDetailService.getOne(orderId);
            } while (Objects.isNull(tradeDetail) ||
                    (!future.isDone() && !(SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus()) || SystemInstance.TRADE_FAIL.equals(tradeDetail.getStatus()))));

            if ("14900".equals(tradeDetail.getMethod()) && SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                EzCardPaymentResponse response = new EzCardPaymentResponse();
                response.setErrorCode("000000");
                response.setTxnResult("Success");
                requestHeader.setMethod("14900");
                ResponseGeneralBody responseGeneralBody = this.getGeneralResponse(requestHeader, null, "0000");
                responseGeneralBody.setData(response);
                result = JsonUtil.toJsonStr(responseGeneralBody);
            } else {
                result = future.get();
            }
            if (!future.isDone()) {
                future.cancel(true);
            }
        } else {
            throw new OrderAlreadyExistedException("Trade exist");
        }
        return result;
    }

}
