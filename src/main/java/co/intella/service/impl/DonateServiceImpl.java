package co.intella.service.impl;

import co.intella.model.IntTbLookupCode;
import co.intella.model.TradeDetail;
import co.intella.service.DonateService;
import co.intella.service.LookupCodeService;
import co.intella.utility.DonateAESUtil;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Service
public class DonateServiceImpl implements DonateService {

    private final Logger LOGGER = LoggerFactory.getLogger(DonateServiceImpl.class);

    @Resource
    private LookupCodeService lookupCodeService;

    @Value("${hotst.donate.server}")
    String DONATE_SERVER;

    @Override
    public List<JsonObject> getNotifyList(TradeDetail tradeDetail) {

        String shortId = tradeDetail.getQrcodeToken();
        List<JsonObject> list = new ArrayList<>();

        try {
            String responseData = this.findEmailListByDonate(shortId);

            Gson gson = JsonUtil.getGson();
            JsonObject responseDataJson = gson.fromJson(responseData, JsonObject.class);
            LOGGER.info("responseDataJson => " + responseDataJson);
            if ("0000".equals(responseDataJson.get("msgCode").getAsString())) {
                JsonArray usersJsonStr = responseDataJson.getAsJsonObject("data").getAsJsonArray("users");
                list = gson.fromJson(usersJsonStr.toString(), new TypeToken<ArrayList<JsonObject>>() {
                }.getType());
            }
        } catch (Exception e) {
            LOGGER.error("DonateServiceImpl error ！");
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
        }

        return list;
    }


    private String findEmailListByDonate(String shortId) throws Exception {
        IntTbLookupCode lookupCode = lookupCodeService.findOne("donateTokenId", "allpaypass_admin");
        Map<String, String> header = new HashMap<>();
        header.put("Secret", DonateAESUtil.encrypt(lookupCode.getType1()));

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("shortId", shortId);
        String reqDataStr = DonateAESUtil.encrypt(jsonObject.toString());
        jsonObject.remove("shortId");
        jsonObject.addProperty("requestData", reqDataStr);

        String result = HttpUtil.doPostJson(header, DONATE_SERVER + "api/QRCode/getUserList", jsonObject.toString());
        LOGGER.info("[findEmailListByDonate] result =>>  " + result);
        Gson gson = JsonUtil.getGson();
        Map<String, String> resultMap = gson.fromJson(result, new TypeToken<HashMap<String, String>>() {
        }.getType());

        String responseData = DonateAESUtil.decrypt(resultMap.get("responseData"));

        return responseData;
    }
}
