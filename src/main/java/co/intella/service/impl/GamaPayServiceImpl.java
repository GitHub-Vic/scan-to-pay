package co.intella.service.impl;

import co.intella.domain.gama.*;
import co.intella.domain.integration.*;
import co.intella.model.*;
import co.intella.net.HttpRequestUtil;
import co.intella.service.*;
import co.intella.utility.GamaPayEncryptAESUtil;
import co.intella.utility.GamaPayUtil;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Objects;

/**
 * @author Alex
 */
@Service
public class GamaPayServiceImpl implements GamaPayService {

    private final Logger LOGGER = LoggerFactory.getLogger(GamaPayServiceImpl.class);

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private OrderService orderService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private DeviceService deviceService;

    @Autowired
    private Environment env;

    @Autowired
    private MessageSource messageSource;

    @Value("${host.request.gamapay}")
    private String GAMAPAY_URL;

    @Value("${api.url}")
    private String API_URL;

    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {
        String generalResponseData = SystemInstance.EMPTY_STRING;

        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        PaymentAccount paymentAccount = paymentAccountService.getOne(integrateMchId, bankId);

        if (requestHeader.getServiceType().equals(SystemInstance.TYPE_MICROPAY)) {
            Merchant merchant = merchantService.getOne(requestHeader.getMerchantId());

            GamaCreateOrderRequestData gamaCreateOrderRequestData = new Gson().fromJson(data.getAsString(),
                    GamaCreateOrderRequestData.class);
            JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
            gamaCreateOrderRequestData.setMerchantId(paymentAccount.getAccount());
            gamaCreateOrderRequestData.setSubMerchantId(merchant.getAccountId());
            gamaCreateOrderRequestData.setRemark1(merchant.getAccountId());
            gamaCreateOrderRequestData.setRemark2(merchant.getBrandName());
            gamaCreateOrderRequestData.setTransAmount(requestData.get("TotalFee").getAsInt());
            gamaCreateOrderRequestData.setTradingContent(requestData.get("Body").getAsString());
            gamaCreateOrderRequestData.setTradingToken(requestData.get("AuthCode").getAsString());
            gamaCreateOrderRequestData.setMerchantOrderId(requestData.get("StoreOrderNo").getAsString());
            gamaCreateOrderRequestData.setTransType(10);
            gamaCreateOrderRequestData.setCurrencyCode("TWD");
            gamaCreateOrderRequestData.setBusinessDate(requestHeader.getCreateTime().substring(0, 8));

            String gamaMacDataString = new Gson().toJson(gamaCreateOrderRequestData);
            GamaMacData gamaMacData = this.setMacData(gamaMacDataString);

            gamaCreateOrderRequestData.setMac(getMacValue(gamaMacData, paymentAccount));

            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
            String result = createOrderForMicropay(gamaCreateOrderRequestData, paymentAccount);
            LOGGER.info("[GamaPay][Micropay][result].." + result);

            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);

            GamaResponseData gamaResponseData = new Gson().fromJson(result, GamaResponseData.class);

            ResponseGeneralHeader responseGeneralHeader = new GamaPayUtil().getResponseHeader(
                    gamaResponseData.getResultCode(), gamaResponseData.getStatusCode(), requestHeader, messageSource);
            requestHeader.setMerchantId(integrateMchId);
            IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();

            if (gamaResponseData.getResultCode().equals("0000")) {
                integratedMicropayResponseData.setSysOrderNo(gamaResponseData.getTransactionId());
                integratedMicropayResponseData.setTotalFee(gamaResponseData.getTransAmount().toString());
            } else {
                integratedMicropayResponseData.setSysOrderNo(SystemInstance.EMPTY_STRING);
                integratedMicropayResponseData.setTotalFee(SystemInstance.EMPTY_STRING);
            }
            integratedMicropayResponseData.setStoreOrderNo(tradeDetail.getOrderId());
            integratedMicropayResponseData.setAuthCode(tradeDetail.getTradeToken());
            integratedMicropayResponseData.setPlatformRsp(result);

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedMicropayResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);
            LOGGER.info("integrate response ready :" + generalResponseData);

        } else if (requestHeader.getServiceType().equals("Refund")) {
            LOGGER.info("[GamaPay][Refund][data]" + data.getAsString());

            GamaRefundRequestData gamaRefundRequestData = new Gson().fromJson(data.getAsString(),
                    GamaRefundRequestData.class);
            IntegratedRefundRequestData integratedRefundRequestData = new Gson().fromJson(data.getAsString(),
                    IntegratedRefundRequestData.class);

            JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

            gamaRefundRequestData.setMerchantId(paymentAccount.getAccount());
            gamaRefundRequestData.setMerchantOrderId(requestData.get("StoreOrderNo").getAsString());
            gamaRefundRequestData.setTransactionID(tradeDetail.getSystemOrderId());
            gamaRefundRequestData.setCurrencyCode("TWD");
            gamaRefundRequestData.setTransAmount((int) tradeDetail.getPayment());

            String result = refund(gamaRefundRequestData, paymentAccount);
            LOGGER.info("[GamaPay][refund][result].." + result);

            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);

            GamaResponseData gamaResponseData = new Gson().fromJson(result, GamaResponseData.class);

            ResponseGeneralHeader responseGeneralHeader = new GamaPayUtil().getResponseHeader(
                    gamaResponseData.getResultCode(), gamaResponseData.getStatusCode(), requestHeader, messageSource);
            responseGeneralHeader.setMchId(integrateMchId);
            IntegratedRefundResponseData integratedRefundResponseData = new GamaPayUtil()
                    .getIntegratedRefundResponseData(gamaResponseData, requestHeader, tradeDetail,
                            integratedRefundRequestData);
            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedRefundResponseData);
            generalResponseData = new Gson().toJson(responseGeneralBody);

            LOGGER.info("integrate response ready :" + generalResponseData);

        } else if (requestHeader.getServiceType().equals("SingleOrderQuery")) {
            LOGGER.info("[GamaPay][SingleOrderQuery][data]" + data.getAsString());

            GamaSingleOrderQueryRequestData gamaSingleOrderQueryRequestData = new GamaSingleOrderQueryRequestData();
            JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

            gamaSingleOrderQueryRequestData.setMerchantId(paymentAccount.getAccount());
            gamaSingleOrderQueryRequestData.setTransactionId("ReversalTrading");
            gamaSingleOrderQueryRequestData.setMerchantOrderId(tradeDetail.getOrderId());
            gamaSingleOrderQueryRequestData.setCurrencyCode("TWD");
            gamaSingleOrderQueryRequestData.setTransAmount((int) tradeDetail.getPayment());
            gamaSingleOrderQueryRequestData.setTransType(10);

            String result = singleOrderQuery(gamaSingleOrderQueryRequestData, paymentAccount);
            GamaResponseData gamaResponseData = new Gson().fromJson(result, GamaResponseData.class);

            LOGGER.info("[GamaPay][SingleOrderQuery][result].." + result);

            if ("0000".equals(gamaResponseData.getResultCode())) {
                String currencyTimeString = LocalDateTime.now().toString("yyyyMMddHHmmss");
                DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
                if (((df.parse(currencyTimeString).getTime() - df.parse(tradeDetail.getCreateDate()).getTime()) > 300000 && "0".equals(gamaResponseData.getStatusCode())) || "102".equals(gamaResponseData.getStatusCode())) {
                    LOGGER.info("[GamaPay][SingleOrderQuery] Order is Expired.");
                    GamaCancelRequestData gamaCancelRequestData = new GamaCancelRequestData();
                    gamaCancelRequestData.setMerchantId(paymentAccount.getAccount());
                    gamaCancelRequestData.setTransactionID(gamaResponseData.getTransactionId());
                    gamaCancelRequestData.setMerchantOrderId(gamaResponseData.getMerchantOrderId());
                    gamaCancelRequestData.setCurrencyCode("TWD");
                    gamaCancelRequestData.setTransAmount(gamaResponseData.getTransAmount().intValue());
                    gamaCancelRequestData.setTransType(10);
                    result = this.cancel(gamaCancelRequestData, paymentAccount);
                    LOGGER.info("[GamaPay][Cancel][result].." + result);
                    gamaResponseData = new Gson().fromJson(result, GamaResponseData.class);
                }
            }

            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);

            ResponseGeneralHeader responseGeneralHeader = new GamaPayUtil().getResponseHeader(
                    gamaResponseData.getResultCode(), gamaResponseData.getStatusCode(), requestHeader, messageSource);
            responseGeneralHeader.setMchId(integrateMchId);
            IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new GamaPayUtil()
                    .getIntegratedSingleOrderQueryResponseData(gamaResponseData, tradeDetailService.getOne(tradeDetail.getOrderId()));
            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedSingleOrderQueryResponseData);
            generalResponseData = new Gson().toJson(responseGeneralBody);

            LOGGER.info("integrate response ready :" + generalResponseData);
        }
        return generalResponseData;
    }

    @Override
    public String getPaymentUrl(String amount, String shortId, String agen) throws Exception {

        String result = null;
        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(shortId);

        if (!Objects.isNull(qrcodeParameter)) {

            String storeOrderNo = "1".equals(qrcodeParameter.getCodeType()) ? qrcodeParameter.getStoreOrderNo() : orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);

            Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());
            PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 13);
            String bodyRandom = qrcodeService.getRandomBody(qrcodeParameter);

            GamaCreateOrderRequestData gamaCreateOrderRequestData = new GamaCreateOrderRequestData();
            gamaCreateOrderRequestData.setMerchantOrderId(storeOrderNo);
            gamaCreateOrderRequestData.setSubMerchantId(merchant.getAccountId());
            gamaCreateOrderRequestData.setRemark1(merchant.getAccountId());
            gamaCreateOrderRequestData.setRemark2(merchant.getBrandName());
            gamaCreateOrderRequestData.setTransAmount(Integer.parseInt(amount));

            setOnlineTradeDetailRequest(gamaCreateOrderRequestData, merchant, paymentAccount, qrcodeParameter);
            DateTime dateTime = DateTime.now();
            gamaCreateOrderRequestData.setMerchantId(paymentAccount.getAccount());
            gamaCreateOrderRequestData.setTransType(10);
            gamaCreateOrderRequestData.setTradingContent(bodyRandom);
            gamaCreateOrderRequestData.setCurrencyCode("TWD");
            gamaCreateOrderRequestData.setBusinessDate(dateTime.toString("yyyyMMdd"));
            gamaCreateOrderRequestData.setPayWay(1);
            gamaCreateOrderRequestData.setPostBackUrl(String.format("%sallpaypass/api/gamapay/backUrl", API_URL));
            String request = new Gson().toJson(gamaCreateOrderRequestData);

            LOGGER.info("[GamaPay][OnLine][Request]: " + request);
            for (int i = 0; i < 2; i++) {
                try {
                    result = createOrder(gamaCreateOrderRequestData, paymentAccount);
                    if (StringUtils.isNotBlank(result))
                        break;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (StringUtils.isEmpty(result)) {
                GamaSingleOrderQueryRequestData gamaSingleOrderQueryRequestData = new GamaSingleOrderQueryRequestData();
                gamaSingleOrderQueryRequestData.setMerchantId(paymentAccount.getAccount());
                gamaSingleOrderQueryRequestData.setTransactionId("ReversalTrading");
                gamaSingleOrderQueryRequestData.setMerchantOrderId(storeOrderNo);
                gamaSingleOrderQueryRequestData.setCurrencyCode("TWD");
                gamaSingleOrderQueryRequestData.setTransAmount(Integer.parseInt(amount));
                gamaSingleOrderQueryRequestData.setTransType(10);

                String queryResult = singleOrderQuery(gamaSingleOrderQueryRequestData, paymentAccount);
                GamaResponseData gamaResponseData = new Gson().fromJson(queryResult, GamaResponseData.class);
                if ("0000".equals(gamaResponseData.getResultCode()) && "102".equals(gamaResponseData.getStatusCode())) {
                    LOGGER.info("[GamaPay][SingleOrderQuery] Order is Expired.");
                    GamaCancelRequestData gamaCancelRequestData = new GamaCancelRequestData();
                    gamaCancelRequestData.setMerchantId(paymentAccount.getAccount());
                    gamaCancelRequestData.setTransactionID(gamaResponseData.getTransactionId());
                    gamaCancelRequestData.setMerchantOrderId(gamaResponseData.getMerchantOrderId());
                    gamaCancelRequestData.setCurrencyCode("TWD");
                    gamaCancelRequestData.setTransAmount(gamaResponseData.getTransAmount().intValue());
                    gamaCancelRequestData.setTransType(10);
                    String cancelResult = this.cancel(gamaCancelRequestData, paymentAccount);
                    LOGGER.info("[GamaPay][SingleOrderQuery][result].." + cancelResult);
                }
            }


            LOGGER.info("[GamaPay][OnLine][Response]: " + result);
        } else if (qrcodeParameter == null) {
            LOGGER.error("[REQUEST][GamaPay] qrcode not found");
        }

        return result;
    }

    public String createOrder(GamaCreateOrderRequestData request, PaymentAccount paymentAccount) throws Exception {
        String gamaMacDataString = new Gson().toJson(request);
        GamaMacData gamaMacData = this.setMacData(gamaMacDataString);
        request.setMac(getMacValue(gamaMacData, paymentAccount));
        LOGGER.info("[GamaPay][createOrder][sendingRequest]..");
        String gamaPayRequest = new GsonBuilder().disableHtmlEscaping().create().toJson(request);
        String result = HttpRequestUtil.post(GAMAPAY_URL + "Transaction/OrderCreate", gamaPayRequest);
        return result;
    }

    public String createOrderForMicropay(GamaCreateOrderRequestData request, PaymentAccount paymentAccount) throws Exception {

        LOGGER.info("[GamaPay][createOrderMerchant][sendingRequest]..");
        String gamaPayRequest = new GsonBuilder().disableHtmlEscaping().create().toJson(request);
        LOGGER.info("[GamaPay][createOrderMerchant]  request =>" + gamaPayRequest);
        String result = HttpRequestUtil.post(GAMAPAY_URL + "Transaction/OrderPaybyMerchant", gamaPayRequest);
        return result;
    }

    public String cancel(GamaCancelRequestData request, PaymentAccount paymentAccount) throws Exception {
        String gamaMacDataString = new Gson().toJson(request);
        GamaMacData gamaMacData = this.setMacData(gamaMacDataString);
        request.setMac(getMacValue(gamaMacData, paymentAccount));
        LOGGER.info("[GamaPay][orderCancel][sendingRequest]..");
        String gamaPayRequest = new GsonBuilder().disableHtmlEscaping().create().toJson(request);
        String result = HttpRequestUtil.post(GAMAPAY_URL + "Transaction/OrderCancel", gamaPayRequest);
        return result;
    }

    public String refund(GamaRefundRequestData request, PaymentAccount paymentAccount) throws Exception {
        String gamaMacDataString = new Gson().toJson(request);
        GamaMacData gamaMacData = this.setMacData(gamaMacDataString);
        request.setMac(getMacValue(gamaMacData, paymentAccount));
        LOGGER.info("[GamaPay][refund][sendingRequest]..");
        String gamaPayRequest = new GsonBuilder().disableHtmlEscaping().create().toJson(request);
        LOGGER.info("[GamaPay][refund][request]:" + gamaPayRequest);
        String result = HttpRequestUtil.post(GAMAPAY_URL + "Refund/OrderRefund", gamaPayRequest);
        return result;
    }

    public String refundQuery(GamaSingleOrderQueryRequestData request, PaymentAccount paymentAccount) throws Exception {
        String gamaMacDataString = new Gson().toJson(request);
        GamaMacData gamaMacData = this.setMacData(gamaMacDataString);

        request.setTransactionId("ReversalTrading");
        request.setTransType(30);
        request.setMac(getMacValue(gamaMacData, paymentAccount));
        LOGGER.info("[GamaPay][refundQuery][sendingRequest]..");
        String gamaPayRequest = new GsonBuilder().disableHtmlEscaping().create().toJson(request);
        LOGGER.info("[GamaPay][refundQuery]  request =>" + gamaPayRequest);
        String result = HttpRequestUtil.post(GAMAPAY_URL + "Transaction/GetTransOrder", gamaPayRequest);
        return result;
    }

    public String singleOrderQuery(GamaSingleOrderQueryRequestData request, PaymentAccount paymentAccount) throws Exception {
        String gamaMacDataString = new Gson().toJson(request);
        GamaMacData gamaMacData = this.setMacData(gamaMacDataString);

        request.setMac(getMacValue(gamaMacData, paymentAccount));
        LOGGER.info("[GamaPay][singleOrderQuery][sendingRequest]..");
        String gamaPayRequest = new GsonBuilder().disableHtmlEscaping().create().toJson(request);
        LOGGER.info("[GamaPay][singleOrderQuery]  request =>" + gamaPayRequest);
        String result = HttpRequestUtil.post(GAMAPAY_URL + "Transaction/GetTransOrder", gamaPayRequest);
        return result;
    }

    private String getMacValue(GamaMacData request, PaymentAccount paymentAccount) throws Exception {
        String macData = String.format("%s%s%s%s%s%s", request.getMerchantId(),
                request.getSubMerchantId() == null ? "" : request.getSubMerchantId(), request.getMerchantOrderId(),
                request.getCurrencyCode(), String.format("%014d", request.getTransAmount() * 100),
                paymentAccount.getHashIV());
        GamaPayEncryptAESUtil gamaPayEncryptAESUtil = new GamaPayEncryptAESUtil(paymentAccount.getHashIV(),
                paymentAccount.getHashKey());
        String mac = gamaPayEncryptAESUtil.encrypt(macData);
        System.out.println(macData);
        System.out.println("[GamaPay][mac]: " + mac);
        return mac;
    }

    private GamaMacData setMacData(String request) throws Exception {
        GamaMacData gamaMacData = new GamaMacData();
        JsonObject dataJson = new Gson().fromJson(request, JsonObject.class);
        gamaMacData.setMerchantId(dataJson.get("MerchantID").getAsString());
        gamaMacData.setMerchantOrderId(dataJson.get("MerchantOrderID").getAsString());
        gamaMacData.setCurrencyCode(dataJson.get("CurrencyCode").getAsString());
        gamaMacData.setTransAmount(dataJson.get("TransAmount").getAsInt());
        try {
            gamaMacData.setSubMerchantId(dataJson.get("SubMerchantID").getAsString());
        } catch (Exception e) {
            gamaMacData.setSubMerchantId(null);
        }
        return gamaMacData;
    }

    private TradeDetail setOnlineTradeDetailRequest(GamaCreateOrderRequestData gamaCreateOrderRequestData,
                                                    Merchant merchant, PaymentAccount paymentAccount, QrcodeParameter qrcodeParameter) {
        LOGGER.info("enter trade detail request:" + new Gson().toJson(gamaCreateOrderRequestData));

        TradeDetail tradeDetail = tradeDetailService.getOne(merchant.getAccountId(),
                gamaCreateOrderRequestData.getMerchantOrderId());

        if (Objects.isNull(tradeDetail)) {

            tradeDetail = new TradeDetail();
            tradeDetail.setAccountId(merchant.getAccountId());
            tradeDetail.setMethod("11300");
            tradeDetail.setPaymentAccount(paymentAccount);
            tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
            tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
            tradeDetail.setOrderId(gamaCreateOrderRequestData.getMerchantOrderId());
            tradeDetail.setDeviceRandomId(deviceService.getOne(merchant, "APP-001"));
            tradeDetail.setPayment(gamaCreateOrderRequestData.getTransAmount());
            tradeDetail.setServiceType("OLPay");
            tradeDetail.setQrcodeToken(qrcodeParameter.getShortId());
            tradeDetail.setDescription(qrcodeParameter.getBody());

            if (qrcodeParameter.getOnSaleTotalFee() == gamaCreateOrderRequestData.getTransAmount()) {
                tradeDetail.setOnSale(true);
            } else {
                tradeDetail.setOnSale(false);
            }
            tradeDetail.setOriginalPrice(qrcodeParameter.getTotalFee());

            tradeDetail = tradeDetailService.save(tradeDetail);
            tradeProcedureService.captureOrderSave(tradeDetail);
        }
        LOGGER.info("save online trade detail : " + new Gson().toJson(tradeDetail));
        return tradeDetail;
    }

}
