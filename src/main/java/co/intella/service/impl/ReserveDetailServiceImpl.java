package co.intella.service.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import co.intella.domain.contratStore.TradeDetailVo;
import co.intella.model.ReserveDetail;
import co.intella.net.HttpRequestUtil;
import co.intella.service.ReserveDetailService;

/**
 * @author Miles
 */
@Service
@Transactional
public class ReserveDetailServiceImpl implements ReserveDetailService {

    private final Logger LOGGER = LoggerFactory.getLogger(ReserveDetailServiceImpl.class);

    private static String HOST_DAO = "http://localhost:8080/scan2pay-data/";


    public ReserveDetail save(ReserveDetail reserveDetail) {
        try {
            String result = HttpRequestUtil.post(HOST_DAO + "api/reserve/save", new ObjectMapper().writeValueAsString(reserveDetail));
            return new Gson().fromJson(result, ReserveDetail.class);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public ReserveDetail getOne(String merchantId, String orderId) {
        try {
            String result = HttpRequestUtil.httpGet(HOST_DAO + "api/reserve/get/" + merchantId + "/" +orderId);
            return new Gson().fromJson(result, ReserveDetail.class);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public List<ReserveDetail> listByBatchNumber(String merchantId, String batchNumber) {
        try {
            String result = HttpRequestUtil.httpGet(HOST_DAO + "api/reserve/listByType/batch/" + batchNumber + "/" + merchantId);
            return new Gson().fromJson(result, new TypeToken<List<ReserveDetail>>(){}.getType());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public List<ReserveDetail> listByDeviceId(String merchantId, String deviceId) {
        try {
            String result = HttpRequestUtil.httpGet(HOST_DAO + "api/reserve/listByType/device/" + deviceId + "/" + merchantId);
            return new Gson().fromJson(result, new TypeToken<List<ReserveDetail>>(){}.getType());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public List<ReserveDetail> listByBatchNo(String merchantId, String deviceId, String batchNo) {
        try {
            String result = HttpRequestUtil.httpGet(HOST_DAO + "api/reserve/list/" +merchantId+ "/" + deviceId + "/" + batchNo);

            LOGGER.info("listByBatchNo : " + result);
            return new Gson().fromJson(result, new TypeToken<List<ReserveDetail>>(){}.getType());
//            TypeReference<List<ReserveDetail>> mapType = new TypeReference<List<ReserveDetail>>() {};
//            return new ObjectMapper().readValue(result, mapType);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }
    
    public List<ReserveDetail> listGroupEZC(String terminalId, String deviceId, String batchNo) {
      try {
          String result = HttpRequestUtil.httpGet(HOST_DAO + "api/reserve/listEzc/" +terminalId+ "/" + deviceId + "/" + batchNo);

          LOGGER.info("listByBatchNo : " + result);
          return new Gson().fromJson(result, new TypeToken<List<ReserveDetail>>(){}.getType());
      } catch (Exception e) {
          LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
          return null;
      }
  }

    public List<ReserveDetail> listByUserId(String merchantId, String deviceId, String userId) {
        try {
            Map<String, String> request = new HashMap<String, String>();
            request.put("type", "MDU");
            request.put("merchantId", merchantId);
            request.put("deviceId", deviceId);
            request.put("userId", userId);
            String result = HttpRequestUtil.post(HOST_DAO + "api/reserve/list", new Gson().toJson(request));
            LOGGER.info("listByUserId : " + result);
            return new Gson().fromJson(result, new TypeToken<List<ReserveDetail>>(){}.getType());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public List<ReserveDetail> listTopOneByUserId(String merchantId, String deviceId, String userId) {
        try {
            Map<String, String> request = new HashMap<String, String>();
            request.put("type", "MDU_TOP1");
            request.put("merchantId", merchantId);
            request.put("deviceId", deviceId);
            request.put("userId", userId);
            String result = HttpRequestUtil.post(HOST_DAO + "api/reserve/list", new Gson().toJson(request));
            LOGGER.info("listTopOneByUserId : " + result);
            return new Gson().fromJson(result, new TypeToken<List<ReserveDetail>>(){}.getType());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }
}
