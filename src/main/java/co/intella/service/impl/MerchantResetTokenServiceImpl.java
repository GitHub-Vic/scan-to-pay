package co.intella.service.impl;

import co.intella.model.MerchantResetToken;
import co.intella.model.QBank;
import co.intella.model.QMerchantResetToken;
import co.intella.repository.MerchantResetTokenRepository;
import co.intella.service.MerchantResetTokenService;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;

/**
 * @author Alex
 */
@Service
@Transactional
public class MerchantResetTokenServiceImpl implements MerchantResetTokenService {
    @Resource
    private MerchantResetTokenRepository merchantResetTokenRepository;

    public void save(MerchantResetToken merchantResetToken) {
        merchantResetTokenRepository.save(merchantResetToken);
    }

    public MerchantResetToken getOne(String accountId, String resetToken) {
        QMerchantResetToken qMerchantResetToken = QMerchantResetToken.merchantResetToken;
        BooleanExpression predicate = qMerchantResetToken.accountId.eq(accountId).and(
                qMerchantResetToken.resetToken.eq(resetToken)).and(
                qMerchantResetToken.resetTimeout.gt(DateTime.now().toString("yyyyMMddHHmmss")).and(
                        qMerchantResetToken.isUsed.eq(false)
                )
        );

        return merchantResetTokenRepository.findOne(predicate);
    }
}
