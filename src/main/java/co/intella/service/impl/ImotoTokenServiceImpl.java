package co.intella.service.impl;

import co.intella.model.ImotoToken;
import co.intella.service.ImotoTokenService;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ImotoTokenServiceImpl implements ImotoTokenService {

    private final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Value("${host.dao}")
    private String DAO_URL;

    @Override
    public Integer insert(ImotoToken ntuImotoToken) {
        String result = HttpUtil.doPostJson(DAO_URL + "api/ImotoToken/insert", JsonUtil.toJsonStr(ntuImotoToken));
        return Integer.valueOf(result);
    }

    @Override
    public ImotoToken getOneForEffectiveByShortId(String shortId) {
        String result = HttpUtil.doGet(DAO_URL + "api/ImotoToken/getOneForEffectiveByShortId/" + shortId);
        ImotoToken ntuImotoToken = null;
        try {
            ntuImotoToken = JsonUtil.parseJson(result, ImotoToken.class);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return ntuImotoToken;
    }

    @Override
    public ImotoToken getOneForEffectiveByOutcode(String Outcode) {
        String result = HttpUtil.doGet(DAO_URL + "api/ImotoToken/getOneForEffectiveByOutcode/" + Outcode);
        ImotoToken ntuImotoToken = null;
        try {
            ntuImotoToken = JsonUtil.parseJson(result, ImotoToken.class);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return ntuImotoToken;
    }
}
