package co.intella.service.impl;

import co.intella.crypto.KeyReader;
import co.intella.model.IntTbLookupCode;
import co.intella.model.Merchant;
import co.intella.model.TradeDetail;
import co.intella.model.TradeDetailOtherInfo;
import co.intella.net.HttpRequestUtil;
import co.intella.service.LookupCodeService;
import co.intella.service.MerchantService;
import co.intella.service.NotifyService;
import co.intella.service.TradeDetailOtherInfoService;
import co.intella.utility.GeneralUtil;
import co.intella.utility.JsonUtil;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.security.PrivateKey;
import java.security.Signature;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author Alex
 */
@Service
public class NotifyServiceImpl implements NotifyService {

    private final Logger LOGGER = LoggerFactory.getLogger(NotifyServiceImpl.class);


    @Resource
    private MerchantService merchantService;

    @Autowired
    private Environment env;

    @Autowired
    private MessageSource messageSource;

    @Resource
    private TradeDetailOtherInfoService tradeDetailOtherInfoService;
    @Resource
    private LookupCodeService lookupCodeService;

    @Async
    public Boolean notifyMerchant(TradeDetail tradeDetail) throws Exception {

        Merchant merchant = merchantService.getOne(tradeDetail.getAccountId());

        String url = merchant.getNotifyUrl();

        if (url == null) {
            return false;
        } else {
            url = "https://" + url;
        }
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        map.put("Detail", tradeDetail.getDetail());
        map.put("MchId", tradeDetail.getAccountId());
        map.put("MethodName", GeneralUtil.getMethodName(tradeDetail.getMethod(), messageSource));
        map.put("Result", tradeDetail.getStatus().equals(SystemInstance.TRADE_SUCCESS) ? "0000" : "9999");
        map.put("StoreOrderNo", tradeDetail.getOrderId());
        map.put("TotalFee", String.valueOf(tradeDetail.getPayment()));
        if (!("20840").equals(tradeDetail.getMethod()) && tradeDetail.getMethod().startsWith("2")) {

            String[] a = tradeDetail.getDescription().split("-");
            map.put("LastFour", a[a.length - 1]);
        }
        if (lookupCodeService.isIParkingAccount(merchant.getAccountId())) {
            TradeDetailOtherInfo tradeDetailOtherInfo = tradeDetailOtherInfoService.getOneByUNHEXUuid(tradeDetail.getTradeDetailRandomId().toString());
            Map<String, String> tradeDetailOtherInfoMap = JsonUtil.parseJson(tradeDetailOtherInfo.getRequestDetail(), Map.class);
            map.put("LicensePlate", StringUtils.defaultIfBlank(tradeDetailOtherInfoMap.get("LicensePlate"), ""));
            map.put("Place", StringUtils.defaultIfBlank(tradeDetailOtherInfoMap.get("Place"), ""));
        }
        map.put("Sign", getSign(map));
        LOGGER.info("[postURL]" + url);
        LOGGER.info("[map]" + new Gson().toJson(map));
        try {
            /**
             * government for scan2pay-government 專案使用，
             * 根據特店帳號撈IntTbLookupCode如果有撈到資料，就把url替換成通知 scan2pay-government 的notifyUrl，
             * 由scan2pay-government接收、做紀錄、通知特店
             * by eason 2021/04/26
             */
            IntTbLookupCode government = lookupCodeService.findOne("government", tradeDetail.getAccountId());
            IntTbLookupCode  lookupCode = lookupCodeService.findOne("government", "notifyUrl");
            if(Objects.nonNull(government)&&Objects.nonNull(lookupCode)){
                url = "https://" + lookupCode.getType1();
                LOGGER.info("[postURL][to s2p-govrnment]: " + url);
            }

            String response = HttpRequestUtil.httpsPost(url, map);
            LOGGER.info("[notify] [government]response" + response);

        } catch (Exception e) {
            LOGGER.error("[notify][government][ERROR]" + e.getMessage());
        }

        return true;
    }


    public Boolean notifyMerchantTest(String merchantId) throws Exception {

        Merchant merchant = merchantService.getOne(merchantId);

        String url = merchant.getNotifyUrl();

        if (url == null) {
            return false;
        } else {
            url = "https://" + url;
        }

        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        map.put("Detail", "Item test");
        map.put("MethodName", GeneralUtil.getMethodName("11500", messageSource));
        map.put("MchId", merchantId);
        map.put("Result", "0000");
        map.put("StoreOrderNo", merchantId + "NotifyTest");
        map.put("TotalFee", "100");
        map.put("Sign", getSign(map));
        LOGGER.info("[notify] url:" + url);
        LOGGER.info("[notify] body" + new Gson().toJson(map));
        try {
            String response = HttpRequestUtil.httpsPost(url, map);
            LOGGER.info("[notify] response" + response);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }


    private String getSign(LinkedHashMap<String, String> map) throws Exception {

        String requestJson = (new Gson()).toJson(map);

        Signature signature = Signature.getInstance("SHA256withRSA");

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(env.getProperty("intella.private.key")).getFile());
        PrivateKey privateKey = KeyReader.loadPrivateKeyFromDER(file);

        signature.initSign(privateKey);

        signature.update(requestJson.getBytes());

        byte[] sign = signature.sign();
        return Base64.getEncoder().encodeToString(sign);

    }
}
