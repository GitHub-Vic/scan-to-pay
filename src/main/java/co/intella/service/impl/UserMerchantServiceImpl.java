package co.intella.service.impl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import co.intella.domain.UserMerchantRequest;
import co.intella.model.Merchant;
import co.intella.net.HttpRequestUtil;
import co.intella.service.UserMerchantService;

@Service
public class UserMerchantServiceImpl  implements UserMerchantService{
	
	private Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	@Value("${host.dao}")
    private String DAO_URL;
	
	@Autowired
    private MessageSource messageSource;
    
     private Locale locale = new Locale("zh_TW");
	
	@Override
	public Map<String, Object> getByUserId(String userId)  throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		if (StringUtils.isEmpty(userId)) {
			result.put("msgCode", "0005");
			result.put("msg", "userId" + messageSource.getMessage("co.intella.not.allow.empty", null, locale));
		} else {
			UserMerchantRequest userMerchantRequest = new UserMerchantRequest();
			userMerchantRequest.setUserId(userId);
			String response = HttpRequestUtil.post(DAO_URL + "api/userMerchant/userId", new Gson().toJson(userMerchantRequest));
			LOGGER.info("[UserMerchant][RESPONSE] getByUserId " + response);
	        JSONArray jsonArray = new JSONArray(response);
	        if (jsonArray.length() > 0) {
	        	result.put("msgCode", "0000");
	        	result.put("msg", "");
	        } else {
	        	result.put("msgCode", "0001");
	        	result.put("msg", messageSource.getMessage("co.intella.userMerchant.query.no.data", null, locale));
	        }
	        List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();
	        for (int i = 0; i < jsonArray.length(); i ++) {
	        	Map<String, Object> item = new HashMap<String, Object>();
	        	item.put("userId", jsonArray.getJSONObject(i).getString("userId"));
	        	item.put("merchantSeqId", jsonArray.getJSONObject(i).get("merchantSeqId"));
	        	item.put("isDefault", jsonArray.getJSONObject(i).getString("isDefault"));
	        	item.put("name", jsonArray.getJSONObject(i).getString("name"));
	        	item.put("accountId", jsonArray.getJSONObject(i).getString("accountId"));
	        	items.add(item);
	        }
	        result.put("data", items);
		}
		return result;
	}

	@Override
	public Map<String, Object> insert(UserMerchantRequest userMerchant) throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		List<String> errMsgList = new ArrayList<String>();
		if (StringUtils.isEmpty(userMerchant.getUserId())) {
			errMsgList.add("userId" + messageSource.getMessage("co.intella.not.allow.empty", null, locale));
		}
		if (StringUtils.isEmpty(userMerchant.getAccountId())) {
			errMsgList.add("accountId" + messageSource.getMessage("co.intella.not.allow.empty", null, locale));
		}
		if (StringUtils.isEmpty(userMerchant.getPin())) {
			errMsgList.add("pin" + messageSource.getMessage("co.intella.not.allow.empty", null, locale));
		}
		String errMsg = errMsgList.stream().collect(Collectors.joining(","));
		if (!errMsg.equals("")) {
			result.put("msg", errMsg);
            result.put("msgCode", "0005");
		} else {
			String responseUser = HttpRequestUtil.post(DAO_URL + "api/user/userId", new Gson().toJson(userMerchant));
			LOGGER.info("[UserMerchant][RESPONSE] insert responseUser " + responseUser);
	        JSONObject jsonObjectUser = new JSONObject(responseUser);
	        if (!jsonObjectUser.getString("userId").equals(userMerchant.getUserId())) {
	        	result.put("msg", messageSource.getMessage("co.intella.userMerchant.userId.not.exit", null, locale));
	            result.put("msgCode", "0004");
	        } else {
	        	RestTemplate restTemplate = new RestTemplate();
				String merchantResultString = restTemplate.getForObject(DAO_URL + "api/merchant/getOneByLoginId/{loginId}", String.class, userMerchant.getAccountId());
				LOGGER.info("[UserMerchant][RESPONSE] insert merchantResultString " + merchantResultString);
				Merchant merchantResult = new Gson().fromJson(merchantResultString, Merchant.class);
				if (merchantResult != null) {
					if (!getHash(userMerchant.getPin()).equals(merchantResult.getPin())) {
						result.put("msg", messageSource.getMessage("co.intella.userMerchant.account.or.pin.error", null, locale));
			            result.put("msgCode", "0003");
					} else {
						userMerchant.setMerchantSeqId(merchantResult.getMerchantSeqId());
						String response = HttpRequestUtil.post(DAO_URL + "api/userMerchant/userId", new Gson().toJson(userMerchant));
						LOGGER.info("[UserMerchant][RESPONSE] insert response " + response);
				        JSONArray jsonArray = new JSONArray(response);
				        if (jsonArray.length() > 0) {
				        	userMerchant.setIsDefault("0");
				        } else {
				        	userMerchant.setIsDefault("1");
				        }
				        boolean exitFlag = false;
				        for (int i = 0 ; i < jsonArray.length(); i ++) {
				        	JSONObject jsonObject = jsonArray.getJSONObject(i);
				        	String userId = jsonObject.getString("userId");
				        	Integer merchantSeqId = (Integer)jsonObject.get("merchantSeqId");
				        	if (userId.equals(userMerchant.getUserId()) && merchantSeqId == userMerchant.getMerchantSeqId().intValue()) {
				        		result.put("msg", messageSource.getMessage("co.intella.userMerchant.already.insert", null, locale));
					            result.put("msgCode", "0001");
					            exitFlag = true;
				        	}
				        }
				        if (!exitFlag) {
					        String count = HttpRequestUtil.post(DAO_URL + "api/userMerchant/insert", new Gson().toJson(userMerchant));
					        LOGGER.info("[UserMerchant][RESPONSE] insert count " + response);
					        if (Integer.parseInt(count) > 0) {
					        	result.put("msg", "");
					            result.put("msgCode", "0000");
					        } else {
					        	result.put("msg", messageSource.getMessage("co.intella.userMerchant.insert.fail", null, locale));
					            result.put("msgCode", "0002");
					        }
				        }
					}
				} else {
					result.put("msg", messageSource.getMessage("co.intella.userMerchant.account.or.pin.error", null, locale));
		            result.put("msgCode", "0006");
				}
	        }
		}
		return result;
	}

	@Override
	public Map<String, Object> delete(UserMerchantRequest userMerchant)  throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		List<String> errMsgList = new ArrayList<String>();
		if (StringUtils.isEmpty(userMerchant.getUserId())) {
			errMsgList.add("userId" + messageSource.getMessage("co.intella.not.allow.empty", null, locale));
		}
		if (userMerchant.getMerchantSeqId() == null) {
			errMsgList.add("merchantSeqId" + messageSource.getMessage("co.intella.not.allow.empty", null, locale));
		}
		String errMsg = errMsgList.stream().collect(Collectors.joining(","));
		if (!errMsg.equals("")) {
			result.put("msg", errMsg);
            result.put("msgCode", "0005");
		} else {
			String count = HttpRequestUtil.post(DAO_URL + "api/userMerchant/delete", new Gson().toJson(userMerchant));
	        LOGGER.info("[UserMerchant][RESPONSE] delete count " + count);
	        String response = HttpRequestUtil.post(DAO_URL + "api/userMerchant/userId", new Gson().toJson(userMerchant));
			LOGGER.info("[UserMerchant][RESPONSE] getByUserId " + response);
	        JSONArray jsonArray = new JSONArray(response);
	        boolean exitIsDefault = false;
	        for (int i = 0; i < jsonArray.length(); i ++) {
	        	JSONObject jsonObject = jsonArray.getJSONObject(i);
	        	String isDefault = jsonObject.getString("isDefault");
	        	if (isDefault.equals("1")) {
	        		exitIsDefault = true;
	        		break;
	        	}
	        }
	        if (!exitIsDefault &&  jsonArray.length() > 0) {
	        	JSONObject jsonObject = jsonArray.getJSONObject(0);
	        	jsonObject.put("isDefault", "1");
	        	 HttpRequestUtil.post(DAO_URL + "api/userMerchant/update", jsonObject.toString());
	        }
	        if (Integer.parseInt(count) > 0) {
	        	result.put("msg", "");
	        	result.put("msgCode", "0000");
	        } else {
	        	result.put("msg", messageSource.getMessage("co.intella.userMerchant.delete.fail", null, locale));
	        	result.put("msgCode", "0001");
	        }
		}
		return result;
	}
	
	private String getHash(String pin) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return byte2Hex(digest.digest(pin.getBytes()));
    }
	
	private String byte2Hex(byte[] inputBytes) {
        String result = "";
        for (byte currentByte : inputBytes) result += Integer.toString((currentByte & 0xff) + 0x100, 16).substring(1);
        return result;
    }

	@Override
	public Map<String, Object> update(UserMerchantRequest userMerchant) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		List<String> errMsgList = new ArrayList<String>();
		if (StringUtils.isEmpty(userMerchant.getUserId())) {
			errMsgList.add("userId" + messageSource.getMessage("co.intella.not.allow.empty", null, locale));
		}
		if (userMerchant.getMerchantSeqId() == null) {
			errMsgList.add("merchantSeqId" + messageSource.getMessage("co.intella.not.allow.empty", null, locale));
		}
		if (userMerchant.getIsDefault() == null) {
			errMsgList.add("isDefault" + messageSource.getMessage("co.intella.not.allow.empty", null, locale));
		}
		String errMsg = errMsgList.stream().collect(Collectors.joining(","));
		if (!errMsg.equals("")) {
			result.put("msg", errMsg);
            result.put("msgCode", "0005");
		} else {
	        String response = HttpRequestUtil.post(DAO_URL + "api/userMerchant/userId", new Gson().toJson(userMerchant));
			LOGGER.info("[UserMerchant][RESPONSE] getByUserId " + response);
	        JSONArray jsonArray = new JSONArray(response);
	        for (int i = 0; i < jsonArray.length(); i ++) {
	        	JSONObject jsonObject = jsonArray.getJSONObject(i);
	        	String isDefault = jsonObject.getString("isDefault");
	        	if (isDefault.equals("1")) {
	        		jsonObject.put("isDefault", "0");
	        		HttpRequestUtil.post(DAO_URL + "api/userMerchant/update", jsonObject.toString());
	        	}
	        }
	        String count = HttpRequestUtil.post(DAO_URL + "api/userMerchant/update", new Gson().toJson(userMerchant));
	        LOGGER.info("[UserMerchant][RESPONSE] update count " + count);
	        if (Integer.parseInt(count) > 0) {
	        	result.put("msg", "");
	        	result.put("msgCode", "0000");
	        } else {
	        	result.put("msg", messageSource.getMessage("co.intella.userMerchant.update.fail", null, locale));
	        	result.put("msgCode", "0001");
	        }
		}
		return result;
	}
}

