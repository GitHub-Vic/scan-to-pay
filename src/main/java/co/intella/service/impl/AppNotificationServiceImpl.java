package co.intella.service.impl;

import co.intella.domain.notify.ApnsMessage;
import co.intella.domain.notify.TransactionMessage;
import co.intella.model.*;
import co.intella.repository.MultiQrcodeRespository;
import co.intella.service.*;
import co.intella.utility.AppNotificationUtil;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Miles
 */
@Service
@Transactional
public class AppNotificationServiceImpl implements AppNotificationService {

    private final Logger LOGGER = LoggerFactory.getLogger(AppNotificationServiceImpl.class);

    @Value("${execution.mode}")
    private String EXECUTION_MODE;

    @Autowired
    private MessageSource messageSource;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private NotifyService notifyService;

    @Resource
    private MqttService mqttService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private RecipientService recipientService;

    @Resource
    private DonateService donateService;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private MultiQrcodeRespository multiQRCodeRespository;

    @Resource
    private NtuImotoService ntuImotoService;


    @Value("${hotst.mgt.server}")
    String MGT_SERVER;


    public void notifyApp(TradeDetail tradeDetail, Merchant merchantPid) {
        List<Recipient> list = new ArrayList<Recipient>(merchantPid.getRecipients());
        doNotify(tradeDetail);
    }


    public void notifyApp(String mchId, String orderId) {
        TradeDetail tradeDetail = tradeDetailService.getOne(mchId, orderId);
        doNotify(tradeDetail);
    }


    private void doNotify(TradeDetail tradeDetail) {
        String topic = tradeDetail.getAccountId();
        final TransactionMessage transactionMessage = AppNotificationUtil.getNotifyMessages(tradeDetail, messageSource);
        ApnsMessage apnsMessge = AppNotificationUtil.getApnsMessages(tradeDetail, messageSource);

        boolean check =  !"15300".equals(tradeDetail.getMethod()) || this.checkCashNotifyMch(tradeDetail) ;

        if (!"15300".equals(tradeDetail.getMethod()) || this.checkCashNotifyMQ(tradeDetail)) {
            this.doMQTTNotify(topic, transactionMessage);
            this.doAPNSNotify(topic, transactionMessage, apnsMessge, tradeDetail);
            this.doNotifyMerchant(tradeDetail, transactionMessage);
//            this.doDonateNotify(topic, transactionMessage, apnsMessge, tradeDetail);
        }

//        if (!"15300".equals(tradeDetail.getMethod()) || this.checkCashNotifyMch(tradeDetail)) {
//            this.doNotifyMerchant(tradeDetail, transactionMessage);
//        }
//        if (!"15300".equals(tradeDetail.getMethod()) || this.checkCashNotifyMch(tradeDetail)) {
//            this.doDonateNotify(topic, transactionMessage, apnsMessge, tradeDetail);
//        }
//        this.doMessageNotify(topic, transactionMessage, tradeDetail);
//        this.doMergeQrcodeUpdate(tradeDetail);
        this.doNotifyNtuImoto(tradeDetail);     //TODO 傳入的tradeDetail 會被存檔替換!!!
    }

    private void doMQTTNotify(String topic, TransactionMessage transactionMessage) {
        try {
            if (!"prod".equals(EXECUTION_MODE)) {
                topic += "-" + EXECUTION_MODE;
            }
            mqttService.publish("", new Gson().toJson(transactionMessage), topic);
            LOGGER.info("[DoNotify][MQTT][" + topic + "] -> " + new Gson().toJson(transactionMessage));
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("[DoNotify][MQTT][" + topic + "] -> " + new Gson().toJson(transactionMessage) + "\nError:" + e.getMessage());
        }
    }

    private void doAPNSNotify(String topic, TransactionMessage transactionMessage, ApnsMessage apnsMessge, TradeDetail tradeDetail) {
        try {

            List<Recipient> recipients = recipientService.listByMerchant(merchantService.getOne(tradeDetail.getAccountId()));

            List<Recipient> recipientsTemp = recipients.stream().filter(recipient -> recipient.getAppId() == null).collect(Collectors.toList());
            List<Recipient> recipientsB = recipients.stream().filter(recipient -> recipient.getAppId() != null).filter(recipient -> !recipientsTemp.contains(recipient)).collect(Collectors.toList());
            recipientsTemp.addAll(recipientsB);
            LOGGER.info("[DoNotify][APNS][doList] -> " + recipientsTemp.toString());

            // 20210220 因IOS13 以上原先的token拿取方法有問題無法做回調，故這邊先擋住錯誤的token
            List<Recipient> _recipientsTemp = recipientsTemp.stream()
                    .filter(recipient -> !StringUtils.defaultIfBlank(recipient.getApnsToken(), "").trim().startsWith("{")).collect(Collectors.toList());

            for (Recipient r : _recipientsTemp) {
                if (r.getApnsToken() != null) {
                    mqttService.publishAPNS(r.getApnsToken(), apnsMessge, r.getAppId()); //TODO test
                }
            }
            LOGGER.info("[DoNotify][APNS][" + topic + "] -> " + new Gson().toJson(transactionMessage));
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("[DoNotify][APNS][" + topic + "] -> " + new Gson().toJson(transactionMessage) + "\nError:" + e.getMessage());
        }
    }


    private void doDonateNotify(String topic, TransactionMessage transactionMessage, ApnsMessage apnsMessge, TradeDetail tradeDetail) {
        //Sol  新增回調佛光山，透過shortId查詢訂閱的信箱去通知
        if (StringUtils.isNotEmpty(tradeDetail.getQrcodeToken())) {
            try {
                List<JsonObject> list = donateService.getNotifyList(tradeDetail);
                //額外傳送shortId跟qrCode.detail
                QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(tradeDetail.getQrcodeToken());
                Gson gson = JsonUtil.getGson();
                JsonObject jsonObject = gson.fromJson(new Gson().toJson(transactionMessage), JsonObject.class);
                jsonObject.addProperty("qrShortId", qrcodeParameter.getShortId());
                jsonObject.addProperty("qrDetail", qrcodeParameter.getDetail());
                jsonObject.addProperty("systemOrderId", tradeDetail.getSystemOrderId());

                for (JsonObject json : list) {
                    mqttService.publish("", jsonObject.toString(), json.get("userId").getAsString());
                    LOGGER.info("[donateService][NotifyMerchant][" + json.get("userId").getAsString() + "] -> " + new Gson().toJson(transactionMessage));
                }

                //TODO APNS test
                for (JsonObject json : list) {
                    String userId = json.get("userId").getAsString();
                    List<Recipient> recipientList = recipientService.listByUserId(userId);
                    for (Recipient recipient : recipientList) {
                        ApnsMessage apnsMessage = new ApnsMessage();
                        apnsMessage.setTitle("Intella");
                        apnsMessage.setMsg(jsonObject.toString());
                        mqttService.publishAPNS(recipient.getApnsToken(), apnsMessge, "donate");
                        LOGGER.info("[donateService][NotifyAPNS][" + recipient.getApnsToken() + "] -> " + new Gson().toJson(transactionMessage));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                LOGGER.error("[donateService][NotifyMerchant][" + tradeDetail.getAccountId() + "] -> " + new Gson().toJson(transactionMessage) + "\nError:" + e.getMessage());
            }

        }
    }


    private void doMessageNotify(String topic, TransactionMessage transactionMessage, TradeDetail tradeDetail) {
        //sol   mgt 簡訊點數回調通知
        try {
            Map<String, Object> notifyMap = new HashMap<>();
            notifyMap.put("shortId", tradeDetail.getQrcodeToken());
            notifyMap.put("count", tradeDetail.getPayment());
            notifyMap.put("merchantId", tradeDetail.getAccountId());
            String reqJsonStr = JsonUtil.toJsonStr(notifyMap);
            LOGGER.info("[mgt-server][coda] notify reqJsonStr =>" + reqJsonStr);
            String resutl = HttpUtil.doPostJson(MGT_SERVER + "batchChargeManagement/coda/notify", JsonUtil.toJsonStr(notifyMap));
            LOGGER.info("[mgt-server][coda] notify resutl =>" + resutl);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("[donateService][NotifyMerchant][" + tradeDetail.getAccountId() + "] -> " + new Gson().toJson(transactionMessage) + "\nError:" + e.getMessage());
        }
    }


    private void doMergeQrcodeUpdate(TradeDetail tradeDetail) {
        //ziv 因各家支付回調不同 合併Qrcode付款 更新被合併Qrcode狀態寫在此處
        String shortId = tradeDetail.getQrcodeToken();
        //被合併的付款 清除同樣的mergeShorId 且mergeShorId的Qrcode status改為0
        try {
            MultiQrcode mutiQrcode = multiQRCodeRespository.findOneByShortId(shortId);
            if (!StringUtils.isNotBlank(mutiQrcode.getMergeShortId())) {
                String mergeShortId = mutiQrcode.getMergeShortId();
                multiQRCodeRespository.updateCleanOriginQrcodeMergeShortId(mergeShortId);
                multiQRCodeRespository.updateQrcode(mergeShortId);
            }

        } catch (Exception e) {
            LOGGER.error("[PayerQrcode] sub not found shortId = " + shortId);
        }

        //合併的付款 被合併的Qrcode status改為0
        try {
            List<MultiQrcode> mutiQrcodes = multiQRCodeRespository.findListByMergeShortId(shortId);
            mutiQrcodes.forEach(mutiQrcode -> {
                multiQRCodeRespository.updateQrcode(mutiQrcode.getShortId());
            });
        } catch (Exception e) {
            LOGGER.error("[PayerQrcode] super not found shortId = " + shortId);
        }
    }


    private void doNotifyMerchant(TradeDetail tradeDetail, TransactionMessage transactionMessage) {
        try {
            notifyService.notifyMerchant(tradeDetail);
            LOGGER.info("[NotifyMerchant][" + tradeDetail.getAccountId() + "] -> " + new Gson().toJson(transactionMessage));

        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("[NotifyMerchant][" + tradeDetail.getAccountId() + "] -> " + new Gson().toJson(transactionMessage) + "\nError:" + e.getMessage());
        }
    }

    /**
     * 傳入的tradeDetail 會被存檔替換!!
     *
     * @param tradeDetail
     */

    private void doNotifyNtuImoto(TradeDetail tradeDetail) {
        try {
            TradeDetail _tradeDetail = tradeDetailService.save(tradeDetail); //edit  sol : 因怕imoto可能立即退款，故這邊會替代並先把當時傳入狀態存起來
            boolean doScan2PayRefund = ntuImotoService.doNotify(_tradeDetail);
            if (doScan2PayRefund) {
                _tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());      //重新取得訂單物件
                this.transformVO2String(tradeDetail, _tradeDetail);     //將原物件的String 型態的成員都做替換
            }
            LOGGER.info("[doNotifyNtuImoto][" + tradeDetail.getAccountId() + "] -> " + tradeDetail.getOrderId() + "\t" + tradeDetail.getRefundStatus());
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("[doNotifyNtuImoto][" + tradeDetail.getAccountId() + "] -> " + tradeDetail.getOrderId() + "\t" + tradeDetail.getQrcodeToken());
        }
    }


    private void transformVO2String(TradeDetail tradeDetail, TradeDetail _tradeDetail) throws Exception {
        Class<? extends Object> fromClazz = _tradeDetail.getClass();

        for (Field field : fromClazz.getDeclaredFields()) {
            String fieldName = field.getName();
            String getMethodName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);

            try {
                Method getMethod = fromClazz.getMethod(getMethodName);
                Object result = getMethod.invoke(_tradeDetail);

                if (result instanceof String) {
                    Object _result = getMethod.invoke(tradeDetail);
                    field.setAccessible(true);
                    field.set(tradeDetail, (String) result);
                    LOGGER.info("transformVO2String. " + fieldName + "\t" + _result + " to " + result);
                }
            } catch (Exception e) {
                LOGGER.info("transformVO2String error." + fieldName);
            }
        }
    }

    /**
     * 現金要求說要可以控制要通知那些方式
     * 0：都不通知(含小於0)，1：只通知 MQ推播，2：只通知Notify Merchant，3：都通知(含NULL或大於3)
     *
     * @param tradeDetail
     * @return
     */
    private boolean checkCashNotifyMQ(TradeDetail tradeDetail) {
        int s = Integer.valueOf(StringUtils.defaultIfBlank(tradeDetail.getPaymentAccount().getPlatformCheckCode(), "3"));
        return 0 < s && 2 != s;
    }

    /**
     * 現金要求說要可以控制要通知那些方式
     * 0：都不通知(含小於0)，1：只通知 MQ推播，2：只通知Notify Merchant，3：都通知(含NULL或大於3)
     *
     * @param tradeDetail
     * @return
     */
    private boolean checkCashNotifyMch(TradeDetail tradeDetail) {
        int s = Integer.valueOf(StringUtils.defaultIfBlank(tradeDetail.getPaymentAccount().getPlatformCheckCode(), "3"));
        return 0 < s && 1 != s;
    }
}
