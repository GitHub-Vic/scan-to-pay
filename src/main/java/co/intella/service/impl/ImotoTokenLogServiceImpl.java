package co.intella.service.impl;

import co.intella.model.ImotoTokenLog;
import co.intella.service.ImotoTokenLogService;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ImotoTokenLogServiceImpl implements ImotoTokenLogService {

    private final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Value("${host.dao}")
    private String DAO_URL;

    @Override
    public Integer insert(ImotoTokenLog ntuImotoTokenLog) {
        int count = -1;
        String result = HttpUtil.doPostJson(DAO_URL + "api/imotoTokenLog/insert", JsonUtil.toJsonStr(ntuImotoTokenLog));
        try {
            count = Integer.valueOf(result);
        } catch (NumberFormatException e) {
            LOGGER.error("[NumberFormatException] result:" + result);
        }

        return count;
    }

    @Override
    public List<ImotoTokenLog> getListByOutercodeAndTokenSeq(String tokenOutercode, long imotoTokenSeq) {
        String result = HttpUtil.doGet(DAO_URL + "api/imotoTokenLog/getListByOutercodeAndTokenSeq/" + imotoTokenSeq + "/" + tokenOutercode);
        return JsonUtil.parseJsonList(result, ImotoTokenLog.class);
    }

    @Override
    public ImotoTokenLog getLastLogByOutercodeAndTokenSeq(String tokenOutercode, long imotoTokenSeq) {

        List<ImotoTokenLog> imotoTokenLogList = this.getListByOutercodeAndTokenSeq(tokenOutercode, imotoTokenSeq);
        ImotoTokenLog ntuImotoTokenLog = null;
        LocalDateTime now = LocalDateTime.now();
        imotoTokenLogList = imotoTokenLogList.parallelStream().filter(imotoTokenLog -> {
            LocalDateTime logTime = imotoTokenLog.getCrDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            return Duration.between(logTime, now).toMinutes() < 5;
        }).collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(imotoTokenLogList)) {
            ntuImotoTokenLog = imotoTokenLogList.get(0);
        }

        return ntuImotoTokenLog;
    }
}
