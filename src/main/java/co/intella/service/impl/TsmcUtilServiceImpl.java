package co.intella.service.impl;

import co.intella.model.RequestHeader;
import co.intella.service.TicketLogicService;
import co.intella.service.TsmcUtilService;
import com.google.gson.JsonObject;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.Future;

@Service
public class TsmcUtilServiceImpl implements TsmcUtilService {

    @Resource
    private TicketLogicService ticketLogicService;

    @Override
    public Future<String> asyncTicket(RequestHeader requestHeader, JsonObject requestData) throws Exception {
        return new AsyncResult<String>(ticketLogicService.doRequest(requestHeader, requestData));
    }
}
