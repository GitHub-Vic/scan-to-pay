package co.intella.service.impl;

import co.intella.crypto.AesCryptoUtil;
import co.intella.domain.allpay.*;
import co.intella.domain.integration.*;
import co.intella.model.PaymentAccount;
import co.intella.model.RefundDetail;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import co.intella.service.*;
import co.intella.utility.AllPayUtil;
import co.intella.utility.GeneralUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.crypto.SecretKey;
import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * @author Alex
 */
@Service
public class AllpayServiceImpl implements AllpayService {

    private final Logger LOGGER = LoggerFactory.getLogger(AllpayServiceImpl.class);


    @Autowired
    private Environment env;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Value("${host.request.allpay}")
    private String ALLPAY_URL;

    @Value("${allpay.online.platformId}")
    private String ALLPAY_ONLINE_PLATFORM_ID;

    @Value("${allpay.offline.platformId}")
    private String ALLPAY_OFFLINE_PLATFORM_ID;

    @Autowired
    private MessageSource messageSource;

    @Resource
    private RefundDetailService refundDetailService;


    public String micropay(AllpayMicropayRequestData request, PaymentAccount paymentAccount) throws Exception {

        Map<String, String> map = new HashMap<String, String>();
        map.put("Barcode", request.getBarcode());
        map.put("CheckOutType", request.getCheckOutType());
        map.put("MerchantID", request.getMerchantId());
        map.put("MerchantTradeDate", request.getMerchantTradeDate());
        map.put("MerchantTradeNo", request.getMerchantTradeNo());
        map.put("MerchantTradeTime", request.getMerchantTradeTime());
        map.put("PlatformID", request.getPlatformId());
        map.put("PosID", request.getPosId());
        map.put("Remark", request.getRemark());
        map.put("UseRedeem", request.getUseRedeem());
        map.put("UseRedeemAmount", request.getUseRedeemAmount());

        map.put("TradeAmount", request.getTradeAmount());
        map.put("TradeDesc", request.getTradeDesc());
        map.put("StoreID", request.getStoreId());

        request.setCheckMacValue(getCheckMacValue(map, paymentAccount));

        LOGGER.info("[Allpay][request]" + new Gson().toJson(request));

        String encryptRequest = getAllpayEncryptRequest(request, "1", paymentAccount);

        LOGGER.info("[hash]" + paymentAccount.getHashKey());

        SecretKey secretKey = AesCryptoUtil.convertSecretKey(paymentAccount.getHashKey().getBytes());

        String result = allpayPost(ALLPAY_URL + "/POS/Payment", encryptRequest); //HttpRequestUtil.httpsPost(env.getProperty("host.request.allpay.micropay"), map);
        LOGGER.info("[Allpay][Raw Result]" + result);
        AllpayGeneralResponseBody allpayGeneralResponseBody = convertStr2Object(result);

        String responseData = URLDecoder.decode(allpayGeneralResponseBody.getData(), "UTF-8");
        responseData = URLDecoder.decode(responseData, "UTF-8");

        return AesCryptoUtil.decrypt(secretKey, paymentAccount.getHashIV().getBytes(), Base64.decode(responseData));
    }

    public String singleQuery(AllpaySingleQueryRequestData request, PaymentAccount paymentAccount) throws Exception {

        Map<String, String> map = new HashMap<String, String>();
        map.put("Barcode", request.getBarcode());
        map.put("MerchantID", request.getMerchantId());
        map.put("MerchantTradeNo", request.getMerchantTradeNo());
        map.put("PlatformID", request.getPlatformId());


        request.setCheckMacValue(getCheckMacValue(map, paymentAccount));

        String encryptRequest = getAllpayEncryptRequest(request, "1", paymentAccount);

        SecretKey secretKey = AesCryptoUtil.convertSecretKey(paymentAccount.getHashKey().getBytes());

        LOGGER.info("[Allpay][TRANSACTION][BEGIN]");
        String result = allpayPost(ALLPAY_URL + "/POS/QueryTrade", encryptRequest); //HttpRequestUtil.httpsPost(env.getProperty("host.request.allpay.micropay"), map);
        LOGGER.info("[Allpay][TRANSACTION][END]" + result);
        AllpayGeneralResponseBody allpayGeneralResponseBody = convertStr2Object(result);

        String responseData = URLDecoder.decode(allpayGeneralResponseBody.getData(), "UTF-8");
        responseData = URLDecoder.decode(responseData, "UTF-8");

        return AesCryptoUtil.decrypt(secretKey, paymentAccount.getHashIV().getBytes(), Base64.decode(responseData));

    }

    public String commit(AllpayCommitRequestData request, PaymentAccount paymentAccount) throws Exception {
        Map<String, String> commitMap = new HashMap<String, String>();
        commitMap.put("PlatformID", request.getPlatformId());
        commitMap.put("MerchantID", request.getMerchantId());
        commitMap.put("MerchantTradeNo", request.getMerchantTradeNo());
        commitMap.put("StoreID", request.getStoreId());
        commitMap.put("PosID", request.getPosId());
        commitMap.put("Barcode", request.getBarcode());
        commitMap.put("AllpayTradeNo", request.getAllpayTradeNo());
        commitMap.put("TradeType", request.getTradeType());

        request.setCheckMacValue(getCheckMacValue(commitMap, paymentAccount));

        String encryptRequest = getAllpayEncryptRequest(request, "1", paymentAccount);

        String result = allpayPost(ALLPAY_URL + "/POS/Commit", encryptRequest); //HttpRequestUtil.httpsPost(env.getProperty("host.request.allpay.micropay"), map);
        AllpayGeneralResponseBody allpayGeneralResponseBody = convertStr2Object(result);
        String responseData = URLDecoder.decode(allpayGeneralResponseBody.getData(), "UTF-8");
        responseData = URLDecoder.decode(responseData, "UTF-8");

        SecretKey secretKey = AesCryptoUtil.convertSecretKey(paymentAccount.getHashKey().getBytes());

        String decryptData = AesCryptoUtil.decrypt(secretKey, paymentAccount.getHashIV().getBytes(), Base64.decode(responseData));
        AllpaycommitResponseData allpaycommitResponseData = new Gson().fromJson(decryptData, AllpaycommitResponseData.class);
        if (allpaycommitResponseData.getRtnCode().equals("1")) {
            return "1";
        }

        return null;
    }

    public String refund(AllpayRefundRequestData request, PaymentAccount paymentAccount) throws Exception {
        //Offline
        Map<String, String> map = new HashMap<String, String>();
        map.put("Barcode", request.getBarcode());
        map.put("MerchantID", request.getMerchantId());
        map.put("MerchantTradeNo", request.getMerchantTradeNo());
        map.put("PlatformID", request.getPlatformId());
        map.put("PosID", request.getPosId());
        map.put("StoreID", request.getStoreId());
        map.put("AllpayTradeNo", request.getAllpayTradeNo());
        map.put("RefundAmount", request.getRefundAmount());

        request.setCheckMacValue(getCheckMacValue(map, paymentAccount));

        String encryptRequest = getAllpayEncryptRequest(request, "1", paymentAccount);

        SecretKey secretKey = AesCryptoUtil.convertSecretKey(paymentAccount.getHashKey().getBytes());

        String result = allpayPost(ALLPAY_URL + "/POS/ChargeBack", encryptRequest);

        AllpayGeneralResponseBody allpayGeneralResponseBody = convertStr2Object(result);
        String responseData = URLDecoder.decode(allpayGeneralResponseBody.getData(), "UTF-8");
        responseData = URLDecoder.decode(responseData, "UTF-8");

        String decryptData = AesCryptoUtil.decrypt(secretKey, paymentAccount.getHashIV().getBytes(), Base64.decode(responseData));
        AllpayRefundResponseData allpayRefundResponseData = new Gson().fromJson(decryptData, AllpayRefundResponseData.class);

        LOGGER.info("[AllpayRefundResponseData]" + decryptData);
        //String decryptData = "{\"PlatformID\":\"2000132\",\"MerchantID\":\"2000132\",\"MerchantTradeNo\":\"allpa0f4g3y006\",\"StoreID\":\"10001\",\"PosID\":\"skb0001\",\"Barcode\":\"AP163A02SSXRHLI15V\",\"AllpayTradeNo\":\"1705081139509303\",\"RefundTradeNo\":\"3524\",\"RefundDate\":\"20170508\",\"RefundTime\":\"162758\",\"RefundAmount\":\"1\",\"PayAmount\":\"1\",\"RedeemAmount\":\"0\",\"RtnCode\":\"1\",\"RtnMsg\":\"refund success\",\"CheckMacValue\":\"4DD63E5456098DC2272A274D63674E642CC83E76E2018F2C02BD601943E34903\"}";
        //AllpayRefundResponseData allpayRefundResponseData =new Gson().fromJson(decryptData, AllpayRefundResponseData.class);

        if (allpayRefundResponseData.getRtnCode().equals("1")) {
            AllpayCommitRequestData allpayCommitRequestData = new Gson().fromJson(new Gson().toJson(allpayRefundResponseData), AllpayCommitRequestData.class);
            allpayCommitRequestData.setPlatformId(allpayRefundResponseData.getPlatformId());
            allpayCommitRequestData.setMerchantId(allpayRefundResponseData.getMerchantId());
            allpayCommitRequestData.setTradeType("210");
            String commitResult = commit(allpayCommitRequestData, paymentAccount);

            if (commitResult.equals("1")) {
                return decryptData;
            }
            //todo if commit fail
        }
        return decryptData;
    }

    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {
        String generalResponseData = SystemInstance.EMPTY_STRING;

        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));


        String result;
        if (requestHeader.getServiceType().equals(SystemInstance.TYPE_MICROPAY)) {
            PaymentAccount paymentAccount = paymentAccountService.getOne(integrateMchId, 19);
            AllpayMicropayRequestData allpayMicropayRequestData = new Gson().fromJson(data.getAsString(), AllpayMicropayRequestData.class);
            allpayMicropayRequestData.setTradeDesc(SystemInstance.EMPTY_STRING);
            allpayMicropayRequestData.setMerchantId(paymentAccount.getAccount());
            allpayMicropayRequestData.setPlatformId(ALLPAY_OFFLINE_PLATFORM_ID);
            if (allpayMicropayRequestData.getStoreId() == null)
                allpayMicropayRequestData.setStoreId("00001");
            if (allpayMicropayRequestData.getPosId() == null)
                allpayMicropayRequestData.setPosId("00001");
            allpayMicropayRequestData.setMerchantTradeDate(requestHeader.getCreateTime().substring(0, 8));
            allpayMicropayRequestData.setMerchantTradeTime(requestHeader.getCreateTime().substring(8));
            allpayMicropayRequestData.setCheckOutType("1");
            allpayMicropayRequestData.setRemark(SystemInstance.EMPTY_STRING);
            allpayMicropayRequestData.setUseRedeem("N");
            allpayMicropayRequestData.setUseRedeemAmount("0");

            tradeProcedureService.setTradeDetailRequest(requestHeader, data);
            result = micropay(allpayMicropayRequestData, paymentAccount);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);


            AllpayMicropayResponseData allpayMicropayResponseData = new Gson().fromJson(result, AllpayMicropayResponseData.class);

            ResponseGeneralHeader responseGeneralHeader = new AllPayUtil().getAllpayResponseHeader(allpayMicropayResponseData, requestHeader, messageSource);
            IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();

            if (allpayMicropayResponseData.getRtnCode().equals("1")) {
                integratedMicropayResponseData.setSysOrderNo(allpayMicropayResponseData.getAllpayTradeNo());
                integratedMicropayResponseData.setTotalFee(String.valueOf(allpayMicropayResponseData.getTradeAmount()));
            } else {
                integratedMicropayResponseData.setSysOrderNo(SystemInstance.EMPTY_STRING);
                integratedMicropayResponseData.setTotalFee(SystemInstance.EMPTY_STRING);
            }
            integratedMicropayResponseData.setStoreOrderNo(allpayMicropayResponseData.getMerchantTradeNo());
            integratedMicropayResponseData.setAuthCode(allpayMicropayResponseData.getBarcode());
            integratedMicropayResponseData.setPlatformRsp(new Gson().toJson(allpayMicropayResponseData));

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedMicropayResponseData);


            generalResponseData = new Gson().toJson(responseGeneralBody);
            LOGGER.info("integrate response ready :" + generalResponseData);


        } else if (requestHeader.getServiceType().equals("SingleOrderQuery")) {
            LOGGER.info("[Allpay][SingleOrderQuery][]");
            PaymentAccount paymentAccount;
            AllpaySingleQueryRequestData allpaySingleQueryRequestData = new Gson().fromJson(data.getAsString(), AllpaySingleQueryRequestData.class);


            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
            if (tradeDetail.getServiceType().equals(SystemInstance.TYPE_MICROPAY)) {
                paymentAccount = paymentAccountService.getOne(integrateMchId, 19);
            } else {
                paymentAccount = paymentAccountService.getOne(integrateMchId, 5);
            }
//            if (allpaySingleQueryRequestData.getMerchantTradeNo()!= null){
//                tradeDetail = tradeDetailService.getOne(integrateMchId, allpaySingleQueryRequestData.getMerchantTradeNo());
//            }

            LOGGER.info("[Allpay][SingleOrderQuery][Get tradedetail]");

            allpaySingleQueryRequestData.setMerchantId(paymentAccount.getAccount());
            allpaySingleQueryRequestData.setPlatformId(tradeDetail.getServiceType().equals(SystemInstance.TYPE_MICROPAY) ? ALLPAY_OFFLINE_PLATFORM_ID : ALLPAY_ONLINE_PLATFORM_ID);
            allpaySingleQueryRequestData.setBarcode(tradeDetail.getBarcode());

            LOGGER.info("[Allpay][SingleOrderQuery][Requset].." + new Gson().toJson(allpaySingleQueryRequestData));

            result = singleQuery(allpaySingleQueryRequestData, paymentAccount);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);
            LOGGER.info("[Allpay][SingleOrderQuery][result].." + result);

            AllpaySingleQueryResponseData allpaySingleQueryResponseData = new Gson().fromJson(result, AllpaySingleQueryResponseData.class);

            ResponseGeneralHeader responseGeneralHeader = new AllPayUtil().getAllpayResponseHeader(allpaySingleQueryResponseData, requestHeader, messageSource);
            IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();

            integratedSingleOrderQueryResponseData.setStoreOrderNo(allpaySingleQueryResponseData.getMerchantTradeNo());
            integratedSingleOrderQueryResponseData.setSysOrderNo(allpaySingleQueryResponseData.getAllpayTradeNo());
            integratedSingleOrderQueryResponseData.setFeeType(SystemInstance.CURRENCY_TWD);
            integratedSingleOrderQueryResponseData.setDeviceInfo(allpaySingleQueryResponseData.getPosId());
            integratedSingleOrderQueryResponseData.setBody(SystemInstance.EMPTY_STRING);
            integratedSingleOrderQueryResponseData.setStoreInfo(allpaySingleQueryResponseData.getStoreID());
            integratedSingleOrderQueryResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
            integratedSingleOrderQueryResponseData.setPaidAt(tradeDetail.getCreateDate());
            integratedSingleOrderQueryResponseData.setRefundedMsg(tradeDetail.getRefundStatus());
            RefundDetail refundDetail = refundDetailService.getOneByOrderIdAndMethod(tradeDetail.getOrderId(), tradeDetail.getMethod());
            if (Objects.nonNull(refundDetail)) {
                integratedSingleOrderQueryResponseData.setRefundedAt(refundDetail.getCreateTime());
            }
//            integratedSingleOrderQueryResponseData.setPlatformRsp( new Gson().toJson(allpaySingleQueryResponseData));
            if (allpaySingleQueryResponseData.getTradeStatus().equals("110")) {
                integratedSingleOrderQueryResponseData.setOrderStatus("1");
            } else if (allpaySingleQueryResponseData.getTradeStatus().equals("220") || allpaySingleQueryResponseData.getTradeStatus().equals("210")) {
                integratedSingleOrderQueryResponseData.setOrderStatus("3");
            } else if (allpaySingleQueryResponseData.getTradeStatus().equals("130")) {
                integratedSingleOrderQueryResponseData.setOrderStatus("0");
            } else if (allpaySingleQueryResponseData.getTradeStatus().equals("0")) {
                integratedSingleOrderQueryResponseData.setOrderStatus("2");
            }

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedSingleOrderQueryResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);


            LOGGER.info("integrate response ready :" + generalResponseData);


        } else if (requestHeader.getServiceType().equals("Refund")) {

            AllpayRefundRequestData allpayRefundRequestData = new Gson().fromJson(data.getAsString(), AllpayRefundRequestData.class);

            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

            PaymentAccount paymentAccount;
            if (tradeDetail.getServiceType().equals(SystemInstance.TYPE_MICROPAY)) {
                LOGGER.info("[Refund][Get19]");
                paymentAccount = paymentAccountService.getOne(integrateMchId, 19);
            } else {
                LOGGER.info("[Refund][5]");
                paymentAccount = paymentAccountService.getOne(integrateMchId, 5);
            }

            if (tradeDetail.getServiceType().equals("OLPay")) {
                AllpayOLRefundRequest allpayOLRefundRequest = new Gson().fromJson(data.getAsString(), AllpayOLRefundRequest.class);
                allpayOLRefundRequest.setMerchantId(paymentAccount.getAccount());
                allpayOLRefundRequest.setPlatformId(ALLPAY_ONLINE_PLATFORM_ID);
                allpayOLRefundRequest.setTradeNo(tradeDetail.getSystemOrderId());
                if (tradeDetail.getType() == 1) {
                    result = aioRefund(allpayOLRefundRequest, paymentAccount);
                    tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);
                    ResponseGeneralHeader responseGeneralHeader = new AllPayUtil().getAllpayAioRefundResponseHeader(result, requestHeader, messageSource);
                    IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();
                    if (result.startsWith("1")) {
                        integratedRefundResponseData.setSysRefundNo("R" + tradeDetail.getOrderId());
                        integratedRefundResponseData.setRefundedAt(responseGeneralHeader.getResponseTime());
                    } else {
                        integratedRefundResponseData.setSysRefundNo(SystemInstance.EMPTY_STRING);
                        integratedRefundResponseData.setRefundedAt(SystemInstance.EMPTY_STRING);
                    }
                    integratedRefundResponseData.setStoreRefundNo("R" + tradeDetail.getOrderId());
                    integratedRefundResponseData.setStoreOrderNo(tradeDetail.getOrderId());
                    integratedRefundResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
                    integratedRefundResponseData.setPlatformRsp(result);

                    ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
                    responseGeneralBody.setHeader(responseGeneralHeader);
                    responseGeneralBody.setData(integratedRefundResponseData);

                    generalResponseData = new Gson().toJson(responseGeneralBody);
                    LOGGER.info("integrate response ready :" + generalResponseData);
                } else if (tradeDetail.getType() == 2) {
                    result = creditCardRefund(allpayOLRefundRequest, paymentAccount);
                    tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);
                    AllpayOLCreditRefundResponse allpayOLCreditRefundResponse = new Gson().fromJson(result, AllpayOLCreditRefundResponse.class);
                    ResponseGeneralHeader responseGeneralHeader = new AllPayUtil().getAllpayOLCreditRefundResponseHeader(result, requestHeader, messageSource);
                    IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();
                    if (allpayOLCreditRefundResponse.getRtnCode().equals("1")) {
                        integratedRefundResponseData.setSysRefundNo(allpayOLCreditRefundResponse.getRefundTradeNo());
                        integratedRefundResponseData.setRefundedAt(responseGeneralHeader.getResponseTime());
                    } else {
                        integratedRefundResponseData.setSysRefundNo(SystemInstance.EMPTY_STRING);
                        integratedRefundResponseData.setRefundedAt(SystemInstance.EMPTY_STRING);
                    }
                    integratedRefundResponseData.setStoreRefundNo("R" + tradeDetail.getOrderId());
                    integratedRefundResponseData.setStoreOrderNo(tradeDetail.getOrderId());
                    integratedRefundResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
                    integratedRefundResponseData.setPlatformRsp(result);

                    ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
                    responseGeneralBody.setHeader(responseGeneralHeader);
                    responseGeneralBody.setData(integratedRefundResponseData);

                    generalResponseData = new Gson().toJson(responseGeneralBody);
                    LOGGER.info("integrate response ready :" + generalResponseData);

                }


            } else {

                allpayRefundRequestData.setMerchantId(paymentAccount.getAccount());
                allpayRefundRequestData.setPlatformId(ALLPAY_OFFLINE_PLATFORM_ID);
                allpayRefundRequestData.setStoreId("00001");

                allpayRefundRequestData.setBarcode(tradeDetail.getBarcode());
                allpayRefundRequestData.setAllpayTradeNo(tradeDetail.getSystemOrderId());
                allpayRefundRequestData.setMerchantTradeNo(tradeDetail.getOrderId());

                LOGGER.info("[AllPay][Request]" + new Gson().toJson(allpayRefundRequestData));

                result = refund(allpayRefundRequestData, paymentAccount);
                tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);
                AllpayRefundResponseData allpayRefundResponseData = new Gson().fromJson(result, AllpayRefundResponseData.class);

                ResponseGeneralHeader responseGeneralHeader = new AllPayUtil().getAllpayResponseHeader(allpayRefundResponseData, requestHeader, messageSource);
                IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();

                if (allpayRefundResponseData.getRtnCode().equals("1")) {
                    integratedRefundResponseData.setSysRefundNo(allpayRefundResponseData.getRefundTradeNo());
                    integratedRefundResponseData.setRefundedAt(allpayRefundResponseData.getRefundDate() + allpayRefundResponseData.getRefundTime());
                } else {
                    integratedRefundResponseData.setSysRefundNo(SystemInstance.EMPTY_STRING);
                    integratedRefundResponseData.setRefundedAt(SystemInstance.EMPTY_STRING);


                }
                integratedRefundResponseData.setStoreRefundNo(allpayRefundResponseData.getMerchantTradeNo());
                integratedRefundResponseData.setStoreOrderNo(allpayRefundResponseData.getMerchantTradeNo());
                integratedRefundResponseData.setSysOrderNo(allpayRefundResponseData.getAllpayTradeNo());
                integratedRefundResponseData.setPlatformRsp(new Gson().toJson(allpayRefundResponseData));

                ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
                responseGeneralBody.setHeader(responseGeneralHeader);
                responseGeneralBody.setData(integratedRefundResponseData);


                generalResponseData = new Gson().toJson(responseGeneralBody);
                LOGGER.info("integrate response ready :" + generalResponseData);
            }

        } else if (requestHeader.getServiceType().equals("CreateOrder")) {

            // todo set real response
            PaymentAccount paymentAccount = paymentAccountService.getOne(integrateMchId, 19);
            AllpayCreateOrderRequestData allpayMicropayRequestData = new Gson().fromJson(data.getAsString(), AllpayCreateOrderRequestData.class);
            allpayMicropayRequestData.setTradeDesc("dwdwdw");
            allpayMicropayRequestData.setMerchantId(paymentAccount.getAccount());
            allpayMicropayRequestData.setPlatformId(ALLPAY_OFFLINE_PLATFORM_ID);
            allpayMicropayRequestData.setMerchantTradeTime("2017/06/27 14:00:00");
            allpayMicropayRequestData.setUseRedeem("N");
            allpayMicropayRequestData.setReturnURL("https://s.intella.co/");
            allpayMicropayRequestData.setItemName("test item");
            allpayMicropayRequestData.setCustomField1("0");
            allpayMicropayRequestData.setCustomField2("0");
            allpayMicropayRequestData.setCustomField3("0");
            allpayMicropayRequestData.setTradeEffectiveTime("600");

            result = createOrder(allpayMicropayRequestData, paymentAccount);
            LOGGER.info("create order result :" + result);

            AllpayMicropayResponseData allpayMicropayResponseData = new Gson().fromJson(result, AllpayMicropayResponseData.class);

            ResponseGeneralHeader responseGeneralHeader = new AllPayUtil().getAllpayResponseHeader(allpayMicropayResponseData, requestHeader, messageSource);
            IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();

            if (allpayMicropayResponseData.getRtnCode().equals("1")) {
                integratedMicropayResponseData.setSysOrderNo(allpayMicropayResponseData.getAllpayTradeNo());
                integratedMicropayResponseData.setTotalFee(String.valueOf(allpayMicropayResponseData.getTradeAmount()));
            } else {
                integratedMicropayResponseData.setSysOrderNo(SystemInstance.EMPTY_STRING);
                integratedMicropayResponseData.setTotalFee(SystemInstance.EMPTY_STRING);
            }
            integratedMicropayResponseData.setStoreOrderNo(allpayMicropayResponseData.getMerchantTradeNo());
            integratedMicropayResponseData.setAuthCode(allpayMicropayResponseData.getBarcode());
            integratedMicropayResponseData.setPlatformRsp(new Gson().toJson(allpayMicropayResponseData));

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedMicropayResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);
            LOGGER.info("integrate response ready :" + generalResponseData);

        } else if (requestHeader.getServiceType().equals("InApp")) {

            return allPayInApp(data, requestHeader);

        }

        return generalResponseData;
    }

    private String allPayInApp(JsonPrimitive data, RequestHeader requestHeader) throws Exception {

        AllpayMicropayRequestData allpayMicropayRequestData = new Gson().fromJson(data.getAsString(), AllpayMicropayRequestData.class);

        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

        String tradeToken = getTradeToken(tradeDetail.getTradeDetailRandomId().toString());


        IntegratedInAppResponseData integratedInAppResponseData = new IntegratedInAppResponseData();

        ResponseGeneralHeader responseGeneralHeader = new AllPayUtil().getAllpayInAppResponseHeader(allpayMicropayRequestData, requestHeader, messageSource);

        integratedInAppResponseData.setPartnerId(requestHeader.getMerchantId());
        integratedInAppResponseData.setTradeToken(tradeToken);

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        responseGeneralBody.setHeader(responseGeneralHeader);
        responseGeneralBody.setData(integratedInAppResponseData);


        String generalResponseData = new Gson().toJson(responseGeneralBody);
        LOGGER.info("integrate response ready :" + generalResponseData);
        return generalResponseData;
    }

    public String createOrder(AllpayCreateOrderRequestData request, PaymentAccount paymentAccount) throws Exception {
        Map<String, String> map = new HashMap<String, String>();
        map.put("PlatformID", request.getPlatformId());
        map.put("MerchantID", request.getMerchantId());
        map.put("MerchantTradeNo", request.getMerchantTradeNo());
        map.put("MerchantTradeTime", request.getMerchantTradeTime());
        map.put("TradeAmount", request.getTradeAmount());
        map.put("TradeDesc", request.getTradeDesc());
        map.put("ItemName", request.getItemName());
        map.put("ReturnURL", request.getReturnURL());
        map.put("CustomField1", request.getCustomField1());
        map.put("CustomField2", request.getCustomField2());
        map.put("CustomField3", request.getCustomField3());
        map.put("UseRedeem", request.getUseRedeem());
        map.put("TradeEffectiveTime", request.getTradeEffectiveTime());


        request.setCheckMacValue(getCheckMacValue(map, paymentAccount));


        String encryptRequest = getAllpayEncryptRequest(request, "2", paymentAccount);

        SecretKey secretKey = AesCryptoUtil.convertSecretKey(paymentAccount.getHashKey().getBytes());

        String result = allpayPost("https://corp-stage.allpay.com.tw/QRCodeCashier/CreateTrade", encryptRequest); //HttpRequestUtil.httpsPost(env.getProperty("host.request.allpay.micropay"), map);
        //result is json type
        //AllpayGeneralResponseBody allpayGeneralResponseBody = convertStr2Object(result);

        String responseData = URLDecoder.decode("7loGM%2fjV%2f2s0KXOhLPqZrtZsPUPyuTcnKSaUhltFdjDFA2I8cXKlw%2bXRfkPeODJcAJau9qJ9LZFkc8H4SY9PMMChv77DubmNH0dzqAmr3vk0Uc%2bfH8sJ%2bhe%2bnwG7cxZqE5LJ8KeTMuRiHITYiLC%2fLDPwy1N%2bDwviV1UWf8R6MArOCsGkf8sHjcrIe%2b0S1xwLoVimbiqQftR6WiTBnDAejTAN7Cxe3moE6mvtpjNbEHhogFWStEbxgWEa2f8OZsjYvaX4Htw2cIq8LHKS9xosiFbjdaoSiB36ux9yu6oWM0H%2bWzjhbw6aUj%2bEq5efJvwBLY5gHY%2fjqU1bMPOemlEhEuSpCP%2b7RXUW21LGGUQ8JgQulqGUhC2dcbB65oYMnW09bVKRQBkvJPYnFNlq0pV63DMNYLKLb3v%2b%2b3Kkvkg5iuuXAyNT6TWghNXH%2b3ArAwX6R7IQ3c4UyuJFExjEoxY7VBtfmugQNnlZm9wAVbJv6UEf4%2f6MvYXfx%2bqmG3m%2b%2bcjAfeQmj%2fdQ86p%2f8429fqTTTgian0GH14VC%2fxZG8j13XxI%3d", "UTF-8");
        //responseData  = URLDecoder.decode(responseData,"UTF-8");

        return AesCryptoUtil.decrypt(secretKey, paymentAccount.getHashIV().getBytes(), Base64.decode(responseData));

        //return null;
    }

    public String getCheckMacValue(Map<String, String> map, PaymentAccount paymentAccount) throws Exception {
        StringBuilder checkStr = new StringBuilder();
        Map<String, String> treeMap = new TreeMap<String, String>(map);

        for (Map.Entry<String, String> entry : treeMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            checkStr.append(key).append("=").append(value).append("&");
        }
        checkStr = new StringBuilder("HashKey=" + paymentAccount.getHashKey() + "&" + checkStr + "HashIV=" + paymentAccount.getHashIV());
        checkStr = new StringBuilder(URLEncoder.encode(checkStr.toString(), "UTF-8").toLowerCase());
        return GeneralUtil.getHash(checkStr.toString()).toUpperCase();
    }

    private String getAllpayEncryptRequest(AllpayBasicRequestData request, String type, PaymentAccount paymentAccount) throws Exception {

        //Class<?> c = request.getClass();
        String platformId = request.getPlatformId();//c.getDeclaredMethod("getPlatformId").invoke(request,null).toString();
        String merchantId = request.getMerchantId();//c.getDeclaredMethod("getMerchantId").invoke(request,null).toString();

        String requestJson = new ObjectMapper().writeValueAsString(request);
        LOGGER.info("[getAllpayEncryptRequest]requestJson" + requestJson);

        SecretKey secretKey = AesCryptoUtil.convertSecretKey(paymentAccount.getHashKey().getBytes());

        LOGGER.info("hashIV" + paymentAccount.getHashIV().substring(4));

        String encodeStr = Base64.encode(AesCryptoUtil.encrypt(secretKey, paymentAccount.getHashIV().getBytes(), requestJson));
        encodeStr = encodeStr.replace("\n", "");
        String requestData = URLEncoder.encode(encodeStr, "UTF-8");
        requestData = URLEncoder.encode(requestData, "UTF-8");
        String req;
        if (type.equals("2")) {//Third party(Online)

            req = "PlatformID=" + platformId + "&" +
                    "MerchantID=" + merchantId + "&" +
                    "Version=1&" +
                    "Encryption=1&" +
                    "Format=2&" +
                    "Data=" + requestData;
        } else {//offline
            req = "PlatformID=" + platformId + "&" +
                    "MerchantID=" + merchantId + "&" +
                    //"Version=1&"+
                    "Encryption=1&" +
                    "Format=2&" +
                    "Data=" + requestData;
        }
        LOGGER.info("[getAllpayEncryptRequest]" + req);

        return req;

    }

    private AllpayGeneralResponseBody convertStr2Object(String result) {
        String[] split = result.split("&");
        Map<String, String> map = new HashMap<String, String>();
        for (String element : split) {
            String[] temp = element.split("=");
            map.put(temp[0], temp[1]);
        }
        JsonElement jsonElement = new Gson().toJsonTree(map);
        return new Gson().fromJson(jsonElement, AllpayGeneralResponseBody.class);

    }

    private JsonElement convertStr2JsonElement(String result) {
        String[] split = result.split("&");
        Map<String, String> map = new HashMap<String, String>();
        for (String element : split) {
            String[] temp = element.split("=");
            if (temp.length == 1) {
                map.put(temp[0], "1");
            } else {
                map.put(temp[0], temp[1]);
            }
        }
        return new Gson().toJsonTree(map);


    }

    private StringBuilder geturlform(Map<String, String> map) throws Exception {
        StringBuilder checkStr = new StringBuilder();
        Map<String, String> treeMap = new TreeMap<String, String>(map);

        for (Map.Entry<String, String> entry : treeMap.entrySet()) {
//            LOGGER.info(entry.getValue().toString());
            String key = entry.getKey();
            String value = URLEncoder.encode(entry.getValue());

            checkStr.append(key).append("=").append(value).append("&");
        }
        checkStr = checkStr.delete(checkStr.length() - 1, checkStr.length());
        return checkStr;
    }

    public String allpayPost(String url, String query) throws Exception {
        URL requestUrl = new URL(url);
        HttpURLConnection con = (HttpsURLConnection) requestUrl.openConnection();
        con.setRequestMethod("POST");

        con.setRequestProperty("Content-length", String.valueOf(query.length()));
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        con.setDoOutput(true);
        con.setDoInput(true);

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();
        DataInputStream input = new DataInputStream(con.getInputStream());

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(input));
        String result;

        StringBuilder buffer = new StringBuilder();
        while ((result = bufferedReader.readLine()) != null) {
            buffer.append(result);
        }

        input.close();

        return buffer.toString();
    }

    public String aioRefund(AllpayOLRefundRequest request, PaymentAccount paymentAccount) throws Exception {
        Map<String, String> map = new HashMap<String, String>();
        map.put("MerchantID", request.getMerchantId());
        map.put("MerchantTradeNo", request.getMerchantTradeNo());
        map.put("TradeNo", request.getTradeNo());
        map.put("ChargeBackTotalAmount", request.getChargeBackTotalAmount());
        map.put("PlatformID", request.getPlatformId());
        map.put("Remark", "");


        map.put("CheckMacValue", getCheckMacValue(map, paymentAccount));


        String encryptRequest = geturlform(map).toString();
        LOGGER.info("[AllpayaioRefund][encryptRequest]:" + encryptRequest);

        String result = allpayPost(env.getProperty("host.request.allpay.olrefund.wallet"), encryptRequest);
        LOGGER.info("[AllpayaioRefund][result]:" + result);

        return result;

    }

    public String creditCardRefund(AllpayOLRefundRequest request, PaymentAccount paymentAccount) throws Exception {

        TradeDetail tradeDetail = tradeDetailService.getOne(request.getMerchantTradeNo());
        String action;
        String nDate;
        DateTime createDateTime = DateTime.parse(tradeDetail.getCreateDate(), DateTimeFormat.forPattern(SystemInstance.DATE_PATTERN));
        if (createDateTime.getHourOfDay() < 20) {
            nDate = createDateTime.toString("yyyyMMdd").concat("200000");
        } else {
            nDate = createDateTime.plusDays(1).toString("yyyyMMdd").concat("200000");
        }
        if (nDate.compareTo(DateTime.now().toString(SystemInstance.DATE_PATTERN)) == -1) {
            action = "R";
        } else {
            action = "N";
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("MerchantID", request.getMerchantId());
        map.put("MerchantTradeNo", request.getMerchantTradeNo());
        map.put("TradeNo", request.getTradeNo());
        map.put("Action", action);
        map.put("TotalAmount", request.getChargeBackTotalAmount());
        map.put("PlatformID", request.getPlatformId());


        map.put("CheckMacValue", getCheckMacValue(map, paymentAccount));


        String encryptRequest = geturlform(map).toString();

        LOGGER.info("[AllpaycreditCardRefund][encryptRequest]:" + encryptRequest);

        SecretKey secretKey = AesCryptoUtil.convertSecretKey(paymentAccount.getHashKey().getBytes());

        String result = allpayPost(env.getProperty("host.request.allpay.olrefund.credit"), encryptRequest); //HttpRequestUtil.httpsPost(env.getProperty("host.request.allpay.micropay"), map);
        LOGGER.info("[AllpaycreditCardRefund][result]:" + result);
        JsonElement jsonElement = convertStr2JsonElement(result);

        AllpayOLCreditRefundResponse allpayOLCreditRefundResponse = new Gson().fromJson(jsonElement, AllpayOLCreditRefundResponse.class);

        String responseData = new Gson().toJson(allpayOLCreditRefundResponse);
        responseData = URLDecoder.decode(responseData, "UTF-8");

        return responseData;

    }

    public String AioAll(AllpayAio requestData, PaymentAccount paymentAccount) throws Exception {
        Map<String, String> map = new HashMap<String, String>();
        map.put("MerchantID", requestData.getMerchantId());
        map.put("MerchantTradeNo", requestData.getMerchantTradeNo());
        //map.put("StoreID",requestData.getStoreId());
        map.put("MerchantTradeDate", requestData.getMerchantTradeDate());
        map.put("PaymentType", "aio");
        map.put("TotalAmount", requestData.getTotalAmount());
        map.put("TradeDesc", requestData.getTradeDesc());
        map.put("ItemName", requestData.getItemName());
        map.put("ReturnURL", requestData.getReturnURL());
        map.put("ChoosePayment", "TopUpUsed");
        map.put("ClientBackURL", "https://s.intella.co/allpaypass/api/test/2");
        map.put("EncryptType", requestData.getEncryptType());
        return getCheckMacValue(map, paymentAccount);
    }

    private String getTradeToken(String alias) throws Exception {
        URL url = new URL("http://localhost/intella-allpaypass/allpaypass/allpay.php?alias=" + alias);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        String token = "";

        BufferedReader in;

        if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {

            in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            token = in.readLine();

        } else {
            InputStream err = conn.getErrorStream();
            LOGGER.debug("[Allpay][InApp][getTradeToken]error");
        }
        LOGGER.info("[Allpay][TradeToken]" + token);
        return token;
    }
}

