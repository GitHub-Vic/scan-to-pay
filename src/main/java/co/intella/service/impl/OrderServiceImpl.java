package co.intella.service.impl;

import co.intella.model.Merchant;
import co.intella.model.QrcodeParameter;
import co.intella.net.HttpRequestUtil;
import co.intella.service.LookupCodeService;
import co.intella.service.MerchantService;
import co.intella.service.OrderService;
import co.intella.utility.JsonUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Alex
 */
@Service
public class OrderServiceImpl implements OrderService {

    private final Logger LOGGER = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Value("${host.dao}")
    private String DAO_URL;

    @Resource
    private LookupCodeService lookupCodeService;

    @Resource
    private MerchantService merchantService;

    public String createOrderId(String merchantId) {

        QrcodeParameter qrcodeParameter = new QrcodeParameter();
        return this.createOrderId(merchantId, qrcodeParameter);
    }

    public String createOrderId(String merchantId, QrcodeParameter qrcodeParameter) {

        String orderId = null;
        try {

            if (lookupCodeService.isFGSAccount(merchantId)) {

                orderId = this.createFGSOrderId(merchantId);
                orderId += StringUtils.defaultIfEmpty(qrcodeParameter.getStoreInfo(), "");
            } else {
                orderId = HttpRequestUtil.httpGet(DAO_URL + "api/orderId/create/" + URLEncoder.encode(merchantId, "UTF-8"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return orderId;
    }


    public String createOrderIdBySequence(String merchantId) {

        String orderId = null;
        try {

//            if (lookupCodeService.isFGSAccount(merchantId)) {
//                orderId = this.createFGSOrderId(merchantId);
//            } else {
            orderId = HttpRequestUtil.httpGet(DAO_URL + "api/orderId/seqName/twpay/merchantId/" + merchantId);
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return orderId;
    }

    @Override
    public Map<String, String> createOrderIdByMchId(String merchantId) {

        Map<String, String> resultMap = new HashMap<>();
        try {
            String result = HttpRequestUtil.httpGet(DAO_URL + "api/orderId/create/appOrder/" + merchantId);
            LOGGER.info("[createOrderIdByMchId] result =  " + result);
            Gson gson = JsonUtil.getGson();
            resultMap = gson.fromJson(result, new TypeToken<HashMap<String, String>>() {
            }.getType());
            Merchant merchant = merchantService.getOne(Long.valueOf(merchantId));
            if (lookupCodeService.isFGSAccount(merchant.getAccountId()) && MapUtils.isNotEmpty(resultMap)) {
                String orderId = this.createFGSOrderId(merchant.getAccountId(), resultMap.get("orderNo"));
                resultMap.put("orderNo", orderId);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultMap;
    }


    public String createDGpayOrderId(String merchantId) {

        String orderId = null;
        try {

            if (lookupCodeService.isFGSAccount(merchantId)) {

                orderId = this.createFGSOrderId(merchantId);
            } else {
                orderId = HttpRequestUtil.httpGet(DAO_URL + "api/orderId/seqName/dgpay/merchantId/" + merchantId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return orderId;
    }


    /**
     * 佛光山  客制訂單號碼
     *
     * @param merchantId
     * @return
     */
    @Override
    public String createFGSOrderId(String merchantId) {
        Merchant merchant = merchantService.getOne(merchantId);
        Map<String, String> resultMap = this.createOrderIdByMchId(String.valueOf(merchant.getMerchantSeqId()));

        return resultMap.get("orderNo");
    }

    /**
     *  佛光山訂單浩；最尾碼需要加上辨識碼，存放在qrcodeParameter.StoreInfo
     *  因來源太多，有點懶惰整合，故辨識碼在呼叫端各自處裡串上
     * @param merchantId
     * @param ordeNo
     * @return
     */
    private String createFGSOrderId(String merchantId, String ordeNo) {

        StringBuilder sb = new StringBuilder();
        LocalDateTime now = LocalDateTime.now();

        sb.append(merchantId)

                .append(String.valueOf(now.getYear()).substring(2))
                .append(String.format("%02d", now.getMonthValue()))
                .append(String.format("%02d", now.getDayOfMonth()))
                .append("P")
                .append(ordeNo.substring(ordeNo.length() - 4));


        return sb.toString();
    }
}
