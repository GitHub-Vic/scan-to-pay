package co.intella.service.impl;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.querydsl.core.types.dsl.BooleanExpression;

import co.intella.controller.QrcodePaymentController;
import co.intella.model.QQrcodeParameter;
import co.intella.model.QrType7;
import co.intella.model.QrcodeParameter;
import co.intella.repository.QrType7Repository;
import co.intella.repository.QrcodeParameterRepository;
import co.intella.repository.impl.QrType7RepositoryImpl;
import co.intella.service.QrType7Service;

@Service
public class QrType7ServiceImpl implements QrType7Service {
	@Resource
	private QrType7Repository qrType7Repository;

	@Resource
	private QrcodeParameterRepository qrcodeParameterRepository;
	
	@Resource
	private QrType7RepositoryImpl qrType7RepositoryImpl;

	private final Logger LOGGER = LoggerFactory.getLogger(QrcodePaymentController.class);

	@Override
	public Map<String, Object> saveQrType7(QrType7 qrType7) throws InterruptedException {

		Map<String, Object> response = new HashMap<String, Object>();
		String responseCode = "0000";

		String shortId = qrType7.getShortId();

		QQrcodeParameter qQrcodeParameter = QQrcodeParameter.qrcodeParameter;
		BooleanExpression predicate = qQrcodeParameter.shortId.eq(shortId);

		QrcodeParameter qrcodeParameter = qrcodeParameterRepository.findOne(predicate);
		String newTimeExpire = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
		String oldTimreExpire = qrcodeParameter.getTimeExpire();
		
		if(!"7".equals(qrcodeParameter.getCodeType())) {
			responseCode="9993";	
		} else if (null == oldTimreExpire
				|| Duration.between(LocalDateTime.parse(oldTimreExpire, dtf2), now).toMinutes() >= 5) {
			qrcodeParameter.setTimeExpire(newTimeExpire);
			qrcodeParameterRepository.save(qrcodeParameter);
			qrType7.setAccountId(qrcodeParameter.getMchId());

			try {
				QrType7 qrType7Saved = qrType7Repository.save(qrType7);
				response.put("data", qrType7Saved);
			} catch (Exception e) {
				LOGGER.error(e.getMessage());
				responseCode = "9999";
			}
		} else {// 時間小於五分鐘
			responseCode = "9995";
		}
		response.put("responseCode", responseCode);
		return response;
	}

	@Override
	public int getDemoInfoAmount(String shortId) {
		QrType7 qrType7=qrType7RepositoryImpl.searchTheNewest(shortId);
		return qrType7==null?-1:qrType7.getAmount();
	}

}
