package co.intella.service.impl;


import co.intella.domain.CheckoutCtrlVo;
import co.intella.domain.contratStore.*;
import co.intella.domain.easycard.EzCardReceipt;
import co.intella.domain.icash.*;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.exception.*;
import co.intella.model.*;
import co.intella.net.HttpRequestUtil;
import co.intella.repository.CheckoutRepository;
import co.intella.service.*;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Strings;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Service
public class ICashServiceImpl implements ICashService {
    private final Logger LOGGER = LoggerFactory.getLogger(ICashServiceImpl.class);

    @Value("${host.iCash.TransactionUrl}")
    private String ICASH_TRANSACTION_URL;

    @Value("${host.iCash.NontransactionUrl}")
    private String ICASH_NON_TRANSACTION_URL;

    @Value("${iCash.TestMode}")
    private String TEST_MODE;

    @Value("${iCash.ClientIP}")
    private String CLIENT_IP;

    @Value("${host.request.ezc}")
    private String EZC_HOST;

    @Value("${host.dao}")
    private String HOST_DAO;

    @Resource
    private MerchantService merchantService;

    @Resource
    private TicketLogicService ticketLogicService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private OperationHistoryService operationHistoryService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private ReserveDetailService reserveDetailService;

    @Resource
    private RefundDetailService refundDetailService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private ReserveCancelService reserveCancelService;

    @Resource
    private LookupCodeService lookupCodeService;

    @Resource
    private CheckoutRepository checkoutRepository;

    @Resource
    private INT_TB_CheckoutCtrlService INT_TB_CheckoutCtrlService;

    @Resource
    private INT_TB_CheckoutLogService INT_TB_CheckoutLogService;

    @Resource
    private TradeDetailOtherInfoService tradeDetailOtherInfoService;

    @Autowired
    private Environment env;

    private Locale locale = new Locale("zh_TW");

    @Autowired
    private MessageSource messageSource;

    // 只有歐特儀、英特拉paymentAccount.PlatformCheckCode
    final private String oldPlatformCheckCode = "ITL|I00";

    @Override
    public String ezRequest(RequestHeader requestHeader, JsonObject requestData, Merchant merchant) throws Exception {

        Map<String, Object> verificationResultMap = ticketLogicService.verification(requestHeader, requestData, merchant);
        final long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        String resultStr;
        Device device = (Device) verificationResultMap.get("device");
        List<PaymentAccount> paymentAccountList = ((List<PaymentAccount>) verificationResultMap.get("paymentAccountList")).parallelStream()
                .filter(paymentAccount1 -> bankId == paymentAccount1.getBank().getBankId()).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(paymentAccountList)) {
            throw new InvalidDataFormatException("payment account not found");
        }
        PaymentAccount paymentAccount = paymentAccountList.get(0);
        //TODO  有新增serviceType 如果必須要有卡片的相關serviceType，請去TicketLogicServiceImpl 也增加到相對的 private static final String[]
        String serviceType = requestHeader.getServiceType();
        switch (serviceType) {
            case "TermAuth":       //登入
                resultStr = doTermAuth(device, paymentAccount, requestHeader, requestData);
                break;
            case "BalanceQuery":  //讀卡
                resultStr = doQueryCardInfo(device, paymentAccount, requestHeader, requestData);
                break;
            case "Payment":      //交易(含自動加值)
                resultStr = doTradeSale(device, paymentAccount, requestHeader, requestData, merchant);
                break;
            case "Refund":    //退款
                resultStr = doTradeRefund(device, paymentAccount, requestHeader, requestData, merchant);
                break;
            case "Reserve":    //手動加值
                resultStr = doTradeCharge(device, paymentAccount, requestHeader, requestData, merchant);
                break;
            case "TradeChargeCancel":    //手動加值取消
                resultStr = doTradeChargeCancel(device, paymentAccount, requestHeader, requestData, merchant);
                break;
            case "GetApVersion":    //取得POS API版本
                resultStr = doGetApVersion(device, paymentAccount, requestHeader, requestData);
                break;
            case "StoreMain":       // 建立門市主檔       sh  每天 3點  正式機在A2
                resultStr = doStoreMain();
                break;
            case "BlcDownload":       //  下載黑名單      sh 每天 3點  正式機在A2
                resultStr = doBlcDownload();
                break;
            case "OrderQuery":    // TODO  ezc抄來的
                resultStr = doOrderQuery(requestHeader, requestData, merchant, device, paymentAccount);
                break;
            case "DeviceQuery": // TODO  ezc抄來的
                resultStr = doDeviceQuery(merchant, requestHeader);
                break;
            case "IdQuery":
                resultStr = doIdQuery(device, paymentAccount, requestHeader, requestData);
                break;
            default:
                throw new ServiceTypeNotSupportException(serviceType + " ServiceType Not Support.");
        }

        LOGGER.error("[ezRequest][ICASH] response => " + resultStr);
        return resultStr;
    }

    /**
     * 登入 末端授權
     *
     * @param device
     * @param paymentAccount
     * @param requestHeader
     * @return
     */
    private String doTermAuth(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader, JsonObject requestData) throws InvokeAPIException, JsonProcessingException {

        LOGGER.info("[ICASH][TermAuth][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");
        IcashTermAuthRequest request = new IcashTermAuthRequest(TEST_MODE, device, paymentAccount);
        String requestJsonStr = JsonUtil.toJsonStr(request);

        LOGGER.info("[ICASH][TermAuth]request => " + requestJsonStr);
        String result = this.sendPost(ICASH_TRANSACTION_URL, requestJsonStr);
        LOGGER.info("[ICASH][TermAuth]response => " + result);

        IcashTermAuthResponse response = JsonUtil.parseJson(result, IcashTermAuthResponse.class);

        if (StringUtils.isNotEmpty(response.getNewAesKey())) {
            device.setAesKey(response.getNewAesKey());
        }
        deviceService.save(device);

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    /**
     * 讀卡
     *
     * @param device
     * @param paymentAccount
     * @param requestHeader
     * @return
     */
    private String doQueryCardInfo(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader, JsonObject requestData) throws JsonProcessingException, InvokeAPIException {

        LOGGER.info("[ICASH][QueryCardInfo][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");
        IcashQueryCardInfoRequest request = new IcashQueryCardInfoRequest(TEST_MODE, device, paymentAccount);
        String requestJsonStr = JsonUtil.toJsonStr(request);

        LOGGER.info("[ICASH][QueryCardInfo]request => " + requestJsonStr);
        String result = this.sendPost(ICASH_TRANSACTION_URL, requestJsonStr);
        LOGGER.info("[ICASH][QueryCardInfo]response => " + result);

        IcashQueryCardInfoResponse response = JsonUtil.parseJson(result, IcashQueryCardInfoResponse.class);

        String userId = requestData.has("UserId") ? requestData.get("UserId").getAsString() : response.getCardNumberForPrint();
        LOGGER.info("[ICASH][QueryCardInfo]chang EzcCardId  => " + response.getEzCardID() + " to " + userId);
        response.setEzCardID(userId);

        if (StringUtils.isNotEmpty(response.getNewAesKey())) {
            device.setAesKey(response.getNewAesKey());
        }
        deviceService.save(device);

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    /**
     * 交易(含自動加值)
     *
     * @param device
     * @param paymentAccount
     * @param requestHeader
     * @return
     */
    private String doTradeSale(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader, JsonObject requestData, Merchant merchant) throws Exception {

        LOGGER.info("[ICASH][TradeSale][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");

        String userId = this.verificationForTradeSale(device, requestData, merchant, paymentAccount);

        String batchNo = this.getBatchNumber(paymentAccount);

        TradeDetail tradeDetail = this.createOrder(device, paymentAccount, requestHeader, requestData, batchNo, userId);

        IcashTradeSaleRequest request = new IcashTradeSaleRequest(TEST_MODE, device, paymentAccount);
        request.setAmount(requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString());
        if (requestData.has("Cashier") && StringUtils.isNotEmpty(requestData.get("Cashier").getAsString())) {
            request.setCashierCode(requestData.get("Cashier").getAsString());
        }
        String requestJsonStr = JsonUtil.toJsonStr(request);

        LOGGER.info("[ICASH][TradeSale]request => " + requestJsonStr);
        String result = this.sendPost(ICASH_TRANSACTION_URL, requestJsonStr);
        LOGGER.info("[ICASH][TradeSale]response => " + result);

        IcashTradeSaleResponse response = new Gson().fromJson(result, IcashTradeSaleResponse.class);
        return this.updateDoTradeSale(requestHeader, requestData, device, merchant, request, response, tradeDetail, paymentAccount);
    }

    /**
     * 退款退貨
     *
     * @param device
     * @param paymentAccount
     * @param requestHeader
     * @return
     */
    private String doTradeRefund(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader, JsonObject requestData, Merchant merchant) throws Exception {

        LOGGER.info("[ICASH][TradeRefund][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");
        long isRetry = requestData.get("Retry").getAsLong();
        if (isRetry >= 4) {
            LOGGER.error("[ICASH][TradeRefund]. " + device.getOwnerDeviceId() + ", " + requestHeader.getMerchantId());
            throw new RuntimeException("Already retry 3 times.");
        }

        String orderId = requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString();
        TradeDetail tradeDetail = tradeDetailService.getOne(orderId);
        if (Objects.isNull(tradeDetail)) {
            throw new OrderNotFoundException(SystemInstance.ORDER_NOT_FOUND);
        } else if (tradeDetail.getRefundStatus() != null
                && tradeDetail.getRefundStatus().equals(SystemInstance.TRADE_REFUND_SUCCESS)) {
            LOGGER.error("[ICASH][TradeRefund] tradeDetail already refund.");
            throw new OrderAlreadyRefundedException("tradeDetail already refund.");
        } else if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
            LOGGER.info("tradeDetail not TRADE_SUCCESS \t" + tradeDetail.getOrderId() + "\t" + tradeDetail.getStatus());
            throw new OtherAPIException(messageSource.getMessage("response.7351", null, locale));
        } else {
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_INIT);
            tradeDetailService.save(tradeDetail);
            LOGGER.info("[ICASH][TradeRefund] update tradeDetail and create refund detail.");
        }

        IcashTradeRefundRequest request = new IcashTradeRefundRequest(TEST_MODE, device, paymentAccount);
        request.setAmount(String.valueOf(tradeDetail.getPayment()));
        String requestJsonStr = JsonUtil.toJsonStr(request);

        LOGGER.info("[ICASH][TradeRefund]request => " + requestJsonStr);
        String result = this.sendPost(ICASH_TRANSACTION_URL, requestJsonStr);
        LOGGER.info("[ICASH][TradeRefund]response => " + result);

        IcashTradeRefundResponse response = new Gson().fromJson(result, IcashTradeRefundResponse.class);
        return this.updateDoRefund(requestHeader, requestData, device, merchant, response, tradeDetail, paymentAccount);
    }

    /**
     * 手動加值
     *
     * @param device
     * @param paymentAccount
     * @param requestHeader
     * @return
     */
    private String doTradeCharge(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader, JsonObject requestData, Merchant merchant) throws Exception {

        LOGGER.info("[ICASH][TradeCharge][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");

        long isRetry = requestData.get("Retry").getAsLong();
        String orderId = requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString();
        String batchNum = this.getBatchNumber(paymentAccount);
        String userId;
        if (isRetry > 0 && isRetry < 4) {
            ReserveDetail reserveDetail = reserveDetailService.getOne(requestHeader.getMerchantId(), orderId);
            userId = reserveDetail.getUserId();
        } else if (isRetry >= 4) {
            LOGGER.error("[ICASH][TradeCharge]. " + device.getOwnerDeviceId() + ", " + requestHeader.getMerchantId());
            throw new RuntimeException("Already retry 3 times.");
        } else {
            userId = this.getUserId(merchant, requestData, device, paymentAccount);
        }

        IcashTradeChargeRequest request = new IcashTradeChargeRequest(TEST_MODE, device, paymentAccount);
        request.setAmount(requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString());

        String requestJsonStr = JsonUtil.toJsonStr(request);
        LOGGER.info("[ICASH][TradeCharge]request => " + requestJsonStr);
        String result = this.sendPost(ICASH_TRANSACTION_URL, requestJsonStr);
        LOGGER.info("[ICASH][TradeCharge]response => " + result);

        IcashTradeChargeResponse response = new Gson().fromJson(result, IcashTradeChargeResponse.class);

        this.writeDBLog(requestHeader.getMerchantId(),
                requestHeader.getServiceType(), requestHeader.getMethod(), "RESERVE | " + paymentAccount.getAccount()
                        + "|" + device.getOwnerDeviceId() + "|" + response.getAmount(),
                response.getOrderId() + "|" + response.getTxnResult());

        if (StringUtils.isNotEmpty(response.getNewAesKey())) {
            device.setAesKey(response.getNewAesKey());
        }
        deviceService.save(device);
        this.saveReserveDetail(batchNum, requestData, requestHeader, orderId, response, device, userId, getDate(), 0, 0, getTime());

        if (SystemInstance.STATUS_DESCRIPTION_SUCCESS.equals(response.getTxnResult())) {
            //列印收據
            this.printReceipt(response, device, paymentAccount, requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString(), response.getChineseErrorMessage());
        }
        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    /**
     * 加值取消
     *
     * @param device
     * @param paymentAccount
     * @param requestHeader
     * @return
     */
    private String doTradeChargeCancel(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader, JsonObject requestData, Merchant merchant) throws Exception {

        LOGGER.info("[ICASH][TradeChargeCancel][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");

        long isRetry = requestData.get("Retry").getAsLong();
        if (isRetry >= 4) {
            LOGGER.error("[ICASH][TradeChargeCancel]. " + device.getOwnerDeviceId() + ", " + requestHeader.getMerchantId());
            throw new RuntimeException("Already retry 3 times.");
        }
        String userId = this.getUserId(merchant, requestData, device, paymentAccount);
        List<ReserveDetail> listTopOneByUserId = reserveDetailService.listTopOneByUserId(merchant.getAccountId(), device.getOwnerDeviceId(), userId);
        ReserveDetail reserveDetail = CollectionUtils.isNotEmpty(listTopOneByUserId) ? listTopOneByUserId.get(0) : null;
        if (Objects.isNull(reserveDetail)) {
            throw new OrderNotFoundException(SystemInstance.ORDER_NOT_FOUND);
        }

        String amount = String.valueOf(reserveDetail.getAmount());
        if ("0".equals(amount)) {
            LOGGER.error("[ICASH][TradeChargeCancel] Tx Amount is 0. ");
            throw new RuntimeException("order reserve Amount is 0.");
        }

        IcashTradeChargeCancelRequest request = new IcashTradeChargeCancelRequest(TEST_MODE, device, paymentAccount);
        request.setAmount(amount);
        String requestJsonStr = JsonUtil.toJsonStr(request);
        LOGGER.info("[ICASH][TradeChargeCancel]request => " + requestJsonStr);
        String result = this.sendPost(ICASH_TRANSACTION_URL, requestJsonStr);
        LOGGER.info("[ICASH][TradeChargeCancel]response => " + result);

        IcashTradeChargeCancelResponse response = new Gson().fromJson(result, IcashTradeChargeCancelResponse.class);

        String batchNum = this.getBatchNumber(paymentAccount);

        this.writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                "Cancel(NOT DO): " + merchant.getAccountId() + "|" + device.getOwnerDeviceId(), "N/A");

        if (StringUtils.isNotEmpty(response.getNewAesKey())) {
            device.setAesKey(response.getNewAesKey());
        }
        deviceService.save(device);

        if (SystemInstance.STATUS_DESCRIPTION_SUCCESS.equals(response.getTxnResult())) {
            ReserveCancel reserveCancel = new ReserveCancel();
            reserveCancel.setAccountId(requestHeader.getMerchantId());
            reserveCancel.setBatchNo(batchNum);
            reserveCancel.setReserveDetail(reserveDetail);
            reserveCancel.setCashier(requestHeader.getMerchantId());
            reserveCancel.setAmount(Long.valueOf(amount));
            reserveCancel.setDeviceId(device.getOwnerDeviceId());
            reserveCancel.setMethod(requestHeader.getMethod());
            reserveCancel.setUserId(userId);
            reserveCancel.setOrderId(reserveDetail.getReserveOrderId());
            reserveCancel.setTxParams(this.getTxDataLog(response));
            reserveCancel.setStatus("Success".equals(response.getTxnResult()) ? "0" : "1");
            reserveCancel.setEzcTxnDate(getDate() + getTime());
            reserveCancel.setDeviceRandomId(device);
            reserveCancel.setCreateTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
            reserveCancelService.save(reserveCancel);
            //列印收據
            this.printReceipt(response, device, paymentAccount, amount, response.getCardNumberForPrint());
        }

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    /**
     * 取得API 版本
     *
     * @param device
     * @param paymentAccount
     * @param requestHeader
     * @return
     */
    private String doGetApVersion(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader, JsonObject requestData) throws JsonProcessingException, InvokeAPIException {

        LOGGER.info("[ICASH][GetApVersion][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");
        IcashGetApVersionRequest request = new IcashGetApVersionRequest(TEST_MODE, device, paymentAccount);
        String requestJsonStr = JsonUtil.toJsonStr(request);

        LOGGER.info("[ICASH][GetApVersion]request => " + requestJsonStr);
        String result = this.sendPost(ICASH_TRANSACTION_URL, requestJsonStr);
        LOGGER.info("[ICASH][GetApVersion]response => " + result);

        IcashGetApVersionResponse response = JsonUtil.parseJson(result, IcashGetApVersionResponse.class);

        if (StringUtils.isNotEmpty(response.getNewAesKey())) {
            device.setAesKey(response.getNewAesKey());
        }
        deviceService.save(device);

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    /**
     * 驗證 交易 資料，並回傳 userId
     *
     * @param device
     * @param requestData
     * @param merchant
     * @throws AlreadyRetryException
     * @throws OrderAlreadyExistedException
     * @throws TicketReadCardException
     */
    private String verificationForTradeSale(Device device, JsonObject requestData, Merchant merchant, PaymentAccount paymentAccount) throws Exception {

        String orderId = requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString();
        long isRetry = requestData.get("Retry").getAsLong();
        TradeDetail tradeDetail;
        String userId;
        if (isRetry > 0 && isRetry < 4) {
            tradeDetail = tradeDetailService.getOne(orderId);
            userId = tradeDetail.getUserId();
        } else if (isRetry >= 4) {
            LOGGER.error("[REQUEST][IPASS] STOP RETRY. " + device.getOwnerDeviceId() + ", " + merchant.getAccountId());
            throw new AlreadyRetryException("Already retry 3 times.");
        } else {
            tradeDetail = tradeDetailService.getOne(orderId);
            if (Objects.nonNull(tradeDetail)) {
                throw new OrderAlreadyExistedException("Trade exist");
            }
            userId = this.getUserId(merchant, requestData, device, paymentAccount);
        }
        return userId;
    }

    private TradeDetail createOrder(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader, JsonObject requestData, String batchNo, String userId) throws OtherAPIException {
        String time = getTime();
        TradeDetail tradeDetail = new TradeDetail();
        tradeDetail.setStatus(SystemInstance.TRADE_CREATING_ORDER);
        tradeDetail.setOrderId(requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString());
        tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        tradeDetail.setAccountId(requestHeader.getMerchantId());
        tradeDetail.setPayment(Long.valueOf(requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString()));
        tradeDetail.setDeviceRandomId(device);
        tradeDetail.setPaymentAccount(paymentAccount);
        tradeDetail.setMethod("32800");
        tradeDetail.setSystemOrderId(time);
        tradeDetail.setBatchNo(batchNo);
        tradeDetail.setUserId(userId);
        tradeDetail.setEzcTxnDate(getDate() + time);
        tradeDetail.setComment(paymentAccount.getAccount() + device.getOwnerDeviceId());
        if (requestData.has("Body")) {
            tradeDetail.setDescription(requestData.get("Body").getAsString());
        }
        tradeDetail = tradeDetailService.save(tradeDetail);
        if (Objects.isNull(tradeDetail)) {
            throw new OtherAPIException(messageSource.getMessage("response.7142", null, locale));
        }
        return tradeDetail;
    }


    /**
     * 因悠遊卡 可以查所有卡號，也較快，固都改走悠遊卡查卡號
     *
     * @param merchant
     * @param requestData
     * @return
     * @throws Exception
     */
    private String getUserId(Merchant merchant, JsonObject requestData, Device device, PaymentAccount paymentAccount) throws Exception {
        LOGGER.info("[ICASH] getUserId : " + requestData.get("DeviceId").getAsString() + ", " + merchant.getAccountId());
        String userId = "-1", errMsg = "Can not read card info";

        int tryCount = 0;
        do {
            IcashQueryCardInfoRequest request = new IcashQueryCardInfoRequest(TEST_MODE, device, paymentAccount);
            String requestJsonStr = JsonUtil.toJsonStr(request);

            LOGGER.info("[ICASH][getUserId][" + (++tryCount) + "]request => " + requestJsonStr);
            String result = this.sendPost(ICASH_TRANSACTION_URL, requestJsonStr);
            LOGGER.info("[ICASH][getUserId][" + tryCount + "]response => " + result);
            IcashQueryCardInfoResponse response = JsonUtil.parseJson(result, IcashQueryCardInfoResponse.class);
            if (StringUtils.isNotEmpty(response.getNewAesKey())) {
                device.setAesKey(response.getNewAesKey());
            }
            String errorCode = response.getMcErrorCode();
            if ("Success".equals(response.getTxnResult())) {
                userId = response.getCardNumberForPrint();
            } else if ("9000000502".equals(errorCode) || "9000000503".equals(errorCode) || "9000000505".equals(errorCode)) {
                //      黑名單                             已鎖卡                             過期卡
                errMsg = errorCode + " - " + response.getChineseErrorMessage();
                break;
            }
            deviceService.save(device);
        } while (("-1".equals(userId) || StringUtils.isEmpty(userId)) && tryCount < 10);

        if (StringUtils.isEmpty(userId) || userId.equals("-1")) {
            throw new TicketReadCardException(errMsg);
        }

        return userId;
    }

    private String getDate() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    }

    private String getTime() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
    }

    public String getGeneralResponse(RequestHeader requestHeader, IcashBaseResponse response, String errorCode) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setStatusCode(errorCode);
        responseGeneralHeader.setStatusDesc(SystemInstance.STATUS_DESCRIPTION_SUCCESS);
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setMchId(requestHeader.getMerchantId());
        responseGeneralHeader.setMethod(requestHeader.getMethod());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        responseGeneralBody.setData(response);
        responseGeneralBody.setHeader(responseGeneralHeader);

        return new Gson().toJson(responseGeneralBody);
    }

    /**
     * 發送請求
     *
     * @param url
     * @param jsonStr
     * @return
     */
    private String sendPost(String url, String jsonStr) throws InvokeAPIException {
        String result = "";
        try {
            result = HttpUtil.doPostJson(url, jsonStr);
        } catch (Exception e) {
            LOGGER.error("[ICASH][EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            throw new InvokeAPIException(this.getClass().getSimpleName(), e.getMessage());
        }
        if (StringUtils.isEmpty(result)) {
            throw new InvokeAPIException(this.getClass().getSimpleName(), messageSource.getMessage("response.7350", null, locale));
        }
        return result;
    }

    @Async
    private void writeDBLog(String merchantId, String serviceType, String method, String instruction, String response) {

        OperationHistory operationHistory = new OperationHistory();
        operationHistory.setServiceType(serviceType);

        operationHistory.setMethod(method);
        operationHistory.setMerchantId(merchantId);
        operationHistory.setInstruction(instruction);
        operationHistory.setResponse(response);

        operationHistoryService.save(operationHistory);

    }

    private void saveReserveDetail(String batchNo, JsonObject requestData, RequestHeader requestHeader, String orderId,
                                   IcashTransactionResponse response, Device device, String userId, String date, long retry, int isAuto,
                                   String time) {
        ReserveDetail reserveDetail;
        if (retry > 0) {
            reserveDetail = reserveDetailService.getOne(requestHeader.getMerchantId(), orderId);
            LOGGER.info("[ICASH][RESERVE][RETRY] saveReserveDetail > " + response.getTxnResult());
            reserveDetail.setStatus(response.getTxnResult().equals("Success") ? "0" : "1");
            reserveDetail.setRrn(date + time);
            reserveDetail.setEzcTxnDate(date + time);
        } else {
            LOGGER.info("[ICASH][RESERVE] saveReserveDetail > " + response.getTxnResult());
            reserveDetail = new ReserveDetail();
            reserveDetail.setAccountId(requestHeader.getMerchantId());
            reserveDetail.setAmount(isAuto == 1 ? Long.valueOf(response.getAutoTopUpAmount())
                    : requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsLong());
            reserveDetail.setBatchNumber(batchNo);
            reserveDetail.setCashier(requestData.has("Cashier") ? requestData.get("Cashier").getAsString()
                    : SystemInstance.EMPTY_STRING);
            reserveDetail.setDeviceId(device.getOwnerDeviceId());
            reserveDetail.setReserveOrderId(orderId);
            reserveDetail.setStatus(SystemInstance.STATUS_DESCRIPTION_SUCCESS.equals(response.getTxnResult()) ? "0" : "1");
            reserveDetail.setMethod("32800");
            reserveDetail.setUserId(userId);
            reserveDetail.setTxParams(this.getTxDataLog(response));
            reserveDetail.setEzcTxnDate(date + time);
            reserveDetail.setIsAuto(isAuto);
            reserveDetail.setDeviceRandomId(device);
        }

        ReserveDetail entity = reserveDetailService.save(reserveDetail);
        if (entity == null) {
            LOGGER.error("[REQUEST][ICASH] write reserve detail failed. " + JsonUtil.toJsonStr(response));
        }
    }

    /**
     * 取得交易紀錄(退款看到的清單)
     *
     * @param requestHeader
     * @param requestData
     * @return
     * @throws JsonProcessingException
     * @throws InvokeAPIException
     */
    private String doOrderQuery(RequestHeader requestHeader, JsonObject requestData, Merchant merchant, Device device, PaymentAccount paymentAccount) throws Exception {

        String userId = "-1";
        if ("1.0".equals(device.getSwVersion()) || Objects.isNull(device.getSwVersion())) {
            userId = this.getUserId(merchant, requestData, device, paymentAccount);
        } else {
            userId = requestData.has("UserId") ? requestData.get("UserId").getAsString() : this.getUserId(merchant, requestData, device, paymentAccount);
        }

        LOGGER.info("[doOrderQuery][icash] list order by user cardId : " + userId);
        List<TradeDetailVo> tradeDetailVos = tradeDetailService.listByUserId(merchant.getAccountId(), userId);

        IcashOrderQueryResponse response = new IcashOrderQueryResponse();
        List<EzcTradeDetailVo> ezcTradeDetailVos = new ArrayList<EzcTradeDetailVo>();
        for (TradeDetailVo tx : tradeDetailVos) {

            boolean isRefund = !Strings.isNullOrEmpty(tx.getRefundStatus())
                    && tx.getRefundStatus().equals("Refund success");
            if (tx.getPayment() > 0 && !isRefund) {
                EzcTradeDetailVo ezcTx = new EzcTradeDetailVo();
                ezcTx.setCreateDate(tx.getCreateDate());
                ezcTx.setOrderId(tx.getOrderId());
                ezcTx.setPayment(tx.getPayment());

                ezcTradeDetailVos.add(ezcTx);
            }
        }

        response.setList(ezcTradeDetailVos);
        response.setErrorCode("00000");

        this.writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                "OrderQuery : " + userId + "|" + merchant.getAccountId(), "N/A");
        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    /**
     * 下載黑名單，排程 每天早上 2 點 ，在ScheduledTasks
     *
     * @return
     */
    @Override
    public String doBlcDownload() throws JsonProcessingException, InvokeAPIException {

        IcashNonTransactionRequest request = new IcashNonTransactionRequest(TEST_MODE);
        request.setServiceType("BlcDownload");
        request.setTaxIDNumber("5439037L");        //預設放我們公司向ICash註冊統編
        request.setFileSN("01");                   //預設
        //TODO ftp 黑名單只需要一家資料，直接寫死super之後再改
        IcashStoreMain icashStoreMain = new IcashStoreMain();
        icashStoreMain.setPassword("6nT6G4ayh9Hh");
        icashStoreMain.setUser("sftpitl");
        List<IcashStoreMain> lsit = new ArrayList<>();
        lsit.add(icashStoreMain);
        request.setData(JsonUtil.toJsonStr(lsit));

        String requestJsonStr = JsonUtil.toJsonStr(request).replaceAll("\\\\\"", "\"")
                .replaceAll("\"\\[", "[")
                .replaceAll("\\]\"", "]");

        LOGGER.info("[ICASH][doBlcDownload]request => " + requestJsonStr);
        String result = this.sendPost(ICASH_NON_TRANSACTION_URL, requestJsonStr);
        LOGGER.info("[ICASH][doBlcDownload]response => " + result);
//        IcashBaseResponse response = JsonUtil.parseJson(result, IcashBaseResponse.class);

        return result;
    }

    /**
     * 上傳門市主檔，排程 每天早上 3 點 ，在ScheduledTasks
     *
     * @return
     */
    @Override
    public String doStoreMain() throws JsonProcessingException, InvokeAPIException {

        IcashNonTransactionRequest request = new IcashNonTransactionRequest(TEST_MODE);
        request.setServiceType("StoreMain");
        request.setTaxIDNumber("5439037L");        //預設放我們公司向ICash註冊統編
        request.setFileSN("01");                   //預設
        List<PaymentAccount> paymentAccountList = paymentAccountService.listByBankId(28);
        List<IcashStoreMain> storeMainList = this.transformStoreMain(paymentAccountList);

        //以下替代，只是在python那邊是直接接收JSON陣列，但是我是用字串，所以做些替換將字串轉回標準的JSON陣列
        request.setData(JsonUtil.toJsonStr(storeMainList).replaceAll("\\\\\\\\u", "\\\\u"));

        String requestJsonStr = JsonUtil.toJsonStr(request).replaceAll("\\\\u", "\\u").
                replaceAll("\\\\\"", "\"")
                .replaceAll("\"\\[", "[")
                .replaceAll("\\]\"", "]");

        LOGGER.info("[ICASH][doStoreMain]request => " + requestJsonStr);
        String result = this.sendPost(ICASH_NON_TRANSACTION_URL, requestJsonStr);
        LOGGER.info("[ICASH][doStoreMain]response => " + result);
//        IcashBaseResponse response = JsonUtil.parseJson(result, IcashBaseResponse.class);

        return result;
    }

    @Override
    public void settlementMain(String createDate, String batchEndStr) {
        this.settlementMain(createDate, "", batchEndStr);
    }

    @Override
    public void settlementMain(String createDate, String endDate, final String batchEndStr) {
        /**
         *
         *   Icash 因為我們的 特店，在ICASH那邊都算在 "英特拉底下 "的分店
         *   所以每次結帳都是全部結帳
         */
        String createDateStart = "", createDateEnd = "";
        try {
            createDateStart = LocalDate.parse(createDate, DateTimeFormatter.ofPattern("yyyyMMdd")).format(DateTimeFormatter.ofPattern("yyyyMMdd000000"));
            createDateEnd = LocalDate.parse(StringUtils.defaultIfEmpty(endDate, createDate), DateTimeFormatter.ofPattern("yyyyMMdd")).format(DateTimeFormatter.ofPattern("yyyyMMdd235959"));
        } catch (Exception e) {
            LOGGER.debug("[settlementMain][ERROR] createDate => " + createDate);
        }

        List<CheckoutCtrlVo> checkoutCtrlVos = checkoutRepository.getCheckoutList("", "", "", createDateStart, createDateEnd, "28");

//        final String yesterDayDateStr = LocalDate.now().minusDays(1).format(DateTimeFormatter.ofPattern("yyyyMMdd"));

        checkoutCtrlVos = checkoutCtrlVos.parallelStream().filter(checkoutCtrlVo -> checkoutCtrlVo.getBatchNo().endsWith(batchEndStr)).collect(Collectors.toList());
        List<Map<String, Object>> resultMapList = this.transformCheckoutCtrlVoList(checkoutCtrlVos);
        try {
//            if (key || CollectionUtils.isNotEmpty(resultMapList)) {
            this.doSettlement(resultMapList, true, batchEndStr);
//            }
        } catch (Exception e) {
            resultMapList.parallelStream().forEach(stringObjectMap -> this.updateCheckoutCtrl("ERR", resultMapList));
            LOGGER.error("[settlementMain][EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
        }
        //結帳fileSN 只有 01 或 02 ，固這邊只分昨天的結帳批次 01 、非昨天結帳批次02
//        final Map<Boolean, List<CheckoutCtrlVo>> settlementMap = checkoutCtrlVos.parallelStream()
//                .filter(checkoutCtrlVo -> checkoutCtrlVo.getBatchNo().endsWith(batchEndStr))
//                .collect(Collectors.partitioningBy(CheckoutCtrlVo -> yesterDayDateStr.equals(CheckoutCtrlVo.getBatchNo().substring(0, 8))));
//
//        // 因python 前人不知道怎麼寫，一定01要先給！
//        for (int i = 0; i <= 1; i++) {
//            boolean key = (i == 0);
//            List<CheckoutCtrlVo> checkoutCtrlVoList = settlementMap.get(key);
//            List<Map<String, Object>> resultMapList = this.transformCheckoutCtrlVoList(checkoutCtrlVoList);
//            try {
//                if (key || CollectionUtils.isNotEmpty(resultMapList)) {
//                    this.doSettlement(resultMapList, key, batchEndStr);
//                }
//            } catch (Exception e) {
//                resultMapList.parallelStream().forEach(stringObjectMap -> this.updateCheckoutCtrl("ERR", resultMapList));
//                LOGGER.error("[settlementMain][EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
//            }
//        }

//        settlementMap.forEach((key, checkoutCtrlVoList) -> {
////           2019/10/29  edit:sol  不管有無交易都要打結帳
//            List<Map<String, Object>> resultMapList = this.transformCheckoutCtrlVoList(checkoutCtrlVoList);
//            try {
//                if (key || CollectionUtils.isNotEmpty(resultMapList)) {
//                    this.doSettlement(resultMapList, key, batchEndStr);
//                }
//            } catch (Exception e) {
//                resultMapList.parallelStream().forEach(stringObjectMap -> this.updateCheckoutCtrl("ERR", resultMapList));
//                LOGGER.error("[settlementMain][EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
//            }
//        });
    }

    /**
     * 結帳 包含 自動上傳帳務檔
     *
     * @param resultMapList
     * @param yesterday
     * @return
     * @throws JsonProcessingException
     * @throws InvokeAPIException
     */
    private String doSettlement(List<Map<String, Object>> resultMapList, boolean yesterday, final String batchEndStr) throws JsonProcessingException, InvokeAPIException {

        LOGGER.info("[ICASH][doSettlement] start ");
        IcashNonTransactionRequest request = new IcashNonTransactionRequest(TEST_MODE);
        if ("01".equals(batchEndStr)) { //新款結帳只有差異這兩個參數
            request.setTxLogType("1");
            request.setTaxIDNumber("43427602"); //都由我們公司代收
        }
        request.setServiceType("Settlement");
        request.setFileSN(yesterday ? "01" : "02");     //每天只會有兩個FN 01 或02，01都放昨天交易，其他都是02

        List<IcashSettlementData> dataList = this.getSettlementDataList(resultMapList, batchEndStr);
        request.setData(JsonUtil.toJsonStr(dataList).replaceAll("\\\\\"", "\""));

        String requestJsonStr = JsonUtil.toJsonStr(request)
                .replaceAll("\\\\\"", "\"")
                .replaceAll("\"\\[", "[")
                .replaceAll("\\]\"", "]");

        LOGGER.info("[ICASH][doSettlement]request => " + requestJsonStr);
        String result = this.sendPost(ICASH_NON_TRANSACTION_URL, requestJsonStr);
        LOGGER.info("[ICASH][doSettlement]response => " + result);

        IcashBaseResponse response = JsonUtil.parseJson(result, IcashBaseResponse.class);

        String checkoutStatus = SystemInstance.STATUS_DESCRIPTION_SUCCESS.equals(response.getTxnResult()) ? "COMP" : "FAIL";
        this.updateCheckoutCtrl(checkoutStatus, resultMapList);

        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setMerchantId("AutoSettlement");
        requestHeader.setMethod("32800");
        requestHeader.setServiceType("Settlement");

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }


    private List<IcashStoreMain> transformStoreMain(List<PaymentAccount> paymentAccountList) {
        // 舊款結帳的三家才要拋主檔
        return paymentAccountList.parallelStream().filter(paymentAccount -> oldPlatformCheckCode.contains(paymentAccount.getPlatformCheckCode()))
                .filter(this.distinctByKey(PaymentAccount::getAccount)).map(paymentAccoun -> {
                    IcashStoreMain storeMain = new IcashStoreMain();
                    Merchant merchant = paymentAccoun.getMerchant();

                    storeMain.setAddress(merchant.getAddress());
                    String storeFullName = merchant.getCompanyName();
                    storeMain.setStoreFullName(storeFullName.substring(0, Integer.min(15, storeFullName.length())));     // 中文最多五碼
                    String storeAbbreviation = merchant.getBrandName();
                    storeMain.setStoreAbbreviation(storeAbbreviation.substring(0, Integer.min(5, storeAbbreviation.length())));     // 中文最多五碼
                    storeMain.setPostalCode(merchant.getPostalCode());
                    storeMain.setTaxIDNumber(paymentAccoun.getSpID());
                    storeMain.setStoreMatchCode(paymentAccoun.getAppId());
                    String phoneNum = merchant.getPhone().replaceAll("\\s", "");
                    if (phoneNum.startsWith("09")) {
                        storeMain.setPhoneNum(merchant.getPhone());
                    } else {
                        String[] phone = phoneNum.replaceAll("\\)", "-").split("-");
                        storeMain.setPhoneAreaNum(phone[0].replaceAll("\\(", ""));
                        storeMain.setPhoneNum(phone[1]);
                    }

                    storeMain.setStartDate(merchant.getStartDate());
                    storeMain.setEndDate(merchant.getEndDate());
                    storeMain.setSalesDate(merchant.getSalesDate());
                    storeMain.setCloseDate(merchant.getCloseDate());
                    storeMain.setOpenDate(merchant.getOpenDate());

                    storeMain.setUser(paymentAccoun.getSftpUser());
                    storeMain.setPassword(paymentAccoun.getSftpPasswd());

                    return storeMain;
                }).collect(Collectors.toList());
    }


    private String getTxDataLog(IcashTransactionResponse response) {
        String txDataLog = null;
        List<IcashTransactionResponse.ICTX2Data> ictx2DataList = response.getiCTX2Data();
        if (CollectionUtils.isNotEmpty(ictx2DataList)) {
            IcashTransactionResponse.ICTX2Data ictx2Data = ictx2DataList.get(0);
            if (Objects.nonNull(ictx2Data)) {
                List<JsonObject> txDataJson = ictx2Data.getTxData();
                txDataLog = new Gson().toJson(txDataJson).replaceAll("[\\[\\]]", "");
            }
        }
        return txDataLog;
    }

    /**
     * 列印發票，因不改POS機，直接抄EZC來用
     *
     * @param response
     * @param device
     * @param paymentAccount
     * @param amount
     * @param cardId
     * @throws Exception
     */
    private void printReceipt(IcashBaseResponse response, Device device, PaymentAccount paymentAccount, String amount, String cardId) {
        LOGGER.info("[ICASH][Receipt][Print][input] " + new Gson().toJson(response));
        EzCardReceipt ezCardReceipt = new Gson().fromJson(new Gson().toJson(response), EzCardReceipt.class);
        ezCardReceipt.setDeviceId(device.getOwnerDeviceId());
        ezCardReceipt.setTerminalId(paymentAccount.getAccount());
        ezCardReceipt.setPrintId("01");
        ezCardReceipt.setDate(response.getDate());
        ezCardReceipt.setTime(response.getTime());
        ezCardReceipt.setEzCardId(cardId);
        ezCardReceipt.setCpuPurseVersion("0");
        ezCardReceipt.setAmount(amount);
        ezCardReceipt.setBatchNumber(this.getBatchNumber(paymentAccount));

        LOGGER.info("[ICASH][Receipt][Print][REQ] " + JsonUtil.toJsonStr(ezCardReceipt));

        if (response.getAutoTopUpAmount() != null && !response.getAutoTopUpAmount().equals("0")) {
            ezCardReceipt.setAutoTopUpAmount(response.getAutoTopUpAmount());
        }

        String printResponse = null;
        try {
            String requestUrl = EZC_HOST + URLEncoder.encode(JsonUtil.toJsonStr(ezCardReceipt), "UTF-8");
            printResponse = HttpRequestUtil.httpsGet(requestUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
        LOGGER.info("[ICASH][Receipt][Print][response] " + printResponse);
    }


    private void saveOrUpdateTransactionDetails(TradeDetail tradeDetail, Merchant merchant,
                                                IcashTransactionResponse response, Device device, String batchNo, long amount, long isRetry, String txnTime,
                                                boolean isCancel) {

        RefundDetail refundDetail = new RefundDetail();
        refundDetail.setMethod(tradeDetail.getMethod());
        refundDetail.setOrderId(tradeDetail.getOrderId());
        refundDetail.setAmount(amount);
        refundDetail.setTradeDetailRandomId(tradeDetailService.getOne(tradeDetail.getOrderId()));
        refundDetail.setCreateTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        refundDetail.setTxParams(this.getTxDataLog(response));
        refundDetail.setStoreRefundId(tradeDetail.getOrderId());
        refundDetail.setSystemRefundId(txnTime);
        LOGGER.info(
                "[ICASH] EZC_REFUND success and update refund information." + new Gson().toJson(response));
        tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());
        if (response.getTxnResult().equals("Success")) {
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);

            refundDetail.setStatus(SystemInstance.TRADE_REFUND_SUCCESS);
            refundDetail.setPartialRefund(false);
            refundDetail.setBatchNo(batchNo);
            refundDetail.setDeviceRandomId(device);
            refundDetail.setAccountId(merchant.getAccountId());
            refundDetail.setReason(isRetry > 0 ? "Retry" : "");
            refundDetail.setEzcTxnDate(response.getDate() + response.getTime());
            refundDetail.setEzcCancel(isCancel);

        } else {
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);

            refundDetail.setStatus(SystemInstance.TRADE_REFUND_FAIL);
            refundDetail.setPartialRefund(false);
            refundDetail.setDeviceRandomId(device);
            refundDetail.setAccountId(merchant.getAccountId());
            refundDetail.setReason(isRetry > 0 ? "Retry" : response.getErrorCode());
            refundDetail.setEzcTxnDate(response.getDate() + response.getTime());
        }
        tradeDetailService.save(tradeDetail);
        refundDetailService.save(refundDetail);
    }


    private INT_TB_CheckoutCtrl getCheckoutCtrl(CheckoutCtrlVo checkoutCtrlVo) {
        INT_TB_CheckoutCtrl checkoutCtrl = new INT_TB_CheckoutCtrl();
        checkoutCtrl.setCcSeqId(checkoutCtrlVo.getCcSeqId().intValue());
        checkoutCtrl.setAccountId(checkoutCtrlVo.getAccountId());
        checkoutCtrl.setMethod("32800");
        checkoutCtrl.setBatchNo(checkoutCtrlVo.getBatchNo());
        checkoutCtrl.setType(checkoutCtrlVo.getType());
        checkoutCtrl.setComment(checkoutCtrlVo.getComment());
        checkoutCtrl.setOwnerDeviceId(checkoutCtrlVo.getOwnerDeviceId());
        checkoutCtrl.setStatus(checkoutCtrlVo.getStatus());
        checkoutCtrl.setCrDate(checkoutCtrlVo.getCrDate());
        checkoutCtrl.setDateStamp(new Date());

        return checkoutCtrl;
    }

    /**
     * 新增ctrl log 檔 (status 為 I or R)
     *
     * @param checkoutCtrlVo
     */
    private INT_TB_CheckoutLog getCheckoutLog(CheckoutCtrlVo checkoutCtrlVo) {
        INT_TB_CheckoutLog checkoutLog = new INT_TB_CheckoutLog();
        checkoutLog.setCcSeqId(checkoutCtrlVo.getCcSeqId().intValue());
        checkoutLog.setAccountId(checkoutCtrlVo.getAccountId());
        checkoutLog.setBatchNo(checkoutCtrlVo.getBatchNo());
        checkoutLog.setComment(checkoutCtrlVo.getComment());
        checkoutLog.setOwnerDeviceId(checkoutCtrlVo.getOwnerDeviceId());
        checkoutLog.setStatus(checkoutCtrlVo.getStatus());
        checkoutLog.setCrDate(new Date());
        checkoutLog.setDateStamp(new Date());
        return checkoutLog;
    }

    private Map<String, Object> getDetails(String deviceId, String account, String batchNo) {

        List<TradeDetailVo> tradeDetailList = tradeDetailService.listByBatchNo(account,
                deviceId, batchNo);
        List<ReserveDetail> reserveDetailList = reserveDetailService.listByBatchNo(account,
                deviceId, batchNo);
        List<RefundDetailVo> refundDetailList = refundDetailService.listByBatchNo(account,
                deviceId, batchNo);
        List<ReserveCancel> reserveCancelList = reserveCancelService.listByMerchantIdAndDeviceIdAndBatchNo(
                account, deviceId, batchNo);
        final String method = "32800";
        Map<String, Object> map = new HashMap<>();
        map.put("tradeDetailList", tradeDetailList.parallelStream().filter(tradeDetailVo -> method.equals(tradeDetailVo.getMethod())).collect(Collectors.toList()));
        map.put("reserveDetailList", reserveDetailList.parallelStream().filter(reserveDetail -> method.equals(reserveDetail.getMethod())).collect(Collectors.toList()));
        map.put("refundDetailList", refundDetailList.parallelStream().filter(refundDetailVo -> method.equals(refundDetailVo.getMethod())).collect(Collectors.toList()));
        map.put("reserveCancelList", reserveCancelList.parallelStream().filter(reserveCancel -> method.equals(reserveCancel.getMethod())).collect(Collectors.toList()));

        return map;
    }

    private String getSettlementData(Map<String, Object> reqMap) {

        List<TradeDetailVo> tradeDetailList = (List<TradeDetailVo>) reqMap.get("tradeDetailList");
        List<ReserveDetail> reserveDetailList = (List<ReserveDetail>) reqMap.get("reserveDetailList");
        List<RefundDetailVo> refundDetailList = (List<RefundDetailVo>) reqMap.get("refundDetailList");
        List<ReserveCancel> reserveCancelList = (List<ReserveCancel>) reqMap.get("reserveCancelList");

        //合併並將紀錄的LOG資料取出
        final List<String> resList = Stream.of(
                tradeDetailList.parallelStream().sorted(Comparator.comparing(tradeDetailVo -> Long.valueOf(tradeDetailVo.getCreateDate()))).map(TradeDetailVo::getTxParams).filter(StringUtils::isNoneBlank).collect(Collectors.toList())
                , reserveDetailList.parallelStream().sorted(Comparator.comparing(reserveDetail -> Long.valueOf(reserveDetail.getCreateTime()))).map(ReserveDetail::getTxParams).filter(StringUtils::isNoneBlank).collect(Collectors.toList())
                , refundDetailList.parallelStream().sorted(Comparator.comparing(refundDetail -> Long.valueOf(refundDetail.getCreateTime()))).map(RefundDetailVo::getTxParams).filter(StringUtils::isNoneBlank).collect(Collectors.toList())
                , reserveCancelList.parallelStream().sorted(Comparator.comparing(reserveCancel -> Long.valueOf(reserveCancel.getCreateTime()))).map(ReserveCancel::getTxParams).filter(StringUtils::isNoneBlank).collect(Collectors.toList()))
                .flatMap(Collection::stream)
                .distinct().collect(Collectors.toList());

        //替換LOG計數的數字以及join字串
        return JsonUtil.toJsonStr(resList);
    }

    private String getBatchNumber(PaymentAccount paymentAccount) {
        // 只有正好停、歐特儀、英特拉走舊款結帳，結帳會用到BatchNumber結尾做區隔
        return getDate() + (oldPlatformCheckCode.contains(paymentAccount.getPlatformCheckCode()) ? "00" : "01");
    }

    private String doDeviceQuery(Merchant merchant, RequestHeader requestHeader) {
        LOGGER.info("[REQUEST][Icash] list device by merchantId : " + merchant.getAccountId());
        List<DeviceVo> deviceList = deviceService.listAllByMerchantAndType(merchant.getAccountId(), "ezc");
        List<DeviceVo> trafficDeviceList = deviceService.listAllByMerchantAndType(merchant.getAccountId(), "traffic");
        deviceList = CollectionUtils.isEmpty(deviceList) ? new ArrayList<>() : deviceList;
        if (CollectionUtils.isNotEmpty(trafficDeviceList)) {
            deviceList.addAll(trafficDeviceList);
        }

        IcashDeviceQueryResponse response = new IcashDeviceQueryResponse();
        response.setDeviceList(deviceList);

        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                "DeviceQuery:" + merchant.getAccountId(), response.getOrderId() + "|" + response.getTxnResult());

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }


    /**
     * 查卡號
     * 查卡號，因悠遊卡都可以查所以直接走悠遊卡查卡ID
     *
     * @param device
     * @param paymentAccount
     * @param requestHeader
     * @param requestData
     * @return
     * @throws Exception
     */
    private String doIdQuery(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader, JsonObject requestData) throws JsonProcessingException, InvokeAPIException {

        String result = this.doQueryCardInfo(device, paymentAccount, requestHeader, requestData);
        JsonNode jsonObject = JsonUtil.parseJson(result, JsonNode.class);
        IcashQueryCardInfoResponse icashQueryCardInfoResponse = JsonUtil.parseJson(jsonObject.get("Data").toString(), IcashQueryCardInfoResponse.class);

        writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(), requestHeader.getMethod(),
                JsonUtil.toJsonStr(icashQueryCardInfoResponse), icashQueryCardInfoResponse.getEzCardID() + "|" + icashQueryCardInfoResponse.getTxnResult());

        return getGeneralResponse(requestHeader, icashQueryCardInfoResponse, SystemInstance.STATUS_SUCCESS);

    }

    private List<IcashSettlementData> getSettlementDataList(List<Map<String, Object>> resultMapList, final String batchEndStr) {
        final boolean batchEndStrIs01 = "01".equals(batchEndStr);
        List<Map<String, Object>> mapList = paymentAccountService.listByBankId(28).parallelStream()
                .filter(paymentAccount -> "active".equals(paymentAccount.getMerchant().getStatus()))
                .filter(paymentAccount -> batchEndStrIs01 != oldPlatformCheckCode.contains(paymentAccount.getPlatformCheckCode()))
                .map(paymentAccount -> {
                            List<Device> deviceList = deviceService.listAllByMerchant(paymentAccount.getMerchant()).parallelStream()
                                    .filter(device -> "0".equals(device.getBan()) && "taxi|ezc|traffic".contains(device.getType())).collect(Collectors.toList());
                            Map<String, Object> map = new HashMap<>();
                            map.put("paymentAccount", paymentAccount);
                            map.put("deviceList", deviceList);
                            return map;
                        }
                ).collect(Collectors.toList());

        return mapList.parallelStream().filter(stringObjectMap -> Objects.nonNull(stringObjectMap.get("paymentAccount")) && Objects.nonNull(stringObjectMap.get("deviceList")))
                .map(stringObjectMap -> {
                    final PaymentAccount iCashPaymentAccount = (PaymentAccount) stringObjectMap.get("paymentAccount");
                    List<Device> deviceList = (List<Device>) stringObjectMap.get("deviceList");
                    return deviceList.parallelStream().map(device -> {
                        IcashSettlementData icashSettlementData = new IcashSettlementData(device, iCashPaymentAccount);
                        if (batchEndStrIs01) {
                            icashSettlementData.setMarketNumber(StringUtils.defaultIfBlank(iCashPaymentAccount.getAccountTerminalId(), ""));
                        }
                        String txData = resultMapList.parallelStream().filter(resultMap -> {
                            Device device2 = (Device) resultMap.get("device");
                            CheckoutCtrlVo checkoutCtrlVo = (CheckoutCtrlVo) resultMap.get("checkoutCtrlVo");
                            return device2.getOwnerDeviceId().equals(device.getOwnerDeviceId()) && iCashPaymentAccount.getMerchant().getAccountId().equals(checkoutCtrlVo.getAccountId());
                        }).map(this::getSettlementData).collect(Collectors.joining("||"));

//                        if (StringUtils.isNotEmpty(txData)) {
                        String[] txDatas = txData.split("\\|\\|");
                        List<JsonNode> jsonNodeList = null;
                        for (String str : txDatas) {
                            if (StringUtils.isNotBlank(str)) {
                                if (Objects.isNull(jsonNodeList)) {
                                    jsonNodeList = JsonUtil.parseJsonList(str, JsonNode.class);
                                } else {
                                    List<JsonNode> jsonNodeList2 = JsonUtil.parseJsonList(str, JsonNode.class);
                                    jsonNodeList.addAll(jsonNodeList2);
                                }
                            }
                        }
                        if (CollectionUtils.isNotEmpty(jsonNodeList)) {
                            List<JsonNode> jsonNodeList2 = jsonNodeList;
                            txData = IntStream.range(0, jsonNodeList2.size()).mapToObj(i -> JsonUtil.toJsonStr(jsonNodeList2.get(i)).replaceAll("\\\\\"", "\"")
                                    .replaceAll("(\"Log)\\d{2}(\")", "$1" + String.format("%02d", (i + 1)) + "$2")
                                    .replaceAll("\"\\{", "{")
                                    .replaceAll("}\"", "}"))
                                    .collect(Collectors.joining(",", "[", "]"));
                            icashSettlementData.setTxData(txData);
                        }
//                        }

                        return icashSettlementData;
                    }).collect(Collectors.toList());
                }).flatMap(Collection::stream)
                .distinct().collect(Collectors.toList());
    }

    private void updateCheckoutCtrl(final String statusStr, List<Map<String, Object>> resultMapList) {

        resultMapList.parallelStream().forEach(map -> {
            INT_TB_CheckoutCtrl checkoutCtrl = (INT_TB_CheckoutCtrl) map.get("checkoutCtrl");
            INT_TB_CheckoutLog checkoutLog = (INT_TB_CheckoutLog) map.get("checkoutLog");
            checkoutCtrl.setStatus(statusStr);
            checkoutCtrl.setDateStamp(new Date());
            checkoutLog.setStatus(statusStr);
            checkoutLog.setCrDate(new Date());
            checkoutLog.setDateStamp(new Date());
            INT_TB_CheckoutCtrlService.save(checkoutCtrl);
            INT_TB_CheckoutLogService.save(checkoutLog);
        });
    }

    /**
     * 轉換資料，並同時建立INT_TB_CheckoutCtrl  、 INT_TB_CheckoutLog
     *
     * @param CheckoutCtrlVoList
     * @return
     */
    private List<Map<String, Object>> transformCheckoutCtrlVoList(List<CheckoutCtrlVo> CheckoutCtrlVoList) {
        return CollectionUtils.isEmpty(CheckoutCtrlVoList) ? new ArrayList<>() : CheckoutCtrlVoList.parallelStream().map(checkoutCtrlVo -> {
            //初始化 INT_TB_CheckoutCtrl 、INT_TB_CheckoutLog
            INT_TB_CheckoutCtrl checkoutCtrl = this.getCheckoutCtrl(checkoutCtrlVo);
            INT_TB_CheckoutCtrlService.save(checkoutCtrl);
            checkoutCtrlVo.setCcSeqId(BigInteger.valueOf(checkoutCtrl.getCcSeqId()));
            INT_TB_CheckoutLog checkoutLog = this.getCheckoutLog(checkoutCtrlVo);
            INT_TB_CheckoutLogService.save(checkoutLog);

            Map<String, Object> orderMap = this.getDetails(checkoutCtrlVo.getOwnerDeviceId(), checkoutCtrlVo.getAccountId(), checkoutCtrlVo.getBatchNo());

            List<TradeDetailVo> tdList = (List<TradeDetailVo>) orderMap.get("tradeDetailList");
            List<ReserveDetail> reDList = (List<ReserveDetail>) orderMap.get("reserveDetailList");
            List<RefundDetailVo> refundDList = (List<RefundDetailVo>) orderMap.get("refundDetailList");
            List<ReserveCancel> reserveCancels = (List<ReserveCancel>) orderMap.get("reserveCancelList");

            Device device = null;
            if (tdList.size() > 0) {
                device = deviceService.getOneByTDOrderId(tdList.get(0).getOrderId());
            } else if (reDList.size() > 0) {
                device = deviceService.getOneByRDOrderId(reDList.get(0).getReserveOrderId());
            } else if (refundDList.size() > 0) {
                device = deviceService.getOneByRefundOrderId(refundDList.get(0).getOrderId());
            }

            checkoutCtrl.setStatus("P");
            checkoutCtrl.setDateStamp(new Date());
            checkoutCtrl.setTradeAmount(tdList.parallelStream().mapToLong(TradeDetailVo::getPayment).sum());
            checkoutCtrl.setRefundAmount(refundDList.parallelStream().mapToLong(RefundDetailVo::getAmount).sum());
            checkoutCtrl.setReserveAmount(reDList.parallelStream().mapToLong(ReserveDetail::getAmount).sum());
            checkoutCtrl.setReserveCancelAmount(reserveCancels.parallelStream().mapToLong(ReserveCancel::getAmount).sum());
            INT_TB_CheckoutCtrlService.save(checkoutCtrl);
            checkoutLog.setStatus("P");
            checkoutLog.setCrDate(new Date());
            checkoutLog.setDateStamp(new Date());
            INT_TB_CheckoutLogService.save(checkoutLog);

            orderMap.put("checkoutCtrlVo", checkoutCtrlVo);
            orderMap.put("device", device);
            orderMap.put("checkoutCtrl", checkoutCtrl);
            orderMap.put("checkoutLog", checkoutLog);

            return orderMap;
        }).collect(Collectors.toList());
    }

    @Override
    public String updateDoTradeSale(RequestHeader requestHeader, JsonObject requestData, Device device, Merchant merchant, IcashTradeSaleRequest icashTradeSaleRequest, IcashTradeSaleResponse response, TradeDetail tradeDetail, PaymentAccount paymentAccount) {
        response.setOrderId(tradeDetail.getOrderId());
        response.setCardDesc("iCash");
        response.setEzCardId(tradeDetail.getUserId());

        if (StringUtils.isNotEmpty(response.getNewAesKey())) {
            device.setAesKey(response.getNewAesKey());
        }
        deviceService.save(device);

//        tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());
        tradeDetail.setBatchNo(this.getBatchNumber(paymentAccount));
        tradeDetail.setTxParams(this.getTxDataLog(response));

        this.writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(),
                requestHeader.getMethod(), "Payment | " + merchant.getMerchantSeqId() + " | "
                        + device.getOwnerDeviceId() + " | " + tradeDetail.getPayment() + " | " + tradeDetail.getUserId(),
                response.getOrderId() + "|" + response.getTxnResult());

        if (StringUtils.isNotBlank(response.getAutoTopUpAmount()) && !"0".equals(response.getAutoTopUpAmount())) {
            this.saveReserveDetail(tradeDetail.getBatchNo(), requestData, requestHeader, tradeDetail.getOrderId(), response, device, tradeDetail.getUserId(), icashTradeSaleRequest.getDate(), 0, 1, icashTradeSaleRequest.getTime());
        }

        if ("Success".equals(response.getTxnResult())) {
            LOGGER.info("[RESPONSE][ICASH][TradeSale][Success] " + new Gson().toJson(response));
            tradeDetail.setMethod("32800");
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetail.setEzcTxnDate(icashTradeSaleRequest.getDate() + icashTradeSaleRequest.getTime());

            tradeDetailService.save(tradeDetail);
            appNotificationService.notifyApp(tradeDetail, merchant);
            //列印收據
            this.printReceipt(response, device, paymentAccount, response.getAmount(), response.getCardNumberForPrint());
            this.saveTradeDetailOtherInfo(tradeDetail, response);
        } else {
            LOGGER.error("[RESPONSE][ICASH][TradeSale][Fail] " + new Gson().toJson(response));
            response.setTxnResult(StringUtils.defaultIfEmpty(response.getTxnResult(), SystemInstance.STATUS_DESCRIPTION_FAIL2));
            response.setErrorCode(StringUtils.defaultIfEmpty(response.getErrorCode(), "900000"));
            if (!lookupCodeService.isTsmcAccount(requestHeader.getMerchantId()) && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            } else if ("14900".equals(tradeDetail.getMethod())) {
                if (SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    requestHeader.setMethod("14900");
                    response.setTxnResult(SystemInstance.STATUS_DESCRIPTION_SUCCESS);
                    response.setErrorCode("000000");
                } else {
                    tradeDetail.setMethod("32800");
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }
            tradeDetailService.save(tradeDetail);
        }

        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }

    @Override
    public String updateDoRefund(RequestHeader requestHeader, JsonObject requestData, Device device, Merchant merchant, IcashTradeRefundResponse response, TradeDetail tradeDetail, PaymentAccount paymentAccount) {

        response.setEzCardId(response.getCardNumberForPrint());

        this.writeDBLog(requestHeader.getMerchantId(), requestHeader.getServiceType(),
                requestHeader.getMethod(), "REFUND(LIMIT) | " + paymentAccount.getAccount() + "|"
                        + device.getOwnerDeviceId() + "|" + response.getAmount(),
                response.getOrderId() + "|" + response.getTxnResult());
        this.saveOrUpdateTransactionDetails(tradeDetail, merchant, response, device, this.getBatchNumber(paymentAccount),
                tradeDetail.getPayment(), requestData.get("Retry").getAsLong(), getTime(), false);

        if (StringUtils.isNotEmpty(response.getNewAesKey())) {
            device.setAesKey(response.getNewAesKey());
        }
        deviceService.save(device);
        if (SystemInstance.STATUS_DESCRIPTION_SUCCESS.equals(response.getTxnResult())) {
            //列印收據
            this.printReceipt(response, device, paymentAccount, response.getAmount(), response.getCardNumberForPrint());
        }
        return getGeneralResponse(requestHeader, response, SystemInstance.STATUS_SUCCESS);
    }


    /**
     * stream 去重複用
     *
     * @param keyExtractor
     * @param <T>
     * @return
     */
    private <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return object -> seen.putIfAbsent(keyExtractor.apply(object), Boolean.TRUE) == null;
    }

    private void saveTradeDetailOtherInfo(TradeDetail tradeDetail, IcashTradeSaleResponse response) {
        TradeDetailOtherInfo tradeDetailOtherInfo = new TradeDetailOtherInfo();
        tradeDetailOtherInfo.setTradeDetailRandomId(tradeDetail.getTradeDetailRandomId());
        tradeDetailOtherInfo.setExeTime(1L);
        tradeDetailOtherInfo.setCardNumberForPrint(StringUtils.defaultString(response.getCardNumberForPrint(), response.getEzCardId()));
        tradeDetailOtherInfo.setCardId(response.getEzCardId());
        tradeDetailOtherInfoService.save(tradeDetailOtherInfo);
    }
}
