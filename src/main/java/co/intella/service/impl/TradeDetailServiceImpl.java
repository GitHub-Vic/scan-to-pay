package co.intella.service.impl;

import co.intella.domain.TSMC.QueryForCardIdRequest;
import co.intella.domain.contratStore.CustomizePageRequest;
import co.intella.domain.contratStore.TradeDetailVo;
import co.intella.domain.integration.IntegratedOrderQueryRequestData;
import co.intella.model.DiscountTradeOLPay;
import co.intella.model.QrcodeParameter;
import co.intella.model.TradeDetail;
import co.intella.model.TradeDetailOtherInfo;
import co.intella.net.HttpRequestUtil;
import co.intella.repository.QrType7Repository;
import co.intella.repository.QrcodeParameterRepository;
import co.intella.repository.impl.QrType7RepositoryImpl;
import co.intella.service.DiscountTradeOLPayService;
import co.intella.service.QrcodeService;
import co.intella.service.TradeDetailOtherInfoService;
import co.intella.service.TradeDetailService;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.net.URLEncoder;
import java.util.*;

/**
 * @author Alex
 */
@Service
public class TradeDetailServiceImpl implements TradeDetailService {

    private final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(TradeDetailServiceImpl.class);

    @Value("${host.dao}")
    private String DAO_URL;

    @Resource
    private TradeDetailOtherInfoService tradeDetailOtherInfoService;

    @Resource
    private QrcodeService qrcodeService;

    @Autowired
    private QrType7RepositoryImpl qrType7RepositoryImpl;

    @Autowired
    private QrType7Repository qrType7Repository;

    @Autowired
    private QrcodeParameterRepository qrcodeParameterRepository;

    @Autowired
    private DiscountTradeOLPayService discountTradeOLPayService;

    @Autowired
    private EntityManager entityManager;

    /**
     * 2021 03 05 ，更改為傳入的tradeDetail 也會換成最新的！
     *
     * @param tradeDetail
     * @return
     */

    public TradeDetail save(TradeDetail tradeDetail) {
//    	setOrderIdDemoInfo(tradeDetail);
        try {
            this.checkTradeAmount(tradeDetail);
            String response = HttpUtil.post(DAO_URL + "api/trade/update", new ObjectMapper().writeValueAsString(tradeDetail));

            tradeDetail = new Gson().fromJson(response, TradeDetail.class);
            if (Objects.isNull(tradeDetail.getTradeDetailRandomId()) || StringUtils.isBlank(tradeDetail.getOrderId())) {
                throw new RuntimeException(String.format("Objects.isNull(tradeDetail.getTradeDetailRandomId()) {} , StringUtils.isBlank(tradeDetail.getOrderId()) {}"
                        , Objects.isNull(tradeDetail.getTradeDetailRandomId()), StringUtils.isBlank(tradeDetail.getOrderId())));
            }
            this.checkRequsetDetail(tradeDetail);

        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] save() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));

            // 如果新增失敗，可能是換支付付款，故這邊額外將method更換過去新的
            TradeDetail _tradeDetail1 = this.getOne(tradeDetail.getAccountId(), tradeDetail.getOrderId());
            if (Objects.nonNull(_tradeDetail1)
                    && !tradeDetail.getMethod().equals(_tradeDetail1.getMethod())
                    && !SystemInstance.TRADE_SUCCESS.equals(_tradeDetail1.getStatus())
                    && _tradeDetail1.getPayment() == tradeDetail.getPayment()) {
                _tradeDetail1.setMethod(tradeDetail.getMethod());
                _tradeDetail1.setPaymentAccount(tradeDetail.getPaymentAccount());
                try {
                    String str = HttpUtil.post(DAO_URL + "api/trade/update", new ObjectMapper().writeValueAsString(_tradeDetail1));
                    LOGGER.info("TradeDetail update method {}", str);
                } catch (Exception ex) {
                    LOGGER.error("TradeDetail update method {} to {} FAIL", _tradeDetail1.getMethod(), tradeDetail.getMethod());
                }
            }

            return null;
        } finally {

            if (SystemInstance.TYPE_OLPAY.equals(tradeDetail.getServiceType()) && StringUtils.isNotBlank(tradeDetail.getQrcodeToken())
                    && Arrays.asList(SystemInstance.TRADE_WAITING, SystemInstance.TRADE_CREATING_ORDER).contains(tradeDetail.getStatus())) {
                DiscountTradeOLPay discountTradeOLPay = discountTradeOLPayService.getOne(tradeDetail.getAccountId(), tradeDetail.getQrcodeToken());
                if (Objects.nonNull(discountTradeOLPay)) {
                    discountTradeOLPay.setOrderId(tradeDetail.getOrderId());
                    discountTradeOLPayService.save(discountTradeOLPay);
                }
            }
        }

        return tradeDetail;
    }

    public TradeDetail getOne(String orderId) {
        try {
            String responseEntity = HttpRequestUtil.httpGet(DAO_URL + "api/trade/" + orderId);
            return new Gson().fromJson(responseEntity, TradeDetail.class);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] save() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public TradeDetail getOne(UUID uuid) {
        try {
            String responseEntity = HttpRequestUtil.httpGet(DAO_URL + "api/trade/uuid/" + uuid.toString());
            return new Gson().fromJson(responseEntity, TradeDetail.class);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] getOne() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public TradeDetail getOneBySystemOrderId(String systemOrderId) {
        try {
            String responseEntity = HttpRequestUtil.httpGet(DAO_URL + "api/trade/systemOrderId/" + systemOrderId);
            return new Gson().fromJson(responseEntity, TradeDetail.class);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] getOneBySystemOrderId() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    @Override
    public TradeDetail getOneByIntellaOrderId(String intellaOrderId) {
        try {
            String responseEntity = HttpRequestUtil.httpGet(DAO_URL + "api/trade/intellaOrderId/" + intellaOrderId);
            Map<String, String> resultMap = JsonUtil.parseJson(responseEntity, Map.class);
            return new Gson().fromJson(resultMap.get("message"), TradeDetail.class);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] getOneBySystemOrderId() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public TradeDetail getOneBySysOrder(String accountId, String sysOrderId) {
        try {
            String responseEntity = HttpRequestUtil.httpGet(DAO_URL + "api/trade/systemId/" + accountId + "/" + sysOrderId);
            return new Gson().fromJson(responseEntity, TradeDetail.class);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] getOneBySysOrder() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public TradeDetail getOneByTradeToken(String method, String tradeToken) {
        try {
            String responseEntity = HttpRequestUtil.httpGet(DAO_URL + "api/trade/tradeToken/" + method + "/" + tradeToken);
            return new Gson().fromJson(responseEntity, TradeDetail.class);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] getOneByTradeToken() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public TradeDetail getOne(String accountId, String orderId) {
        try {
            String responseEntity = HttpRequestUtil.httpGet(DAO_URL + "api/trade/getByAccountAndOrderId/" + URLEncoder.encode(accountId, "UTF-8") + "/" + orderId);
            return new Gson().fromJson(responseEntity, TradeDetail.class);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] getOne() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public TradeDetail getOneByPartnerUserIdAndOrderId(String partnerUserId, String orderId) {
        try {
            String responseEntity = HttpRequestUtil.httpGet(DAO_URL + "api/trade/getOneByPartnerUserIdAndOrderId/" + partnerUserId + "/" + orderId);
            return new Gson().fromJson(responseEntity, TradeDetail.class);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] getOneByPartnerUserIdAndOrderId() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public TradeDetail getOneByOrderIdAndMethod(String orderId, String method) {
        try {
            String responseEntity = HttpRequestUtil.httpGet(DAO_URL + "api/trade/getOneByOrderIdAndMethod/" + method + "/" + orderId);
            return new Gson().fromJson(responseEntity, TradeDetail.class);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] getOneByOrderIdAndMethod() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public List<TradeDetail> listByMerchants(CustomizePageRequest request) {
        try {
            String responseEntity = HttpUtil.post(DAO_URL + "api/trade/listByMerchants", new ObjectMapper().writeValueAsString(request));
            return new Gson().fromJson(responseEntity, new TypeToken<List<TradeDetail>>() {
            }.getType());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] pageList() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public TradeDetail getOneByShortId(String shortId) {
        try {
            String responseEntity = HttpRequestUtil.httpGet(DAO_URL + "api/trade/shortId/" + shortId);
            return new Gson().fromJson(responseEntity, TradeDetail.class);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] getOneByShortId() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public List<TradeDetail> pageList(CustomizePageRequest request) {
        try {
            String responseEntity = HttpUtil.post(DAO_URL + "api/trade/pageList", new ObjectMapper().writeValueAsString(request));
            return new Gson().fromJson(responseEntity, new TypeToken<List<TradeDetail>>() {
            }.getType());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] pageList() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public long countPaymentByCreateDate(CustomizePageRequest request) {
        try {
            String responseEntity = HttpUtil.post(DAO_URL + "api/trade/countByDate", new ObjectMapper().writeValueAsString(request));
            Long count = Long.parseLong(responseEntity);
            if (count == null) {
                return 0;
            }
            return count;
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] countPaymentByCreateDate() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return 0;
        }
    }

    public long pageCount(CustomizePageRequest request) {
        try {
            String responseEntity = HttpUtil.post(DAO_URL + "api/trade/pageCount", new ObjectMapper().writeValueAsString(request));
            Long count = Long.parseLong(responseEntity);
            if (count == null) {
                return 0;
            }
            return count;
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] countPaymentByCreateDate() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return 0;
        }
    }

    public List<TradeDetail> listByAccountId(String accountId) {

        try {
            String result = HttpRequestUtil.httpGet(DAO_URL + "api/trade/list/" + accountId);
            LOGGER.info("listByAccountId : " + result);
            return new Gson().fromJson(result, new TypeToken<List<TradeDetail>>() {
            }.getType());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }

    }

    public List<TradeDetail> listByMerchantIdAndDate(String mchId, String startDate, String endDate) {

        LOGGER.info("listByMerchantIdAndDate() " + mchId + ", " + startDate + " - " + endDate);
        Map<String, String> map = new HashMap<String, String>();

        map.put("merchantId", mchId);
        map.put("startDate", startDate);
        map.put("endDate", endDate);
        map.put("type", "DATE");

        try {
            String responseEntity = HttpUtil.post(DAO_URL + "api/trade/list", new Gson().toJson(map));
            return new Gson().fromJson(responseEntity, new TypeToken<List<TradeDetail>>() {
            }.getType());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] listByMerchantIdAndDate() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public List<TradeDetail> list(IntegratedOrderQueryRequestData requestData, String merchantId) {

        // todo startDate, endDate, orderStatus, platform, bank, storeOrderNo, storeInfo, cashier

        Map<String, String> map = new HashMap<String, String>();

        map.put("StartDate", requestData.getStartDate());
        map.put("EndData", requestData.getEndDate());
        map.put("OrderStatus", requestData.getOrderStatus());
        map.put("Platform", requestData.getPlatform());
        map.put("MerchantId", merchantId);

        try {
            String responseEntity = HttpUtil.post(DAO_URL + "api/trade/listByDate", new Gson().toJson(map));
            return new Gson().fromJson(responseEntity, new TypeToken<List<TradeDetail>>() {
            }.getType());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] list() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public List<TradeDetailVo> listByBatchNo(String merchantId, String deviceId, String batchNo) {
        try {
            String result = HttpRequestUtil.httpGet(DAO_URL + "api/trade/list/" + merchantId + "/" + deviceId + "/" + batchNo);

            LOGGER.debug("listByBatchNo : " + result);
            return new Gson().fromJson(result, new TypeToken<List<TradeDetailVo>>() {
            }.getType());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public List<TradeDetailVo> listGroupEZC(String terminalId, String deviceId, String batchNo) {
        try {
            String result = HttpRequestUtil.httpGet(DAO_URL + "api/trade/listEzc/" + terminalId + "/" + deviceId + "/" + batchNo);

            LOGGER.debug("listGroupEZC : " + result);
            return new Gson().fromJson(result, new TypeToken<List<TradeDetailVo>>() {
            }.getType());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }


    public List<TradeDetailVo> listByUserId(String merchantId, String userId) {
        try {
            String result = HttpRequestUtil.httpGet(DAO_URL + "api/trade/listDSL/" + merchantId + "/" + userId);
            LOGGER.debug("listByUserId : " + result);
            return new Gson().fromJson(result, new TypeToken<List<TradeDetailVo>>() {
            }.getType());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public List<TradeDetailVo> listByUserId(String merchantId, String deviceId, String userId) {
        try {
            Map<String, String> request = new HashMap<String, String>();
            request.put("type", "MDU");
            request.put("merchantId", merchantId);
            request.put("deviceId", deviceId);
            request.put("userId", userId);
            String result = HttpUtil.post(DAO_URL + "api/trade/list", new Gson().toJson(request));
            LOGGER.debug("listByUserId : " + result);
            return new Gson().fromJson(result, new TypeToken<List<TradeDetailVo>>() {
            }.getType());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public List<TradeDetailVo> listTopOneByUserId(String merchantId, String deviceId, String userId) {
        try {
            Map<String, String> request = new HashMap<String, String>();
            request.put("type", "MDU_TOP1");
            request.put("merchantId", merchantId);
            request.put("deviceId", deviceId);
            request.put("userId", userId);

            LOGGER.info("listTopOneByUserId() " + new Gson().toJson(request));
            String result = HttpUtil.post(DAO_URL + "api/trade/list", new Gson().toJson(request));
            LOGGER.debug("listByUserId : " + result);
            return new Gson().fromJson(result, new TypeToken<List<TradeDetailVo>>() {
            }.getType());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    @Override
    public List<TradeDetailVo> listByBatchNoAndMethod(String merchantId, String deviceId, String batchNo,
                                                      String method) {
        try {
            String result = HttpRequestUtil.httpGet(DAO_URL + "api/trade/list/" + merchantId + "/" + deviceId + "/" + batchNo + "/" + method);

            LOGGER.debug("listByBatchNoAndMethod : " + result);
            return new Gson().fromJson(result, new TypeToken<List<TradeDetailVo>>() {
            }.getType());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    @Override
    public List<TradeDetail> getFubonOrderList() {
        String result = HttpUtil.doGet(DAO_URL + "api/trade/getFubonOrderList");
        return JsonUtil.getGson().fromJson(result, new TypeToken<List<TradeDetail>>() {
        }.getType());
    }

    @Override
    public List<TradeDetail> queryForCardIdMchId(QueryForCardIdRequest queryForCardIdRequest, String mchId) {
        StringBuilder sb = new StringBuilder();
        sb.append(DAO_URL).append("api/trade/queryForCardIdMchId")
                .append("/").append(queryForCardIdRequest.getCardId())
                .append("/").append(mchId)
                .append("/").append(queryForCardIdRequest.getStartDate())
                .append("/").append(queryForCardIdRequest.getEndDate());
        String result = HttpUtil.doGet(sb.toString());
        return JsonUtil.getGson().fromJson(result, new TypeToken<List<TradeDetail>>() {
        }.getType());
    }

    public List<String> queryTobWebAtmOrder() {

        String sql = "select orderId from allpaypass_new.TradeDetail where sysTime>=(NOW() - interval 1 day)  and status not in ('Trade success','Trade fail') and method = :method ;";
        Query query = entityManager.createNativeQuery(sql).setParameter("method", "15700");
        return query.getResultList();
    }


    /**
     * 20200429    因J 不知道答應啥反就是這邊欄位帶JSON格式 則就存起來，裏頭目前只訂出CallBack
     *
     * @param tradeDetail1
     */
    private void checkRequsetDetail(TradeDetail tradeDetail1) {

        if (StringUtils.isNotEmpty(tradeDetail1.getQrcodeToken()) && Objects.isNull(tradeDetailOtherInfoService.getOneByUNHEXUuid(tradeDetail1.getTradeDetailRandomId().toString()))) {
            QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(tradeDetail1.getQrcodeToken());
            if (StringUtils.isNotEmpty(qrcodeParameter.getDetail())) {
                String deatil = qrcodeParameter.getDetail().trim();
                if (deatil.startsWith("{") && deatil.endsWith("}")) {
                    Map<String, String> deatilMap = JsonUtil.parseJson(deatil, Map.class);
                    if (MapUtils.isNotEmpty(deatilMap)) {
                        String callBack = deatilMap.getOrDefault("CallBackUrl", null);
                        TradeDetailOtherInfo tradeDetailOtherInfo = new TradeDetailOtherInfo();
                        tradeDetailOtherInfo.setTradeDetailRandomId(tradeDetail1.getTradeDetailRandomId());
                        tradeDetailOtherInfo.setCallBackUrl(callBack);
                        tradeDetailOtherInfo.setRequestDetail(deatil);
                        tradeDetailOtherInfoService.save(tradeDetailOtherInfo);
                    }
                }
            }
        }
    }

    private void checkTradeAmount(TradeDetail tradeDetail) {

    }
}
