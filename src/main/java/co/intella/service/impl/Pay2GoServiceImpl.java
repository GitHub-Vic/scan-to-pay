package co.intella.service.impl;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.net.ssl.HttpsURLConnection;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import co.intella.crypto.AesCryptoUtil;
import co.intella.domain.integration.IntegratedRefundRequestData;
import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.domain.pay2go.Pay2GoNotifyRequest;
import co.intella.domain.pay2go.Pay2GoRefundResult;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import co.intella.service.AppNotificationService;
import co.intella.service.MerchantService;
import co.intella.service.Pay2GoService;
import co.intella.service.PaymentAccountService;
import co.intella.service.TradeDetailService;
import co.intella.service.TradeProcedureService;
import co.intella.utility.GeneralUtil;
import co.intella.utility.Pay2GoUtil;
import co.intella.utility.SystemInstance;

/**
 * @author Alex
 */
@Service
public class Pay2GoServiceImpl implements Pay2GoService {

	private final Logger LOGGER = LoggerFactory.getLogger(Pay2GoServiceImpl.class);

	@Resource
	private PaymentAccountService paymentAccountService;

	@Autowired
	private Environment env;

	@Autowired
	private MessageSource messageSource;

	@Resource
	private TradeProcedureService tradeProcedureService;

	@Resource
	private AppNotificationService appNotificationService;

	@Resource
	private MerchantService merchantService;

	@Resource
	private TradeDetailService tradeDetailService;

	@Value("${host.request.p2g}")
	private String PAY2GO_URL;

	@Value("${api.url}")
	private String API_URL;

	public Boolean verify(Map<String, String> map, PaymentAccount paymentAccount) {
		String tradeSha = map.get("TradeSha").trim();
		String tradeInfo = map.get("TradeInfo").trim();
		String calValue = getTradeSha(tradeInfo, paymentAccount);
		return tradeSha.equals(calValue);

	}

	public String getTradeSha(String tradeInfo, PaymentAccount paymentAccount) {
		String calValue = "HashKey=" + paymentAccount.getHashKey() + "&" + tradeInfo + "&HashIV="
				+ paymentAccount.getHashIV();
		return GeneralUtil.getHash(calValue).toUpperCase();

	}

	public void updateNotify(Map<String, String> map, PaymentAccount paymentAccount) throws Exception {
		SecretKey secretKey = AesCryptoUtil.convertSecretKey(paymentAccount.getHashKey().getBytes());
		String tradeInfo = aesDecrypt(secretKey, paymentAccount.getHashIV().getBytes(),
				GeneralUtil.hexStringToByteArray(map.get("TradeInfo")));
		LOGGER.info("[tradeInfo] : " + tradeInfo.trim());

		Pay2GoNotifyRequest request = new Gson().fromJson(tradeInfo.trim(), Pay2GoNotifyRequest.class);

		TradeDetail tradeDetail = tradeDetailService.getOne(request.getResult().getMerchantOrderNo());

		if (tradeDetail == null) {
			return;
		}
		tradeDetail.setMethod("12400");
		if (request.getStatus().equals("SUCCESS")) {
			if (request.getResult().getOrderStatus().equals("2")) {
				if(!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
					tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
					tradeDetail.setSystemOrderId(request.getResult().getTradeNo());
					tradeDetail.setPlatformPaidDate(request.getResult().getPaymentTime());
					tradeDetail.setPayment(Long.parseLong(request.getResult().getAmt()));
					appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
				}
			} else if (request.getResult().getOrderStatus().equals("6")) {
				tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
			}
		}
		tradeDetailService.save(tradeDetail);
	}

	public static byte[] aesEncrypt(SecretKey secretKey, byte[] iv, String plaintext) throws Exception {
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(1, secretKey, new IvParameterSpec(iv));
		return cipher.doFinal(plaintext.getBytes());
	}

	public String aesDecrypt(SecretKey secretKey, byte[] iv, byte[] encryptText) throws Exception {
		Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
		cipher.init(2, secretKey, new IvParameterSpec(iv));
		byte[] decryptedText = cipher.doFinal(encryptText);
		return new String(decryptedText).trim();
	}

	public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {
		String generalResponseData = SystemInstance.EMPTY_STRING;

		String integrateMchId = requestHeader.getMerchantId();
		long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
		PaymentAccount paymentAccount = paymentAccountService.getOneByDao(integrateMchId, bankId);

		if (requestHeader.getServiceType().equals(SystemInstance.TYPE_MICROPAY)) {

		} else if (requestHeader.getServiceType().equals("Refund")) {
			LOGGER.info("[Pay2Go][Refund][data]" + data.getAsString());

			IntegratedRefundRequestData integratedRefundRequestData = new Gson().fromJson(data.getAsString(),
					IntegratedRefundRequestData.class);

//            TradeDetail tradeDetail = tradeDetailService.getOneByDao(integratedRefundRequestData.getStoreOrderNo());

			String refundResult;
			TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

			refundResult = prepareRefund(paymentAccount, tradeDetail);
			LOGGER.info("[Pay2Go][Refund][Request]" + refundResult);

			refundResult = refund(refundResult);
//            refundResult="{\"Status\":\"SUCCESS\",\"Version\":\"2.0\",\"MerchantID\":\"PG300000004668\",\"RefundInfo\":\"f9e15055bc35c78d583277acfc39c0563e10f6c8d6d04064f8a8c2016b8720a93cbfc05493999889062c4804661904565596799db8cde9ef4c7582dd45c206c166d7ceb68f7b60ba34b629b36107d0088211ae842ce8b528e0b3db4ac9ed58e481873fb824cf1fb53a9a4a244fb26e2b3782a3e99108fece6fe578d994a353fdda4b2df027760aba8bbe246ca4c81b3272da4d7028c9d03e58ec95b0753cd97ff03307522c21061b4e184e9f14b95abcdd889ac1a96764b36fee9437741ad98a06e51f7a4adfc5f0240f6b0933d9055eba89f627c7a7d827077b5c39c2670b823ebb0ea361e41e242bf9d96a57da63c7edc809555928854ad0bdefd05459b653a69bb3bd6b0fa07b4feacd562a1642f5aec8eb8e6c6574fd05b66a61be5f9ac104f4ebeca5828bcdb8fa17a55510cb80c69defc5cb0ba98ebd23e000a42517d0aef6d6aaef15369ff1e28a702bcd9f1475c6bfe383f2db9c2b08d360bbf776ceaaaffcc85ecd0a7c850efaba65e399965ff93ea996fc2a0a2b0990478b14cdde\",\"RefundSha\":\"408141E725EB5A4FA8CADECF24AF710A2E1E90692A65E4542A7C57CDF8DB5171\"}";
			LOGGER.info("[Pay2Go][Refund][Response]" + refundResult);

			JsonObject jsonObject = new Gson().fromJson(refundResult, JsonObject.class);
			String status = jsonObject.get("Status").getAsString();
			String refundInfo = decryptRefund(jsonObject.get("RefundInfo").getAsString(), paymentAccount);
			LOGGER.info("[Pay2Go][Refund][refundInfo]" + refundInfo);
			JsonObject jsonObject2 = new Gson().fromJson(refundInfo, JsonObject.class);

			Pay2GoRefundResult refundResult1 = new Gson().fromJson(jsonObject2.get("Result"), Pay2GoRefundResult.class);

			updateRefund(tradeDetail, status, refundResult1, data);

//            tradeProcedureService.setTradeDetailResponse(integrateMchId,requestHeader,refundResult,data);

			ResponseGeneralHeader responseGeneralHeader = new Pay2GoUtil().getResponseHeader(status, integrateMchId,
					requestHeader, messageSource);
			IntegratedRefundResponseData integratedRefundResponseData = new Pay2GoUtil()
					.getIntegratedRefundResponseData(status, refundResult1, requestHeader, tradeDetail,
							integratedRefundRequestData);
//            integratedRefundResponseData.setPlatformRsp(refundResult);

			ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
			responseGeneralBody.setHeader(responseGeneralHeader);
			responseGeneralBody.setData(integratedRefundResponseData);
			generalResponseData = new Gson().toJson(responseGeneralBody);

			LOGGER.info("integrate response ready :" + generalResponseData);

		}

		return generalResponseData;
	}

	private void updateRefund(TradeDetail tradeDetail, String status, Pay2GoRefundResult refundResult,
			JsonPrimitive data) {

		if (status.equals("SUCCESS")) {
			tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
			tradeDetail.setSystemRefundId(refundResult.getTradeNo());
			tradeProcedureService.updateRefundDeatil(tradeDetail,
					new Gson().fromJson(data.getAsString(), JsonObject.class), SystemInstance.TRADE_REFUND_SUCCESS,
					refundResult.getRefundTime());
		} else if (status.equals("MTR01018") || status.equals("MTR01016")) {
			tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
		} else {
			tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
			tradeProcedureService.updateRefundDeatil(tradeDetail,
					new Gson().fromJson(data.getAsString(), JsonObject.class), SystemInstance.TRADE_REFUND_FAIL);
		}

		tradeDetailService.save(tradeDetail);
	}

	private String decryptRefund(String refundInfo, PaymentAccount paymentAccount) {
		SecretKey secretKey = AesCryptoUtil.convertSecretKey(paymentAccount.getHashKey().getBytes());
		try {
			return aesDecrypt(secretKey, paymentAccount.getHashIV().getBytes(), GeneralUtil.hex2Byte(refundInfo));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String refund(String requestDataString) {
		String result = null;
		try {
			result = pay2GoPost(PAY2GO_URL + "trade_refund", requestDataString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public String prepareRefund(PaymentAccount paymentAccount, TradeDetail tradeDetail) {

		DateTime now = DateTime.now();
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("TimeStamp", String.valueOf(now.getMillis()));
		map.put("MerchantID", paymentAccount.getAccount());
		map.put("Version", "2.0");
//        map.put("TradeNo",request.getMerchantTradeDate());
		map.put("TradeNo", tradeDetail.getSystemOrderId());
//        map.put("RefundBarCode",request.getMerchantTradeTime());
		map.put("RefundType", "1");
		map.put("Currency", "TWD");
		map.put("RefundAmt", String.valueOf(tradeDetail.getPayment()));

		StringBuilder checkStr = new StringBuilder();

		for (Map.Entry<String, String> entry : map.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();

			checkStr.append(key).append("=").append(value).append("&");
		}
		checkStr.deleteCharAt(checkStr.length() - 1);

		String str = null;
		try {
			str = convertMapToUrlForm(map);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		LOGGER.info("[Pay2Go][Refund][Request]" + str);

		SecretKey secretKey = AesCryptoUtil.convertSecretKey(paymentAccount.getHashKey().getBytes());
		String enc = null;
		try {
			enc = GeneralUtil.byte2Hex(AesCryptoUtil.encrypt(secretKey, paymentAccount.getHashIV().getBytes(), str));
		} catch (Exception e) {
			e.printStackTrace();
		}

		String tradeSha = getTradeSha(enc, paymentAccount);

		HashMap<String, String> map2 = new HashMap<String, String>();
		map2.put("MerchantID", paymentAccount.getAccount());
		map2.put("Version", "2.0");
		map2.put("RefundInfo", enc);
		map2.put("RefundSha", tradeSha);

		String requestDataString = null;
		try {
			requestDataString = convertMapToUrlForm(map2);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return requestDataString;
	}

	private String convertMapToUrlForm(HashMap<String, String> params) throws UnsupportedEncodingException {
		StringBuilder result = new StringBuilder();
		boolean first = true;
		for (Map.Entry<String, String> entry : params.entrySet()) {
			if (first)
				first = false;
			else
				result.append("&");
			result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
			result.append("=");
			result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
		}
		return result.toString();
	}

	public String pay2GoPost(String url, String query) throws Exception {
		URL requestUrl = new URL(url);
		HttpURLConnection con = (HttpsURLConnection) requestUrl.openConnection();
		con.setRequestMethod("POST");

		con.setRequestProperty("Content-length", String.valueOf(query.length()));
		con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		con.setDoOutput(true);
		con.setDoInput(true);

		DataOutputStream output = new DataOutputStream(con.getOutputStream());
		output.writeBytes(query);
		output.close();
		DataInputStream input = new DataInputStream(con.getInputStream());

		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(input));
		String result;

		StringBuilder buffer = new StringBuilder();
		while ((result = bufferedReader.readLine()) != null) {
			buffer.append(result);
		}

		input.close();

		return buffer.toString();
	}

}
