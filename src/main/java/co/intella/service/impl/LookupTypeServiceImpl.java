package co.intella.service.impl;

import co.intella.model.IntTbLookupType;
import co.intella.net.HttpRequestUtil;
import co.intella.service.LookupTypeService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class LookupTypeServiceImpl implements LookupTypeService {

    @Value("${host.dao}")
    private String DAO_URL;


    @Override
    public IntTbLookupType findOneByLookType(String type) {
        IntTbLookupType lookupType = null;
        try {
            String responseEntity = HttpRequestUtil.httpGet(DAO_URL + "api/lookupType/" + type);
            lookupType = new Gson().fromJson(responseEntity, IntTbLookupType.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lookupType;

    }


}
