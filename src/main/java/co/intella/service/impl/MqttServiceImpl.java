package co.intella.service.impl;

import co.intella.AllPayApplication;
import co.intella.domain.notify.ApnsMessage;
import co.intella.service.MqttService;
import co.intella.service.impl.ApnsClientProvider.ApnsClientMeta;
import com.turo.pushy.apns.ApnsClient;
import com.turo.pushy.apns.ApnsClientBuilder;
import com.turo.pushy.apns.PushNotificationResponse;
import com.turo.pushy.apns.util.ApnsPayloadBuilder;
import com.turo.pushy.apns.util.SimpleApnsPushNotification;
import com.turo.pushy.apns.util.TokenUtil;
import io.netty.util.concurrent.Future;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.fusesource.mqtt.client.FutureConnection;
import org.fusesource.mqtt.client.MQTT;
import org.fusesource.mqtt.client.QoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

/**
 * @author Miles
 */
@Service
public class MqttServiceImpl implements MqttService {

    private final Logger LOGGER = LoggerFactory.getLogger(MqttServiceImpl.class);

    @Autowired
    private ApnsClientProvider apnsClientProvider;

    @Value("${host.activemq}")
    private String ACTIVMQ_SERVER_URL;


//    private static String APNS_KEY;
//
//    private static String APPLE_PUSH_SERVER;
//
//    @Value("${intella.apns.key}")
//    public void setApnsKey(String apnsKey) {
//        APNS_KEY = apnsKey;
//    }
//
//    @Value("${apple.push.server}")
//    public void setApplePushServer(String url){
//        APPLE_PUSH_SERVER = url;
//    }


    private static final ApnsClient apnsClient = createObject();

    private static ApnsClient createObject() {
        try {
            return new ApnsClientBuilder()
                    .setClientCredentials(AllPayApplication.APNS_KEY, AllPayApplication.APNS_PD)
                    .setApnsServer(AllPayApplication.APPLE_PUSH_SERVER).build();
        } catch (final Exception exc) {
            throw new Error(exc);
        }
    }


    @Async
    public void publish(String clientId, String message, String topic) throws Exception {
        LOGGER.info("[MQTT][" + topic + "] [" + ACTIVMQ_SERVER_URL + "] sendMessage() " + clientId + ": " + message);

        MQTT mqtt = new MQTT();
        mqtt.setHost(ACTIVMQ_SERVER_URL);
        mqtt.setClientId("FuturePublisher");
        FutureConnection connection = mqtt.futureConnection();
        connection.connect().await();

        connection.publish(topic, message.getBytes(), QoS.EXACTLY_ONCE, false).await();
        LOGGER.info("Publishing succeeded");
        connection.disconnect().await();
    }

    @Async
    public void publishAPNS(String token, ApnsMessage message, String appId) {

        try {
            LOGGER.info("publishAPNS start token: " + token);

//            LOGGER.info("pushNotification start...");
//            final SimpleApnsPushNotification pushNotification;
//            final SimpleApnsPushNotification pushNotificationTaxi;

//            { //TODO test block cancel
            final ApnsPayloadBuilder payloadBuilder = new ApnsPayloadBuilder();
            payloadBuilder.setAlertBody(message.getMsg());
            payloadBuilder.setAlertTitle(message.getTitle());
            payloadBuilder.setSoundFileName(ApnsPayloadBuilder.DEFAULT_SOUND_FILENAME);
            final String payload = payloadBuilder.buildWithDefaultMaximumLength();
            final String sanitizeTokenString = TokenUtil.sanitizeTokenString(token);

            if (appId == null || appId.isEmpty()) {
                doPublishByAppId(sanitizeTokenString, payload, "scan2pay", token);
                doPublishByAppId(sanitizeTokenString, payload, "taxi", token);
            } else {
                doPublishByAppId(sanitizeTokenString, payload, appId, token);
            }

//                pushNotification = new SimpleApnsPushNotification(sanitizeTokenString, "co.intella.ScanToPay", payload);
//                pushNotificationTaxi = new SimpleApnsPushNotification(sanitizeTokenString, "co.intella.Taxi", payload);

//            }
//            doPublish(pushNotification, apnsClient);
//            doPublish(pushNotificationTaxi, apnsClientTaxi);

        } catch (InterruptedException e) {
            LOGGER.info("InterruptedException." + Arrays.toString(e.getStackTrace()));
        } catch (ExecutionException e) {
            LOGGER.info("ExecutionException." + Arrays.toString(e.getStackTrace()));
        }
    }


    private void doPublish(final SimpleApnsPushNotification pushNotification, ApnsClient client, String token)
            throws InterruptedException, ExecutionException {
        Future<PushNotificationResponse<SimpleApnsPushNotification>> sendNotificationFuture = null;
        try {
            sendNotificationFuture = client.sendNotification(pushNotification);

            final PushNotificationResponse<SimpleApnsPushNotification> pushNotificationResponse = sendNotificationFuture.get();

            LOGGER.info("pushNotification end token: " + token);

            if (pushNotificationResponse.isAccepted()) {
                LOGGER.info("Push notification accepted by APNs gateway. token: " + token);
            } else {
                LOGGER.error("Notification rejected by the APNs gateway:" + pushNotificationResponse.getRejectionReason() + "token: " + token);
                if (pushNotificationResponse.getTokenInvalidationTimestamp() != null) {
                    LOGGER.error("\t and the token is invalid as of " + pushNotificationResponse.getTokenInvalidationTimestamp() + "token: " + token);
                }
            }
        } catch (Exception e) {
            LOGGER.error("[doPublish] Exception token => {}",token);
            LOGGER.error(ExceptionUtils.getMessage(e) + "\t" + ExceptionUtils.getStackTrace(e));
        } finally {
            if (Objects.nonNull(sendNotificationFuture)) {
                sendNotificationFuture.cancel(true);
            }
        }
    }

    private SimpleApnsPushNotification getSimpleApnsPushNotification(final String sanitizeTokenString, String topic, String payload) {
        return new SimpleApnsPushNotification(sanitizeTokenString, topic, payload);
    }

    private void doPublishByAppId(final String sanitizeTokenString, final String payload, String appId, String token) throws InterruptedException, ExecutionException {
        LOGGER.info("doPublish AppId: " + appId + " token:" + token);
        ApnsClientMeta apnsClientMeta = apnsClientProvider.getApnsClientMeta(appId);
        final SimpleApnsPushNotification pushNotification = getSimpleApnsPushNotification(sanitizeTokenString, apnsClientMeta.getTopic(), payload);

        doPublish(pushNotification, apnsClientMeta.getApnsClient(), token);
    }

}
