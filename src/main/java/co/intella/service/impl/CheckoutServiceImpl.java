package co.intella.service.impl;

import co.intella.domain.CheckoutCtrlRequest;
import co.intella.domain.CheckoutCtrlVo;
import co.intella.domain.contratStore.OperationHistory;
import co.intella.domain.contratStore.RefundDetailVo;
import co.intella.domain.contratStore.TradeDetailVo;
import co.intella.domain.easycard.EzCardBasicResponse;
import co.intella.domain.integration.ResponseDetailBody;
import co.intella.model.*;
import co.intella.net.HttpRequestUtil;
import co.intella.repository.CheckoutRepository;
import co.intella.repository.MerchantRepository;
import co.intella.service.*;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Strings;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import javax.annotation.Resource;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CheckoutServiceImpl implements CheckoutService {

    private final Logger LOGGER = LoggerFactory.getLogger(CheckoutServiceImpl.class);

    @Value("${host.dao}")
    private String DAO_URL;

    @Resource
    private LookupCodeService lookupCodeService;

    @Resource
    private CheckoutRepository checkoutRepository;

    @Resource
    private INT_TB_CheckoutCtrlService INT_TB_CheckoutCtrlService;

    @Resource
    private INT_TB_CheckoutLogService INT_TB_CheckoutLogService;

    @Resource
    private MerchantRepository merchantRepository;

    @Resource
    private MerchantService merchantService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private ReserveDetailService reserveDetailService;

    @Resource
    private ReserveCancelService reserveCancelService;

    @Resource
    private RefundDetailService refundDetailService;

    @Resource
    private OperationHistoryService operationHistoryService;

    @Resource(type = co.intella.service.impl.CheckoutTraffixServiceImpl.class)
    private CheckoutCustomService checkoutTraffixServiceImpl;

    @Resource(type = co.intella.service.impl.CheckoutSmallAmountServiceImpl.class)
    private CheckoutCustomService checkoutSmallAmountServiceImpl;

    @Resource(type = co.intella.service.impl.CheckoutTaxiServiceImpl.class)
    private CheckoutCustomService checkoutTaxiServiceImpl;

    @Resource
    private MailService mailService;

    private final static String P = "P";

    private final static String FAIL = "FAIL";

    private final static String COMP = "COMP";

    private final static String ERR = "ERR";

    final static String REQUEST_HEADER = "requestHeader";

    final static String MERCHANT = "merchant";

    final static String PAYMENT_ACCOUNT = "paymentAccount";

    final static String DEVICE = "device";

    private final static String SUCCESS_CODE = "000000";

    final static String SUCCESS = "Success";

    final static String TRADE_DETAILS = "tradeDetails";

    final static String RESERVE_DETAILS = "reserveDetails";

    final static String REFUND_DETAILS = "refundDetails";

    final static String RESERVE_CANCELS = "reserveCancels";

    final static String TX_COUNT_TRADE = "txCountTrade";

    final static String TX_AMOUNT_TRADE = "txAmountTrade";

    final static String TX_COUNT_REFUND = "txCountRefund";

    final static String TX_AMOUNT_REFUND = "txAmountRefund";

    final static String TX_COUNT_RESERVE = "txCountReserve";

    final static String TX_AMOUNT_RESERVE = "txAmountReserve";

    final static String TX_COUNT_CANCEL = "txCountCancel";

    final static String TX_AMOUNT_CANCEL = "txAmountCancel";

    final static String SETTLEMENT_RESULT = "settlementResult";

    final static String TX_COUNT_AUTO_RESERVE = "txCountAutoReserve";

    final static String TX_COUNT_CASH_RESERVE = "txCountCashReserve";

    final static String TX_AMOUNT_AUTO_RESERVE = "txAmountAutoReserve";

    final static String TX_AMOUNT_CASH_RESERVE = "txAmountCashReserve";

    final static String TXN_DATA_LIST = "txnDataList";

    final static String CHECKOUT_CTRL_VO = "checkoutCtrlVo";

    private final static String TAXI_TYPE = "taxi";

    private final static String EZC_TYPE = "ezc";

    private final static String TRAFFIC_TYPE = "traffic";

    private final static String CHECKOUT_EMAIL = "checkoutEmail";

    private final static String CHECKOUT_STATUS = "checkoutStatus";

    final static String BATCH_NO = "batchNo";

    @Value("${host.request.ezcPark}")
    private String parkServiceHost;

    @SuppressWarnings("unchecked")
    public String checkout(String accountId, String ownerDeviceId, String batchNo, RequestHeader requestHeader,
                           Calendar createDate, String flag) {
        String paymentResult = "";
        String createDateEnd = "";
        String createDateStart = "";

        // 如果是noDate 不帶日期查詢
        if (!flag.equals("noDate")) {
            // 取得開始結束時間(當天、前一天)，若無則取當下
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            if (createDate == null) {
                createDate = Calendar.getInstance();
            }
            setMaxTimeOfDay(createDate);
            createDateEnd = sdf.format(createDate.getTime());
//			createDate.add(Calendar.DATE, -1);
            setMinTimeOfDay(createDate);
            createDateStart = sdf.format(createDate.getTime());
        }

        LOGGER.info("[CHECKOUT] START ");
        LOGGER.info("[CHECKOUT] accountId " + accountId);
        LOGGER.info("[CHECKOUT] ownerDeviceId " + ownerDeviceId);
        LOGGER.info("[CHECKOUT] batchNo " + batchNo);
        LOGGER.info("[CHECKOUT] createDateStart " + createDateStart);
        LOGGER.info("[CHECKOUT] createDateEnd " + createDateEnd);

        if (!flag.equals("noDate")
                && (Strings.isNullOrEmpty(createDateStart) || Strings.isNullOrEmpty(createDateEnd))) {
            LOGGER.error("[EXCEPTION] illegal Date ");
            throw new RuntimeException("illegal Date");
        }
        // 取的所有需要結帳之control檔
        // distinct accountId, batchNo, comment, ownerDeviceId
        List<CheckoutCtrlVo> checkoutCtrlVos = checkoutRepository.getCheckoutList(accountId, ownerDeviceId, batchNo,
                createDateStart, createDateEnd, "18");
        LOGGER.info("[CHECKOUT] checkoutCtrlVos size " + checkoutCtrlVos.size());


        for (CheckoutCtrlVo checkoutCtrlVo : checkoutCtrlVos) {
            try {
                CheckoutCustomService checkoutCustomService = null;
                Map<String, Object> checkOutObj = new HashMap<String, Object>();
                LOGGER.info("[CHECKOUT] checkoutCtrlVo " + checkoutCtrlVo);

                checkoutCustomService = prepareCheckoutData(requestHeader, checkoutCtrlVo, checkOutObj);

                // 新增或更新ctrl檔 (status 為 I or R)
                INT_TB_CheckoutCtrl checkoutCtrl = parseCheckoutCtrl(checkoutCtrlVo);
                INT_TB_CheckoutCtrlService.save(checkoutCtrl);

                checkoutCtrlVo.setCcSeqId(BigInteger.valueOf((Long) checkoutCtrl.getCcSeqId()));

                // 新增ctrl log 檔 (status 為 I or R)
                INT_TB_CheckoutLog checkoutLog = parseCheckoutLog(checkoutCtrlVo);
                INT_TB_CheckoutLogService.save(checkoutLog);

                // 撈取結帳資料
                getDetails(checkoutCtrlVo, checkOutObj);

                Device device = prepareDevice(checkOutObj);

                // 計算結帳資料
                checkoutCustomService.computeCheckout(checkOutObj);

                // 更新ctrl檔 (status 為 P)
                checkoutCtrl.setStatus(P);
                checkoutCtrl.setDateStamp(new Date());
                checkoutCtrl.setTradeAmount((long) checkOutObj.get(TX_AMOUNT_TRADE));
                checkoutCtrl.setRefundAmount((long) checkOutObj.get(TX_AMOUNT_REFUND));
                checkoutCtrl.setReserveAmount((long) checkOutObj.get(TX_AMOUNT_RESERVE));
                checkoutCtrl.setReserveCancelAmount((long) checkOutObj.get(TX_AMOUNT_CANCEL));
                INT_TB_CheckoutCtrlService.save(checkoutCtrl);

                // 新增ctrl log 檔(status 為 P)
                checkoutLog.setStatus(P);
                checkoutLog.setCrDate(new Date());
                checkoutLog.setDateStamp(new Date());
                INT_TB_CheckoutLogService.save(checkoutLog);

                // 執行結帳
                paymentResult = checkoutCustomService.executeCheckout(checkOutObj);
                LOGGER.info("[EZC][taxiAutoPayment][Result]" + paymentResult);
                ResponseDetailBody responseGeneralBody = new Gson().fromJson(paymentResult, ResponseDetailBody.class);
                EzCardBasicResponse ezCardBasicResponse = responseGeneralBody.getData();
                String txnResult = ezCardBasicResponse.getTxnResult();
                String errorCode = ezCardBasicResponse.getErrorCode();

                // 更新ctrl檔 (status 為 COMP or Fail)
                if (errorCode.toString().equals("000000") && txnResult.toString().equals("Success")) {
                    checkoutCtrl.setStatus(COMP);
                } else {
                    checkoutCtrl.setStatus(FAIL);
                }
                checkoutCtrl.setDateStamp(new Date());
                INT_TB_CheckoutCtrlService.save(checkoutCtrl);

                // 新增ctrl log 檔(status 為 COMP or Fail)
                checkoutLog.setStatus(checkoutCtrl.getStatus());
                checkoutLog.setCrDate(new Date());
                checkoutLog.setDateStamp(new Date());
                INT_TB_CheckoutLogService.save(checkoutLog);
                LOGGER.info("[Checkout] RESPONSE 1 " + paymentResult);

                //更新aes key
                if (StringUtils.isNotEmpty(ezCardBasicResponse.getNewAESKey())) {
                    device.setAesKey(ezCardBasicResponse.getNewAESKey());
                }
                // 如果來源為 Trade Detail && batchNo 和當天相同 則 + 1
                updateBatchNo(checkoutCtrlVo, checkoutCtrl, device, checkOutObj);
                deviceService.save(device);
            } catch (Exception e) {
                LOGGER.error("[Checkout][EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));

                // 新增ctrl 檔 (status 為 ERR)
                checkoutCtrlVo.setStatus(ERR);
                INT_TB_CheckoutCtrl errorCheckoutCtrl = parseCheckoutCtrl(checkoutCtrlVo);
                INT_TB_CheckoutCtrlService.save(errorCheckoutCtrl);

                // 新增ctrl log 檔 (status 為 ERR)
                INT_TB_CheckoutLog errorCheckoutLog = parseCheckoutLog(checkoutCtrlVo);
                INT_TB_CheckoutLogService.save(errorCheckoutLog);
            }
        }
        // 返回結帳結果
        if (checkoutCtrlVos.size() > 0) {
            LOGGER.info("[Checkout] end 1");
            return paymentResult;
        } else {
            try {
                paymentResult = getNoCtrlResult(requestHeader);
                LOGGER.info("[Checkout] RESPONSE 2 " + paymentResult);
            } catch (Exception e) {
                LOGGER.error("[Checkout][EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
            }
            LOGGER.info("[Checkout] end 2");
            return paymentResult;
        }
    }

    private Device prepareDevice(Map<String, Object> checkOutObj) {
        List<TradeDetailVo> tdList = (List<TradeDetailVo>) checkOutObj.get(TRADE_DETAILS);
        List<ReserveDetail> reDList = (List<ReserveDetail>) checkOutObj.get(RESERVE_DETAILS);
        List<RefundDetailVo> refundDList = (List<RefundDetailVo>) checkOutObj.get(REFUND_DETAILS);
        Device device = null;
        if (tdList.size() > 0) {
            device = deviceService.getOneByTDOrderId(tdList.get(0).getOrderId());
        } else if (reDList.size() > 0) {
            device = deviceService.getOneByRDOrderId(reDList.get(0).getReserveOrderId());
        } else if (refundDList.size() > 0) {
            device = deviceService.getOneByRefundOrderId(refundDList.get(0).getOrderId());
        }
        checkOutObj.put(DEVICE, device);
        return device;
    }

    private CheckoutCustomService prepareCheckoutData(RequestHeader requestHeader, CheckoutCtrlVo checkoutCtrlVo,
                                                      Map<String, Object> checkOutObj) {
        CheckoutCustomService checkoutCustomService;
        if (checkoutCtrlVo.getType().equals(TAXI_TYPE)) {
            requestHeader.setServiceType("SettlementForTaxi");
            checkoutCustomService = checkoutTaxiServiceImpl;
        } else if (checkoutCtrlVo.getType().equals(EZC_TYPE)) {
            requestHeader.setServiceType("Settlement");
            checkoutCustomService = checkoutSmallAmountServiceImpl;
        } else if (checkoutCtrlVo.getType().equals(TRAFFIC_TYPE)) {
            requestHeader.setServiceType("Settlement");
            checkoutCustomService = checkoutTraffixServiceImpl;
        } else {
            requestHeader.setServiceType("Settlement");
            checkoutCustomService = checkoutSmallAmountServiceImpl;
        }

        checkOutObj.put(REQUEST_HEADER, requestHeader);
        checkOutObj.put(CHECKOUT_CTRL_VO, checkoutCtrlVo);
        checkOutObj.put(BATCH_NO, checkoutCtrlVo.getBatchNo());

        Merchant merchant = merchantService.getOne(checkoutCtrlVo.getAccountId());
        checkOutObj.put(MERCHANT, merchant);

        PaymentAccount paymentAccount = paymentAccountService.getOne(merchant.getAccountId(), 18);
        checkOutObj.put(PAYMENT_ACCOUNT, paymentAccount);
        return checkoutCustomService;
    }

    private String getNoCtrlResult(RequestHeader requestHeader) throws JSONException {
        String paymentResult;
        JSONObject jsonObject = new JSONObject();
        JSONObject dataJsonObject = new JSONObject();
        dataJsonObject.put("TXNResult", SUCCESS);
        dataJsonObject.put("ErrorCode", SUCCESS_CODE);
        dataJsonObject.put("TxPointAmount", 0);
        dataJsonObject.put("TxRefundAmount", 0);
        dataJsonObject.put("TxCountTripForDriver", 0);
        dataJsonObject.put("TxTradeAmount", 0);
        dataJsonObject.put("TxReserveAmount", 0);
        dataJsonObject.put("TxCancelAmount", 0);
        dataJsonObject.put("TxPointAmount", 0);
        dataJsonObject.put("TxTradeCount", 0);
        dataJsonObject.put("TxReserveCount", 0);
        dataJsonObject.put("TxCancelCount", 0);
        dataJsonObject.put("TxRefundCount", 0);

        JSONObject headerJsonObject = new JSONObject();
        headerJsonObject.put("StatusCode", SystemInstance.STATUS_SUCCESS);
        headerJsonObject.put("StatusDesc", "STATUS_SUCCESS");
        headerJsonObject.put("Method", requestHeader.getMethod());
        headerJsonObject.put("ServiceType", requestHeader.getServiceType());
        headerJsonObject.put("MchId", requestHeader.getMerchantId());
        headerJsonObject.put("ResponseTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));

        jsonObject.put("Data", dataJsonObject);
        jsonObject.put("Header", headerJsonObject);
        paymentResult = jsonObject.toString();
        return paymentResult;
    }

    private INT_TB_CheckoutCtrl parseCheckoutCtrl(CheckoutCtrlVo checkoutCtrlVo) {
        INT_TB_CheckoutCtrl checkoutCtrl = new INT_TB_CheckoutCtrl();
        checkoutCtrl.setCcSeqId(checkoutCtrlVo.getCcSeqId().intValue());
        checkoutCtrl.setAccountId(checkoutCtrlVo.getAccountId());
        checkoutCtrl.setBatchNo(checkoutCtrlVo.getBatchNo());
        checkoutCtrl.setComment(checkoutCtrlVo.getComment());
        checkoutCtrl.setOwnerDeviceId(checkoutCtrlVo.getOwnerDeviceId());
        checkoutCtrl.setType(checkoutCtrlVo.getType());
        checkoutCtrl.setStatus(checkoutCtrlVo.getStatus());
        checkoutCtrl.setCrDate(checkoutCtrlVo.getCrDate());
        checkoutCtrl.setMethod(checkoutCtrlVo.getMethod());
        checkoutCtrl.setDateStamp(new Date());
        return checkoutCtrl;

    }

    private INT_TB_CheckoutLog parseCheckoutLog(CheckoutCtrlVo checkoutCtrlVo) {
        INT_TB_CheckoutLog checkoutLog = new INT_TB_CheckoutLog();
        checkoutLog.setCcSeqId(checkoutCtrlVo.getCcSeqId().intValue());
        checkoutLog.setAccountId(checkoutCtrlVo.getAccountId());
        checkoutLog.setBatchNo(checkoutCtrlVo.getBatchNo());
        checkoutLog.setComment(checkoutCtrlVo.getComment());
        checkoutLog.setOwnerDeviceId(checkoutCtrlVo.getOwnerDeviceId());
        checkoutLog.setStatus(checkoutCtrlVo.getStatus());
        checkoutLog.setCrDate(new Date());
        checkoutLog.setDateStamp(new Date());
        return checkoutLog;
    }

    private void updateBatchNo(CheckoutCtrlVo checkoutCtrlVo, INT_TB_CheckoutCtrl checkoutCtrl, Device device, Map<String, Object> checkOutObj) {
        if (checkoutCtrlVo.getSource().equals("TD")) {
            String nowString = DateTime.now().toString("yyMMdd");
            LOGGER.info("nowString: " + nowString);
            LOGGER.info("checkoutCtrlVo.getBatchNo().substring(0, 6): " + checkoutCtrlVo.getBatchNo().substring(0, 6));
            if (nowString.equals(checkoutCtrlVo.getBatchNo().substring(0, 6))) {
                long batch = device.getBatchNumber();
                if (batch >= 99) {
                    batch = 0;
                } else {
                    batch = batch + 1;
                }
                LOGGER.info("update batch: " + batch);
                device.setBatchNumber(batch);
            }
        }
    }

    private void getDetails(CheckoutCtrlVo checkoutCtrlVo, Map<String, Object> checkOutObj) {
        List<TradeDetailVo> tradeDetails = tradeDetailService.listByBatchNo(checkoutCtrlVo.getAccountId(),
                checkoutCtrlVo.getOwnerDeviceId(), checkoutCtrlVo.getBatchNo())
                .stream().filter(vo -> StringUtils.isNotEmpty(vo.getRrn())).collect(Collectors.toList());
        List<ReserveDetail> reserveDetails = reserveDetailService.listByBatchNo(checkoutCtrlVo.getAccountId(),
                checkoutCtrlVo.getOwnerDeviceId(), checkoutCtrlVo.getBatchNo())
                .stream().filter(vo -> StringUtils.isNotEmpty(vo.getRrn())).collect(Collectors.toList());
        List<RefundDetailVo> refundDetails = refundDetailService.listByBatchNo(checkoutCtrlVo.getAccountId(),
                checkoutCtrlVo.getOwnerDeviceId(), checkoutCtrlVo.getBatchNo())
                .stream().filter(vo -> StringUtils.isNotEmpty(vo.getRrn())).collect(Collectors.toList());
        List<ReserveCancel> reserveCancels = reserveCancelService.listByMerchantIdAndDeviceIdAndBatchNo(
                checkoutCtrlVo.getAccountId(), checkoutCtrlVo.getOwnerDeviceId(), checkoutCtrlVo.getBatchNo())
                .stream().filter(vo -> StringUtils.isNotEmpty(vo.getRrn())).collect(Collectors.toList());
        LOGGER.info("[CHECKOUT][tradeDetails] size= " + tradeDetails.size());
        LOGGER.info("[CHECKOUT][reserveDetails] size= " + reserveDetails.size());
        LOGGER.info("[CHECKOUT][refundDetails] size= " + refundDetails.size());
        LOGGER.info("[CHECKOUT][reserveCancels] size= " + reserveCancels.size());
        final String method = "31800";
        checkOutObj.put(TRADE_DETAILS, tradeDetails.parallelStream().filter(tradeDetailVo -> method.equals(tradeDetailVo.getMethod())).collect(Collectors.toList()));
        checkOutObj.put(RESERVE_DETAILS, reserveDetails.parallelStream().filter(reserveDetail -> method.equals(reserveDetail.getMethod())).collect(Collectors.toList()));
        checkOutObj.put(REFUND_DETAILS, refundDetails.parallelStream().filter(refundDetailVo -> method.equals(refundDetailVo.getMethod())).collect(Collectors.toList()));
        checkOutObj.put(RESERVE_CANCELS, reserveCancels.parallelStream().filter(reserveCancel -> method.equals(reserveCancel.getMethod())).collect(Collectors.toList()));
    }

    private void setMaxTimeOfDay(Calendar c) {
        c.set(Calendar.HOUR_OF_DAY, c.getMaximum(Calendar.HOUR_OF_DAY));
        c.set(Calendar.MINUTE, c.getMaximum(Calendar.MINUTE));
        c.set(Calendar.SECOND, c.getMaximum(Calendar.SECOND));
        c.set(Calendar.MILLISECOND, c.getMaximum(Calendar.MILLISECOND));
    }

    private void setMinTimeOfDay(Calendar c) {
        c.set(Calendar.HOUR_OF_DAY, c.getMinimum(Calendar.HOUR_OF_DAY));
        c.set(Calendar.MINUTE, c.getMinimum(Calendar.MINUTE));
        c.set(Calendar.SECOND, c.getMinimum(Calendar.SECOND));
        c.set(Calendar.MILLISECOND, c.getMinimum(Calendar.MILLISECOND));
    }

    @Async
    private void writeDBLog(String merchantId, String serviceType, String method, String instruction, String response) {
        OperationHistory operationHistory = new OperationHistory();
        operationHistory.setServiceType(serviceType);
        operationHistory.setMethod(method);
        operationHistory.setMerchantId(merchantId);
        operationHistory.setInstruction(instruction);
        operationHistory.setResponse(response);
        operationHistoryService.save(operationHistory);
    }

    public String doSendFailEmail() {
        JSONObject jsonObject = new JSONObject();
        try {
            Integer countDateStamp = checkoutRepository.countDateStamp();
            List<IntTbLookupCode> lookupCodes = lookupCodeService.findAllByLookType(CHECKOUT_EMAIL);
            List<IntTbLookupCode> lookupCodesStatus = lookupCodeService.findAllByLookType(CHECKOUT_STATUS);
            for (IntTbLookupCode lookupCode : lookupCodes) {
                String[] arr = lookupCode.getLookupCode().split(";");
                List<INT_TB_CheckoutCtrl> list = new ArrayList<INT_TB_CheckoutCtrl>();
                for (String item : arr) {
                    list.addAll(checkoutRepository.getCheckoutList(item));
                }
                if (CollectionUtils.isEmpty(list))
                    continue;
                for (INT_TB_CheckoutCtrl item : list) {
                    for (IntTbLookupCode status : lookupCodesStatus) {
                        if (status.getLookupCode().equals(item.getStatus())) {
                            item.setStatus(status.getDscr());
                            break;
                        }
                    }
                }
                MailInformation mailInformation = new MailInformation();
                mailInformation.setMailTo(lookupCode.getType1().split(";"));
                mailInformation.setSubject("ezc settlement failed");
                Context ctx = new Context();
                ctx.setVariable("checkoutList", list);
                ctx.setVariable("checkoutTitle", lookupCode.getDscr());
                mailService.send(mailInformation, ctx, "checkoutNotifyMail");
                jsonObject.put(lookupCode.getDscr(), list);
                jsonObject.put(lookupCode.getDscr() + "email", Arrays.asList(arr));

                if (countDateStamp < 50) {
                    mailInformation.setSubject("結帳排程異常");
                    ctx.clearVariables();
                    ctx.setVariable("message", "結帳排程異常");
                    mailService.send(mailInformation, ctx);
                }
            }
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
        }
        return jsonObject.toString();
    }

    @Override
    public Map<String, Object> listByAccountId(CheckoutCtrlRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            String checkoutString = HttpRequestUtil.post(DAO_URL + "api/CheckoutCtrl/listByAccountId", new Gson().toJson(request));
            String statusLookupCode = HttpRequestUtil.httpGet(DAO_URL + "api/lookupCode/findAllByLookupType/checkoutstatus");
            JSONArray statusJsonArray = new JSONArray(statusLookupCode);
            JSONArray jsonArray = new JSONArray(checkoutString);
            List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
            for (int i = 0; i < jsonArray.length(); i++) {
                Map<String, Object> resultMap = new HashMap<String, Object>();
                JSONObject checkout = jsonArray.getJSONObject(i);
                String status = checkout.getString("status");
                for (int j = 0; j < statusJsonArray.length(); j++) {
                    JSONObject statusObj = statusJsonArray.getJSONObject(j);
                    if (statusObj.getString("lookupCode").equals(status)) {
                        resultMap.put("statusDscr", statusObj.getString("dscr"));
                        resultMap.put("checkoutButton", statusObj.getInt("type1"));
                        break;
                    }
                }
                resultMap.put("status", status);
                resultMap.put("batchNo", checkout.getString("batchNo"));
                resultMap.put("accountId", checkout.getString("accountId"));
                resultMap.put("tradeAmount", checkout.getString("tradeAmount"));
                resultMap.put("refundAmount", checkout.getString("refundAmount"));
                resultMap.put("dateStamp", checkout.getString("dateStamp"));
                resultMap.put("ownerDeviceId", checkout.getString("ownerDeviceId"));
                resultList.add(resultMap);
            }
            LOGGER.error(resultList.toString());
            map.put("data", resultList);
            map.put("msg", "");
            map.put("msgCode", "0000");
            return map;
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] listByAccountId " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            map.put("data", null);
            map.put("msg", e.getMessage());
            map.put("msgCode", "9999");
            return map;
        }
    }
}
