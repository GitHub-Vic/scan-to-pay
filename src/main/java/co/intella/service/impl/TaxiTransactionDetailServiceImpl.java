package co.intella.service.impl;

import co.intella.model.TaxiTransactionDetail;
import co.intella.net.HttpRequestUtil;
import co.intella.service.TaxiTransactionDetailService;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaxiTransactionDetailServiceImpl implements TaxiTransactionDetailService {

    @Value("${host.dao}")
    private String DAO_URL;

    @Override
    public int countAbnormalRule001(String mins, String licensePlate, String cardId) {

        int count = 0;
        try {
            count = Integer.valueOf(HttpRequestUtil.httpGet(DAO_URL + "api/TaxiTransactionDetail/countAbnormalRule001/" + licensePlate + "/" + cardId + "/" + mins));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return count;
    }

    @Override
    public int countAbnormalRule002(String licensePlate, String cardId) {
        int count = 0;
        try {
            count = Integer.valueOf(HttpRequestUtil.httpGet(DAO_URL + "api/TaxiTransactionDetail/countAbnormalRule002/" + licensePlate + "/" + cardId));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return count;
    }

    @Override
    public int countAbnormalRule003(String cycleTime, String licensePlate, String cardId) {
        int count = 0;
        try {
            count = Integer.valueOf(HttpRequestUtil.httpGet(DAO_URL + "api/TaxiTransactionDetail/countAbnormalRule003/" + licensePlate + "/" + cardId + "/" + cycleTime));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return count;
    }

    @Override
    public List<TaxiTransactionDetail> getCycleList(String startDate, String endDate, String licensePlate, String cardId, String terminalId) {
        List<TaxiTransactionDetail> list = null;
        try {

            String url = DAO_URL + "api/TaxiTransactionDetail/getCycleList/" + startDate + "/" + endDate
                    + "?licensePlate=" + StringUtils.defaultIfBlank(licensePlate, "")
                    + "&cardId=" + StringUtils.defaultIfBlank(cardId, "")
                    + "&terminalId=" + StringUtils.defaultIfBlank(terminalId, "");
            String responseEntity = HttpRequestUtil.httpGet(url);

            list = new ObjectMapper().readValue(responseEntity, new TypeReference<List<TaxiTransactionDetail>>() {
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public TaxiTransactionDetail save(String taxiTransactionDetail) {
        TaxiTransactionDetail taxiTransactionDetail1 = null;
        try {
            String result = HttpUtil.post(DAO_URL + "api/TaxiTransactionDetail/save", taxiTransactionDetail);
            taxiTransactionDetail1 = JsonUtil.parseJson(result, TaxiTransactionDetail.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taxiTransactionDetail1;
    }
}
