package co.intella.service.impl;

import co.intella.model.TradeDetailOtherInfo;
import co.intella.service.TradeDetailOtherInfoService;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

@Service
public class TradeDetailOtherInfoServiceImpl implements TradeDetailOtherInfoService {

    private final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Value("${host.dao}")
    private String DAO_URL;

    @Autowired
    private EntityManager entityManager;

    @Override
    public TradeDetailOtherInfo getOneByUNHEXUuid(String uuid) {
        try {
            String response = HttpUtil.doGet(DAO_URL + "api/tradeOtherInfo/uuid/" + uuid);
            return JsonUtil.parseJson(response, TradeDetailOtherInfo.class);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] save() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    @Override
    public String getCardIdByCardNumberForPrint(String cardNumberForPrint) {
        try {
            String response = HttpUtil.doGet(DAO_URL + "api/tradeOtherInfo/getCardIdByCardNumberForPrint/" + cardNumberForPrint);
            return response;
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] save() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    @Override
    public TradeDetailOtherInfo save(TradeDetailOtherInfo tradeDetailOtherInfo) {

        try {
            String response = HttpUtil.doPostJson(DAO_URL + "api/tradeOtherInfo/save", URLEncoder.encode(JsonUtil.toJsonStr(tradeDetailOtherInfo), "UTF-8"));
            return JsonUtil.parseJson(response, TradeDetailOtherInfo.class);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] save() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }

    }

    @Override
    public String getCardIdByLikeCardNumberForPrint(String cardNumberForPrint) {
        String sql = "SELECT tdof.cardId FROM allpaypass_new.INT_TB_TradeDetailOtherInfo tdof WHERE tdof.cardNumberForPrint like  CONCAT(:cardNumberForPrint,'%')";
        Query query = entityManager.createNativeQuery(sql)
                .setParameter("cardNumberForPrint", cardNumberForPrint);
        query.unwrap(SQLQuery.class).setResultTransformer(Transformers.aliasToBean(TradeDetailOtherInfo.class));
        List<TradeDetailOtherInfo> resultList = query.getResultList();

        return CollectionUtils.isNotEmpty(resultList) ? resultList.get(0).getCardId() : null;
    }
}
