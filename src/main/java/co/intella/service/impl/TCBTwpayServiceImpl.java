package co.intella.service.impl;


import co.intella.constant.Bank;
import co.intella.domain.twpay.BarCodeData;
import co.intella.domain.twpay.TwPayRequestData;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import co.intella.service.PaymentAccountService;
import co.intella.service.TCBTwpayService;
import co.intella.service.TwPayService;
import co.intella.utility.SystemInstance;
import co.intella.utility.TWPayUtil;
import com.google.gson.Gson;
import com.google.gson.JsonPrimitive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.xml.bind.DatatypeConverter;
import java.util.Base64;
import java.util.Objects;

@Service
public class TCBTwpayServiceImpl implements TCBTwpayService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Resource
    private TwPayService twPayService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Override
    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {

        /**
         *  合作金庫    ，因被掃有區分金融卡 要走上海、信勇卡要直接打財金故 另外寫service 判斷
         *  要改走上海還是繼續合庫
         *
         */

        //上海
        PaymentAccount twPaymentAccount = paymentAccountService.getOne(requestHeader.getMerchantId(), Bank.TWPAY.getId());

        if (SystemInstance.TYPE_MICROPAY.equals(requestHeader.getServiceType())) {

            TwPayRequestData twPayRequestData = new Gson().fromJson(data.getAsString(), TwPayRequestData.class);
            String authCode = twPayRequestData.getAuthCode();
            boolean isQrCode = !authCode.matches("^9[56].{20}$");  // 金融卡才有qrCode
            if (isQrCode && Objects.nonNull(twPaymentAccount)) {
                // qrCode 的話 有上海走上海，沒有則不動(走合庫)
                requestHeader.setMethod("12000");
            } else if (!isQrCode) {
                Base64.Decoder decoder = Base64.getDecoder();
                byte[] authCodeBtye = decoder.decode(authCode.substring(2));
                String hexStr = DatatypeConverter.printHexBinary(authCodeBtye);
                if (hexStr.startsWith("0") && Objects.nonNull(twPaymentAccount)) {
                    // 版本0 為金融卡 獨有 有上海走上海
                    requestHeader.setMethod("12000");
                } else if (hexStr.startsWith("1")) {
                    String item3 = hexStr.substring(24);
                    if (item3.startsWith("0") && Objects.nonNull(twPaymentAccount)) {
                        //保留碼2碼  開頭 1 或 2 為信用卡(走合庫)，0為金融卡(有上海走上海)
                        requestHeader.setMethod("12000");
                    }
                }
            }
        }

        return twPayService.doRequest(requestHeader, data);
    }
}
