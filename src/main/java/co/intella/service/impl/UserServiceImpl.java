package co.intella.service.impl;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.security.auth.login.LoginException;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import co.intella.model.IntTbLookupCode;
import co.intella.model.Token;
import co.intella.model.User;
import co.intella.net.HttpRequestUtil;
import co.intella.service.LookupCodeService;
import co.intella.service.UserService;
import co.intella.utility.HttpUtils;

@Service
public class UserServiceImpl implements UserService{
	
	private Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	@Resource
	private LookupCodeService lookupCodeService;
	
	@Autowired
    private MessageSource messageSource;
    
     private Locale locale = new Locale("zh_TW");

    @Value("${host.dao}")
    private String DAO_URL;
    
	@Override
	public Map<String, Object> getToken(String clientId, String code) {
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> oauthMsg = new HashMap<String, Object>();
		String authServerDomainName = null;
		String oauth2ClientSecret = null;
		try {
			IntTbLookupCode oauth2Server = lookupCodeService.findOne("oauth2Server", "allpaypass_server");
			authServerDomainName = oauth2Server.getType1();
			if (clientId == null) {
				clientId = oauth2Server.getLookupCode();
			}
			oauth2ClientSecret = oauth2Server.getType2();
			String encoding = new String(Base64.encodeBase64((clientId + ":" + oauth2ClientSecret).getBytes()));
			String secret = "Basic " + encoding;
			String url = authServerDomainName + "oauth/token";
			Map<String, String> requestProperties = new HashMap<>();
			requestProperties.put("Content-Type", "application/x-www-form-urlencoded");
			requestProperties.put(HttpHeaders.AUTHORIZATION, secret);
			String response = null;
			try {
				response = HttpUtils.doPost(url, requestProperties, "grant_type=authorization_code&code=" + code);
			}catch (IOException e) {
				LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
				oauthMsg.put("oauth2ServerDomain", authServerDomainName);
				oauthMsg.put("oauth2ClientId", clientId);
				oauthMsg.put("oauth2ClientSecret", oauth2ClientSecret);
				result.put("msg", messageSource.getMessage("co.intella.auth.not.auth", null, locale));
				result.put("msgCode", "9997");
				result.put("data", oauthMsg);
				return result;
			} 
			JSONObject jsonObject =  new JSONObject(response);
			String userId = checkToken(jsonObject.getString("access_token"));
			String accountId = this.getUserInfo(jsonObject.getString("access_token"));
			User user = new User();
			user.setUserId(userId);
			user.setAccountId(accountId);
			String userResponse = HttpRequestUtil.post(DAO_URL + "api/user/userId", new Gson().toJson(user));
	        LOGGER.info("[UserServiceImpl][RESPONSE] userResponse " + userResponse);
	        JSONObject userJSON = new JSONObject(userResponse);
			if (userJSON.getString("userId").equals("null")) {
				LOGGER.info("[UserServiceImpl][RESPONSE] user insert request " + user);
				String count = HttpRequestUtil.post(DAO_URL + "api/user/insert", new Gson().toJson(user));
				LOGGER.info("[UserServiceImpl][RESPONSE] count " + count);
			}
			Token newToken = new Token();
			newToken.setUserId(userId);
			newToken.setTokenId(jsonObject.getString("access_token"));
			newToken.setRefreshToken(jsonObject.getString("refresh_token"));
			HttpRequestUtil.post(DAO_URL + "api/token/insert", new Gson().toJson(newToken));
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("tokenId", jsonObject.getString("access_token"));
			data.put("userId", userId);
			result.put("data", data);
			result.put("msgCode", "0000");
			result.put("msg", "");
		} catch (Exception e) {
	        LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
	        result.put("msg", messageSource.getMessage("co.intella.exception.msg", null, locale));
	        result.put("msgCode", "9999");
	    }
		return result;
	}
	
	public String checkToken (String token) throws Exception {
		IntTbLookupCode oauth2Server = lookupCodeService.findOne("oauth2Server", "allpaypass_server");
		String authServerDomainName = oauth2Server.getType1();
		JSONObject jsonObject =  null;
		try {
			String response = HttpUtils.doGet(authServerDomainName + "oauth/check_token?token=" + token);
			LOGGER.info(response);
			jsonObject = new JSONObject(response);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new LoginException("co.intella.user.token.error");
		}
		return jsonObject.getString("user_name");
	}

	public String getUserInfo (String tokenId) throws Exception {
		String authServerDomainName = lookupCodeService.findOne("oauth2Server", "allpaypass_server").getType1();
		JSONObject jsonObject;
		String accountId = null;
		HttpGet get = new HttpGet(authServerDomainName + "auth/getUserInfo");
		get.setHeader(org.apache.http.HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
		get.setHeader("tokenId", tokenId);
		String responseString;
		try (CloseableHttpClient httpClient = HttpClients.createDefault();
			 CloseableHttpResponse response2 = httpClient.execute(get)) {
			responseString = EntityUtils.toString(response2.getEntity());
		}
		LOGGER.info("response = " + responseString);
		jsonObject = new JSONObject(responseString);
		if (jsonObject.has("msgCode") && "000000".equals(jsonObject.getString("msgCode"))) {
			accountId = jsonObject.getJSONObject("data").getString("accountId");
		}
		return accountId;
	}

}
