package co.intella.service.impl;

import co.intella.constant.Payment;
import co.intella.constant.ServiceType;
import co.intella.domain.focas.ApiTransType;
import co.intella.domain.focas.CreditAuthReq;
import co.intella.domain.focas.CreditCancelAuthReq;
import co.intella.domain.focas.FOCASPayResponse;
import co.intella.domain.integration.*;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import co.intella.net.HttpRequestUtil;
import co.intella.service.*;
import co.intella.utility.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.stream.Collectors;

@Service
@Transactional
public class ScsbServiceImpl implements ScsbService {

    private final Logger LOGGER = LoggerFactory.getLogger(ScsbServiceImpl.class);

    @Autowired
    private Environment env;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Value("${api.url}")
    private String API_URL;

    @Value("${payment.gateway.api.origin}")
    private String PAYMENT_GATEWAY_API_ORIGIN;

//    private String PAYMENT_GATEWAY_API_ORIGIN = "http://34.80.101.124:30391";

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private RestTemplate restTemplate;

    private final String CREDIT_METHOD = "22600";

    private final String CREDIT3D_METHOD = "23900";

    private final String UPOP_REG = "^62[0-5]\\d{13,16}$";

    @Override
    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {

        String generalResponseData = SystemInstance.EMPTY_STRING;
        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        PaymentAccount paymentAccount = paymentAccountService.getOne(integrateMchId, bankId);

        if (ServiceType.CANCEL.equals(requestHeader.getServiceType())) {

            LOGGER.info("[SCSB][Refund][data]" + data.getAsString());

            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

            JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);

            CreditCancelAuthReq creditCancelAuthReq = new CreditCancelAuthReq();

            creditCancelAuthReq.setLidm(tradeDetail.getSystemOrderId());
            String platformResponse = cancelPayment(creditCancelAuthReq, paymentAccount);

            Gson gson = new Gson();
            FOCASPayResponse focasPayResponse = gson.fromJson(platformResponse, FOCASPayResponse.class);

            ResponseGeneralHeader responseGeneralHeader = getResponseHeader(focasPayResponse, requestHeader, integrateMchId, messageSource);

            IntegratedRefundResponseData integratedRefundResponseData = getIntegratedRefundResponseData(
                    focasPayResponse, requestHeader, tradeDetail, gson.fromJson(data.getAsString(), IntegratedRefundRequestData.class));
            integratedRefundResponseData.setPlatformRsp(platformResponse);

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedRefundResponseData);

            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, platformResponse, data);

            responseGeneralHeader.setServiceType(ServiceType.REFUND);
            generalResponseData = gson.toJson(responseGeneralBody);
            LOGGER.info("integrate response ready :" + generalResponseData);
        } else if ("Payment".equals(requestHeader.getServiceType())) {
            generalResponseData = this.creditPayment(requestHeader, data);
            LOGGER.info("generalResponseData before:" + generalResponseData);
            JsonObject jsonObject = new Gson().fromJson(generalResponseData, JsonObject.class);
            JsonObject resdata = new Gson().fromJson(jsonObject.get("Data"), JsonObject.class);
            resdata.addProperty("platformRsp", resdata.get("redirectUrl").getAsString());
            resdata.remove("redirectUrl");
            jsonObject.add("Data", resdata);
            generalResponseData = new Gson().toJson(jsonObject);
            LOGGER.info("generalResponseData after:" + generalResponseData);
        }

        return generalResponseData;
    }

    @Override
    public String creditAuth(RequestHeader requestHeader, JsonPrimitive data, String integrateMchId, String clientIP)
            throws Exception {

        LOGGER.info("[SCSB][Payment][doRequest]");
        PaymentAccount paymentAccount = paymentAccountService.getOne(integrateMchId, 26);

        CreditAuthReq creditAuthReq = new Gson().fromJson(data.getAsString(), CreditAuthReq.class);
        String pan = creditAuthReq.getPan();
        String focasPayNormalizeResponse = SystemInstance.EMPTY_STRING;
        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

        if (!pan.matches(UPOP_REG)) {
            String cvv = creditAuthReq.getCvv();
            String expDate = creditAuthReq.getExpiry();

            creditAuthReq.setPan(pan.substring(0, 6) + "*************" + pan.substring(pan.length() - 4));
//            LOGGER.info("[SCSB][Payment] " + new Gson().toJson(creditAuthReq));
            creditAuthReq.setPan(pan);
            creditAuthReq.setCvv(cvv);
            creditAuthReq.setExpiry(expDate);
            creditAuthReq.setMerchantID(paymentAccount.getAccount());
            focasPayNormalizeResponse = this.pay(creditAuthReq, integrateMchId, Payment.CREDIT_CARD);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, focasPayNormalizeResponse, data);
        } else {
            focasPayNormalizeResponse = this.payUpop(creditAuthReq, paymentAccount);
        }

        return focasPayNormalizeResponse;
    }

    @Override
    public String creditAuth3D(RequestHeader requestHeader, JsonPrimitive data, String integrateMchId, String clientIP)
            throws Exception {

        LOGGER.info("[SCSB_3D][Payment][doRequest]");
        PaymentAccount paymentAccount = paymentAccountService.getOne(integrateMchId, 39);

        CreditAuthReq creditAuthReq = new Gson().fromJson(data.getAsString(), CreditAuthReq.class);

        String pan = creditAuthReq.getPan();
        String cvv = creditAuthReq.getCvv();
        String expDate = creditAuthReq.getExpiry();

        creditAuthReq.setPan(pan.substring(0, 6) + "*************" + pan.substring(pan.length() - 4));
        LOGGER.info("[SCSB_3D][Payment] " + new Gson().toJson(creditAuthReq));
        creditAuthReq.setPan(pan);
        creditAuthReq.setCvv(cvv);
        creditAuthReq.setExpiry(expDate);
        creditAuthReq.setMerchantID(paymentAccount.getAccount());

        TradeDetail scsb3dTradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
        String focasPayNormalizeResponse = pay3D(creditAuthReq, integrateMchId, scsb3dTradeDetail, Payment.CREDIT_CARD);

        return focasPayNormalizeResponse;
    }

    public String pay(CreditAuthReq request, String integrateMchId, String type) throws Exception {
        try {
            String pan = request.getPan();
            String cvv = request.getCvv();
            request.setPan(GeneralUtil.maskCreditCardNumber(pan));
            request.setCvv("***");

            String order = request.getLidm();
            if (order.length() > 19) {
                request.setLidm(order.substring(order.length() - 19));
                LOGGER.info("[SCSB][TRANSACTION]  order: "+order+" -----> "+request.getLidm());
            }

            LOGGER.info("[SCSB][TRANSACTION] start... " + new ObjectMapper().writeValueAsString(request));

            request.setPan(pan);
            request.setCvv(cvv);



            String response = SystemInstance.EMPTY_STRING;
            if (Payment.CREDIT_CARD.equals(type)) {
                request.setApiTransType(ApiTransType.CREDIT_AUTH);
                String postUrl = PAYMENT_GATEWAY_API_ORIGIN + "/focaspay/api/v1/credit/auth";
                request.setPayType("0");
                String requestJson = new ObjectMapper().writeValueAsString(request);
//                LOGGER.info("[SCSB][FOCASPAY]" + requestJson);
                response = HttpRequestUtil.post(postUrl, requestJson);
            }

            LOGGER.info("[SCSB][TRANSACTION] complete. " + response);
            return convertPaymentResponse(response, request, integrateMchId, CREDIT_METHOD, String.format("/allpaypass/api/payment/order/result?orderId=%s", EncryptUtil.encryptSimple(order)));

        } catch (JsonSyntaxException e) {
            LOGGER.error("[SCSB][EXCEPTION][JsonSyntaxException] " + e.getMessage());
            return "[EXCEPTION] jsonSyntax is wrong";
        } catch (Exception e) {
            LOGGER.error("[SCSB][EXCEPTION] " + e.getMessage());
            return "[EXCEPTION] post to scsb wrong";
        }
    }

    public String pay3D(CreditAuthReq request, String integrateMchId, TradeDetail scsb3dTradeDetail, String type) throws Exception {
        try {
            String pan = request.getPan();
            String cvv = request.getCvv();
            request.setPan(GeneralUtil.maskCreditCardNumber(pan));
            request.setCvv("***");

            String orderId = request.getLidm();
            if (orderId.length() > 19) {
                request.setLidm(orderId.substring(orderId.length() - 19));
                LOGGER.info("[SCSB_3D][TRANSACTION]  orderId: "+orderId+" -----> "+request.getLidm());
            }

            LOGGER.info("[SCSB_3D][TRANSACTION] start... " + new ObjectMapper().writeValueAsString(request));

            request.setPan(pan);
            request.setCvv(cvv);




            String response = SystemInstance.EMPTY_STRING;
            if (Payment.CREDIT_CARD.equals(type)) {
                request.setApiTransType(ApiTransType.CREDIT_REDIRECT_AUTH);
                request.setPayType("0");
                request.setAuthResURL("https://a.intella.co/allpaypass/api/scsb/credit3D/Notify");
                String requestJson = new ObjectMapper().writeValueAsString(request);
                LOGGER.info("[SCSB][FOCASPAY]" + requestJson);
                response = HttpRequestUtil.post(PAYMENT_GATEWAY_API_ORIGIN + "/focaspay/api/v1/credit/redirect-auth", requestJson);
            }

            //�摮�3D撽����
            scsb3dTradeDetail.setTxParams(response);

            FOCASPayResponse responseData = new Gson().fromJson(response, FOCASPayResponse.class);
            String paypage = StringUtils.defaultIfBlank(responseData.getHtml(), "");
            if (SystemInstance.EMPTY_STRING.equals(paypage)) {
                scsb3dTradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                paypage = String.format("/allpaypass/api/payment/order/result?orderId=%s", EncryptUtil.encryptSimple(orderId));
            } else {
                paypage = this.substringForm2QueryStr(paypage);
            }
            tradeDetailService.save(scsb3dTradeDetail);

            LOGGER.info("[SCSB_3D][TRANSACTION] complete. " + response);
            return convertPaymentResponse(response, request, integrateMchId, CREDIT3D_METHOD, paypage);

        } catch (JsonSyntaxException e) {
            LOGGER.error("[SCSB_3D][EXCEPTION][JsonSyntaxException] " + e.getMessage());
            return "[EXCEPTION] jsonSyntax is wrong";
        } catch (Exception e) {
            LOGGER.error("[SCSB_3D][EXCEPTION] " + e.getMessage());
            return "[EXCEPTION] post to scsb wrong";
        }
    }

    @Override
    public String scsb3DNotify(String orderId, String errorCode) throws Exception {
        TradeDetail tradeDetail = tradeDetailService.getOneBySystemOrderId(orderId);

        if (tradeDetail != null && "00".equals(errorCode)) {
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetail.setSystemOrderId(tradeDetail.getOrderId());
            tradeDetailService.save(tradeDetail);
            appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
            //成功就轉入請款檔
            CreditAuthReq creditAuthReq = new CreditAuthReq();
            creditAuthReq.setApiTransType(6);
            creditAuthReq.setLidm(orderId);
            creditAuthReq.setMerchantID(tradeDetail.getPaymentAccount().getAccount());
            creditAuthReq.setPurchAmt(String.valueOf(Long.valueOf(tradeDetail.getPayment()).intValue()));
            String settletResp = HttpRequestUtil.post(PAYMENT_GATEWAY_API_ORIGIN + "/focaspay/api/v1/credit/settle", JsonUtil.toJsonStr(creditAuthReq));
            LOGGER.info("[scsb3DNotify] settletResp => " + settletResp);
        } else if (tradeDetail != null && !"00".equals(errorCode)) {
            if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            }
            tradeDetail.setSystemOrderId(tradeDetail.getOrderId());
            tradeDetailService.save(tradeDetail);
        }

        return String.format("/allpaypass/api/payment/order/result?orderId=%s", EncryptUtil.encryptSimple(tradeDetail.getOrderId()));
    }


    private String convertPaymentResponse(String tsResponse, CreditAuthReq request, String integrateMchId, String method, String redirectUrl) {

        FOCASPayResponse fOCASPayResponse = new Gson().fromJson(tsResponse, FOCASPayResponse.class);

        ResponseGeneralHeader responseHeader = new FocasUtil().getFOCASPayResponseHeader(fOCASPayResponse, messageSource);

        responseHeader.setMchId(integrateMchId);
        responseHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        responseHeader.setServiceType("Payment");
        responseHeader.setMethod(method);

        IntegratedPaymentResponseData responseData = new IntegratedPaymentResponseData();
        responseData.setSysOrderNo(request.getLidm());
        responseData.setStoreOrderNo(request.getLidm());
        responseData.setTotalFee(request.getPurchAmt());
        responseData.setFeeType("TWD");
        responseData.setDeviceInfo("DeviceMiles");
        responseData.setBody("");
        responseData.setPlatformRsp(tsResponse);
        responseData.setRedirectUrl(redirectUrl);
        ResponseGeneralBody responseBody = new ResponseGeneralBody();
        responseBody.setHeader(responseHeader);
        responseBody.setData(responseData);

        return new Gson().toJson(responseBody);
    }

    private String cancelPayment(CreditCancelAuthReq creditCancelAuthReq, PaymentAccount paymentAccount) {

        creditCancelAuthReq.setApiTransType(ApiTransType.CREDIT_CANCEL);
        creditCancelAuthReq.setMerchantID(paymentAccount.getAccount());
        creditCancelAuthReq.setLidm(creditCancelAuthReq.getLidm());

        LOGGER.error("[Request][SCSB_3D][Refund] " + creditCancelAuthReq.toString());

        return restTemplate.postForObject(PAYMENT_GATEWAY_API_ORIGIN + "/focaspay/api/v1/credit/cancel", creditCancelAuthReq, String.class);
    }

    private static ResponseGeneralHeader getResponseHeader(FOCASPayResponse responseObj, RequestHeader requestHeader,
                                                           String integrateMchId, MessageSource messageSource) {

        ResponseGeneralHeader responseHeader = new FocasUtil().getFOCASPayResponseHeader(responseObj, messageSource);
        responseHeader.setMethod("22600");
        responseHeader.setMchId(integrateMchId);
        responseHeader.setServiceType(requestHeader.getServiceType());
        responseHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        return responseHeader;
    }

    private static IntegratedRefundResponseData getIntegratedRefundResponseData(FOCASPayResponse responseObj, RequestHeader requestHeader, TradeDetail tradeDetail, IntegratedRefundRequestData integratedRefundRequestData) {

        IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();
        if ("0000".equals(responseObj.getErrCode())) {
            integratedRefundResponseData.setSysRefundNo(responseObj.getLidm());
            integratedRefundResponseData.setRefundedAt(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        }

        integratedRefundResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
        integratedRefundResponseData.setStoreRefundNo(integratedRefundRequestData.getOutRefundNo());
        integratedRefundResponseData.setStoreOrderNo(tradeDetail.getOrderId());

        return integratedRefundResponseData;
    }

    /**
     * 銀聯   20200102
     *
     * @param creditAuthReq
     * @param paymentAccount
     * @return
     */
    private String payUpop(CreditAuthReq creditAuthReq, PaymentAccount paymentAccount) throws Exception {

        CreditAuthReq _creditAuthReq = new CreditAuthReq();
        _creditAuthReq.setApiTransType(ApiTransType.UPOP_AUTH);
        _creditAuthReq.setMerchantID(paymentAccount.getAccount());
        _creditAuthReq.setLidm(creditAuthReq.getLidm());
        _creditAuthReq.setPurchAmt(creditAuthReq.getPurchAmt());
        _creditAuthReq.setMerchantName(paymentAccount.getMerchant().getAccountId());
        _creditAuthReq.setAuthResURL(API_URL + "allpaypass/api/scsb/upopNotify");

        String reqJsonStr = JsonUtil.getGson().toJson(_creditAuthReq);
        LOGGER.info("[SCSB][payUpop] reqJsonStr =>" + reqJsonStr);
        String result = HttpRequestUtil.post(PAYMENT_GATEWAY_API_ORIGIN + "/focaspay/api/v1/upop/auth", reqJsonStr);
        LOGGER.info("[SCSB][payUpop] result =>" + result);
        FOCASPayResponse focasPayResponse = JsonUtil.parseJson(result, FOCASPayResponse.class);

        String paypage = StringUtils.defaultIfBlank(focasPayResponse.getHtml(), "");
        TradeDetail tradeDetail = tradeDetailService.getOne(_creditAuthReq.getLidm());

        if (SystemInstance.EMPTY_STRING.equals(paypage)) {
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            paypage = String.format("/allpaypass/api/payment/order/result?orderId=%s", EncryptUtil.encryptSimple(tradeDetail.getOrderId()));
        } else {
            tradeDetail.setTradeToken("UPOP");
            paypage = this.substringForm2QueryStr(paypage);
        }
        tradeDetailService.save(tradeDetail);
        return convertPaymentResponse(result, _creditAuthReq, paymentAccount.getMerchant().getAccountId(), CREDIT_METHOD, paypage);
    }

    private String substringForm2QueryStr(String htmlStr) {
        htmlStr = htmlStr.replaceAll("\\\\t", "").replaceAll("\\s", "");
        String form = htmlStr.substring(htmlStr.indexOf("<form"), htmlStr.lastIndexOf("form>")).replaceAll("\r\n", "");

        LOGGER.info("[substringForm2QueryStr] form = " + form);

        String action = form.replaceAll(".*action=['\"]?([^'\"]*)['\"]?/?>.*", "$1");
        LOGGER.info("[substringForm2QueryStr] action = " + action);

        String[] inputs = form.split("<input");
        final String regexStr = ".*name=['\"]?(\\w*)['\"]?value=['\"]?([^'\"]*)['\"]?/?>.*";
        String queryStr = Arrays.stream(inputs).filter(s -> s.contains("name=") && s.contains("value=")).map(s -> {
            try {
                return s.replaceAll(regexStr, "$1") + "=" + URLEncoder.encode(s.replaceAll(regexStr, "$2"), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return "";
            }
        }).collect(Collectors.joining("&"));

        LOGGER.info("[substringForm2QueryStr] queryStr = " + queryStr);
        return action + "?" + queryStr;
    }


    public String creditPayment(RequestHeader requestHeader, JsonPrimitive data) throws IOException {

        String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
//        String clientIP = request.getHeader("X-FORWARDED-FOR");

        JsonObject requestJson = new Gson().fromJson(data.getAsString(), JsonObject.class);

        String integrateMchId = requestHeader.getMerchantId();

        if (requestJson.has("Body")) {
            String bodyRandom = requestJson.get("Body").getAsString();
            if ("Payment".equals(requestHeader.getServiceType())) {
                bodyRandom += requestJson.has("CardId") ? "-" + requestJson.get("CardId").getAsString().substring(12) : "";
            }
            requestJson.remove("Body");
            requestJson.addProperty("Body", bodyRandom);
            data = new JsonPrimitive(new Gson().toJson(requestJson));
        }

        LOGGER.debug("[dispatch credit][scsb][" + sessionId + "]");

        String generalResponseData = null;

        try {
            if ("22600".equals(requestHeader.getMethod())) {
                generalResponseData = this.creditAuth(requestHeader, data, integrateMchId, "");
            } else if ("23900".equals(requestHeader.getMethod())) {
                generalResponseData = this.creditAuth3D(requestHeader, data, integrateMchId, "");
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("[Exception][" + requestHeader.getMethod() + "]" + e.getMessage());
        }

        LOGGER.debug("[RESPONSE][" + requestHeader.getMethod() + "][" + sessionId + "]" + generalResponseData);

        return generalResponseData;
    }
}
