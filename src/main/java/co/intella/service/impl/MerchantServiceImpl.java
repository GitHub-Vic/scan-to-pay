package co.intella.service.impl;

import co.intella.domain.contratStore.MerchantAppLogin;
import co.intella.model.Merchant;
import co.intella.model.QMerchant;
import co.intella.net.HttpRequestUtil;
import co.intella.repository.MerchantRepository;
import co.intella.schedule.AutoRefundSchedule;
import co.intella.service.MerchantService;
import co.intella.utility.JsapiParamMediator;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author Miles Wu
 */
@Service
@Transactional
public class MerchantServiceImpl implements MerchantService {

    private final Logger LOGGER = LoggerFactory.getLogger(MerchantServiceImpl.class);
    
    @Value("${host.dao}")
    private String SCAN_2_PAY_DATA_DOMAIN;
    
    @Autowired
    private MessageSource messageSource;
    
     private Locale locale = new Locale("zh_TW");
    
    @Resource
    private MerchantRepository merchantRepository;

    private JsapiParamMediator jsapiParamMediator = new JsapiParamMediator();

    @Value("${host.dao}")
    private String DAO_URL;

    public List<Merchant> listByHierarchy(String hierarchy) {

        LOGGER.info("listByHierarchy() : " + hierarchy);
        try {
            String response = HttpRequestUtil.httpGet(DAO_URL + "api/merchant/list/"+hierarchy);
            return new Gson().fromJson(response, new TypeToken<List<Merchant>>() {}.getType());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public List<Merchant> queryWithLike(String accountId, String name) {
        QMerchant qMerchant = QMerchant.merchant;
        BooleanExpression predicate = qMerchant.accountId.like('%' + accountId + '%')
                .and(qMerchant.name.like('%' + name + '%'));
        return Lists.newArrayList(merchantRepository.findAll(predicate));
    }

    public Merchant getOne(long id) {
        QMerchant qMerchant = QMerchant.merchant;
        BooleanExpression predicate = qMerchant.merchantSeqId.eq(id);
        return merchantRepository.findOne(predicate);
    }

    public Merchant getOne(String accountId){
        QMerchant qMerchant = QMerchant.merchant;
        BooleanExpression predicate = qMerchant.accountId.eq(accountId).and(
                qMerchant.status.eq("active")
        );
        return merchantRepository.findOne(predicate);
    }

    public MerchantAppLogin getLoginMerchantByLoginId(String loginId) {
        MerchantAppLogin entity = null;
        try {
            String response= HttpRequestUtil.httpGet(DAO_URL + "api/merchant/getLoginMerchantByLoginId/"+loginId);
            entity = new Gson().fromJson(response, MerchantAppLogin.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entity;
    }

	@Override
	public JSONObject getAppImg(String loginId) {
        try {
            String response= HttpRequestUtil.httpGet(DAO_URL + "api/merchant/getAppImg/"+loginId);
            return new JSONObject(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject();
	}

    public Merchant getOneByLoginId(String loginId) {
        Merchant entity = null;
        try {
            String response= HttpRequestUtil.httpGet(DAO_URL + "api/merchant/getOneByLoginId/"+loginId);
            entity = new Gson().fromJson(response, Merchant.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entity;
    }

    public List<Merchant> listAll() {
        return merchantRepository.findAll();
    }

    public Merchant create(Merchant merchant) {
        return merchantRepository.save(merchant);
    }

    public void editPassword(Merchant merchant){
        merchantRepository.save(merchant);
    }

    public Iterable<Merchant> findBy(Predicate predicate) {
        return merchantRepository.findAll(predicate);
    }

    public Page findAll(Pageable pageable) {
        return merchantRepository.findAll(pageable);
    }

    public long count(Predicate predicate) {
        return merchantRepository.count(predicate);
    }

    public long count() {
        return merchantRepository.count();
    }

    public boolean activateAccount(String accountId, String activateToken) {
        QMerchant qMerchant = QMerchant.merchant;
        BooleanExpression predicate = qMerchant.accountId.eq(accountId).and(
                qMerchant.activateToken.eq(activateToken)
        );
        Merchant merchant = merchantRepository.findOne(predicate) ;
        if ( merchant == null)
            return false;
        else {
            merchant.setStatus("active");
            merchantRepository.save(merchant);
            return true;
        }
    }

    public Merchant save(Merchant merchant) {
        return merchantRepository.save(merchant);
    }

    public void registerJsapiMediator(JsapiParamMediator jsapiParamMediator) {
        this.jsapiParamMediator = jsapiParamMediator;
    }

    public JsapiParamMediator getJsapiMediator() {
        return this.jsapiParamMediator;
    }
    
    /**
     * @author mustang
     */
    public Map<String, String> checkPin (String accountId, String pin) throws Exception{    	
		Map<String, String> generalResponse = new HashMap<String, String>();
		RestTemplate restTemplate = new RestTemplate();
		String merchantResultString = restTemplate.getForObject(SCAN_2_PAY_DATA_DOMAIN + "api/merchant/get/{accountId}", String.class, accountId);
		Merchant merchantResult = new Gson().fromJson(merchantResultString, Merchant.class);

		if (merchantResult == null || !pin.equals(merchantResult.getPin())) {
			generalResponse.put("msgCode", "0002");
			generalResponse.put("msg", messageSource.getMessage("api.merchant.checkPin.msg.0002", null, locale));
			generalResponse.put("name", "");
		} else if (pin.equals(merchantResult.getPin())) {
			generalResponse.put("msgCode", "0000");
			generalResponse.put("msg", "");
			generalResponse.put("name", merchantResult.getName());
		}else {
			throw new Exception("[SCAN2PAY_DATA /merchant/get]" + merchantResultString);
		}
    	return generalResponse;
    }

}
