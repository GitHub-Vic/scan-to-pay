package co.intella.service.impl;

import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.IntegratedSingleOrderQueryResponseData;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.domain.pi.PiBasicRequestData;
import co.intella.domain.pi.PiServerOrderQueryResponse;
import co.intella.domain.pi.PiServerRefundResponse;
import co.intella.model.RefundDetail;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import co.intella.net.HttpRequestUtil;
import co.intella.service.PiInAppService;
import co.intella.service.RefundDetailService;
import co.intella.service.TradeDetailService;
import co.intella.utility.StatusManipulation;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Locale;
import java.util.Objects;

/**
 * @author Miles
 */
@Service
@Transactional
public class PiInAppServiceImpl implements PiInAppService {

    private final Logger LOGGER = LoggerFactory.getLogger(PiInAppServiceImpl.class);

    @Value("${host.pi.request.inapp}")
    private String PI_API_URL;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private TradeDetailService tradeDetailService;

    @Resource
    private RefundDetailService refundDetailService;

     private Locale locale = new Locale("zh_TW");

    public String doRequest(PiBasicRequestData request, RequestHeader header) throws JsonProcessingException {
        String requestJson = new ObjectMapper().writeValueAsString(request);

        String type = header.getServiceType();

        try {
            LOGGER.info("[Pi][REQUEST] " + new Gson().toJson(requestJson));
            if (type.equals("Refund")) {
                String piResponse = HttpRequestUtil.post(PI_API_URL + "payments/orders/refund", requestJson);
                LOGGER.info("[Pi][RESPONSE] " + piResponse);
                return convertRefundResponse(piResponse, header);
            } else if (type.equals("SingleOrderQuery")) {
                String piResponse = HttpRequestUtil.post(PI_API_URL + "payments/orders", requestJson);
                LOGGER.info("[Pi][RESPONSE] " + piResponse);
                return convertSingleQueryResponse(piResponse, header);
            } else {
                LOGGER.info("[Pi] Method not support.");
                return SystemInstance.EMPTY_STRING;
            }

        } catch (JsonSyntaxException e) {
            LOGGER.error("[EXCEPTION] jsonSyntax is wrong", e);
            String responseData = getErrorResponse(header, requestJson, "json parsing error");
            return responseData;
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] Connect to Pi Failed.", e);
            String responseData = getErrorResponse(header, requestJson, "maybe time out");
            return responseData;
        }
    }

    private String convertRefundResponse(String piResponse, RequestHeader header) {

        PiServerRefundResponse piRefundResponseData = new Gson().fromJson(piResponse, PiServerRefundResponse.class);
        String statusCode = piRefundResponseData.getResult().equals("success") ?
                SystemInstance.STATUS_SUCCESS : StatusManipulation.getCodeOfPi(piRefundResponseData.getErrCode(), header.getServiceType());

        ResponseGeneralHeader responseHeader = new ResponseGeneralHeader();
        responseHeader.setStatusCode(statusCode);
        responseHeader.setStatusDesc(piRefundResponseData.getResult());
        responseHeader.setMethod(header.getMethod());
        responseHeader.setServiceType(header.getServiceType());
        responseHeader.setMchId(header.getMerchantId());
        responseHeader.setResponseTime(piRefundResponseData.getRefundedAt());

        IntegratedRefundResponseData responseData = new IntegratedRefundResponseData();
        responseData.setSysOrderNo(piRefundResponseData.getPartnerOrderId());
        responseData.setStoreRefundNo(piRefundResponseData.getPartnerOrderId());
        responseData.setStoreOrderNo(piRefundResponseData.getPartnerOrderId());
        responseData.setPlatformRsp(piResponse);

        ResponseGeneralBody piResponseBody = new ResponseGeneralBody();
        piResponseBody.setHeader(responseHeader);
        piResponseBody.setData(responseData);

        return new Gson().toJson(piResponseBody);
    }

    private String convertSingleQueryResponse(String piResponse, RequestHeader header) {
        PiServerOrderQueryResponse piServerOrderQueryResponse = new Gson().fromJson(piResponse, PiServerOrderQueryResponse.class);


        String statusCode;
        IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();

        TradeDetail tradeDetail = tradeDetailService.getOne(piServerOrderQueryResponse.getPartnerOrderId());
        if (Objects.nonNull(tradeDetail)) {
            integratedSingleOrderQueryResponseData.setStoreOrderNo(tradeDetail.getOrderId());
            integratedSingleOrderQueryResponseData.setSysOrderNo(StringUtils.defaultIfEmpty(tradeDetail.getSystemOrderId(), ""));
            integratedSingleOrderQueryResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
            integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());
            integratedSingleOrderQueryResponseData.setPaidAt(tradeDetail.getCreateDate());
            integratedSingleOrderQueryResponseData.setRefundedMsg(tradeDetail.getRefundStatus());
            RefundDetail refundDetail = refundDetailService.getOneByOrderIdAndMethod(tradeDetail.getOrderId(), tradeDetail.getMethod());
            if (Objects.nonNull(refundDetail)) {
                integratedSingleOrderQueryResponseData.setRefundedAt(refundDetail.getCreateTime());
            }
        }

        if ("failure".equals(piServerOrderQueryResponse.getResult())) {
            statusCode = SystemInstance.STATUS_SUCCESS;//piServerOrderQueryResponse.getErrorCode();
            integratedSingleOrderQueryResponseData.setOrderStatus("0");
//            integratedSingleOrderQueryResponseData.setPlatformRsp(piResponse);

        } else {
            statusCode = SystemInstance.STATUS_SUCCESS;

            integratedSingleOrderQueryResponseData.setFeeType(SystemInstance.CURRENCY_TWD);
            integratedSingleOrderQueryResponseData.setMobile(piServerOrderQueryResponse.getMobile());

            if (piServerOrderQueryResponse.getStatus().equals("3")) {
                integratedSingleOrderQueryResponseData.setOrderStatus("1");
            } else if (piServerOrderQueryResponse.getStatus().equals("D")) {
                integratedSingleOrderQueryResponseData.setOrderStatus("3");
            } else {
                integratedSingleOrderQueryResponseData.setOrderStatus("2");
            }

            integratedSingleOrderQueryResponseData.setTotalFee(piServerOrderQueryResponse.getMoney());
            integratedSingleOrderQueryResponseData.setUserId(piServerOrderQueryResponse.getPartnerUserId());
            integratedSingleOrderQueryResponseData.setStoreOrderNo(piServerOrderQueryResponse.getPartnerOrderId());
            integratedSingleOrderQueryResponseData.setSysOrderNo(piServerOrderQueryResponse.getId());
            integratedSingleOrderQueryResponseData.setPaidAt(piServerOrderQueryResponse.getPaidAt());
            integratedSingleOrderQueryResponseData.setPlatformRsp(piResponse);

        }


        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setStatusCode(statusCode);
        responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        responseGeneralHeader.setMethod(header.getMethod());
        responseGeneralHeader.setServiceType(header.getServiceType());
        responseGeneralHeader.setMchId(header.getMerchantId());


        ResponseGeneralBody piResponseBody = new ResponseGeneralBody();
        piResponseBody.setHeader(responseGeneralHeader);
        piResponseBody.setData(integratedSingleOrderQueryResponseData);

        return new Gson().toJson(piResponseBody);
    }

    private String getErrorResponse(RequestHeader header, String request, String message) {

        JsonObject jsonObject = new Gson().fromJson(request, JsonObject.class);

        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setStatusCode("9998");
        responseGeneralHeader.setStatusDesc(message);
        responseGeneralHeader.setMethod(header.getMethod());
        responseGeneralHeader.setServiceType(header.getServiceType());
        responseGeneralHeader.setMchId(header.getMerchantId());

        IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();
        integratedSingleOrderQueryResponseData.setFeeType(SystemInstance.CURRENCY_TWD);
        integratedSingleOrderQueryResponseData.setMobile("");
        integratedSingleOrderQueryResponseData.setOrderStatus(message);
        integratedSingleOrderQueryResponseData.setTotalFee("");
        integratedSingleOrderQueryResponseData.setUserId("");
        integratedSingleOrderQueryResponseData.setStoreOrderNo(jsonObject.get("partner_order_id").getAsString());
        integratedSingleOrderQueryResponseData.setSysOrderNo("");
        integratedSingleOrderQueryResponseData.setPaidAt("");

        ResponseGeneralBody piResponseBody = new ResponseGeneralBody();
        piResponseBody.setHeader(responseGeneralHeader);
        piResponseBody.setData(integratedSingleOrderQueryResponseData);

        return new Gson().toJson(piResponseBody);
    }
}
