package co.intella.service.impl;

import co.intella.domain.contratStore.CustomizePageRequest;
import co.intella.model.CaptureDetail;
import co.intella.model.CaptureOrderDetail;
import co.intella.model.QCaptureDetail;
import co.intella.model.QCaptureOrderDetail;
import co.intella.net.HttpRequestUtil;
import co.intella.repository.CaptureOrderDetailRepository;
import co.intella.service.CaptureOrderDetailService;
import co.intella.utility.HttpUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.assertj.core.util.Strings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Andy Lin
 */
@Service
@Transactional
public class CaptureOrderDetailServiceImpl implements CaptureOrderDetailService {

    @Resource
    private CaptureOrderDetailRepository captureOrderDetailRepository;
    @Value("${host.dao}")
    private String DAO_URL;

    public List<CaptureOrderDetail> update(List<CaptureOrderDetail> entities) {
        try {
            String response= HttpUtil.post(DAO_URL + "api/captureOrder/saveList" ,new ObjectMapper().writeValueAsString(entities));
            entities = new Gson().fromJson(response, new TypeToken<List<CaptureOrderDetail>>(){}.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entities;
//        return captureOrderDetailRepository.save(entities);
    }

    public List<CaptureOrderDetail> save(List<CaptureOrderDetail> entities) {
        try {
            String response= HttpUtil.post(DAO_URL + "api/captureOrder/saveList" ,new ObjectMapper().writeValueAsString(entities));
            entities = new Gson().fromJson(response, new TypeToken<List<CaptureOrderDetail>>(){}.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entities;
//        return captureOrderDetailRepository.save(entities);
    }

    public CaptureOrderDetail save(CaptureOrderDetail captureOrderDetail) {

        CaptureOrderDetail entity=null;
        try {
            String response= HttpUtil.post(DAO_URL + "api/captureOrder/save" ,new ObjectMapper().writeValueAsString(captureOrderDetail));
            entity = new Gson().fromJson(response,CaptureOrderDetail.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entity;

//        return captureOrderDetailRepository.save(captureOrderDetail);
    }

    public CaptureOrderDetail getOne(String mchId) {

        CaptureOrderDetail entity=null;
        try {
            String response= HttpRequestUtil.httpGet(DAO_URL + "api/captureOrder/getOneByMchId/"+mchId);
            entity = new Gson().fromJson(response, CaptureOrderDetail.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entity;
//        QCaptureOrderDetail qCaptureDetail = QCaptureOrderDetail.captureOrderDetail;
//        BooleanExpression predicate = qCaptureDetail.merchantId.eq(mchId);
//        return captureOrderDetailRepository.findOne(predicate);
    }

    public CaptureOrderDetail getOne(String mchId, String storeOrderNo) {
        CaptureOrderDetail entity=null;
        try {
            String response= HttpRequestUtil.httpGet(DAO_URL + "api/captureOrder/getOneByMchIdAndOrderId/"+mchId + "/" + storeOrderNo);
            entity = new Gson().fromJson(response, CaptureOrderDetail.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entity;

//        QCaptureOrderDetail qCaptureDetail = QCaptureOrderDetail.captureOrderDetail;
//        BooleanExpression predicate = qCaptureDetail.merchantId.eq(mchId).and(
//                qCaptureDetail.storeOrderNo.eq(storeOrderNo)
//        );
//        return captureOrderDetailRepository.findOne(predicate);
    }

    public List<CaptureOrderDetail> list(String mchId, String from, String to, String status) {

        Map<String,String> map = new HashMap<String, String>();

        map.put("type","FTS");
        map.put("mchId",mchId);
        map.put("from",from);
        map.put("to",to);
        map.put("status",status);

        List<CaptureOrderDetail> entities=null;
        try {
            String response= HttpUtil.post(DAO_URL + "api/captureOrder/listByType" ,new ObjectMapper().writeValueAsString(map));
            entities = new Gson().fromJson(response,new TypeToken<List<CaptureOrderDetail>>(){}.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entities;


//        QCaptureOrderDetail qCaptureOrderDetail = QCaptureOrderDetail.captureOrderDetail;
//        BooleanExpression predicate = qCaptureOrderDetail.merchantId.eq(mchId)
//                .and(qCaptureOrderDetail.tradeDate.between(from, to))
//                .and(qCaptureOrderDetail.status.eq(status));
//
//        return  Lists.newArrayList( captureOrderDetailRepository.findAll(predicate));
    }

    public List<CaptureOrderDetail> listByMerchantIdAndDate(String mchId, String date) {
        Map<String,String> map = new HashMap<String, String>();

        map.put("type","Date");
        map.put("mchId",mchId);
        map.put("date",date);

        List<CaptureOrderDetail> entities=null;
        try {
            String response= HttpUtil.post(DAO_URL + "api/captureOrder/listByType" ,new ObjectMapper().writeValueAsString(map));
            entities = new Gson().fromJson(response,new TypeToken<List<CaptureOrderDetail>>(){}.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entities;
//        QCaptureOrderDetail qCaptureOrderDetail = QCaptureOrderDetail.captureOrderDetail;
//        BooleanExpression predicate = qCaptureOrderDetail.merchantId.eq(mchId)
//                .and(qCaptureOrderDetail.captureDate.eq(date));
//
//        return  Lists.newArrayList( captureOrderDetailRepository.findAll(predicate));
    }

    public List<CaptureOrderDetail> listSettleCaptureOrder(String mchId, String from, String to, String type) {


        Map<String,String> map = new HashMap<String, String>();

        map.put("type","FTTDone");
        map.put("mchId",mchId);
        map.put("from",from);
        map.put("to",to);
        map.put("type",type);

        List<CaptureOrderDetail> entities=null;
        try {
            String response= HttpUtil.post(DAO_URL + "api/captureOrder/listByType" ,new ObjectMapper().writeValueAsString(map));
            entities = new Gson().fromJson(response,new TypeToken<List<CaptureOrderDetail>>(){}.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entities;



//        QCaptureOrderDetail qCaptureOrderDetail = QCaptureOrderDetail.captureOrderDetail;
//        BooleanExpression predicate = qCaptureOrderDetail.merchantId.eq(mchId)
//                .and(qCaptureOrderDetail.tradeDate.between(from, to))
//                .and(qCaptureOrderDetail.method.eq(type))
//                .and(qCaptureOrderDetail.status.eq("done"));
//
//        return  Lists.newArrayList( captureOrderDetailRepository.findAll(predicate));
    }

    public List<CaptureOrderDetail> listUnSettleCaptureOrder(String mchId, String from, String to, String type) {

        Map<String,String> map = new HashMap<String, String>();

        map.put("type","FTTWait");
        map.put("mchId",mchId);
        map.put("from",from);
        map.put("to",to);
        map.put("type",type);

        List<CaptureOrderDetail> entities=null;
        try {
            String response= HttpUtil.post(DAO_URL + "api/captureOrder/listByType" ,new ObjectMapper().writeValueAsString(map));
            entities = new Gson().fromJson(response,new TypeToken<List<CaptureOrderDetail>>(){}.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entities;

//        QCaptureOrderDetail qCaptureOrderDetail = QCaptureOrderDetail.captureOrderDetail;
//        BooleanExpression predicate = qCaptureOrderDetail.merchantId.eq(mchId)
//                .and(qCaptureOrderDetail.tradeDate.between(from, to))
//                .and(qCaptureOrderDetail.method.eq(type))
//                .and(qCaptureOrderDetail.status.eq("wait"));
//
//        return  Lists.newArrayList( captureOrderDetailRepository.findAll(predicate));
    }

    public List<CaptureOrderDetail> listByMerchantIdAndDate(String mchId, String from, String to, String type) {
        Map<String,String> map = new HashMap<String, String>();

        map.put("type","BetweenDate");
        map.put("mchId",mchId);
        map.put("from",from);
        map.put("to",to);
        map.put("type",type);

        List<CaptureOrderDetail> entities=null;
        try {
            String response= HttpUtil.post(DAO_URL + "api/captureOrder/listByType" ,new ObjectMapper().writeValueAsString(map));
            entities = new Gson().fromJson(response,new TypeToken<List<CaptureOrderDetail>>(){}.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entities;

//        QCaptureOrderDetail qCaptureOrderDetail = QCaptureOrderDetail.captureOrderDetail;
//        BooleanExpression predicate = qCaptureOrderDetail.merchantId.eq(mchId)
//                .and(qCaptureOrderDetail.tradeDate.between(from, to))
//                .and(qCaptureOrderDetail.method.eq(type));
//
//        return  Lists.newArrayList( captureOrderDetailRepository.findAll(predicate));
    }

    public List<CaptureOrderDetail> listAll() {
        List<CaptureOrderDetail> entities=null;
        try {
            String response= HttpRequestUtil.httpGet(DAO_URL + "api/captureOrder/listAll");
            entities = new Gson().fromJson(response,new TypeToken<List<CaptureOrderDetail>>(){}.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entities;

//        return captureOrderDetailRepository.findAll();
    }

    public List<CaptureOrderDetail> pageList(CustomizePageRequest request) {
        List<CaptureOrderDetail> entities=null;
        try {
            String response= HttpUtil.post(DAO_URL + "api/captureOrder/pageList" ,new ObjectMapper().writeValueAsString(request));
            entities = new Gson().fromJson(response,new TypeToken<List<CaptureOrderDetail>>(){}.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entities;


//        Page<CaptureOrderDetail> pageableList = getPageList(request);
//        return pageableList.getContent();
    }

    public long pageCount(CustomizePageRequest request) {
        Long count = null;

        try {
            String responseEntity = HttpUtil.post(DAO_URL+"api/captureOrder/pageCount" , new ObjectMapper().writeValueAsString(request));
            count = Long.parseLong(responseEntity);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(count == null) {
            return 0;
        }
        return count;

//        Page<CaptureOrderDetail> pageableList = getPageList(request);
//        return pageableList.getTotalElements();
    }

    private Page<CaptureOrderDetail> getPageList(CustomizePageRequest request) {
        PageRequest pageRequest = new PageRequest(request.getIndex(), request.getPageSize(), Sort.Direction.DESC, "createTime");
        QCaptureOrderDetail qCaptureOrderDetail = QCaptureOrderDetail.captureOrderDetail;

        BooleanExpression predicate = qCaptureOrderDetail.merchantId.eq(request.getAccountId());

        if(request.getStartDate() == null || request.getEndDate() == null) {
            if(!Strings.isNullOrEmpty(request.getPaymentMethod())) {
                predicate = predicate.and(qCaptureOrderDetail.method.eq(request.getPaymentMethod()));
            }

            if(!Strings.isNullOrEmpty(request.getOrderId())) {
                predicate = predicate.and(qCaptureOrderDetail.storeOrderNo.eq(request.getOrderId()));
            }

        } else {

            predicate = predicate.and(qCaptureOrderDetail.captureDate.between(request.getStartDate(), request.getEndDate()));

            if(!Strings.isNullOrEmpty(request.getPaymentMethod())) {
                predicate = predicate.and(qCaptureOrderDetail.method.eq(request.getPaymentMethod()));
            }

            if(!Strings.isNullOrEmpty(request.getOrderId())) {
                predicate = predicate.and(qCaptureOrderDetail.storeOrderNo.eq(request.getOrderId()));
            }
        }

        return captureOrderDetailRepository.findAll(predicate, pageRequest);
    }
}
