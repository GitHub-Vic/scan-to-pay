package co.intella.service.impl;

import co.intella.model.InvoiceDetail;
import co.intella.repository.InvoiceDetailRepository;
import co.intella.service.InvoiceDetailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Alex
 */
@Service
public class InvoiceDetailServiceImpl implements InvoiceDetailService {

    @Resource
    private InvoiceDetailRepository invoiceDetailRepository;

    public InvoiceDetail save(InvoiceDetail invoiceDetail) {
        return invoiceDetailRepository.save(invoiceDetail);
    }
}
