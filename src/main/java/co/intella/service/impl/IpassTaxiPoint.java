package co.intella.service.impl;

import co.intella.model.Device;
import co.intella.model.IntTbAbnormalRule;
import co.intella.model.IntTbLookupCode;
import co.intella.service.*;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class IpassTaxiPoint extends TaxiPoint {

    private final Logger LOGGER = LoggerFactory.getLogger(IpassTaxiPoint.class);

    @Resource
    private LookupCodeService lookupCodeService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private AbnormalRuleService abnormalRuleService;

    @Resource
    private AbnormalRuleCtrlService abnormalRuleCtrlService;

    @Resource
    private TaxiTransactionDetailService taxiTransactionDetailService;

    @Resource
    private TicketLogicService ticketLogicService;


    @Override
    public String getScan2PayLcation(Device device) {

        IntTbLookupCode lookupCode = lookupCodeService.findOne("ipasslocation", device.getIpasslocation());
        String location = "TC";
        if (Objects.nonNull(lookupCode)) {
            location = lookupCode.getType1();
            LOGGER.info("[getScan2PayLcation][ipasslocation][" + device.getIpasslocation() + "]" + lookupCode.getType1());
        }

        return location;
    }

    @Override
    public String getlLcationUsePointLimit(String location) {

        IntTbLookupCode lookupCode = lookupCodeService.findOne("ipasslocation", location);
        String pointLimit = "85";
        if (Objects.nonNull(lookupCode)) {
            pointLimit = lookupCode.getType2();
            LOGGER.info("[getlLcationUsePointLimit][ipasslocation][" + location + "]" + lookupCode.getType2());
        }

        return pointLimit;
    }

    @Override
    public IntTbLookupCode isCanDeductionPoint(Device device, JsonObject queryCardJson) {

        LOGGER.info("[isCanDeductionPoint][device.ipassLocation = " + device.getIpasslocation() + "]" + "card AreaCode = " + queryCardJson.get("SpecialIdentityProvider").getAsString());
        if (!device.getIpasslocation().equals(queryCardJson.get("SpecialIdentityProvider").getAsString()))
            return null;
        // "21" + device.getIpasslocation()+  specialCardType + identityType + specialIdentityType;
        //2106A30302
        String specialCardType = queryCardJson.get("SpecialCardType").getAsString().trim(); // 特種票票卡種類
        String identityType = queryCardJson.get("IdentityType").getAsString().trim(); // 身份別
        String specialIdentityType = queryCardJson.get("SpecialIdentityType").getAsString().trim(); // 特種票識別身分
        String lookupCode = "21" + device.getIpasslocation() + specialCardType + identityType + specialIdentityType;

        LOGGER.info("[isCanDeductionPoint][carePoint]" + lookupCode);

        return lookupCodeService.findOne("carePoint", lookupCode);

    }

    @Override
    public String verificationRule(Device device, JsonObject queryCardJson, List<IntTbAbnormalRule> abRList) {

        return ticketLogicService.getAbnormalRuleMsg(abRList, device.getLicensePlate(), queryCardJson.get("CardSN").getAsString());
    }

    /**
     * 剩餘可使用點數
     *
     * @return
     */
    private int getBeforePoint(JsonObject queryCardJson) {
        String usedate = queryCardJson.get("SpecialIdentityResetDate").getAsString();
        String now = new SimpleDateFormat("yyyyMM").format(new Date());
        //剩餘點數
        Integer beforePoint = Integer.valueOf(queryCardJson.get("SpecialIdentityUsagePointLimit").getAsString()) -
                (!now.equals(usedate.substring(0, 6)) || "00000000".equals(usedate) ? 0 : Integer.valueOf(queryCardJson.get("UsageAccumulatedPointBeforeTxn").getAsString()));
        return beforePoint;
    }

    /*
     * { "CardID": "01", 卡別（01 一卡通）
     * "CardSN": "6AEF5633",
     * "CardSNFull":"6AEF5633E088040047C12B9499001607", 卡號
     * "Balance": "-1",卡片餘額
     * "BackUpBalance": "-1",
     * "CardType": "01", 卡片種類
     * "IdentityType": "03",身份別
     * "IdentityExpiryDate": "20481130235959", 特種票有效期限(YYYYMMDD HHMMSS)
     * "SpecialCardType": "A3", 特種票票卡種類
     *  "SpecialIdentityProvider":"06",特種票識別單位（區碼）
     * "SpecialIdentityType": "02", 特種票識別身分
     * "SpecialIdentityUsagePointLimit": "1000", 社福點數最大限制
     * "UsageAccumulatedPointBeforeTxn": "150", 已使用過的社福點數
     * "SpecialIdentityResetDate": "20190322080000", 特種票重置日期(YYYYMMDD HHMMSS)上次更新日期
     * "AutoloadFlag": "FF", 自動加值旗標 01 為開啟，FF 為關閉
     * "AutoloadUnit": "0", 自動加值面
     * "SpecialIdentityPointRatio": "1", 點數折合台幣比率
     * "MaxPointPerTrip": "50", 最大扣點數
     * "UsePointLimit": "85", 使用點數限制（交易金額最少為）
     * "TXNResult": "Success",
     * "ErrorCode": "000000",
     * "NewAESKey": "9aa329ba0773b04ca00b61733cd5d82f" }
     */
    public Map<String, String> allocationForTC(JsonObject queryCardJson, JsonObject requestData, IntTbLookupCode intTbLookupCode, Boolean verificationRule, Device device) {
        String lcationUsePointLimit = getlLcationUsePointLimit(device.getIpasslocation());
        LOGGER.info("[verificationRule]" + verificationRule);
        LOGGER.info("[IntTbLookupCode]" + intTbLookupCode);

        int needBalance = 0, point = 0, cash = 0, discountAmount = 0, specialIdentityPointRatio = 0;
        Map<String, String> map = new HashMap<>();

        Integer total = Integer.valueOf(requestData.get("Total").getAsString());
        Integer balance = Integer.valueOf(queryCardJson.get("Balance").getAsString());  //錢包餘額
        if (verificationRule && !(Objects.isNull(intTbLookupCode) || Objects.isNull(intTbLookupCode.getLookupType()))
                && total >= Integer.valueOf(lcationUsePointLimit)) { // 可扣社福點

            //剩餘點數
            Integer beforePoint = this.getBeforePoint(queryCardJson);
            //點數折合台幣比率
            specialIdentityPointRatio = Integer.valueOf(queryCardJson.get("SpecialIdentityPointRatio").getAsString());
            //最大可扣除點數
            point = Math.min(beforePoint, intTbLookupCode.getValue());
            //最大可優惠金額
            discountAmount = point * specialIdentityPointRatio;
        }

        if (discountAmount >= total) {   //優惠金額可全抵車資
            point = total > 0 ? total / specialIdentityPointRatio : 0;
        } else if (balance >= (total - discountAmount) || "01".equals(queryCardJson.get("AutoloadFlag").getAsString())) {     //有自動充值
            cash = total - discountAmount;
        } else {       //無自動充值
            needBalance = total - discountAmount;
            if (balance > 0) {                      //若有餘額,錢包扣完
                cash = balance;
                needBalance -= balance;
            }
        }

        map.put("Point", Integer.toString(point));
        map.put("Cash", Integer.toString(cash));
        map.put("NeedBalance", Integer.toString(needBalance));
        return map;
    }

    /**
     * 桃園
     *
     * @param queryCardJson
     * @param requestData
     * @param intTbLookupCode
     * @param verificationRule
     * @param device
     * @return
     */
    public Map<String, String> allocationForTY(JsonObject queryCardJson, JsonObject requestData, IntTbLookupCode intTbLookupCode, Boolean verificationRule, Device device) {
        String lcationUsePointLimit = getlLcationUsePointLimit(device.getIpasslocation());
        LOGGER.info("[verificationRule]" + verificationRule);
        LOGGER.info("[IntTbLookupCode]" + intTbLookupCode);

        int needBalance = 0, point = 0, cash = 0, discountAmount = 0, specialIdentityPointRatio = 0;
        Map<String, String> map = new HashMap<>();

        Integer total = Integer.valueOf(requestData.get("Total").getAsString());
        Integer balance = Integer.valueOf(queryCardJson.get("Balance").getAsString());  //錢包餘額
        if (verificationRule && !(Objects.isNull(intTbLookupCode) || Objects.isNull(intTbLookupCode.getLookupType()))
                && total >= Integer.valueOf(lcationUsePointLimit)) { // 可扣社福點

            //剩餘點數
            Integer beforePoint = this.getBeforePoint(queryCardJson);
            //點數折合台幣比率
            specialIdentityPointRatio = Integer.valueOf(queryCardJson.get("SpecialIdentityPointRatio").getAsString());

            int _point = total <= Integer.valueOf(lookupCodeService.findOne("ipasslocation", device.getIpasslocation()).getType3()) ? intTbLookupCode.getValue() : intTbLookupCode.getValue() * 2;
            //最大可扣除點數
            //當乘車點數不足36或72點時，仍可以搭乘愛心計程車，付款時會將點數扣至0，其餘車資由卡片餘額扣除；並請留意倘點數及卡片餘額皆不足時，將無法交易。
            point = Math.min(_point, beforePoint);
            //最大可優惠金額
            discountAmount = point * specialIdentityPointRatio;
        }

        if (discountAmount >= total) {   //優惠金額可全抵車資
            point = total > 0 ? total / specialIdentityPointRatio : 0;
        } else if (balance >= (total - discountAmount) || "01".equals(queryCardJson.get("AutoloadFlag").getAsString())) {     //有自動充值
            cash = total - discountAmount;
        } else {       //無自動充值
            point = 0;
            needBalance = total;
            cash = 0;
        }

        map.put("Point", Integer.toString(point));
        map.put("Cash", Integer.toString(cash));
        map.put("NeedBalance", Integer.toString(needBalance));
        return map;
    }
}
