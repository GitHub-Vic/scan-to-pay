package co.intella.service.impl;

import co.intella.domain.integration.*;
import co.intella.domain.megaweixin.*;
import co.intella.domain.weixin.WeixinMicropayRequestData;
import co.intella.domain.weixin.WeixinRefundRequestData;
import co.intella.domain.weixin.WeixinSingleQueryRequestData;
import co.intella.exception.ServiceTypeNotSupportException;
import co.intella.model.*;
import co.intella.service.*;
import co.intella.utility.AESUtil;
import co.intella.utility.HttpUtil;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.crypto.Cipher;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @author Ziv
 */
@Service
public class MegaWeixinServiceImpl implements MegaWeiXinService {

    private final static Logger LOGGER = LoggerFactory.getLogger(MegaWeixinServiceImpl.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private Environment env;

    @Autowired
    private AESUtil aesUtil;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private OrderService orderService;

    @Value("${host.request.megaweixin}")
    private String MEGAWEIXIN_URL;

    @Value("${api.url}")
    private String API_URL;

    @Value("${megaweixin.rsa.public.key}")
    private String MEGAWEIXIN_RSA_PUBLIC_KEY;

    private static final String SINGLE_ORDER_QUERY = "SingleOrderQuery";
    private static final String PAYMENT = "Payment";
    private static final String REFUND = "Refund";
    private static final String PAYMENT_SUCCESS_STATUS = "1";
    private static final String REFUND_SUCCESS_STATUS = "Y";
    private static final String PKCS_1_PEM_HEADER = "-----BEGIN RSA PRIVATE KEY-----";
    private static final String PKCS_1_PEM_FOOTER = "-----END RSA PRIVATE KEY-----";

    @Override
    public String doRequest(RequestHeader requestHeader, JsonPrimitive data, String gw, String clientIP) throws Exception {
        String generalResponseData = SystemInstance.EMPTY_STRING;

        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        PaymentAccount paymentAccount = paymentAccountService.getOneByDao(integrateMchId, bankId);

        if (SystemInstance.TYPE_MICROPAY.equals(requestHeader.getServiceType())) {
            LOGGER.info("[MegaWexin][Micropay][data]" + data.getAsString());
            if (ObjectUtils.isEmpty(paymentAccount)) {
                throw new ServiceTypeNotSupportException("Exception: The store's account isn't opened.");
            }
            LOGGER.info("[MegaWexin][Micropay][account]" + paymentAccount.getAccount());

            //建交易請求
            MegaWeiXinRequestData megaPaymentReqtData = getMegaWeiXinRequestData(data, paymentAccount, PAYMENT, clientIP);
            MegaWeiXinRequestHeader megaPaymentReqHeader = getMegaWeiXinRequestHeader(paymentAccount, megaPaymentReqtData, clientIP);
            MegaWeiXinRequest megaWeiXinReq = new MegaWeiXinRequest();
            megaWeiXinReq.setHeader(megaPaymentReqHeader);
            megaWeiXinReq.setData(megaPaymentReqtData);
            //建交易訂單
            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
            //取得一維碼
            String auth_code = data.getAsString().replaceAll(".*\"AuthCode\":\"(\\w*)\".*", "$1");
            LOGGER.info("[MegaWexin][Micropay][auth_code]" + auth_code);
            //交易請求取得回應 直到成功或相同請求重複6次 每次間隔10秒
            String requestInfo = new GsonBuilder().disableHtmlEscaping().create().toJson(megaWeiXinReq);
            LOGGER.info("[MegaWexin][Micropay][requestInfo]" + requestInfo);
            int retry = 0;
            MegaWeiXinResponse megaPaymentRes = getMegaWeiXinResponse(requestInfo, retry);
            String sys_order_no = null;
            if ("0000".equals(megaPaymentRes.getHeader().getStatusCode())) {
                sys_order_no = megaPaymentRes.getData().getDataValue().replaceAll(".*\"SYS_ORDER_NO\":\"(\\w*)\".*", "$1");
            }

            //查詢交易是否成功
            //建查詢請求
            MegaWeiXinRequestData paymentQueryRequestData = getMegaWeiXinPaymentQueryRequestData(megaPaymentReqtData, paymentAccount, tradeDetail);
            MegaWeiXinRequestHeader paymentQueryRequestHeader = getMegaWeiXinRequestHeader(paymentAccount, paymentQueryRequestData, clientIP);
            MegaWeiXinRequest paymentQueryRequest = new MegaWeiXinRequest(paymentQueryRequestHeader, paymentQueryRequestData);
            //請求查詢取得回應
            String queryResInfo = new GsonBuilder().disableHtmlEscaping().create().toJson(paymentQueryRequest);
            MegaWeiXinResponse queryResponse = getMegaWeiXinResponse(queryResInfo);
            //取得回應訂單DATA
            MegaWeiXinResponseData queryResponseData = queryResponse.getData();
            String dataValueResult = queryResponseData.getDataValue();
            WeiXinOutSingleOrderQueryRes queryOrderData = new Gson().fromJson(dataValueResult, WeiXinOutSingleOrderQueryRes.class);
            if ("0000".equals(queryResponse.getHeader().getStatusCode())) {
                String sys_status = queryOrderData.getSys_status();
                if (PAYMENT_SUCCESS_STATUS.equals(sys_status)) {
                    sys_order_no = StringUtils.isNotBlank(sys_order_no) ? sys_order_no : queryOrderData.getSys_order_no();
                    tradeDetail.setSystemOrderId(sys_order_no);
                }
            }

            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, new Gson().toJson(megaPaymentRes), data);

            ResponseGeneralHeader responseGeneralHeader = getResponseHeader(megaPaymentRes.getHeader().getStatusCode(), megaPaymentRes.getHeader().getStatusDesc(), requestHeader, messageSource, requestHeader.getServiceType());

            requestHeader.setMerchantId(integrateMchId);
            IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();
            integratedMicropayResponseData.setStoreOrderNo(queryOrderData.getStore_order_no());
            if (StringUtils.isNotBlank(tradeDetail.getSystemOrderId())) {
                integratedMicropayResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
            }
            integratedMicropayResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
            integratedMicropayResponseData.setAuthCode(auth_code);
            integratedMicropayResponseData.setPlatformRsp(dataValueResult);

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedMicropayResponseData);

            generalResponseData = new GsonBuilder().disableHtmlEscaping().create().toJson(responseGeneralBody);
            LOGGER.info("integrate response ready :" + generalResponseData);

        } else if ("Refund".equals(requestHeader.getServiceType())) {
            LOGGER.info("[MegaWexin][Refund][data]" + data.getAsString());

            //建退款請求
            MegaWeiXinRequestData megaRefundReqData = getMegaWeiXinRequestData(data, paymentAccount, REFUND, clientIP);
            MegaWeiXinRequestHeader megaRefundReqHeader = getMegaWeiXinRequestHeader(paymentAccount, megaRefundReqData, clientIP);
            MegaWeiXinRequest megaWeiXinReq = new MegaWeiXinRequest();
            megaWeiXinReq.setHeader(megaRefundReqHeader);
            megaWeiXinReq.setData(megaRefundReqData);
            //查詢欲退款訂單
            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
            //退款請求取得回應
            String requestInfo = new GsonBuilder().disableHtmlEscaping().create().toJson(megaWeiXinReq);
            MegaWeiXinResponse megaRefundRes = getMegaWeiXinResponse(requestInfo);
            String sys_refund_no = null;
            if ("0000".equals(megaRefundRes.getHeader().getStatusCode())) {
                sys_refund_no = megaRefundRes.getData().getDataValue().replaceAll(".*\"SYS_REFUND_NO\":\"(\\w*)\".*", "$1");
                //查詢無法取得退款sys_refund_no 在退款成功當下先存
                tradeDetail.setSystemRefundId(sys_refund_no);
            }

            //查詢退款是否成功
            //建查詢請求
            MegaWeiXinRequestData refundQueryRequestData = getMegaWeiXinPaymentQueryRequestData(megaRefundReqData, paymentAccount, tradeDetail);
            MegaWeiXinRequestHeader refundQueryRequestHeader = getMegaWeiXinRequestHeader(paymentAccount, refundQueryRequestData, clientIP);
            MegaWeiXinRequest refundQueryRequest = new MegaWeiXinRequest(refundQueryRequestHeader, refundQueryRequestData);
            //請求查詢取得回應
            String queryResInfo = new Gson().toJson(refundQueryRequest);
            int retry = 0;
            MegaWeiXinResponse queryResponse = getMegaWeiXinResponse(queryResInfo, retry);
            //取得回應訂單DATA
            MegaWeiXinResponseData queryResponseData = queryResponse.getData();
            String dataValueResult = queryResponseData.getDataValue();
            WeiXinOutSingleOrderQueryRes queryOrderData = new GsonBuilder().disableHtmlEscaping().create().fromJson(dataValueResult, WeiXinOutSingleOrderQueryRes.class);
            if ("0000".equals(queryResponse.getHeader().getStatusCode())) {
                String sys_status = queryOrderData.getIs_refund();
                if (REFUND_SUCCESS_STATUS.equals(sys_status)) {
                    sys_refund_no = StringUtils.isNotBlank(sys_refund_no) ? sys_refund_no : "";
                    tradeDetail.setSystemRefundId(sys_refund_no);
                }
            }

            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, new Gson().toJson(queryResponse), data);

            ResponseGeneralHeader responseGeneralHeader = getResponseHeader(new Gson().fromJson(queryResponse.getData().getDataValue(), JsonObject.class).get("IS_REFUND").getAsString(), megaRefundRes.getHeader().getStatusDesc(), requestHeader, messageSource, requestHeader.getServiceType());

            responseGeneralHeader.setMchId(integrateMchId);

            IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();

            integratedRefundResponseData.setStoreOrderNo(tradeDetail.getOrderId());
            integratedRefundResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
            integratedRefundResponseData.setStoreRefundNo(sys_refund_no);
            integratedRefundResponseData.setRefundedAt(DateTime.now().toString(SystemInstance.DATE_PATTERN));

            integratedRefundResponseData.setPlatformRsp(dataValueResult);

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedRefundResponseData);
            generalResponseData = new GsonBuilder().disableHtmlEscaping().create().toJson(responseGeneralBody);

            LOGGER.info("integrate response ready :" + generalResponseData);

        } else if ("SingleOrderQuery".equals(requestHeader.getServiceType())) {
            LOGGER.info("[Mega][SingleOrderQuery][data]" + data.getAsString());
            //建查詢請求
            MegaWeiXinRequestData megaQueryReqData = getMegaWeiXinRequestData(data, paymentAccount, SINGLE_ORDER_QUERY, clientIP);
            MegaWeiXinRequestHeader megaQueryReqHeader = getMegaWeiXinRequestHeader(paymentAccount, megaQueryReqData, clientIP);
            MegaWeiXinRequest megaWeiXinReq = new MegaWeiXinRequest();
            megaWeiXinReq.setHeader(megaQueryReqHeader);
            megaWeiXinReq.setData(megaQueryReqData);
            //請求查詢取得回應
            String requestInfo = new GsonBuilder().disableHtmlEscaping().create().toJson(megaWeiXinReq);
            MegaWeiXinResponse megaQueryRes = getMegaWeiXinResponse(requestInfo);
            //更新訂單
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, new Gson().toJson(megaQueryRes), data);
            //組成responseGeneralHeader
            MegaWeiXinResponseHeader queryResponseHeader = megaQueryRes.getHeader();
            ResponseGeneralHeader responseGeneralHeader = getResponseHeader(queryResponseHeader.getStatusCode(), queryResponseHeader.getStatusDesc(), requestHeader, messageSource, requestHeader.getServiceType());
            responseGeneralHeader.setMchId(integrateMchId);
            //取得回應訂單DATA
            MegaWeiXinResponseData queryResponseData = megaQueryRes.getData();
            String dataValueResult = queryResponseData.getDataValue();
            WeiXinOutSingleOrderQueryRes queryOrderData = new Gson().fromJson(dataValueResult, WeiXinOutSingleOrderQueryRes.class);
            //組成IntegratedSingleOrderQueryResponseData
            IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();
            WeixinSingleQueryRequestData queryReq = new Gson().fromJson(data.getAsString(), WeixinSingleQueryRequestData.class);
            if (Objects.nonNull(queryReq)) {
                TradeDetail tradeDetail = tradeDetailService.getOne(queryReq.getOutTradeNo());
                if (Objects.nonNull(tradeDetail)) {
                    integratedSingleOrderQueryResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
                    integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());
                    integratedSingleOrderQueryResponseData.setPaidAt(tradeDetail.getCreateDate());
                    integratedSingleOrderQueryResponseData.setRefundedMsg(tradeDetail.getRefundStatus());
                }
            }
            integratedSingleOrderQueryResponseData.setStoreOrderNo(queryOrderData.getStore_order_no());
            integratedSingleOrderQueryResponseData.setSysOrderNo(queryOrderData.getSys_order_no());
            if (REFUND_SUCCESS_STATUS.equals(queryOrderData.getIs_refund())) {
                integratedSingleOrderQueryResponseData.setRefundedAt(responseGeneralHeader.getResponseTime());
            }
            integratedSingleOrderQueryResponseData.setOrderStatus(queryOrderData.getSys_status());
            integratedSingleOrderQueryResponseData.setOrderStatusDesc("TODO");

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedSingleOrderQueryResponseData);
            generalResponseData = new Gson().toJson(responseGeneralBody);

            LOGGER.info("integrate response ready :" + generalResponseData);

        }
        return generalResponseData;
    }

    public ModelAndView doOLPay(QrcodeParameter qrcodeParameter, Merchant merchant, String totalFee, HttpServletResponse response) {
        try {
            PaymentAccount paymentAccount = paymentAccountService.getOne(merchant.getAccountId(), 40);

            String storeOrderNoNew = getStoreOrderNoNew(qrcodeParameter);
            createMegaWeixinTradeDetail(storeOrderNoNew, merchant, totalFee, "14010");
            createCookie(storeOrderNoNew, response);
            //new WeiXinUnifiedorderJsapiReq set storeOderNo, barcodeSeq, tradeTime(now + 1hr), totalFee, purposeType
            WeiXinUnifiedorderJsapiReq weiXinUnifiedorderJsapiReq = new WeiXinUnifiedorderJsapiReq(storeOrderNoNew, paymentAccount.getHashIV(), DateTime.now().plusHours(1).toString(SystemInstance.DATE_PI_PATTERN), totalFee, paymentAccount.getDepositNo());
            //new goodsDetail
//		    GoodsDetail[] goodsDetail = {new GoodsDetail(qrcodeParameter.getBody(), 1)};
//		    Map<String, Object> detail = new HashMap<String, Object>();
//		    detail.put("goods_detail", goodsDetail);
            //set goods_detail
//		    weiXinUnifiedorderJsapiReq.setDetail(new Gson().toJson(detail));
            //getDataValue
            String dataValue = aesUtil.useRfc2898DeriveBytesToEncrypt(new Gson().toJson(weiXinUnifiedorderJsapiReq), paymentAccount.getHashKey(), paymentAccount.getAccount());
            //new JsapiHeaderRequestData set tradeMode, tradeTime(now + 1hr), barcodeSeq, tradeCurrency, tradeAmount, productDesc
            JsapiHeaderRequestData jsapiHeaderRequestData = new JsapiHeaderRequestData(weiXinUnifiedorderJsapiReq, qrcodeParameter.getBody());
            jsapiHeaderRequestData.setDataValue(dataValue);
            //getHashValue
            String reqData = new GsonBuilder().disableHtmlEscaping().create().toJson(jsapiHeaderRequestData);
            LOGGER.info("reqData : " + reqData);
            String hashValue = encryptSHA256(reqData);

            return new ModelAndView("redirect:" + "https://www.megaezgo.com/wxapi/pay/JsapiGateway.aspx?"
                    + "s=" + URLEncoder.encode(paymentAccount.getAccount(), "UTF-8")
                    + "&m=" + URLEncoder.encode(jsapiHeaderRequestData.getTradeMode(), "UTF-8")
                    + "&b=" + URLEncoder.encode(paymentAccount.getHashIV(), "UTF-8")
                    + "&c=" + URLEncoder.encode(jsapiHeaderRequestData.getTradeCurrency(), "UTF-8")
                    + "&a=" + URLEncoder.encode(totalFee, "UTF-8")
                    + "&d=" + URLEncoder.encode(dataValue, "UTF-8")
                    + "&h=" + URLEncoder.encode(hashValue, "UTF-8")
                    + "&t=" + URLEncoder.encode(weiXinUnifiedorderJsapiReq.getTradeTime(), "UTF-8")
                    + "&r=Y"
            );
        } catch (Exception e) {
            LOGGER.info("[MegaWeixin][EXCEPTION] - " + e.getMessage() + " - " + e.getStackTrace());
            return null;
        }
    }

    //兆豐微信正掃 只會在成功時才有回調
    public String doNotify(OrderPaymentJson orderPaymentJson) {

        String orderId = orderPaymentJson.getStore_order_no();
        TradeDetail tradeDetail = tradeDetailService.getOne(orderId);
        Map<String, String> result = new HashMap<String, String>();
        result.put("StatusCode", "0000");
        result.put("StatusDesc", "SUCCESS");

        try {
            if (tradeDetail != null && tradeDetail.getPayment() == Long.parseLong(orderPaymentJson.getOrder_amount())) {
                updateMegaWeixinTradeDetail(orderPaymentJson, tradeDetail);
                LOGGER.info("[MegaWeixin][Notify] update tradeDetail success");
            }
        } catch (Exception e) {
            result = null;
            LOGGER.info("[MegaWeixin][Notify] update tradeDetail fail");
        }
        return new Gson().toJson(result);
    }

    public void updateMegaWeixinTradeDetail(OrderPaymentJson orderPaymentJson, TradeDetail tradeDetail) {
        tradeDetail.setTxParams(new Gson().toJson(orderPaymentJson));
        tradeDetail.setPlatformPaidDate(String.format(orderPaymentJson.getOrder_time()));
        tradeDetail.setSystemOrderId(orderPaymentJson.getSys_order_id());
        tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);

        tradeDetailService.save(tradeDetail);
        appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
    }


    //取得MegaWeiXinRequestHeader
    public MegaWeiXinRequestHeader getMegaWeiXinRequestHeader(PaymentAccount paymentAccount, MegaWeiXinRequestData requestData, String clientIP) throws Exception {
        MegaWeiXinRequestHeader paymentRequestHeader = null;
        clientIP = clientIP.split(",")[0];
        if (StringUtils.isNotEmpty(requestData.getDataValue())) {
            String reqData = new GsonBuilder().disableHtmlEscaping().create().toJson(requestData);
            LOGGER.info("reqData : " + reqData);
            String hashValue = encryptRSA(encryptSHA256(reqData));
            LOGGER.info("hashValue : " + hashValue);
            String version = "1.0";
            String apiStoreId = paymentAccount.getAccount();
            String serviceName = "WeiXin";
            String terminalIp = clientIP;

            paymentRequestHeader = new MegaWeiXinRequestHeader(version, apiStoreId, terminalIp, serviceName, hashValue);
        }
        return paymentRequestHeader;
    }

    //取得MegaWeiXinRequestData
    public MegaWeiXinRequestData getMegaWeiXinRequestData(JsonPrimitive request, PaymentAccount paymentAccount, String paymentType, String clientIP) throws Exception {
        MegaWeiXinRequestData megaWeiXinRequestData = new MegaWeiXinRequestData();
        clientIP = clientIP.split(",")[0];
        String apiStorePw = paymentAccount.getHashKey();
        String tradeSign = DateTime.now().toString(SystemInstance.DATE_PI_PATTERN);

        megaWeiXinRequestData.setApiStorePw(apiStorePw);
        megaWeiXinRequestData.setTradeSign(tradeSign);
        megaWeiXinRequestData.setPaymentType(paymentType);

        String dataValue = "";
        if (PAYMENT.equals(paymentType)) {
            /*
             * store_order_no, device_info , desc, purpose_type,
             * order_amount, currency, spbill_create_ip, auth_code, detail
             * device_info 需在後台設定才可使用
             */
            WeixinMicropayRequestData microReq = new Gson().fromJson(request.getAsString(), WeixinMicropayRequestData.class);
            WeiXinMicroPayReq megaWeixinPaymentReq = new WeiXinMicroPayReq(microReq.getOutTradeNo(), "skb0001", microReq.getBody().startsWith("Item") ? microReq.getBody() : "Item name" + microReq.getBody(), "132",
                    Integer.parseInt(microReq.getTotalFee()), microReq.getFeeType(), clientIP, request.getAsString().replaceAll(".*\"AuthCode\":\"(\\w*)\".*", "$1"), microReq.getDetail());
            GoodsDetail[] goodsDetail = {new GoodsDetail(megaWeixinPaymentReq.getDesc(), 1)};
            Map<String, Object> detail = new HashMap<String, Object>();
            detail.put("goods_detail", goodsDetail);
            megaWeixinPaymentReq.setDetail(new Gson().toJson(detail));
            dataValue = new Gson().toJson(megaWeixinPaymentReq);
        } else if (REFUND.equals(paymentType)) {
            /*
             * store_order_no sys_order_no 擇一使用
             * store_refund_no, store_order_no, sys_order_no, device_info,
             * order_amount, currency, refund_amount
             */
            WeixinRefundRequestData refundReq = new Gson().fromJson(request.getAsString(), WeixinRefundRequestData.class);
            WeiXinRefundPayReq megaWeixinRefundReq = new WeiXinRefundPayReq(refundReq.getOutRefundNo(), refundReq.getOutTradeNo(), "", "skb0001",
                    Integer.parseInt(refundReq.getRefundFee()), refundReq.getRefundFeeType(), Integer.parseInt(refundReq.getRefundFee()));
            dataValue = new Gson().toJson(megaWeixinRefundReq);
        } else if (SINGLE_ORDER_QUERY.equals(paymentType)) {
            /*
             * store_order_no sys_order_no 擇一使用
             * device_info, store_order_no, sys_order_no
             */
            WeixinSingleQueryRequestData queryReq = new Gson().fromJson(request.getAsString(), WeixinSingleQueryRequestData.class);
            TradeDetail tradeDetail = tradeDetailService.getOne(queryReq.getOutTradeNo());
            WeiXinOutSingleOrderQueryReq megaWeixinQueryReq = new WeiXinOutSingleOrderQueryReq(tradeDetail.getTxParams(), queryReq.getOutTradeNo(), "");
            dataValue = new Gson().toJson(megaWeixinQueryReq);
        }
        megaWeiXinRequestData.setDataValue(dataValue);

        return megaWeiXinRequestData;
    }

    //取得 PAYMENT/REFUND 查詢之 MegaWeiXinRequestData
    public static MegaWeiXinRequestData getMegaWeiXinPaymentQueryRequestData(MegaWeiXinRequestData requestData, PaymentAccount paymentAccount, TradeDetail tradeDetail) throws Exception {
        //查詢的MegaWeiXinRequestData
        MegaWeiXinRequestData megaWeiXinRequestData = new MegaWeiXinRequestData();
        //從PAYMENT/REFUND請求取得device
        String requestInfo = requestData.getDataValue();
        String deviceinfo = requestInfo.replaceAll(".*\"device_info\":\"(\\w*)\".*", "$1");
        WeiXinOutSingleOrderQueryReq queryReqData = new WeiXinOutSingleOrderQueryReq(deviceinfo, tradeDetail.getOrderId(), "");
        String dataValue = new Gson().toJson(queryReqData);

        String apiStorePw = paymentAccount.getHashKey();
        String tradeSign = DateTime.now().toString(SystemInstance.DATE_PI_PATTERN);

        megaWeiXinRequestData.setApiStorePw(apiStorePw);
        megaWeiXinRequestData.setTradeSign(tradeSign);
        megaWeiXinRequestData.setPaymentType(SINGLE_ORDER_QUERY);

        megaWeiXinRequestData.setDataValue(dataValue);

        return megaWeiXinRequestData;
    }

    //SHA256加密
    public static String encryptSHA256(String content) throws Exception {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(content.getBytes(StandardCharsets.UTF_8));
            String result = Base64.getEncoder().encodeToString(hash);
            LOGGER.info("encryptSHA256 :" + result);
            return result;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new RuntimeException("encryptSHA256 fail", e);
        }
    }

    //RSA加密 公鑰有分STAGE/PROD
    public String encryptRSA(String content) throws Exception {
        int blockSize = 53;
        byte[] data = content.getBytes("utf-8");
        int blockNum = (int) Math.ceil((1.0 * data.length) / blockSize);

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(MEGAWEIXIN_RSA_PUBLIC_KEY).getFile());
        RSAPrivateKey pk = (RSAPrivateKey) loadKey(file);

        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        cipher.init(Cipher.ENCRYPT_MODE, pk);

        for (int i = 0; i < blockNum; i++) {
            if (i < blockNum - 1) {
                out.write(cipher.doFinal(data, i * blockSize, blockSize));
            } else {
                out.write(cipher.doFinal(data, i * blockSize, data.length - i * blockSize));
            }
        }


        byte[] encryptedData = out.toByteArray();
        out.close();
        return Base64.getEncoder().encodeToString(encryptedData);
    }

    //取得回應 直到成功或相同請求重複6次 每次間隔2秒	
    public MegaWeiXinResponse getMegaWeiXinResponse(String requestData, int retry) throws Exception {

        MegaWeiXinResponse paymentResponse = null;
        MegaWeiXinResponseHeader responseHeader = null;

        do {
            if (retry >= 1 && retry < 6) {
                Thread.sleep(2000);
            }
            String paymentResult = HttpUtil.post(MEGAWEIXIN_URL, requestData);
            LOGGER.info("[Mega][Response]:" + paymentResult);
            paymentResponse = new Gson().fromJson(paymentResult, MegaWeiXinResponse.class);
            responseHeader = paymentResponse.getHeader();
            retry++;
        } while ("9997".equals(responseHeader.getStatusCode()) && retry <= 6);

        return paymentResponse;
    }

    //取得回應
    public MegaWeiXinResponse getMegaWeiXinResponse(String requestData) throws Exception {

        String paymentResult = HttpUtil.post(MEGAWEIXIN_URL, requestData);
        LOGGER.info("[Mega][Response]:" + paymentResult);
        MegaWeiXinResponse paymentResponse = new Gson().fromJson(paymentResult, MegaWeiXinResponse.class);

        return paymentResponse;
    }

    //依returnCode 取得 ResponseGeneralHeader
    public ResponseGeneralHeader getResponseHeader(String returnCode, String returnMessage, RequestHeader requestHeader, MessageSource messageSource, String serviceType) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        Locale locale = new Locale("zh_TW");

        if ("0000".equals(returnCode) || REFUND_SUCCESS_STATUS.equals(returnCode)) {
            responseGeneralHeader.setStatusCode("0000");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        }

        if (!("0000".equals(responseGeneralHeader.getStatusCode()))) {
            if (SystemInstance.TYPE_MICROPAY.equals(serviceType)) {
                responseGeneralHeader.setStatusCode("9998");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.9998", null, locale));
            } else if ("Refund".equals(serviceType)) {
                responseGeneralHeader.setStatusCode("7138");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7138", null, locale));
            } else if ("SingleOrderQuery".equals(serviceType)) {
                responseGeneralHeader.setStatusCode("7141");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7141", null, locale));
            }

        }

        responseGeneralHeader.setMethod("14010");

        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        return responseGeneralHeader;
    }

    private PrivateKey loadKey(File file) throws Exception {
        byte[] keyDataBytes = Files.readAllBytes(file.toPath());
        System.out.println("keyDataBytes " + keyDataBytes.length);
        String keyDataString = new String(keyDataBytes, StandardCharsets.UTF_8);

        keyDataString = keyDataString.replace(PKCS_1_PEM_HEADER, "");
        keyDataString = keyDataString.replace(PKCS_1_PEM_FOOTER, "");
        keyDataString = keyDataString.replaceAll("[\r\n]", "");
        byte[] keyData64 = Base64.getDecoder().decode(keyDataString);
        return readPkcs1PrivateKey(keyData64);
    }

    private PrivateKey readPkcs1PrivateKey(byte[] pkcs1Bytes) throws GeneralSecurityException {
        // We can't use Java internal APIs to parse ASN.1 structures, so we build a PKCS#8 key Java can understand
        int pkcs1Length = pkcs1Bytes.length;
        int totalLength = pkcs1Length + 22;
        byte[] pkcs8Header = new byte[]{
                0x30, (byte) 0x82, (byte) ((totalLength >> 8) & 0xff), (byte) (totalLength & 0xff), // Sequence + total length
                0x2, 0x1, 0x0, // Integer (0)
                0x30, 0xD, 0x6, 0x9, 0x2A, (byte) 0x86, 0x48, (byte) 0x86, (byte) 0xF7, 0xD, 0x1, 0x1, 0x1, 0x5, 0x0, // Sequence: 1.2.840.113549.1.1.1, NULL
                0x4, (byte) 0x82, (byte) ((pkcs1Length >> 8) & 0xff), (byte) (pkcs1Length & 0xff) // Octet string + length
        };
        byte[] pkcs8bytes = join(pkcs8Header, pkcs1Bytes);
        return readPkcs8PrivateKey(pkcs8bytes);
    }

    private byte[] join(byte[] byteArray1, byte[] byteArray2) {
        byte[] bytes = new byte[byteArray1.length + byteArray2.length];
        System.arraycopy(byteArray1, 0, bytes, 0, byteArray1.length);
        System.arraycopy(byteArray2, 0, bytes, byteArray1.length, byteArray2.length);
        return bytes;
    }

    private PrivateKey readPkcs8PrivateKey(byte[] pkcs8Bytes) throws GeneralSecurityException {
        KeyFactory keyFactory = KeyFactory.getInstance("RSA", "SunRsaSign");
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(pkcs8Bytes);
        try {
            return keyFactory.generatePrivate(keySpec);
        } catch (InvalidKeySpecException e) {
            throw new IllegalArgumentException("Unexpected key format!", e);
        }
    }

    private String getStoreOrderNoNew(QrcodeParameter qrcodeParameter) {
        String storeOrderNoNew;
        if (qrcodeParameter.getCodeType().equals("1")) {
            storeOrderNoNew = qrcodeParameter.getStoreOrderNo();
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            storeOrderNoNew = orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            storeOrderNoNew = orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
        } else {
            storeOrderNoNew = orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
        }
        return storeOrderNoNew;
    }

    private void createMegaWeixinTradeDetail(String storeOrderNoNew, Merchant merchant, String totalFee, String method) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        LocalDateTime time = LocalDateTime.now();
        String nowDate = df.format(time);
        String bodyRandom = String.format("%04d", new Random().nextInt(9999));
        String bankId = method.substring(1, 3);
        TradeDetail tradeDetail = new TradeDetail();
        tradeDetail.setMethod(method);
        tradeDetail.setServiceType("OLPAY");
        tradeDetail.setAccountId(merchant.getAccountId());
        tradeDetail.setCreateDate(nowDate);
        tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
        tradeDetail.setOrderId(storeOrderNoNew);
        tradeDetail.setDeviceRandomId(deviceService.getOne(merchant, "APP-001"));
        tradeDetail.setPaymentAccount(paymentAccountService.getOne(merchant.getAccountId(), Long.parseLong(bankId)));
        tradeDetail.setPayment(Long.parseLong(totalFee));
        tradeDetail.setDescription("Item-" + bodyRandom);
        tradeDetail.setOnSale(false);
        tradeDetail.setOriginalPrice(Double.parseDouble(totalFee));
        tradeDetail = tradeDetailService.save(tradeDetail);
        tradeDetailService.save(tradeDetail);
        LOGGER.info("[MegaWeixin" + bankId + "] TradeDetail save success");
    }

    public boolean createCookie(String orderId, HttpServletResponse response) {
        LOGGER.info("[SEND_COOKIE][MegaWeixin] starting... orderId = " + orderId);

        boolean result = true;
        try {
            Cookie cookie = new Cookie("MegaWeixinOLpayOrderId", orderId);
            cookie.setHttpOnly(true);
            cookie.setMaxAge(1800);
            cookie.setPath("/allpaypass/");
            response.addCookie(cookie);
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

}
