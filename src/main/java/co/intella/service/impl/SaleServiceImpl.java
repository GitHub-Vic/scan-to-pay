package co.intella.service.impl;

import co.intella.model.QSale;
import co.intella.model.Sale;
import co.intella.repository.SaleRepository;
import co.intella.service.SaleService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Andy Lin
 */
@Service
@Transactional
public class SaleServiceImpl implements SaleService {

    private final Logger LOGGER = LoggerFactory.getLogger(SaleServiceImpl.class);

    @Resource
    private SaleRepository saleRepository;

    public Sale login(String accountId, String pin) throws JsonProcessingException {

        QSale qSale = QSale.sale;
        BooleanExpression predicate = qSale.accountId.eq(accountId);
        Sale sale = saleRepository.findOne(predicate);
        LOGGER.info("[AUTH] " + accountId + ", " + pin + ", " + new ObjectMapper().writeValueAsString(sale));

        if(sale == null) {
            LOGGER.error("[AUTH] sales not found. " + accountId);
            return null;
        }

        if(sale.getPin().equals(pin)) {
            return sale;
        } else {
            LOGGER.error("[AUTH] password incorrect. " + accountId);
            return null;
        }
    }

    public List<Sale> queryWithLike(String accountId, String name) {
        QSale qSale = QSale.sale;
        BooleanExpression predicate = qSale.accountId.like('%' + accountId + '%')
                .and(qSale.name.like('%' + name + '%'));
        return Lists.newArrayList(saleRepository.findAll(predicate));
    }

    public List<Sale> getListByType(int type) {
        QSale qSale = QSale.sale;
        BooleanExpression predicate = qSale.type.eq(type);
        return Lists.newArrayList(saleRepository.findAll(predicate));
    }

    public List<Sale> getListAll() {
        return saleRepository.findAll();
    }

    public Sale getOneByAccountId(String accountId) {
        QSale qSale = QSale.sale;
        BooleanExpression predicate = qSale.accountId.eq(accountId);
        return saleRepository.findOne(predicate);
    }

    public Sale getOneById(long id) {
        QSale qSale = QSale.sale;
        BooleanExpression predicate = qSale.id.eq(id);
        return saleRepository.findOne(predicate);
    }

    public Sale save(Sale sale) {
        return saleRepository.save(sale);
    }

    public void delete(Sale sale) {

        // fixme mark delete
//        saleRepository.delete(sale);
    }

    public Sale update(Sale sale) {
        return saleRepository.save(sale);
    }
}
