package co.intella.service.impl;

import co.intella.domain.aliShangHai.*;
import co.intella.domain.integration.*;
import co.intella.exception.ServiceTypeNotSupportException;
import co.intella.model.*;
import co.intella.net.HttpRequestUtil;
import co.intella.service.*;
import co.intella.utility.AliShangHaiUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @author Andy Lin
 */
@Service
public class AlipayShangHaiServiceImpl implements AlipayShangHaiService {

    private final Logger LOGGER = LoggerFactory.getLogger(AlipayShangHaiServiceImpl.class);

    @Autowired
    private Environment env;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private MerchantService merchantService;

    @Autowired
    private MessageSource messageSource;

    @Resource
    private DeviceService deviceService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private OrderExtendInfoService orderExtendInfoService;

    @Value("${host.request.alipay.shanghai}")
    private String ALIPAY_SHANGHAI_URL;

    @Value("${api.url}")
    private String API_URL;

    @Value("${shanghai.alipay.rsa.public.key}")
    private String ALIPAY_SHANGHAI_RSA_PUBLIC_KEY;

    @Resource
    private RefundDetailService refundDetailService;

    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {
        LOGGER.info("[Alipay][ShangHai][doRequest]");

        String generalResponseData = SystemInstance.EMPTY_STRING;

        JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        PaymentAccount paymentAccount = paymentAccountService.getOne(integrateMchId, bankId);
        Merchant merchant = merchantService.getOne(integrateMchId);
        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

        String result;
        //todo: switch to choose corresponding service
        if (SystemInstance.TYPE_MICROPAY.equals(requestHeader.getServiceType())) {
            AliShangHaiMicropayRequest aliShangHaiMicropayRequest = new AliShangHaiMicropayRequest();
            aliShangHaiMicropayRequest.setQuantity(null);// N
            aliShangHaiMicropayRequest.setOutTradeNo(tradeDetail.getOrderId());
            aliShangHaiMicropayRequest.setTransName(tradeDetail.getDescription());
            aliShangHaiMicropayRequest.setCurrency("TWD");
            aliShangHaiMicropayRequest.setTotalFee(Long.toString(tradeDetail.getPayment()));
            aliShangHaiMicropayRequest.setBuyerIdentityCode(tradeDetail.getBarcode());
            aliShangHaiMicropayRequest.setIdentityCodeType("barcode");
            AliShangHaiRequestExtendParams extendParams = orderExtendInfoService.findOne(paymentAccount.getPaymentAccountRandomId().toString().replaceAll("-", ""));
            aliShangHaiMicropayRequest.setExtendParams(extendParams);

            AliShangHaiRequest aliShangHaiRequest = new AliShangHaiRequest();
            aliShangHaiRequest.setService("spotPay");
            aliShangHaiRequest.setMchId(paymentAccount.getAccount());
            aliShangHaiRequest.setCreateTime(requestHeader.getCreateTime());
            aliShangHaiRequest.setRequest(aliShangHaiMicropayRequest);

            Gson gson = new Gson();
            String pre = gson.toJson(aliShangHaiRequest);
            LOGGER.info("pre: " + pre);
            result = spotPay(pre);
            LOGGER.info("result: " + result);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);

            JsonObject jo = new Gson().fromJson(result, JsonObject.class);

            AliShangHaiMicropayResponse aliShangHaiMicropayResponse = new Gson().fromJson(jo.get("response"), AliShangHaiMicropayResponse.class);
            LOGGER.info("aliShangHaiMicropayResponse" + aliShangHaiMicropayResponse);

            AliShangHaiResponse aliShangHaiResponse = new Gson().fromJson(result, AliShangHaiResponse.class);
            LOGGER.info("aliShangHaiResponse" + aliShangHaiResponse);
            aliShangHaiResponse.setResponse(new Gson().fromJson(jo.get("response"), AliShangHaiMicropayResponse.class));
            LOGGER.info("aliShangHaiResponse" + aliShangHaiResponse);

            ResponseGeneralHeader responseGeneralHeader = new AliShangHaiUtil().getResponseHeader(
                    aliShangHaiResponse.getStatusCode(), aliShangHaiResponse.getMchId(), requestHeader,
                    messageSource, aliShangHaiMicropayResponse.getResultCode());

            IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();

            if ("SUCCESS".equals(aliShangHaiMicropayResponse.getResultCode())) {
                integratedMicropayResponseData.setSysOrderNo(aliShangHaiMicropayResponse.getAlipayTransId());
                integratedMicropayResponseData.setTotalFee(aliShangHaiMicropayResponse.getTransAmount());
            } else {
                integratedMicropayResponseData.setSysOrderNo(SystemInstance.EMPTY_STRING);
                integratedMicropayResponseData.setTotalFee(SystemInstance.EMPTY_STRING);
            }
            integratedMicropayResponseData.setStoreOrderNo(aliShangHaiMicropayRequest.getOutTradeNo());
            integratedMicropayResponseData.setAuthCode(aliShangHaiMicropayRequest.getBuyerIdentityCode());
            integratedMicropayResponseData.setPlatformRsp(new Gson().toJson(aliShangHaiResponse.getResponse()));

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedMicropayResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);
            LOGGER.info("integrate response ready :" + generalResponseData);

        } else if ("SingleOrderQuery".equals(requestHeader.getServiceType())) {
            LOGGER.info("[Alipay][ShangHai][SingleOrderQuery][data]" + data.getAsString());

            if (tradeDetail.getServiceType().equals(SystemInstance.TYPE_MICROPAY)) {
                paymentAccount = paymentAccountService.getOneByDao(integrateMchId, bankId);
            }
            AliShangHaiQueryRequest aliShangHaiQueryRequest = new AliShangHaiQueryRequest();
            aliShangHaiQueryRequest.setOutTradeNo(tradeDetail.getOrderId());

            AliShangHaiRequest aliShangHaiRequest = new AliShangHaiRequest();
            aliShangHaiRequest.setService("query");
            aliShangHaiRequest.setMchId(paymentAccount.getAccount());
            aliShangHaiRequest.setCreateTime(requestHeader.getCreateTime());
            aliShangHaiRequest.setRequest(aliShangHaiQueryRequest);

            Gson gson = new Gson();
            String queryPre = gson.toJson(aliShangHaiRequest);
            LOGGER.info("queryPre: " + queryPre);
            result = query(queryPre);
            LOGGER.info("result: " + result);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);
            JsonObject jo = new Gson().fromJson(result, JsonObject.class);

            AliShangHaiQueryResponse aliShangHaiQueryResponse = new Gson().fromJson(jo.get("response"), AliShangHaiQueryResponse.class);
            LOGGER.info("aliShangHaiQueryResponse" + aliShangHaiQueryResponse);

            AliShangHaiResponse aliShangHaiResponse = new Gson().fromJson(result, AliShangHaiResponse.class);
            LOGGER.info("aliShangHaiResponse" + aliShangHaiResponse);
            aliShangHaiResponse.setResponse(new Gson().fromJson(jo.get("response"), AliShangHaiQueryResponse.class));
            LOGGER.info("aliShangHaiResponse" + aliShangHaiResponse);

            ResponseGeneralHeader responseGeneralHeader = new AliShangHaiUtil().getResponseHeader(
                    aliShangHaiResponse.getStatusCode(), aliShangHaiResponse.getMchId(), requestHeader,
                    messageSource, aliShangHaiQueryResponse.getResultCode());
            responseGeneralHeader.setMchId(integrateMchId);

            tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());
            IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();
            integratedSingleOrderQueryResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
            integratedSingleOrderQueryResponseData.setStoreOrderNo(tradeDetail.getOrderId());
            integratedSingleOrderQueryResponseData.setDetail(tradeDetail.getDetail());
            integratedSingleOrderQueryResponseData.setStoreInfo(tradeDetail.getStoreInfo());
            integratedSingleOrderQueryResponseData.setFeeType("TWD");
            integratedSingleOrderQueryResponseData.setTotalFee(Long.toString(tradeDetail.getPayment()));

            integratedSingleOrderQueryResponseData.setPaidAt(tradeDetail.getCreateDate());
            integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());
            integratedSingleOrderQueryResponseData.setRefundedMsg(tradeDetail.getRefundStatus());
            RefundDetail refundDetail = refundDetailService.getOneByOrderIdAndMethod(tradeDetail.getOrderId(), tradeDetail.getMethod());
            if (Objects.nonNull(refundDetail)) {
                integratedSingleOrderQueryResponseData.setRefundedAt(refundDetail.getCreateTime());
            }

            if (SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus()) && SystemInstance.TRADE_REFUND_SUCCESS.equals(tradeDetail.getRefundStatus())) {
                integratedSingleOrderQueryResponseData.setOrderStatus("3");
            } else if (SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                integratedSingleOrderQueryResponseData.setOrderStatus("1");
            } else if (SystemInstance.TRADE_FAIL.equals(tradeDetail.getStatus())) {
                integratedSingleOrderQueryResponseData.setOrderStatus("2");
            } else if (SystemInstance.TRADE_WAITING.equals(tradeDetail.getStatus())) {
                integratedSingleOrderQueryResponseData.setOrderStatus("0");
            }

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedSingleOrderQueryResponseData);
            generalResponseData = new Gson().toJson(responseGeneralBody);

            LOGGER.info("integrate response ready :" + generalResponseData);

        } else if ("Refund".equals(requestHeader.getServiceType())) {
            LOGGER.info("[Alipay][ShangHai][Refund][data]" + data.getAsString());

            if (!Long.toString(tradeDetail.getPayment()).equals(requestData.get("RefundFee").getAsString())) {
                throw new ServiceTypeNotSupportException("we don't provide partial refund");
            }
            if (tradeDetail.getServiceType().equals(SystemInstance.TYPE_MICROPAY)) {
                paymentAccount = paymentAccountService.getOneByDao(integrateMchId, bankId);
            }
            AliShangHaiRefundRequest aliShangHaiRefundRequest = new AliShangHaiRefundRequest();
            aliShangHaiRefundRequest.setOutTradeNo(tradeDetail.getOrderId());
            aliShangHaiRefundRequest.setPartnerRefundId(requestData.get("StoreRefundNo").getAsString());
            aliShangHaiRefundRequest.setRefundAmount(Long.toString(tradeDetail.getPayment()));
            aliShangHaiRefundRequest.setCurrency("TWD");

            AliShangHaiRequest aliShangHaiRequest = new AliShangHaiRequest();
            aliShangHaiRequest.setService("refund");
            aliShangHaiRequest.setMchId(paymentAccount.getAccount());
            aliShangHaiRequest.setCreateTime(requestHeader.getCreateTime());
            aliShangHaiRequest.setRequest(aliShangHaiRefundRequest);

            Gson gson = new Gson();
            String refundPre = gson.toJson(aliShangHaiRequest);
            LOGGER.info("refundPre: " + refundPre);
            result = refund(refundPre);
            LOGGER.info("result: " + result);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);
            JsonObject jo = new Gson().fromJson(result, JsonObject.class);

            AliShangHaiRefundResponse aliShangHaiRefundResponse = new Gson().fromJson(jo.get("response"), AliShangHaiRefundResponse.class);
            LOGGER.info("aliShangHaiRefundResponse" + aliShangHaiRefundResponse);

            AliShangHaiResponse aliShangHaiResponse = new Gson().fromJson(result, AliShangHaiResponse.class);
            LOGGER.info("aliShangHaiResponse" + aliShangHaiResponse);
            aliShangHaiResponse.setResponse(new Gson().fromJson(jo.get("response"), AliShangHaiRefundResponse.class));
            LOGGER.info("aliShangHaiResponse" + aliShangHaiResponse);

            ResponseGeneralHeader responseGeneralHeader = new AliShangHaiUtil().getResponseHeader(
                    aliShangHaiResponse.getStatusCode(), aliShangHaiResponse.getMchId(), requestHeader,
                    messageSource, aliShangHaiRefundResponse.getResultCode());
            responseGeneralHeader.setMchId(integrateMchId);

            IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();
            integratedRefundResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
            integratedRefundResponseData.setStoreRefundNo(tradeDetail.getOrderId());
            integratedRefundResponseData.setSysRefundNo(tradeDetail.getStoreRefundId());
            integratedRefundResponseData.setPlatformRsp(new Gson().toJson(aliShangHaiResponse.getResponse()));

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedRefundResponseData);
            generalResponseData = new Gson().toJson(responseGeneralBody);

            LOGGER.info("integrate response ready :" + generalResponseData);

        } else if ("Cancel".equals(requestHeader.getServiceType())) {
            LOGGER.info("[Alipay][ShangHai][Cancel][data]" + data.getAsString());

            if (tradeDetail.getServiceType().equals(SystemInstance.TYPE_MICROPAY)) {
                paymentAccount = paymentAccountService.getOneByDao(integrateMchId, bankId);
            }
            AliShangHaiCancelRequest aliShangHaiCancelRequest = new AliShangHaiCancelRequest();
            aliShangHaiCancelRequest.setOutTradeNo(tradeDetail.getOrderId());

            AliShangHaiRequest aliShangHaiRequest = new AliShangHaiRequest();
            aliShangHaiRequest.setService("cancel");
            aliShangHaiRequest.setMchId(paymentAccount.getAccount());
            aliShangHaiRequest.setCreateTime(requestHeader.getCreateTime());
            aliShangHaiRequest.setRequest(aliShangHaiCancelRequest);

            Gson gson = new Gson();
            String cancelPre = gson.toJson(aliShangHaiRequest);
            LOGGER.info("cancelPre: " + cancelPre);
            result = cancel(cancelPre);
            LOGGER.info("result: " + result);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);
            JsonObject jo = new Gson().fromJson(result, JsonObject.class);

            AliShangHaiCancelResponse aliShangHaiCancelResponse = new Gson().fromJson(jo.get("response"), AliShangHaiCancelResponse.class);
            LOGGER.info("aliShangHaiCancelResponse" + aliShangHaiCancelResponse);

            AliShangHaiResponse aliShangHaiResponse = new Gson().fromJson(result, AliShangHaiResponse.class);
            LOGGER.info("aliShangHaiResponse" + aliShangHaiResponse);
            aliShangHaiResponse.setResponse(new Gson().fromJson(jo.get("response"), AliShangHaiCancelResponse.class));
            LOGGER.info("aliShangHaiResponse" + aliShangHaiResponse);

            ResponseGeneralHeader responseGeneralHeader = new AliShangHaiUtil().getResponseHeader(
                    aliShangHaiResponse.getStatusCode(), aliShangHaiResponse.getMchId(), requestHeader,
                    messageSource, aliShangHaiCancelResponse.getResultCode());
            responseGeneralHeader.setMchId(integrateMchId);

            IntegratedCancelResponseData integratedCancelResponseData = new IntegratedCancelResponseData();
            integratedCancelResponseData.setStoreOrderNo(tradeDetail.getOrderId());

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedCancelResponseData);
            generalResponseData = new Gson().toJson(responseGeneralBody);

            LOGGER.info("integrate response ready :" + generalResponseData);

        }
        return generalResponseData;
    }

    public String sendRequest(String request) {
        LOGGER.info("[Alipay][ShangHai][SendRequest]" + request);
        String result = null;
        try {
            result = HttpRequestUtil.post(ALIPAY_SHANGHAI_URL, request);
        } catch (Exception e) {
            LOGGER.error("[Alipay][ShangHai][SendRequest]" + e.getMessage());
        }
// todo:  following
//       boolean isValid = checkSign(result);

        return result;
    }
//
//    private boolean checkSign(String result) {
//
//        JsonObject jo = new Gson().fromJson(result, JsonObject.class);
//        String sign = jo.get("sign").getAsString();
//        String plainText = new Gson().toJson(jo);
//        PublicKey publicKey = null;
//
//        try {
//            publicKey = KeyReader.loadPublicKeyFromPEM(ALIPAY_SHANGHAI_RSA_PUBLIC_KEY);
//        } catch (Exception e) {
//            LOGGER.error("[Alipay][ShangHai][LOAD][KEY] error : " + ALIPAY_SHANGHAI_RSA_PUBLIC_KEY + "\\n" + e.getMessage());
//        }
//
//        try {
//            return verify(plainText, sign, publicKey);
//        } catch (Exception e) {
//            LOGGER.error("[Alipay][ShangHai][VERIFY][SIGN] error : " + e.getMessage());
//            return false;
//        }
//    }
//
//    public static boolean verify(String plainText, String signature, PublicKey publicKey) throws Exception {
//
//        Signature publicSignature = Signature.getInstance("SHA1WithRSA");
//        publicSignature.initVerify(publicKey);
//        publicSignature.update(plainText.getBytes("UTF-8"));
//
//        byte[] signatureBytes = Base64.getDecoder().decode(signature);
//
//        return publicSignature.verify(signatureBytes);
//    }

    public AliShangHaiRequest prepareAliPayShangHaiReserveRequestData(Integer totalFee, String orderId, PaymentAccount paymentAccount) {

        AliShangHaiOLpayRequest aliShangHaiOLpayRequest = new AliShangHaiOLpayRequest();
        aliShangHaiOLpayRequest.setOutTradeNo(orderId);
        aliShangHaiOLpayRequest.setNotifyUrl(API_URL + "allpaypass/api/alipayshanghai/notify");
        aliShangHaiOLpayRequest.setSubject("1");
        aliShangHaiOLpayRequest.setTotalFee(Integer.toString(totalFee));
        aliShangHaiOLpayRequest.setCurrency("TWD");
        aliShangHaiOLpayRequest.setTransCurrency("TWD");
        AliShangHaiRequestExtendParams extendParams = orderExtendInfoService.findOne(paymentAccount.getPaymentAccountRandomId().toString().replaceAll("-", ""));
        aliShangHaiOLpayRequest.setExtendParams(extendParams);

        AliShangHaiRequest aliShangHaiRequest = new AliShangHaiRequest();
        aliShangHaiRequest.setService("preCreate");
        aliShangHaiRequest.setMchId(paymentAccount.getAccount());
        aliShangHaiRequest.setCreateTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        aliShangHaiRequest.setRequest(aliShangHaiOLpayRequest);
        LOGGER.info("[Alipay][ShangHai][prepareAliPayShangHaiReserveRequestData] : " + new Gson().toJson(aliShangHaiRequest));

        return aliShangHaiRequest;
    }

    @Override
    public String createOrder(TradeDetail tradeDetail, QrcodeParameter qrcodeParameter, Integer totalFee, String storeOrderNoNew, PaymentAccount paymentAccount) throws Exception {
        //mapping
        AliShangHaiRequest aliShangHaiRequest = prepareAliPayShangHaiReserveRequestData(Integer.valueOf(totalFee), storeOrderNoNew, paymentAccount);
        AliShangHaiResponse aliShangHaiResponse;
        AliShangHaiOLpayResponse aliShangHaiOLpayResponse;
        try {
            String result = preCreate(aliShangHaiRequest);
            LOGGER.info("[result]" + result);
            JsonObject jo = new Gson().fromJson(result, JsonObject.class);

            aliShangHaiOLpayResponse = new Gson().fromJson(jo.get("response"), AliShangHaiOLpayResponse.class);
            LOGGER.info("aliShangHaiOLpayResponse " + aliShangHaiOLpayResponse);

            aliShangHaiResponse = new Gson().fromJson(result, AliShangHaiResponse.class);
            LOGGER.info("aliShangHaiResponse " + aliShangHaiResponse);
            aliShangHaiResponse.setResponse(new Gson().fromJson(jo.get("response"), AliShangHaiOLpayResponse.class));
            LOGGER.info("aliShangHaiResponse " + aliShangHaiResponse);

        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("[aliShangHai][call camel exception]" + e.getMessage());
            return "";
        }
        if ("SUCCESS".equals(aliShangHaiOLpayResponse.getResultCode())) {
            try {
                //save trade
                tradeDetail = setOnlineTradeDetailRequest(aliShangHaiRequest, paymentAccount, qrcodeParameter, storeOrderNoNew);
                LOGGER.info("[tradeDetail]" + new Gson().toJson(tradeDetail));
            } catch (Exception e) {
                LOGGER.error("[aliShangHai][set trade exception]" + e.getMessage());
                e.printStackTrace();
            }
            String paymentUrl = aliShangHaiOLpayResponse.getQrCode();
            return paymentUrl;
        } else {
            LOGGER.info("[aliShangHai][aliShangHaiOLpayResponse not SUCCESS]" + aliShangHaiOLpayResponse.getResultCode());
        }
        return "";
    }

    private TradeDetail setOnlineTradeDetailRequest(AliShangHaiRequest aliShangHaiRequest, PaymentAccount paymentAccount, QrcodeParameter qrcodeParameter, String orderId) {
        LOGGER.info("enter trade detail request:" + new Gson().toJson(aliShangHaiRequest));

        String preCreate = new Gson().toJson(aliShangHaiRequest);
        LOGGER.info("preCreate: " + preCreate);
        JsonObject jo = new Gson().fromJson(preCreate, JsonObject.class);

        AliShangHaiOLpayRequest aliShangHaiOLpayRequest = new Gson().fromJson(jo.get("request"), AliShangHaiOLpayRequest.class);
        LOGGER.info("aliShangHaiOLpayRequest" + aliShangHaiOLpayRequest);

        Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());
        TradeDetail tradeDetail = tradeDetailService.getOne(orderId);
        Integer amount = Integer.valueOf(aliShangHaiOLpayRequest.getTotalFee());
        LOGGER.info("[tradeDetail]:" + tradeDetail);
        if (tradeDetail != null) {
            return tradeDetail;
        } else {
            tradeDetail = new TradeDetail();
        }
        tradeDetail.setAccountId(merchant.getAccountId());
        tradeDetail.setMethod("12520");
        tradeDetail.setPaymentAccount(paymentAccount);
        tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
        tradeDetail.setOrderId(orderId);
        tradeDetail.setDeviceRandomId(deviceService.getOne(merchant, "AlipayShangHai"));
        tradeDetail.setServiceType("OLPay");
        tradeDetail.setQrcodeToken(qrcodeParameter.getShortId());
        tradeDetail.setDescription(qrcodeParameter.getBody());
        tradeDetail.setPayment(amount);
        tradeDetail.setQrcodeToken(qrcodeParameter.getShortId());
        tradeDetail.setSystemOrderId(orderId);

        if (qrcodeParameter.getOnSaleTotalFee() == amount) {
            tradeDetail.setOnSale(true);
        } else {
            tradeDetail.setOnSale(false);
        }
        tradeDetail.setOriginalPrice(qrcodeParameter.getTotalFee());

        try {
            LOGGER.info("save online trade detail : " + new ObjectMapper().writeValueAsString(tradeDetail));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            LOGGER.info("save online trade detail error!");
        }
        tradeDetail = tradeDetailService.save(tradeDetail);
        return tradeDetail;
    }

    public String preCreate(AliShangHaiRequest aliShangHaiRequest) {

        LOGGER.info("[Alipay][ShangHai][OLPAY][REQUEST] content: " + new Gson().toJson(aliShangHaiRequest));
        String result = null;
        try {
            result = sendRequest(new Gson().toJson(aliShangHaiRequest));
            LOGGER.info("[Alipay][ShangHai][OLPAY][RESPONSE] result: " + result);
        } catch (Exception e) {
            LOGGER.error("[FAIL][Alipay][ShangHai][OLPAY]" + e.getStackTrace());
        }
        return result;
    }

    public String query(String aliShangHaiRequest) {
        LOGGER.info("[Alipay][ShangHai][SingleOrderQuery]" + aliShangHaiRequest);
        String result = null;
        try {
            result = sendRequest(aliShangHaiRequest);
            LOGGER.info("result" + result);
        } catch (Exception e) {
            LOGGER.error("[Alipay][ShangHai][SingleOrderQuery]" + e.getStackTrace());
        }
        return result;
    }

    public String refund(String aliShangHaiRequest) {
        LOGGER.info("[Alipay][ShangHai][Refund]" + aliShangHaiRequest);
        String result = null;
        try {
            result = sendRequest(aliShangHaiRequest);
            LOGGER.info("result" + result);
        } catch (Exception e) {
            LOGGER.error("[Alipay][ShangHai][Refund]" + e.getStackTrace());
        }
        return result;
    }

    public String spotPay(String aliShangHaiRequest) {
        LOGGER.info("[Alipay][ShangHai][MICROPAY]" + aliShangHaiRequest);
        String result = null;
        try {
            result = sendRequest(aliShangHaiRequest);
            LOGGER.info("result" + result);
        } catch (Exception e) {
            LOGGER.error("[Alipay][ShangHai][MICROPAY]" + e.getStackTrace());
        }
        return result;
    }

    public String cancel(String aliShangHaiRequest) {
        LOGGER.info("[Alipay][ShangHai][Cancel]" + aliShangHaiRequest);
        String result = null;
        try {
            result = sendRequest(aliShangHaiRequest);
            LOGGER.info("result" + result);
        } catch (Exception e) {
            LOGGER.error("[Alipay][ShangHai][Cancel]" + e.getStackTrace());
        }
        return result;
    }

//    private String getSign(LinkedHashMap<String, String> map) throws Exception {
//
//        String requestJson = (new Gson()).toJson(map);
//
//        Signature signature = Signature.getInstance("SHA256withRSA");
//
//        ClassLoader classLoader = getClass().getClassLoader();
//        PublicKey publicKey = KeyReader.loadPublicKeyFromPEM("key/shangHai_alipay_rsa_public_key.pem");
//
//        signature.initSign(publicKey);
//        signature.update(requestJson.getBytes());
//
//        byte[] sign = signature.sign();
//        return  Base64.getEncoder().encodeToString(sign);
//
//    }

//    public String sslNoVerify(String ALIPAY_SHANGHAI_URL, String data) throws IOException, KeyManagementException, NoSuchAlgorithmException {
//        HttpsURLConnection.setDefaultHostnameVerifier(new AlipayShangHaiServiceImpl.NullHostNameVerifier());
//        SSLContext sc = SSLContext.getInstance("TLS");
//        sc.init(null, trustAllCerts, new SecureRandom());
//        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
//        URL url = new URL(ALIPAY_SHANGHAI_URL);
//        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//        conn.setRequestMethod("POST");
//        conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
//        conn.setConnectTimeout(130000);
//        conn.setReadTimeout(130000);
//        conn.setDoOutput(true);
//        conn.setDoInput(true);
//        conn.getOutputStream().write(data.getBytes("UTF-8"));
//        conn.getOutputStream().flush();
//        conn.getOutputStream().close();
//        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
//        StringBuilder text = new StringBuilder();
//
//        String line;
//        while ((line = rd.readLine()) != null) {
//            text.append(line);
//            if (line.length() < 1 && line.charAt(0) == '\ufeff') {
//                text.deleteCharAt(0);
//            }
//        }
//        return text.toString();
//    }
//
//    static TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
//        //        @Override
//        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
//            // TODO Auto-generated method stub
//        }
//
//        //        @Override
//        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
//            // TODO Auto-generated method stub
//        }
//
//        //        @Override
//        public X509Certificate[] getAcceptedIssuers() {
//            // TODO Auto-generated method stub
//            return null;
//        }
//    }};
//
//    public class NullHostNameVerifier implements HostnameVerifier {
//        /*
//         * (non-Javadoc)
//         *
//         * @see javax.net.ssl.HostnameVerifier#verify(java.lang.String,
//         * javax.net.ssl.SSLSession)
//         */
////        @Override
//        public boolean verify(String arg0, SSLSession arg1) {
//            // TODO Auto-generated method stub
//            return true;
//        }
//    }
}
