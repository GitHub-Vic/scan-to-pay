package co.intella.service.impl;

import co.intella.constant.ServiceType;
import co.intella.domain.focas.CreditAuthReq;
import co.intella.domain.focas.CreditCancelAuthReq;
import co.intella.domain.fubon.FuBonParamBean;
import co.intella.domain.integration.*;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import co.intella.service.*;
import co.intella.utility.EncryptUtil;
import co.intella.utility.FubonUtil;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Service
@Transactional
public class FuBonBankServiceimpl implements FuBonBankService {

    private final Logger LOGGER = LoggerFactory.getLogger(FuBonBankServiceimpl.class);

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private ApplePayService applePayService;

    @Resource
    private OrderService orderService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private PaymentService fuBonService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${payment.gateway.api.origin}")
    private String PAYMENT_GATEWAY_API_ORIGIN;

    @Value("${api.url}")
    private String API_URL;

    private Locale locale = new Locale("zh_TW");

    @Override
    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {

        String generalResponseData = SystemInstance.EMPTY_STRING;
        String integrateMchId = requestHeader.getMerchantId();
        PaymentAccount paymentAccount = paymentAccountService.getOne(integrateMchId, 38);

        // 跟富邦APPLEPAY一樣的退款
        if (ServiceType.CANCEL.equals(requestHeader.getServiceType())) {

            requestHeader.setServiceType(ServiceType.REFUND);

            LOGGER.info("[FubonBank][Refund][data]" + data.getAsString());

            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

            JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
            CreditCancelAuthReq creditCancelAuthReq = new CreditCancelAuthReq();

            creditCancelAuthReq.setLidm(requestData.get("StoreOrderNo").getAsString());
            Gson gson = new Gson();
            String platformResponse = refundPayment(paymentAccount, tradeDetail, gson);

            FuBonParamBean fuBonParamBeanRes = gson.fromJson(platformResponse, FuBonParamBean.class);
            ResponseGeneralHeader responseGeneralHeader = getResponseHeader(fuBonParamBeanRes, requestHeader);

            IntegratedRefundResponseData integratedRefundResponseData = getIntegratedRefundResponseData(
                    fuBonParamBeanRes, requestHeader, tradeDetail,
                    gson.fromJson(data.getAsString(), IntegratedRefundRequestData.class));
            integratedRefundResponseData.setPlatformRsp(platformResponse);

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedRefundResponseData);

            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, platformResponse, data);

            fuBonService.fubonECCapture(paymentAccount, tradeDetail, ServiceType.REFUND);

            generalResponseData = gson.toJson(responseGeneralBody);
            LOGGER.info("FUBON integrate response ready :" + generalResponseData);
        }else if("Payment".equals(requestHeader.getServiceType())){

            LOGGER.info("[FubonBank][Payment][data]" + data.getAsString());

            String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
            JsonObject requestJson = new Gson().fromJson(data.getAsString(), JsonObject.class);

            if (requestJson.has("Body")) {
                String bodyRandom = requestJson.get("Body").getAsString();

                    bodyRandom += requestJson.has("CardId") ? "-" + requestJson.get("CardId").getAsString().substring(12) : "";
                requestJson.remove("Body");
                requestJson.addProperty("Body", bodyRandom);
                data = new JsonPrimitive(new Gson().toJson(requestJson));
            }

            LOGGER.info("[dispatch credit][fubonbank3D][" + sessionId + "]");
            requestHeader.setMethod("23800");

            try {
                generalResponseData = this.creditAuth(requestHeader, data, integrateMchId, null);

                LOGGER.info("generalResponseData before:" + generalResponseData);
                JsonObject jsonObject = new Gson().fromJson(generalResponseData, JsonObject.class);
                JsonObject resdata = new Gson().fromJson(jsonObject.get("Data"), JsonObject.class);
                resdata.remove("platformRsp");
                resdata.addProperty("platformRsp",resdata.get("redirectUrl").getAsString());
                resdata.remove("redirectUrl");
                resdata.addProperty("serialNumber",0);
                jsonObject.add("Data", resdata);
                generalResponseData = new Gson().toJson(jsonObject);
                LOGGER.info("generalResponseData after:" + generalResponseData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return generalResponseData;
    }

    @Override
    public String creditAuth(RequestHeader requestHeader, JsonPrimitive data, String integrateMchId, String clientIP)
            throws Exception {

        LOGGER.info("[FubonBank3D][Payment][creditAuth]");
        PaymentAccount paymentAccount = paymentAccountService.getOne(integrateMchId, 38); // 信用卡用bankId38
        String merchantId = paymentAccount.getAccount();
        TradeDetail fubonTradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

        CreditAuthReq creditAuthReq = new Gson().fromJson(data.getAsString(), CreditAuthReq.class);
        String orderId = creditAuthReq.getLidm();
        String pan = creditAuthReq.getPan();
        String cvv = creditAuthReq.getCvv();
        double transAmt = Double.parseDouble(creditAuthReq.getPurchAmt());
        String expDate = creditAuthReq.getExpiry().substring(2, 4) + creditAuthReq.getExpiry().substring(0, 2);// 2019
        // 08 為
        // 0819
        creditAuthReq.setPan(pan.substring(0, 6) + "*************" + pan.substring(pan.length() - 4));
        LOGGER.info("[FubonBank3D][Payment] " + new Gson().toJson(creditAuthReq));

        FuBonParamBean fuBonParamBean = fuBonParamBeanSetting(merchantId, paymentAccount, orderId, pan, cvv, expDate, transAmt);
        String fubonRequest = new Gson().toJson(fuBonParamBean);
        LOGGER.info("[FubonBank3D][Request]" + fubonRequest);
        FuBonParamBean fuBonParamRes = null;
        String fubonResponse = SystemInstance.EMPTY_STRING;
        try {
            fuBonParamRes = restTemplate.postForObject(PAYMENT_GATEWAY_API_ORIGIN + "/fubon/api/v1/3D/auth", fuBonParamBean, FuBonParamBean.class);
            fubonResponse = new Gson().toJson(fuBonParamRes);
            LOGGER.info("[FubonBank3D][Response]" + fubonResponse);
        } catch (Exception e) {
            LOGGER.info("[FubonBank3D][EXCEPTION][HttpServerErrorException]" + " FubonResponse error - " + e.getMessage());
            return messageSource.getMessage("response.8017", null, locale);
        }

        String paypage = StringUtils.defaultIfBlank(fuBonParamRes.getPayPage(), "").replaceAll(".*src=\\\"([^\\\"]*)\\\".*", "$1");
        if (SystemInstance.EMPTY_STRING.equals(paypage)) {
            fubonTradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            paypage = String.format("/allpaypass/api/payment/order/result?orderId=%s", EncryptUtil.encryptSimple(orderId));
        }
        //儲存3D驗證資料
        fubonTradeDetail.setTxParams(fubonResponse);
        boolean is17Pay = fuBonService.checkPaymentAccountIs17Pay(paymentAccount);
        fubonTradeDetail.setComment(is17Pay ? "NewTaipei17Pay" : null);
        fubonTradeDetail.setUserId(creditAuthReq.getPan().substring(0, 6) + "*************" + creditAuthReq.getPan().substring(creditAuthReq.getPan().length() - 4));
        tradeDetailService.save(fubonTradeDetail);
        return convertPaymentResponse(orderId, fubonResponse, String.valueOf(transAmt), paypage,integrateMchId, fubonTradeDetail);
    }

    @Override
    public String fubonBankNotify(String orderId) throws Exception {
        FuBonParamBean fubonRes = null;

        TradeDetail tradeDetail = tradeDetailService.getOne(orderId);

        if (tradeDetail != null) {
            try {
                //拿到3D驗證資料
                FuBonParamBean fubonReq = new Gson().fromJson(tradeDetail.getTxParams(), FuBonParamBean.class);
                fubonRes = restTemplate.postForObject(PAYMENT_GATEWAY_API_ORIGIN + "/fubon/api/v1/3D/get3DResponse", fubonReq, FuBonParamBean.class);
                LOGGER.info("[FubonBank3D][Response] " + new Gson().toJson(fubonRes));
                updateFubonBankTradeDetail(fubonRes, tradeDetail);
            } catch (Exception e) {
                LOGGER.info("[FubonBank3D][EXCEPTION][HttpServerErrorException] " + " FubonResponse error - " + e.getMessage());
                return messageSource.getMessage("response.7315", null, locale);
            }
        }
        return String.format("/allpaypass/api/payment/order/result?orderId=%s", EncryptUtil.encryptSimple(orderId));
    }

    public FuBonParamBean fuBonParamBeanSetting(String merchantId, PaymentAccount paymentAccount, String orderId, String pan, String cvv, String expDate, double transAmt) {
        FuBonParamBean fuBonParamBean = new FuBonParamBean();
        fuBonParamBean.setMerchantID(merchantId);
        fuBonParamBean.setTerminalID(paymentAccount.getAccountTerminalId());
        fuBonParamBean.setOrderID(orderId);
        fuBonParamBean.setPAN(pan);
        fuBonParamBean.setCVV2(cvv);
        fuBonParamBean.setCurrencyCode("901");// 幣別901為台幣
        fuBonParamBean.setExpireDate(expDate);
        fuBonParamBean.setTransCode("00");
        fuBonParamBean.setTransAmt(transAmt);
        fuBonParamBean.setTransMode(0);

        fuBonParamBean.setNotifyURL(API_URL + "allpaypass/api/fubonbank/Notify");
        fuBonParamBean.setNotifyParam("orderId=" + orderId);
        fuBonParamBean.setNotifyType("API");

        return fuBonParamBean;
    }

    public void updateFubonBankTradeDetail(FuBonParamBean fubonRes, TradeDetail tradeDetail) {
        LOGGER.info("[RESPONSE][FubonBank3D][ResCode] " + fubonRes.getResponseCode());
        tradeDetail.setTxParams(fubonRes.getApproveCode().trim());
        tradeDetail.setPlatformPaidDate(String.format("%s %s", fubonRes.getTransDate(), fubonRes.getTransTime()));
        if (FubonUtil.isPaymentApproved(fubonRes)) {
            tradeDetail.setSystemOrderId(fubonRes.getSysOrderID());
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
        } else {
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
        }
        tradeDetailService.save(tradeDetail);

        if (SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
            PaymentAccount paymentAccount = tradeDetail.getPaymentAccount();
            if (!fuBonService.checkPaymentAccountIs17Pay(paymentAccount)) {
                paymentAccount = fuBonService.configedPermitCode(paymentAccount);
                fuBonService.fubonECCapture(paymentAccount, tradeDetail, "FuBon3D");
            }
            appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
        }
    }

    public String refundPayment(PaymentAccount paymentAccount, TradeDetail tradeDetail, Gson gson) {

        Map<String, String> fuBonParam = new HashMap<String, String>();
        fuBonParam.put("merchantID", paymentAccount.getAccount());
        fuBonParam.put("terminalID", paymentAccount.getAccountTerminalId());
        fuBonParam.put("permitCode", paymentAccount.getHashIV());
        fuBonParam.put("orderID", tradeDetail.getOrderId());
        fuBonParam.put("sysOrderID", tradeDetail.getSystemOrderId());

        if (tradeDetail.getPlatformPaidDate() != null) {
            String[] dateTime = tradeDetail.getPlatformPaidDate().split(" ");
            if (dateTime != null && dateTime.length == 2) {
                fuBonParam.put("transDate", dateTime[0]); // yyyyMMdd
                fuBonParam.put("transTime", dateTime[1]); // hhmmss
            }
        }

        fuBonParam.put("userDefine", tradeDetail.getTxParams());
        fuBonParam.put("transAmt", String.valueOf(tradeDetail.getPayment()));
        fuBonParam.put("currencyCode", String.valueOf(Currency.getInstance(Locale.TAIWAN).getNumericCode()));

        fuBonParam.put("notifyURL", API_URL + "allpaypass/api/fubonbank/Notify");
        fuBonParam.put("notifyParam", "orderId=" + tradeDetail.getOrderId());
        fuBonParam.put("notifyType", "API");

        LOGGER.info("[REQUEST][FubonBank3D][Refund] " + new Gson().toJson(fuBonParam));
        String result = restTemplate.postForObject(PAYMENT_GATEWAY_API_ORIGIN + "/fubon/api/v1/auth/refund", fuBonParam,
                String.class);
        LOGGER.info("[RESPONSE][FubonBank3D] result=>" + result);
        return result;
    }

    private ResponseGeneralHeader getResponseHeader(FuBonParamBean fuBonParamBeanRes, RequestHeader requestHeader) {

        ResponseGeneralHeader responseHeader = mapResponseToResponseGeneralHeader(fuBonParamBeanRes, messageSource);
        responseHeader.setMethod(requestHeader.getMethod());
        responseHeader.setMchId(requestHeader.getMerchantId());
        responseHeader.setServiceType(requestHeader.getServiceType());
        responseHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        return responseHeader;
    }

    private IntegratedRefundResponseData getIntegratedRefundResponseData(FuBonParamBean fuBonParamBeanRes, RequestHeader requestHeader, TradeDetail tradeDetail, IntegratedRefundRequestData integratedRefundRequestData) {

        IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();
        if (FubonUtil.isPaymentApproved(fuBonParamBeanRes)) {
            integratedRefundResponseData.setStoreOrderNo(tradeDetail.getOrderId());
            integratedRefundResponseData.setStoreRefundNo(integratedRefundRequestData.getOutRefundNo());
            integratedRefundResponseData.setSysRefundNo(fuBonParamBeanRes.getOrderID());
            integratedRefundResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
            integratedRefundResponseData.setRefundedAt(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        }

        return integratedRefundResponseData;
    }

    private ResponseGeneralHeader mapResponseToResponseGeneralHeader(FuBonParamBean fuBonParamRes, MessageSource messageSource) {

        ResponseGeneralHeader responseHeader = new ResponseGeneralHeader();
        if (FubonUtil.isPaymentApproved(fuBonParamRes)) {
            responseHeader.setStatusCode("0000");
            responseHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        } else {
            responseHeader.setStatusCode("9998");
            responseHeader.setStatusDesc(messageSource.getMessage("response.9998", null, locale));
        }
        return responseHeader;
    }

    public String convertPaymentResponse(String orderId, String fubonResponse, String totalFee, String redirectUrl,String integrateMchId,TradeDetail fubonTradeDetail) {

        ResponseGeneralHeader responseHeader = new ResponseGeneralHeader();

        if(SystemInstance.TRADE_FAIL.equals(fubonTradeDetail.getStatus())){
            responseHeader.setStatusCode("9998");
            responseHeader.setStatusDesc(messageSource.getMessage("response.9998", null, locale));
        }else{
            responseHeader.setStatusCode("0000");
            responseHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        }
        responseHeader.setMchId(integrateMchId);
        responseHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        responseHeader.setServiceType("Payment");
        responseHeader.setMethod("23800");

        IntegratedPaymentResponseData responseData = new IntegratedPaymentResponseData();
        responseData.setSysOrderNo(orderId);
        responseData.setStoreOrderNo(orderId);
        responseData.setTotalFee(totalFee);
        responseData.setFeeType("TWD");
        responseData.setDeviceInfo("T0000000");
        responseData.setBody("");
        responseData.setPlatformRsp(fubonResponse);
        responseData.setRedirectUrl(redirectUrl);

        ResponseGeneralBody responseBody = new ResponseGeneralBody();
        responseBody.setHeader(responseHeader);
        responseBody.setData(responseData);

        return new Gson().toJson(responseBody);
    }

}