package co.intella.service.impl;

import co.intella.model.INT_TB_CancelTradeDetail;
import co.intella.model.QINT_TB_CancelTradeDetail;
import co.intella.model.TradeDetail;
import co.intella.repository.INT_TB_CancelTradeDetailRepository;
import co.intella.service.INT_TB_CancelTradeDetailService;
import com.google.common.collect.Lists;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class INT_TB_CancelTradeDetailServiceImpl implements INT_TB_CancelTradeDetailService {

    @Resource
    private INT_TB_CancelTradeDetailRepository cancelTradeDetailRepository;

    @Override
    public INT_TB_CancelTradeDetail save(INT_TB_CancelTradeDetail cancelTradeDetail) {
        return cancelTradeDetailRepository.saveAndFlush(cancelTradeDetail);
    }

    @Override
    public List<INT_TB_CancelTradeDetail> getFailList() {
        QINT_TB_CancelTradeDetail qintTbCancelTradeDetail = QINT_TB_CancelTradeDetail.iNT_TB_CancelTradeDetail;
        BooleanExpression predicate = qintTbCancelTradeDetail.cancelStatus.eq("F");
        return Lists.newArrayList(cancelTradeDetailRepository.findAll(predicate));
    }

    @Override
    public INT_TB_CancelTradeDetail getOne(TradeDetail tradeDetail) {
        QINT_TB_CancelTradeDetail qintTbCancelTradeDetail = QINT_TB_CancelTradeDetail.iNT_TB_CancelTradeDetail;
        BooleanExpression predicate = qintTbCancelTradeDetail.orderId.eq(tradeDetail.getOrderId())
                .and(qintTbCancelTradeDetail.accountId.eq(tradeDetail.getAccountId()));
        return cancelTradeDetailRepository.findOne(predicate);
    }


}
