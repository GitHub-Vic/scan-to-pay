package co.intella.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.querydsl.core.types.dsl.BooleanExpression;

import co.intella.model.PaymentMethod;
import co.intella.model.QQrcodeParameter;
import co.intella.repository.PaymentMethodRepository;
import co.intella.repository.QrcodeParameterRepository;
import co.intella.service.PaymentMethodService;
@Service
public class PaymentMethodServiceImpl implements PaymentMethodService {
	 @Resource
	    private PaymentMethodRepository paymentMethodRepository;
	@Override
	public PaymentMethod getOne(String method) {
		
	        return paymentMethodRepository.getOne(method);
	}

}
