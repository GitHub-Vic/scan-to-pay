package co.intella.service.impl;

import co.intella.crypto.AesCryptoUtil;
import co.intella.domain.tcbank.*;
import co.intella.model.*;
import co.intella.net.Constant;
import co.intella.service.*;
import co.intella.utility.TcBankUtil;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonPrimitive;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.annotation.Resource;
import javax.crypto.SecretKey;

/**
 * @author Miles
 */
@Service
@Transactional
public class TcBankServiceImpl implements TcBankService {

    private final Logger LOGGER = LoggerFactory.getLogger(TcBankServiceImpl.class);

    @Value("${host.request.tcbank}")
    private String HOST;

    @Value("${api.url}")
    private String API_URL;

    @Resource
    private ConsumerTokenService consumerTokenService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private TradeDetailService tradeDetailService;

    public String payment(TCBCreditCardRequestData request) throws Exception {
//        String result;
//        MbecApiClient apiClient = getClient(request);
//
//        try {
//
//            LOGGER.info("[TCB][REQUEST] payment start...");
//            int returnCode = apiClient.postAuth();
//
//            if(returnCode == 1) {
//                TCBCreditCardResponseData responseData = getResponse(apiClient);
//                responseData.setHtml(apiClient.getHTML());
//                result = new Gson().toJson(responseData);
//
//                LOGGER.info("[TCB][RESPONSE] payment complete. " + result);
//                return result;
//            } else {
//                TCBCreditCardResponseData responseData = getResponse(apiClient);
//                result = new Gson().toJson(responseData);
//
//                LOGGER.info("[TCB][RESPONSE] payment complete." + result);
//                return result;
//            }
//        } catch (Exception e) {
//            LOGGER.error("[EXCEPTION][TCB-PAYMENT]" + e.toString());
//            TCBCreditCardResponseData responseData = getErrorResponse(apiClient, "May be timeout. " + e);
//            return new Gson().toJson(responseData);
//        }
        return "";
    }

    public String cancel(TCBCreditCardCancelRequestData request) throws Exception {
//        String result;
//
//        MbecApiClient apiClient = getCancelClient(request);
//
//        try {
//            LOGGER.info("[TCB][REQUEST] cancel start. " + new Gson().toJson(request));
//            int returnCode = apiClient.postAuth();
//            if(returnCode == 1) {
//                // todo 3D
//                TCBCreditCardResponseData responseData = getErrorResponse(apiClient, "ReturnCode equals 1");
//                return new Gson().toJson(responseData);
//            } else {
//                TCBCreditCardResponseData responseData = getResponse(apiClient);
//                result = new Gson().toJson(responseData);
//                LOGGER.info("[TCB][RESPONSE] cancel complete." + result);
//                return result;
//            }
//        } catch (Exception e) {
//            LOGGER.error("[EXCEPTION][TCB-CANCEL]" + e.toString());
//            TCBCreditCardResponseData responseData = getErrorResponse(apiClient, "May be timeout. " + e);
//            return new Gson().toJson(responseData);
//        }
        return "";
    }

    public String queryOrder(TCBCreditCardOrderQueryRequestData request) throws Exception {
//        String result;
//
//        MbecApiClient apiClient = getQueryClient(request);
//
//        try {
//            LOGGER.info("[TCB][REQUEST] queryOrder start. " + new Gson().toJson(request));
//            int returnCode = apiClient.postAuthQuery();
//            if(returnCode == 1) {
//                // todo 3D
//                TCBCreditCardResponseData responseData = getErrorResponse(apiClient, "ReturnCode equals 1");
//                return new Gson().toJson(responseData);
//            } else {
//                TCBCreditCardResponseData responseData = getResponse(apiClient);
//                result = new Gson().toJson(responseData);
//                LOGGER.info("[TCB][RESPONSE] queryOrder complete." + result);
//                return result;
//            }
//        } catch (Exception e) {
//            LOGGER.error("[EXCEPTION][TCB-QUERY-ORDER]" + e.toString());
//            TCBCreditCardResponseData responseData = getErrorResponse(apiClient, "May be timeout. " + e);
//            return new Gson().toJson(responseData);
//        }
        return "";
    }

    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {

        String result;
        String integrateMchId= requestHeader.getMerchantId();

        if("Payment".equals(requestHeader.getServiceType())) {

            TCBCreditCardRequestData requestData = new Gson().fromJson(data.getAsString(), TCBCreditCardRequestData.class);

            if(Strings.isNullOrEmpty(requestData.getPan()) && !Strings.isNullOrEmpty(requestData.getToken())) {
                ConsumerToken consumerToken = consumerTokenService.getOne(requestData.getTokenSeq());
                String token = requestData.getToken();
                String completeToken = combineToken(token, consumerToken);
                String panCode = decryptToken(completeToken, consumerToken);
                requestData.setPan(panCode);
            }

            PaymentAccount paymentAccount = paymentAccountService.getOne(requestHeader.getMerchantId(), 4);

            requestData.setMerchantId(paymentAccount.getAccount());
            requestData.setTransCode("00");
            //todo deal with transMode 1
            requestData.setTransMode("0");

            requestData.setPrivateData("mchId=" + requestHeader.getMerchantId() + "&orderId=" + requestData.getOrderId());
            requestData.setNotifyUrl(API_URL + "allpaypass/api/notify");

            String tcBankResponse = payment(requestData);

            TokenPair tokenPair = getNewToken(requestData.getPan(), integrateMchId);
            requestData.setToken(tokenPair.getToken());
            requestData.setTokenSeq(tokenPair.getId());

            tradeProcedureService.setTradeDetailRequest(requestHeader,data);
            result = TcBankUtil.convertTcbankResponse(tcBankResponse, requestData, requestHeader, data);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader,tcBankResponse, data);

            return result;

        } else if("Cancel".equals(requestHeader.getServiceType())) {

            TCBCreditCardCancelRequestData requestData = new Gson().fromJson(data.getAsString(), TCBCreditCardCancelRequestData.class);

            PaymentAccount paymentAccount = paymentAccountService.getOne(requestHeader.getMerchantId(), 4);


            requestData.setMerchantId(paymentAccount.getAccount());
            requestData.setTransCode("01");

            String tcBankResponse = cancel(requestData);

            tradeProcedureService.setTradeDetailRequest(requestHeader,data);
            result = TcBankUtil.convertTcbankCancelResponse(tcBankResponse, requestData, requestHeader, data);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, tcBankResponse, data);

            return result;

        } else if("SingleOrderQuery".equals(requestHeader.getServiceType()) || "OrderQuery".equals(requestHeader.getServiceType())) {
            TCBCreditCardOrderQueryRequestData requestData = new Gson().fromJson(data.getAsString(), TCBCreditCardOrderQueryRequestData.class);

            PaymentAccount paymentAccount = paymentAccountService.getOne(requestHeader.getMerchantId(), 4);

            requestData.setMerchantId(paymentAccount.getAccount());
            requestData.setTransCode("90");
            String tcBankResponse = queryOrder(requestData);

            TradeDetail localTradeDetail = tradeDetailService.getOne(requestHeader.getMerchantId(), requestData.getOrderId());
            return TcBankUtil.convertTcbankOrderQueryResponse(tcBankResponse, requestData, requestHeader, data, localTradeDetail);
        } else {
            return "Service Type Not Support.";
        }
    }

//    private MbecApiClient getCancelClient(TCBCreditCardCancelRequestData request) {
//        MbecApiClient apiClient = new MbecApiClient();
//        apiClient.setMERCHANTID(request.getMerchantId());
//        apiClient.setORDERID(request.getOrderId());
//        apiClient.setTRANSCODE(request.getTransCode());
//
//        apiClient.setNotifyURL("/mbec/HppResponse.jsp");
//        apiClient.setURL(HOST,"/mbec/APIRequest");
//        return apiClient;
//    }

//    private MbecApiClient getQueryClient(TCBCreditCardOrderQueryRequestData request) {
//
//        MbecApiClient apiClient = new MbecApiClient();
//        apiClient.setMERCHANTID(request.getMerchantId());
//        apiClient.setTERMINALID(request.getTerminalId());
//        apiClient.setORDERID(request.getOrderId());
//        apiClient.setTRANSCODE(request.getTransCode());
//        apiClient.setPAN(request.getPan());
//        apiClient.setEXPIREDATE(request.getExpireData());
//        apiClient.setEXTENNO(request.getExtenNo());
//        apiClient.setTRANSMODE(request.getTransMode());
//        apiClient.setINSTALLMENT(request.getInstall());
//
//        apiClient.setNotifyURL("/mbec/HppResponse.jsp");
//        apiClient.setURL(HOST,"/mbec/APIRequest");
//        return apiClient;
//    }

//    private MbecApiClient getClient(TCBCreditCardRequestData request) {
//
//        MbecApiClient apiClient = new MbecApiClient();
//        apiClient.setMERCHANTID(request.getMerchantId());
//        apiClient.setTERMINALID(request.getTerminalId());
//        apiClient.setORDERID(request.getOrderId());
//        apiClient.setTRANSCODE(request.getTransCode());
//        apiClient.setPAN(request.getPan());
//        apiClient.setEXPIREDATE(request.getExpireData());
//        apiClient.setEXTENNO(request.getExtenNo());
//        apiClient.setTRANSMODE(request.getTransMode());
//        apiClient.setINSTALLMENT("0");
//        apiClient.setTRANSAMT(request.getTransAmt());
//
//        // for notify
//        apiClient.setNotifyURL(request.getNotifyUrl()  + "?" + request.getPrivateData());
//        apiClient.setPrivateData(request.getPrivateData());
//
//        apiClient.setURL(HOST,"/mbec/APIRequest");
//        return apiClient;
//    }

//    private TCBCreditCardResponseData getResponse(MbecApiClient apiClient) {
//        TCBCreditCardResponseData responseData = new TCBCreditCardResponseData();
//        responseData.setMerchantId(apiClient.getMERCHANTID());
//        responseData.setTerminalId(apiClient.getTERMINALID());
//        responseData.setOrderId(apiClient.getORDERID());
//        responseData.setTransMode(apiClient.getTRANSMODE());
//        responseData.setTransDate(apiClient.getTRANSDATE());
//        responseData.setTransTime(apiClient.getTRANSTIME());
//        responseData.setTransAmt(apiClient.getTRANSAMT());
//        responseData.setApproveCode(apiClient.getAPPROVECODE());
//        responseData.setInstallmentType(apiClient.getINSTALLMENTTYPE());
//        responseData.setRedeemType(apiClient.getREDEEMTYPE());
//        responseData.setInstallment(apiClient.getINSTALLMENT());
//        responseData.setFee(apiClient.getFEE());
//        responseData.setFirstAmt(apiClient.getFIRSTAMT());
//        responseData.setEachAmt(apiClient.getEACHAMT());
//        responseData.setRedeemUsed(apiClient.getREDEEMUSED());
//        responseData.setRedeemBalance(apiClient.getREDEEMBALANCE());
//        responseData.setCreaditAmt(apiClient.getCREDITAMT());
//        responseData.setResponseCode(apiClient.getRESPONSECODE());
//        responseData.setResponseMessage(apiClient.getRESPONSEMSG());
//        responseData.setPrivateData(apiClient.getPrivateData());
//        return responseData;
//    }

//    private TCBCreditCardResponseData getErrorResponse(MbecApiClient apiClient, String message) {
//        TCBCreditCardResponseData responseData = new TCBCreditCardResponseData();
//        responseData.setMerchantId(apiClient.getMERCHANTID());
//        responseData.setTerminalId(apiClient.getTERMINALID());
//        responseData.setOrderId(apiClient.getORDERID());
//        responseData.setTransMode(apiClient.getTRANSMODE());
//        responseData.setTransDate(apiClient.getTRANSDATE());
//        responseData.setTransTime(message);
//        responseData.setTransAmt(apiClient.getTRANSAMT());
//        responseData.setApproveCode(message);
//        responseData.setFee(apiClient.getFEE());
//        responseData.setFirstAmt(apiClient.getFIRSTAMT());
//        responseData.setEachAmt(apiClient.getEACHAMT());
//        responseData.setRedeemUsed(apiClient.getREDEEMUSED());
//        responseData.setRedeemBalance(apiClient.getREDEEMBALANCE());
//        responseData.setCreaditAmt(apiClient.getCREDITAMT());
//        responseData.setResponseCode(message);
//        responseData.setResponseMessage("TC Bank Request " + message);
//        responseData.setPrivateData(apiClient.getPrivateData());
//        return responseData;
//    }

    private String combineToken(String token, ConsumerToken consumerToken) {

        String remote = token;
        String local = consumerToken.getToken();
        StringBuilder completeToken = new StringBuilder();

        for(int i = 0; i < consumerToken.getAlgorithm().length() ; i ++) {
            if('0' == consumerToken.getAlgorithm().charAt(i)) {
                completeToken.append(remote.charAt(0));
                remote = remote.substring(1);
            } else {
                completeToken.append(local.charAt(0));
                local = local.substring(1);
            }
        }

        return completeToken.toString();
    }

    private String decryptToken(String completeToken, ConsumerToken consumerToken) throws Exception {
        SecretKey secretKey = AesCryptoUtil.convertSecretKey(Base64.decode(consumerToken.getKeys()));
        return AesCryptoUtil.decrypt(secretKey, Constant.IV, Base64.decode(completeToken));
    }

    private TokenPair getNewToken(String pan, String accountId) throws Exception {

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        String aesKey = Base64.encode(secretKey.getEncoded());

        byte[] encryptPan = AesCryptoUtil.encrypt(secretKey, Constant.IV, pan);
        String encryptString = Base64.encode(encryptPan);

        StringBuilder remoteToken = new StringBuilder();
        StringBuilder localToken = new StringBuilder();

        String algorithm = generateMasks(encryptString.length());

        for(int i = 0; i < encryptString.length() ; i++) {
            if('0' == algorithm.charAt(i)) {
                remoteToken.append(encryptString.charAt(i));
            } else {
                localToken.append(encryptString.charAt(i));
            }
        }

        ConsumerToken consumerToken = new ConsumerToken();
        consumerToken.setAccountId(accountId);
        consumerToken.setAlgorithm(algorithm);
        consumerToken.setToken(localToken.toString());
        consumerToken.setKeys(aesKey);

        ConsumerToken entity = consumerTokenService.createOrUpdate(consumerToken);

        return new TokenPair(entity.getId(), remoteToken.toString());
    }


    private String generateMasks(int strLen) {

        char seeds[] = {'0','1'};
        char randStr[] = new char[strLen];
        for (int i=0;i< randStr.length;i++) {
            randStr[i] = seeds[(int)Math.round(Math.random() * (seeds.length - 1))];
        }

        return new String(randStr);
    }

}
