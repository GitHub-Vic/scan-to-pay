package co.intella.service.impl;

import co.intella.model.DiscountTrade;
import co.intella.service.DiscountTradeService;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DiscountTradeServiceImpl implements DiscountTradeService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Value("${host.dao}")
    private String DAO_URL;

    @Override
    public DiscountTrade save(DiscountTrade discountTrade) {

        String result = HttpUtil.doPostJson(DAO_URL + "api/discountTrade/save", JsonUtil.toJsonStr(discountTrade));
        return JsonUtil.parseJson(result, DiscountTrade.class);
    }
}
