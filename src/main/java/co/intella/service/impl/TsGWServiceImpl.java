package co.intella.service.impl;

import co.intella.domain.integration.*;
import co.intella.domain.tsgw.*;
import co.intella.exception.ServiceTypeNotSupportException;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import co.intella.service.*;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonPrimitive;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import javax.net.ssl.*;
import javax.xml.bind.DatatypeConverter;
import java.beans.PropertyDescriptor;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.*;

@Service
public class TsGWServiceImpl implements TsGWService {

    private final Logger LOGGER = LoggerFactory.getLogger(TsGWServiceImpl.class);

    @Resource
    private PaymentAccountService paymentAccountService;

    @Autowired
    private Environment env;

    @Autowired
    private MessageSource messageSource;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Value("${host.request.tsgw}")
    private String TSGW_URL;

    @Value("${api.url}")
    private String API_URL;

    public final static String ALIPAY_O = "ALIPAY_O";// 13320
    public final static String WEIXIN_O = "WEIXIN_O";// 13410

    public String doRequest(RequestHeader requestHeader, JsonPrimitive data, String gw) throws Exception {
        String generalResponseData = SystemInstance.EMPTY_STRING;

        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        PaymentAccount paymentAccount = paymentAccountService.getOneByDao(integrateMchId, bankId);

        if (SystemInstance.TYPE_MICROPAY.equals(requestHeader.getServiceType())) {

            if (ObjectUtils.isEmpty(paymentAccount)) {
                throw new ServiceTypeNotSupportException("Exception: The store's account isn't opened.");
            }
            LOGGER.info("[TSGW][Micropay][account]" + paymentAccount.getAccount());

            GwMerchantApiPayRequest paymentRequestData = new Gson().fromJson(data.getAsString(), GwMerchantApiPayRequest.class);

            paymentRequestData.setMerchantid(paymentAccount.getAccount());
            paymentRequestData.setGw(gw);
            //反掃storeid 為 正掃storeid + "-1"
            paymentRequestData.setStoreid(paymentAccount.getAppId() + "-1");

            paymentRequestData.setOrdername(StringUtils.isNotBlank(paymentRequestData.getOrdername()) ? paymentRequestData.getOrdername().replaceAll("[&=,'|]", "") : "");
            paymentRequestData.setOrdermemo(StringUtils.isNotBlank(paymentRequestData.getOrdermemo()) ? paymentRequestData.getOrdermemo().replaceAll("[&=,'|]", "") : "");
            paymentRequestData.setTimestamp(DateTime.now().toString(SystemInstance.DATE_PATTERN));

            String preSignUrl = toURLQueryStr(paymentRequestData);
            LOGGER.info(preSignUrl);
            //tokenkey = paymentAccount.getHashKey() + paymentAccount.getHashIV()
            String preSign = preSignUrl + paymentAccount.getHashKey() + paymentAccount.getHashIV();
            LOGGER.info(preSign);
            String sign = sha256(preSign);
            LOGGER.info(sign);
            paymentRequestData.setSign(sign);
            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
            String paymentResult = payment(paymentRequestData);
            LOGGER.info("[TSGW][Micropay][Response]:" + paymentResult);

            Map<String, String> resultMap = convertXml2Map(paymentResult);
            ObjectMapper mapper = new ObjectMapper(); // jackson's objectmapper
            GwMerchantApiPayResponse apiResponseData = mapper.convertValue(resultMap, GwMerchantApiPayResponse.class);
            //若交易回傳代碼為000則交易成功
            //若交易回傳代碼為以下四種 須查詢原訂單是否付款成功 其中714查無結果直到查詢6次才停止
            List<String> queryReturnCode = Arrays.asList("714", "715", "716", "799");

            //二段交易 查詢確認交易已完成
            if ("714".equals(apiResponseData.getReturncode())) {
                GwMerchantApiQueryRequest queryPayment = genApiQueryRequest(data.getAsString(), gw, paymentAccount, tradeDetail, "P");
                queryPayment.setId(tradeDetail.getOrderId());
                int retry = 0;

                //query次數為台新要求為防止短時間內多次查詢
                while (retry > 6 || "000".equals(apiResponseData.getReturncode())) {
                    Thread.sleep(10000);

                    String queryResult = singleOrderQuery(queryPayment);

                    resultMap = convertXml2Map(queryResult);
                    GwMerchantApiQueryResponse queryResp = mapper.convertValue(resultMap,
                            GwMerchantApiQueryResponse.class);

                    LOGGER.info("[TSGW][MicropayQuery][Result].." + queryResult);
                    if ("1".equals(queryResp.getStatus())) {
                        apiResponseData.setReturncode(queryResp.getReturnCode());
                        break;
                    }
                    retry++;
                }

            } else if (queryReturnCode.contains(apiResponseData.getReturncode())) {
                GwMerchantApiQueryRequest queryPayment = genApiQueryRequest(data.getAsString(), gw, paymentAccount, tradeDetail, "P");
                queryPayment.setId(tradeDetail.getOrderId());
                String queryResult = singleOrderQuery(queryPayment);

                LOGGER.info("[TSGW][MicropayQuery][Result].." + queryResult);
                resultMap = convertXml2Map(queryResult);

                GwMerchantApiQueryResponse queryResp = mapper.convertValue(resultMap, GwMerchantApiQueryResponse.class);
                if ("1".equals(queryResp.getStatus())) {
                    apiResponseData.setReturncode(queryResp.getReturnCode());
                }
            }

            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, new Gson().toJson(apiResponseData), data);

            ResponseGeneralHeader responseGeneralHeader = getResponseHeader(apiResponseData.getReturncode(), apiResponseData.getReturnmessage(), requestHeader, messageSource);

            requestHeader.setMerchantId(integrateMchId);
            IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();

            if ("000".equals(apiResponseData.getReturncode())) {
                integratedMicropayResponseData.setTotalFee(String.valueOf(apiResponseData.getAmount()));
            } else {
                integratedMicropayResponseData.setSysOrderNo(SystemInstance.EMPTY_STRING);
            }
            integratedMicropayResponseData.setStoreOrderNo(paymentRequestData.getOrderid());
            integratedMicropayResponseData.setAuthCode(paymentRequestData.getBarcode());
            integratedMicropayResponseData.setPlatformRsp(new Gson().toJson(resultMap));

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedMicropayResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);
            LOGGER.info("integrate response ready :" + generalResponseData);

        } else if ("Refund".equals(requestHeader.getServiceType())) {
            LOGGER.info("[TSGW][Refund][data]" + data.getAsString());
            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

            GwMerchantApiRefundRequest refundRequestData = new Gson().fromJson(data.getAsString(), GwMerchantApiRefundRequest.class);
            refundRequestData.setGw(gw);
            refundRequestData.setMerchantId(paymentAccount.getAccount());
            refundRequestData.setTimeStamp(DateTime.now().toString(SystemInstance.DATE_PATTERN));

            if (SystemInstance.TYPE_MICROPAY.equals(tradeDetail.getServiceType())) {
                //反掃storeid 為 正掃storeid + "-1"
                refundRequestData.setStoreId(paymentAccount.getAppId() + "-1");
            } else {
                refundRequestData.setStoreId(paymentAccount.getAppId());
            }
            refundRequestData.setRefundReason("退款");
            String preSignUrl = toURLQueryStr(refundRequestData);
            LOGGER.info(preSignUrl);
            //tokenkey = paymentAccount.getHashKey() + paymentAccount.getHashIV()
            String preSign = preSignUrl + paymentAccount.getHashKey() + paymentAccount.getHashIV();
            LOGGER.info(preSign);
            String sign = sha256(preSign);
            LOGGER.info(sign);
            refundRequestData.setSign(sign);

            String refundResult = refundPayment(refundRequestData);
            LOGGER.info("[TSGW][SingleOrderQuery][refundResult].." + refundResult);

            Map<String, String> resultMap = convertXml2Map(refundResult);
            ObjectMapper mapper = new ObjectMapper(); // jackson's objectmapper

            GwMerchantApiRefundResponse apiResponseData = mapper.convertValue(resultMap, GwMerchantApiRefundResponse.class);

            //若退款回傳代碼為以下四種 須查詢原訂單是否退款成功
            List<String> queryReturnCode = Arrays.asList("712", "713", "714", "799");

            if (queryReturnCode.contains(apiResponseData.getReturnCode())) {

                GwMerchantApiQueryRequest queryRefund = genApiQueryRequest(data.getAsString(), gw, paymentAccount, tradeDetail, "R");
                queryRefund.setId(refundRequestData.getRefundId());

                String query = singleOrderQuery(queryRefund);
                LOGGER.info("[TSGW][SingleOrderQuery][refundResult].." + query);

                Map<String, String> refundResultMap = convertXml2Map(query);

                GwMerchantApiQueryResponse refundResp = mapper.convertValue(refundResultMap, GwMerchantApiQueryResponse.class);

                apiResponseData.setReturnCode(refundResp.getReturnCode());

            }

            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, new Gson().toJson(apiResponseData), data);

            ResponseGeneralHeader responseGeneralHeader = getResponseHeader(apiResponseData.getReturnCode(), apiResponseData.getReturnMessage(), requestHeader, messageSource);

            responseGeneralHeader.setMchId(integrateMchId);

            IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();

            integratedRefundResponseData.setStoreOrderNo(tradeDetail.getOrderId());
            integratedRefundResponseData.setStoreRefundNo(apiResponseData.getRefundId());
            integratedRefundResponseData.setRefundedAt(DateTime.now().toString(SystemInstance.DATE_PATTERN));

            integratedRefundResponseData.setPlatformRsp(new Gson().toJson(resultMap));

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedRefundResponseData);
            generalResponseData = new Gson().toJson(responseGeneralBody);

            LOGGER.info("integrate response ready :" + generalResponseData);

        } else if ("SingleOrderQuery".equals(requestHeader.getServiceType())) {

            LOGGER.info("[TSGW][SingleOrderQuery][data]" + data.getAsString());

            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
            ObjectMapper mapper = new ObjectMapper(); // jackson's objectmapper
            String paymentResult = null;

            GwMerchantApiQueryRequest queryPayment = genApiQueryRequest(data.getAsString(), gw, paymentAccount, tradeDetail, "P");
            try {
                paymentResult = singleOrderQuery(queryPayment);
            } catch (Exception e) {
                LOGGER.error("[TSGW][SingleOrderQuery] :" + e.getMessage());
            }
            LOGGER.info("[TSGW][SingleOrderQuery][paymentResult].." + paymentResult);

            Map<String, String> paymentResultMap = convertXml2Map(paymentResult);

            GwMerchantApiQueryResponse apiResponseData = mapper.convertValue(paymentResultMap,
                    GwMerchantApiQueryResponse.class);

            if (tradeDetail.getStoreRefundId() != null) {
                GwMerchantApiQueryRequest queryRefund = genApiQueryRequest(data.getAsString(), gw, paymentAccount, tradeDetail, "R");
                String refundResult = singleOrderQuery(queryRefund);
                LOGGER.info("[TSGW][SingleOrderQuery][refundResult].." + refundResult);
                Map<String, String> refundResultMap = convertXml2Map(refundResult);
                GwMerchantApiQueryResponse refundResp = mapper.convertValue(refundResultMap, GwMerchantApiQueryResponse.class);
                if ("000".equals(refundResp.getReturnCode())) {
                    apiResponseData = refundResp;
                }
            }

            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, new Gson().toJson(apiResponseData), data);

            ResponseGeneralHeader responseGeneralHeader = getResponseHeader(apiResponseData.getReturnCode(), apiResponseData.getReturnMessage(), requestHeader, messageSource);

            responseGeneralHeader.setMchId(integrateMchId);
            IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = this.getIntegratedSingleOrderQueryResponseData(apiResponseData, tradeDetailService.getOne(tradeDetail.getOrderId()));
            // integratedSingleOrderQueryResponseData.setPlatformRsp(result);
            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedSingleOrderQueryResponseData);
            generalResponseData = new Gson().toJson(responseGeneralBody);

            LOGGER.info("integrate response ready :" + generalResponseData);

        }
        return generalResponseData;
    }

    public GwMerchantApiQueryRequest genApiQueryRequest(String data, String gw, PaymentAccount paymentAccount, TradeDetail tradeDetail, String type) throws Exception {
        GwMerchantApiQueryRequest request = new Gson().fromJson(data, GwMerchantApiQueryRequest.class);
        request.setGw(gw);
        request.setMerchantId(paymentAccount.getAccount());
        request.setTimeStamp(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        if ("R".equals(type)) {
            request.setId(tradeDetail.getStoreRefundId());
        } else {
            request.setId(tradeDetail.getOrderId());
        }

        if (SystemInstance.TYPE_MICROPAY.equals(tradeDetail.getServiceType())) {
            request.setStoreId(paymentAccount.getAppId() + "-1");
        } else {
            request.setStoreId(paymentAccount.getAppId());
        }
        request.setType(type);
        String preSignUrl = toURLQueryStr(request);
        LOGGER.info(preSignUrl);
        String preSign = preSignUrl + paymentAccount.getHashKey() + paymentAccount.getHashIV();
        LOGGER.info(preSign);
        String sign = sha256(preSign);
        LOGGER.info(sign);
        request.setSign(sign);
        return request;
    }

    public String singleOrderQuery(GwMerchantApiQueryRequest request) throws Exception {
        LOGGER.info("[TSGW][singleOrderQuery][request] : " + new ObjectMapper().writeValueAsString(request));
        String result = httpsGet(TSGW_URL + "TSCBOgwAPI/gwMerchantApiQuery.ashx?" + toURLQueryStr(request));
        return result;
    }

    public String payment(GwMerchantApiPayRequest request) throws Exception {
        LOGGER.info("[TSGW][Payment][request] : " + new ObjectMapper().writeValueAsString(request));
        String result = httpsGet(TSGW_URL + "TSCBOgwAPI/gwMerchantApiPay.ashx?" + toURLQueryStr(request));
        return result;
    }

    public String refundPayment(GwMerchantApiRefundRequest request) throws Exception {
        LOGGER.info("[TSGW][refundPayment][request] : " + new ObjectMapper().writeValueAsString(request));
        String result = httpsGet(TSGW_URL + "TSCBOgwAPI/gwMerchantApiRefund.ashx?" + toURLQueryStr(request));
        return result;
    }

    public static String httpsGet(String requestUrl) throws Exception {
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                }
        };
        SSLContext context = SSLContext.getInstance("TLS");
        context.init((KeyManager[]) null, trustAllCerts, (SecureRandom) null);
        URL url = new URL(requestUrl);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setSSLSocketFactory(context.getSocketFactory());
        connection.setReadTimeout(10000);
        connection.setConnectTimeout(15000);
        connection.setDoInput(true);
        connection.setRequestMethod("GET");
        connection.setDoOutput(false);

        StringBuilder text = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

        String line;
        while ((line = reader.readLine()) != null) {
            text.append(line);
        }

        reader.close();
        return text.toString();
    }

    private String sha256(String data) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(data.getBytes(StandardCharsets.UTF_8));

        return DatatypeConverter.printHexBinary(hash).toLowerCase();
    }

    private Map<String, String> convertXml2Map(String paymentResult) {
        Document xml;
        try {
            xml = DocumentHelper.parseText(paymentResult);
        } catch (DocumentException e) {
            e.printStackTrace();
            return null;
        }
        Element root = xml.getRootElement();
        Map<String, String> map = new HashMap<String, String>();
        for (Iterator<Element> it = root.elementIterator(); it.hasNext(); ) {
            Element element = it.next();
            map.put(element.getName(), element.getText());
        }
        return map;
    }

    //物件 轉 URLQueryStr
    public static String toURLQueryStr(Object o) {
        Field[] fields = o.getClass().getDeclaredFields();
        List<String> fieldList = new ArrayList<String>();
        StringBuffer sb = new StringBuffer();
        try {
            //滾成員變數排序
            for (Field f : fields) {
                if (f.getName() != null) {
                    fieldList.add(f.getName());
                }
            }

            fieldList.sort(String.CASE_INSENSITIVE_ORDER);

            //串QueryStr
            for (String feildName : fieldList) {

                PropertyDescriptor pd = new PropertyDescriptor(feildName, o.getClass());
                Method rM = pd.getReadMethod();
                Object num = rM.invoke(o);
                if (num != null) {
                    if (sb.length() != 0) {
                        sb.append("&");
                    }
                    if ("".equals(((String) num).trim())) {
                        num = "Undocumented";
                    }
                    sb.append(feildName + "=" + ((String) num).trim().replaceAll(" ", ""));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public ResponseGeneralHeader getResponseHeader(String returnCode, String returnMessage, RequestHeader requestHeader, MessageSource messageSource) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        Locale locale = new Locale("zh_TW");

        if ("000".equals(returnCode) || "1150".equals(returnCode)) {
            responseGeneralHeader.setStatusCode("0000");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        } else if ("1133".equals(returnCode)) {
            responseGeneralHeader.setStatusCode("7123");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7123", null, locale));
        } else if ("1124".equals(returnCode)) {
            responseGeneralHeader.setStatusCode("7126");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7126", null, locale));
        } else if ("1104".equals(returnCode) || returnCode.equals("1105")) {
            responseGeneralHeader.setStatusCode("7132");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7132", null, locale));
        } else if ("1106".equals(returnCode)) {
            responseGeneralHeader.setStatusCode("7158");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7158", null, locale));
        } else if ("1155".equals(returnCode)) {
            responseGeneralHeader.setStatusCode("7138");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7138", null, locale));
        } else if ("1163".equals(returnCode)) {
            responseGeneralHeader.setStatusCode("7155");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7155", null, locale));
        } else if ("1164".equals(returnCode)) {
            responseGeneralHeader.setStatusCode("7112");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7112", null, locale));
        } else if ("1165".equals(returnCode)) {
            responseGeneralHeader.setStatusCode("7112");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7112", null, locale));
        } else if ("1282".equals(returnCode)) {
            responseGeneralHeader.setStatusCode("7342");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7342", null, locale));
        } else if ("1288".equals(returnCode)) {
            responseGeneralHeader.setStatusCode("7345");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7345", null, locale));
        } else {
            responseGeneralHeader.setStatusCode("9998");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.9998", null, locale));
        }
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        return responseGeneralHeader;
    }

    public IntegratedSingleOrderQueryResponseData getIntegratedSingleOrderQueryResponseData(
            GwMerchantApiQueryResponse data, TradeDetail tradeDetail) {
        IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();
        integratedSingleOrderQueryResponseData.setSysOrderNo(StringUtils.defaultIfEmpty(tradeDetail.getSystemOrderId(),""));
        integratedSingleOrderQueryResponseData.setStoreOrderNo(tradeDetail.getOrderId());
        integratedSingleOrderQueryResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
        integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());
        integratedSingleOrderQueryResponseData.setPaidAt(tradeDetail.getCreateDate());
        integratedSingleOrderQueryResponseData.setRefundedMsg(tradeDetail.getRefundStatus());
        if ("000".equals(data.getReturnCode())) {
            integratedSingleOrderQueryResponseData.setStoreOrderNo(data.getOrderId());
            integratedSingleOrderQueryResponseData.setFeeType(SystemInstance.CURRENCY_TWD);
            integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());
            integratedSingleOrderQueryResponseData.setDetail(tradeDetail.getDetail());
            if ("0".equals(data.getStatus())) {
                integratedSingleOrderQueryResponseData.setOrderStatus("0");
            } else {
                integratedSingleOrderQueryResponseData.setOrderStatus("1");
            }
            if (data.getRefundId() != null && !"".equals(data.getRefundId())) {
                integratedSingleOrderQueryResponseData.setOrderStatus("3");
            }
        } else {
            integratedSingleOrderQueryResponseData.setOrderStatus("2");
        }
        return integratedSingleOrderQueryResponseData;
    }

}
