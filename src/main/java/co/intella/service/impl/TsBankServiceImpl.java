package co.intella.service.impl;

import co.intella.domain.PaymentResponse;
import co.intella.domain.PaymentResponseData;
import co.intella.domain.PaymentResponseHeader;
import co.intella.domain.integration.*;
import co.intella.domain.tsbank.*;
import co.intella.model.*;
import co.intella.net.HttpRequestUtil;
import co.intella.service.*;
import co.intella.utility.GeneralUtil;
import co.intella.utility.HttpUtil;
import co.intella.utility.SystemInstance;
import co.intella.utility.TsBankUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @author Alex
 */
@Service
@Transactional
public class TsBankServiceImpl implements TsBankService {

    private final Logger LOGGER = LoggerFactory.getLogger(TsBankServiceImpl.class);

    private static final String ENTERPRISE_NO = "96389";

    private static final long ENTERPRISE_WIGHT = 77;

    private static final String ACCOUNT_WEIGHT = "131313";

    private static final String DATE_WEIGHT = "1313";

    private static final String PRICE_WEIGHT = "5432345";

    private static final long DIV = 10;

    @Autowired
    private TradeDetailService tradeDetailService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private InvoiceDetailService invoiceDetailService;

    @Resource
    private RefundDetailService refundDetailService;

    @Value("${api.url}")
    private String API_URL;

    @Value("${host.ts.request.auth}")
    private String TS_AUTH_API_URL;

    @Value("${host.ts.request.other}")
    private String TS_OTHER_API_URL;

    @Value("${host.ts.request.union.auth}")
    private String TS_UNION_AUTH_API_URL;

    @Value("${host.ts.request.union.other}")
    private String TS_UNION_OTHER_API_URL;

    @Value("${host.ts.request.apple.auth}")
    private String TS_APPLE_AUTH_API_URL;

    @Autowired
    private MessageSource messageSource;

    public String payment(TSBCreditCardRequestData request, String integrateMchId, String type) throws Exception {
        String requestJson = new ObjectMapper().writeValueAsString(request);

        String pan = request.getParams().getPan();
        if (!type.equals("applepay")) {
            request.getParams().setPan(pan.substring(0, 6) + "*************" + pan.substring(pan.length() - 4));
            request.getParams().setCvv2("***");
            request.getParams().setExpDate("****");
        }
        LOGGER.info("[TS][TRANSACTION] start... " + new ObjectMapper().writeValueAsString(request));


        try {
            String tcResponse = SystemInstance.EMPTY_STRING;
            if (type.equals("creditcard")) {
                tcResponse = HttpUtil.doPostJson(TS_AUTH_API_URL, requestJson);
            } else if (type.equals("union")) {
                tcResponse = HttpUtil.doPostJson(TS_UNION_AUTH_API_URL, requestJson);
            } else if (type.equals("applepay")) {
                tcResponse = HttpUtil.doPostJson(TS_APPLE_AUTH_API_URL, requestJson);
                return tcResponse;
            }

            LOGGER.info("[TS][TRANSACTION] complete. " + tcResponse);
            return convertPaymentResponse(tcResponse, request, integrateMchId);

        } catch (JsonSyntaxException e) {
            // todo compose json
            LOGGER.error("[TS][EXCEPTION][JsonSyntaxException] " + e.getMessage());
            return "[EXCEPTION] jsonSyntax is wrong";
        } catch (Exception e) {
            // todo compose json
            LOGGER.error("[TS][EXCEPTION] " + e.getMessage());
            return "[EXCEPTION] post to TS bank wrong";
        }
    }

    public String other(TSBCreditCardBasicRequestData request, String integrateMchId) throws Exception {

        String requestJson = new ObjectMapper().writeValueAsString(request);
        try {
            String tsResponse;
            if (((TSBOtherRequestData) request).getPayType().equals(2)) {
                LOGGER.info("[Union][Other]" + requestJson);
                tsResponse = HttpRequestUtil.post(TS_UNION_OTHER_API_URL, requestJson);

            } else {
                LOGGER.info("[CreditCard][Other]" + requestJson);
//                tsResponse="{\"ver\":\"1.0.0\",\"mid\":\"000812770060201\",\"tid\":\"T0000000\",\"pay_type\":1,\"tx_type\":7,\"ret_value\":0,\"params\":{\"ret_code\":\"00\",\"order_status\":\"02\",\"auth_type\":\"SSL\",\"cur\":\"NTD\",\"purchase_date\":\"2017-08-07 10:11:48\",\"tx_amt\":\"100\",\"auth_id_resp\":\"000000\",\"rrn\":\"721902398812\",\"settle_amt\":\"0\",\"refund_trans_amt\":\"0\",\"redeem_pt\":\"0\",\"redeem_amt\":\"0\",\"post_redeem_amt\":\"0\",\"post_redeem_pt\":\"0\",\"install_down_pay\":\"0\",\"install_pay\":\"0\"}}";
//                tsResponse="{\"ver\":\"1.0.0\",\"mid\":\"000812770060201\",\"tid\":\"T0000000\",\"pay_type\":1,\"tx_type\":3,\"ret_value\":0,\"params\":{\"ret_code\":\"00\",\"order_status\":\"02\",\"auth_type\":\"SSL\",\"cur\":\"NTD\",\"purchase_date\":\"2017-10-16 15:57:22\",\"tx_amt\":\"300\",\"auth_id_resp\":\"010154\",\"rrn\":\"728907843527\",\"settle_amt\":\"100\",\"settle_seq\":\"171016948033\",\"settle_date\":\"2017-10-16\",\"refund_trans_amt\":\"0\",\"redeem_pt\":\"0\",\"redeem_amt\":\"0\",\"post_redeem_amt\":\"0\",\"post_redeem_pt\":\"0\",\"install_down_pay\":\"0\",\"install_pay\":\"0\"}}";
                tsResponse = HttpRequestUtil.post(TS_OTHER_API_URL, requestJson);
            }
            LOGGER.info("[TS][TRANSACTION] complete. " + tsResponse);
            return convertOtherResponse(tsResponse, request, integrateMchId);

        } catch (JsonSyntaxException e) {
            LOGGER.error("[TS][EXCEPTION][JsonSyntaxException] " + e.getMessage());
            return "[EXCEPTION] jsonSyntax is wrong";
        } catch (Exception e) {
            LOGGER.error("[TS][EXCEPTION] " + e.getMessage());
            return "[EXCEPTION] post to ts bank wrong";
        }
    }

    public String unionPayment(TSBCreditCardRequestData request, String integrateMchId) throws Exception {
        LOGGER.info("[TS][TRANSACTION] start... " + new Gson().toJson(request));
        String requestJson = new ObjectMapper().writeValueAsString(request);

        try {

            String tcResponse = HttpRequestUtil.post(TS_UNION_AUTH_API_URL, requestJson);

            LOGGER.info("[TS][TRANSACTION] complete. " + tcResponse);
            //return convertPaymentResponse(tcResponse, request, integrateMchId);

        } catch (JsonSyntaxException e) {
            // todo compose json
            LOGGER.error("[TS][EXCEPTION][JsonSyntaxException] " + e.getMessage());
            return "[EXCEPTION] jsonSyntax is wrong";
        } catch (Exception e) {
            // todo compose json
            LOGGER.error("[TS][EXCEPTION] " + e.getMessage());
            return "[EXCEPTION] post to ts bank wrong";
        }
        return null;
    }

    public String unionOther(TSBCreditCardBasicRequestData request, String integrateMchId) throws Exception {

        String requestJson = new ObjectMapper().writeValueAsString(request);
        try {
            LOGGER.info("[Union][Other].." + requestJson);
            String tcResponse = HttpRequestUtil.post(TS_UNION_OTHER_API_URL, requestJson);
            return convertOtherResponse(tcResponse, request, integrateMchId);

        } catch (JsonSyntaxException e) {
            LOGGER.error("[TS][EXCEPTION][JsonSyntaxException] " + e.getMessage());
            return "[EXCEPTION] jsonSyntax is wrong";
        } catch (Exception e) {
            LOGGER.error("[TS][EXCEPTION] " + e.getMessage());
            return "[EXCEPTION] post to ts bank wrong";
        }


    }

    public String getVirtualAccount(String virtualAccountNo, String date, long price) {

        long priceWeight = 0;
        String priceString = String.valueOf(price);
        for (int i = 6; i > 6 - priceString.length(); i--) {
            int offset = 6 - i;
            char ps = priceString.charAt((priceString.length() - 1) - offset);
            priceWeight = priceWeight + convertCharToLong(ps) * convertCharToLong(PRICE_WEIGHT.charAt(i));
        }

        long accountWeight = 0;
        for (int i = 0; i < 6; i++) {
            accountWeight = accountWeight + convertCharToLong(virtualAccountNo.charAt(i)) * convertCharToLong(ACCOUNT_WEIGHT.charAt(i));
        }

        long dateWeight = 0;
        for (int i = 0; i < 4; i++) {
            dateWeight = dateWeight + convertCharToLong(date.charAt(i)) * convertCharToLong(DATE_WEIGHT.charAt(i));
        }

        long totalWeight = ENTERPRISE_WIGHT + accountWeight + dateWeight + priceWeight;
        long mod = totalWeight % DIV;

        String authCode = String.valueOf(10 - mod);
        return ENTERPRISE_NO + virtualAccountNo + date + (authCode.equals("10") ? "0" : authCode);
    }

    private long convertCharToLong(char ch) {
        return Long.valueOf(String.valueOf(ch));
    }

    public String doRequest(RequestHeader requestHeader, JsonPrimitive data, String integrateMchId, String clientIP) throws Exception {
        LOGGER.info("[TsBank][Payment][doRequest]");
        PaymentAccount paymentAccount = paymentAccountService.getOne(integrateMchId, 8);

        if ("Payment".equals(requestHeader.getServiceType())) {
//            LOGGER.info("[TsBank][Payment] " + data.getAsString());
            TSBCreditCardRequestDataPara requestData = new Gson().fromJson(data.getAsString(), TSBCreditCardRequestDataPara.class);
            String pan = requestData.getPan();
            String cvv2 = requestData.getCvv2();
            String expDate = requestData.getExpDate();
            requestData.setPan(pan.substring(0, 6) + "*************" + pan.substring(pan.length() - 4));
            requestData.setCvv2("***");
            requestData.setExpDate("****");
            LOGGER.info("[TsBank][Payment] " + new Gson().toJson(requestData));
            requestData.setPan(pan);
            requestData.setCvv2(cvv2);
            requestData.setExpDate(expDate);


            TSBCreditCardRequestData tsbCreditCardRequestData = new TSBCreditCardRequestData();

            requestData.setLayout("1");
            requestData.setAmt(requestData.getAmt() + "00");
            requestData.setCur("NTD");
            requestData.setCaptFlag("0");
            requestData.setResultFlag("1");
            requestData.setPostBackUrl(API_URL + "allpaypass/api/tsb/return");
            requestData.setResultUrl(API_URL + "allpaypass/api/tsb/notify");

            tsbCreditCardRequestData.setMid(paymentAccount.getAccount());
            tsbCreditCardRequestData.setTid("T0000000");
            tsbCreditCardRequestData.setVer("1.0.0");
            tsbCreditCardRequestData.setSender("rest");
            tsbCreditCardRequestData.setTxType(1);
            tsbCreditCardRequestData.setParams(requestData);

            tradeProcedureService.setTradeDetailRequest(requestHeader, data);
            String tcBankResponse;
            if (requestData.getPan().startsWith("62")) {
                tsbCreditCardRequestData.setPayType(2);
                requestData.setCardholderIp(clientIP);
                requestData.setOrderTimeout("10");
                tcBankResponse = payment(tsbCreditCardRequestData, integrateMchId, "union");
            } else {
                tsbCreditCardRequestData.setPayType(1);
                tcBankResponse = payment(tsbCreditCardRequestData, integrateMchId, "creditcard");
            }
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, tcBankResponse, data);

            return tcBankResponse;

        } else if ("ApplePay".equals(requestHeader.getServiceType())) {

            paymentAccount = paymentAccountService.getOne(requestHeader.getMerchantId(), 12);

            TSBCreditCardRequestData tsbCreditCardRequestData = new TSBCreditCardRequestData();
            TSBCreditCardRequestDataPara requestData = new Gson().fromJson(data.getAsString(), TSBCreditCardRequestDataPara.class);

            tsbCreditCardRequestData.setMid(paymentAccount.getAccount());
            tsbCreditCardRequestData.setTid("T0000000");
            tsbCreditCardRequestData.setVer("1.0.0");
            tsbCreditCardRequestData.setSender("rest");
            tsbCreditCardRequestData.setPayType(3);
            tsbCreditCardRequestData.setTxType(1);
            tsbCreditCardRequestData.setParams(requestData);

            //tradeProcedureService.setTradeDetailRequest(requestHeader,data);
            String tsBankResponse = payment(tsbCreditCardRequestData, integrateMchId, "applepay");
            //tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader,tsBankResponse, data);

            LOGGER.info("[TS][ApplePay][Response].." + tsBankResponse);
            return tsBankResponse;

        } else {
            TSBOtherRequestDataPara requestData = new Gson().fromJson(data.getAsString(), TSBOtherRequestDataPara.class);
            TSBOtherRequestData tsbCreditCardRequestData = new TSBOtherRequestData();


            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
            if (requestData.getAmt() != null) {
                requestData.setAmt(requestData.getAmt() + "00");
            }
            requestData.setResultFlag("1");
            tsbCreditCardRequestData.setMid(paymentAccount.getAccount());
            tsbCreditCardRequestData.setTid("T0000000");
            tsbCreditCardRequestData.setVer("1.0.0");
            tsbCreditCardRequestData.setSender("rest");
            //union
            if (tradeDetail.getType() == 1) {
                tsbCreditCardRequestData.setPayType(2);
            } else {
                tsbCreditCardRequestData.setPayType(1);
            }

            tsbCreditCardRequestData.setParams(requestData);

            if ("SingleOrderQuery".equals(requestHeader.getServiceType())) {
                tsbCreditCardRequestData.setTxType(7);
            } else if ("Charge".equals(requestHeader.getServiceType())) {
                tsbCreditCardRequestData.setTxType(3);
            } else if ("Cancel".equals(requestHeader.getServiceType())) {
                tsbCreditCardRequestData.setTxType(8);
            } else if ("Return".equals(requestHeader.getServiceType())) {
                tsbCreditCardRequestData.setTxType(5);
            } else if ("CancelCharge".equals(requestHeader.getServiceType())) {
                tsbCreditCardRequestData.setTxType(4);
            } else if ("CancelReturn".equals(requestHeader.getServiceType())) {
                tsbCreditCardRequestData.setTxType(6);
            } else if ("PartialRefund".equals(requestHeader.getServiceType())) {

                LOGGER.info("[TS][PartialRefund]");
                tsbCreditCardRequestData.setTxType(7);

                String requestJson = new ObjectMapper().writeValueAsString(tsbCreditCardRequestData);
                LOGGER.info("[TS][PartialRefund][QueryRequest]" + requestJson);
                String tsResponse = HttpRequestUtil.post(TS_OTHER_API_URL, requestJson);
                LOGGER.info("[TS][PartialRefund][QueryResponse]" + tsResponse);
                //tsResponse="{\"ver\":\"1.0.0\",\"mid\":\"000812770060201\",\"tid\":\"T0000000\",\"pay_type\":1,\"tx_type\":7,\"ret_value\":0,\"params\":{\"ret_code\":\"00\",\"order_status\":\"02\",\"auth_type\":\"SSL\",\"cur\":\"NTD\",\"purchase_date\":\"2017-08-07 10:11:48\",\"tx_amt\":\"100\",\"auth_id_resp\":\"000000\",\"rrn\":\"721902398812\",\"settle_amt\":\"0\",\"refund_trans_amt\":\"0\",\"redeem_pt\":\"0\",\"redeem_amt\":\"0\",\"post_redeem_amt\":\"0\",\"post_redeem_pt\":\"0\",\"install_down_pay\":\"0\",\"install_pay\":\"0\"}}";

//                tsResponse="{\"ver\":\"1.0.0\",\"mid\":\"000812770060201\",\"tid\":\"T0000000\",\"pay_type\":1,\"tx_type\":7,\"ret_value\":0,\"params\":{\"ret_code\":\"00\",\"order_status\":\"03\",\"auth_type\":\"SSL\",\"cur\":\"NTD\",\"purchase_date\":\"2017-10-16 15:57:22\",\"tx_amt\":\"300\",\"auth_id_resp\":\"010154\",\"rrn\":\"728907843527\",\"settle_amt\":\"100\",\"settle_seq\":\"171016948033\",\"settle_date\":\"2017-10-16\",\"refund_trans_amt\":\"0\",\"redeem_pt\":\"0\",\"redeem_amt\":\"0\",\"post_redeem_amt\":\"0\",\"post_redeem_pt\":\"0\",\"install_down_pay\":\"0\",\"install_pay\":\"0\"}}";

                TSBOtherResponseData tsbOtherResponseData = new Gson().fromJson(tsResponse, TSBOtherResponseData.class);
                if (!tsbOtherResponseData.getParams().getRetCode().equals("00")) {
                    return convertPartialRefundResponse("F", tsbOtherResponseData, tradeDetail, integrateMchId, tsbCreditCardRequestData);
                } else {
                    if (tsbOtherResponseData.getParams().getOrderStatus().equals("03")) {

                        tsbOtherResponseData = cancelCharge(tsbCreditCardRequestData, tradeDetail, tsbOtherResponseData);

                    }
                    if (!tsbOtherResponseData.getParams().getOrderStatus().equals("02")) {
                        return convertPartialRefundResponse("F", tsbOtherResponseData, tradeDetail, integrateMchId, tsbCreditCardRequestData);
                    }

                    JsonObject jsonObject = new Gson().fromJson(data.getAsString(), JsonObject.class);
                    Integer refundFee = jsonObject.get("RefundFee").getAsInt();
                    String chargeAmount = getchargeAmount(tsbOtherResponseData, refundFee);
                    tsbCreditCardRequestData.getParams().setAmt(chargeAmount + "00");

                    TSBOtherResponseData chargerRes = charge(tsbCreditCardRequestData, tradeDetail, refundFee, chargeAmount);
                    if (chargerRes != null && chargerRes.getParams().getRetCode().equals("00")) {
                        tradeDetail.setRefundStatus("P Refund success");
                        tradeDetailService.save(tradeDetail);
                        return convertPartialRefundResponse("S", chargerRes, tradeDetail, integrateMchId, tsbCreditCardRequestData);

                    } else {
                        return convertPartialRefundResponse("F", tsbOtherResponseData, tradeDetail, integrateMchId, tsbCreditCardRequestData);
                    }
                }
            }

            String tsBankResponse = other(tsbCreditCardRequestData, integrateMchId);
            LOGGER.info("[tsBankResponse]" + tsBankResponse);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, tsBankResponse, data);
            LOGGER.info("[tsBankResponse][setEND]" + tsBankResponse);

            return tsBankResponse;
        }

    }

    private TSBOtherResponseData cancelCharge(TSBOtherRequestData tsbCreditCardRequestData, TradeDetail tradeDetail, TSBOtherResponseData tsbOtherResponseData) throws Exception {
        tsbCreditCardRequestData.setTxType(4);
        tsbCreditCardRequestData.getParams().setAmt(tsbOtherResponseData.getParams().getSettleAmt());
        String requestJson = new ObjectMapper().writeValueAsString(tsbCreditCardRequestData);
        LOGGER.info("[TS][PartialRefund][cancelChargeRequest]" + requestJson);
        String cancelChargeResponse = HttpRequestUtil.post(TS_OTHER_API_URL, requestJson);
        LOGGER.info("[TS][PartialRefund][cancelChargeResponse]" + cancelChargeResponse);

//        cancelChargeResponse ="{\"ver\":\"1.0.0\",\"mid\":\"000812770060201\",\"tid\":\"T0000000\",\"pay_type\":1,\"tx_type\":4,\"ret_value\":0,\"params\":{\"ret_code\":\"00\",\"order_status\":\"02\",\"auth_type\":\"SSL\",\"cur\":\"NTD\",\"purchase_date\":\"2017-10-16 15:57:22\",\"tx_amt\":\"300\",\"auth_id_resp\":\"010154\",\"rrn\":\"728907843527\",\"settle_amt\":\"0\",\"refund_trans_amt\":\"0\",\"redeem_pt\":\"0\",\"redeem_amt\":\"0\",\"post_redeem_amt\":\"0\",\"post_redeem_pt\":\"0\",\"install_down_pay\":\"0\",\"install_pay\":\"0\"}}";
        return new Gson().fromJson(cancelChargeResponse, TSBOtherResponseData.class);
    }

    private String convertPartialRefundResponse(String status, TSBOtherResponseData tsbOtherResponseData, TradeDetail tradeDetail, String integrateMchId, TSBOtherRequestData request) {
        ResponseGeneralHeader responseHeader = new TsBankUtil().getPartiaRefundHeader(status, messageSource);
        responseHeader.setMchId(integrateMchId);
        responseHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        responseHeader.setMethod("20800");

        IntegratedPartialRefundResponseData responseData = new IntegratedPartialRefundResponseData();

        if (status.equals("S")) {
            Integer settle = Integer.valueOf(tsbOtherResponseData.getParams().getSettleAmt()) / 100;
            Integer totalFee = Integer.valueOf(tsbOtherResponseData.getParams().getTxAmt()) / 100;
            responseData.setOrderFee(String.valueOf(settle));
            responseData.setRefundFee(String.valueOf(totalFee - settle));
            responseData.setPlatformRsp(new Gson().toJson(tsbOtherResponseData));
            responseData.setStoreOrderNo(request.getParams().getOrderNo());
            responseData.setStoreRefundNo(tradeDetail.getStoreRefundId());
            responseData.setRefundedAt(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        }

        ResponseGeneralBody responseBody = new ResponseGeneralBody();
        responseBody.setHeader(responseHeader);
        responseBody.setData(responseData);
        return new Gson().toJson(responseBody);

    }

    private String convertPaymentResponse(String tsResponse, TSBCreditCardRequestData request, String integrateMchId) {

        TSBCreditCardResponseData tsbCreditCardResponseData = new Gson().fromJson(tsResponse, TSBCreditCardResponseData.class);

        ResponseGeneralHeader responseHeader = new TsBankUtil().getTsbankResponseHeader(tsbCreditCardResponseData, messageSource);


        responseHeader.setMchId(integrateMchId);
        responseHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        responseHeader.setServiceType("Payment");
        responseHeader.setMethod("20800");

        IntegratedPaymentResponseData responseData = new IntegratedPaymentResponseData();
        responseData.setSysOrderNo("");
        responseData.setStoreOrderNo(request.getParams().getOrderNo());
        responseData.setTotalFee(request.getParams().getAmt());
        responseData.setFeeType("TWD");
        responseData.setDeviceInfo("DeviceMiles");
        responseData.setBody(request.getParams().getOrderDesc());
        responseData.setPlatformRsp(tsbCreditCardResponseData.getParams().getHppUrl());
        responseData.setSerialNumber(0);

        ResponseGeneralBody responseBody = new ResponseGeneralBody();
        responseBody.setHeader(responseHeader);
        responseBody.setData(responseData);

        return new Gson().toJson(responseBody);
    }

    private String convertOtherResponse(String tsResponse, TSBCreditCardBasicRequestData request, String integrateMchId) {
        TSBOtherResponseData tsbOtherResponseData = new Gson().fromJson(tsResponse, TSBOtherResponseData.class);

        TradeDetail tradeDetail = tradeDetailService.getOne(integrateMchId, ((TSBOtherRequestData) request).getParams().getOrderNo());

        //todo can not get store refund no
        //ResponseGeneralHeader responseHeader = new ResponseGeneralHeader();
        ResponseGeneralHeader responseHeader = new TsBankUtil().getTsbankOtherResponseHeader(tsbOtherResponseData, messageSource);
        responseHeader.setMchId(integrateMchId);
        responseHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        responseHeader.setMethod("20800");

        if (tsbOtherResponseData.getTxType().equals(8)) {

            responseHeader.setServiceType("Cancel");
//            if (tsbOtherResponseData.getParams().getRetCode().equals("00")){
//                responseHeader.setStatusCode("0000");
//                responseHeader.setStatusDesc("success");
//            }

            IntegratedRefundResponseData responseData = new IntegratedRefundResponseData();
            responseData.setSysRefundNo(tsbOtherResponseData.getParams().getRrn());
            responseData.setPlatformRsp(new Gson().toJson(tsbOtherResponseData));
            responseData.setStoreOrderNo(((TSBOtherRequestData) request).getParams().getOrderNo());
            responseData.setSysOrderNo(tradeDetail.getSystemOrderId());
            responseData.setStoreRefundNo(tradeDetail.getStoreRefundId());
            responseData.setRefundedAt(DateTime.now().toString(SystemInstance.DATE_PATTERN));

            ResponseGeneralBody responseBody = new ResponseGeneralBody();
            responseBody.setHeader(responseHeader);
            responseBody.setData(responseData);
            return new Gson().toJson(responseBody);

        } else if (tsbOtherResponseData.getTxType().equals(7)) {
            responseHeader.setServiceType("SingleOrderQuery");
//            if (tsbOtherResponseData.getParams().getRetCode().equals("00")){
//                responseHeader.setStatusCode("0000");
//                responseHeader.setStatusDesc("success");
//            }

            IntegratedSingleOrderQueryResponseData responseData = new IntegratedSingleOrderQueryResponseData();
//            responseData.setPlatformRsp(new Gson().toJson(tsbOtherResponseData));
            responseData.setRefundedMsg(tradeDetail.getRefundStatus());
            responseData.setStoreOrderNo(tradeDetail.getOrderId());
            responseData.setSysOrderNo(StringUtils.defaultIfEmpty(tradeDetail.getSystemOrderId(), ""));
            responseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
            responseData.setBody(tradeDetail.getDescription());
            responseData.setPaidAt(tradeDetail.getCreateDate());
            responseData.setRefundedMsg(tradeDetail.getRefundStatus());
            //todo add order status
            if (tsbOtherResponseData.getParams().getRetCode().equals("00")) {
                if (tsbOtherResponseData.getParams().getOrderStatus().equals("02") || tsbOtherResponseData.getParams().getOrderStatus().equals("03") || tsbOtherResponseData.getParams().getOrderStatus().equals("04")) {
                    responseData.setOrderStatus("1");
                    responseData.setStoreOrderNo(((TSBOtherRequestData) request).getParams().getOrderNo());
                } else if (tsbOtherResponseData.getParams().getOrderStatus().equals("12")) {
                    responseData.setOrderStatus("3");
                    RefundDetail refundDetail = refundDetailService.getOneByOrderIdAndMethod(tradeDetail.getOrderId(), tradeDetail.getMethod());
                    if (Objects.nonNull(refundDetail)) {
                        responseData.setRefundedAt(refundDetail.getCreateTime());
                    }
                } else if (tsbOtherResponseData.getParams().getOrderStatus().equals("ZP")) {
                    responseData.setOrderStatus("0");
                } else {
                    responseData.setOrderStatus("2");
                }
            } else if (tsbOtherResponseData.getParams().getRetCode().equals("-323")) {
                responseData.setOrderStatus("0");
            } else {
                responseData.setOrderStatus("2");

            }
            if (!tradeDetail.getMethod().equals("20840")) {
                String[] a = tradeDetail.getDescription().split("-");
                responseData.setLastFour(a[a.length - 1]);
            }
            ResponseGeneralBody responseBody = new ResponseGeneralBody();
            responseBody.setHeader(responseHeader);
            responseBody.setData(responseData);
            return new Gson().toJson(responseBody);
        } else if (tsbOtherResponseData.getTxType().equals(5)) {
            responseHeader.setServiceType("Return");
//            if (tsbOtherResponseData.getParams().getRetCode().equals("00")){
//                responseHeader.setStatusCode("0000");
//                responseHeader.setStatusDesc("success");
//            }

            IntegratedSingleOrderQueryResponseData responseData = new IntegratedSingleOrderQueryResponseData();
//            responseData.setPlatformRsp(new Gson().toJson(tsbOtherResponseData));
            responseData.setStoreOrderNo(((TSBOtherRequestData) request).getParams().getOrderNo());

            ResponseGeneralBody responseBody = new ResponseGeneralBody();
            responseBody.setHeader(responseHeader);
            responseBody.setData(responseData);
            return new Gson().toJson(responseBody);
        } else if (tsbOtherResponseData.getTxType().equals(3)) {
            responseHeader.setServiceType("Charge");
//            if (tsbOtherResponseData.getParams().getRetCode().equals("00")){
//                responseHeader.setStatusCode("0000");
//                responseHeader.setStatusDesc("success");
//            }

            IntegratedSingleOrderQueryResponseData responseData = new IntegratedSingleOrderQueryResponseData();
//            responseData.setPlatformRsp(new Gson().toJson(tsbOtherResponseData));
            responseData.setStoreOrderNo(((TSBOtherRequestData) request).getParams().getOrderNo());

            ResponseGeneralBody responseBody = new ResponseGeneralBody();
            responseBody.setHeader(responseHeader);
            responseBody.setData(responseData);
            return new Gson().toJson(responseBody);
        } else {
            return null;
        }


    }

    private String getchargeAmount(TSBOtherResponseData tsbOtherResponseData, Integer refundAmount) {
        String chargeAmount;

//        if (!tradeDetail.getRefundStatus().equals("Cancel success")) {
        chargeAmount = String.valueOf(Integer.valueOf(tsbOtherResponseData.getParams().getTxAmt()) / 100 - refundAmount);
        return chargeAmount;
//        }
//        else return null;
    }

    private TSBOtherResponseData charge(TSBOtherRequestData tsbCreditCardRequestData, TradeDetail tradeDetail, Integer refundFee, String chargeAmount) throws Exception {
        tsbCreditCardRequestData.setTxType(3);
        String requestJson = new ObjectMapper().writeValueAsString(tsbCreditCardRequestData);
        LOGGER.info("[TS][PartialRefund][chargeRequest]" + requestJson);

        String chargeResponse = HttpRequestUtil.post(TS_OTHER_API_URL, requestJson);
        LOGGER.info("[TS][PartialRefund][chargeResponse]" + chargeResponse);

//        chargeResponse ="{\"ver\":\"1.0.0\",\"mid\":\"000812770060201\",\"tid\":\"T0000000\",\"pay_type\":1,\"tx_type\":3,\"ret_value\":0,\"params\":{\"ret_code\":\"00\",\"order_status\":\"03\",\"auth_type\":\"SSL\",\"cur\":\"NTD\",\"purchase_date\":\"2017-10-16 15:57:22\",\"tx_amt\":\"300\",\"auth_id_resp\":\"010154\",\"rrn\":\"728907843527\",\"settle_amt\":\"100\",\"settle_seq\":\"171016948033\",\"settle_date\":\"2017-10-16\",\"refund_trans_amt\":\"0\",\"redeem_pt\":\"0\",\"redeem_amt\":\"0\",\"post_redeem_amt\":\"0\",\"post_redeem_pt\":\"0\",\"install_down_pay\":\"0\",\"install_pay\":\"0\"}}";


        TSBOtherResponseData chargeResponseData = new Gson().fromJson(chargeResponse, TSBOtherResponseData.class);

        if (chargeResponseData.getParams().getRetCode().equals("00")) {
            String createDate = DateTime.now().toString(SystemInstance.DATE_PATTERN);
            RefundDetail refundDetail = new RefundDetail();
            refundDetail.setMethod("20800");
            refundDetail.setOrderId(tradeDetail.getOrderId());
            refundDetail.setAmount(refundFee.longValue());
            refundDetail.setTradeDetailRandomId(tradeDetail);
            refundDetail.setCreateTime(createDate);
            refundDetail.setStoreRefundId("P" + tradeDetail.getOrderId());
            refundDetail.setStatus("Refund success");
            refundDetail.setPartialRefund(true);
            refundDetailService.save(refundDetail);

            InvoiceDetail invoiceDetail = new InvoiceDetail();

            invoiceDetail.setOrderId(tradeDetail.getOrderId());
            invoiceDetail.setAmount(Double.valueOf(chargeAmount));
            invoiceDetail.setCreateDate(createDate);
            invoiceDetail.setRefundDetailRandomId(refundDetail);
            invoiceDetail.setTradeDetailRandomId(tradeDetail);

            invoiceDetailService.save(invoiceDetail);
            LOGGER.info("[TS][PartialRefund][charge][DetailSaved]");

            return chargeResponseData;
        }
        return null;
    }

    @Override
    public PaymentResponse mapToPaymentResponse(TSBOtherResponseData tsbOtherResponseData) {

        PaymentResponse paymentResponse = new PaymentResponse();
        PaymentResponseHeader header = paymentResponse.getHeader();
        PaymentResponseData data = paymentResponse.getData();

        TSBOtherResponseDataPara params = tsbOtherResponseData.getParams();
        if ("00".equals(params.getRetCode())) {
            header.setStatusCode(SystemInstance.TRADE_SUCCESS);
        } else {
            header.setStatusCode(SystemInstance.TRADE_FAIL);
        }

        data.setTotalFee(NumberUtils.toDouble(params.getTxAmt()));
        data.setStoreOrderNo(params.getOrderNo());
        data.setCallbackUrl(GeneralUtil.getOrderResultUrl(API_URL, tsbOtherResponseData.getParams().getOrderNo()));

        return paymentResponse;

    }


}