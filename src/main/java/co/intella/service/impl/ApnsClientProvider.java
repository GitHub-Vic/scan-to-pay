package co.intella.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.turo.pushy.apns.ApnsClient;
import com.turo.pushy.apns.ApnsClientBuilder;

import co.intella.model.IntTbLookupCodeNoFK;
import co.intella.service.LookupCodeService;

@Service
public class ApnsClientProvider {
	
	private final Logger LOGGER = LoggerFactory.getLogger(ApnsClientProvider.class);

	private static Map<String, ApnsClientMeta> apnsClientMap;	
	
	@Resource
	private LookupCodeService lookupCodeService;
	
	private ApnsClient apnsClientFactory(File p12File, String p12Password, String hostname) {
		ApnsClient apnsClient;
		try {
			apnsClient = new ApnsClientBuilder()
					.setClientCredentials(p12File, p12Password)
					.setApnsServer(hostname).build();
		} catch (final Exception exc) {
    		throw new Error(exc);
    	}
		return apnsClient;
	}
		
	private File getApnsKey(String apnsKeyFilePath) {
		ClassLoader classLoader = getClass().getClassLoader();
		File p12File = new File(classLoader.getResource(apnsKeyFilePath).getFile());
		return p12File;
	}
	
	@PostConstruct
	public void init() {
		LOGGER.info("[ApnsClientProvider] init");
		apnsClientMap = new HashMap<String, ApnsClientMeta>();
		try {
			List<ApnsClientMeta> apnsClientList = new ArrayList<ApnsClientMeta>();
			List<IntTbLookupCodeNoFK> list = lookupCodeService.findAllByLookTypeQuery("apnsGroup");
			for (IntTbLookupCodeNoFK intTbLookupCode : list) {
				ApnsClientMeta apnsClientMeta = new ApnsClientMeta();
				apnsClientMeta.setAppId(intTbLookupCode.getLookupCode());
				apnsClientMeta.setApnsKeyFilePath(intTbLookupCode.getType1()); // p12 file path
				apnsClientMeta.setP12Password(intTbLookupCode.getType2());
				apnsClientMeta.setHostname(intTbLookupCode.getType3());
				apnsClientMeta.setTopic(intTbLookupCode.getDscr()); //need pair with p12 file
				apnsClientList.add(apnsClientMeta);
			}
			for (ApnsClientMeta apnsClientMeta : apnsClientList) {
				File p12File = getApnsKey(apnsClientMeta.getApnsKeyFilePath());
				ApnsClient apnsClient = apnsClientFactory(p12File, apnsClientMeta.getP12Password(), apnsClientMeta.getHostname());
				apnsClientMeta.setApnsClient(apnsClient);
				apnsClientMap.put(apnsClientMeta.getAppId(), apnsClientMeta);
			}
			LOGGER.info("[ApnsClientProvider] init success");
		} catch (Exception e) {
			throw e;
		}		
    }
		
	public ApnsClientMeta getApnsClientMeta(String appId) {
//		LOGGER.info("[ApnsClientProvider][getApnsClientMeta] "+appId);
		return apnsClientMap.get(appId);
    }
	
	class ApnsClientMeta {
		private String appId;
		private String apnsKeyFilePath;
		private String p12Password;
		private String hostname;
		private String topic;
		private ApnsClient apnsClient;
		public String getAppId() {
			return appId;
		}
		public void setAppId(String appId) {
			this.appId = appId;
		}
		public String getApnsKeyFilePath() {
			return apnsKeyFilePath;
		}
		public void setApnsKeyFilePath(String apnsKeyFilePath) {
			this.apnsKeyFilePath = apnsKeyFilePath;
		}
		public String getP12Password() {
			return p12Password;
		}
		public void setP12Password(String p12Password) {
			this.p12Password = p12Password;
		}
		public String getHostname() {
			return hostname;
		}
		public void setHostname(String hostname) {
			this.hostname = hostname;
		}
		public String getTopic() {
			return topic;
		}
		public void setTopic(String topic) {
			this.topic = topic;
		}
		public ApnsClient getApnsClient() {
			return apnsClient;
		}
		public void setApnsClient(ApnsClient apnsClient) {
			this.apnsClient = apnsClient;
		}
	}
	
}
