package co.intella.service.impl;

import co.intella.constant.Bank;
import co.intella.constant.MethodToBankCode;
import co.intella.domain.integration.*;
import co.intella.domain.twpay.*;
import co.intella.exception.AuthCodeErrorException;
import co.intella.exception.OrderAlreadyExistedException;
import co.intella.exception.OtherAPIException;
import co.intella.model.*;
import co.intella.net.HttpRequestUtil;
import co.intella.service.*;
import co.intella.utility.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.common.net.UrlEscapers;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import javax.crypto.SecretKey;
import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

/**
 * @author Alex
 */
@Service
public class TwPayServiceImpl implements TwPayService {

    private final Logger LOGGER = LoggerFactory.getLogger(TwPayServiceImpl.class);

    @Resource
    private PaymentAccountService paymentAccountService;

    @Autowired
    private Environment env;

    @Autowired
    private MessageSource messageSource;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private OrderService orderService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private TradeDetailCardLogService tradeDetailCardLogService;

    @Resource
    private LookupCodeService lookupCodeService;

    @Value("${api.url}")
    private String API_URL;

    @Value("${host.request.focas}")
    private String FOCAS_URL;

    @Value("${payment.gateway.api.origin}")
    private String PAYMENT_GATEWAY_API_ORIGIN;

//    private String PAYMENT_GATEWAY_API_ORIGIN = "http://34.80.101.124:30395";

    @Value("${fisc.parameter}")
    private String FISC_PARAMETER = "A69896AF6057EFAD836FB7EB3109B52AB896E6B9FEA7173E";

    @Value("${fisc.host.id}")
    private String FISC_HOST_ID = "01102543903790200000001";

    @Autowired
    RestTemplate restTemplate;

    private Locale locale = new Locale("zh_TW");

    @Value("${iPass.TestMode}")
    private String testMode;
    // Prod = 0
    // TestMode = 1

    @Resource
    private RefundDetailService refundDetailService;

    public String getPaymentUrl(String amount, String shortId, String agent) throws OtherAPIException, UnsupportedEncodingException, OrderAlreadyExistedException {

        PaymentAccount paymentAccount = this.getONLPayPaymentAccount(shortId);
        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(shortId);
        String orderNoForTwPay = orderService.createOrderIdBySequence(qrcodeParameter.getMchId());
        Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());

        String body = qrcodeService.getRandomBody(qrcodeParameter);
        qrcodeParameter.setBody(body);

        TwPayRequestData twPayRequestData = new TwPayRequestData();
        twPayRequestData.setSrrn(orderNoForTwPay);
        twPayRequestData.setOrderNbr(orderNoForTwPay);

        QRPayment qrPayment = new QRPayment();
        qrPayment.setTxnAmt(amount + "00"); // twpay 交易單位放大百倍
        twPayRequestData.setQrPayment(qrPayment);

        TradeDetail tradeDetail = this.getONLPayTradeDetail(amount, orderNoForTwPay, merchant, paymentAccount, qrcodeParameter);

        DateTime dateTime = DateTime.now();
        twPayRequestData.setTxnDateLocal(dateTime.toString("yyyyMMdd"));
        twPayRequestData.setTxnStatus("0");
        twPayRequestData.setTxnTimeLocal(dateTime.toString("HHmmss"));
        twPayRequestData.setTxnType("01");
        twPayRequestData.setOrderNbr(twPayRequestData.getOrderNbr());
        twPayRequestData.setRedirectUrl(String.format("%sallpaypass/api/payment/order/result?orderId=%s", API_URL,
                EncryptUtil.encryptSimple(tradeDetail.getOrderId())));

        String reqQrcodeStr;
        IntTbLookupCode lookupCode = lookupCodeService.findOne("merchantPan", String.valueOf(paymentAccount.getBank().getBankId()));
        LOGGER.info("merchantPan => " + JsonUtil.toJsonStr(lookupCode));
        if (StringUtils.isBlank(paymentAccount.getSystemID()) || Objects.isNull(lookupCode)) {
            reqQrcodeStr = TWPayUtil.generateQrPaymentInfoString(paymentAccount, amount + "00", orderNoForTwPay);
            tradeDetail.setCreditCardType("7");
            tradeDetailService.save(tradeDetail);
        } else {
            reqQrcodeStr = TWPayUtil.generateEMVCode(paymentAccount, amount + "00", orderNoForTwPay, lookupCode);
        }
//        twPayRequestData.setQrPaymentInfo(URLEncoder.encode(reqQrcodeStr, "UTF-8"));
        twPayRequestData.setQrPaymentInfo(UrlEscapers.urlFragmentEscaper().escape(reqQrcodeStr));

        qrPayment.setName(paymentAccount.getAppId());
        qrPayment.setSecureCode(paymentAccount.getHashKey());
        qrPayment.setAcquirerCode(paymentAccount.getBankCode());
        qrPayment.setMerchantId(paymentAccount.getAccount());
        qrPayment.setTerminalId(paymentAccount.getAccountTerminalId());

        if (agent.contains("iPhone") || agent.contains("iPad")) {
            twPayRequestData.setDeviceType("ios");
        } else {
            twPayRequestData.setDeviceType("android");
        }

        ObjectMapper mapper = new ObjectMapper();
        String request = null;
        try {
            request = mapper.writeValueAsString(twPayRequestData);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        LOGGER.info("[TwPay][OnLine][Request]: " + request);

        try {
            return HttpRequestUtil.post(PAYMENT_GATEWAY_API_ORIGIN + "/twpay/api/v1/payment", request);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {

        String generalResponseData = SystemInstance.EMPTY_STRING;
        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        PaymentAccount paymentAccount = paymentAccountService.getOne(integrateMchId, bankId);

        if (SystemInstance.TYPE_MICROPAY.equals(requestHeader.getServiceType())) {

            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

            Gson gson = new Gson();
            TwPayRequestData twPayRequestData = gson.fromJson(data.getAsString(), TwPayRequestData.class);
            twPayRequestData.setMerchantId(paymentAccount.getAccount());
            twPayRequestData.setOrderNumber(tradeDetail.getSystemOrderId());
            String authCode = twPayRequestData.getAuthCode();

            boolean isOk = this.prepareTwPayRequestData(twPayRequestData, paymentAccount, tradeDetail);
            if (!isOk) {
                tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                tradeDetailService.save(tradeDetail);
                throw new AuthCodeErrorException("Auth code error");
            }

            String paymentResult = micropay(twPayRequestData, paymentAccount);
            LOGGER.info("[TWPay][Micropay][Response] xml :" + paymentResult);
            if (paymentResult == null || paymentResult.isEmpty()) {
                return "";
            }

            Document paymentResultDoc = DocumentHelper.parseText(paymentResult);
            Element root = paymentResultDoc.getRootElement();
            Map<String, String> response2541 = new HashMap<String, String>();
            for (Iterator<Element> it = root.elementIterator(); it.hasNext(); ) {
                Element element = it.next();
                response2541.put(element.getName(), element.getText());
            }

            QrpNotifyReq qrpNotifyReq = convertToQrpNotifyReq(response2541, twPayRequestData);
            tradeDetail.setTxParams(XmlUtil.marshall(qrpNotifyReq));

            TwPayRequestData responseObj = new ObjectMapper().convertValue(response2541, TwPayRequestData.class);
            LOGGER.info("[TWPay][Micropay][Response]:" + gson.toJson(responseObj));
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, new Gson().toJson(responseObj), data, tradeDetail);

            ResponseGeneralHeader responseGeneralHeader = TWPayUtil.getResponseHeader(responseObj, requestHeader,
                    integrateMchId, messageSource);

            IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();

            if ("0000".equals(responseObj.getResponseCode())) {
                integratedMicropayResponseData.setSysOrderNo(twPayRequestData.getOrderNumber());
                integratedMicropayResponseData.setTotalFee(responseObj.getAmt().substring(0, responseObj.getAmt().length() - 2).replaceFirst("^0+(?!$)", ""));
                appNotificationService.notifyApp(tradeDetail.getAccountId(), tradeDetail.getOrderId());
            } else {
                integratedMicropayResponseData.setSysOrderNo(SystemInstance.EMPTY_STRING);
                integratedMicropayResponseData.setTotalFee(SystemInstance.EMPTY_STRING);
            }

            JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
            integratedMicropayResponseData.setStoreOrderNo(requestData.get("StoreOrderNo").getAsString());
            integratedMicropayResponseData.setAuthCode(authCode);
            integratedMicropayResponseData.setPlatformRsp(paymentResult);
            integratedMicropayResponseData.setSysRefundNo(tradeDetail.getSystemOrderId());

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedMicropayResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);
            LOGGER.info("[TWPay][Micropay]integrate response ready :" + generalResponseData);

        } else if (requestHeader.getServiceType().equals("Refund")) {

            LOGGER.info("[TWPay][Refund][data]" + data.getAsString());

            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
            if (StringUtils.isBlank(tradeDetail.getCreditCardType()) && StringUtils.isNotBlank(tradeDetail.getBarcode())) {
                this.setTradeDetailCreditCardType(tradeDetail);
            }
            Gson gson = new Gson();

            String reqJson;
            String endStr = "";
//            if ("OLPay".equals(tradeDetail.getStatus()) && StringUtils.defaultIfBlank(tradeDetail.getTxParams(), "").contains("QrpEmvqrNotifyReq")) {
//                TWPayEmvqrRefundRequest emvqrRefundRequest = TWPayUtil.generateEmvqrRefund(tradeDetail);
//                reqJson = JsonUtil.toJsonStr(emvqrRefundRequest);
//            } else {
//                TWPayRefundRequest refundRequest = TWPayUtil.generateRefund2543Req(tradeDetail);
//                reqJson = JsonUtil.toJsonStr(refundRequest);
//            }
            try {
                TWPayRefundRequest refundRequest = TWPayUtil.generateRefund2543Req(tradeDetail);
                reqJson = JsonUtil.toJsonStr(refundRequest);
            } catch (Exception e) {
                TWPayEmvqrRefundRequest emvqrRefundRequest = TWPayUtil.generateEmvqrRefund(tradeDetail);
                reqJson = JsonUtil.toJsonStr(emvqrRefundRequest);
            }
            tradeDetailService.save(tradeDetail);
            LOGGER.info("Refund req => " + reqJson);
            String platformResponse = refundPayment(tradeDetail, reqJson);

            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, platformResponse, data);


            TwPayRequestData twPayRequestData = gson.fromJson(platformResponse, TwPayRequestData.class);
            ResponseGeneralHeader responseGeneralHeader = TWPayUtil.getResponseHeader(twPayRequestData, requestHeader, integrateMchId, messageSource);

            IntegratedRefundResponseData integratedRefundResponseData = TWPayUtil.getIntegratedRefundResponseData(
                    twPayRequestData, requestHeader, tradeDetail, gson.fromJson(data.getAsString(), IntegratedRefundRequestData.class));
            integratedRefundResponseData.setPlatformRsp(platformResponse);

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedRefundResponseData);
            generalResponseData = gson.toJson(responseGeneralBody);

            LOGGER.info("integrate response ready :" + generalResponseData);
        } else if ("SingleOrderQuery".equals(requestHeader.getServiceType())) {

            LOGGER.info("[TWPay][SingleOrderQuery][data]" + data.getAsString());

            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

            Gson gson = new Gson();
            String platformResult;
//			tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, platformResult, data);
//			tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
            QrpInqueryResp qrpInqueryResp;

            // 20190503 edit:Sol	因可能回調來源問題,可能無法像財金查單,故調整邏輯,若轉換出問題一律轉換成“交易失敗”
            try {
                platformResult = singleOrderQuery(paymentAccount, tradeDetail, gson);
                LOGGER.info("[TWPay][SingleOrderQuery][result].." + platformResult);
                qrpInqueryResp = gson.fromJson(platformResult, QrpInqueryResp.class);
                LOGGER.info("[TWPay][SingleOrderQuery][result] qrpInqueryResp => " + qrpInqueryResp);
            } catch (Exception e) {
                LOGGER.info("[TWPay][SingleOrderQuery][result][Exception].." + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
                Map<String, String> map = new HashMap<String, String>();
                map.put("tag08", "");
                qrpInqueryResp = new QrpInqueryResp();
                qrpInqueryResp.setOtherInfo(gson.toJson(map));
            }

            ResponseGeneralHeader responseGeneralHeader = TWPayUtil.getResponseHeader(qrpInqueryResp, requestHeader, integrateMchId, messageSource, gson);

            if (SystemInstance.TRADE_FAIL.equals(tradeDetail.getStatus())) {
                responseGeneralHeader.setMchId(integrateMchId);
                IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();
                integratedSingleOrderQueryResponseData.setStoreOrderNo(tradeDetail.getOrderId());
                integratedSingleOrderQueryResponseData.setSysOrderNo(StringUtils.defaultIfEmpty(tradeDetail.getSystemOrderId(), ""));
                integratedSingleOrderQueryResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
                integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());
                integratedSingleOrderQueryResponseData.setPaidAt(tradeDetail.getCreateDate());
                integratedSingleOrderQueryResponseData.setRefundedMsg(tradeDetail.getRefundStatus());
                integratedSingleOrderQueryResponseData.setOrderStatus("2");
                ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
                responseGeneralBody.setHeader(responseGeneralHeader);
                responseGeneralBody.setData(integratedSingleOrderQueryResponseData);
                generalResponseData = gson.toJson(responseGeneralBody);
                LOGGER.info("integrate response ready :" + generalResponseData);
                return generalResponseData;
            }


            responseGeneralHeader.setMchId(integrateMchId);
            IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = TWPayUtil.getIntegratedSingleOrderQueryResponseData(qrpInqueryResp, tradeDetail, gson);
            RefundDetail refundDetail = refundDetailService.getOneByOrderIdAndMethod(tradeDetail.getOrderId(), tradeDetail.getMethod());
            if (Objects.nonNull(refundDetail)) {
                integratedSingleOrderQueryResponseData.setRefundedAt(refundDetail.getCreateTime());
            }

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedSingleOrderQueryResponseData);
            generalResponseData = new Gson().toJson(responseGeneralBody);

            LOGGER.info("integrate response ready :" + generalResponseData);
        } else if ("changeKey".equals(requestHeader.getServiceType())) {
            changeKey(requestHeader, data, paymentAccount);

        }

        return generalResponseData;
    }

    private TwPayRequestData convertXml2Obj(String paymentResult) {

        Document doc;
        try {
            doc = DocumentHelper.parseText(paymentResult);
        } catch (DocumentException e) {
            e.printStackTrace();
            return null;
        }
        Element root = doc.getRootElement();
        Map<String, String> map = new HashMap<String, String>();
        for (Iterator<Element> it = root.elementIterator(); it.hasNext(); ) {
            Element element = it.next();
            map.put(element.getName(), element.getText());
        }
        return new ObjectMapper().convertValue(map, TwPayRequestData.class);
    }

    public String micropay(TwPayRequestData paymentRequestData, PaymentAccount paymentAccount) {

        Document doc = createMicorpayXml(paymentRequestData, "auth2541", new PaymentAccount(), new Device());
        LOGGER.info("[MICROPAY] request =>" + doc.asXML().replace("\n", ""));
        try {
            return xmlPost(env.getProperty("host.request.taiwanpay") + "auth2541", doc.asXML().replace("\n", ""));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public class StandaloneWriter extends XMLWriter {
        protected void writeDeclaration() throws IOException {
            OutputFormat format = getOutputFormat();
            String encoding = format.getEncoding();
            if (!format.isSuppressDeclaration()) {
                if (encoding.equals("UTF8")) {
                    writer.write("<?xml version=\"1.0\"");
                    if (!format.isOmitEncoding()) {
                        writer.write(" encoding=\"UTF-8\"");
                    }
                    writer.write(" standalone=\"true\"");
                    writer.write("?>");
                } else {
                    writer.write("<?xml version=\"1.0\"");
                    if (!format.isOmitEncoding()) {
                        writer.write(" encoding=\"" + encoding + "\"");
                    }
                    writer.write(" standalone=\"true\"");
                    writer.write("?>");
                }
                if (format.isNewLineAfterDeclaration()) {
                    println();
                }
            }
        }
    }

    public String getTerminalVerifyCode(String data, String verifyCodeParam) {
        try {
            LOGGER.info("[TerminalVerifyCode] " + data);
            byte[] byteKey = GeneralUtil.hex2Byte(verifyCodeParam);

            byte[] key;
            if (byteKey.length == 16) {
                key = new byte[24];
                System.arraycopy(byteKey, 0, key, 0, 16);
                System.arraycopy(byteKey, 0, key, 16, 8);
            } else {
                key = byteKey;
            }

            SecretKey secretKey = TripleDESUtil.convertTripleDESSecretKey(key);
            String hexEncrypted = Hex.encodeHexString(TripleDESUtil.des3EncodeCBCNoPadding(secretKey,
                    env.getProperty("twpay.3des.iv").getBytes(), data.getBytes()));

            return hexEncrypted.substring(hexEncrypted.length() - 8, hexEncrypted.length()).toUpperCase();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean prepareTwPayRequestData(TwPayRequestData twPayRequestData, PaymentAccount paymentAccount, TradeDetail tradeDetail) {
        boolean isOk = false;
        try {
            String authCode = twPayRequestData.getAuthCode();
            boolean isQrCode = !authCode.matches("^9[56].{20}$");
            String berTlvHexText = isQrCode ? TWPayUtil.decode2541PassiveQrCodeAsText(twPayRequestData.getAuthCode()) : "";
            BarCodeData barCodeData = isQrCode ? new BarCodeData() : TWPayUtil.decode2541PassiveBarCodeAsText(authCode);

            twPayRequestData.setAuthCode(null);
            twPayRequestData.setMti("0200");
            twPayRequestData.setCardNumber(isQrCode ? getCardNo(berTlvHexText) : barCodeData.getCardNum());
            twPayRequestData.setProcessingCode("002541");
            twPayRequestData.setAmt(String.format("%010d00", Integer.valueOf(twPayRequestData.getAmt())));
            twPayRequestData.setTraceNumber(String.valueOf(new Random().nextInt(900000) + 100000)); //必定要六位數
            twPayRequestData.setLocalTime(isQrCode ? getAuthTime(berTlvHexText) : barCodeData.getTime());
            twPayRequestData.setLocalDate(isQrCode ? getAuthDate(berTlvHexText) : barCodeData.getDate());
            twPayRequestData.setCountryCode("158");
            twPayRequestData.setPosEntryMode("071");
            twPayRequestData.setPosConditionCode("77");
            twPayRequestData.setAcqBank(paymentAccount.getBankCode());
            twPayRequestData.setTerminalId(paymentAccount.getAccountTerminalId());
            twPayRequestData.setMerchantId(paymentAccount.getAccount());

            String verifyCodeParam = StringUtils.defaultIfEmpty(paymentAccount.getPlatformCheckCode(), this.FISC_PARAMETER);
            TwPayOtherInfo otherInfo = getTwPayOtherInfo(twPayRequestData, StringUtils.defaultIfEmpty(paymentAccount.getDepositNo(), verifyCodeParam), berTlvHexText, isQrCode, authCode);
            if (!isQrCode) {
                otherInfo.setTag81(barCodeData.getKeep());
            }
            twPayRequestData.setOtherInfo(new Gson().toJson(otherInfo));
            twPayRequestData.setTxnCurrencyCode("901");
            String chipData = getChipData(berTlvHexText, twPayRequestData, authCode, isQrCode, barCodeData);
            twPayRequestData.setChipData(chipData);
            twPayRequestData.setVerifyCode(getVerifyCode(twPayRequestData, verifyCodeParam));
            isOk = true;
            this.setTradeDetailCreditCardType(tradeDetail);
            this.saveTradeDetailCardLog(tradeDetail);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
        }

        return isOk;
    }

    private String getProcessingCode(String berTlvText) {
        return berTlvText.substring(0, 4);
    }

    private String getAuthDate(String berTlvText) {
        return berTlvText.substring(4, 12);
    }

    private String getAuthTime(String berTlvText) {
        return berTlvText.substring(12, 18);
    }

    private String getIssuerCode(String berTlvText) {
        return berTlvText.substring(18, 26);
    }

    private byte[] getRemark(byte[] bytes) {
        return Arrays.copyOfRange(bytes, 26, 56);
    }

    private String getCardNo(String berTlvText) {
        return berTlvText.substring(56, 72);
    }

    private byte[] getWriteRecordWSnumTac(byte[] bytes) {

        return Arrays.copyOfRange(bytes, 72, 91);
    }

    private String getWalletType(String berTlvText) {
        return berTlvText.substring(91, 92);
    }

    public Document createMicorpayXml(TwPayRequestData twPayRequestData, String type, PaymentAccount paymentAccount,
                                      Device device) {

        Document doc = DocumentHelper.createDocument();
        String name = SystemInstance.EMPTY_STRING;
        String serverPath = SystemInstance.EMPTY_STRING;

        if (type.equals("auth2541")) {
            name = "FiscIIAuth2541Req";
            serverPath = "auth2541";
        }

        Element root = doc.addElement(name, "http://www.focas.fisc.com.tw/FiscII/" + serverPath);

        root.addElement("mti").addText(twPayRequestData.getMti());
        root.addElement("cardNumber").addText(twPayRequestData.getCardNumber());
        root.addElement("processingCode").addText(twPayRequestData.getProcessingCode());
        root.addElement("amt").addText(twPayRequestData.getAmt());
        root.addElement("traceNumber").addText(twPayRequestData.getTraceNumber());
        root.addElement("localTime").addText(twPayRequestData.getLocalTime());
        root.addElement("localDate").addText(twPayRequestData.getLocalDate());
        root.addElement("countryCode").addText(twPayRequestData.getCountryCode());
        root.addElement("posEntryMode").addText(twPayRequestData.getPosEntryMode());
        root.addElement("posConditionCode").addText(twPayRequestData.getPosConditionCode());
        root.addElement("acqBank").addText(twPayRequestData.getAcqBank());
        root.addElement("terminalId").addText(twPayRequestData.getTerminalId());
        root.addElement("merchantId").addText(twPayRequestData.getMerchantId());
        root.addElement("orderNumber").addText(twPayRequestData.getOrderNumber());
        root.addElement("otherInfo").addText(twPayRequestData.getOtherInfo());
        root.addElement("txnCurrencyCode").addText(twPayRequestData.getTxnCurrencyCode());
        root.addElement("chipData").addText(twPayRequestData.getChipData());
        root.addElement("verifyCode").addText(twPayRequestData.getVerifyCode());

        return doc;
    }

    private String getChipData(String berTlvText, TwPayRequestData twPayRequestData, String authCode, boolean isQrCode, BarCodeData barCodeData) {

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] bytes = isQrCode ? TWPayUtil.decode2541PassiveQrCodeAsHexBytes(authCode) : "".getBytes();
        try {
            outputStream.write("2541".getBytes());
            outputStream.write(isQrCode ? getIssuerCode(berTlvText).getBytes() : barCodeData.getBankCode().getBytes());
            outputStream.write(isQrCode ? getRemark(bytes) : TWPayUtil.hexStr2Btye("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")); // 晶片金融卡備註欄
            outputStream.write(Strings.padStart(twPayRequestData.getCardNumber(), 16, '0').getBytes());
            if (!isQrCode) {
                outputStream.write(0x08);
//                outputStream.write(barCodeData.getTSN().getBytes());
                outputStream.write(Strings.padStart(barCodeData.getTSN(), 8, '0').getBytes());
                outputStream.write(0x00);
                outputStream.write(0x08);
                outputStream.write(TWPayUtil.hexStr2Btye("FFFFFFFF"));
            }
            outputStream.write(isQrCode ? getWriteRecordWSnumTac(bytes) : barCodeData.getTAC()); // WRITE_RECORD_W_SNUM_TAC
            outputStream.write(0x15); // 固定 0x15
            outputStream.write("0000".getBytes()); // 固定0000
            outputStream.write("0000".getBytes()); // 分行代號; 不知道可補0
            outputStream.write("  ".getBytes());   // 2541交易固定2個空白
//            int amtLen = twPayRequestData.getAmt().length();
            outputStream.write("00000".getBytes()); // 促銷金額 5byte; 單位元

        } catch (IOException e) {
            e.printStackTrace();
        }

        return java.util.Base64.getEncoder().encodeToString(outputStream.toByteArray());
    }

    private TwPayOtherInfo getTwPayOtherInfo(TwPayRequestData twPayRequestData, String verifyCodeParam, String berTlvHexText, boolean isQrCode, String authCode) {
        TwPayOtherInfo otherInfo = new TwPayOtherInfo();

        String data = twPayRequestData.getTraceNumber() + twPayRequestData.getMerchantId()
                + twPayRequestData.getLocalDate() + twPayRequestData.getTerminalId() + twPayRequestData.getLocalTime()
                + twPayRequestData.getAcqBank() + "00";
        LOGGER.info("[getTwPayOtherInfo]  data => " + data);
        LOGGER.info("[getTwPayOtherInfo]  verifyCodeParam => " + verifyCodeParam);
        otherInfo.setTag05(getTerminalVerifyCode(data, verifyCodeParam));
        otherInfo.setTag12(getTag12ValueByBerTlv(berTlvHexText, isQrCode, authCode));
        if (isQrCode) {
            otherInfo.setTag80("B");
        } else {
            otherInfo.setTag80("C");
        }

        return otherInfo;
    }

    private String getTag12ValueByBerTlv(String berTlvHexText, boolean isQrCode, String authCode) {

        // 1: twpay app -> 雲支付 A50
        // 2: back wallet app -> 行動錢包  639
        if ((!isQrCode && authCode.startsWith("95")) || (isQrCode && "1".equals(getWalletType(berTlvHexText)))) {
            return "A50";
        } else {
            return "639";
        }
    }

    public String getVerifyCode(TwPayRequestData twPayRequestData, String verifyCodeParam) {

        Map map = new ObjectMapper().convertValue(twPayRequestData, TreeMap.class);
        Map<String, String> treeMap = new TreeMap<String, String>(map);

        StringBuilder checkStr = new StringBuilder();

        for (Map.Entry<String, String> entry : treeMap.entrySet()) {
            if (!entry.getKey().equals("verifyCode") && entry.getValue() != null) {
                checkStr.append(entry.getValue());
            }
        }

        checkStr.append(verifyCodeParam);
        return GeneralUtil.getHash(checkStr.toString()).toUpperCase();
    }

    public String xmlPost(String url, String query) throws Exception {

        URL requestUrl = new URL(url);
        HttpURLConnection con = (HttpsURLConnection) requestUrl.openConnection();
        con.setRequestMethod("POST");

        con.setRequestProperty("Content-length", String.valueOf(query.length()));
        con.setRequestProperty("Content-Type", "application/xml");
        con.setDoOutput(true);
        con.setDoInput(true);

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();
        DataInputStream input = new DataInputStream(con.getInputStream());

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(input));
        String result;

        StringBuilder buffer = new StringBuilder();
        while ((result = bufferedReader.readLine()) != null) {
            buffer.append(result);
        }

        input.close();
        return buffer.toString();
    }


    private TradeDetail setOnlineTradeDetailRequest(TwPayRequestData twpayRequestData, Merchant merchant,
                                                    PaymentAccount paymentAccount, QrcodeParameter qrcodeParameter) throws OrderAlreadyExistedException {
        LOGGER.info("enter trade detail request:" + new Gson().toJson(twpayRequestData));

        String storeOrderNo = null;
        if ("1".equals(qrcodeParameter.getCodeType())) {
            storeOrderNo = qrcodeParameter.getStoreOrderNo();
        } else if (!lookupCodeService.isFGSAccount(merchant.getAccountId())) {
            storeOrderNo = twpayRequestData.getSrrn();
        } else {
            storeOrderNo = orderService.createFGSOrderId(merchant.getAccountId()) + StringUtils.defaultIfBlank(qrcodeParameter.getStoreInfo(), "");
        }

        TradeDetail tradeDetail = tradeDetailService.getOne(merchant.getAccountId(), storeOrderNo);
        if (tradeDetail != null) {
            if (SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                throw new OrderAlreadyExistedException("Trade exist");
            }
            tradeDetail.setSystemOrderId(twpayRequestData.getSrrn());
            tradeDetail.setPaymentAccount(paymentAccount);
            tradeDetail.setMethod("1" + paymentAccount.getBank().getBankId() + "00");
            tradeDetailService.save(tradeDetail);
            return tradeDetail;
        } else {
            TradeDetail tradeDetail2 = tradeDetailService.getOne(storeOrderNo);
            if (Objects.nonNull(tradeDetail2)) {
                throw new OrderAlreadyExistedException("Trade exist");
            }
            tradeDetail = new TradeDetail();
        }

        long amount = Long.parseLong(twpayRequestData.getQrPayment().getTxnAmt()) / 100; // 還原送給 twpay 的金額; 原先放大100倍
        tradeDetail.setAccountId(merchant.getAccountId());
//        tradeDetail.setMethod("12000");
        tradeDetail.setMethod("1" + paymentAccount.getBank().getBankId() + "00");
        tradeDetail.setPaymentAccount(paymentAccount);
        tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        tradeDetail.setStatus("Creating order");


        tradeDetail.setOrderId(storeOrderNo);
        tradeDetail.setSystemOrderId(twpayRequestData.getSrrn());
        tradeDetail.setDeviceRandomId(deviceService.getOne(merchant, "APP-001"));
        tradeDetail.setPayment(amount);
        tradeDetail.setServiceType("OLPay");
        tradeDetail.setQrcodeToken(qrcodeParameter.getShortId());
        tradeDetail.setDescription(qrcodeParameter.getBody());
        tradeDetail.setDetail(qrcodeParameter.getDetail());

        if (qrcodeParameter.getOnSaleTotalFee() == amount) {
            tradeDetail.setOnSale(true);
        } else {
            tradeDetail.setOnSale(false);
        }
        tradeDetail.setOriginalPrice(qrcodeParameter.getTotalFee());

        try {
            LOGGER.info("save online trade detail : " + new ObjectMapper().writeValueAsString(tradeDetail));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            LOGGER.info("save online trade detail error!");
        }
        twpayRequestData.setOrderNumber(storeOrderNo);

        tradeDetail = tradeDetailService.save(tradeDetail);
        tradeProcedureService.captureOrderSave(tradeDetail);
        return tradeDetail;
    }

    private static RequestHeader covertToRequestHeader(QrpNotifyReq qrpNotifyReq, TradeDetail tradeDetail) {

        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setMerchantId(tradeDetail.getAccountId());

        return requestHeader;
    }

    @Override
    public void updateTradeDetailByQrpNotifyReq(QrpNotifyReq qrpNotifyReq, TradeDetail tradeDetail, String cardType) {

//        tradeDetail.setMethod("12000");
        Type typeOfMap = new TypeToken<Map<String, String>>() {
        }.getType();
        RequestHeader requestHeader = covertToRequestHeader(qrpNotifyReq, tradeDetail);

        Gson gson = new Gson();
        Map<String, String> otherInfo = gson.fromJson(qrpNotifyReq.getOtherInfo(), typeOfMap);
        String statusCode = otherInfo.get("tag11");

        Map<String, String> orgTxnData = gson.fromJson(qrpNotifyReq.getOrgTxnData(), typeOfMap);
        String originTxnType = orgTxnData.get("tag02");

        if ("000171".equals(qrpNotifyReq.getProcessingCode()) && "002541".equals(originTxnType)) { // 金融卡購貨通知
            if (statusCode != null && "0000".equals(statusCode)) {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    tradeDetail.setCreditCardType(cardType);
                    appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                }
            } else {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }
        } else if ("000171".equals(qrpNotifyReq.getProcessingCode()) && "002543".equals(originTxnType)) { // 金融卡購物退貨交易通知
            requestHeader.setServiceType("Refund");
        } else if ("000171".equals(qrpNotifyReq.getProcessingCode()) && "002542".equals(originTxnType)) { // 金融卡購物取消交易通知
            requestHeader.setServiceType("Cancel");
        }

        try {
            tradeDetail.setTxParams(XmlUtil.marshall(qrpNotifyReq));
            tradeDetailService.save(tradeDetail);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("[TwPay][updateTradeDetailByQrpNotifyReq]: save tradeDetail fail.");
            return;
        }

    }

    @Override
    public void webToAppReq(String shortId, String amount, HttpServletResponse response) throws Exception {

        PaymentAccount paymentAccount = this.getONLPayPaymentAccount(shortId);
        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(shortId);
        String orderNoForTwPay = orderService.createOrderIdBySequence(qrcodeParameter.getMchId());
        Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());

        TradeDetail tradeDetail = this.getONLPayTradeDetail(amount, orderNoForTwPay, merchant, paymentAccount, qrcodeParameter);
        amount = amount + "00";

        Map<String, String> formMap = new HashMap<>();
        formMap.put("acqBank", paymentAccount.getBankCode());
        formMap.put("terminalId", paymentAccount.getAccountTerminalId());
        formMap.put("merchantId", paymentAccount.getAccount());
        String reqQrcodeStr;
        IntTbLookupCode lookupCode = lookupCodeService.findOne("merchantPan", String.valueOf(paymentAccount.getBank().getBankId()));
        if (StringUtils.isBlank(paymentAccount.getSystemID()) || "01".equals(paymentAccount.getSpID()) || Objects.isNull(lookupCode)) {
            reqQrcodeStr = TWPayUtil.generateQrPaymentInfoString(paymentAccount, amount, orderNoForTwPay);
        } else {
            reqQrcodeStr = TWPayUtil.generateEMVCode(paymentAccount, amount, orderNoForTwPay, lookupCode);
        }
        LOGGER.info("reqQrcodeStr =>" + reqQrcodeStr);
        String key = StringUtils.defaultIfEmpty(paymentAccount.getPlatformCheckCode(), this.FISC_PARAMETER);
        formMap.put("encQRCode", TWPayUtil.encodeWeb2App(reqQrcodeStr, StringUtils.defaultIfEmpty(paymentAccount.getDepositNo(), key), "Qrcode", paymentAccount.getSftpPasswd()));
//        formMap.put("encRetURL", TWPayUtil.encodeWeb2App(API_URL + "allpaypass/api/twpay/webToAppNotify", key, "URL"));
//        formMap.put("encRetURL", TWPayUtil.encodeWeb2App(String.format("%sallpaypass/api/payment/order/result?orderId=%s", API_URL, EncryptUtil.encryptSimple(tradeDetail.getOrderId())), key, "URL"));
        formMap.put("encRetURL", TWPayUtil.encodeWeb2App(String.format("%sallpaypass/api/twpay/webToAppCallBack", API_URL), StringUtils.defaultIfEmpty(paymentAccount.getDepositNo(), key), "URL", paymentAccount.getSftpPasswd()));
        formMap.put("orderNumber", orderNoForTwPay);
        formMap.put("txnType", StringUtils.defaultIfBlank(paymentAccount.getSpID(), "01"));    // 01 FISC 金融卡 2541   04 信用卡購物主掃
        formMap.put("preferWalletList", "94901,00401,00601,01702");//排序
        formMap.put("verifyCode", TWPayUtil.encryptForWebToApp(formMap, key));

        HttpUtil.sendForm(response, FOCAS_URL + "WebToAppReq", formMap);
    }

    @Override
//    public void updateTradeDetailByWebToApp(WebToAppNotify webToAppNotify, TradeDetail tradeDetail) {
    public void updateTradeDetailByWebToApp(QrpNotifyResp qrpNotifyResp, TradeDetail tradeDetail) {


        String respCode = qrpNotifyResp.getResponseCode();

        String tradeDetailBefortStatus = tradeDetail.getStatus();
        if ("0000".equals(respCode) && !SystemInstance.TRADE_SUCCESS.equals(tradeDetailBefortStatus)) {
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
        } else if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetailBefortStatus)) {
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
        }

        if (StringUtils.isBlank(tradeDetail.getTxParams())) {
            tradeDetail.setTxParams(JsonUtil.toJsonStr(qrpNotifyResp));
        }
        tradeDetailService.save(tradeDetail);

        if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetailBefortStatus) && SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
            appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
        }
    }

    @Override
    public QrpNotifyResp getQrpNotifyResp(QrpNotifyReq qrpNotifyReq) {

        QrpNotifyResp res = new QrpNotifyResp();
        String[] ignoreProperties = new String[]{"localTime", "localDate", "countryCode", "posEntryMode",
                "posConditionCode", "otherInfo", "txnCurrencyCode", "verifyCode", "orgTxnData"};
        BeanUtils.copyProperties(qrpNotifyReq, res, ignoreProperties);

        try {
            res.setResponseCode("0000");
            res.setMti("0810");
            res.setVerifyCode(TWPayUtil.getFiscVerifyCode(res));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return res;
    }

    public String refundPayment(TradeDetail tradeDetail, String reqJson) {
        String url = "refund2543";
        if (SystemInstance.TYPE_OLPAY.equals(tradeDetail.getServiceType()) && StringUtils.defaultIfBlank(tradeDetail.getTxParams(), "").contains("QrpEmvqrNotifyReq")) {
            url += "Emvqr";
        } else if (SystemInstance.TYPE_MICROPAY.equals(tradeDetail.getServiceType()) && "8".equals(tradeDetail.getCreditCardType())) {
            url += SystemInstance.TYPE_MICROPAY;
        }
        LOGGER.info("[TWPAY] [refundPayment] reqJson = " + reqJson);
        String result = restTemplate.postForObject(PAYMENT_GATEWAY_API_ORIGIN + "/twpay/api/v1/fisc/" + url, reqJson, String.class);
        LOGGER.info("[TWPAY] [refundPayment] result = " + result);
        return result;
    }

    public String singleOrderQuery(PaymentAccount paymentAccount, TradeDetail tradeDetail, Gson gson) {

        QrpInqueryReq qrpInqueryReq = TWPayUtil.generateQrpInqueryReq(paymentAccount, tradeDetail, FISC_HOST_ID);
        String url = "";
        if ("Micropay".equals(tradeDetail.getServiceType())) {
            url = "Micropay";
        }
        return restTemplate.postForObject(PAYMENT_GATEWAY_API_ORIGIN + "/twpay/api/v1/fisc/inquiry" + url, gson.toJson(qrpInqueryReq), String.class);
    }

    private QrpNotifyReq convertToQrpNotifyReq(Map<String, String> response2541, TwPayRequestData twPayRequestData) {

        QrpNotifyReq qrpNotifyReq = new QrpNotifyReq();

        qrpNotifyReq.setMti(response2541.get("mti"));
        qrpNotifyReq.setOrderNumber(response2541.get("orderNumber"));
        qrpNotifyReq.setTraceNumber(response2541.get("traceNumber"));
        qrpNotifyReq.setProcessingCode(response2541.get("processingCode"));
        qrpNotifyReq.setAmt(response2541.get("amt"));
        qrpNotifyReq.setTerminalId(response2541.get("terminalId"));
        qrpNotifyReq.setSrrn(response2541.get("srrn"));
        qrpNotifyReq.setAcqBank(response2541.get("acqBank"));
        qrpNotifyReq.setMerchantId(response2541.get("merchantId"));
        qrpNotifyReq.setSystemDateTime(response2541.get("systemDateTime"));
        qrpNotifyReq.setCardNumber(response2541.get("cardNumber"));
        qrpNotifyReq.setOtherInfo(response2541.get("otherInfo"));
        qrpNotifyReq.setLocalDate(twPayRequestData.getLocalDate());
        qrpNotifyReq.setLocalTime(twPayRequestData.getLocalTime());
        qrpNotifyReq.setTxnCurrencyCode(twPayRequestData.getTxnCurrencyCode());

        Map<String, String> orgTxnData = new HashMap<>();
        orgTxnData.put("tag01", twPayRequestData.getMti());
        orgTxnData.put("tag02", response2541.get("processingCode"));

        qrpNotifyReq.setOrgTxnData(new Gson().toJson(orgTxnData));
        qrpNotifyReq.setHostId(twPayRequestData.getHostId() != null ? twPayRequestData.getHostId() : FISC_HOST_ID);

        return qrpNotifyReq;
    }


    /**
     * 更新KEY
     *
     * @param requestHeader
     * @param data
     * @param paymentAccount
     * @return
     */
    private String changeKey(RequestHeader requestHeader, JsonPrimitive data, PaymentAccount paymentAccount) {

        Map<String, String> reqMap = new HashMap<>();
        reqMap.put("terminalId", paymentAccount.getAccountTerminalId());
        reqMap.put("merchantId", paymentAccount.getAccount());
        reqMap.put("acqBank", paymentAccount.getBankCode());

        LOGGER.info("[changeKey] reqMap : " + reqMap);
        String result = restTemplate.postForObject(PAYMENT_GATEWAY_API_ORIGIN + "/twpay/api/v1/fisc/changeKey", new Gson().toJson(reqMap), String.class);
        LOGGER.info("[changeKey] result : " + result);

        Map<String, String> resultMap = null;
        try {
            resultMap = new ObjectMapper().readValue(result, HashMap.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    /**
     * 根據授權碼 判別為 信用卡 或 金融卡
     *
     * @param tradeDetail
     */
    @Override
    public void setTradeDetailCreditCardType(TradeDetail tradeDetail) {
        String authCode = tradeDetail.getBarcode();
        boolean isQrCode = !authCode.matches("^9[56].{20}$");  // 金融卡才有qrCode
        String cardType = "8";
        if (!isQrCode) {
            Base64.Decoder decoder = Base64.getDecoder();
            byte[] authCodeBtye = decoder.decode(authCode.substring(2));
            String hexStr = DatatypeConverter.printHexBinary(authCodeBtye);
            //1:VISA 2:Master 3 JCB 4:American Express 5:UnionPay(中國銀聯) 6:Diners Club  7:不分卡別-金融卡 8:不分卡別:-信用卡
            if (hexStr.startsWith("0")) {
                cardType = "7";
            } else if (hexStr.startsWith("1")) {
                String item3 = hexStr.substring(24);
                if (item3.startsWith("0")) {
                    cardType = "7";
                }
            }
        } else {
            cardType = "7";
        }
        tradeDetail.setCreditCardType(cardType);
    }

    /**
     * 新增TradeDetailCredLog
     *
     * @param tradeDetail
     */
    @Override
    public void saveTradeDetailCardLog(TradeDetail tradeDetail) {
        TradeDetailCardLog tradeDetailCardLog = new TradeDetailCardLog();
        String authCode = tradeDetail.getBarcode();
        boolean isQrCode = !authCode.matches("^9[56].{20}$");
        String berTlvHexText = isQrCode ? TWPayUtil.decode2541PassiveQrCodeAsText(authCode) : "";
        BarCodeData barCodeData = isQrCode ? new BarCodeData() : TWPayUtil.decode2541PassiveBarCodeAsText(authCode);

        tradeDetailCardLog.setOrderId(tradeDetail.getOrderId());
        tradeDetailCardLog.setCardType(tradeDetail.getCreditCardType());
        MethodToBankCode methodToBankCode = MethodToBankCode.findMethodToBankCode(tradeDetail.getMethod());

        if (Objects.isNull(methodToBankCode)) {
            return;
        }
        tradeDetailCardLog.setBeneficiaryBank(methodToBankCode.getBankCode());

        String issuBank = isQrCode ? getIssuerCode(berTlvHexText) : barCodeData.getBankCode();
        tradeDetailCardLog.setIssuBank(issuBank.substring(0, issuBank.length() - 5));
        tradeDetailCardLog.setCardNumber(isQrCode ? getCardNo(berTlvHexText) : barCodeData.getCardNum());

        tradeDetailCardLogService.save(tradeDetailCardLog);
    }


    private PaymentAccount getONLPayPaymentAccount(String shortId) throws OtherAPIException {
        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(shortId);
        if (StringUtils.isBlank(shortId) || Objects.isNull(qrcodeParameter)) {
            throw new OtherAPIException(" qrcode not found. ");
        }

        PaymentAccount tobPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), Bank.TOBTWPAY.getId());
        PaymentAccount fbPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), Bank.FBTWPAY.getId());
        PaymentAccount twPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), Bank.TWPAY.getId());
        PaymentAccount lbPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), Bank.LBTWPAY.getId());
        PaymentAccount tcbPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), Bank.TCBTWPAY.getId());
        PaymentAccount megaPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), Bank.MEGATWPAY2.getId());

        PaymentAccount paymentAccount = null;
        if (null != tobPaymentAccount) {
            paymentAccount = tobPaymentAccount;
        } else if (null != fbPaymentAccount) {
            paymentAccount = fbPaymentAccount;
        } else if (null != twPaymentAccount) {
            paymentAccount = twPaymentAccount;
        } else if (null != lbPaymentAccount) {
            paymentAccount = lbPaymentAccount;
        } else if (null != tcbPaymentAccount) {
            paymentAccount = tcbPaymentAccount;
        } else if (null != megaPaymentAccount) {
            paymentAccount = megaPaymentAccount;
        }
        if (paymentAccount == null) {
            throw new OtherAPIException(messageSource.getMessage("response.7203", null, locale));
        }

        return paymentAccount;
    }


    private TradeDetail getONLPayTradeDetail(String amount, String sysOrderId, Merchant merchant, PaymentAccount paymentAccount, QrcodeParameter qrcodeParameter) throws OrderAlreadyExistedException {
        TwPayRequestData twPayRequestData = new TwPayRequestData();
        twPayRequestData.setSrrn(sysOrderId);
        twPayRequestData.setOrderNbr(sysOrderId);

        QRPayment qrPayment = new QRPayment();
        qrPayment.setTxnAmt(amount + "00"); // twpay 交易單位放大百倍
        twPayRequestData.setQrPayment(qrPayment);

        TradeDetail tradeDetail = null;
//        try {
        tradeDetail = setOnlineTradeDetailRequest(twPayRequestData, merchant, paymentAccount, qrcodeParameter);
//            if (Objects.isNull(tradeDetail)) {
//                throw new OrderAlreadyExistedException("Trade exist");
//            }
//        } catch (Exception e) {
//            tradeDetail = tradeDetailService.getOne(twPayRequestData.getOrderNumber());
//            if (Objects.nonNull(tradeDetail)) {
//                tradeDetail.setSystemOrderId(twPayRequestData.getSrrn());
//                tradeDetail.setPaymentAccount(paymentAccount);
//                tradeDetail.setMethod("1" + paymentAccount.getBank().getBankId() + "00");
//                tradeDetailService.save(tradeDetail);
//            }
//            e.printStackTrace();
//            LOGGER.info("[twpay][set trade exception]" + e.getMessage());
//        }
        return tradeDetail;
    }

    @Override
    public QrpEmcqrNotifyResp getQrpEmvqrNotifyResp(QrpEmvqrNotifyReq qrpNotifyReq) {

        QrpEmcqrNotifyResp res = new QrpEmcqrNotifyResp();
        String[] ignoreProperties = new String[]{"localTime", "localDate", "countryCode", "posEntryMode",
                "posConditionCode", "otherInfo", "txnCurrencyCode", "verifyCode", "orgTxnData"};
        BeanUtils.copyProperties(qrpNotifyReq, res, ignoreProperties);

        try {
            res.setResponseCode("0000");
            res.setMti("0810");
            res.setVerifyCode(TWPayUtil.getFiscVerifyCode(res));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return res;
    }

    @Override
    public void updateTradeDetailByQrpEmvqrNotifyReq(QrpEmvqrNotifyReq qrpNotifyReq, TradeDetail tradeDetail) {

//        tradeDetail.setMethod("12000");
        Type typeOfMap = new TypeToken<Map<String, String>>() {
        }.getType();
        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setMerchantId(tradeDetail.getAccountId());

        Gson gson = new Gson();
        Map<String, String> otherInfo = gson.fromJson(qrpNotifyReq.getOtherInfo(), typeOfMap);
        String statusCode = otherInfo.get("tag11");

        Map<String, String> orgTxnData = gson.fromJson(qrpNotifyReq.getOrgTxnData(), typeOfMap);
        String originTxnType = orgTxnData.get("tag02");

        if ("280000".equals(qrpNotifyReq.getProcessingCode()) && "280000".equals(originTxnType)) { // 金融卡購貨通知
            if ("0000".equals(statusCode)) {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    tradeDetail.setCreditCardType("8");
                    appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                }
            } else {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }
        } else if ("280000".equals(qrpNotifyReq.getProcessingCode()) && "002543".equals(originTxnType)) { // 金融卡購物退貨交易通知
            requestHeader.setServiceType("Refund");
        } else if ("280000".equals(qrpNotifyReq.getProcessingCode()) && "002542".equals(originTxnType)) { // 金融卡購物取消交易通知
            requestHeader.setServiceType("Cancel");
        }

        try {
            tradeDetail.setTxParams(XmlUtil.marshall(qrpNotifyReq));
            tradeDetailService.save(tradeDetail);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("[TwPay][updateTradeDetailByQrpNotifyReq]: save tradeDetail fail.");
            return;
        }

    }
}
