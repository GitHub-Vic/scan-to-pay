package co.intella.service.impl;

import co.intella.crypto.AesCryptoUtil;
import co.intella.crypto.KeyReader;
import co.intella.crypto.RsaCryptoUtil;
import co.intella.domain.ali.*;
import co.intella.domain.integration.*;
import co.intella.domain.weixin.WeixinJsapiRequestData;
import co.intella.exception.InvalidDataFormatException;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import co.intella.net.Constant;
import co.intella.net.HttpRequestUtil;
import co.intella.service.AliService;
import co.intella.service.PaymentAccountService;
import co.intella.service.TradeDetailService;
import co.intella.service.TradeProcedureService;
import co.intella.utility.AliPayUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.crypto.SecretKey;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Miles Wu
 */
@Service
@Transactional
public class AliServiceImpl implements AliService {

    private final Logger LOGGER = LoggerFactory.getLogger(AliServiceImpl.class);

    private static final String BANK_KEY_VERSION = "004";

    private static PrivateKey PRIVATE_KEY;

    @Value("${host.request.yuanta.offline}")
    private String HOST;

    @Value("${host.request.jsapi}")
    private String JSAPI_HOST;

    @Autowired
    private Environment env;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Autowired
    private MessageSource messageSource;

    public String doRequest(AliBasicRequestData request, String requestType) throws Exception {
        String requestJson = new ObjectMapper().writeValueAsString(request);

        SecretKey key = AesCryptoUtil.generateSecreteKey();
        String requestParam = getParameters(requestJson, requestType, key);
        LOGGER.info("[HOST].."+HOST+" [requestParam]... " + requestParam);

        String result = yuantaPost(HOST, requestParam);
        AliResponseBody aliResponseBody = new Gson().fromJson(URLDecoder.decode(result, "UTF-8"), AliResponseBody.class);

        if("F".equals(aliResponseBody.getStatus())) {
            return new Gson().toJson(aliResponseBody);
        }

        String decryptResult = getDecryptResponse(aliResponseBody, key);
        aliResponseBody.setSecureData(decryptResult);
        LOGGER.info("[decryptResult].. " + decryptResult);

        return new Gson().toJson(aliResponseBody);
    }

    public String callJsapi(WeixinJsapiRequestData request, String requestType) throws Exception {
        LOGGER.info(new Gson().toJson(request));
        Map<String, String> map = new HashMap<String, String>();
        //commom
        map.put("ver", request.getVer());
        map.put("method", request.getMethod());
        map.put("mch_id", request.getMchId());
        map.put("nonce_str", request.getNonceStr() == null ? "" : request.getNonceStr());

        //JSAPI
        map.put("time_expire", request.getTimeExpire());
        map.put("trade_type", request.getTradeType());
        map.put("out_trade_no", request.getOutTradeNo());
        map.put("body", request.getBody()== null ? "" : request.getBody());
        map.put("fee_type", request.getFeeType()== null ? "TWD" : request.getFeeType());
        map.put("total_fee", request.getTotalFee());
        map.put("detail", request.getDetail()== null ? "" : request.getDetail());
        map.put("device_info", request.getDeviceInfo());
        LOGGER.info(map.toString());

        return HttpRequestUtil.post(JSAPI_HOST, map, getPrivateKey());
    }

    public String callRefund(WeixinJsapiRequestData request, String requestType) throws Exception {
        LOGGER.info(new Gson().toJson(request));
        Map<String, String> map = new HashMap<String, String>();
        //commom
        map.put("ver", request.getVer());
        map.put("method", request.getMethod());
        map.put("mch_id", request.getMchId());
        map.put("nonce_str", request.getNonceStr() == null ? "" : request.getNonceStr());

        //JSAPI
        map.put("trade_type", request.getTradeType());
        map.put("out_trade_no", request.getOutTradeNo());
        map.put("body", request.getBody()== null ? "" : request.getBody());
        map.put("fee_type", request.getFeeType()== null ? "TWD" : request.getFeeType());
        map.put("total_fee", request.getTotalFee());
        LOGGER.info(map.toString());

        return HttpRequestUtil.post(JSAPI_HOST, map, getPrivateKey());
    }

    private String getErrorResponse(AliResponseBody responseBody, String type) {

        // todo compose integrated response body
        ResponseGeneralHeader header = new ResponseGeneralHeader();
        header.setServiceType(type);
        header.setStatusDesc(responseBody.getErrorDesc());
        ResponseGeneralData data = new ResponseGeneralData();
//        data.setPlatformRsp(new Gson().toJson(responseBody));

        ResponseGeneralBody body = new ResponseGeneralBody();
//        body.setData(data);
        body.setHeader(header);

        IntegratedResponse res = new IntegratedResponse();
        res.setResult(new Gson().toJson(body));

        return new Gson().toJson(res);
    }

    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {
        String result;
        String generalResponseData="";
        String integrateMchId = requestHeader.getMerchantId();

        if(SystemInstance.TYPE_MICROPAY.equals(requestHeader.getServiceType())) {
            AliMicropayRequestData requestData = new Gson().fromJson(data.getAsString(), AliMicropayRequestData.class);
            JsonObject requestJson = new JsonParser().parse(new Gson().toJson(requestData)).getAsJsonObject();

            requestData.setOrderCurrency(SystemInstance.CURRENCY_TWD);

            if(Strings.isNullOrEmpty(requestHeader.getMerchantId()) || Strings.isNullOrEmpty(requestData.getTermId()) || Strings.isNullOrEmpty(requestData.getBuyerId())
                    || !requestJson.has("timeout") || Strings.isNullOrEmpty(requestData.getOrderNumber()) || Strings.isNullOrEmpty(requestData.getOrderCurrency())
                    || !requestJson.has("TotalFee") || Strings.isNullOrEmpty(requestHeader.getCreateTime()) || Strings.isNullOrEmpty(requestData.getOrderTitle())) {
                throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
            }

            tradeProcedureService.setTradeDetailRequest(requestHeader,data);
            requestData.setStoreId(requestHeader.getMerchantId());
            requestData.setOrderDate(requestHeader.getCreateTime());
            requestData.setTermId("SCTW05");
            requestData.setTimeout(30);


            result = doRequest(requestData, AliRequestBody.PAYMENT);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader,result, data);
            AliResponseBody aliResponseBody = new Gson().fromJson(result, AliResponseBody.class);

            ResponseGeneralHeader responseGeneralHeader = new AliPayUtil().getAliResponseHeader(aliResponseBody, requestHeader,messageSource);

            IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();

            AliMicropayResponseData aliMicropayResponseData = new Gson().fromJson(aliResponseBody.getSecureData(),AliMicropayResponseData.class);

            try {
                integratedMicropayResponseData.setSysOrderNo(aliMicropayResponseData.getBankOrderNo());
                integratedMicropayResponseData.setTotalFee(String.valueOf(aliMicropayResponseData.getRealOrderAmount()));
            } catch (Exception ex) {
                integratedMicropayResponseData.setSysOrderNo(SystemInstance.EMPTY_STRING);
                integratedMicropayResponseData.setTotalFee(SystemInstance.EMPTY_STRING);

            }

            integratedMicropayResponseData.setStoreOrderNo(requestData.getOrderNumber());
            integratedMicropayResponseData.setAuthCode(requestData.getBuyerId());
            integratedMicropayResponseData.setPlatformRsp(new Gson().toJson(aliResponseBody));

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedMicropayResponseData);


            generalResponseData = new Gson().toJson(responseGeneralBody);
            LOGGER.info("integrate response ready :"+generalResponseData);

        } else if("Refund".equals(requestHeader.getServiceType())) {
            AliRefundRequestData requestData = new Gson().fromJson(data.getAsString(), AliRefundRequestData.class);
            JsonObject requestJson = new JsonParser().parse(new Gson().toJson(requestData)).getAsJsonObject();


            if(Strings.isNullOrEmpty(requestHeader.getMerchantId()) || Strings.isNullOrEmpty(requestData.getTermId()) || !requestJson.has("timeout")
                    || Strings.isNullOrEmpty(requestData.getOrderNum()) || Strings.isNullOrEmpty(requestData.getRefundOrderNum())) {
                throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
            }

            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader,data);

            if ("OLPay".equals(tradeDetail.getServiceType())){

                //WeixinJsapiRequestData requestData2  = new Gson().fromJson(data.getAsString(), WeixinJsapiRequestData.class);
                WeixinJsapiRequestData weixinJsapiRequestData = new WeixinJsapiRequestData();
                long bankId =Long.valueOf(env.getProperty("bank.id.10220"));

                PaymentAccount paymentAccount = paymentAccountService.getOne(tradeDetail.getAccountId(), bankId);

                weixinJsapiRequestData.setVer("101");
                weixinJsapiRequestData.setMchId(paymentAccount.getAccount());
                weixinJsapiRequestData.setMethod("12");
                weixinJsapiRequestData.setNonceStr(null);


                weixinJsapiRequestData.setTradeType("REFUND");
                weixinJsapiRequestData.setOutTradeNo(requestData.getOrderNum());
                weixinJsapiRequestData.setBody("refund");
                weixinJsapiRequestData.setFeeType("TWD");
                weixinJsapiRequestData.setTotalFee(String.valueOf(tradeDetail.getPayment()));

                String result2 = callRefund(weixinJsapiRequestData,"");
                //todo online refund
                LOGGER.info(result2);

            }

            else {
                requestData.setStoreId(requestHeader.getMerchantId());
                requestData.setOrderDate(requestHeader.getCreateTime());
                requestData.setOrderNum(tradeDetailService.getOne(integrateMchId, requestData.getOrderNum()).getOrderId());
                requestData.setTimeout(30);
                requestData.setTermId("SCTW05");
                result = doRequest(requestData, AliRequestBody.REFUND);
                tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);

                AliResponseBody aliResponseBody = new Gson().fromJson(result, AliResponseBody.class);
                ResponseGeneralHeader responseGeneralHeader = new AliPayUtil().getAliResponseHeader(aliResponseBody, requestHeader,messageSource);
                IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();
                AliRefundResponseData aliRefundResponseData = new Gson().fromJson(aliResponseBody.getSecureData(), AliRefundResponseData.class);
                try {
                    integratedRefundResponseData.setSysRefundNo(aliRefundResponseData.getBankRefundOrderNo());
                    integratedRefundResponseData.setRefundedAt(aliRefundResponseData.getBankRefundOrderDate());
                } catch (NullPointerException ex) {
                    integratedRefundResponseData.setSysRefundNo(null);
                    integratedRefundResponseData.setRefundedAt(null);
                }
                integratedRefundResponseData.setStoreRefundNo(requestData.getRefundOrderNum());
                integratedRefundResponseData.setSysOrderNo(requestData.getOrderNum());
                integratedRefundResponseData.setStoreOrderNo(requestData.getOrderNum());

                ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
                responseGeneralBody.setHeader(responseGeneralHeader);
                responseGeneralBody.setData(integratedRefundResponseData);

                generalResponseData = new Gson().toJson(responseGeneralBody);
            }

        } else if("Currency".equals(requestHeader.getServiceType())) {
            AliCurrencyRequestData requestData = new Gson().fromJson(data.getAsString(), AliCurrencyRequestData.class);
            JsonObject requestJson = new JsonParser().parse(new Gson().toJson(requestData)).getAsJsonObject();
            setPlatformMchId(requestHeader);
            requestData.setStoreId(requestHeader.getMerchantId());
            requestData.setAmount(requestData.getAmount());
            requestData.setCurrency(requestData.getCurrency());
            requestData.setTermId("SCTW05");
            requestData.setTimeout(30);
            if(Strings.isNullOrEmpty(requestData.getStoreId()) || Strings.isNullOrEmpty(requestData.getTermId()) || !requestJson.has("timeout") || !requestJson.has("Amount")) {
                throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
            }


            result = doRequest(requestData, AliRequestBody.CURRENCY);

            AliResponseBody aliResponseBody = new Gson().fromJson(result, AliResponseBody.class);
            ResponseGeneralHeader responseGeneralHeader = new AliPayUtil().getAliResponseHeader(aliResponseBody, requestHeader,messageSource);
            IntegratedCurrencyResponseData integratedCurrencyResponseData = new IntegratedCurrencyResponseData();
            AliCurrencyResponseData aliCurrencyResponseData = new Gson().fromJson(aliResponseBody.getSecureData(),AliCurrencyResponseData.class);
            integratedCurrencyResponseData.setCurrency(SystemInstance.CURRENCY_TWD);
            integratedCurrencyResponseData.setExchangeCurrency(requestData.getCurrency() == null ? "CNY":requestData.getCurrency());
            integratedCurrencyResponseData.setExchangeRate(aliCurrencyResponseData.getExchangeRate());
            integratedCurrencyResponseData.setExchangeAmount(aliCurrencyResponseData.getExchangeAmount());

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedCurrencyResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);


        } else if("SingleOrderQuery".equals(requestHeader.getServiceType())) {
            AliQueryRequestData requestData = new Gson().fromJson(data.getAsString(), AliQueryRequestData.class);
            JsonObject requestJson = new JsonParser().parse(new Gson().toJson(requestData)).getAsJsonObject();
            //mapping store order to system order
            String storeOrderId = requestData.getOrderNo();
            String sysOrderId = tradeDetailService.getOne(requestHeader.getMerchantId(),requestData.getOrderNo()).getSystemOrderId();
            setPlatformMchId(requestHeader);
            requestData.setStoreId(requestHeader.getMerchantId());
            requestData.setOrderNo(storeOrderId);
            requestData.setOrderDate(requestHeader.getCreateTime());
            requestData.setTermId("SCTW05");
            requestData.setTimeout(30);
            if(Strings.isNullOrEmpty(requestData.getStoreId()) || !requestJson.has("timeout")) {
                // todo termid, orderid?
                throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
            }

            result = doRequest(requestData, AliRequestBody.QUERY);

            AliResponseBody aliResponseBody = new Gson().fromJson(result, AliResponseBody.class);
            ResponseGeneralHeader responseGeneralHeader = new AliPayUtil().getAliResponseHeader(aliResponseBody, requestHeader,messageSource);

            IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();
            AliQueryResponseData aliQueryResponseData = new Gson().fromJson(aliResponseBody.getSecureData(),AliQueryResponseData.class);

            integratedSingleOrderQueryResponseData.setStoreOrderNo(storeOrderId);
            integratedSingleOrderQueryResponseData.setSysOrderNo(sysOrderId);
            integratedSingleOrderQueryResponseData.setFeeType(aliQueryResponseData.getOrderCurrency());
            integratedSingleOrderQueryResponseData.setDeviceInfo(requestData.getTermId());
            integratedSingleOrderQueryResponseData.setBody(aliQueryResponseData.getOrderDesc());

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedSingleOrderQueryResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);

        } else if("QueryAction".equals(requestHeader.getServiceType())) {
            AliQueryActionRequestData requestData = new Gson().fromJson(data.getAsString(), AliQueryActionRequestData.class);
            JsonObject requestJson = new JsonParser().parse(new Gson().toJson(requestData)).getAsJsonObject();
            setPlatformMchId(requestHeader);
            requestData.setStoreId(requestHeader.getMerchantId());
            requestData.setTimeout(6);
            requestData.setTermId("SCTW05");
            requestData.setTimeout(30);
            if(Strings.isNullOrEmpty(requestData.getStoreId()) || Strings.isNullOrEmpty(requestData.getTermId()) || !requestJson.has("timeout")
                    || Strings.isNullOrEmpty(requestData.getOrderNo())) {
                throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
            }
            result = doRequest(requestData, AliRequestBody.QUERY_ACTION);

            AliResponseBody aliResponseBody = new Gson().fromJson(result, AliResponseBody.class);
            ResponseGeneralHeader responseGeneralHeader = new AliPayUtil().getAliResponseHeader(aliResponseBody, requestHeader,messageSource);

            IntegratedQueryActionResponseData integratedQueryActionResponseData = new IntegratedQueryActionResponseData();
            AliQueryActionResponseData aliQueryActionResponseData = new Gson().fromJson(aliResponseBody.getSecureData(),AliQueryActionResponseData.class);

            integratedQueryActionResponseData.setOrderNo(requestData.getOrderNo());
            integratedQueryActionResponseData.setIsRefundEnable(aliQueryActionResponseData.getIsRefundActionEnable());
            integratedQueryActionResponseData.setIsCancelEnable(aliQueryActionResponseData.getIsCancelActionEnable());
            integratedQueryActionResponseData.setRefundMax(aliQueryActionResponseData.getRefundMax());
            integratedQueryActionResponseData.setRefundMin(aliQueryActionResponseData.getRefundMin());

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedQueryActionResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);
        } else if("Cancel".equals(requestHeader.getServiceType())) {
            AliCancelRequestData requestData = new Gson().fromJson(data.getAsString(), AliCancelRequestData.class);
            JsonObject requestJson = new JsonParser().parse(new Gson().toJson(requestData)).getAsJsonObject();
            String storeOrderId = requestData.getOrderNumber();
            TradeDetail tradeDetail = tradeDetailService.getOne(requestHeader.getMerchantId(),storeOrderId);
            String sysOrderId = tradeDetail.getSystemOrderId();

            setPlatformMchId(requestHeader);

            requestData.setStoreId(requestHeader.getMerchantId());
            requestData.setOrderDate(requestHeader.getCreateTime());
            requestData.setOrderNumber(tradeDetail.getOrderId());
            requestData.setTermId("SCTW05");
            requestData.setTimeout(30);
            if(Strings.isNullOrEmpty(requestData.getStoreId()) || Strings.isNullOrEmpty(requestData.getTermId()) || !requestJson.has("timeout")
                    || Strings.isNullOrEmpty(requestData.getOrderNumber())) {
                throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
            }

            result = doRequest(requestData, AliRequestBody.CANCEL);

            AliResponseBody aliResponseBody = new Gson().fromJson(result, AliResponseBody.class);
            ResponseGeneralHeader responseGeneralHeader = new AliPayUtil().getAliResponseHeader(aliResponseBody, requestHeader,messageSource);

            IntegratedCancelResponseData integratedCancelResponseData = new IntegratedCancelResponseData();
            AliCancelResponseData aliCancelResponseData = new Gson().fromJson(aliResponseBody.getSecureData(),AliCancelResponseData.class);

            integratedCancelResponseData.setStoreOrderNo(storeOrderId);
            integratedCancelResponseData.setPlatformRsp(new Gson().toJson(aliCancelResponseData));

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedCancelResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);
        }

        return generalResponseData;
    }

    private static String yuantaPost(String url, String query) throws Exception {
        URL requestUrl = new URL(url);
        HttpURLConnection con = (HttpURLConnection) requestUrl.openConnection();
        con.setRequestMethod("POST");

        con.setRequestProperty("Content-length", String.valueOf(query.length()));
        con.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
        con.setDoOutput(true);
        con.setDoInput(true);

        DataOutputStream output = new DataOutputStream(con.getOutputStream());
        output.writeBytes(query);
        output.close();
        DataInputStream input = new DataInputStream( con.getInputStream() );

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(input));
        String result;

        StringBuilder buffer = new StringBuilder();
        while((result = bufferedReader.readLine()) != null) {
            buffer.append(result);
        }

        input.close();

        return buffer.toString();
    }

    private String getParameters(String requestJson, String requestType, SecretKey key) throws Exception {

        LOGGER.info("requestJson " +requestJson);
        // 256 bits AES key
        String apiToken = byte2Hex(key.getEncoded());
        SecretKey secretKey = AesCryptoUtil.convertSecretKey(apiToken.getBytes());

        // setting api key
        String encryptApiToken = getEncryptApiToken(apiToken);

        // SecureData
        byte[] byteSecureData = AesCryptoUtil.encrypt(secretKey, Constant.IV, requestJson);
        String aesEncryptJson = Base64.encode(byteSecureData);
        LOGGER.info("aesEncryptJson : " + Base64.encode(byteSecureData));

        // HashDigest
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        String digest = requestType + aesEncryptJson + encryptApiToken + BANK_KEY_VERSION;
        md.update(digest.getBytes());
        byte[] hashDigest = md.digest();

        return "Action=" + URLEncoder.encode(requestType, "UTF-8")
                + "&BankPKeyVersion=" + BANK_KEY_VERSION
                + "&SecureData=" + URLEncoder.encode(aesEncryptJson, "UTF-8")
                + "&APIToken=" + URLEncoder.encode(encryptApiToken, "UTF-8")
                + "&HashDigest=" + URLEncoder.encode(byte2Hex(hashDigest), "UTF-8");
    }

    private String getEncryptApiToken(String token) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        InputStreamReader inputStreamReader = new InputStreamReader( new FileInputStream(env.getProperty("yuanta.key.public.offline")));
        PublicKey publicKey = KeyReader.loadPublicKeyFromPEM(inputStreamReader);
        byte[] byteAesKey = RsaCryptoUtil.encrypt(token, publicKey);
        return Base64.encode(byteAesKey);
    }

    private String byte2Hex(byte[] inputBytes) {
        String result = "";
        for (byte currentByte : inputBytes) result += Integer.toString((currentByte & 0xff) + 0x100, 16).substring(1);
        return result;
    }

    private String getDecryptResponse(AliResponseBody aliResponseBody, SecretKey key) throws UnsupportedEncodingException {
        String secureResponseData = URLDecoder.decode(aliResponseBody.getSecureData(), "UTF-8");

        String apiToken = byte2Hex(key.getEncoded());
        SecretKey secretKey = AesCryptoUtil.convertSecretKey(apiToken.getBytes());
        try {
            return AesCryptoUtil.decrypt(secretKey, Constant.IV, Base64.decode(secureResponseData));
        } catch (Exception e) {
            LOGGER.info(e.toString());
            return getErrorResponse(aliResponseBody, "SYSTEM ERROR");
        }
    }

    private PrivateKey getPrivateKey() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("key/intella_rsa_private.der").getFile());

        if(PRIVATE_KEY == null) {
            PRIVATE_KEY =  KeyReader.loadPrivateKeyFromDER(file);
        }
        return PRIVATE_KEY;
    }

    private PrivateKey getPublicKey() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("key/intella_rsa_private.der").getFile());

        if(PRIVATE_KEY == null) {
            PRIVATE_KEY =  KeyReader.loadPrivateKeyFromDER(file);
        }
        return PRIVATE_KEY;
    }

    private boolean setPlatformMchId(RequestHeader requestHeader){
        long bankId =Long.valueOf(env.getProperty("bank.id."+requestHeader.getMethod()));
        PaymentAccount paymentAccount = paymentAccountService.getOne(requestHeader.getMerchantId(), 11);
        if (paymentAccount != null) {
            requestHeader.setMerchantId(paymentAccount.getAccount());
            return true;
        } else {
            return false;
        }
    }
}
