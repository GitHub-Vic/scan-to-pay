package co.intella.service.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import co.intella.domain.contratStore.TradeDetailVo;
import co.intella.model.ReserveCancel;
import co.intella.net.HttpRequestUtil;
import co.intella.service.ReserveCancelService;

/**
 * @author Miles
 */
@Service
@Transactional
public class ReserveCancelServiceImpl implements ReserveCancelService {

    private final Logger LOGGER = LoggerFactory.getLogger(ReserveCancelServiceImpl.class);

//    private static String HOST_DAO = "http://localhost:8080/scan2pay-data/";
    
    @Value("${host.dao}")
    private String HOST_DAO;

    public ReserveCancel save(ReserveCancel ReserveCancel) {
        try {

            String result = HttpRequestUtil.post(HOST_DAO + "api/reserveCancel/save", new ObjectMapper().writeValueAsString(ReserveCancel));
            return new Gson().fromJson(result, ReserveCancel.class);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public ReserveCancel getOne(String orderId) {
        try {
            String result = HttpRequestUtil.httpGet(HOST_DAO + "api/reserveCancel/get/" + orderId);
            return new Gson().fromJson(result, ReserveCancel.class);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public List<ReserveCancel> listByMerchantIdAndDeviceIdAndBatchNo(String merchantId, String deviceId, String batchNo) {
        try {
            Map<String, String> request = new HashMap<String, String>();
            request.put("type", "MDB");
            request.put("merchantId", merchantId);
            request.put("deviceId", deviceId);
            request.put("batchNo", batchNo);
            String result = HttpRequestUtil.post(HOST_DAO + "api/reserveCancel/list", new Gson().toJson(request));
            LOGGER.info("listByUserId : " + result);
            return new Gson().fromJson(result, new TypeToken<List<ReserveCancel>>(){}.getType());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public List<ReserveCancel> listGroupEZC(String terminalId, String deviceId, String batchNo) {
      try {
        String result = HttpRequestUtil.httpGet(HOST_DAO + "api/reserveCancel/listEzc/" +terminalId+ "/" + deviceId + "/" + batchNo);

        LOGGER.debug("listGroupEZC : " + result);
        return new Gson().fromJson(result, new TypeToken<List<ReserveCancel>>(){}.getType());
    } catch (Exception e) {
        LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
        return null;
    }
  }
}
