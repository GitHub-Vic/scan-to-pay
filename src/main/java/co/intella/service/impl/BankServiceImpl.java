package co.intella.service.impl;

import co.intella.model.Bank;
import co.intella.net.HttpRequestUtil;
import co.intella.service.BankService;
import co.intella.utility.HttpUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Alex
 */
@Service
@Transactional
public class BankServiceImpl implements BankService {

    @Value("${host.dao}")
    private String DAO_URL;

    public Bank getOne(long bankId) {
        Bank bank;
        try {
            String responseEntity = HttpRequestUtil.httpGet(DAO_URL + "api/bank/"+ bankId );
            bank = new Gson().fromJson(responseEntity, Bank.class);
        } catch (Exception e) {
            e.printStackTrace();
            bank = null;
        }
        return bank;
    }

    public List<Bank> listAll() {
        List<Bank> list;
        try {
            String responseEntity = HttpRequestUtil.httpGet(DAO_URL + "api/bank/list");
            list = new Gson().fromJson(responseEntity,  new TypeToken<List<Bank>>(){}.getType());
        } catch (Exception e) {
            e.printStackTrace();
            list = null;
        }
        return list;

    }

    public void delete(long bankId) {

    }

    public void save(Bank bank) {
        try {
            HttpUtil.post(DAO_URL + "api/bank/save" ,new ObjectMapper().writeValueAsString(bank));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
