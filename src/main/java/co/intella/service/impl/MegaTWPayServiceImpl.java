package co.intella.service.impl;

import co.intella.domain.integration.*;
import co.intella.domain.megatwpay.*;
import co.intella.exception.ServiceTypeNotSupportException;
import co.intella.model.*;
import co.intella.service.*;
import co.intella.utility.HttpUtil;
import co.intella.utility.MegaTWPayUtil;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Objects;

@Service
public class MegaTWPayServiceImpl implements MegaTWPayService {

    private final Logger LOGGER = LoggerFactory.getLogger(MegaTWPayServiceImpl.class);

    @Autowired
    private Environment env;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private MerchantService merchantService;

    @Autowired
    private MessageSource messageSource;

    @Value("${payment.gateway.api.origin}")
    private String PAYMENT_GATEWAY_API_ORIGIN;
//    private String PAYMENT_GATEWAY_API_ORIGIN = "http://34.80.101.124:30394";

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    private TradeDetailService tradeDetailService;

    @Resource
    private RefundDetailService refundDetailService;

    @Override
    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {

        LOGGER.info("[MegaTWPay][doRequest]");
        String generalResponseData = SystemInstance.EMPTY_STRING;
        JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        PaymentAccount paymentAccount = paymentAccountService.getOne(integrateMchId, bankId);
        Merchant merchant = merchantService.getOne(integrateMchId);
        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

        String result;
        String request;
        if (SystemInstance.TYPE_MICROPAY.equals(requestHeader.getServiceType())) {
            LOGGER.info("[MegaTWPay][doRequest][MICROPAY]");
            //mapping request
            TxnPaymentReq txnPaymentReq = MegaTWPayUtil.generatePaymentReq(tradeDetail, paymentAccount);
            request = new Gson().toJson(txnPaymentReq);
            LOGGER.info("request :" + request);
            //sendRequest
            result = sendRequest(PAYMENT_GATEWAY_API_ORIGIN + "/megatwpay/api/txnpayment", request);
            LOGGER.info("result :" + result);
            //get result and save DB
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);
            TxnPaymentRes txnPaymentRes = new Gson().fromJson(result, TxnPaymentRes.class);
            ResponseGeneralHeader responseGeneralHeader = MegaTWPayUtil
                    .getResponseHeader(txnPaymentRes, requestHeader, integrateMchId, messageSource);
            //mapping return
            IntegratedMicropayResponseData integratedMicropayResponseData = MegaTWPayUtil
                    .getIntegratedResponseData(txnPaymentRes, txnPaymentReq);
            integratedMicropayResponseData.setPlatformRsp(result);

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedMicropayResponseData);
            generalResponseData = new Gson().toJson(responseGeneralBody);
            LOGGER.info("integrate response ready :" + generalResponseData);
        } else if ("SingleOrderQuery".equals(requestHeader.getServiceType())) {
            LOGGER.info("[MegaTWPay][doRequest][SingleOrderQuery][data]" + data.getAsString());
            //mapping request
            SingleOrderReq singleOrderReq = MegaTWPayUtil
                    .generateSingleOrderReq(tradeDetail, paymentAccount);
            request = new Gson().toJson(singleOrderReq);
            LOGGER.info("request :" + request);
            //sendRequest
            result = sendRequest(PAYMENT_GATEWAY_API_ORIGIN + "/megatwpay/api/singleorder", request);
            LOGGER.info("result :" + result);
            //get result and save DB
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);
            SingleOrderRes singleOrderRes = new Gson().fromJson(result, SingleOrderRes.class);
            ResponseGeneralHeader responseGeneralHeader = MegaTWPayUtil
                    .getResponseHeader(singleOrderRes, requestHeader, integrateMchId, messageSource);
            //mapping return
            IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = MegaTWPayUtil
                    .getIntegratedResponseData(singleOrderRes, tradeDetailService.getOne(tradeDetail.getOrderId()), paymentAccount);
            RefundDetail refundDetail = refundDetailService.getOneByOrderIdAndMethod(tradeDetail.getOrderId(), tradeDetail.getMethod());
            if (Objects.nonNull(refundDetail)) {
                integratedSingleOrderQueryResponseData.setRefundedAt(refundDetail.getCreateTime());
            }
            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedSingleOrderQueryResponseData);
            generalResponseData = new Gson().toJson(responseGeneralBody);
            LOGGER.info("integrate response ready :" + generalResponseData);
        } else if ("Refund".equals(requestHeader.getServiceType())) {
            LOGGER.info("[MegaTWPay][doRequest][Refund][data]" + data.getAsString());
            String refundOrderAmount = requestData.get("RefundFee").getAsString();

            if (!Long.toString(tradeDetail.getPayment()).equals(refundOrderAmount)) {
                throw new ServiceTypeNotSupportException("we don't provide partial refund");
            }
            //mapping request
            TxnCancelReq txnCancelReq = MegaTWPayUtil.generateTxnCancelReq(tradeDetail, paymentAccount);
            request = new Gson().toJson(txnCancelReq);
            LOGGER.info("request :" + request);
            //sendRequest
            result = sendRequest(PAYMENT_GATEWAY_API_ORIGIN + "/megatwpay/api/cancel", request);
            LOGGER.info("result :" + result);
            //get result and save DB
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);
            TxnCancelRes txnCancelRes = new Gson().fromJson(result, TxnCancelRes.class);
            ResponseGeneralHeader responseGeneralHeader = MegaTWPayUtil
                    .getResponseHeader(txnCancelRes, requestHeader, integrateMchId, messageSource);
            //mapping return
            IntegratedRefundResponseData integratedRefundResponseData = MegaTWPayUtil
                    .getIntegratedResponseData(tradeDetail, requestData.get("StoreRefundNo").getAsString());
            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedRefundResponseData);
            generalResponseData = new Gson().toJson(responseGeneralBody);
            LOGGER.info("integrate response ready :" + generalResponseData);
        }

        return generalResponseData;
    }

    @Override
    public String sendRequest(String url, String request) throws Exception {
        LOGGER.info("[MegaTWPay][sendRequest][request]" + request);

//        return restTemplate.postForObject(url, request, String.class);

        return HttpUtil.doPostJson(url, request);
    }

}
