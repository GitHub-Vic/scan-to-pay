package co.intella.service.impl;

import co.intella.domain.contratStore.CustomizePageRequest;
import co.intella.model.CaptureDetail;
import co.intella.model.CaptureOrderDetail;
import co.intella.net.HttpRequestUtil;
import co.intella.service.CaptureDetailService;
import co.intella.service.CaptureOrderDetailService;
import co.intella.utility.HttpUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.List;

/**
 * @author Andy Lin
 */
@Service
@Transactional
public class CaptureDetailServiceImpl implements CaptureDetailService{

    private final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(CaptureDetailServiceImpl.class);

    @Resource
    private CaptureOrderDetailService captureOrderDetailService;

    @Value("${host.dao}")
    private String DAO_URL;

    public CaptureDetail settle(String merchantId, String method, String from, String to) {

        List<CaptureOrderDetail> captureOrderDetails = captureOrderDetailService.listUnSettleCaptureOrder(merchantId, from, to, method);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0); //anything 0 - 23
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        String monthString = (calendar.get(Calendar.MONTH) + 1)<10?"0" + String.valueOf(calendar.get(Calendar.MONTH) + 1):String.valueOf(calendar.get(Calendar.MONTH) + 1);
        String captureDate = String.valueOf(calendar.get(Calendar.YEAR))
                + monthString
                + String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));

        CaptureDetail captureDetail = new CaptureDetail();
        captureDetail.setCaptureDate(captureDate);
        captureDetail.setMerchantId(merchantId);
        captureDetail.setStatus("wait");
        captureDetail.setFeeRate(getFeeRate(method));
        captureDetail.setMethod(method);
        captureDetail.setSequentialNo("0");

        CaptureDetail entity =null;

        try {
            String response= HttpUtil.post(DAO_URL + "api/capture/save" ,new ObjectMapper().writeValueAsString(captureDetail));
            entity = new Gson().fromJson(response,CaptureDetail.class);
        } catch (Exception e) {
            e.printStackTrace();
        }


        long sum = 0;
        if(!captureOrderDetails.isEmpty()) {
            for (CaptureOrderDetail captureOrderDetail : captureOrderDetails) {
                captureOrderDetail.setStatus("done");
                captureOrderDetail.setCaptureDetail(entity);
                captureOrderDetail.setCaptureDate(captureDate);
                sum = sum + captureOrderDetail.getTotalFee();
            }
        }

        entity.setTotalCount(sum);
        //entity = captureDetailRepository.save(captureDetail);

        try {
            String response= HttpUtil.post(DAO_URL + "api/capture/save" ,new ObjectMapper().writeValueAsString(captureDetail));
            entity = new Gson().fromJson(response,CaptureDetail.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<CaptureOrderDetail> updateCaptureOrderDetails = captureOrderDetailService.save(captureOrderDetails);

        return entity;
    }

    public List<CaptureDetail> save(List<CaptureDetail> entities) {
        try {
            String response= HttpUtil.post(DAO_URL + "api/capture/saveList" ,new ObjectMapper().writeValueAsString(entities));
            entities = new Gson().fromJson(response, new TypeToken<List<CaptureDetail>>(){}.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entities;
//        return captureDetailRepository.save(entities);
    }

    public CaptureDetail save(CaptureDetail captureDetail) {

        CaptureDetail ret =null ;
        try {
            String response= HttpUtil.post(DAO_URL + "api/capture/save" ,new ObjectMapper().writeValueAsString(captureDetail));
            ret = new Gson().fromJson(response, CaptureDetail.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ret;

//        return captureDetailRepository.save(captureDetail);
    }

    public CaptureDetail getOne(String id) {

        CaptureDetail captureDetail = null;
        try {
            String response = HttpRequestUtil.httpGet(DAO_URL + "api/capture/"+id);
            captureDetail = new Gson().fromJson(response,CaptureDetail.class);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("[GetOne][Failed]");
        }
        return captureDetail;
//        QCaptureDetail qCaptureDetail = QCaptureDetail.captureDetail;
//        BooleanExpression predicate = qCaptureDetail.captureDetailRandomId.eq(UUID.fromString(id));
//        return captureDetailRepository.findOne(predicate);
    }

    public CaptureDetail getOne(String mchId, String fromDate, String method) {
        CaptureDetail captureDetail = null;
        try {
            String response = HttpRequestUtil.httpGet(DAO_URL + "api/capture/"+mchId+"/"+fromDate+"/"+method);
            captureDetail = new Gson().fromJson(response,CaptureDetail.class);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("[GetOne][Failed]");
        }
        return captureDetail;
//        QCaptureDetail qCaptureDetail = QCaptureDetail.captureDetail;
//        BooleanExpression predicate = qCaptureDetail.merchantId.eq(mchId)
//                .and(qCaptureDetail.captureDate.eq(fromDate))
//                .and(qCaptureDetail.method.eq(method));
//        return captureDetailRepository.findOne(predicate);
    }

//    public List<CaptureDetail> listByDate(String mchId, String startDate, String endDate) {

//        QCaptureDetail qCaptureDetail = QCaptureDetail.captureDetail;
//        BooleanExpression predicate = qCaptureDetail.merchantId.eq(mchId).and(qCaptureDetail.captureDate.between(startDate,endDate));
//
//        return Lists.newArrayList(captureDetailRepository.findAll(predicate));
//    }

    public List<CaptureDetail> listByMerchantIdAndDate(String mchId, String date) {

        List<CaptureDetail> list = null;
        try {
            String response = HttpRequestUtil.httpGet(DAO_URL + "api/capture/listByMerchantIdAndDate/"+mchId+"/"+date);
            list = new Gson().fromJson(response,new TypeToken<List<CaptureDetail>>(){}.getType());
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("[listByMerchantIdAndDate][Failed]");
        }
        return list;
//        QCaptureDetail qCaptureDetail = QCaptureDetail.captureDetail;
//        BooleanExpression predicate = qCaptureDetail.merchantId.eq(mchId)
//                .and(qCaptureDetail.captureDate.eq(date));
//
//        return  Lists.newArrayList( captureDetailRepository.findAll(predicate));
    }

    public List<CaptureDetail> listByStatus(String status) {
        List<CaptureDetail> list = null;
        try {
            String response = HttpRequestUtil.httpGet(DAO_URL + "api/capture/listByStatus/"+status);
            list = new Gson().fromJson(response,new TypeToken<List<CaptureDetail>>(){}.getType());
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("[listByMerchantIdAndDate][Failed]");
        }
        return list;
//        QCaptureDetail qCaptureDetail = QCaptureDetail.captureDetail;
//        BooleanExpression predicate = qCaptureDetail.status.eq(status);
//
//        return Lists.newArrayList( captureDetailRepository.findAll(predicate));
    }

    public List<CaptureDetail> listByDate(String date) {
        List<CaptureDetail> list = null;
        try {
            String response = HttpRequestUtil.httpGet(DAO_URL + "api/capture/listByDate/"+date);
            list = new Gson().fromJson(response,new TypeToken<List<CaptureDetail>>(){}.getType());
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("[listByMerchantIdAndDate][Failed]");
        }
        return list;

//        QCaptureDetail qCaptureDetail = QCaptureDetail.captureDetail;
//        BooleanExpression predicate = qCaptureDetail.captureDate.eq(date);
//
//        return Lists.newArrayList( captureDetailRepository.findAll(predicate));
    }

    public List<CaptureDetail> listAll() {
        List<CaptureDetail> list = null;
        try {
            String response = HttpRequestUtil.httpGet(DAO_URL + "api/capture/listAll");
            list = new Gson().fromJson(response,new TypeToken<List<CaptureDetail>>(){}.getType());
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("[listByMerchantIdAndDate][Failed]");
        }
        return list;
//        return captureDetailRepository.findAll();
    }

    public List<CaptureDetail> pageList(CustomizePageRequest request) {

        List<CaptureDetail> list = null;
        try {
            String response = HttpUtil.post(DAO_URL+"api/capture/pageList" , new Gson().toJson(request));
            list = new Gson().fromJson(response,new TypeToken<List<CaptureDetail>>(){}.getType());
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("[listByMerchantIdAndDate][Failed]");
        }
        return list;

//        Page<CaptureDetail> pageableList = getPageList(request);
//        return pageableList.getContent();
    }

    public List<CaptureDetail> list(CustomizePageRequest request) {
        List<CaptureDetail> list = null;
        try {
            String response = HttpUtil.post(DAO_URL+"api/capture/list" , new Gson().toJson(request));
            list = new Gson().fromJson(response,new TypeToken<List<CaptureDetail>>(){}.getType());
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("[listByMerchantIdAndDate][Failed]");
        }
        return list;


//        QCaptureDetail qCaptureDetail = QCaptureDetail.captureDetail;
//        BooleanExpression predicate = qCaptureDetail.merchantId.eq(merchantId)
//                .and(qCaptureDetail.captureDate.between(from, to));
//
//        return Lists.newArrayList(captureDetailRepository.findAll(predicate));
    }

    public Long pageCount(CustomizePageRequest request) {
        Long count = null;
        try {
            String responseEntity = HttpUtil.post(DAO_URL+"api/capture/pageCount" , new Gson().toJson(request));
            count = Long.parseLong(responseEntity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(count == null) {
            return 0L;
        }
        return count;
//        Page<CaptureDetail> pageableList = getPageList(request);
//        return pageableList.getTotalElements();
    }

//    private Page<CaptureDetail> getPageList(CustomizePageRequest request) {
//        PageRequest pageRequest = new PageRequest(request.getIndex(), request.getPageSize(), Sort.Direction.DESC, "tradeDate");
//        QCaptureDetail qCaptureDetail = QCaptureDetail.captureDetail;
//
//        BooleanExpression predicate = qCaptureDetail.merchantId.eq(request.getAccountId());
//        return captureDetailRepository.findAll(predicate, pageRequest);
//    }

    private double getFeeRate(String method) {

        if("10110".equals(method)) {
            return 0.19;
        }

        if("10500".equals(method)) {
            return 0.21;
        }

        if("20400".equals(method)) {
            return 0.25;
        }

        return 0;
    }
}
