package co.intella.service.impl;

import co.intella.model.Merchant;
import co.intella.model.MultiQrcode;
import co.intella.model.PayerInfo;
import co.intella.model.QrcodeParameter;
import co.intella.repository.MultiQrcodeRespository;
import co.intella.repository.PayerInfoRepository;
import co.intella.service.*;
import co.intella.utility.GeneralUtil;
import co.intella.utility.SmsUtil;
import co.intella.utility.SystemInstance;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class MultiQrcodeServiceImpl implements MultiQrcodeService {
	
    @Value("${api.url}")
    private String API_URL;

    private final Logger LOGGER = LoggerFactory.getLogger(MultiQrcodeServiceImpl.class);
    
    @Resource
    SmsUtil smsUtil;
    
	@Resource
	private MultiQrcodeRespository multiQRCodeRespository;
	
    @Resource
    private PayerInfoRepository payerInfoRepository;
	
    @Resource
    private QrcodeService qrcodeService;
    
    @Resource
    private MerchantService merchantService;
    
    @Resource
    private MailService mailService;

    private SecureRandom random = new SecureRandom();
    
    String regex1 = "[0-9]{4}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])(2[0-3]|[01][0-9])[0-5][0-9][0-5][0-9]";
    String regex2 = "[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]";
    String mergeQrcodeUrl = "https://api.pay.stage.intella.co/payment-ui/#/";
    String[] randmToken = {
    		                   "0","1","2","3","4","5","6","7","8","9",
    		                   "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
    		                   "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"
    		               };
    
	private static DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
	private static DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	
	@Override
	public List<MultiQrcode> findMutiQRCodeByPayer(String shortId, String email, String payerName, String mobileNumber) throws Exception {
		List<MultiQrcode> list = multiQRCodeRespository.findMutiQRCodeByPayer(shortId, email, payerName, mobileNumber);
		return getResultMultiQrcodes(list);
	}
    
	@Override
	public List<MultiQrcode> findMutiQRCodeByPayer(String shortId, String email, String payerName, String mobileNumber, String payerId, String token, HttpServletResponse response) throws Exception {
		checkoutToken(payerId, token);
		List<MultiQrcode> list = multiQRCodeRespository.findMutiQRCodeByPayer(shortId, email, payerName, mobileNumber, payerId);
		addCookie(payerId, token, response);
		return getResultMultiQrcodes(list);
	}
	
	@Override
	public List<MultiQrcode> findMutiQRCode(String shortId) throws Exception {
		List<MultiQrcode> list = multiQRCodeRespository.findMutiQRCode(shortId);
		return getResultMultiQrcodes(list);
	}
    
	@Override
	public Map<String, Object> findMutiQRCode(String shortId, String payerId, String token, HttpServletResponse response) throws Exception {	
		PayerInfo payerInfo = checkoutToken(payerId, token);		
		List<MultiQrcode> list = multiQRCodeRespository.findMutiQRCode(shortId, payerId);
		addCookie(payerId, token, response);
		getResultMultiQrcodes(list);
		return getResultMap(list, payerInfo, shortId);
	}
	
	private List<MultiQrcode> getResultMultiQrcodes(List<MultiQrcode> list) {
		Long now = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
		List<MultiQrcode> result = list.stream().map(multiQrcode -> {
			if(null != multiQrcode.getMergeShortId()) {
			    QrcodeParameter qp = qrcodeService.getOneByShortId(multiQrcode.getMergeShortId());
			    if (null != qp) {
		            Long expire = null;
				    if(qp.getTimeExpire().matches(regex1)) {
				        expire = LocalDateTime.parse(qp.getTimeExpire(), dtf1).toInstant(ZoneOffset.of("+8")).toEpochMilli();
			        } else if (qp.getTimeExpire().matches(regex2)) {
				        expire = LocalDateTime.parse(qp.getTimeExpire(), dtf2).toInstant(ZoneOffset.of("+8")).toEpochMilli();
				    }
			        if(null != expire && now > expire) {
				        multiQRCodeRespository.updateCleanOriginQrcodeMergeShortId(multiQrcode.getMergeShortId());
				        multiQrcode.setMergeShortId(null);
				    }
			    }
			}
			return multiQrcode;
		}).collect(Collectors.toList());
		return result;
	}
	
	private Map<String, Object> getResultMap(List<MultiQrcode> list, PayerInfo payerInfo, String shortId) {
		Map<String, Object> result = new HashMap<String, Object>();
		Merchant merchant = merchantService.getOne(payerInfo.getAccountId());
		String payerName = payerInfo.getPayerName();
		String email = payerInfo.getEmail();
		String mobileNumber = payerInfo.getMobileNumber();
		result.put("payerName", payerName.length() > 3 ? payerName.substring(0, payerName.length() - 2) + "**" : payerName.substring(0, payerName.length() - 1) + "*");
		result.put("merchantName", merchant.getName());
		if(StringUtils.isNotBlank(email)) {
			String eAccount = email.replaceAll("([\\w-.]+)@(.*)", "$1");
			String eAdress = email.replaceAll("([\\w-.]+)@(.*)", "$2");
			result.put("email", (eAccount.length() > 3 ? eAccount.substring(0, eAccount.length() - 3) + "***@" : eAccount.substring(0, eAccount.length() - 1) + "*@") + eAdress);
		}
		if(StringUtils.isNotBlank(mobileNumber)) {
			String preMobileNumber = mobileNumber.replaceAll("(\\d{4})(\\d*)", "$1");
			String lastMobileNumber = mobileNumber.replaceAll("(\\d{4})(\\d*)", "$2");
			result.put("mobileNumber", preMobileNumber + "-***" + lastMobileNumber.substring(2));
		}
		result.put("data", list);
		return result;
	}
	
	@Override
	public String mergeQrcode(String shortIdsData, String payerId, String token, HttpServletResponse httpServletResponse) throws Exception {
		Set<String> validates = validate(shortIdsData, payerId, token);
		List<String> shortIds = new Gson().fromJson(shortIdsData, new TypeToken<List<String>>() {}.getType());
		String accountId = getAccountId(payerId, token);
		QrcodeParameter qrcodeParameter = null;
		int shortIdCounts = 0;
        if (validates.isEmpty()) {
        	String totalFee = getTotalFee(shortIds, validates);
            qrcodeParameter = createMergeQrcode(totalFee, accountId);
            
            if(qrcodeParameter != null) {
            	updateMergedQrcode(shortIds, qrcodeParameter.getShortId(), validates);
            	shortIdCounts = shortIds.size();
            }
        }
        
        Map<String, String> responseHeader = getResponseHeader(shortIdCounts, qrcodeParameter, validates);
        Map<String, String> responseData = getResponseData(responseHeader, qrcodeParameter);
        Map<String, Object> response = new HashMap<String, Object>();
        response.put("Header", responseHeader);
        response.put("Data", responseData);
        
        disableOldQrcode(responseHeader, shortIds);
        addCookie(payerId, token, httpServletResponse);
		return new Gson().toJson(response);
	}
	
	private Set<String> validate(String shortIdsData, String payerId, String token) throws Exception {
		Set<String> validateList = new HashSet<String>();	
		PayerInfo payerInfo = null;
		if(StringUtils.isBlank(payerId) || StringUtils.isBlank(token)) {
			validateList.add("payerId & token is null");
		} else {
			try {
			    payerInfo = payerInfoRepository.findByToken(payerId, token);
			} catch (Exception e) {
				validateList.add("comfirm fail or token timeExpired");
				throw new Exception("comfirm fail or token timeExpired");
			}
		}
		String accountId = payerInfo.getAccountId();
		if (StringUtils.isBlank(accountId)) {
			validateList.add("need qrcode accountId");
        }
		Merchant merchant = merchantService.getOne(accountId);
		if (null == merchant) {
			validateList.add("merchant not found");
		}
		if (StringUtils.isBlank(shortIdsData)) {
			validateList.add("need shortIdsData and at least 2 qrcodes");
        } else if (null != payerInfo) { 
        	try {
        	    List<String> shortIds = new Gson().fromJson(shortIdsData, new TypeToken<List<String>>() {}.getType());
        	    if(shortIds.size() < 2) {
        	        validateList.add("at least 2 qrcodes");
        	    }
        	    String payerIdchecked = "";
        	    for(String shortId:shortIds) {
        	    	 payerIdchecked = multiQRCodeRespository.checkPayerId(shortId);
        	    	 if (!payerId.equals(payerIdchecked)) {
        	    		 validateList.add("is not correct");
        	    	 }
        	    	 try {
        	    		 String orderchecked = multiQRCodeRespository.checkSelfOrderFinished(shortId);
        	    	     if("1".equals(orderchecked)) {
        	    	    	 multiQRCodeRespository.updateQrcode(shortId);
        	    		     validateList.add("contain paid qrcode");
        	    		     break;
        	    	     }
        	    	 } catch (Exception se) {
        	    	 }
        	    	 try {
        	    	     String orderchecked = multiQRCodeRespository.checkOrderFinished(shortId);
        	    	     if("1".equals(orderchecked)) {
        	    	    	 multiQRCodeRespository.updateQrcode(shortId);
        	    		     validateList.add("contain paid qrcode");
        	    		     break;
        	    	     }
        	    	 } catch (Exception me) {
        	    	 }
        	    }
        	} catch (Exception e) {
        		 validateList.add("shortId array format fail");
        	}
        }
		LOGGER.info(", accountId = " + accountId +  ", shortIdsData = " + shortIdsData);
		return validateList;
	}
	
	private String getTotalFee(List<String> shortIds, Set<String> validates) {
		BigDecimal totalFee = new BigDecimal(0);
		for(String shortId : shortIds) {
			try {
				totalFee = totalFee.add(multiQRCodeRespository.queryQrcodeTotalFee(shortId));
			} catch (Exception e) {
				LOGGER.debug("query qrcode totalFee fail");
				validates.add("query qrcode totalFee fail");
			}
		}
		LOGGER.info("totalFee = "  + totalFee.toPlainString());
		return totalFee.toPlainString();
	}
	
	private QrcodeParameter createMergeQrcode(String totalFee, String accountId) {
		QrcodeParameter qrcodeParameter = new QrcodeParameter();

        qrcodeParameter.setVer("101");
        qrcodeParameter.setMchId(accountId);
        
        qrcodeParameter.setTimeExpire(null);
        qrcodeParameter.setCodeType("1");
        qrcodeParameter.setTimeExpire(DateTime.now().plusMinutes(5).toString(SystemInstance.DATE_PATTERN));
        String ramdom = String.format("%04d", new Random().nextInt(9999));
        qrcodeParameter.setStoreOrderNo("MQR" + DateTime.now().plusMinutes(5).toString(SystemInstance.DATE_PATTERN) + ramdom);
        String bodyRandom = "合併訂單-" + ramdom;
        qrcodeParameter.setBody(bodyRandom);
        
        String alias = new BigInteger(55, random).toString(32);
        while (qrcodeService.getOneByShortId(alias) != null) {
            alias = new BigInteger(55, random).toString(32);
        }
        qrcodeParameter.setTotalFee(Double.parseDouble(totalFee));
        qrcodeParameter.setStatus(1);
        qrcodeParameter.setShortId(alias);
        qrcodeParameter.setDeviceInfo("skb0001");
        qrcodeParameter.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        qrcodeService.save(qrcodeParameter);
        LOGGER.debug("create qrcode !!!");
        return qrcodeParameter;
	}
	
	private void updateMergedQrcode(List<String> shortIds, String mergeShortId, Set<String> validates) {
		Set<String> disableShortIds = new HashSet<String>();
		String oldMergeShortId = "";
		for (String shortId : shortIds) {
			try {
				// first query payerQrcode mergeShortId and hold on them
				oldMergeShortId = multiQRCodeRespository.queryMergeShortId(shortId);
				disableShortIds.add(oldMergeShortId);
				// second update new mergeShortId
				multiQRCodeRespository.updateOriginQrcode(shortId, mergeShortId);
			} catch (Exception e) {
				// if updatenum = 0, its mean new Qrcode create fail
				// and disable new Qrcode
				multiQRCodeRespository.updateQrcode(mergeShortId);
				// and clean up payerQrcode mergeShortId = new mergeShortId
				multiQRCodeRespository.updateCleanOriginQrcodeMergeShortId(mergeShortId);
				LOGGER.debug("update qrcode fail - " +  e.getMessage() + " - " + e.getStackTrace());
				validates.add("update qrcode fail");
				break;
			}
		}
		for(String disableShortId : disableShortIds) {
			//useless Qrcode set timeExpire and status = 0
		    multiQRCodeRespository.updateQrcode(disableShortId);
		    //then clean up payerQrcode mergeShortId = old mergeShortId
		    multiQRCodeRespository.updateCleanOriginQrcodeMergeShortId(disableShortId);
		}
		LOGGER.debug("disable old qrcode and update qrcode !!!");
	}
	
	private Map<String, String> getResponseHeader(int totalcnt, QrcodeParameter qrcodeParameter, Set<String> valiudates) {
		Map<String, String> responseHeader = new HashMap<String, String>();
		
		if(qrcodeParameter != null && valiudates.isEmpty()) {
			responseHeader.put("msgCode", "0000");
		    responseHeader.put("msg", "");
		    responseHeader.put("totalcnt", String.valueOf(totalcnt));
		} else {
			responseHeader.put("msgCode", "9998");
		    responseHeader.put("msg", valiudates.isEmpty() ? "create Qrcode fail" : valiudates.stream().collect(Collectors.joining(",")));
		    responseHeader.put("totalcnt", String.valueOf(totalcnt));
		}

		return responseHeader;
	}
	
	private Map<String, String> getResponseData(Map<String, String> responseHeader, QrcodeParameter qrcodeParameter) {
		Map<String, String> responseData = new HashMap<String, String>();
		
		if("0000".contentEquals(responseHeader.get("msgCode"))) {
			responseData.put("urlToken", API_URL + qrcodeParameter.getShortId());
		} else {
			responseData.put("urlToken", "");
		}

		return responseData;
	}
	
	private void disableOldQrcode (Map<String, String> responseHeader, List<String> shortIds) {
		if("0000".equals(responseHeader.get("msgCode"))) {
//			shortIds.forEach(shortId -> multiQRCodeRespository.updateQrcode(shortId));
		}
	}
	
	public String checkoutCookie(HttpServletRequest req) throws Exception {
		String payerId = "";
        Cookie[] allcookie = req.getCookies();
		int i = 0;
		if (allcookie != null) {
		    for (i = 0; i < allcookie.length; i++) {
		        Cookie temp = allcookie[i];
		        if (temp.getName().equals("PayerKey")) {
		        	payerId = temp.getValue();
		            break;
		        }
		    }
		    if (allcookie.length == i) {
		    	payerId = "";
		    	LOGGER.info("cookie are expired");
		    	throw new Exception("cookie are expired");
		    }
		} else {
			payerId = "";
			LOGGER.info("cookie are expired or exist");
			throw new Exception("cookie are expired");
		}
        return payerId;
	}
	
	public PayerInfo checkoutToken(String payerId, String token) throws Exception {
		 PayerInfo payerInfo = null;
		try {
			if(StringUtils.isBlank(payerId) || StringUtils.isBlank(token)) {
				throw new Exception("payerId or token is null");
			}
		    payerInfo = payerInfoRepository.findByToken(payerId, token);
		    if (null == payerInfo) {
		    	throw new Exception("not find payerInfo or token timeExpired");
		    }
		} catch (Exception e) {
			throw new Exception("comfirm fail or token timeExpired " + e.getMessage() + " - " + e.getStackTrace());
		}
		return payerInfo;
	}

	@Override
	public String sendCookie(Map<String, String> request, HttpServletResponse response) throws Exception {
		String email = request.get("email");
		String mobileNumber = request.get("mobileNumber");
		String shortId = request.get("shortId");
		
		LOGGER.info("[SEND_COOKIE][MUTIQRCODE] starting... email = " + email + ", mobileNumber = " + mobileNumber + ", shortId = " + shortId);		

		PayerInfo payerInfo = payerInfoRepository.findPayerInfo(email, mobileNumber, shortId);
		Cookie cookie = new Cookie("PayerKey", payerInfo.getPayerId());
		cookie.setHttpOnly(true);
		cookie.setMaxAge(1800);
		cookie.setPath("/allpaypass/");
		response.addCookie(cookie);
		String result = new Gson().toJson(payerInfo);
		//mailService.send(GeneralUtil.getMailInformation(mailToStaff, emailTitle), GeneralUtil.getMailContext(input), "emergencyMail");
		return result;
	}
	
	public void sendToken(Map<String, String> request) throws Exception {
		String email = request.get("email");
		String mobileNumber = request.get("mobileNumber");
		String shortId = request.get("shortId");
		StringBuffer sb = new StringBuffer();
		LOGGER.info("[SEND_COOKIE][MUTIQRCODE] starting... email = " + email + ", mobileNumber = " + mobileNumber + ", shortId = " + shortId);	
		PayerInfo payerInfo = payerInfoRepository.findPayerInfo(email, mobileNumber, shortId);
		if(null != payerInfo) {
			int randomIndex = 0;
			while(sb.length() < 20) {
				randomIndex = (int) (Math.random()*62);
				sb.append(randmToken[randomIndex]);
			}
			payerInfoRepository.updateToken(payerInfo.getPayerId(), sb.toString());
			if(null != email || null != payerInfo.getEmail()) {
			    mailService.send(GeneralUtil.getMailInformation(new String[] {payerInfo.getEmail()}, "intella mutiQrcode confirmation"), GeneralUtil.getMailContext(mergeQrcodeUrl + "?payerId=" + payerInfo.getPayerId() + "&token=" + sb.toString()), "emergencyMail");
			}
			if(null != mobileNumber || null != payerInfo.getMobileNumber()) {
				smsUtil.sendSms(payerInfo.getMobileNumber(), mergeQrcodeUrl + "?payerId=" + payerInfo.getPayerId() + "&token=" + sb.toString());
			}
		}
	}
	
	private void addCookie(String payerId, String token, HttpServletResponse response) throws Exception {
		Cookie cookie = new Cookie("PayerKey", payerId + "#" + token);
		cookie.setHttpOnly(true);
		cookie.setSecure(true);
		cookie.setMaxAge(1800);
		response.addCookie(cookie);
	}
	
	private String getAccountId(String payerId, String token) {
		PayerInfo payerInfo = payerInfoRepository.findByToken(payerId, token);
		return payerInfo.getAccountId();
	}

}