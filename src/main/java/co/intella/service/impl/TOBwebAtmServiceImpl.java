package co.intella.service.impl;

import co.intella.domain.integration.IntegratedSingleOrderQueryResponseData;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.domain.tobWebAtm.*;
import co.intella.exception.OrderNotFoundException;
import co.intella.exception.OtherAPIException;
import co.intella.exception.ServiceTypeNotSupportException;
import co.intella.model.*;
import co.intella.service.*;
import co.intella.utility.HttpUtilBig5;
import co.intella.utility.SystemInstance;
import co.intella.utility.TOBwebAtmUtil;
import co.intella.utility.XmlUtil;
import com.google.gson.Gson;
import com.google.gson.JsonPrimitive;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
public class TOBwebAtmServiceImpl implements TOBwebAtmService {
    private final Logger LOGGER = LoggerFactory.getLogger(TOBwebAtmServiceImpl.class);
    @Autowired
    private Environment env;
    @Resource
    private TradeProcedureService tradeProcedureService;
    @Resource
    private DeviceService deviceService;
    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private OrderService orderService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Value("${api.url}")
    private String API_URL;


    @Value("${host.request.TOBwebATM}")
    private String TOBwebATM_URL;

    @Value("${host.request.QueryTOBwebATM}")
    private String TOBwebATM_QueryURL;

    @Autowired
    private MessageSource messageSource;

    @Resource
    private AppNotificationService appNotificationService;

    @Override
    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {
        String generalResponseData = SystemInstance.EMPTY_STRING;

        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));

        PaymentAccount paymentAccount = paymentAccountService.getOneByDao(integrateMchId, bankId);
        if (Objects.isNull(paymentAccount)) {
            throw new ServiceTypeNotSupportException("Exception: The store's account isn't opened.");
        }

        String serviceType = requestHeader.getServiceType();
        switch (serviceType) {

            case "SingleOrderQuery":
                TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
                generalResponseData = this.queryMerchantOrder(tradeDetail, paymentAccount);
                break;
            default:
                LOGGER.error("[TOBwebATM][SingleOrderQuery]Service type not support. " + serviceType);
                generalResponseData = "Service type not support.";
        }

        return generalResponseData;
    }

    public String schedulingQueryMerchantOrder(String orderId) throws Exception {

        TradeDetail tradeDetail = tradeDetailService.getOne(orderId);
        PaymentAccount paymentAccount = paymentAccountService.getOneByDao(tradeDetail.getAccountId(), 57);
        String generalResponseData = this.queryMerchantOrder(tradeDetail, paymentAccount);
        LOGGER.debug("[RESPONSE][schedulingQueryMerchantOrder]" + generalResponseData);
        return generalResponseData;
    }

    private String queryMerchantOrder(TradeDetail tradeDetail, PaymentAccount paymentAccount) throws Exception {

        String orderId = tradeDetail.getOrderId();
        String account = paymentAccount.getAccount();
        TOBwebAtmUtil toBwebAtmUtil = new TOBwebAtmUtil();
        String enkey = toBwebAtmUtil.queryRequestEnMethod(orderId, account, paymentAccount.getHashKey());
        TOBwebAtmQueryRequest toBwebAtmQueryRequest = new TOBwebAtmQueryRequest(orderId, account, enkey);

        String reqData = toBwebAtmQueryRequest.toString();
        LOGGER.info("[REQUEST][TOBwebATM][queryMerchantOrder] requestData = " + reqData);
        byte[] encodedBytes = Base64.getEncoder().encode(reqData.getBytes());
        String encodedStr = new String(encodedBytes);

        Map<String, String> map = new HashMap<>();
        map.put("CardPayInqRq", encodedStr);
        String resData = HttpUtilBig5.doPost(TOBwebATM_QueryURL, map);
        LOGGER.info("[TOBwebATM][RESPONSE][queryMerchantOrder] responseData = " + resData);

        TOBwebATMQueryResponse tOBwebATMQueryResponse = XmlUtil.unmarshall(resData, TOBwebATMQueryResponse.class);

        LOGGER.info("[TobWebATM][RESPONSE][queryMerchantOrder][toString]:" + tOBwebATMQueryResponse.toString());

        String enMethod = new TOBwebAtmUtil().queryResponseEnMethod(tOBwebATMQueryResponse, paymentAccount.getHashKey());

        tradeDetail.setSystemOrderId(tOBwebATMQueryResponse.getoNO());

        //驗簽
        //訂單編號
        //金額
        //交易狀態回傳"0"成功，其餘都是失敗
        if (enMethod.equals(tOBwebATMQueryResponse.getmAC()) &&
                tOBwebATMQueryResponse.getoNO().equals(tradeDetail.getOrderId()) &&
                tOBwebATMQueryResponse.getCurAmt().equals(String.valueOf(tradeDetail.getPayment())) &&
                tOBwebATMQueryResponse.getrC().equals("0")) {
            tradeDetail.setTxParams(resData);
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);

            tradeDetailService.save(tradeDetail);
            LOGGER.info("[TobWebATM][RESPONSE][queryMerchantOrder] save tradedetail is success.");

            appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));

        } else {
            if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            }
            tradeDetailService.save(tradeDetail);
            LOGGER.error("[TobWebATM][RESPONSE][queryMerchantOrder] notify save tradeDetail fail.");

        }

        return convertSingleQueryResponse(tOBwebATMQueryResponse, tradeDetail);
    }

    private String convertSingleQueryResponse(TOBwebATMQueryResponse tOBwebATMQueryResponse, TradeDetail tradeDetail) {
        ResponseGeneralHeader responseHeader = new TOBwebAtmUtil().getTobWebATMQueryResponseHeader(tOBwebATMQueryResponse, messageSource);

        responseHeader.setMethod("15700");
        responseHeader.setServiceType("SingleOrderQuery");
        responseHeader.setMchId(tradeDetail.getAccountId());
        responseHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        IntegratedSingleOrderQueryResponseData integratedResponseData = new IntegratedSingleOrderQueryResponseData();

        if (SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus()) && SystemInstance.TRADE_REFUND_SUCCESS.equals(tradeDetail.getRefundStatus())) {
            integratedResponseData.setOrderStatus("3");
        } else if (SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
            integratedResponseData.setOrderStatus("1");
        } else if (SystemInstance.TRADE_FAIL.equals(tradeDetail.getStatus())) {
            integratedResponseData.setOrderStatus("2");
        } else if (SystemInstance.TRADE_WAITING.equals(tradeDetail.getStatus())) {
            integratedResponseData.setOrderStatus("0");
        }
        integratedResponseData.setSysOrderNo(tOBwebATMQueryResponse.getoNO());
        integratedResponseData.setStoreOrderNo(tOBwebATMQueryResponse.getoNO());
        integratedResponseData.setTotalFee(tOBwebATMQueryResponse.getCurAmt());
        integratedResponseData.setBody(tradeDetail.getDescription());
        integratedResponseData.setPaidAt(tOBwebATMQueryResponse.getTrnDt() + tOBwebATMQueryResponse.getTrnTime());

        ResponseGeneralBody responseBody = new ResponseGeneralBody();
        responseBody.setHeader(responseHeader);
        responseBody.setData(integratedResponseData);

        return new Gson().toJson(responseBody);
    }

    @Override
    public void getPaymentUrl(String amount, String shortId, HttpServletResponse response) throws Exception {

        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(shortId);
        if (!Objects.isNull(qrcodeParameter)) {

            Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());
            PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 57);

            String nowStr = LocalDateTime.now().format(DateTimeFormatter.ofPattern(SystemInstance.DATE_PATTERN));
            if (StringUtils.isNotBlank(qrcodeParameter.getTimeExpire()) && Long.valueOf(nowStr) > Long.valueOf(qrcodeParameter.getTimeExpire())) {
                throw new OtherAPIException("QrCode is invalid");
            }
            if (Objects.isNull(paymentAccount)) {
                throw new ServiceTypeNotSupportException("Exception: The store's account isn't opened.");
            }

            String storeOrder = "1".equals(qrcodeParameter.getCodeType()) ? qrcodeParameter.getStoreOrderNo() : orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);


            TOBwebATMCreateOrderRequestData toBwebATMCreateOrderRequestData = new TOBwebATMCreateOrderRequestData();
            toBwebATMCreateOrderRequestData.setSendSeqNo(storeOrder);//傳送序號SendSeqNo
            toBwebATMCreateOrderRequestData.setmID(paymentAccount.getAccount()); //特店代碼MID  資料庫欄位Account
            toBwebATMCreateOrderRequestData.settID(paymentAccount.getAccountTerminalId()); //終端機代碼TID  DB欄位AccountTerminalId
            toBwebATMCreateOrderRequestData.setTxnType(paymentAccount.getBankCode()); //交易類別 TxnType   DB欄位bankCode
            toBwebATMCreateOrderRequestData.setUserData(storeOrder); //預設notify會帶回來的參數
            toBwebATMCreateOrderRequestData.setAccIdTo(qrcodeParameter.getDetail()); //銷帳編號AccIdTo(須串街方自編，由qrcodeParameter動態帶入)
            toBwebATMCreateOrderRequestData.setCurAmt(Integer.parseInt(amount)); //金額
            toBwebATMCreateOrderRequestData.setRsURL(String.format("%sallpaypass/api/TOBwebATM/backUrl", API_URL));//臺銀回傳訪問的網址

            toBwebATMCreateOrderRequestData.setoNO(storeOrder);
            //客製加密工具
            TOBwebAtmUtil toBwebAtmUtil = new TOBwebAtmUtil();
            String enkey = toBwebAtmUtil.requestEnMethod(paymentAccount.getAccount(),//特店代碼MID  資料庫欄位Account
                    storeOrder, //訂單編號ONO
                    qrcodeParameter.getDetail(),//銷帳編號AccIdTo
                    Integer.parseInt(amount),//金額CurAmt
                    paymentAccount.getHashKey());//密鑰

            toBwebATMCreateOrderRequestData.setmAC(enkey);//押碼

            //建立訂單
            setOnlineTradeDetailRequest(toBwebATMCreateOrderRequestData, merchant, paymentAccount, qrcodeParameter);


            String request = new Gson().toJson(toBwebATMCreateOrderRequestData);
            LOGGER.info("[REQUEST][TOBwebATM][OnLine]: " + request);

            createOrder(toBwebATMCreateOrderRequestData, paymentAccount, response);


        } else if (qrcodeParameter == null) {
            LOGGER.error("[REQUEST][TOBwebATM] qrcode not found");
        }

    }

    @Override
    public String backUrl(String res) throws Exception {
        LOGGER.info("[TobWebATM][backUrl][CardPayRs]:" + res);

        //原本xml轉Base64送出去，在這邊轉回來，xmlUtil解析URL
        byte[] decodedBytes = Base64.getDecoder().decode(res.getBytes());
        String CardPayRs = new String(decodedBytes, "Big5");

        LOGGER.info("[TobWebATM][backUrl][CardPayRsStr]:" + CardPayRs);


        TOBwebATMCreateOrderResponseData tOBwebATMCreateOrderResponseData = XmlUtil.unmarshall(CardPayRs, TOBwebATMCreateOrderResponseData.class);


        LOGGER.info("[TobWebATM][backUrl][toString]:" + tOBwebATMCreateOrderResponseData.toString());


        TradeDetail tradeDetail = tradeDetailService.getOne(tOBwebATMCreateOrderResponseData.getoNO());

        PaymentAccount paymentAccount = paymentAccountService.getOne(tradeDetail.getAccountId(), 57);
        String enMethod = new TOBwebAtmUtil().responseEnMethod(tOBwebATMCreateOrderResponseData, paymentAccount.getHashKey());

        if (null == tradeDetail) {
            LOGGER.error("[TobWebATM][backUrl] tradedetail is not found.");
            throw new OrderNotFoundException("Exception: Order is not found");
        } else {
            tradeDetail.setMethod("15700");
            tradeDetail.setPaymentAccount(paymentAccount);
            tradeDetail.setTxParams(CardPayRs);
            tradeDetail.setSystemOrderId(tOBwebATMCreateOrderResponseData.getoNO());
        }

        //驗簽
        //訂單編號
        //金額
        //交易狀態回傳"0"成功，其餘都是失敗
        if (enMethod.equals(tOBwebATMCreateOrderResponseData.getmAC()) &&
                tOBwebATMCreateOrderResponseData.getoNO().equals(tradeDetail.getOrderId()) &&
                tOBwebATMCreateOrderResponseData.getCurAmt().equals(String.valueOf(tradeDetail.getPayment())) &&
                tOBwebATMCreateOrderResponseData.getrC().equals("0")) {

            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetailService.save(tradeDetail);
            LOGGER.info("[TobWebATM][REQUEST][backUrl] tradedetail is success.");

            appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));

        } else {
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            tradeDetailService.save(tradeDetail);
            LOGGER.error("[TobWebATM][REQUEST][backUrl]  save tradeDetail fail.");

        }


        return tOBwebATMCreateOrderResponseData.getoNO();


    }

    @Override
    public void notify(String reqData) throws Exception {

        //先去除前面不要的字串
        String data = reqData.substring(reqData.indexOf("<?xml")).replaceAll("\r\n", "");
        LOGGER.info("[TobWebATM][notify][data]:" + data);
        TOBwebATMNotifyData tOBwebATMNotifyData = XmlUtil.unmarshall(data, TOBwebATMNotifyData.class);
        LOGGER.info("[TobWebATM][notify][toString]:" + tOBwebATMNotifyData.toString());


        //檢查TradeDetail.txParams欄位是否有backURL的資料，如果沒有就塞當前接收到的資料進去
//        TradeDetail tradeDetail = tradeDetailService.getOne("");
    }

    //將資料轉xml格式再轉Base64再把請求發送給臺銀API
    @Override
    public void createOrder(TOBwebATMCreateOrderRequestData request, PaymentAccount paymentAccount, HttpServletResponse response) throws Exception {


        LOGGER.info("[sendingRequest][TOBwebATM][createOrder]..");
        //String_to_xml_to_base64
        String toxml = request.toString();
        LOGGER.info("[sendingRequest][TOBwebATM][createOrder][StringToXml].." + toxml);
        byte[] encodedBytes = Base64.getEncoder().encode(toxml.getBytes());
        String encodedStr = new String(encodedBytes);

        Map<String, String> data = new HashMap<>();
        data.put("CardPayRq", encodedStr);

        HttpUtilBig5.sendForm(response, TOBwebATM_URL, data);

    }

    //建立訂單
    private TradeDetail setOnlineTradeDetailRequest(TOBwebATMCreateOrderRequestData toBwebATMCreateOrderRequestData, Merchant merchant, PaymentAccount paymentAccount, QrcodeParameter qrcodeParameter) {
        LOGGER.info("[TOBwebATM]enter trade detail request:" + new Gson().toJson(toBwebATMCreateOrderRequestData));
        TradeDetail tradeDetail = tradeDetailService.getOne(merchant.getAccountId(), toBwebATMCreateOrderRequestData.getoNO());

        if (Objects.isNull(tradeDetail)) {
            tradeDetail = new TradeDetail();
            tradeDetail.setAccountId(merchant.getAccountId());
            tradeDetail.setMethod("15700");//五位數
            LOGGER.info("[TOBwebATM]tradeDetail.getMethod()" + new Gson().toJson(tradeDetail.getMethod()));
            tradeDetail.setPaymentAccount(paymentAccountService.getOne(qrcodeParameter.getMchId(), 57));
            tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
            tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
            tradeDetail.setOrderId(toBwebATMCreateOrderRequestData.getoNO());//訂單編號
            tradeDetail.setDeviceRandomId(deviceService.getOne(merchant, "APP-001"));
            tradeDetail.setPayment(toBwebATMCreateOrderRequestData.getCurAmt());//交易金額
            tradeDetail.setServiceType("OLPay");
            tradeDetail.setQrcodeToken(qrcodeParameter.getShortId());
            tradeDetail.setDescription(qrcodeParameter.getBody());
            tradeDetail.setOriginalPrice(qrcodeParameter.getTotalFee());
            tradeDetail = tradeDetailService.save(tradeDetail);
            tradeProcedureService.captureOrderSave(tradeDetail);

        }
        LOGGER.info("[TOBwebATM]save online trade detail : " + new Gson().toJson(tradeDetail));
        return tradeDetail;
    }

}
