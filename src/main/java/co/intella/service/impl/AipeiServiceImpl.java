package co.intella.service.impl;

import co.intella.domain.aipei.AipeiPaymentParaData;
import co.intella.domain.aipei.AipeiPaymentRequestData;
import co.intella.domain.aipei.AipeiPaymentResponseData;
import co.intella.domain.aipei.AipeiPaymentResponseParaData;
import co.intella.domain.integration.*;
import co.intella.model.*;
import co.intella.net.HttpRequestUtil;
import co.intella.service.*;
import co.intella.utility.AipeiUtil;
import co.intella.utility.GeneralUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonPrimitive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @author Alex
 */
@Service
public class AipeiServiceImpl implements AipeiService {


    private final Logger LOGGER = LoggerFactory.getLogger(AipeiServiceImpl.class);

    @Resource
    private PaymentAccountService paymentAccountService;

    @Autowired
    private Environment env;

    @Resource
    private MerchantService merchantService;

    @Autowired
    private MessageSource messageSource;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private RefundDetailService refundDetailService;

    @Value("${host.request.aipei.offline}")
    private String AIPEI_URL;

    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {

        String generalResponseData = SystemInstance.EMPTY_STRING;

        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        PaymentAccount paymentAccount = paymentAccountService.getOneByDao(integrateMchId, bankId);
        Merchant merchant = merchantService.getOne(integrateMchId);

        if (requestHeader.getServiceType().equals(SystemInstance.TYPE_MICROPAY)) {
            paymentAccount = paymentAccountService.getOneByDao(integrateMchId, 17);
            IntegratedMicropayRequestData integratedMicropayRequestData = new Gson().fromJson(data.getAsString(), IntegratedMicropayRequestData.class);
            AipeiPaymentParaData aipeiPaymentParaData = new Gson().fromJson(data.getAsString(), AipeiPaymentParaData.class);
            aipeiPaymentParaData.setTransCreateTime(requestHeader.getCreateTime());
            aipeiPaymentParaData.setTransAmount(Integer.valueOf(integratedMicropayRequestData.getTotalFee()) * 100);
            aipeiPaymentParaData.setTransName(integratedMicropayRequestData.getBody());
            aipeiPaymentParaData.setCurrency("TWD");
            aipeiPaymentParaData.setBuyerCodeType("qrcode");
            aipeiPaymentParaData.setMerchantType("5995");
            aipeiPaymentParaData.setMerchantId(paymentAccount.getAccount());
            aipeiPaymentParaData.setMerchantName(merchant.getName());
            aipeiPaymentParaData.setTerminalName(aipeiPaymentParaData.getTerminalId());


            AipeiPaymentRequestData aipeiPaymentRequestData = new AipeiPaymentRequestData();

            aipeiPaymentRequestData.setPartnerId(paymentAccount.getAccount());
            aipeiPaymentRequestData.setService("ap.pay");
            aipeiPaymentRequestData.setData(new ObjectMapper().writeValueAsString(aipeiPaymentParaData));
            aipeiPaymentRequestData.setSignType("MD5");
            aipeiPaymentRequestData.setSign(getSign(aipeiPaymentRequestData, paymentAccount));


            tradeProcedureService.setTradeDetailRequest(requestHeader, data);
            String paymentResult = payment(aipeiPaymentRequestData);

            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, paymentResult, data);

            AipeiPaymentResponseData aipeiPaymentResponseData = new Gson().fromJson(paymentResult, AipeiPaymentResponseData.class);
            AipeiPaymentResponseParaData aipeiPaymentResponseParaData = new Gson().fromJson(aipeiPaymentResponseData.getData(), AipeiPaymentResponseParaData.class);


            ResponseGeneralHeader responseGeneralHeader = new AipeiUtil().getResponseHeader(aipeiPaymentResponseData.getErrorCode(), aipeiPaymentResponseData.getData(), requestHeader, messageSource);
            responseGeneralHeader.setMchId(integrateMchId);


            IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();

            if (aipeiPaymentResponseParaData.getApTransResult().equals("SUCCESSFUL")) {
                integratedMicropayResponseData.setSysOrderNo(aipeiPaymentResponseParaData.getApTransId());
                integratedMicropayResponseData.setTotalFee(String.valueOf(aipeiPaymentResponseParaData.getTransAmount() / 100));
            } else {
                integratedMicropayResponseData.setSysOrderNo(SystemInstance.EMPTY_STRING);
                integratedMicropayResponseData.setTotalFee(SystemInstance.EMPTY_STRING);
            }
            integratedMicropayResponseData.setStoreOrderNo(aipeiPaymentResponseParaData.getPartnerTransId());
            integratedMicropayResponseData.setAuthCode(aipeiPaymentParaData.getBuyerCode());
            integratedMicropayResponseData.setPlatformRsp(paymentResult);

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedMicropayResponseData);


            generalResponseData = new Gson().toJson(responseGeneralBody);
            LOGGER.info("integrate response ready :" + generalResponseData);


        } else if (requestHeader.getServiceType().equals("Refund")) {
            LOGGER.info("[Aipei][Refund][data]" + data.getAsString());
            IntegratedRefundRequestData integratedRefundRequestData = new Gson().fromJson(data.getAsString(), IntegratedRefundRequestData.class);

            AipeiPaymentParaData aipeiPaymentParaData = new Gson().fromJson(data.getAsString(), AipeiPaymentParaData.class);
            if (aipeiPaymentParaData.getPartnerRefundId() == null) {
                aipeiPaymentParaData.setPartnerRefundId("R" + aipeiPaymentParaData.getPartnerTransId());
            }
            aipeiPaymentParaData.setCurrency("TWD");
            aipeiPaymentParaData.setRefundAmount(Integer.valueOf(integratedRefundRequestData.getRefundFee()) * 100);

            AipeiPaymentRequestData aipeiPaymentRequestData = new AipeiPaymentRequestData();

            String refundResult;
            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
            if (tradeDetail.getServiceType().equals(SystemInstance.TYPE_MICROPAY)) {
                paymentAccount = paymentAccountService.getOneByDao(integrateMchId, 17);
                aipeiPaymentRequestData.setPartnerId(paymentAccount.getAccount());
                aipeiPaymentRequestData.setService("ap.refund");
                aipeiPaymentRequestData.setData(new ObjectMapper().writeValueAsString(aipeiPaymentParaData));
                aipeiPaymentRequestData.setSignType("MD5");
                aipeiPaymentRequestData.setSign(getSign(aipeiPaymentRequestData, paymentAccount));

                refundResult = refundPayment(aipeiPaymentRequestData);
            } else {
                refundResult = null;
//                refundResult = refundReserve(lineRefundRequestData,paymentAccount,tradeDetail.getSystemOrderId());
            }
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, refundResult, data);

            AipeiPaymentResponseData aipeiPaymentResponseData = new Gson().fromJson(refundResult, AipeiPaymentResponseData.class);


            ResponseGeneralHeader responseGeneralHeader = new AipeiUtil().getResponseHeader(aipeiPaymentResponseData.getErrorCode(), aipeiPaymentResponseData.getData(), requestHeader, messageSource);
            responseGeneralHeader.setMchId(integrateMchId);

            IntegratedRefundResponseData integratedRefundResponseData = new AipeiUtil().getIntegratedRefundResponseData(aipeiPaymentResponseData.getData(), requestHeader, tradeDetail, integratedRefundRequestData);
            integratedRefundResponseData.setPlatformRsp(refundResult);

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedRefundResponseData);
            generalResponseData = new Gson().toJson(responseGeneralBody);

            LOGGER.info("integrate response ready :" + generalResponseData);

        } else if (requestHeader.getServiceType().equals("SingleOrderQuery")) {
            LOGGER.info("[Aipei][SingleOrderQuery][data]" + data.getAsString());


            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

            if (tradeDetail.getServiceType().equals(SystemInstance.TYPE_MICROPAY)) {
                paymentAccount = paymentAccountService.getOneByDao(integrateMchId, 17);
            }

            AipeiPaymentParaData aipeiPaymentParaData = new Gson().fromJson(data.getAsString(), AipeiPaymentParaData.class);

            AipeiPaymentRequestData aipeiPaymentRequestData = new AipeiPaymentRequestData();
            aipeiPaymentRequestData.setPartnerId(paymentAccount.getAccount());
            aipeiPaymentRequestData.setService("ap.query");
            aipeiPaymentRequestData.setData(new ObjectMapper().writeValueAsString(aipeiPaymentParaData));
            aipeiPaymentRequestData.setSignType("MD5");
            aipeiPaymentRequestData.setSign(getSign(aipeiPaymentRequestData, paymentAccount));


            String result = query(aipeiPaymentRequestData);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);


            LOGGER.info("[LinePay][SingleOrderQuery][result].." + result);

            AipeiPaymentResponseData aipeiPaymentResponseData = new Gson().fromJson(result, AipeiPaymentResponseData.class);


            ResponseGeneralHeader responseGeneralHeader = new AipeiUtil().getResponseHeader(aipeiPaymentResponseData.getErrorCode(), aipeiPaymentResponseData.getData(), requestHeader, messageSource);
            responseGeneralHeader.setMchId(integrateMchId);

            IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData =
                    new AipeiUtil().getIntegratedSingleOrderQueryResponseData(aipeiPaymentResponseData, tradeDetailService.getOne(tradeDetail.getOrderId()));
            RefundDetail refundDetail = refundDetailService.getOneByOrderIdAndMethod(tradeDetail.getOrderId(), tradeDetail.getMethod());
            if (Objects.nonNull(refundDetail)) {
                integratedSingleOrderQueryResponseData.setRefundedAt(refundDetail.getCreateTime());
            }
//            integratedSingleOrderQueryResponseData.setPlatformRsp(result);
            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedSingleOrderQueryResponseData);
            generalResponseData = new Gson().toJson(responseGeneralBody);

            LOGGER.info("integrate response ready :" + generalResponseData);

        }
        return generalResponseData;
    }

    public String payment(AipeiPaymentRequestData request) throws Exception {
        LOGGER.info("[Aipei][payment][request] : " + new ObjectMapper().writeValueAsString(request));

        return HttpRequestUtil.post(AIPEI_URL + "barcode/pay", new ObjectMapper().writeValueAsString(request));
    }

    public String query(AipeiPaymentRequestData request) throws Exception {
        LOGGER.info("[Aipei][payment][request] : " + new ObjectMapper().writeValueAsString(request));

        return HttpRequestUtil.post(AIPEI_URL + "barcode/query", new ObjectMapper().writeValueAsString(request));
    }

    public String refundPayment(AipeiPaymentRequestData request) throws Exception {
        LOGGER.info("[Aipei][refundPayment][request] : " + new ObjectMapper().writeValueAsString(request));

        return HttpRequestUtil.post(AIPEI_URL + "barcode/refund", new ObjectMapper().writeValueAsString(request));
    }

    private String getSign(AipeiPaymentRequestData request, PaymentAccount paymentAccount) {

        return GeneralUtil.getMD5(request.getPartnerId() + request.getService() + request.getData() + request.getSignType() + paymentAccount.getHashKey());

    }


}
