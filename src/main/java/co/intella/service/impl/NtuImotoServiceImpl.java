package co.intella.service.impl;

import co.intella.crypto.AesCryptoUtil;
import co.intella.crypto.KeyReader;
import co.intella.domain.ntuImoto.IMotoValuation;
import co.intella.domain.ntuImoto.ImotoBaseResponse;
import co.intella.model.*;
import co.intella.net.Constant;
import co.intella.net.HttpRequestUtil;
import co.intella.service.*;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import co.intella.utility.MD5Util;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.crypto.SecretKey;
import java.io.File;
import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

import static java.util.Objects.nonNull;

@Service("ntuImotoService")
public class NtuImotoServiceImpl implements NtuImotoService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Value("${api.url}")
    private String API_URL;

    @Resource
    private MerchantService merchantService;

    @Resource
    private ImotoTokenService imotoTokenService;

    @Resource
    private ImotoTokenLogService imotoTokenLogService;

    @Resource
    private LookupCodeService lookupCodeService;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Value("${execution.mode}")
    private String EXEC_MODE;

    @Value("${host.scan2pay.api}")
    private String HOST_SCAN2PAY_API;

    final private String NTU_INTELLA_KEY = "883650CCDB112";
    final private String NTU_PSS_KEY = "12681939@PSS";
    final private String IMOTO_LOOKUP_TYPE = "imoto";

    @Override
    public String doValuation(IMotoValuation iMotoValuationRequest) {
        LOGGER.info("[valuation] input :" + iMotoValuationRequest);
        ImotoToken ntuImotoToken = imotoTokenService.getOneForEffectiveByOutcode(iMotoValuationRequest.getTokenOutercode());
        String redirectStr = "redirect:" + API_URL + "/error";

        if (nonNull(ntuImotoToken)) {
            Map<String, String> map = this.getPssGroupValuationMap(iMotoValuationRequest, ntuImotoToken);
            String result = this.send(map);
            ImotoBaseResponse response = JsonUtil.parseJson(result, ImotoBaseResponse.class);
            if (this.checkSign(JsonUtil.parseJson(result, Map.class))) {
                int logCont = this.insertImotoTokenLog(iMotoValuationRequest, ntuImotoToken, response);
                LOGGER.info("[valuation] insert ntuImotoTokenLog: " + logCont);
                if (logCont == 1) {
                    redirectStr = "redirect:" + API_URL + ntuImotoToken.getShortId();
                }
            }
        }
        return redirectStr;
    }

    @Override
    public Long checkoutImotoAmount(String shortId) {
        long amount = -1;
        ImotoToken ntuImotoToken = imotoTokenService.getOneForEffectiveByShortId(shortId);
        if (nonNull(ntuImotoToken)) {
            ImotoTokenLog imotoTokenLog = imotoTokenLogService.getLastLogByOutercodeAndTokenSeq(ntuImotoToken.getTokenOutercode(), ntuImotoToken.getImotoTokenSeq());
            if (nonNull(imotoTokenLog)) {
                amount = imotoTokenLog.getAmount();
            }
        }
        return amount;
    }

    private String getLookupCodeByNtuPssGroupUrl() {
        return lookupCodeService.findOne(IMOTO_LOOKUP_TYPE, "ntuPssGroupUrl").getType1();
    }

    private Map<String, String> getPssGroupValuationMap(IMotoValuation iMotoValuation, ImotoToken imotoToken) {
        Map<String, String> map = new HashMap<>();
        map.put("tokenInternalCode", imotoToken.getTokenInternalCode());
        map.put("promoCode", iMotoValuation.getPromoCode());
        String sign = this.getSign(map);
        map.put("sign", sign);
        String request = JsonUtil.toJsonStr(map);
        map.clear();
        map.put("TransData", request);
        return map;
    }

    private String getSign(Map<String, String> map) {
        Map<String, String> signMap = new TreeMap<>();
        signMap.putAll(map);
        IntTbLookupCode intTbLookupCode = this.getLookupCodeByNtuPssGroupKey();
        boolean checkLookupCode = Objects.isNull(intTbLookupCode);
        signMap.put("intella", checkLookupCode ? NTU_INTELLA_KEY : intTbLookupCode.getType1());
        signMap.put("pssGroup", checkLookupCode ? NTU_PSS_KEY : intTbLookupCode.getType2());
        String signStr = JsonUtil.toJsonStr(signMap);
        LOGGER.info("[getSign]  signStr  = " + signStr);

        return MD5Util.getMD5(signStr).toUpperCase();

    }

    private IntTbLookupCode getLookupCodeByNtuPssGroupKey() {
        return lookupCodeService.findOne(IMOTO_LOOKUP_TYPE, "ntuPssGroupKey");
    }

    private int insertImotoTokenLog(IMotoValuation iMotoValuation, ImotoToken ntuImotoToken, ImotoBaseResponse response) {
        ImotoTokenLog ntuImotoTokenLog = new ImotoTokenLog();
        ntuImotoTokenLog.setPhoneNum(iMotoValuation.getPhoneNum());
        ntuImotoTokenLog.setImotoTokenSeq(ntuImotoToken.getImotoTokenSeq());
        ntuImotoTokenLog.setTokenOutercode(ntuImotoToken.getTokenOutercode());
        ntuImotoTokenLog.setAmount(Long.valueOf(StringUtils.defaultIfBlank(response.getAmount(), "0")));
        ntuImotoTokenLog.setPromoCode(iMotoValuation.getPromoCode());
        ntuImotoTokenLog.setPromoCodeValid(response.getPromoCodeValid());

        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        try {
            ntuImotoTokenLog.setEnterTime(sdf.parse(response.getEnterTime()));
        } catch (Exception e) {
            LOGGER.error("  setEnterTime err  value => " + response.getEnterTime());
        }
        try {
            ntuImotoTokenLog.setExitTime(sdf.parse(response.getExitTime()));
        } catch (Exception e) {
            LOGGER.error("  setExitTime err  value => " + response.getExitTime());
        }
        try {
            ntuImotoTokenLog.setLastExitTime(sdf.parse(response.getLastExitTime()));
        } catch (Exception e) {
            LOGGER.error("  setLastExitTime err  value => " + response.getLastExitTime());
        }

        return imotoTokenLogService.insert(ntuImotoTokenLog);
    }

    private boolean checkSign(Map<String, String> resultMap) {
        String respSign = resultMap.remove("sign");
        String sign = this.getSign(resultMap);
        LOGGER.info("[checkSign]  respSign =>\t\t" + respSign);
        LOGGER.info("[checkSign]  sign =>\t\t" + sign);
        return StringUtils.defaultIfBlank(respSign, "").equalsIgnoreCase(sign);
    }

    @Override
    public boolean doNotify(TradeDetail tradeDetail) {
        boolean doScan2PayRefund = false;
        if (StringUtils.isNotBlank(tradeDetail.getQrcodeToken())) {
            ImotoToken ntuImotoToken = imotoTokenService.getOneForEffectiveByShortId((tradeDetail.getQrcodeToken()));
            if (nonNull(ntuImotoToken)) {
                LOGGER.info("[ntuImoto][doNotify] tradeDetail : " + tradeDetail.getOrderId() + "\t" + tradeDetail.getQrcodeToken());
                ImotoTokenLog imotoTokenLog = imotoTokenLogService.getLastLogByOutercodeAndTokenSeq(ntuImotoToken.getTokenOutercode(), ntuImotoToken.getImotoTokenSeq());
                Map<String, String> reqMap = this.getNotifyMap(tradeDetail, ntuImotoToken, imotoTokenLog);
                String result = this.send(reqMap);
                ImotoBaseResponse response = JsonUtil.parseJson(result, ImotoBaseResponse.class);
                if (this.checkSign(JsonUtil.parseJson(result, Map.class))) {
                    if (!"00".equals(response.getMsgCode())) {
                        //TODO  阜爾不接受 該次回調通知，則直接發退款請求 一次
                        this.doScan2PayRefund(tradeDetail, response);
                        doScan2PayRefund = true;
                    }
                }
            }
        }
        return doScan2PayRefund;
    }

    private String send(Map<String, String> reqMap) {
        LOGGER.info("[send] req : " + JsonUtil.toJsonStr(reqMap));
        String result = HttpUtil.doPost(this.getLookupCodeByNtuPssGroupUrl(), reqMap).replaceAll("\r?\n", "").replaceAll("^.*(\\{.*\\}).*$", "$1");
        LOGGER.info("[send] resp : " + result);
        return result;
    }

    private Map<String, String> getNotifyMap(TradeDetail tradeDetail, ImotoToken imotoToken, ImotoTokenLog imotoTokenLog) {
        Map<String, String> map = new HashMap<>();
        map.put("tokenInternalCode", imotoToken.getTokenInternalCode());
        map.put("promoCode", imotoTokenLog.getPromoCode());
        map.put("amount", String.valueOf(tradeDetail.getPayment()));
        map.put("orderNo", tradeDetail.getOrderId());
        map.put("orderStatus", tradeDetail.getStatus());
        String sign = this.getSign(map);
        map.put("sign", sign);
        String request = JsonUtil.toJsonStr(map);
        map.clear();
        map.put("TransData", request);
        return map;
    }

    private void doScan2PayRefund(TradeDetail tradeDetail, ImotoBaseResponse response) {
        Merchant merchant = merchantService.getOne(tradeDetail.getAccountId());
        Map<String, String> requestData = new HashMap<String, String>();
        requestData.put("Method", tradeDetail.getMethod());
        requestData.put("ServiceType", "Refund");
        requestData.put("MchId", tradeDetail.getAccountId());
        requestData.put("TradeKey", merchant.getTradePin());
        requestData.put("CreateTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        requestData.put("DeviceId", "skb0001");
        requestData.put("StoreOrderNo", tradeDetail.getOrderId());
        requestData.put("StoreRefundNo", "R" + tradeDetail.getOrderId());
        requestData.put("RefundKey", merchant.getRefundPin());
        requestData.put("RefundFee", String.valueOf(tradeDetail.getPayment()));
        requestData.put("RefundedMsg", "imoto notify error. " + response.getMsg());

        try {
            String keyPath = EXEC_MODE.equals("stage") ? "stage-pub.der" : "pub.der";
            SecretKey secretKey = AesCryptoUtil.generateSecreteKey();

            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("key/" + keyPath).getFile());
            PublicKey publicKey = KeyReader.loadPublicKeyFromDER(file);

            String responseEntity = HttpRequestUtil.httpsPost(HOST_SCAN2PAY_API, requestData, publicKey, secretKey);

            String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, responseEntity);
            LOGGER.info("[decryptResponse] " + decryptResponse);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
