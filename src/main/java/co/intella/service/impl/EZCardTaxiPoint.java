package co.intella.service.impl;

import co.intella.model.Device;
import co.intella.model.IntTbAbnormalRule;
import co.intella.model.IntTbLookupCode;
import co.intella.service.*;
import co.intella.utility.EzcUtil;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class EZCardTaxiPoint extends TaxiPoint {
    private final Logger LOGGER = LoggerFactory.getLogger(EZCardTaxiPoint.class);

    @Resource
    private LookupCodeService lookupCodeService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private AbnormalRuleService abnormalRuleService;

    @Resource
    private AbnormalRuleCtrlService abnormalRuleCtrlService;

    @Resource
    private TaxiTransactionDetailService taxiTransactionDetailService;

    @Resource
    private TicketLogicService ticketLogicService;


    @Override
    public String getScan2PayLcation(Device device) {

        IntTbLookupCode lookupCode = lookupCodeService.findOne("ezclocation", device.getLocation());
        String location = "";
        if (Objects.nonNull(lookupCode) && lookupCode.getValue() == 1) {
            location = lookupCode.getType1();
            LOGGER.info("[getScan2PayLcation][ezclocation][" + device.getLocation() + "]" + lookupCode.getType1());
        }
        return location;
    }

    @Override
    public String getlLcationUsePointLimit(String location) {

        IntTbLookupCode lookupCode = lookupCodeService.findOne("ezclocation", location);
        String pointLimit = "85";
        if (Objects.nonNull(lookupCode)) {
            pointLimit = lookupCode.getType2();
            LOGGER.info("[getlLcationUsePointLimit][ezclocation][" + location + "]" + lookupCode.getType2());
        }
        return pointLimit;
    }

    @Override
    public IntTbLookupCode isCanDeductionPoint(Device device, JsonObject queryCardJson) {
        LOGGER.info("[isCanDeductionPoint][device.location = " + device.getLocation() + "]" + "card AreaCode = " + queryCardJson.get("AreaCode").getAsString());

        String cardAreaCode = queryCardJson.get("AreaCode").getAsString().toUpperCase();
        String lcation = lookupCodeService.findOne("ezclocation", cardAreaCode).getType1();

        boolean isCanDeductionPoint = EzcUtil.isCanDeductionPoint(device, queryCardJson, lcation);

        String cardType = queryCardJson.get("EZCardType").getAsString().trim();
        String personalProfile = queryCardJson.get("PersonalProfile").getAsString().trim();
        String authorization = queryCardJson.get("PersonalProfileAuthorization").getAsString().trim().toUpperCase();

        IntTbLookupCode ticketLookupCode = new IntTbLookupCode();
//        if (isCanDeductionPoint) {
//            if ("TP".equals(lcation)) {
//                authorization = "";
//            }
//            String lookupCode = "18" + device.getLocation() + cardType + personalProfile + authorization;
//            ticketLookupCode = lookupCodeService.findOne("carePoint", lookupCode);
//        } else {
        ticketLookupCode.setValue(0);
        ticketLookupCode.setType1(lookupCodeService.findOne("ezclocation", cardAreaCode).getType1());
        ticketLookupCode.setType2("0");
        ticketLookupCode.setType3("0");
        IntTbLookupCode personalLookupCode = lookupCodeService.findOne("ezcPersonalProfile", personalProfile);
        ticketLookupCode.setDscr(Objects.nonNull(personalLookupCode) ? personalLookupCode.getDscr() : personalProfile);
//        }
        if ("TP".equals(lcation)) {
            authorization = "";
        }
        String lookupCode = "18" + cardAreaCode + cardType + personalProfile + authorization;

        if (isCanDeductionPoint && device.getLocation().equals(cardAreaCode)) {
            ticketLookupCode = lookupCodeService.findOne("carePoint", lookupCode);
        } else if (isCanDeductionPoint) {
            IntTbLookupCode ticketLookupCode2 = lookupCodeService.findOne("carePoint", lookupCode);
            if (Objects.nonNull(ticketLookupCode2)) {
                ticketLookupCode.setType2(ticketLookupCode2.getType2());
            }
        }

        LOGGER.info("[isCanDeductionPoint][carePoint] " + ticketLookupCode);
        return ticketLookupCode;
    }

    @Override
    public String verificationRule(Device device, JsonObject queryCardJson, List<IntTbAbnormalRule> abRList) {
        return ticketLogicService.getAbnormalRuleMsg(abRList, device.getLicensePlate(), queryCardJson.get("EZCardID").getAsString());
    }

    /*
     * { "Retry": "0",
     * "EZCIDExpiryDate": "20190911",       悠遊卡身份有效日期
     * "NewAESKey": "0d9bd4cc86fd5408fac28de766f9e144",
     *  "CardNumberForPrint": "949897717",     悠遊卡收據列印卡號
     * "CountOfDeductPoint": "1000",        當月扣點累計
     *  "PointType": "WELFARE",
     *  "Balance": "2647",          交易前餘額
     * "AutoLoadFlag": "0",       0 卡片沒有開啟 Autoload 功能  1 卡片有開啟 Autoload 功能
     *  "PersonalProfile": "03",
     *  "PointLastUseDate": "20190425",     點數最後使用日期00000000 表示未使用過
     *  "TXNResult": "Success",
     * "ErrorCode": "000000",
     * "CPUSubAreaCode": "0000",
     * "Date": "20190510",
     * "EZCardType": "01",          卡片類別
     * "PersonalProfileAuthorization": "04",地區身分認證
     * "RRNumber": "19051019275820",        Retrieval Reference Number
     * "AreaCode": "0A",                //地區別
     *  "EZCardID": "949897717",        //悠遊卡ＩＤ
     * "PointID": "WELFARE0A",
     * "ExpiryDate": "20190911145205",      悠遊卡有效日期
     * "TerminalTXNNumber": "180143",           ＰＯＳＩＤ
     *  "TXNType":
     * "EZCardDataInQueryForTaxi",
     * "Time": "180143",
     *  "AutoTopUpAmount": "0",     自動加值金額
     * "BeforeTXNBalance": "0",     交易後餘額
     * "HostSerialNumber": "192758",        交易序號
     * "CPUPurseVersion": "00",
     * "BankCode": "00" }           Bank Code 00悠遊卡
     */

    /**
     * 台中
     *
     * @param queryCardJson
     * @param requestData
     * @param intTbLookupCode
     * @param verificationRule
     * @param device
     * @return
     */
    public Map<String, String> allocationForTC(JsonObject queryCardJson, JsonObject requestData, IntTbLookupCode intTbLookupCode, Boolean verificationRule, Device device) {
        String lcationUsePointLimit = getlLcationUsePointLimit(device.getLocation());
        LOGGER.info("[verificationRule]" + verificationRule);
        LOGGER.info("[IntTbLookupCode]" + intTbLookupCode);

        int needBalance = 0, point = 0, cash = 0, discountAmount = 0, specialIdentityPointRatio = 0;
        Map<String, String> map = new HashMap<>();

        Integer total = Integer.valueOf(requestData.get("Total").getAsString());
        Integer balance = Integer.valueOf(queryCardJson.get("Balance").getAsString());  //錢包餘額
        if (verificationRule && !(Objects.isNull(intTbLookupCode) || Objects.isNull(intTbLookupCode.getLookupType()))
                && total >= Integer.valueOf(lcationUsePointLimit)) { // 可扣社福點
            //剩餘點數
            Integer beforePoint = this.getBeforePoint(queryCardJson, intTbLookupCode);
            //點數折合台幣比率
            specialIdentityPointRatio = Integer.valueOf(intTbLookupCode.getType3());
            //最大可扣除點數
            point = Math.min(beforePoint, intTbLookupCode.getValue());
            //最大可優惠金額
            discountAmount = point * specialIdentityPointRatio;
        }

        if (discountAmount >= total) {   //優惠金額可全抵車資
            point = total > 0 ? total / specialIdentityPointRatio : 0;
        } else if (balance >= (total - discountAmount) || "1".equals(queryCardJson.get("AutoLoadFlag").getAsString())) {
            cash = total - discountAmount;
        } else {       //無自動充值 且餘額不足
            needBalance = total - discountAmount;
            if (balance > 0) {                      //若有餘額,錢包扣完
                cash = balance;
                needBalance -= balance;
            }
        }

        map.put("Point", Integer.toString(point));
        map.put("Cash", Integer.toString(cash));
        map.put("NeedBalance", Integer.toString(needBalance));
        return map;
    }

    /**
     * 桃園
     *
     * @param queryCardJson
     * @param requestData
     * @param intTbLookupCode
     * @param verificationRule
     * @param device
     * @return
     */
    public Map<String, String> allocationForTY(JsonObject queryCardJson, JsonObject requestData, IntTbLookupCode intTbLookupCode, Boolean verificationRule, Device device) {
        String lcationUsePointLimit = getlLcationUsePointLimit(device.getLocation());
        LOGGER.info("[verificationRule]" + verificationRule);
        LOGGER.info("[IntTbLookupCode]" + intTbLookupCode);

        int needBalance = 0, point = 0, cash = 0, discountAmount = 0, specialIdentityPointRatio = 0;
        Map<String, String> map = new HashMap<>();

        Integer total = Integer.valueOf(requestData.get("Total").getAsString());
        Integer balance = Integer.valueOf(queryCardJson.get("Balance").getAsString());  //錢包餘額
        if (verificationRule && !(Objects.isNull(intTbLookupCode) || Objects.isNull(intTbLookupCode.getLookupType()))
                && total >= Integer.valueOf(lcationUsePointLimit)) { // 可扣社福點
            //剩餘點數
            Integer beforePoint = this.getBeforePoint(queryCardJson, intTbLookupCode);
            //點數折合台幣比率
            specialIdentityPointRatio = Integer.valueOf(intTbLookupCode.getType3());
            //最大可扣除點數   <=100   36點,  101以上 72元
            int _point = total <= Integer.valueOf(lookupCodeService.findOne("ezclocation", device.getLocation()).getType3()) ? intTbLookupCode.getValue() : intTbLookupCode.getValue() * 2;
            //當乘車點數不足36或72點時，仍可以搭乘愛心計程車，付款時會將點數扣至0，其餘車資由卡片餘額扣除；並請留意倘點數及卡片餘額皆不足時，將無法交易。
            point = Math.min(_point, beforePoint);
            //最大可優惠金額
            discountAmount = point * specialIdentityPointRatio;
        }

        if (discountAmount >= total) {   //優惠金額可全抵車資
            point = total > 0 ? total / specialIdentityPointRatio : 0;
        } else if (balance >= (total - discountAmount) || "1".equals(queryCardJson.get("AutoLoadFlag").getAsString())) {
            cash = total - discountAmount;
        } else {       //無自動充值 且餘額不足
            point = 0;
            needBalance = total;
            cash = 0;
        }

        map.put("Point", Integer.toString(point));
        map.put("Cash", Integer.toString(cash));
        map.put("NeedBalance", Integer.toString(needBalance));
        return map;
    }

    /**
     * 台北
     *
     * @param queryCardJson
     * @param requestData
     * @param intTbLookupCode
     * @param verificationRule
     * @param device
     * @return
     */
    public Map<String, String> allocationForTP(JsonObject queryCardJson, JsonObject requestData, IntTbLookupCode intTbLookupCode, Boolean verificationRule, Device device) {
        String lcationUsePointLimit = getlLcationUsePointLimit(device.getLocation());
        LOGGER.info("[verificationRule]" + verificationRule);
        LOGGER.info("[IntTbLookupCode]" + intTbLookupCode);

        int needBalance = 0, point = 0, cash = 0, discountAmount = 0, specialIdentityPointRatio = 0;
        Map<String, String> map = new HashMap<>();

        Integer total = Integer.valueOf(requestData.get("Total").getAsString());
        Integer balance = Integer.valueOf(queryCardJson.get("Balance").getAsString());  //錢包餘額
        if (verificationRule && !(Objects.isNull(intTbLookupCode) || Objects.isNull(intTbLookupCode.getLookupType()))
                && total >= Integer.valueOf(lcationUsePointLimit)) { // 可扣社福點
            //剩餘點數
            Integer beforePoint = this.getBeforePoint(queryCardJson, intTbLookupCode);
            //點數折合台幣比率
            specialIdentityPointRatio = Integer.valueOf(intTbLookupCode.getType3());
            //最大可扣除點數
            //不分搭乘車資高低，每趟次最高扣點補助50元 (扣除點數50點)，其餘車資於悠遊卡內自行儲值金額中扣除
            point = Math.min(beforePoint, intTbLookupCode.getValue());
            //最大可優惠金額
            discountAmount = point * specialIdentityPointRatio;
        }

        if (discountAmount >= total) {   //優惠金額可全抵車資
            point = total > 0 ? total / specialIdentityPointRatio : 0;
        } else if (balance >= (total - discountAmount) || "1".equals(queryCardJson.get("AutoLoadFlag").getAsString())) {
            cash = total - discountAmount;
        } else {       //無自動充值 且餘額不足
            point = 0;
            needBalance = total;
            cash = 0;
        }

        map.put("Point", Integer.toString(point));
        map.put("Cash", Integer.toString(cash));
        map.put("NeedBalance", Integer.toString(needBalance));
        return map;
    }

    /**
     * 共通，不扣點 ，
     *
     * @param queryCardJson
     * @param requestData
     * @param intTbLookupCode
     * @param verificationRule
     * @param device
     * @return
     */
    public Map<String, String> allocationFor(JsonObject queryCardJson, JsonObject requestData, IntTbLookupCode intTbLookupCode, Boolean verificationRule, Device device) {
        LOGGER.info("[verificationRule]" + verificationRule);
        LOGGER.info("[IntTbLookupCode]" + intTbLookupCode);

        int needBalance = 0, point = 0, cash = 0;
        Map<String, String> map = new HashMap<>();

        Integer total = Integer.valueOf(requestData.get("Total").getAsString());
        Integer balance = Integer.valueOf(queryCardJson.get("Balance").getAsString());  //錢包餘額


        if (balance > total || "1".equals(queryCardJson.get("AutoLoadFlag").getAsString())) {
            cash = total;
        } else {       //無自動充值 且餘額不足
            needBalance = total;
            cash = 0;
        }

        map.put("Point", Integer.toString(point));
        map.put("Cash", Integer.toString(cash));
        map.put("NeedBalance", Integer.toString(needBalance));
        return map;
    }

    /**
     * 剩餘可使用點數
     *
     * @return
     */
    private int getBeforePoint(JsonObject queryCardJson, IntTbLookupCode intTbLookupCode) {
        String usedate = queryCardJson.get("PointLastUseDate").getAsString();
        String now = new SimpleDateFormat("yyyyMM").format(new Date());
        Integer beforePoint = Integer.valueOf(intTbLookupCode.getType2()) -
                (!now.equals(usedate.substring(0, 6)) || "00000000".equals(usedate) ? 0 : Integer.valueOf(queryCardJson.get("CountOfDeductPoint").getAsString()));
        return beforePoint;
    }

}
