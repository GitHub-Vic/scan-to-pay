package co.intella.service.impl;

import co.intella.model.ConsumerToken;
import co.intella.model.QConsumerToken;
import co.intella.repository.ConsumerTokenRepository;
import co.intella.service.ConsumerTokenService;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author Miles
 */
@Service
@Transactional
public class ConsumerTokenServiceImpl implements ConsumerTokenService {

    @Resource
    private ConsumerTokenRepository consumerTokenRepository;

    public ConsumerToken getOne(Long id) {
        QConsumerToken qConsumerToken = QConsumerToken.consumerToken;
        BooleanExpression predicate = qConsumerToken.id.eq(id);
        return consumerTokenRepository.findOne(predicate);
    }

    public ConsumerToken createOrUpdate(ConsumerToken entity) {
        return consumerTokenRepository.save(entity);
    }
}
