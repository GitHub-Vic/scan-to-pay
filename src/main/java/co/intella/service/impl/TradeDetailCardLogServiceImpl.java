package co.intella.service.impl;

import co.intella.model.TradeDetailCardLog;
import co.intella.service.TradeDetailCardLogService;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class TradeDetailCardLogServiceImpl implements TradeDetailCardLogService {

    private final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Value("${host.dao}")
    private String DAO_URL;

    @Override
    public TradeDetailCardLog save(TradeDetailCardLog tradeDetailCardLog) {
        try {
            String response = HttpUtil.doPostJson(DAO_URL + "api/tradeCardLog/save", JsonUtil.toJsonStr(tradeDetailCardLog));

            return JsonUtil.parseJson(response, TradeDetailCardLog.class);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] save() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }
}
