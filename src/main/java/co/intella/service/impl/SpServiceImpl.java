package co.intella.service.impl;

import co.intella.crypto.AesCryptoUtil;
import co.intella.domain.integration.IntegratedPaymentResponseData;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.domain.sp.*;
import co.intella.model.RequestHeader;
import co.intella.service.SpService;
import co.intella.utility.StatusManipulation;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.crypto.SecretKey;
import javax.net.ssl.*;
import java.io.*;
import java.net.URL;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Andy Lin
 */
@Service
@Transactional
public class SpServiceImpl implements SpService{
    private final Logger LOGGER = LoggerFactory.getLogger(SpServiceImpl.class);

    private final static String FORMALHOST = "https://core.spgateway.com/API/CreditCard";
    private final static String TESTHOST = "https://ccore.spgateway.com/API/CreditCard";
    private final static String HOST = TESTHOST;
    private final static String FORMALKEY = "";
    private final static String TESTKEY = "E8v7jy0dWp2g51C7GjFT1vbo5TP0dgMR";
    private final static String KEY = TESTKEY;
    private final static String FORMALIV = "YawWdOZ1P3O9n6mo";
    private final static String TESTIV = "piZIQ0WNqY4vZGCF";
    private final static String IV = TESTIV;

    private static String post(String requestUrl, String data, String method) throws Exception {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }};
        SSLContext context = SSLContext.getInstance("TLS");
        context.init((KeyManager[])null, trustAllCerts, (SecureRandom)null);
        URL url = new URL(requestUrl);
        HttpsURLConnection connection = (HttpsURLConnection)url.openConnection();
        connection.setSSLSocketFactory(context.getSocketFactory());
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setReadTimeout(10000);
        connection.setConnectTimeout(15000);
        connection.setDoInput(true);
        if("GET".equals(method)) {
            connection.setRequestMethod(method);
            connection.setDoOutput(false);
        } else if("POST".equals(method)) {
            connection.setRequestMethod(method);
            connection.setDoOutput(true);
            if(data != null) {
                BufferedWriter text = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
                text.write(data);
                text.flush();
                text.close();
                connection.getOutputStream().flush();
                connection.getOutputStream().close();
            }
        }

        StringBuilder text1 = new StringBuilder();
        BufferedReader reader = null;
        reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
        String line = null;

        while((line = reader.readLine()) != null) {
            text1.append(line);
        }

        reader.close();
        return text1.toString();
    }



    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {
        String result = "";
        if ("Payment".equals(requestHeader.getServiceType())) {
            SpPaymentRequestData requestData = new Gson().fromJson(data.getAsString(), SpPaymentRequestData.class);
            JsonObject requestData2 = new JsonParser().parse(data.getAsString()).getAsJsonObject();

            String postData = "";
            String ver = "1.0";
            postData = "TimeStamp=" + convertMillisecondToDate(requestHeader.getCreateTime()) + "&Version=" + ver + "&MerchantOrderNo=" + requestData.getStoreOrderNo() +
                    "&Amt=" + requestData.getTotalFee() + "&ProdDesc=" + requestData.getBody() + "&PayerEmail="+ requestData.getEmail() +
                    "&Inst=" + getInst(requestData2.get("TransMode").getAsString(), requestData2.get("Installment").getAsString()) +
                    "&CardNo=" + requestData.getCardId() + "&Exp=" + requestData.getExpireDate() + "&CVC=" + requestData.getExtenNo();

            SpRequestData spRequestData = new SpRequestData();
            spRequestData.setMchId(requestHeader.getMerchantId());
            spRequestData.setPostData(getSpEncryptedReq(postData));
            spRequestData.setPos("JSON");

//            result = payment(spRequestData);
            String response = getPaymentResponse();
            result = convertPaymentResponse(response, requestHeader, requestData);
            return result;

        }else if ("Capture".equals(requestHeader.getServiceType()) || "Refund".equals(requestHeader.getServiceType())) {
            SpRefundAndCaptureRequestData requestData = new Gson().fromJson(data.getAsString(), SpRefundAndCaptureRequestData.class);
            JsonObject requestData2 = new JsonParser().parse(data.getAsString()).getAsJsonObject();

            String postData = "";
            String ver = "1.0";
            postData = "RespondType=" + requestData.getRespondType() + "&Version=" + ver + "&Amt=" + requestData.getTotalFee() +
                    "&MerchantOrderNo=" + requestData.getStoreOrderNo() + "&TimeStamp=" + convertMillisecondToDate(requestHeader.getCreateTime()) +
                    "&IndexType=" + getIndexType(requestData.getStoreOrderNo(), requestData.getSysOrderNo()) +
                    "&TradeNo=" + requestData.getSysOrderNo() + "&CloseType=" + getCloseType(requestHeader.getServiceType());

            SpRequestData spRequestData = new SpRequestData();
            spRequestData.setMchId(requestHeader.getMerchantId());
            spRequestData.setPostData(getSpEncryptedReq(postData));

//            result = refundAndPay(spRequestData);
            String response = getRefundAndCaptureResponse();
            result = convertRefundAndCaptureResponse(response, requestHeader, requestData);
            return result;
        }else if ("SingleQuery".equals(requestHeader.getServiceType())) {
            SpSingleQueryRequestData requestData = new Gson().fromJson(data.getAsString(), SpSingleQueryRequestData.class);
            JsonObject requestData2 = new JsonParser().parse(data.getAsString()).getAsJsonObject();

            requestData.setMchId(requestHeader.getMerchantId());
            requestData.setVer("1.1");
            requestData.setCreateTime(convertMillisecondToDate(requestHeader.getCreateTime()));
            requestData.setCheckValue(calculateQueryCheckValue(requestData.getTotalFee(), requestData.getMchId(), requestData.getStoreOrderNo()));

//            result = singleQuery(requestData);
            String response = getSingleQueryResponse();
            result = convertSingleQueryResponse(response, requestHeader, requestData);
            return result;
        }
        return result;
    }

    public String payment(SpRequestData request) throws JsonProcessingException {

        LOGGER.info( "[REQUEST]"+ request);

        String spRequestData = new Gson().toJson(request);
        String result = "No result";
        try {
//            String result = HttpRequestUtil.post(spgateway,spRequestData,"POST");
            result = post(HOST,spRequestData,"POST");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    public String refundAndCapture(SpRequestData request) throws JsonProcessingException {

        LOGGER.info( "[REQUEST]"+ request);

        String spRequestData = new Gson().toJson(request);
        String result = "No result";
        try {
            result = post(HOST,spRequestData, "POST");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    public String singleQuery(SpSingleQueryRequestData request) throws JsonProcessingException {

        LOGGER.info( "[REQUEST]"+ request);

        String requestJson = new ObjectMapper().writeValueAsString(request);
        String result = "No result";
        try {
            result = post(HOST,requestJson,"POST");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private String convertPaymentResponse(String rawResponse, RequestHeader requestHeader, SpPaymentRequestData requestData) {
        SpPaymentResponseData spPaymentResponseData = new Gson().fromJson(rawResponse, SpPaymentResponseData.class);
        SpPaymentResponseResult resultData = spPaymentResponseData.getResult();

        ResponseGeneralHeader responseHeader = new ResponseGeneralHeader();
        responseHeader.setStatusCode(StatusManipulation.getCodeOfSpGateway(spPaymentResponseData.getStatus(), requestHeader.getServiceType()));
        responseHeader.setStatusDesc(spPaymentResponseData.getMessage());
        responseHeader.setMchId(resultData.getMchId());
        responseHeader.setResponseTime(convertMillisecondToDate(Long.toString(new Date().getTime())));
        responseHeader.setServiceType(requestHeader.getServiceType());
        responseHeader.setMethod(requestHeader.getMethod());

        IntegratedPaymentResponseData responseData = new IntegratedPaymentResponseData();
        responseData.setSysOrderNo(resultData.getSysOrderNo());
        responseData.setStoreOrderNo(resultData.getStoreOrderNo());
        responseData.setTotalFee(resultData.getTotalFee().toString());
        responseData.setFeeType("TWD");
        responseData.setDeviceInfo("DeviceMiles");
        responseData.setBody(requestData.getBody());
        responseData.setPlatformRsp(rawResponse);

        ResponseGeneralBody responseBody = new ResponseGeneralBody();
        responseBody.setHeader(responseHeader);
        responseBody.setData(responseData);

        return new Gson().toJson(responseBody);
    }

    private String convertRefundAndCaptureResponse(String rawResponse, RequestHeader requestHeader, SpRefundAndCaptureRequestData requestData) {
        SpRefundAndCaptureResponseData spRefundAndCaptureResponseData = new Gson().fromJson(rawResponse, SpRefundAndCaptureResponseData.class);
        SpRefundAndCaptureResponseResult resultData = spRefundAndCaptureResponseData.getResult();

        ResponseGeneralHeader responseHeader = new ResponseGeneralHeader();
        responseHeader.setStatusCode(StatusManipulation.getCodeOfSpGateway(spRefundAndCaptureResponseData.getStatus(), requestHeader.getServiceType()));
        responseHeader.setStatusDesc(spRefundAndCaptureResponseData.getMessage());
        responseHeader.setMchId(resultData.getMchId());
        responseHeader.setResponseTime(convertMillisecondToDate(Long.toString(new Date().getTime())));
        responseHeader.setServiceType(requestHeader.getServiceType());
        responseHeader.setMethod(requestHeader.getMethod());

        IntegratedPaymentResponseData responseData = new IntegratedPaymentResponseData();
        responseData.setSysOrderNo(resultData.getSysOrderNo());
        responseData.setStoreOrderNo(resultData.getStoreOrderNo());
        responseData.setTotalFee(resultData.getTotalFee().toString());
        responseData.setDeviceInfo("DeviceMiles");
        responseData.setPlatformRsp(rawResponse);

        ResponseGeneralBody responseBody = new ResponseGeneralBody();
        responseBody.setHeader(responseHeader);
        responseBody.setData(responseData);

        return new Gson().toJson(responseBody);
    }

    private String convertSingleQueryResponse(String rawResponse, RequestHeader requestHeader, SpSingleQueryRequestData requestData) {
        SpSingleQueryResponseData spSingleQueryResponseData = new Gson().fromJson(rawResponse, SpSingleQueryResponseData.class);
        SpSingleQueryResponseResult resultData = spSingleQueryResponseData.getResult();

        ResponseGeneralHeader responseHeader = new ResponseGeneralHeader();
        responseHeader.setStatusCode(StatusManipulation.getCodeOfSpGateway(spSingleQueryResponseData.getStatus(), requestHeader.getServiceType()));
        responseHeader.setStatusDesc(spSingleQueryResponseData.getMessage());
        responseHeader.setMchId(resultData.getMchId());
        responseHeader.setResponseTime(convertMillisecondToDate(Long.toString(new Date().getTime())));
        responseHeader.setServiceType(requestHeader.getServiceType());
        responseHeader.setMethod(requestHeader.getMethod());

        IntegratedPaymentResponseData responseData = new IntegratedPaymentResponseData();
        responseData.setSysOrderNo(resultData.getSysOrderNo());
        responseData.setStoreOrderNo(resultData.getStoreOrderNo());
        responseData.setTotalFee(resultData.getTotalFee().toString());
        responseData.setDeviceInfo("DeviceMiles");
        responseData.setFeeType("TWD");
        // todo: info of body neither in REQ nor in RES
        responseData.setBody("no body-info");
        responseData.setApproveCode(resultData.getAuthCode());
        responseData.setPlatformRsp(rawResponse);

        ResponseGeneralBody responseBody = new ResponseGeneralBody();
        responseBody.setHeader(responseHeader);
        responseBody.setData(responseData);

        return new Gson().toJson(responseBody);
    }

    private String getInst(String transMode, String installment) {
        String inst = "0";
        if("0".equals(transMode)) {
            inst = "0";
        } else if ("1".equals(transMode)) {
            inst = installment;
        } else if("2".equals(transMode)) {

        }
        return inst;
    }

    private String getIndexType(String storeOrderNo, String sysOrderNo) {
        String flag = "";

        if(Strings.isNullOrEmpty(storeOrderNo)) {
            flag = "2";
        }else if(Strings.isNullOrEmpty(storeOrderNo)) {
            flag = "1";
        }
        return flag;
    }

    private String getCloseType(String serviceType) {
        String str= "";

        if("Pay".equals(serviceType)) {
            str = "1";
        }else if("Refund".equals(serviceType)){
            str = "2";
        }
        return str;
    }

    private String calculateQueryCheckValue(String amt, String mchId, String mchOrderNo) throws Exception {

        String key = KEY;
        String iv = IV;
        String checkValue = "IV=" + iv + "&Amt=" + amt + "&MerchantID=" + mchId + "&MerchantOrderNo=" + mchOrderNo + "&Key=" + key;
        String digest = getDigestSHA256(checkValue).toUpperCase();

        return digest;
    }

    private String calculateCheckValue(String amt, String mchId, String mchOrderNo, String timeStamp, String ver) throws Exception {
        String key = KEY;
        String iv = IV;
        String checkValue = "HashKey=" + key + "&Amt=" + amt + "&MerchantID=" + mchId + "&MerchantOrderNo=" + mchOrderNo +
                "&TimeStamp=" + timeStamp + "&Version="+ ver +"&HashIV=" + iv;
        String digest = getDigestSHA256(checkValue).toUpperCase();

        return digest;
    }

    private String calculateCheckCode(String amt, String mchId, String mchOrderNo, String tradeNo) throws Exception {
        String key = KEY;
        String iv = IV;
        String checkValue = "HashIV=" + iv + "&Amt=" + amt + "&MerchantID=" + mchId + "&MerchantOrderNo=" + mchOrderNo + "&TradeNo=" + tradeNo+ "&HashKey=" + key;
        String digest = getDigestSHA256(checkValue).toUpperCase();

        return digest;
    }

    private String getDigestSHA256(String raw) throws Exception {

        MessageDigest sha = MessageDigest.getInstance("SHA-256");
        sha.update(raw.getBytes());
        return byte2Hex(sha.digest());
    }

    private String convertDateToMillisecond(String dateTime) {
        Date d = new Date();
        String pattern = SystemInstance.DATE_PATTERN;
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        try {
            d = sdf.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Long.toString(d.getTime());
    }

    private String convertMillisecondToDate(String dateTime) {
        Date d = new Date(Long.parseLong(dateTime));
        String pattern = SystemInstance.DATE_PATTERN;
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        String str = sdf.format(d);
        return str;
    }

    private String getSpEncryptedReq(String postData) throws Exception {
        SecretKey secretKey = AesCryptoUtil.convertSecretKey(KEY.getBytes());
        byte[] result = AesCryptoUtil.encrypt(secretKey, IV.getBytes(), postData);
        String after = byte2Hex(result);

        return after;
    }

    private String byte2Hex(byte[] inputBytes) {
        String result = "";
        for (byte currentByte : inputBytes) result += Integer.toString((currentByte & 0xff) + 0x100, 16).substring(1);
        return result;
    }

    private String getPaymentResponse() {
        String result = "{\"Status\":\"SUCCESS\",\"Message\":\"\\u6388\\u6b0a\\u6210\\u529f\","+
                "\"Result\":{\"MerchantID\":\"MS35199\",\"Amt\":1,\"MerchantOrderNo\":\"1457941786\","+
                "\"TradeNo\":\"16031415500981007\",\"CheckCode\":\"EA6FE8A9F29BB6746FCBC5402DED6526DAE5C9475D75AE5E48FFE8C1DFCFE5D7\","+
                "\"RespondCode\":\"00\",\"Auth\":\"930637\",\"AuthDate\":\"20160314\",\"AuthTime\":\"155010\","+
                "\"Card6No\":\"400022\",\"Card4No\":\"2222\",\"InstFirst\":1,\"InstEach\":0,\"Inst\":0,\"IP\":\"61.56.223.63\","+
                "\"EscrowBank\":\"KGI\",\"ECI\":\"\",\"Exp\":\"1806\"}}";
        return result;
    }

    private String getRefundAndCaptureResponse() {
        String result = "{\"Status\":\"TRA20011\",\"Message\":\"\\u6b64\\u8a02\\u55ae\\u5df2\\u7533\\u8acb\\u904e\\"+
                "u8acb\\u6b3e\\uff0c\\u4e0d\\u53ef\\u91cd\\u8986\\u8acb\\u6b3e\",\"Result\":{\"MerchantID\":\"11250\","+
                "\"Amt\":\"10\",\"MerchantOrderNo\":\"20140519193443\"}}";
        return result;
    }

    private String getSingleQueryResponse() {
        String result = "{\"Status\":\"SUCCESS\",\"Message\":\"\\u6388\\u6b0a\\u6210\\u529f\""+
                ",\"Result\":{\"MerchantID\":\"MS35199\",\"Amt\":1,\"MerchantOrderNo\":\"1457941786\","+
                "\"TradeNo\":\"16031415500981007\",\"TradeStatus\":\"1\",\"PaymentType\":\"CREDIT\","+
                "\"CreateTime\":\"2014-06-2516:43:49\",\"PayTime\":\"2014-06-2516:43:49\","+
                "\"CheckCode\":\"EA6FE8A9F29BB6746FCBC5402DED6526DAE5C9475D75AE5E48FFE8C1DFCFE5D7\","+
                "\"FundTime\":\"2014-06-25\",\"RespondCode\":\"00\",\"Auth\":\"930637\",\"ECI\":\"\","+
                "\"CloseAmt\":1,\"CloseStatus\":3,\"BackBalance\":\"1\",\"BackStatus\":2,\"RespondMsg\":\"good\","+
                "\"InstFirst\":1,\"InstEach\":0,\"Inst\":0,\"Bonus\":0,\"RedAmt\":0}}";
        return result;
    }
}
