package co.intella.service.impl;

import co.intella.model.INT_VW_IpassCheckOut;
import co.intella.service.IpassCheckOutService;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class IpassCheckOutServiceImpl implements IpassCheckOutService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<INT_VW_IpassCheckOut> findAll() {

        String sql = "select ip.* from allpaypass_new.INT_VW_IpassCheckOut ip";
        Query query = entityManager.createNativeQuery(sql);
        query.unwrap(SQLQuery.class)
                .setResultTransformer(Transformers.aliasToBean(INT_VW_IpassCheckOut.class));
        List<INT_VW_IpassCheckOut> resultList = query.getResultList();
        return resultList;
    }
}
