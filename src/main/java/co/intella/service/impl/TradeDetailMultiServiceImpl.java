package co.intella.service.impl;

import co.intella.crypto.AesCryptoUtil;
import co.intella.domain.integration.*;
import co.intella.model.RequestHeader;
import co.intella.net.Constant;
import co.intella.repository.TradeDetailMultiRepository;
import co.intella.request.TradeDetailMultiRequest;
import co.intella.service.TradeDetailMultiService;
import co.intella.service.validation.TradeDetailMultiValidationService;
import co.intella.utility.GeneralResponseMessageUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.*;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.crypto.SecretKey;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Service
public class TradeDetailMultiServiceImpl implements TradeDetailMultiService {

    @Resource
    private TradeDetailMultiRepository tradeDetailMultiRepository;

    @Resource
    private TradeDetailMultiValidationService tradeDetailMultiValidationService;

    @Resource
    private MessageSource messageSource;

    private Locale locale = new Locale("zh_TW");

    private Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Override
    public ResponseEntity<String> doService(String body, SecretKey secretKey,  RequestHeader requestHeader) throws Exception {
        TradeDetailMultiRequest request = prepareData(body, requestHeader);
        LOGGER.info("[TRADE_DETAIL_MULTI] prepareData request " + request);
        String validateResult = tradeDetailMultiValidationService.doValidate(request);
        if (StringUtils.isEmpty(validateResult)) {
            List<Map<String, Object>> result = doCustomService(request);
            String responseBodyString = getResponseBody(requestHeader, result);
            return encryptResponseBody(secretKey, responseBodyString);
        } else {
            LOGGER.info("[TRADE_DETAIL_MULTI] validateResult " + validateResult);
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "8002", validateResult);
        }
    }

    private TradeDetailMultiRequest prepareData(String body, RequestHeader requestHeader) {
        JsonObject jsonObject = new JsonParser().parse(body).getAsJsonObject();
        JsonPrimitive data = jsonObject.getAsJsonPrimitive("Data");
        JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
        TradeDetailMultiRequest request = new TradeDetailMultiRequest();
        request.setMchId(requestHeader.getMerchantId());
        if (requestData.has("StartDate")) {
            request.setStartDate(requestData.get("StartDate").getAsString());
        }
        if (requestData.has("EndDate")) {
            request.setEndDate(requestData.get("EndDate").getAsString());
        }
        if (requestData.has("AccountId")) {
            request.setAccountId(requestData.get("AccountId").getAsString());
        }
        if (requestData.has("StoreType")) {
            request.setStoreType(requestData.get("StoreType").getAsString());
        }
        request.setTradeKey(requestHeader.getTradeKey());
        request.setMchId(requestHeader.getMerchantId());
        return request;
    }

    private ResponseEntity<String> encryptResponseBody(SecretKey secretKey, String responseBodyString) throws Exception {
        byte[] encryptBytes = AesCryptoUtil.encrypt(secretKey, Constant.IV, responseBodyString);
        IntegratedResponse integratedResponse = new IntegratedResponse();
        integratedResponse.setResult(Base64.encode(encryptBytes));
        String encryptResponse = new Gson().toJson(integratedResponse);
        return new ResponseEntity<>(encryptResponse, HttpStatus.OK);
    }

    private String getResponseBody(RequestHeader requestHeader, List<Map<String, Object>> result) throws JsonProcessingException {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setStatusCode(SystemInstance.STATUS_SUCCESS);
        responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        responseGeneralHeader.setMethod("00000");
        responseGeneralHeader.setServiceType("OrderQuery1.1");
        responseGeneralHeader.setMchId(requestHeader.getMerchantId());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        IntegratedOrderQuery1ResponseData responseData = new IntegratedOrderQuery1ResponseData();
        responseData.setList(result);

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        responseGeneralBody.setHeader(responseGeneralHeader);
        responseGeneralBody.setData(responseData);
        return new GsonBuilder().serializeNulls().create().toJson(responseGeneralBody);
    }

    private List<Map<String, Object>> doCustomService(TradeDetailMultiRequest request) {
        if (StringUtils.isEmpty(request.getAccountId())) {
            List<Integer> merchantSeqIds = tradeDetailMultiRepository.getAccountIdsForListTradeDetail(request);
            request.setMerchantSeqIds(merchantSeqIds);
        }
        LOGGER.info("[TRADE_DETAIL_MULTI] request " + request);
        return tradeDetailMultiRepository.list(request);
    }
}
