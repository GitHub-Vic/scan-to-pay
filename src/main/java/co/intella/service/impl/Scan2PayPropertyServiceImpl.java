package co.intella.service.impl;

import co.intella.service.Scan2PayPropertyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Miles
 */
@Service
public class Scan2PayPropertyServiceImpl implements Scan2PayPropertyService {

    private final static Logger LOGGER = LoggerFactory.getLogger(Scan2PayPropertyServiceImpl.class);

    private Properties properties = null;

    @Value("${scan2pay.config.file.path}")
    private String CONFIG_FILE_PATH;

    public String get(String property) throws IOException {

        if(properties == null) {
            properties = new Properties();
            InputStream input = new FileInputStream(CONFIG_FILE_PATH);
            properties.load(input);
        }

        return properties.getProperty(property);
    }
}
