package co.intella.service.impl;

import co.intella.model.QRole;
import co.intella.model.Role;
import co.intella.repository.RoleRepository;
import co.intella.service.RoleService;
import com.google.common.collect.Lists;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Andy Lin
 */
@Service
@Transactional
public class RoleServiceImpl implements RoleService{

    @Resource
    private RoleRepository roleRepository;

//    public List<Role> getListByIsEnable(int isEnable) {
//        QRole qRole = QRole.role;
//        BooleanExpression predicate = qRole.isEnable.eq(isEnable)
//                .and(qRole.isPersonal.eq(0));
//        return Lists.newArrayList(roleRepository.findAll(predicate));
//    }

    public List<Role> getListByIsPersonal(int isPersonal) {
        QRole qRole = QRole.role;
        BooleanExpression predicate = qRole.isEnable.eq(1)
                .and(qRole.isPersonal.eq(isPersonal));
        return Lists.newArrayList(roleRepository.findAll(predicate));
    }

    public List<Role> getListAll() {
        return roleRepository.findAll();
    }

    public Role getOneById(Long id) {
        QRole qRole = QRole.role;
        BooleanExpression predicate = qRole.id.eq(id);
        return roleRepository.findOne(predicate);
    }

    public Role getOneByName(String name) {
        QRole qRole = QRole.role;
        BooleanExpression predicate = qRole.name.eq(name)
                .and(qRole.isEnable.eq(1));
        return roleRepository.findOne(predicate);
    }

    public Role save (Role role) {
        return roleRepository.save(role);
    }

    public void delete (Role role) {
        roleRepository.delete(role);
    }
}
