package co.intella.service.impl;

import co.intella.model.QTestUUID;
import co.intella.model.TestUUID;
import co.intella.repository.TestRepository;
import co.intella.service.TestService;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author Alex
 */
@Service
@Transactional
public class TestServiceImpl implements TestService {
    @Resource
    private TestRepository testRepository;

    public void save(TestUUID testUUID) {
        testRepository.save(testUUID);
    }

    public TestUUID getOne(String id) {
        QTestUUID qTestUUID = QTestUUID.testUUID;
        BooleanExpression predicate = qTestUUID.id.eq(id);
        return testRepository.findOne(predicate);
    }

    public Page findAll(Pageable pageable) {
        return null;
    }
}
