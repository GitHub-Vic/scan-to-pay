package co.intella.service.impl;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.Resource;
import javax.net.ssl.HttpsURLConnection;
import javax.transaction.Transactional;

import co.intella.utility.EncryptUtil;
import org.assertj.core.util.Strings;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import co.intella.domain.ecpay.ECPayCapture;
import co.intella.domain.ecpay.ECPayCaptureResponse;
import co.intella.domain.ecpay.ECPayDoAction;
import co.intella.domain.ecpay.ECPayDoActionResponse;
import co.intella.domain.ecpay.ECPayOrder;
import co.intella.domain.ecpay.ECPayOrderResponse;
import co.intella.domain.ecpay.ECPayQuery;
import co.intella.domain.ecpay.ECPayQueryResponse;
import co.intella.domain.integration.IntegratedCancelResponseData;
import co.intella.domain.integration.IntegratedCaptureResponseData;
import co.intella.domain.integration.IntegratedPaymentResponseData;
import co.intella.domain.integration.IntegratedSingleOrderQueryResponseData;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.model.Merchant;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import co.intella.service.AllpayService;
import co.intella.service.ECPayService;
import co.intella.service.MerchantService;
import co.intella.service.PaymentAccountService;
import co.intella.service.TradeDetailService;
import co.intella.service.TradeProcedureService;
import co.intella.utility.GeneralUtil;
import co.intella.utility.SystemInstance;

//import static co.intella.model.QPaymentAccount.paymentAccount;

/**
 * @author Miles
 */
@Service
@Transactional
public class ECPayServiceImpl implements ECPayService {

	private final Logger LOGGER = LoggerFactory.getLogger(ECPayServiceImpl.class);

	@Resource
	private AllpayService allpayService;

	@Resource
	private PaymentAccountService paymentAccountService;

	@Resource
	private TradeProcedureService tradeProcedureService;

	@Resource
	private MerchantService merchantService;
	
	@Autowired
	private Environment env;

	@Autowired
	private MessageSource messageSource;

	@Value("${api.url}")
	private String API_URL;

	private Locale locale = new Locale("zh_TW");

	private String queryTrade(String mchId, String platformCheckCode, TradeDetail tradeDetail) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("MerchantID", mchId);
		map.put("CreditRefundId", tradeDetail.getCreditOrderId());
		map.put("CreditAmount", Long.toString(tradeDetail.getPayment()));
		map.put("CreditCheckCode", platformCheckCode);

		try {
			map.put("CheckMacValue", getCheckMacValue(map));
			String urlPara = geturlform(map).toString();

			String result = ecpayPost(env.getProperty("host.request.ecpay.querytrade"), urlPara);
//            Map<String, String> resultMap = getMapFromStringData(result);

			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String doRequest(RequestHeader requestHeader, JsonPrimitive data, String mchId) throws Exception {
		String response = "";

		String integrateMchId = requestHeader.getMerchantId();
		long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
		PaymentAccount paymentAccount = paymentAccountService.getOne(integrateMchId, bankId);
		Merchant merchant = merchantService.getOne(integrateMchId);

		if ("Payment".equals(requestHeader.getServiceType())) {

			TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

			ECPayOrder ecPayOrder = new Gson().fromJson(data.getAsString(), ECPayOrder.class);
			ecPayOrder.setMerchantId(paymentAccount.getAccount());
			ecPayOrder.setMerchantTradeDate(ecpayConvertDateFormat(requestHeader.getCreateTime(), "to"));
			ecPayOrder.setPaymentType("aio");
			ecPayOrder.setChoosePayment("ALL");
			ecPayOrder.setReturnURL(env.getProperty("api.url") + "allpaypass/api/notify/ecpay");
			ecPayOrder.setEncryptType("1");
			ecPayOrder.setNeedExtraPaidInfo("");
			ecPayOrder.setPlatformId("");
			ecPayOrder.setInvoiceMark("");
			ecPayOrder.setHoldTradeAMT("");
			ecPayOrder.setCustomField1("");
			ecPayOrder.setCustomField2("");
			ecPayOrder.setCustomField3("");
			ecPayOrder.setCustomField4("");
			ecPayOrder.setExpireDate("");
			ecPayOrder.setPaymentInfoURL("");
			ecPayOrder.setDesc1("");
			ecPayOrder.setDesc2("");
			ecPayOrder.setDesc3("");
			ecPayOrder.setDesc4("");
			ecPayOrder.setStoreExpireDate("");
			ecPayOrder.setBindingCard("");
			ecPayOrder.setMerchantMemberId("");
			ECPayOrderResponse ecPayOrderResponse = createOrder(ecPayOrder);

			tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader,
					new Gson().toJson(ecPayOrderResponse), data);

			response = getIntegratedOrderResponse(ecPayOrderResponse,tradeDetail);
		} else if ("Capture".equals(requestHeader.getServiceType())) {
			ECPayCapture ecPayCapture = new Gson().fromJson(data.getAsString(), ECPayCapture.class);
			ecPayCapture.setMerchantId(mchId);
			ecPayCapture.setUserRefundAMT("0");
			ecPayCapture.setPlatformID("");

			ECPayCaptureResponse ecPayCaptureResponse = capture(ecPayCapture);
			response = getIntegratedCaptureResponse(ecPayCaptureResponse);
		} else if ("SingleQuery".equals(requestHeader.getServiceType())) {
			ECPayQuery ecPayQuery = new Gson().fromJson(data.getAsString(), ECPayQuery.class);
			ecPayQuery.setMerchantId(paymentAccount.getAccount());
			ecPayQuery.setTimeStamp(Long.toString(new Date().getTime()));
			ecPayQuery.setPlatformID("");

			ECPayQueryResponse ecPayQueryResponse = queryOrder(ecPayQuery);
			response = getIntegratedQueryResponse(ecPayQueryResponse);
		} else if ("Cancel".equals(requestHeader.getServiceType())) {

			tradeProcedureService.setTradeDetailRequest(requestHeader, data);

			ECPayDoAction ecPayDoAction = new Gson().fromJson(data.getAsString(), ECPayDoAction.class);
			TradeDetail tradeDetail = tradeDetailService.getOneByOrderIdAndMethod(ecPayDoAction.getMerchantTradeNo(),
					requestHeader.getMethod());

			ecPayDoAction.setMerchantId(paymentAccount.getAccount());
			ecPayDoAction.setAction(getAction(tradeDetail.getCreateDate()));
			ecPayDoAction.setTotalAmount(Long.toString(tradeDetail.getPayment()));
			ecPayDoAction.setPlatformID("");

			LOGGER.info("[ecpayACtion req] " + new Gson().toJson(ecPayDoAction));

			ECPayDoActionResponse ecPayDoActionResponse = doAction(ecPayDoAction);
			tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader,
					new Gson().toJson(ecPayDoActionResponse), data);
			response = getIntegratedCancelResponse(tradeDetail, merchant, ecPayDoActionResponse);
		}
		return response;
	}

	private String getAction(String createTime) throws ParseException {
		String action = "R";
		DateFormat df1 = new SimpleDateFormat("yyyyMMddhhmmss");
		String createTime20 = createTime.substring(0, 8) + "200000";
		String nowTime = DateTime.now().toString(SystemInstance.DATE_PATTERN);
		String nowTime20 = nowTime.substring(0, 8) + "200000";
		String yesterday20 = DateTime.now().minusDays(1).toString(SystemInstance.DATE_PATTERN).substring(0, 8)
				+ "200000";

		Date dcreateTime = df1.parse(createTime);
		Date dnowTime = df1.parse(nowTime);
		Date dnowTime20 = df1.parse(nowTime20);
		Date dcreateTime20 = df1.parse(createTime20);
		Date dyesterday20 = df1.parse(yesterday20);

		if (dnowTime.compareTo(dnowTime20) == 1 || dnowTime.compareTo(dnowTime20) == 0) {
			if (dcreateTime.compareTo(dnowTime20) == -1) {
				action = "R";
			} else {
				action = "N";
			}
		} else {
			if (dcreateTime.compareTo(dyesterday20) == -1) {
				action = "R";
			} else {
				action = "N";
			}
		}

		return action;
	}

	private String getIntegratedCancelResponse(TradeDetail tradeDetail, Merchant merchant,
			ECPayDoActionResponse ecPayDoActionResponse) {
		ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
		responseGeneralHeader.setMethod("21100");

		responseGeneralHeader.setServiceType("Cancel");
		responseGeneralHeader.setMchId(merchant.getAccountId());
		responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

		String statusCode = "";
		if ("1".equals(ecPayDoActionResponse.getRtnCode())) {
			statusCode = "0000";
			responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
		} else {
			statusCode = "7138";
			responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7138", null, locale));
		}
		responseGeneralHeader.setStatusCode(statusCode);

		IntegratedCancelResponseData integratedCancelResponseData = new IntegratedCancelResponseData();
		integratedCancelResponseData.setStoreOrderNo(tradeDetail.getOrderId());
		integratedCancelResponseData.setPlatformRsp(new Gson().toJson(ecPayDoActionResponse));

		ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
		responseGeneralBody.setHeader(responseGeneralHeader);
		responseGeneralBody.setData(integratedCancelResponseData);

		return new Gson().toJson(responseGeneralBody);
	}

	private String getIntegratedQueryResponse(ECPayQueryResponse ecPayQueryResponse) {
		ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
		responseGeneralHeader.setMethod("21100");

		responseGeneralHeader.setStatusDesc("");
		responseGeneralHeader.setServiceType("SingleQuery");
		responseGeneralHeader.setMchId(ecPayQueryResponse.getMerchantId());
		String statusCode = "";
		if ("0".equals(ecPayQueryResponse.getTradeStatus()) || "1".equals(ecPayQueryResponse.getTradeStatus())) {
			statusCode = "0000";
		} else {
			statusCode = "7002";
		}
		responseGeneralHeader.setStatusCode(statusCode);

		IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();
		integratedSingleOrderQueryResponseData.setStoreOrderNo(ecPayQueryResponse.getMerchantTradeNo());
//        integratedSingleOrderQueryResponseData.setSysOrderNo();
		integratedSingleOrderQueryResponseData
				.setEstablishedAt(ecpayConvertDateFormat(ecPayQueryResponse.getTradeDate(), "from"));
		integratedSingleOrderQueryResponseData.setSysOrderNo(ecPayQueryResponse.getTradeNo());
		integratedSingleOrderQueryResponseData.setFeeType("TWD");
		integratedSingleOrderQueryResponseData.setTotalFee(ecPayQueryResponse.getTradeAmt());
		integratedSingleOrderQueryResponseData.setDeviceInfo("DeviceInfo");
		integratedSingleOrderQueryResponseData.setStoreFeeRate(ecPayQueryResponse.getHandlingCharge());
		integratedSingleOrderQueryResponseData.setBody(ecPayQueryResponse.getItemName());

		ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
		responseGeneralBody.setHeader(responseGeneralHeader);
		responseGeneralBody.setData(integratedSingleOrderQueryResponseData);

		return new Gson().toJson(responseGeneralBody);
	}

	private String getIntegratedCaptureResponse(ECPayCaptureResponse ecPayCaptureResponse) {
		ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
		responseGeneralHeader.setMethod("21100");

		responseGeneralHeader.setStatusDesc(ecPayCaptureResponse.getReturnMessage());
		responseGeneralHeader.setServiceType("Capture");
		responseGeneralHeader.setMchId(ecPayCaptureResponse.getMerchantId());
		responseGeneralHeader.setStatusCode(ecPayCaptureResponse.getReturnCode() == 1 ? "0000" : "7002");

		IntegratedCaptureResponseData integratedCaptureResponseData = new IntegratedCaptureResponseData();
		integratedCaptureResponseData.setStoreOrderNo(ecPayCaptureResponse.getMerchantTradeNo());
		integratedCaptureResponseData.setCaptureDate(ecPayCaptureResponse.getAllocationDate());
		integratedCaptureResponseData.setSystemOrderNo(ecPayCaptureResponse.getTradeNo());

		ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
		responseGeneralBody.setHeader(responseGeneralHeader);
		responseGeneralBody.setData(integratedCaptureResponseData);

		return new Gson().toJson(responseGeneralBody);
	}

	private String getIntegratedOrderResponse(ECPayOrderResponse ecPayOrderResponse,TradeDetail tradeDetail) {
		ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
		responseGeneralHeader.setMethod("21100");

		responseGeneralHeader.setStatusDesc(ecPayOrderResponse.getReturnMessage());
		responseGeneralHeader.setServiceType("Payment");
		responseGeneralHeader.setMchId(ecPayOrderResponse.getMerchantId());
		responseGeneralHeader.setStatusCode(ecPayOrderResponse.getReturnCode() == 1 ? "0000" : "7001");
		responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

		IntegratedPaymentResponseData integratedPaymentResponseData = new IntegratedPaymentResponseData();
		integratedPaymentResponseData.setStoreOrderNo(ecPayOrderResponse.getMerchantTradeNo());
		integratedPaymentResponseData.setSysOrderNo("");
		integratedPaymentResponseData.setToken(ecPayOrderResponse.getSpToken());
		integratedPaymentResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
		integratedPaymentResponseData.setFeeType("TWD");
		integratedPaymentResponseData.setDeviceInfo("DeviceMiles");
		integratedPaymentResponseData.setBody("");
		integratedPaymentResponseData.setPlatformRsp(String.format("%sallpaypass/api/payment/order/result?orderId=%s", API_URL, EncryptUtil.encryptSimple(tradeDetail.getOrderId())));
		integratedPaymentResponseData.setSerialNumber(0);


		ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
		responseGeneralBody.setHeader(responseGeneralHeader);
		responseGeneralBody.setData(integratedPaymentResponseData);

		return new Gson().toJson(responseGeneralBody);
	}

	public ECPayOrderResponse createOrder(ECPayOrder ecPayOrder) {
		String requestJson = new Gson().toJson(ecPayOrder);
		LOGGER.info("[TRANSACTION] createOrder. " + requestJson);

		Map<String, String> map = new HashMap<String, String>();
		map.put("MerchantID", ecPayOrder.getMerchantId());
		map.put("MerchantTradeNo", ecPayOrder.getMerchantTradeNo());
		map.put("StoreID", ecPayOrder.getStoreId());
		map.put("MerchantTradeDate", ecPayOrder.getMerchantTradeDate());
		map.put("PaymentType", ecPayOrder.getPaymentType());
		map.put("TotalAmount", ecPayOrder.getTotalAmount());
		map.put("TradeDesc", ecPayOrder.getTradeDesc());
		map.put("ItemName", ecPayOrder.getItemName());
		map.put("ReturnURL", ecPayOrder.getReturnURL());
		map.put("ChoosePayment", ecPayOrder.getChoosePayment());
		map.put("NeedExtraPaidInfo", ecPayOrder.getNeedExtraPaidInfo());
		map.put("PlatformID", ecPayOrder.getPlatformId());
		map.put("InvoiceMark", ecPayOrder.getInvoiceMark());
		map.put("HoldTradeAMT", ecPayOrder.getHoldTradeAMT());
		map.put("CustomField1", ecPayOrder.getCustomField1());
		map.put("CustomField2", ecPayOrder.getCustomField2());
		map.put("CustomField3", ecPayOrder.getCustomField3());
		map.put("CustomField4", ecPayOrder.getCustomField4());
		map.put("EncryptType", ecPayOrder.getEncryptType());

		try {
			map.put("CheckMacValue", getCheckMacValue(map));
			String urlPara = geturlform(map).toString();

			// String result = ecpayPost(HOST,formurl);
			String result = ecpayPost(env.getProperty("host.request.ecpay.createtrade"), urlPara);
			LOGGER.info("[ECPay][Response]" + result);
			return new Gson().fromJson(result, ECPayOrderResponse.class);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Resource
	private TradeDetailService tradeDetailService;

	public String queryTransaction(String orderId) {
		TradeDetail tradeDetail = tradeDetailService.getOneByOrderIdAndMethod(orderId, "21100");
		JsonObject jsonObject = new JsonObject();

		if (tradeDetail == null) {
			jsonObject.addProperty("status", "7002");
			jsonObject.addProperty("date", "");
			jsonObject.addProperty("amount", "0");
			jsonObject.addProperty("MerchantId", "");
			jsonObject.addProperty("MerchantTradeNo", "");
			jsonObject.addProperty("itemName", "");
		} else {
			String description;
			if (tradeDetail.getDescription() == null) {
				description = "emptyItem";
			} else {
				description = tradeDetail.getDescription();
			}
			jsonObject.addProperty("date", tradeDetail.getCreateDate());
			jsonObject.addProperty("amount", tradeDetail.getPayment());
			jsonObject.addProperty("MerchantId", tradeDetail.getPaymentAccount().getAccount());
			jsonObject.addProperty("MerchantTradeNo", tradeDetail.getSystemOrderId());
			jsonObject.addProperty("itemName", description);
			if ("Trade success".equals(tradeDetail.getStatus())) {
				jsonObject.addProperty("status", "0000");
			} else { // Status "Creating order" trans to "Trade fail", since the situation of the
						// transaction shut-down
				jsonObject.addProperty("status", "9998");
				tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
				tradeDetailService.save(tradeDetail);
			}
		}
		LOGGER.info("sql finished" + new Gson().toJson(jsonObject));
		return new Gson().toJson(jsonObject);
	}

	public ModelAndView doTransaction(String merchantId, String spToken) {

		if ("".equals(merchantId)) { // http_method: GET, run here
			TradeDetail tradeDetail = tradeDetailService.getOneByTradeToken("21100", spToken);
			merchantId = paymentAccountService.getOne(tradeDetail.getAccountId(), 10).getAccount();
		}

		ModelMap modelMap = new ModelMap();
//        if("1".equals(Integer.toString(ecPayOrderResponse.getReturnCode()))) {
		modelMap.addAttribute("domain", env.getProperty("php.url"));
		modelMap.addAttribute("payURL", env.getProperty("host.request.ecpay.dotransaction"));
		modelMap.addAttribute("merchantID", merchantId);
		modelMap.addAttribute("spToken", spToken);
		modelMap.addAttribute("paymentType", "CREDIT");
		modelMap.addAttribute("paymentName", unicodeToChinese("\\u4fe1\\u7528\\u5361"));
//        }else {
//            //returnValue not equals "1", form the error-page
//        }
		LOGGER.info("Data to ModelAndView : " + modelMap.toString());
		return new ModelAndView("payPayment", modelMap);
	}

	public ECPayQueryResponse queryOrder(ECPayQuery ecPayQuery) {
		String requestJson = new Gson().toJson(ecPayQuery);
		LOGGER.info("[TRANSACTION] QueryOrder. " + requestJson);

		Map<String, String> map = new HashMap<String, String>();
		map.put("MerchantID", ecPayQuery.getMerchantId());
		map.put("MerchantTradeNo", ecPayQuery.getMerchantTradeNo());
		map.put("TimeStamp", ecPayQuery.getTimeStamp());
		map.put("PlatformID", ecPayQuery.getPlatformID());

		try {
			map.put("CheckMacValue", getCheckMacValue(map));
			String urlPara = geturlform(map).toString();

			String result = ecpayPost(env.getProperty("host.request.ecpay.queryorder"), urlPara);
			Map<String, String> resultMap = getMapFromStringData(result);

			ECPayQueryResponse ecPayQueryResponse = reorganizeQueryResponse(resultMap);

			String checkMacValue = resultMap.remove("CheckMacValue");
			String calculatedCheckMacValue = getCheckMacValue(resultMap);
//            if(checkMacValue.equals(calculatedCheckMacValue)) {
//                LOGGER.info("[ECPay] Verifying the Message in Query...Success");
			return ecPayQueryResponse;
//            }else {
//                LOGGER.info("[ECPay] Verifying the Message in Query...fail");
//                return ecPayQueryResponse;
//            }
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public ECPayDoActionResponse doAction(ECPayDoAction ecPayDoAction) {
		// todo: url Not provided for test - refund, cancel, close, abandon
		LOGGER.info("[TRANSACTION] [ECPAY-doAction]" + new Gson().toJson(ecPayDoAction));

		Map<String, String> map = new HashMap<String, String>();
		map.put("MerchantID", ecPayDoAction.getMerchantId());
		map.put("MerchantTradeNo", ecPayDoAction.getMerchantTradeNo());
		map.put("Action", ecPayDoAction.getAction()); // C,R,E,N
		map.put("PlatformID", ecPayDoAction.getPlatformID());
		map.put("TotalAmount", ecPayDoAction.getTotalAmount());
		map.put("TradeNo", ecPayDoAction.getTradeNo());

		try {
			map.put("CheckMacValue", getCheckMacValue(map));
			String urlPara = geturlform(map).toString();
			ECPayDoActionResponse ecPayDoActionResponse;

			String result = ecpayPost(env.getProperty("host.request.ecpay.doaction"), urlPara);
			LOGGER.info("[ECPAY] refund response: " + result);

			Map<String, String> resultMap = getMapFromStringData(result);

			ecPayDoActionResponse = reorganizeDoActionResponse(resultMap);

			LOGGER.info(new Gson().toJson(ecPayDoActionResponse));

			return ecPayDoActionResponse;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private ECPayDoActionResponse reorganizeDoActionResponse(Map<String, String> map) {
		ECPayDoActionResponse ecPayDoActionResponse = new ECPayDoActionResponse();
		ecPayDoActionResponse.setMerchantId(map.get("Merchant"));
		ecPayDoActionResponse.setMerchantTradeNo(map.get("MerchantTradeNo"));
		ecPayDoActionResponse.setRtnCode(map.get("RtnCode"));
		ecPayDoActionResponse.setRtnMsg(map.get("RtnMsg"));
		ecPayDoActionResponse.setTradeNo(map.get("TradeNo"));

		return ecPayDoActionResponse;
	}

	private ECPayQueryResponse reorganizeQueryResponse(Map<String, String> map) {
		ECPayQueryResponse ecPayQueryResponse = new ECPayQueryResponse();
		ecPayQueryResponse.setMerchantId(map.get("MerchantID"));
		ecPayQueryResponse.setHandlingCharge(map.get("HandlingCharge"));
		ecPayQueryResponse.setItemName(map.get("ItemName"));
		ecPayQueryResponse.setMerchantTradeNo(map.get("MerchantTradeNo"));
		ecPayQueryResponse.setPaymentType(map.get("PaymentType"));
		ecPayQueryResponse.setPaymentDate(map.get("PaymentDate"));
		ecPayQueryResponse.setPaymentTypeChargeFee(map.get("PaymentTypeChargeFee"));
		ecPayQueryResponse.setTradeAmt(map.get("TradeAmt"));
		ecPayQueryResponse.setTradeStatus(map.get("TradeStatus"));
		ecPayQueryResponse.setTradeNo(map.get("TradeNo"));
		ecPayQueryResponse.setTradeDate(map.get("TradeDate"));
		ecPayQueryResponse.setStoreID(map.get("StoreID"));
		ecPayQueryResponse.setCustomField1(map.get("CustomField1"));
		ecPayQueryResponse.setCustomField2(map.get("CustomField2"));
		ecPayQueryResponse.setCustomField3(map.get("CustomField3"));
		ecPayQueryResponse.setCustomField4(map.get("CustomField4"));
		ecPayQueryResponse.setCheckMacValue(map.get("CheckMacValue"));

		return ecPayQueryResponse;
	}

	public ECPayDoActionResponse cancelOrder(ECPayDoAction ecPayDoAction) {
		String requestJson = new Gson().toJson(ecPayDoAction);
		LOGGER.info("[TRANSACTION] cancelOrder. " + requestJson);

		Map<String, String> map = new HashMap<String, String>();

		return null;
	}

	public ECPayCaptureResponse capture(ECPayCapture ecPayCapture) {
		String requestJson = new Gson().toJson(ecPayCapture);
		LOGGER.info("[CAPTURE] ECPay:  " + requestJson);

		Map<String, String> map = new HashMap<String, String>();
		map.put("MerchantID", ecPayCapture.getMerchantId());
		map.put("MerchantTradeNo", ecPayCapture.getMerchantTradeNo());
		map.put("CaptureAMT", ecPayCapture.getCaptureAMT());
		map.put("UserRefundAMT", ecPayCapture.getUserRefundAMT());
		map.put("PlatformID", ecPayCapture.getPlatformID());
		map.put("Remark", ecPayCapture.getRemark());

		try {
			map.put("CheckMacValue", getCheckMacValue(map));
			String urlPara = geturlform(map).toString();

			// String result = ecpayPost(HOST,formurl);
			String result = ecpayPost(env.getProperty("host.request.ecpay.capture"), urlPara);
			Map<String, String> resultMap = getMapFromStringData(result);
			ECPayCaptureResponse ecPayCaptureResponse = reorganizeCaptureResponse(resultMap);

			LOGGER.info("[Capture] ECPay Response: " + ecPayCaptureResponse);
			return ecPayCaptureResponse;
//            String checkMacValue = map.remove("CheckMacValue");
//            String calculatedCheckMacValue = getCheckMacValue(map);
//            if(checkMacValue.equals(calculatedCheckMacValue)) {
//                LOGGER.info("[ECPay] Verifying the Message in Capture...Success");
//                return ecPayCaptureResponse;
//            }else {
//                LOGGER.info("[ECPay] Verifying the Message in Capture...fail");
//                return new ECPayCaptureResponse();
//            }
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private ECPayCaptureResponse reorganizeCaptureResponse(Map<String, String> map) {
		ECPayCaptureResponse ecPayCaptureResponse = new ECPayCaptureResponse();

		ecPayCaptureResponse.setMerchantId(map.get("MerchantID"));
		ecPayCaptureResponse.setMerchantTradeNo(map.get("MerchantTradeNo"));
		ecPayCaptureResponse.setTradeNo(map.get("TradeNo"));
		ecPayCaptureResponse.setReturnCode(Integer.parseInt(map.get("RtnCode")));
		ecPayCaptureResponse.setReturnMessage(map.get("RtnMsg"));
		ecPayCaptureResponse.setAllocationDate(map.get("AllocationDate"));

		return ecPayCaptureResponse;
	}

	public Map<String, String> verifyNotification(String response) throws Exception {

		LOGGER.info("[ECPay] Verifying the Message in Notification..." + response);
		Map<String, String> returnData = new HashMap<String, String>();
		Map<String, String> map = getMapFromStringData(response);

		String checkMacValue = map.remove("CheckMacValue");
		String calculatedCheckMacValue = getCheckMacValue(map);
		if (checkMacValue.equals(calculatedCheckMacValue)) {
			LOGGER.info("[ECPay] Verifying the Message in Notification...Success");
			returnData.put("returnToECpay", "1|OK");

			long bankId = Long.valueOf(env.getProperty("bank.id.21100"));
			TradeDetail tradeDetail = tradeDetailService.getOneByOrderIdAndMethod(map.get("MerchantTradeNo"), "21100");

			if (tradeDetail != null) {
				if ("1".equals(map.get("RtnCode"))) {
					tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
					tradeDetail.setSystemOrderId(map.get("TradeNo"));
//                    if(map.get("gwsr") != null) {
//                        tradeDetail.setCreditOrderId(map.get("gwsr"));
//                    }
					tradeDetailService.save(tradeDetail);

					LOGGER.info("[ECPay] Trade-status is Updated.");
					returnData.put("RtnCode", "1");
					returnData.put("accountId", tradeDetail.getAccountId());
					returnData.put("orderId", tradeDetail.getOrderId());

				} else {
					returnData.put("RtnCode", "0");
					// Do not Send the goods.
				}
			} else {
				LOGGER.info("[ECPay] Cannot find this order in DB :" + map.get("MerchantTradeNo"));
			}
		} else {
			LOGGER.info("[ECPay] Verifying the Message in Notification...fail");
			returnData.put("returnToECpay", "0");
		}
		return returnData;
	}

	private Map<String, String> getMapFromStringData(String result) {
		Map<String, String> map = new HashMap<String, String>();

		String[] temp = result.split("&");
		for (int i = 0; i < temp.length; i++) {
			if (!"".equals(temp[i]) && !temp[i].matches("(.*)=(.*)") && i > 0) {
				temp[i - 1] += " " + temp[i];
			}
		}
		for (String element : temp) {
			if (element.matches("(.*)=(.*)")) {
				String[] temp2 = element.split("=");
				String value = "";
				if (temp2.length == 2) {
					value = temp2[1];
				}
				try {
					map.put(temp2[0], URLDecoder.decode(value, "UTF-8"));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
		}
		return map;
	}

	public String getCheckMacValue(Map<String, String> map) throws Exception {
		StringBuilder checkStr = new StringBuilder();
		Map<String, String> treeMap = new TreeMap<String, String>(map);

		for (Map.Entry<String, String> entry : treeMap.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();

			checkStr.append(key).append("=").append(value).append("&");
		}
		checkStr = new StringBuilder("HashKey=" + env.getProperty("ecpay.hashkey") + "&" + checkStr + "HashIV="
				+ env.getProperty("ecpay.hashIV"));
		checkStr = new StringBuilder(URLEncoder.encode(checkStr.toString(), "UTF-8").toLowerCase());
		return GeneralUtil.getHash(checkStr.toString()).toUpperCase();
	}

	private String ecpayConvertDateFormat(String dateTime, String way) {
		String pattern1 = "";
		String pattern2 = "";
		if ("to".equals(way)) {
			pattern1 = SystemInstance.DATE_PATTERN;
			pattern2 = "yyyy/MM/dd hh:mm:ss";
		} else {
			pattern1 = "yyyy/MM/dd hh:mm:ss";
			pattern2 = SystemInstance.DATE_PATTERN;
		}

		SimpleDateFormat sdf1 = new SimpleDateFormat(pattern1);
		SimpleDateFormat sdf2 = new SimpleDateFormat(pattern2);
		String str = "";
		try {
			Date d = sdf1.parse(dateTime);
			str = sdf2.format(d);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return str;
	}

	private StringBuilder geturlform(Map<String, String> map) throws Exception {
		StringBuilder checkStr = new StringBuilder();

		for (Map.Entry<String, String> entry : map.entrySet()) {
			String key = entry.getKey();
			String value = "";
			if (!Strings.isNullOrEmpty(entry.getValue())) {
				value = URLEncoder.encode(entry.getValue(), "UTF-8");
			}
			checkStr.append(key).append("=").append(value).append("&");
		}
		checkStr = checkStr.delete(checkStr.length() - 1, checkStr.length());
		return checkStr;
	}

	private String unicodeToChinese(String string) {
		String str = string.replace("\\u", ",");
		String[] s2 = str.split(",");
		String s1 = "";
		for (int i = 1; i < s2.length; i++) {
			s1 = s1 + (char) Integer.parseInt(s2[i], 16);
		}
		return s1;
	}

	private static String ecpayPost(String url, String query) throws Exception {
		URL requestUrl = new URL(url);
		HttpURLConnection con = (HttpsURLConnection) requestUrl.openConnection();
		con.setRequestMethod("POST");

		con.setRequestProperty("Content-length", String.valueOf(query.length()));
		con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		con.setDoOutput(true);
		con.setDoInput(true);

		DataOutputStream output = new DataOutputStream(con.getOutputStream());
		output.writeBytes(query);
		output.close();
		DataInputStream input = new DataInputStream(con.getInputStream());

		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(input));
		String result;

		StringBuilder buffer = new StringBuilder();
		while ((result = bufferedReader.readLine()) != null) {
			buffer.append(result);
		}

		input.close();

		return buffer.toString();
	}

}
