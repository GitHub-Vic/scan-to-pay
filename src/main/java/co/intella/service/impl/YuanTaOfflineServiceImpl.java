package co.intella.service.impl;

import co.intella.crypto.KeyReader;
import co.intella.domain.integration.*;
import co.intella.domain.yuantaoffline.*;
import co.intella.exception.InvalidDataFormatException;
import co.intella.exception.ServiceTypeNotSupportException;
import co.intella.model.*;
import co.intella.service.*;
import co.intella.utility.SystemInstance;
import co.intella.utility.YuanTaOfflineUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.paddings.PKCS7Padding;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import sun.net.www.protocol.https.Handler;

import javax.annotation.Resource;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.*;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

import static java.nio.charset.StandardCharsets.UTF_8;

@Service
public class YuanTaOfflineServiceImpl implements YuanTaOfflineService {

    private final Logger LOGGER = LoggerFactory.getLogger(YuanTaOfflineServiceImpl.class);

    @Autowired
    private Environment env;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private MerchantService merchantService;

    @Autowired
    private MessageSource messageSource;

    @Resource
    private DeviceService deviceService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Value("${host.request.yuantaoffline}")
    private String YUANTAOFFLINE_URL;

    @Value("${api.url}")
    private String API_URL;

    @Value("${yuantaoffline.rsa.public.key}")
    private String YUANTAOFFLINE_RSA_PUBLIC_KEY;

    @Value("${yuantaoffline.aes.iv}")
    private String YUANTAOFFLINE_AES_IV;

    @Value("${yuantaoffline.rsa.public.key.version}")
    private String BANK_KEY_VERSION;

    @Resource
    private RefundDetailService refundDetailService;

    @Override
    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {
        LOGGER.info("[YuanTaOffline][doRequest]");
        String generalResponseData = SystemInstance.EMPTY_STRING;
        JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        PaymentAccount paymentAccount = paymentAccountService.getOne(integrateMchId, bankId);
        Merchant merchant = merchantService.getOne(integrateMchId);
        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

        String result;
        String request;
        //todo: switch to choose corresponding service
        if (SystemInstance.TYPE_MICROPAY.equals(requestHeader.getServiceType())) {
            LOGGER.info("[YuanTaOffline][doRequest][MICROPAY]");
            YuanTaOfflinePaymentRequestDataObject yuanTaOfflinePaymentRequestDataObject = new YuanTaOfflinePaymentRequestDataObject();
            yuanTaOfflinePaymentRequestDataObject.setStoreID(paymentAccount.getAccount());
            yuanTaOfflinePaymentRequestDataObject.setTermID(paymentAccount.getAccountTerminalId());
            yuanTaOfflinePaymentRequestDataObject.setTimeout(YuanTaOfflineRequestBody.TIMEOUT);
            yuanTaOfflinePaymentRequestDataObject.setBuyerID(tradeDetail.getBarcode());
            yuanTaOfflinePaymentRequestDataObject.setOrderNo(tradeDetail.getOrderId());
            yuanTaOfflinePaymentRequestDataObject.setOrderCurrency(SystemInstance.CURRENCY_TWD);
            yuanTaOfflinePaymentRequestDataObject.setOrderAmount(Long.toString(tradeDetail.getPayment()));
            yuanTaOfflinePaymentRequestDataObject.setOrderDT(DateTime.now().toString(SystemInstance.DATE_PATTERN));
            yuanTaOfflinePaymentRequestDataObject.setOrderTitle(tradeDetail.getDescription());//再確認
//            yuanTaOfflinePaymentRequestDataObject.setOrderDesc("");//non-required field
//            yuanTaOfflinePaymentRequestDataObject.setProductQuantity("1");//non-required field
//            yuanTaOfflinePaymentRequestDataObject.setOrderExtraInfo(new OrderExtraInfo());//non-required field
//            yuanTaOfflinePaymentRequestDataObject.setOrderLongitude("121.32");//non-required field
//            yuanTaOfflinePaymentRequestDataObject.setOrderLatitude("25.02");//non-required field
//            yuanTaOfflinePaymentRequestDataObject.setProductInfo("");//non-required field
//            yuanTaOfflinePaymentRequestDataObject.setBuyerWallet("");//non-required field
//            yuanTaOfflinePaymentRequestDataObject.setBuyerPaymentType("0");//non-required field 1、2、3
//            yuanTaOfflinePaymentRequestDataObject.setBuyerTransferType("0");//non-required field 1、2、3、4、5
//            yuanTaOfflinePaymentRequestDataObject.setCoupons("");//non-required field
            request = new Gson().toJson(yuanTaOfflinePaymentRequestDataObject);
            LOGGER.info(request);
            if (Strings.isNullOrEmpty(integrateMchId) || Strings.isNullOrEmpty(yuanTaOfflinePaymentRequestDataObject.getTermID()) || Strings.isNullOrEmpty(yuanTaOfflinePaymentRequestDataObject.getBuyerID())
                    || Strings.isNullOrEmpty(yuanTaOfflinePaymentRequestDataObject.getTimeout()) || Strings.isNullOrEmpty(yuanTaOfflinePaymentRequestDataObject.getOrderNo()) || Strings.isNullOrEmpty(yuanTaOfflinePaymentRequestDataObject.getOrderCurrency())
                    || Strings.isNullOrEmpty(yuanTaOfflinePaymentRequestDataObject.getOrderAmount()) || Strings.isNullOrEmpty(yuanTaOfflinePaymentRequestDataObject.getOrderDT()) || Strings.isNullOrEmpty(yuanTaOfflinePaymentRequestDataObject.getOrderTitle())) {
                throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
            }
            //todo try catch但要注意如果YuanTaOfflinePaymentResponseDataObject 和 YuanTaOfflineQueryResponseDataObject不同
            try {
                result = postRequest("POST", request, YuanTaOfflineRequestBody.PAYMENT);
            } catch (Exception e) {
                YuanTaOfflineQueryRequestDataObject yuanTaOfflineQueryRequestDataObject = new YuanTaOfflineQueryRequestDataObject();
                yuanTaOfflineQueryRequestDataObject.setStoreID(paymentAccount.getAccount());
                yuanTaOfflineQueryRequestDataObject.setTermID(paymentAccount.getAccountTerminalId());
                yuanTaOfflineQueryRequestDataObject.setTimeout(YuanTaOfflineRequestBody.TIMEOUT);
                yuanTaOfflineQueryRequestDataObject.setOrderNo(tradeDetail.getOrderId());
                yuanTaOfflineQueryRequestDataObject.setOrderDT(DateTime.now().toString(SystemInstance.DATE_PATTERN));

                request = new Gson().toJson(yuanTaOfflineQueryRequestDataObject);
                if (Strings.isNullOrEmpty(integrateMchId) || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getTermID())
                        || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getTimeout()) || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getOrderNo())
                        || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getOrderDT())) {
                    throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
                }
                result = postRequest("POST", request, YuanTaOfflineRequestBody.QUERY);
            }
            LOGGER.info("result: " + result);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);
            YuanTaOfflineResponse yuanTaOfflineResponse = new Gson().fromJson(result, YuanTaOfflineResponse.class);
            YuanTaOfflinePaymentResponseDataObject yuanTaOfflinePaymentResponseDataObject = new Gson().fromJson(yuanTaOfflineResponse.getSecureData(), YuanTaOfflinePaymentResponseDataObject.class);

            ResponseGeneralHeader responseGeneralHeader = new YuanTaOfflineUtil().getYuanTaOfflineResponseHeader(
                    yuanTaOfflineResponse, requestHeader, messageSource);
            IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();

            if ("S".equals(yuanTaOfflineResponse.getStatus())) {
                integratedMicropayResponseData.setSysOrderNo(yuanTaOfflinePaymentResponseDataObject.getBankOrderNo());
                integratedMicropayResponseData.setTotalFee(yuanTaOfflinePaymentResponseDataObject.getOrderAmount());
            } else {
                integratedMicropayResponseData.setSysOrderNo(SystemInstance.EMPTY_STRING);
                integratedMicropayResponseData.setTotalFee(SystemInstance.EMPTY_STRING);
            }
            integratedMicropayResponseData.setStoreOrderNo(yuanTaOfflinePaymentResponseDataObject.getOrderNo());
            integratedMicropayResponseData.setAuthCode(yuanTaOfflinePaymentResponseDataObject.getBuyerID());

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedMicropayResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);
            LOGGER.info("integrate response ready :" + generalResponseData);

        } else if ("SingleOrderQuery".equals(requestHeader.getServiceType())) {
            LOGGER.info("[YuanTaOffline][doRequest][SingleOrderQuery][data]" + data.getAsString());
            YuanTaOfflineQueryRequestDataObject yuanTaOfflineQueryRequestDataObject = new YuanTaOfflineQueryRequestDataObject();
            yuanTaOfflineQueryRequestDataObject.setStoreID(paymentAccount.getAccount());
            yuanTaOfflineQueryRequestDataObject.setTermID(paymentAccount.getAccountTerminalId());
            yuanTaOfflineQueryRequestDataObject.setTimeout(YuanTaOfflineRequestBody.TIMEOUT);
            yuanTaOfflineQueryRequestDataObject.setOrderNo(tradeDetail.getOrderId());
            yuanTaOfflineQueryRequestDataObject.setOrderDT(DateTime.now().toString(SystemInstance.DATE_PATTERN));

            request = new Gson().toJson(yuanTaOfflineQueryRequestDataObject);
            if (Strings.isNullOrEmpty(integrateMchId) || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getTermID())
                    || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getTimeout()) || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getOrderNo())
                    || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getOrderDT())) {
                throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
            }
            try {
                result = postRequest("POST", request, YuanTaOfflineRequestBody.QUERY);
            } catch (Exception e) {
                yuanTaOfflineQueryRequestDataObject.setStoreID(paymentAccount.getAccount());
                yuanTaOfflineQueryRequestDataObject.setTermID(paymentAccount.getAccountTerminalId());
                yuanTaOfflineQueryRequestDataObject.setTimeout(YuanTaOfflineRequestBody.TIMEOUT);
                yuanTaOfflineQueryRequestDataObject.setOrderNo(tradeDetail.getOrderId());
                yuanTaOfflineQueryRequestDataObject.setOrderDT(DateTime.now().toString(SystemInstance.DATE_PATTERN));

                request = new Gson().toJson(yuanTaOfflineQueryRequestDataObject);
                if (Strings.isNullOrEmpty(integrateMchId) || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getTermID())
                        || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getTimeout()) || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getOrderNo())
                        || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getOrderDT())) {
                    throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
                }
                result = postRequest("POST", request, YuanTaOfflineRequestBody.QUERY);
            }
            LOGGER.info("result: " + result);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);
            YuanTaOfflineResponse yuanTaOfflineResponse = new Gson().fromJson(result, YuanTaOfflineResponse.class);
            YuanTaOfflineQueryResponseDataObject yuanTaOfflineQueryResponseDataObject = new Gson().fromJson(yuanTaOfflineResponse.getSecureData(), YuanTaOfflineQueryResponseDataObject.class);

            ResponseGeneralHeader responseGeneralHeader = new YuanTaOfflineUtil().getYuanTaOfflineResponseHeader(
                    yuanTaOfflineResponse, requestHeader, messageSource);
            IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();

            tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());
            integratedSingleOrderQueryResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
            integratedSingleOrderQueryResponseData.setStoreOrderNo(tradeDetail.getOrderId());
            integratedSingleOrderQueryResponseData.setTotalFee(Long.toString(tradeDetail.getPayment()));
            integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());
            integratedSingleOrderQueryResponseData.setPaidAt(tradeDetail.getCreateDate());
            integratedSingleOrderQueryResponseData.setRefundedMsg(tradeDetail.getRefundStatus());
            RefundDetail refundDetail = refundDetailService.getOneByOrderIdAndMethod(tradeDetail.getOrderId(), tradeDetail.getMethod());
            if (Objects.nonNull(refundDetail)) {
                integratedSingleOrderQueryResponseData.setRefundedAt(refundDetail.getCreateTime());
            }
            if ("F".equals(yuanTaOfflineResponse.getStatus())) {

            } else {
                integratedSingleOrderQueryResponseData.setStoreOrderNo(yuanTaOfflineQueryResponseDataObject.getOrderNo());
                integratedSingleOrderQueryResponseData.setSysOrderNo(yuanTaOfflineQueryResponseDataObject.getBankOrderNo());
                integratedSingleOrderQueryResponseData.setFeeType(yuanTaOfflineQueryResponseDataObject.getOrderAmount());
                integratedSingleOrderQueryResponseData.setDeviceInfo(paymentAccount.getAccountTerminalId());
                integratedSingleOrderQueryResponseData.setBody(yuanTaOfflineQueryResponseDataObject.getOrderDesc());
            }

            if (SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus()) && SystemInstance.TRADE_REFUND_SUCCESS.equals(tradeDetail.getRefundStatus())) {
                integratedSingleOrderQueryResponseData.setOrderStatus("3");
            } else if (SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                integratedSingleOrderQueryResponseData.setOrderStatus("1");
            } else if (SystemInstance.TRADE_FAIL.equals(tradeDetail.getStatus())) {
                integratedSingleOrderQueryResponseData.setOrderStatus("2");
            } else if (SystemInstance.TRADE_WAITING.equals(tradeDetail.getStatus())) {
                integratedSingleOrderQueryResponseData.setOrderStatus("0");
            }

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedSingleOrderQueryResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);
            LOGGER.info("integrate response ready :" + generalResponseData);

        } else if ("Refund".equals(requestHeader.getServiceType())) {
            LOGGER.info("[YuanTaOfflinei][doRequest][Refund][data]" + data.getAsString());
            String refundOrderAmount = requestData.get("RefundFee").getAsString();

            if (!Long.toString(tradeDetail.getPayment()).equals(refundOrderAmount)) {
                throw new ServiceTypeNotSupportException("we don't provide partial refund");
            }
            YuanTaOfflineRefundRequestDataObject yuanTaOfflineRefundRequestDataObject = new YuanTaOfflineRefundRequestDataObject();
            yuanTaOfflineRefundRequestDataObject.setStoreID(paymentAccount.getAccount());
            yuanTaOfflineRefundRequestDataObject.setTermID(paymentAccount.getAccountTerminalId());
            yuanTaOfflineRefundRequestDataObject.setTimeout(YuanTaOfflineRequestBody.TIMEOUT);
            yuanTaOfflineRefundRequestDataObject.setOrderNo(tradeDetail.getOrderId());
            yuanTaOfflineRefundRequestDataObject.setOrderDT(DateTime.now().toString(SystemInstance.DATE_PATTERN));
            yuanTaOfflineRefundRequestDataObject.setRefundOrderNo(requestData.get("StoreRefundNo").getAsString());
            yuanTaOfflineRefundRequestDataObject.setRefundOrderAmount(refundOrderAmount);

            request = new Gson().toJson(yuanTaOfflineRefundRequestDataObject);
            if (Strings.isNullOrEmpty(integrateMchId) || Strings.isNullOrEmpty(yuanTaOfflineRefundRequestDataObject.getTermID())
                    || Strings.isNullOrEmpty(yuanTaOfflineRefundRequestDataObject.getTimeout()) || Strings.isNullOrEmpty(yuanTaOfflineRefundRequestDataObject.getOrderNo())
                    || Strings.isNullOrEmpty(yuanTaOfflineRefundRequestDataObject.getRefundOrderNo()) || Strings.isNullOrEmpty(yuanTaOfflineRefundRequestDataObject.getRefundOrderAmount())) {
                throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
            }
            try {
                result = postRequest("POST", request, YuanTaOfflineRequestBody.REFUND);
            } catch (Exception e) {
                YuanTaOfflineQueryRequestDataObject yuanTaOfflineQueryRequestDataObject = new YuanTaOfflineQueryRequestDataObject();
                yuanTaOfflineQueryRequestDataObject.setStoreID(paymentAccount.getAccount());
                yuanTaOfflineQueryRequestDataObject.setTermID(paymentAccount.getAccountTerminalId());
                yuanTaOfflineQueryRequestDataObject.setTimeout(YuanTaOfflineRequestBody.TIMEOUT);
                yuanTaOfflineQueryRequestDataObject.setOrderNo(tradeDetail.getOrderId());
                yuanTaOfflineQueryRequestDataObject.setOrderDT(DateTime.now().toString(SystemInstance.DATE_PATTERN));

                request = new Gson().toJson(yuanTaOfflineQueryRequestDataObject);
                if (Strings.isNullOrEmpty(integrateMchId) || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getTermID())
                        || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getTimeout()) || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getOrderNo())
                        || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getOrderDT())) {
                    throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
                }
                result = postRequest("POST", request, YuanTaOfflineRequestBody.QUERY);
            }
            LOGGER.info("result: " + result);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);
            YuanTaOfflineResponse yuanTaOfflineResponse = new Gson().fromJson(result, YuanTaOfflineResponse.class);

            ResponseGeneralHeader responseGeneralHeader = new YuanTaOfflineUtil().getYuanTaOfflineResponseHeader(
                    yuanTaOfflineResponse, requestHeader, messageSource);
            IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();

            integratedRefundResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
            integratedRefundResponseData.setStoreRefundNo(tradeDetail.getOrderId());
            integratedRefundResponseData.setSysRefundNo(tradeDetail.getStoreRefundId());

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedRefundResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);
            LOGGER.info("integrate response ready :" + generalResponseData);

        } else if ("Cancel".equals(requestHeader.getServiceType())) {
            LOGGER.info("[YuanTaOffline][doRequest][Cancel][data]" + data.getAsString());

            YuanTaOfflineCancelRequestDataObject yuanTaOfflineCancelRequestDataObject = new YuanTaOfflineCancelRequestDataObject();
            yuanTaOfflineCancelRequestDataObject.setStoreID(paymentAccount.getAccount());
            yuanTaOfflineCancelRequestDataObject.setTermID(paymentAccount.getAccountTerminalId());
            yuanTaOfflineCancelRequestDataObject.setTimeout(YuanTaOfflineRequestBody.TIMEOUT);
            yuanTaOfflineCancelRequestDataObject.setOrderNo(tradeDetail.getOrderId());
            yuanTaOfflineCancelRequestDataObject.setOrderDT(DateTime.now().toString(SystemInstance.DATE_PATTERN));

            request = new Gson().toJson(yuanTaOfflineCancelRequestDataObject);
            if (Strings.isNullOrEmpty(integrateMchId) || Strings.isNullOrEmpty(yuanTaOfflineCancelRequestDataObject.getTermID())
                    || Strings.isNullOrEmpty(yuanTaOfflineCancelRequestDataObject.getTimeout())
                    || Strings.isNullOrEmpty(yuanTaOfflineCancelRequestDataObject.getOrderNo())) {
                throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
            }

            try {
                result = postRequest("POST", request, YuanTaOfflineRequestBody.CANCEL);
            } catch (Exception e) {
                YuanTaOfflineQueryRequestDataObject yuanTaOfflineQueryRequestDataObject = new YuanTaOfflineQueryRequestDataObject();
                yuanTaOfflineQueryRequestDataObject.setStoreID(paymentAccount.getAccount());
                yuanTaOfflineQueryRequestDataObject.setTermID(paymentAccount.getAccountTerminalId());
                yuanTaOfflineQueryRequestDataObject.setTimeout(YuanTaOfflineRequestBody.TIMEOUT);
                yuanTaOfflineQueryRequestDataObject.setOrderNo(tradeDetail.getOrderId());
                yuanTaOfflineQueryRequestDataObject.setOrderDT(DateTime.now().toString(SystemInstance.DATE_PATTERN));

                request = new Gson().toJson(yuanTaOfflineQueryRequestDataObject);
                if (Strings.isNullOrEmpty(integrateMchId) || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getTermID())
                        || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getTimeout()) || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getOrderNo())
                        || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getOrderDT())) {
                    throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
                }
                result = postRequest("POST", request, YuanTaOfflineRequestBody.QUERY);
            }
            LOGGER.info("result: " + result);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);
            YuanTaOfflineResponse yuanTaOfflineResponse = new Gson().fromJson(result, YuanTaOfflineResponse.class);

            ResponseGeneralHeader responseGeneralHeader = new YuanTaOfflineUtil().getYuanTaOfflineResponseHeader(
                    yuanTaOfflineResponse, requestHeader, messageSource);
            IntegratedCancelResponseData integratedCancelResponseData = new IntegratedCancelResponseData();

            integratedCancelResponseData.setStoreOrderNo(tradeDetail.getOrderId());

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedCancelResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);
            LOGGER.info("integrate response ready :" + generalResponseData);

        } else if ("QueryAction".equals(requestHeader.getServiceType())) {
            LOGGER.info("[YuanTaOffline][doRequest][QueryAction][data]" + data.getAsString());

            YuanTaOfflineQueryActionRequestDataObject yuanTaOfflineQueryActionRequestDataObject = new YuanTaOfflineQueryActionRequestDataObject();
            yuanTaOfflineQueryActionRequestDataObject.setStoreID(paymentAccount.getAccount());
            yuanTaOfflineQueryActionRequestDataObject.setTermID(paymentAccount.getAccountTerminalId());
            yuanTaOfflineQueryActionRequestDataObject.setTimeout(YuanTaOfflineRequestBody.TIMEOUT);
            yuanTaOfflineQueryActionRequestDataObject.setOrderNo(tradeDetail.getOrderId());
            yuanTaOfflineQueryActionRequestDataObject.setOrderDT(DateTime.now().toString(SystemInstance.DATE_PATTERN));

            request = new Gson().toJson(yuanTaOfflineQueryActionRequestDataObject);
            if (Strings.isNullOrEmpty(integrateMchId) || Strings.isNullOrEmpty(yuanTaOfflineQueryActionRequestDataObject.getTermID())
                    || Strings.isNullOrEmpty(yuanTaOfflineQueryActionRequestDataObject.getTimeout())
                    || Strings.isNullOrEmpty(yuanTaOfflineQueryActionRequestDataObject.getOrderNo())) {
                throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
            }

            try {
                result = postRequest("POST", request, YuanTaOfflineRequestBody.QUERY_ACTION);
            } catch (Exception e) {
                YuanTaOfflineQueryRequestDataObject yuanTaOfflineQueryRequestDataObject = new YuanTaOfflineQueryRequestDataObject();
                yuanTaOfflineQueryRequestDataObject.setStoreID(paymentAccount.getAccount());
                yuanTaOfflineQueryRequestDataObject.setTermID(paymentAccount.getAccountTerminalId());
                yuanTaOfflineQueryRequestDataObject.setTimeout(YuanTaOfflineRequestBody.TIMEOUT);
                yuanTaOfflineQueryRequestDataObject.setOrderNo(tradeDetail.getOrderId());
                yuanTaOfflineQueryRequestDataObject.setOrderDT(DateTime.now().toString(SystemInstance.DATE_PATTERN));

                request = new Gson().toJson(yuanTaOfflineQueryRequestDataObject);
                if (Strings.isNullOrEmpty(integrateMchId) || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getTermID())
                        || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getTimeout()) || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getOrderNo())
                        || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getOrderDT())) {
                    throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
                }
                result = postRequest("POST", request, YuanTaOfflineRequestBody.QUERY);
            }
            LOGGER.info("result: " + result);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);
            YuanTaOfflineResponse yuanTaOfflineResponse = new Gson().fromJson(result, YuanTaOfflineResponse.class);

            ResponseGeneralHeader responseGeneralHeader = new YuanTaOfflineUtil().getYuanTaOfflineResponseHeader(
                    yuanTaOfflineResponse, requestHeader, messageSource);
            IntegratedQueryActionResponseData integratedQueryActionResponseData = new IntegratedQueryActionResponseData();
            YuanTaOfflineQueryActionResponseDataObject yuanTaOfflineQueryActionResponseDataObject = new YuanTaOfflineQueryActionResponseDataObject();

            integratedQueryActionResponseData.setOrderNo(yuanTaOfflineQueryActionResponseDataObject.getOrderNo());
            integratedQueryActionResponseData.setIsRefundEnable(yuanTaOfflineQueryActionResponseDataObject.getIsRefundActionEnable());
            integratedQueryActionResponseData.setIsCancelEnable(yuanTaOfflineQueryActionResponseDataObject.getIsCancelActionEnable());
            integratedQueryActionResponseData.setRefundMax(Double.valueOf(yuanTaOfflineQueryActionResponseDataObject.getRefundMax()));
            integratedQueryActionResponseData.setRefundMin(Double.valueOf(yuanTaOfflineQueryActionResponseDataObject.getRefundMin()));

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedQueryActionResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);
            LOGGER.info("integrate response ready :" + generalResponseData);

        } else if ("QueryForex".equals(requestHeader.getServiceType())) {
            LOGGER.info("[YuanTaOffline][doRequest][QueryForex][data]" + data.getAsString());

            YuanTaOfflineQueryForexRequestDataObject yuanTaOfflineQueryForexRequestDataObject = new YuanTaOfflineQueryForexRequestDataObject();
            yuanTaOfflineQueryForexRequestDataObject.setStoreID(paymentAccount.getAccount());
            yuanTaOfflineQueryForexRequestDataObject.setTermID(paymentAccount.getAccountTerminalId());
            yuanTaOfflineQueryForexRequestDataObject.setTimeout(YuanTaOfflineRequestBody.TIMEOUT);
            yuanTaOfflineQueryForexRequestDataObject.setAmount(Long.toString(tradeDetail.getPayment()));
            yuanTaOfflineQueryForexRequestDataObject.setCurrency(SystemInstance.CURRENCY_TWD);

            request = new Gson().toJson(yuanTaOfflineQueryForexRequestDataObject);
            if (Strings.isNullOrEmpty(integrateMchId) || Strings.isNullOrEmpty(yuanTaOfflineQueryForexRequestDataObject.getTermID())
                    || Strings.isNullOrEmpty(yuanTaOfflineQueryForexRequestDataObject.getTimeout())
                    || Strings.isNullOrEmpty(yuanTaOfflineQueryForexRequestDataObject.getOrderNo())) {
                throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
            }

            try {
                result = postRequest("POST", request, YuanTaOfflineRequestBody.QUERY_FOREX);
            } catch (Exception e) {
                YuanTaOfflineQueryRequestDataObject yuanTaOfflineQueryRequestDataObject = new YuanTaOfflineQueryRequestDataObject();
                yuanTaOfflineQueryRequestDataObject.setStoreID(paymentAccount.getAccount());
                yuanTaOfflineQueryRequestDataObject.setTermID(paymentAccount.getAccountTerminalId());
                yuanTaOfflineQueryRequestDataObject.setTimeout(YuanTaOfflineRequestBody.TIMEOUT);
                yuanTaOfflineQueryRequestDataObject.setOrderNo(tradeDetail.getOrderId());
                yuanTaOfflineQueryRequestDataObject.setOrderDT(DateTime.now().toString(SystemInstance.DATE_PATTERN));

                request = new Gson().toJson(yuanTaOfflineQueryRequestDataObject);
                if (Strings.isNullOrEmpty(integrateMchId) || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getTermID())
                        || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getTimeout()) || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getOrderNo())
                        || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getOrderDT())) {
                    throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
                }
                result = postRequest("POST", request, YuanTaOfflineRequestBody.QUERY);
            }
            LOGGER.info("result: " + result);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);
            YuanTaOfflineResponse yuanTaOfflineResponse = new Gson().fromJson(result, YuanTaOfflineResponse.class);

            ResponseGeneralHeader responseGeneralHeader = new YuanTaOfflineUtil().getYuanTaOfflineResponseHeader(
                    yuanTaOfflineResponse, requestHeader, messageSource);
            IntegratedCurrencyResponseData integratedCurrencyResponseData = new IntegratedCurrencyResponseData();
            YuanTaOfflineQueryForexResponseDataObject yuanTaOfflineQueryForexResponseDataObject = new YuanTaOfflineQueryForexResponseDataObject();

            integratedCurrencyResponseData.setCurrency(SystemInstance.CURRENCY_TWD);
            integratedCurrencyResponseData.setExchangeCurrency(yuanTaOfflineQueryForexRequestDataObject.getCurrency() == null ? "CNY" : yuanTaOfflineQueryForexRequestDataObject.getCurrency());
            integratedCurrencyResponseData.setExchangeRate(Double.valueOf(yuanTaOfflineQueryForexResponseDataObject.getExchangeRate()));
            integratedCurrencyResponseData.setExchangeRate(Double.valueOf(yuanTaOfflineQueryForexResponseDataObject.getExchangeAmount()));

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedCurrencyResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);
            LOGGER.info("integrate response ready :" + generalResponseData);
        }

        return generalResponseData;
    }

    @Override
    public String prepareAliPayShangHaiReserveRequestData(String totalFee, String orderId, PaymentAccount paymentAccount) throws Exception {

        YuanTaOfflinePrecreateRequestDataObject yuanTaOfflinePrecreateRequestDataObject = new YuanTaOfflinePrecreateRequestDataObject();
        yuanTaOfflinePrecreateRequestDataObject.setStoreID(paymentAccount.getAccount());
        yuanTaOfflinePrecreateRequestDataObject.setTermID(paymentAccount.getAccountTerminalId());
        yuanTaOfflinePrecreateRequestDataObject.setTimeout(YuanTaOfflineRequestBody.TIMEOUT);
        yuanTaOfflinePrecreateRequestDataObject.setOrderNo(orderId);
        yuanTaOfflinePrecreateRequestDataObject.setOrderCurrency(SystemInstance.CURRENCY_TWD);
        yuanTaOfflinePrecreateRequestDataObject.setOrderAmount(totalFee);
        yuanTaOfflinePrecreateRequestDataObject.setOrderDT(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        yuanTaOfflinePrecreateRequestDataObject.setOrderTitle(paymentAccount.getDescription());//再確認
        yuanTaOfflinePrecreateRequestDataObject.setNotifyUrl(API_URL + "allpaypass/api/yuantaoffline/notify");
        return new Gson().toJson(yuanTaOfflinePrecreateRequestDataObject);
    }

    @Override
    public String createOrder(TradeDetail tradeDetail, QrcodeParameter qrcodeParameter, String totalFee, String storeOrderNoNew, PaymentAccount paymentAccount) throws Exception {
        String request = prepareAliPayShangHaiReserveRequestData(totalFee, storeOrderNoNew, paymentAccount);
        String result;
        try {
            result = postRequest("POST", request, YuanTaOfflineRequestBody.PRECREATE);
        } catch (Exception e) {
            YuanTaOfflineQueryRequestDataObject yuanTaOfflineQueryRequestDataObject = new YuanTaOfflineQueryRequestDataObject();
            yuanTaOfflineQueryRequestDataObject.setStoreID(paymentAccount.getAccount());
            yuanTaOfflineQueryRequestDataObject.setTermID(paymentAccount.getAccountTerminalId());
            yuanTaOfflineQueryRequestDataObject.setTimeout(YuanTaOfflineRequestBody.TIMEOUT);
            yuanTaOfflineQueryRequestDataObject.setOrderNo(storeOrderNoNew);
            yuanTaOfflineQueryRequestDataObject.setOrderDT(DateTime.now().toString(SystemInstance.DATE_PATTERN));

            request = new Gson().toJson(yuanTaOfflineQueryRequestDataObject);
            if (Strings.isNullOrEmpty(paymentAccount.getAccount()) || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getTermID())
                    || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getTimeout()) || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getOrderNo())
                    || Strings.isNullOrEmpty(yuanTaOfflineQueryRequestDataObject.getOrderDT())) {
                throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
            }
            result = postRequest("POST", request, YuanTaOfflineRequestBody.QUERY);
        }
        LOGGER.info("result: " + result);
        YuanTaOfflineResponse yuanTaOfflineResponse = new Gson().fromJson(result, YuanTaOfflineResponse.class);
        YuanTaOfflinePrecreateResponseDataObject yuanTaOfflinePrecreateResponseDataObject = new Gson().fromJson(yuanTaOfflineResponse.getSecureData(), YuanTaOfflinePrecreateResponseDataObject.class);

        if ("S".equals(yuanTaOfflineResponse.getStatus())) {
            try {
                //save trade
                tradeDetail = setOnlineTradeDetailRequest(yuanTaOfflinePrecreateResponseDataObject, paymentAccount, qrcodeParameter, storeOrderNoNew);
                LOGGER.info("[tradeDetail]" + new Gson().toJson(tradeDetail));
            } catch (Exception e) {
                LOGGER.error("[YuanTaOffline][set trade exception]" + e.getMessage());
                e.printStackTrace();
            }
            String paymentUrl = yuanTaOfflinePrecreateResponseDataObject.getQrcode();
            return paymentUrl;
        } else {
            LOGGER.info("[YuanTaOffline][YuanTaOffline preCreate not SUCCESS]" + yuanTaOfflineResponse.getStatus());
        }
        return "";
    }

    //todo setOnlineTradeDetailRequest
    private TradeDetail setOnlineTradeDetailRequest(YuanTaOfflinePrecreateResponseDataObject yuanTaOfflinePrecreateResponseDataObject, PaymentAccount paymentAccount, QrcodeParameter qrcodeParameter, String orderId) {
        LOGGER.info("enter trade detail request:" + new Gson().toJson(yuanTaOfflinePrecreateResponseDataObject));

        Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());
        TradeDetail tradeDetail = tradeDetailService.getOne(orderId);
        Integer amount = yuanTaOfflinePrecreateResponseDataObject.getOrderAmount().intValue();
        LOGGER.info("[tradeDetail]:" + tradeDetail);
        if (tradeDetail != null) {
            return tradeDetail;
        } else {
            tradeDetail = new TradeDetail();
        }
        tradeDetail.setAccountId(merchant.getAccountId());
        tradeDetail.setMethod("11100");
        tradeDetail.setPaymentAccount(paymentAccount);
        tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
        tradeDetail.setOrderId(orderId);
        tradeDetail.setServiceType("OLPay");
        tradeDetail.setQrcodeToken(qrcodeParameter.getShortId());
        tradeDetail.setDescription(qrcodeParameter.getBody());
        tradeDetail.setPayment(amount);
        tradeDetail.setQrcodeToken(qrcodeParameter.getShortId());
        tradeDetail.setSystemOrderId(yuanTaOfflinePrecreateResponseDataObject.getBankOrderNo());
        tradeDetail.setDeviceRandomId(deviceService.getOne(merchant, "skb0001"));

        if (qrcodeParameter.getOnSaleTotalFee() == amount) {
            tradeDetail.setOnSale(true);
        } else {
            tradeDetail.setOnSale(false);
        }
        tradeDetail.setOriginalPrice(qrcodeParameter.getTotalFee());

        try {
            LOGGER.info("save online trade detail : " + new ObjectMapper().writeValueAsString(tradeDetail));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            LOGGER.info("save online trade detail error!");
        }
        tradeDetail = tradeDetailService.save(tradeDetail);
        return tradeDetail;
    }

    @Override
    public String sendRequest(String method, String url, String request) throws Exception {
        LOGGER.info("[YuanTaOffline][sendRequest][request]" + request);
        SSLContext sc = SSLContext.getInstance("TLSv1.2");
        sc.init(null, trustAllCerts, new SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        URL requestUrl = new URL(null, url, new Handler());
        HttpsURLConnection connection = (HttpsURLConnection) requestUrl.openConnection();

        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestMethod(method);// POST,GET,POST,DELETE,INPUT
        connection.setRequestProperty("Content-length", String.valueOf(request.length()));
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.setHostnameVerifier(new YuanTaOfflineServiceImpl().new TrustAnyHostnameVerifier());
        connection.connect();

        DataOutputStream output = new DataOutputStream(connection.getOutputStream());
        output.writeBytes(request);
        output.close();
        DataInputStream input = new DataInputStream(connection.getInputStream());

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(input));
        String result;

        StringBuilder buffer = new StringBuilder();
        while ((result = bufferedReader.readLine()) != null) {
            buffer.append(result);
        }

        input.close();

        return buffer.toString();
    }

    static TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            // TODO Auto-generated method stub
        }

        @Override
        public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    }};

    public class TrustAnyHostnameVerifier implements HostnameVerifier {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

    public String postRequest(String method, String request, String requestType) throws Exception {
        //取得隨機AESKey
        SecretKey secretKey = randomlyGeneratedAesKey();
        String key = byte2Hex(secretKey.getEncoded());
        SecretKey secretKeyConvert = convertSecretKey(key.getBytes());
        LOGGER.info("secretKey: " + DatatypeConverter.printBase64Binary(secretKey.getEncoded()));
        String requestParam = getParameters(request, requestType, secretKeyConvert, key);
        LOGGER.info("[HOST].." + YUANTAOFFLINE_URL + " [requestParam]... " + requestParam);

        String result = sendRequest(method, YUANTAOFFLINE_URL, requestParam);
        LOGGER.info("result: " + result);
        YuanTaOfflineResponse yuanTaOfflineResponse = new Gson().fromJson(URLDecoder.decode(result, "UTF-8"), YuanTaOfflineResponse.class);
        LOGGER.info("resultURLDecoder: " + URLDecoder.decode(result, "UTF-8"));
        LOGGER.info("yuanTaOfflineResponse: " + yuanTaOfflineResponse);


        String response;
        String secureDataResponse = yuanTaOfflineResponse.getSecureData();
        if ("F".equals(yuanTaOfflineResponse.getStatus())) {
            response = new Gson().toJson(yuanTaOfflineResponse);
        } else {
            LOGGER.info("secureDataResponse: " + secureDataResponse);
            secureDataResponse = URLDecoder.decode(secureDataResponse, "UTF-8");
            LOGGER.info("secureDataResponseURLDecoder: " + secureDataResponse);
            //todo decryptAES
            String secureDataResponseDecrypt = decryptAES(secureDataResponse, secretKeyConvert);
            LOGGER.info("secureDataResponseDecrypt: " + secureDataResponseDecrypt);
            //todo 校驗 hashDigest
            String parametersResponse = yuanTaOfflineResponse.getAction() + secureDataResponseDecrypt;
            String checkHashDigest = encryptSHA256(parametersResponse);
            LOGGER.info("checkHashDigest: " + checkHashDigest);
            LOGGER.info("yuanTaOfflineResponse.getHashDigest(): " + yuanTaOfflineResponse.getHashDigest());

            if (yuanTaOfflineResponse.getHashDigest().equals(checkHashDigest)) {
                String decryptResult = secureDataResponseDecrypt;
                yuanTaOfflineResponse.setSecureData(decryptResult);
                LOGGER.info("[decryptResult].. " + decryptResult);
                response = new Gson().toJson(yuanTaOfflineResponse);
            } else {
                //校驗碼有誤
                throw new Exception("Response 校驗碼有誤");
            }
        }
        return response;
    }

    public static SecretKey convertSecretKey(byte[] key) {
        return new SecretKeySpec(key, 0, key.length, "AES");
    }

    private String getParameters(String requestJson, String requestType, SecretKey secretKeyConvert, String key) throws Exception {
        LOGGER.info("requestJson " + requestJson);

        //將隨機產生的AESKey 進行encryptRSA
        String preapiToken = encryptRSA(key);
        LOGGER.info("preapiToken : " + preapiToken);
        String apiToken = URLEncoder.encode(preapiToken, "UTF-8");
        LOGGER.info("apiToken : " + apiToken);

        //將 secureData 進行encryptAES
        String preSecureData = encryptAES(requestJson, secretKeyConvert);
        //將加密後的 secureData 進行URLEncoder編碼
        String secureData = URLEncoder.encode(preSecureData, "UTF-8");
        LOGGER.info("secureData : " + secureData);

        // HashDigest
        String parametersRequest = requestType + preSecureData + preapiToken + BANK_KEY_VERSION;
        LOGGER.info("parametersRequest : " + parametersRequest);
        String preHashDigest = encryptSHA256(parametersRequest);
        LOGGER.info("preHashDigest : " + preHashDigest);
        String hashDigest = URLEncoder.encode(preHashDigest, "UTF-8");
        LOGGER.info("hashDigest : " + hashDigest);

        YuanTaOfflineRequest yuanTaOfflineRequest = new YuanTaOfflineRequest();
        yuanTaOfflineRequest.setAction(requestType);
        yuanTaOfflineRequest.setBankPKeyVersion(BANK_KEY_VERSION);
        yuanTaOfflineRequest.setSecureData(secureData);
        yuanTaOfflineRequest.setApiToken(apiToken);//todo RSA(AESKey of SecureData)with RSA public key
        yuanTaOfflineRequest.setHashDigest(hashDigest);

        return convertMapToUrlForm(objectConvertToMap(yuanTaOfflineRequest));
    }

    private String byte2Hex(byte[] inputBytes) {
        String result = "";
        for (byte currentByte : inputBytes) result += Integer.toString((currentByte & 0xff) + 0x100, 16).substring(1);
        return result;
    }


    //todo randomlyGeneratedAesKey
    public SecretKey randomlyGeneratedAesKey() throws Exception {
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        kgen.init(128, new SecureRandom());
        return kgen.generateKey();
    }

    //todo AesEncrypt IV(8651731586517315)
    public String encryptAES(String value, SecretKey secretKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        final BlockCipher AESCipher = new AESEngine();
        CBCBlockCipher AESCipherCBC = new CBCBlockCipher(AESCipher);
        BufferedBlockCipher cipher = new PaddedBufferedBlockCipher(AESCipherCBC, new PKCS7Padding());
//
        byte[] bytes = YUANTAOFFLINE_AES_IV.getBytes(UTF_8);
//
        try {
            cipher.reset();
            cipher.init(true, new ParametersWithIV(new KeyParameter(secretKey.getEncoded()), bytes));
            byte[] buffer = new byte[cipher.getOutputSize(value.getBytes().length)];
            int len = cipher.processBytes(value.getBytes(UTF_8), 0, value.getBytes(UTF_8).length, buffer, 0);
            len += cipher.doFinal(buffer, len);
            byte[] encrypted = Arrays.copyOfRange(buffer, 0, len);

            return DatatypeConverter.printBase64Binary(encrypted);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("encrypt error in SimpleAesManaged", e);
        }
//        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
//        cipher.init(1, secretKey, new IvParameterSpec(bytes));
//        return new String(DatatypeConverter.printBase64Binary(cipher.doFinal(value.getBytes())));
    }

    //todo RSA(AESKey of SecureData)with RSA public key
    public String encryptRSA(String secretKey) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(YUANTAOFFLINE_RSA_PUBLIC_KEY).getFile());
        FileReader fr = new FileReader(file);

        PublicKey publicKey = KeyReader.loadPublicKeyFromPEM(fr);
        fr.close();
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");//java默认"RSA"="RSA/ECB/PKCS1Padding"
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return DatatypeConverter.printBase64Binary(cipher.doFinal(secretKey.getBytes(UTF_8)));
    }

    //todo SHA256(request.Action+request.SecureData+request.APIToken+request.BankPKeyVersion)16進制
    // and SHA256(response.Action+response.SecureData)
    public String encryptSHA256(String content) throws Exception {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(content.getBytes(StandardCharsets.UTF_8));

            return DatatypeConverter.printHexBinary(hash).toLowerCase();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new RuntimeException("encryptSHA256 fail", e);
        }
    }

    //todo AesDecrypt
    public String decryptAES(String value, SecretKey secretKey) {
        LOGGER.info(value);
        byte[] data = DatatypeConverter.parseBase64Binary(value);

        final BlockCipher AESCipher = new AESEngine();
        CBCBlockCipher AESCipherCBC = new CBCBlockCipher(AESCipher);
        BufferedBlockCipher cipher = new PaddedBufferedBlockCipher(AESCipherCBC, new PKCS7Padding());

        byte[] bytes = YUANTAOFFLINE_AES_IV.getBytes(UTF_8);

        try {
            System.out.println(secretKey);
            cipher.reset();
            cipher.init(false, new ParametersWithIV(new KeyParameter(secretKey.getEncoded()), bytes));
            byte[] buffer = new byte[cipher.getOutputSize(data.length)];
            int len = cipher.processBytes(data, 0, data.length, buffer, 0);
            len += cipher.doFinal(buffer, len);
            byte[] encrypted = Arrays.copyOfRange(buffer, 0, len);

            return new String(encrypted, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("decrypt error in SimpleAesManaged", e);
        }
    }

    //todo yuanTaOfflineRequest Object convert to URLString
    public HashMap<String, String> objectConvertToMap(Object DataObject) {
        ObjectMapper oMapper = new ObjectMapper();
        LinkedHashMap<String, String> map = oMapper.convertValue(DataObject, LinkedHashMap.class);
        return map;
    }

    public String convertMapToUrlForm(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");
            result.append(entry.getKey());
            result.append("=");
            result.append(entry.getValue());
        }
        return result.toString();
    }
}
