package co.intella.service.impl;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Date;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import co.intella.model.INT_TB_CheckoutLog;
import co.intella.service.INT_TB_CheckoutLogService;
import co.intella.utility.HttpUtil;

/**
 * @author Nick	Lian
 */
@Service
public class INT_TB_CheckoutLogServiceImpl implements INT_TB_CheckoutLogService{

	private final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(INT_TB_CheckoutLogServiceImpl.class);

	@Value("${host.dao}")
    private String DAO_URL;

	public INT_TB_CheckoutLog save(INT_TB_CheckoutLog INT_TB_CheckoutLog) {
		try {
			String response = HttpUtil.post(DAO_URL + "api/CheckoutLog/update", new ObjectMapper().writeValueAsString(INT_TB_CheckoutLog));
			LOGGER.info("[CHECKOUT] response for CheckoutLog" + response);
			GsonBuilder builder = new GsonBuilder();
			// Register an adapter to manage the date types as long values
	        builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
	            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
	                return new Date(json.getAsJsonPrimitive().getAsLong());
	            }
	        });
	        Gson gson = builder.create();
            return gson.fromJson(response, INT_TB_CheckoutLog.class);
		} catch (Exception e) {
			LOGGER.error("[EXCEPTION] save() "+e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
			return null;
		}
	}
	
}
