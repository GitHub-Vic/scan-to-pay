package co.intella.service.impl;

import co.intella.model.DiscountTradeOLPay;
import co.intella.service.DiscountTradeOLPayService;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class DiscountTradeOLPayServiceImpl implements DiscountTradeOLPayService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Value("${host.dao}")
    private String DAO_URL;

    @Override
    public DiscountTradeOLPay save(DiscountTradeOLPay discountTrade) {
        DiscountTradeOLPay discountTradeOLPay = null;
        try {
            String result = HttpUtil.doPostJson(DAO_URL + "api/DiscountTradeOLPay/save", JsonUtil.toJsonStr(discountTrade));
            discountTradeOLPay = JsonUtil.parseJson(result, DiscountTradeOLPay.class);
        } catch (Exception e) {
            LOGGER.error("DiscountTradeOLPay save error");
//            e.printStackTrace();
        }
        return discountTradeOLPay;
    }

    @Override
    public DiscountTradeOLPay getOne(String accountId, String shortId) {
        DiscountTradeOLPay discountTradeOLPay = null;
        try {
            String result = HttpUtil.doGet(DAO_URL + "api/DiscountTradeOLPay/getOne/" + accountId + "/" + shortId);
            discountTradeOLPay = JsonUtil.parseJson(result, DiscountTradeOLPay.class);
        } catch (Exception e) {
            LOGGER.error("DiscountTradeOLPay getOne error");
//            e.printStackTrace();
        }
        return discountTradeOLPay;
    }

    @Override
    public DiscountTradeOLPay getOneByOrderId(String orderId, String accountId) {
        DiscountTradeOLPay discountTradeOLPay = null;
        try {
            String result = HttpUtil.doGet(DAO_URL + "api/getOneByOrderId/getOne/" + orderId + "/" + accountId );
            discountTradeOLPay = JsonUtil.parseJson(result, DiscountTradeOLPay.class);
        } catch (Exception e) {
            LOGGER.error("DiscountTradeOLPay getOneByOrderId error");
//            e.printStackTrace();
        }
        return discountTradeOLPay;
    }
}
