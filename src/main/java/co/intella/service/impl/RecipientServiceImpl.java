package co.intella.service.impl;

import co.intella.model.Merchant;
import co.intella.model.QPermission;
import co.intella.model.QRecipient;
import co.intella.model.Recipient;
import co.intella.repository.RecipientRepository;
import co.intella.service.RecipientService;
import com.google.common.collect.Lists;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

/**
 * @author Miles
 */
@Service
@Transactional
public class RecipientServiceImpl implements RecipientService {

    @Resource
    private RecipientRepository recipientRepository;

    public Recipient save(Recipient entity)
    {
        return recipientRepository.save(entity);
    }
    public void delete(String serialId, String appId) {
        recipientRepository.deleteUsersBySerialId(serialId, appId);
    }

    public Recipient getOne(UUID id) {
        QRecipient qRecipient = QRecipient.recipient;
        BooleanExpression predicate = qRecipient.id.eq(id);
        return recipientRepository.findOne(predicate);
    }

    public Recipient getOne(String udid, String appId) {
        QRecipient qRecipient = QRecipient.recipient;
        BooleanExpression predicate = qRecipient.serialId.eq(udid).and(qRecipient.appId.eq(appId));
        return recipientRepository.findOne(predicate);
    }

    public Recipient getOneAppIdNull(String udid) {
        QRecipient qRecipient = QRecipient.recipient;
        BooleanExpression predicate = qRecipient.serialId.eq(udid).and(qRecipient.appId.isNull());
        return recipientRepository.findOne(predicate);
    }

    public List<Recipient> list() {
        return recipientRepository.findAll();
    }

    public List<Recipient> listByMerchant(Merchant merchant) {
        QRecipient qRecipient= QRecipient.recipient;
        BooleanExpression predicate = qRecipient.merchant.eq(merchant);
        return Lists.newArrayList(recipientRepository.findAll(predicate));
    }

    public List<Recipient> listByUserId(String userId) {
        QRecipient qRecipient= QRecipient.recipient;
        BooleanExpression predicate = qRecipient.userId.eq(userId);
        return Lists.newArrayList(recipientRepository.findAll(predicate));
    }
	@Override
	public void deleteNullAppId(String serialId) {
		recipientRepository.deleteUsersBySerialIdNullAppId(serialId);
	}
}
