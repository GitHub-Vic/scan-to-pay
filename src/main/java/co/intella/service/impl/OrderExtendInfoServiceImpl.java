package co.intella.service.impl;

import co.intella.domain.aliShangHai.AliShangHaiRequestExtendParams;
import co.intella.net.HttpRequestUtil;
import co.intella.service.OrderExtendInfoService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class OrderExtendInfoServiceImpl implements OrderExtendInfoService {

    @Value("${host.dao}")
    private String DAO_URL;

    @Override
    public AliShangHaiRequestExtendParams findOne(String randomId) {
        AliShangHaiRequestExtendParams vo = new AliShangHaiRequestExtendParams();
        try {


            String responseEntity = HttpRequestUtil.httpGet(DAO_URL + "api/orderExtendInfo/findOne/" + randomId);

            vo = new Gson().fromJson(responseEntity, AliShangHaiRequestExtendParams.class);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return vo;
    }
}
