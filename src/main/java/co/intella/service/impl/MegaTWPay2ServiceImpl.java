package co.intella.service.impl;


import co.intella.domain.integration.IntegratedMicropayResponseData;
import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.domain.megaTwpay2.*;
import co.intella.exception.NoSuchMerchantException;
import co.intella.exception.ServiceTypeNotSupportException;
import co.intella.model.*;
import co.intella.service.*;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import co.intella.utility.SystemInstance;
import com.google.gson.JsonPrimitive;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MegaTWPay2ServiceImpl implements MegaTWPay2Service {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private Environment env;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private AppNotificationService appNotificationService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    private TradeDetailService tradeDetailService;

    @Autowired
    private TwPayService twPayService;

    @Resource
    private RefundDetailService refundDetailService;

    @Value("${mage.twpay.private.key}")
    private String PRIVATE_KEY_PATCH;

    @Value("${mage.twpay.public.key}")
    private String PUBLIC_KEY_PATCH;

    @Value("${mage.twpay.2.url}")
    private String MEGA_URL;

    private static Locale locale = new Locale("zh_TW");

    @Override
    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {

        LOGGER.info("[MegaTWPay][doRequest]");

        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        PaymentAccount paymentAccount = paymentAccountService.getOne(integrateMchId, bankId);
        if (Objects.isNull(paymentAccount)) {
            throw new ServiceTypeNotSupportException("Exception: The store's account isn't opened.");
        }
        Merchant merchant = merchantService.getOne(integrateMchId);
        if (Objects.isNull(merchant)) {
            throw new NoSuchMerchantException("Merchant error");
        }

        String result;
        String serviceType = requestHeader.getServiceType();
        switch (serviceType) {
            case SystemInstance.TYPE_MICROPAY:
                result = this.doMicropay(requestHeader, data, paymentAccount, merchant);
                break;
            case "Refund":
                result = this.doRefund(requestHeader, data, paymentAccount, merchant);
                break;
            case "SingleOrderQuery":
                result = this.doSingleOrderQuery(requestHeader, data, paymentAccount, merchant);
                break;
            default:
                LOGGER.error("Service type not support. " + serviceType);
                result = "Service type not support.";
        }

        return result;
    }


    private String doRefund(RequestHeader requestHeader, JsonPrimitive data, PaymentAccount paymentAccount, Merchant merchant) throws Exception {

        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
        String accountToken = paymentAccount.getPlatformCheckCode();
        RefundReq req = new RefundReq(tradeDetail, paymentAccount);
        if ("OLPay".equals(tradeDetail.getServiceType())) {
            PaymentAccount _paymentAccount = paymentAccountService.getOne(tradeDetail.getAccountId(), 56);
            req.setStoreId(_paymentAccount.getAccount());
            req.setTerminalId("");
            paymentAccount.setPlatformCheckCode(_paymentAccount.getKeyFilePath());
        }
        String result = this.sendReq(req, MEGA_URL + "refund", paymentAccount, "2");
        LOGGER.info("result => " + result);
        paymentAccount.setPlatformCheckCode(accountToken);
        RefundResp resp = JsonUtil.parseJson(result, RefundResp.class);
//        boolean checkSign = NewMegaTWPayUtil.checkSign(resp, paymentAccount, "2");
//        LOGGER.info("checkSign ->" + checkSign);
        RefundDetail refundDetail = refundDetailService.getOneByOrderId(tradeDetail.getOrderId());
        if ("0000".equals(resp.getRtnCode())) {
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
            tradeDetail.setSystemRefundId(resp.getTxnSeqno());
            refundDetail.setStatus(SystemInstance.TRADE_REFUND_SUCCESS);
            refundDetail.setSystemRefundId(tradeDetail.getSystemRefundId());
        } else {
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
            refundDetail.setStatus(SystemInstance.TRADE_REFUND_FAIL);
        }
        refundDetail.setTradeDetailRandomId(tradeDetail);
        tradeDetailService.save(tradeDetail);
        if (Objects.nonNull(refundDetail)) {
            refundDetailService.save(refundDetail);
        }

        ResponseGeneralBody responseGeneralBody = this.getResponseBody(tradeDetail, requestHeader, resp);
        if ("0000".equals(resp.getRtnCode())) {
            responseGeneralBody.setData(this.getIntegratedRefundResponseData(tradeDetail, result));
        }

        return JsonUtil.toJsonStr(responseGeneralBody);
    }

    private String doSingleOrderQuery(RequestHeader requestHeader, JsonPrimitive data, PaymentAccount paymentAccount, Merchant merchant) throws Exception {
        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
        InqueryReq req = new InqueryReq(paymentAccount, tradeDetail);

        String result = this.sendReq(req, MEGA_URL + "inquery", paymentAccount, "1");
        LOGGER.info("result => " + result);
        InqueryResp resp = JsonUtil.parseJson(result, InqueryResp.class);
//        boolean checkSign = NewMegaTWPayUtil.checkSign(resp, paymentAccount, "1");
//        LOGGER.info("checkSign ->" + checkSign);

        if ("0000".equals(resp.getRtnCode())) {
            if ("P".equals(resp.getTxnType())) {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus()) &&
                        ("1".equals(resp.getTxnStatus()) || "2".equals(resp.getTxnStatus()) || "3".equals(resp.getTxnStatus()))) {
                    // 成功                           已退款                             退款成功
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    tradeDetail.setSystemOrderId(resp.getTxnSeqno());
                } else if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            } else if ("R".equals(resp.getTxnType())) {
                if (!SystemInstance.TRADE_REFUND_SUCCESS.equals(tradeDetail.getStatus()) &&
                        ("1".equals(resp.getTxnStatus()) || "2".equals(resp.getTxnStatus()) || "3".equals(resp.getTxnStatus()))) {
                    // 成功                           已退款                             退款成功
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                    tradeDetail.setSystemRefundId(resp.getTxnSeqno());
                } else if (!SystemInstance.TRADE_REFUND_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }
        }
        tradeDetailService.save(tradeDetail);

        return "";      // 由外面統一產
    }

    /**
     * 被掃
     *
     * @param requestHeader
     * @param data
     * @param paymentAccount
     * @param merchant
     * @return
     * @throws Exception
     */
    private String doMicropay(RequestHeader requestHeader, JsonPrimitive data, PaymentAccount paymentAccount, Merchant merchant) throws Exception {

        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
        OrderReq req = new OrderReq(paymentAccount, tradeDetail);

        String result = this.sendReq(req, MEGA_URL + "order", paymentAccount, "2");
        LOGGER.info("result => " + result);
        OrderResp resp = JsonUtil.parseJson(result, OrderResp.class);
//        boolean checkSign = NewMegaTWPayUtil.checkSign(resp, paymentAccount, "2");
//        LOGGER.info("checkSign ->" + checkSign);
        String oldTradeDetailStatus = tradeDetail.getStatus();
        twPayService.setTradeDetailCreditCardType(tradeDetail);
        if ("0000".equals(resp.getRtnCode())) {
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetail.setSystemOrderId(resp.getTxnSeqno());
        } else {
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
        }

        tradeDetailService.save(tradeDetail);
        twPayService.saveTradeDetailCardLog(tradeDetail);
        if (!SystemInstance.TRADE_SUCCESS.equals(oldTradeDetailStatus) && SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
            appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
        }

        ResponseGeneralBody responseGeneralBody = this.getResponseBody(tradeDetail, requestHeader, resp);
        IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();
        integratedMicropayResponseData.setPlatformRsp(result);
        integratedMicropayResponseData.setAuthCode(tradeDetail.getBarcode());
        integratedMicropayResponseData.setStoreOrderNo(tradeDetail.getOrderId());
        integratedMicropayResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
        integratedMicropayResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
        responseGeneralBody.setData(integratedMicropayResponseData);
        return JsonUtil.toJsonStr(responseGeneralBody);
    }

    private String sendReq(MegaBaseModel baseModel, String url, PaymentAccount paymentAccount, String model) throws Exception {
        HashMap<String, String> header = new HashMap<>();
        header.put("ACCESS-TOKEN", paymentAccount.getPlatformCheckCode());
        baseModel.setSign(this.getSign(baseModel, model, paymentAccount));
        LOGGER.info("sendReq => " + JsonUtil.toJsonStr(baseModel));
        return HttpUtil.doPostJson(header, url, JsonUtil.toJsonStr(baseModel));
    }


    private String getSign(MegaBaseModel baseModel, String model, PaymentAccount paymentAccount) throws Exception {
        String str = JsonUtil.toJsonStr(baseModel);
        Map<String, String> signMap = new TreeMap<>();
        signMap.putAll(JsonUtil.parseJson(str, Map.class));
        String signStr = signMap.entrySet().stream().filter(en -> StringUtils.isNotBlank(en.getValue())).map(m -> m.getKey() + "=" + m.getValue()).collect(Collectors.joining("&"));
        LOGGER.info("signStr = " + signStr);

        if ("1".equals(model)) {
            signStr = this.getSHA256Hex(signStr);
            LOGGER.info("getSHA256Hex = " + signStr);
        } else {
            signStr = this.getSHA256Sign(signStr, paymentAccount);
            LOGGER.info("signSha256 = " + signStr);
        }

        return signStr;
    }

    private String getSHA256Sign(String str, PaymentAccount paymentAccount) throws Exception {

        Signature signature = Signature.getInstance("SHA256withRSA");
        PrivateKey privateKey = this.loadPrivateFromString(paymentAccount);

        signature.initSign(privateKey);
        signature.update(str.getBytes());
        byte[] sign = signature.sign();
        return Base64.getEncoder().encodeToString(sign);
    }

    private ResponseGeneralBody getResponseBody(TradeDetail tradeDetail, RequestHeader requestHeader, MegaBaseModel baseModel) {
        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        ResponseGeneralHeader header = new ResponseGeneralHeader();

        String intellaCode = this.transformReturnCode(baseModel.getRtnCode());

        boolean isNotIntellaCode = "-1".equals(intellaCode);
        header.setStatusCode(isNotIntellaCode ? baseModel.getRtnCode() : intellaCode);
        header.setStatusDesc(isNotIntellaCode ? baseModel.getRtnMsg() : messageSource.getMessage("response." + intellaCode, null, locale));
        header.setMethod(tradeDetail.getMethod());
        header.setMchId(requestHeader.getMerchantId());
        header.setServiceType(requestHeader.getServiceType());
        header.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        responseGeneralBody.setHeader(header);
        return responseGeneralBody;
    }


    private String transformReturnCode(String errcode) {
        String result = "-1";
        switch (errcode) {
            case "0000":
                result = "0000";
                break;
            case "5000":
                result = "9998";
                break;
            case "5007":
                result = "7000";
                break;
            case "9401":
            case "0104":
            case "0102":
                result = "8002";
                break;
            case "0101":
                result = "7202";
                break;
            case "6001":
            case "6000":
                result = "7353";
                break;
        }
        return result;
    }

    private IntegratedRefundResponseData getIntegratedRefundResponseData(TradeDetail tradeDetail, String respJson) {
        IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();
        integratedRefundResponseData.setPlatformRsp(respJson);
        integratedRefundResponseData.setRefundedAt(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        integratedRefundResponseData.setStoreOrderNo(tradeDetail.getOrderId());
        integratedRefundResponseData.setStoreRefundNo(tradeDetail.getStoreRefundId());
        integratedRefundResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
        integratedRefundResponseData.setSysRefundNo(tradeDetail.getSystemRefundId());
        return integratedRefundResponseData;
    }

    //SHA256加密
    private static String getSHA256Hex(String message) throws Exception {

        final MessageDigest digest = MessageDigest.getInstance("sha-256");
        byte[] messageHash = digest.digest(message.getBytes(StandardCharsets.UTF_8));
        return DatatypeConverter.printHexBinary(messageHash).toUpperCase();
    }


    private PrivateKey loadPrivateFromString(PaymentAccount paymentAccount) throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] privateKeyDER = Base64.getDecoder().decode(paymentAccount.getHashIV());
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePrivate(new PKCS8EncodedKeySpec(privateKeyDER));
    }


}
