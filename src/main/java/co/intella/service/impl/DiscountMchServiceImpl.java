package co.intella.service.impl;


import co.intella.model.DiscountMch;
import co.intella.service.DiscountMchService;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DiscountMchServiceImpl implements DiscountMchService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Value("${host.dao}")
    private String DAO_URL;

    @Override
    public List<DiscountMch> getDiscountMchList(String accountId) {
        String result = HttpUtil.doGet(DAO_URL + "api/discountMch/getDiscountMchList/" + accountId);
        return JsonUtil.parseJsonList(result, DiscountMch.class);
    }
}
