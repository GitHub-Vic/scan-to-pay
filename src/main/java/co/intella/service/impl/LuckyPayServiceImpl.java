package co.intella.service.impl;

import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.IntegratedSingleOrderQueryResponseData;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.domain.luckypay.LuckyPayQueryOrderResponseData;
import co.intella.exception.ServiceTypeNotSupportException;
import co.intella.model.PaymentAccount;
import co.intella.model.RefundDetail;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import co.intella.net.HttpRequestUtil;
import co.intella.service.*;
import co.intella.utility.LuckyPayUtil;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
public class LuckyPayServiceImpl implements LuckyPayService {

    private final Logger LOGGER = LoggerFactory.getLogger(LuckyPayServiceImpl.class);

    String currencyTimeString = LocalDateTime.now().toString("yyyyMMddHHmmss");

    @Value("${luckypay.api.url}")
    private String LUCKYPAY_URL;

    @Autowired
    private Environment env;

    @Autowired
    private MessageSource messageSource;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private ReserveDetailService reserveDetailService;

    @Resource
    private RefundDetailService refundDetailService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private ReserveCancelService reserveCancelService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {
        String generalResponseData = SystemInstance.EMPTY_STRING;
        String integrateMchId = requestHeader.getMerchantId();

        JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
        PaymentAccount luckyPayAccount = paymentAccountService.getOne(integrateMchId,
                Long.valueOf(env.getProperty("bank.id.13000")));
        String[] hashIV = null;
        DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        if (luckyPayAccount.getHashIV() == null) {
            try {
                this.terminalRegister(luckyPayAccount);
                this.terminalRegisterConfirm(luckyPayAccount);
                hashIV = luckyPayAccount.getHashIV().split(":");
            } catch (Exception e) {
                LOGGER.error("[LuckyPay][TRANSACTION] terminalRegister :" + e.getMessage());
                String result = "{\"return_code\":\"100104\"}";
                ResponseGeneralHeader responseGeneralHeader = new LuckyPayUtil().getResponseHeader(result,
                        requestHeader, messageSource);
                IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();
                ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
                responseGeneralBody.setHeader(responseGeneralHeader);
                responseGeneralBody.setData(integratedRefundResponseData);
                generalResponseData = new Gson().toJson(responseGeneralBody);
                return generalResponseData;
            }
        } else {
            hashIV = luckyPayAccount.getHashIV().split(":");
            if ((df.parse(hashIV[2]).getTime() - df.parse(currencyTimeString).getTime()) <= 3600000) {
                this.updateKeyExpiry(luckyPayAccount);
                hashIV = luckyPayAccount.getHashIV().split(":");
            }
        }

        if ("Refund".equals(requestHeader.getServiceType())) {
            if (!Long.toString(tradeDetail.getPayment()).equals(requestData.get("RefundFee").getAsString())) {
                throw new ServiceTypeNotSupportException("we don't provide partial refund");
            }
            String queryResult = this.orderQuery(luckyPayAccount, tradeDetail, hashIV);
            String result = null;
            LuckyPayQueryOrderResponseData queryResponse = new Gson().fromJson(queryResult,
                    LuckyPayQueryOrderResponseData.class);
            if ("01".equals(queryResponse.getStatus())) {
                result = this.orderReverse(luckyPayAccount, tradeDetail, hashIV);
            } else if ("03".equals(queryResponse.getStatus()) || "05".equals(queryResponse.getStatus())) {
                result = this.orderRefund(luckyPayAccount, tradeDetail, hashIV);
            }
            requestHeader.setMerchantId(integrateMchId);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);
            ResponseGeneralHeader responseGeneralHeader = new LuckyPayUtil().getResponseHeader(result, requestHeader,
                    messageSource);
            JsonObject dataJson = new Gson().fromJson(result, JsonObject.class);

            IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();

            try {
                integratedRefundResponseData.setSysRefundNo(dataJson.get("transaction_id").getAsString());
            } catch (Exception e) {
                integratedRefundResponseData.setSysRefundNo(null);
            }

            integratedRefundResponseData.setStoreOrderNo(tradeDetail.getOrderId());
            integratedRefundResponseData.setStoreRefundNo(tradeDetail.getStoreRefundId().toString());
            integratedRefundResponseData.setRefundedAt(responseGeneralHeader.getResponseTime());

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedRefundResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);

        } else if ("SingleOrderQuery".equals(requestHeader.getServiceType())) {
            String result = this.orderQuery(luckyPayAccount, tradeDetail, hashIV);
            requestHeader.setMerchantId(integrateMchId);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);
            ResponseGeneralHeader responseGeneralHeader = new LuckyPayUtil().getResponseHeader(result, requestHeader,
                    messageSource);
            tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());
            IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();
            integratedSingleOrderQueryResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
            integratedSingleOrderQueryResponseData.setStoreOrderNo(tradeDetail.getOrderId());
            integratedSingleOrderQueryResponseData.setDetail(tradeDetail.getDetail());
            integratedSingleOrderQueryResponseData.setStoreInfo(tradeDetail.getStoreInfo());
            integratedSingleOrderQueryResponseData.setFeeType("TWD");
            integratedSingleOrderQueryResponseData.setTotalFee(Long.toString(tradeDetail.getPayment()));
            integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());
            integratedSingleOrderQueryResponseData.setPaidAt(tradeDetail.getCreateDate());
            integratedSingleOrderQueryResponseData.setRefundedMsg(tradeDetail.getRefundStatus());
            RefundDetail refundDetail = refundDetailService.getOneByOrderIdAndMethod(tradeDetail.getOrderId(), tradeDetail.getMethod());
            if (Objects.nonNull(refundDetail)) {
                integratedSingleOrderQueryResponseData.setRefundedAt(refundDetail.getCreateTime());
            }

            if (SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())
                    && SystemInstance.TRADE_REFUND_SUCCESS.equals(tradeDetail.getRefundStatus())) {
                integratedSingleOrderQueryResponseData.setOrderStatus("3");
            } else if (SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                integratedSingleOrderQueryResponseData.setOrderStatus("1");
            } else if (SystemInstance.TRADE_FAIL.equals(tradeDetail.getStatus())) {
                integratedSingleOrderQueryResponseData.setOrderStatus("2");
            } else if (SystemInstance.TRADE_WAITING.equals(tradeDetail.getStatus())) {
                integratedSingleOrderQueryResponseData.setOrderStatus("0");
            }

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedSingleOrderQueryResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);
        }
        return generalResponseData;
    }

    @Override
    public PaymentAccount updateKeyExpiry(PaymentAccount luckyPayAccount) {

        String[] hashIV = luckyPayAccount.getHashIV().split(":");

        String luckyPayRequestData = "nonce=32ae3b1544df2453d264cc1d14410aed" + "&" + "store="
                + luckyPayAccount.getAccount() + "&" + "terminal=" + luckyPayAccount.getAppId() + "&"
                + "type=TKEY_REQ&version=1.0&key=" + hashIV[0];
        try {
            String sign = signValueGenerate(luckyPayRequestData);
            Map<String, String> map = new HashMap<String, String>();
            map.put("type", "TKEY_REQ");
            map.put("store", luckyPayAccount.getAccount());
            map.put("terminal", luckyPayAccount.getAppId());
            map.put("nonce", "32ae3b1544df2453d264cc1d14410aed");
            map.put("version", "1.0");
            map.put("sign", sign);

            String luckyPayRequest = new Gson().toJson(map);
            LOGGER.info("[LuckyPay][TRANSACTION] LuckyPay updateKeyExpiry start. :" + luckyPayRequest);
            String result = HttpRequestUtil.post(LUCKYPAY_URL + "key", luckyPayRequest);
            LOGGER.info("[LuckyPay][TRANSACTION] LuckyPay updateKeyExpiry end.: " + result);
            LuckyPayQueryOrderResponseData response = new Gson().fromJson(result, LuckyPayQueryOrderResponseData.class);

            if ("000000".equals(response.getReturnCode()) && "SUCCESS".equals(response.getResultCode())) {
                String newKey = hashIV[0] + ":" + response.getKey() + ":" + response.getKeyExpiry();
                luckyPayAccount.setHashIV(newKey);
                LOGGER.info("[LuckyPay][UPDATEKEY] update key success.");
                paymentAccountService.save(luckyPayAccount);
            } else if ("000104".equals(response.getReturnCode())) {
                LOGGER.error("[LuckyPay][UPDATEKEY] verification failed.");
            } else {
                LOGGER.error("[LuckyPay][UPDATEKEY] unknown error.");
            }

        } catch (Exception e) {
            LOGGER.error("[LuckyPay][TRANSACTION] updateKeyExpiry :" + e.getMessage());
        }
        return luckyPayAccount;

    }

    @Override
    public String orderRefund(PaymentAccount luckyPayAccount, TradeDetail tradeDetail, String[] hashIV)
            throws Exception {

        String luckyPayRequestData = "amount=" + tradeDetail.getPayment() + "00" + "&" + "currency=TWD" + "&"
                + "nonce=32ae3b1544df2453d264cc1d14410aed" + "&" + "store=" + luckyPayAccount.getAccount() + "&"
                + "terminal=" + luckyPayAccount.getAppId() + "&" + "time=" + currencyTimeString + "&"
                + "transaction_id=" + tradeDetail.getSystemOrderId() + "&" + "type=REFUND_REQ&version=1.0&key="
                + hashIV[1];
        String sign = signValueGenerate(luckyPayRequestData);
        Map<String, String> map = new HashMap<String, String>();
        map.put("type", "REFUND_REQ");
        map.put("store", luckyPayAccount.getAccount());
        map.put("terminal", luckyPayAccount.getAppId());
        map.put("transaction_id", tradeDetail.getSystemOrderId());
        map.put("currency", "TWD");
        map.put("amount", Long.toString(tradeDetail.getPayment()) + "00");
        map.put("time", currencyTimeString);
        map.put("nonce", "32ae3b1544df2453d264cc1d14410aed");
        map.put("version", "1.0");
        map.put("currency", "TWD");
        map.put("sign", sign);
        String luckyPayRequest = new Gson().toJson(map);
        LOGGER.info("[LuckyPay][TRANSACTION] LuckyPay refund start. :" + luckyPayRequest);
        String result = HttpRequestUtil.post(LUCKYPAY_URL + "refund", luckyPayRequest);
        LOGGER.info("[LuckyPay][TRANSACTION] LuckyPay refund end.: " + result);
        return result;
    }

    @Override
    public String orderQuery(PaymentAccount luckyPayAccount, TradeDetail tradeDetail, String[] hashIV)
            throws Exception {

        String luckyPayRequestData = "nonce=32ae3b1544df2453d264cc1d14410aed" + "&" + "order_no="
                + tradeDetail.getSystemOrderId() + "&" + "store=" + luckyPayAccount.getAccount() + "&" + "terminal="
                + luckyPayAccount.getAppId() + "&" + "time=" + currencyTimeString + "&"
                + "type=ORDER_REQ&version=1.0&key=" + hashIV[1];
        String sign = signValueGenerate(luckyPayRequestData);

        Map<String, String> map = new HashMap<String, String>();
        map.put("type", "ORDER_REQ");
        map.put("store", luckyPayAccount.getAccount());
        map.put("terminal", luckyPayAccount.getAppId());
        map.put("order_no", tradeDetail.getSystemOrderId());
        map.put("time", currencyTimeString);
        map.put("nonce", "32ae3b1544df2453d264cc1d14410aed");
        map.put("version", "1.0");
        map.put("sign", sign);

        String luckyPayRequest = new Gson().toJson(map);
        LOGGER.info("[LuckyPay][TRANSACTION] LuckyPay queryorder start. :" + luckyPayRequest);
        String result = HttpRequestUtil.post(LUCKYPAY_URL + "queryorder", luckyPayRequest);
        LOGGER.info("[LuckyPay][TRANSACTION] LuckyPay queryorder end.: " + result);
        return result;
    }

    @Override
    public String orderReverse(PaymentAccount luckyPayAccount, TradeDetail tradeDetail, String[] hashIV)
            throws Exception {

        String luckyPayRequestData = "nonce=32ae3b1544df2453d264cc1d14410aed" + "&" + "order_no="
                + tradeDetail.getSystemOrderId() + "&" + "store=" + luckyPayAccount.getAccount() + "&" + "terminal="
                + luckyPayAccount.getAppId() + "&" + "time=" + currencyTimeString + "&"
                + "type=CANCEL_REQ&version=1.0&key=" + hashIV[1];
        String sign = signValueGenerate(luckyPayRequestData);

        Map<String, String> map = new HashMap<String, String>();
        map.put("type", "CANCEL_REQ");
        map.put("store", luckyPayAccount.getAccount());
        map.put("terminal", luckyPayAccount.getAppId());
        map.put("order_no", tradeDetail.getSystemOrderId());
        map.put("time", currencyTimeString);
        map.put("nonce", "32ae3b1544df2453d264cc1d14410aed");
        map.put("version", "1.0");
        map.put("sign", sign);

        String luckyPayRequest = new Gson().toJson(map);
        LOGGER.info("[LuckyPay][TRANSACTION] LuckyPay orderReverse start. :" + luckyPayRequest);
        String result = HttpRequestUtil.post(LUCKYPAY_URL + "reverse", luckyPayRequest);
        LOGGER.info("[LuckyPay][TRANSACTION] LuckyPay orderReverse end.: " + result);
        return result;
    }

    @Override
    public PaymentAccount terminalRegister(PaymentAccount luckyPayAccount) throws Exception {

        Map<String, String> map = new HashMap<String, String>();
        map.put("type", "TREG_REQ");
        map.put("store", luckyPayAccount.getAccount());
        map.put("terminal", luckyPayAccount.getAppId());
        map.put("device_type", "22");
        map.put("version", "1.0");
        map.put("registry_key", luckyPayAccount.getHashKey());

        String luckyPayRequest = new Gson().toJson(map);
        LOGGER.info("[LuckyPay][TRANSACTION] LuckyPay terminalRegister start. :" + luckyPayRequest);
        String result = HttpRequestUtil.post(LUCKYPAY_URL + "reg", luckyPayRequest);
//		String result = "{ \"store_key\": \"5c1c7406febf4fb4953865ad57fffa00\", \"key\": \"3414471ac4f77a03ed2a2a18d0dfa648\", \"key_expiry\": \"20181225135817\", \"type\": \"TREG_RES\", \"store\": \"27560639K\", \"terminal\": \"QRcode03\", \"return_code\": \"000000\", \"return_msg\": \"接受交易\", \"result_code\": \"SUCCESS\" }";
        LOGGER.info("[LuckyPay][TRANSACTION] LuckyPay terminalRegister end.: " + result);

        LuckyPayQueryOrderResponseData response = new Gson().fromJson(result, LuckyPayQueryOrderResponseData.class);
        if ("000000".equals(response.getReturnCode()) && "SUCCESS".equals(response.getResultCode())) {
            String newKey = response.getStoreKey() + ":" + response.getKey() + ":" + response.getKeyExpiry();
            luckyPayAccount.setHashIV(newKey);
            LOGGER.info("[LuckyPay][REGISTER] regist key success.");
            paymentAccountService.save(luckyPayAccount);
        } else if ("100104".equals(response.getReturnCode())) {
            LOGGER.error("[LuckyPay][REGISTER] terminal device is registered.");
            this.terminalUnregister(luckyPayAccount);
        } else if ("000104".equals(response.getReturnCode())) {
            LOGGER.error("[LuckyPay][UPDATEKEY] verification failed.");
        } else {
            LOGGER.error("[LuckyPay][REGISTER] unknown error.");
        }

        return luckyPayAccount;
    }

    @Override
    public void terminalUnregister(PaymentAccount luckyPayAccount) throws Exception {
        Map<String, String> map = new HashMap<String, String>();
        map.put("type", "UNREG_REQ");
        map.put("store", luckyPayAccount.getAccount());
        map.put("terminal", luckyPayAccount.getAppId());
        map.put("device_type", "22");
        map.put("version", "1.0");
        map.put("registry_key", luckyPayAccount.getHashKey());

        String luckyPayRequest = new Gson().toJson(map);
        LOGGER.info("[LuckyPay][TRANSACTION] LuckyPay terminalUnregister start. :" + luckyPayRequest);
        String result = HttpRequestUtil.post(LUCKYPAY_URL + "reg", luckyPayRequest);
        LOGGER.info("[LuckyPay][TRANSACTION] LuckyPay terminalUnregister end.: " + result);

        LuckyPayQueryOrderResponseData response = new Gson().fromJson(result, LuckyPayQueryOrderResponseData.class);
        if ("000000".equals(response.getReturnCode()) && "SUCCESS".equals(response.getResultCode())) {
            LOGGER.info("[LuckyPay][UNREGISTER] unregist success.");
            this.terminalRegister(luckyPayAccount);
        } else {
            LOGGER.error("[LuckyPay][UNREGISTER] unknown error.");
        }
    }

    @Override
    public void terminalRegisterConfirm(PaymentAccount luckyPayAccount) throws Exception {

        String[] hashIV = luckyPayAccount.getHashIV().split(":");
        String luckyPayRequestData = "store=" + luckyPayAccount.getAccount() + "&" + "terminal="
                + luckyPayAccount.getAppId() + "&" + "type=CONFREG_REQ&version=1.0&key=" + hashIV[1];
        String sign = signValueGenerate(luckyPayRequestData);

        Map<String, String> map = new HashMap<String, String>();
        map.put("type", "CONFREG_REQ");
        map.put("store", luckyPayAccount.getAccount());
        map.put("terminal", luckyPayAccount.getAppId());
        map.put("version", "1.0");
        map.put("sign", sign);

        String luckyPayRequest = new Gson().toJson(map);
        LOGGER.info("[LuckyPay][TRANSACTION] LuckyPay terminalRegister start. :" + luckyPayRequest);
        String result = HttpRequestUtil.post(LUCKYPAY_URL + "reg", luckyPayRequest);
        LOGGER.info("[LuckyPay][TRANSACTION] LuckyPay terminalRegister end.: " + result);

        LuckyPayQueryOrderResponseData response = new Gson().fromJson(result, LuckyPayQueryOrderResponseData.class);
        if ("000000".equals(response.getReturnCode()) && "SUCCESS".equals(response.getResultCode())) {
            LOGGER.info("[LuckyPay][CONFIRM] regist key success.");
        } else if ("100104".equals(response.getReturnCode())) {
            LOGGER.error("[LuckyPay][CONFIRM] terminal device is registered.");
        } else if ("000104".equals(response.getReturnCode())) {
            LOGGER.error("[LuckyPay][CONFIRM] verification failed.");
        } else {
            LOGGER.error("[LuckyPay][CONFIRM] unknown error..");
        }
    }

    @Override
    public String signValueGenerate(String requestDATA) throws Exception {
        String sign = null;
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(requestDATA.getBytes(StandardCharsets.UTF_8));
            Base64.Encoder encode = Base64.getEncoder();
            sign = encode.encodeToString(hash);
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("[LuckyPay][TRANSACTION] signValueGenerate :" + e.getMessage());
        }
        return sign;
    }
}
