package co.intella.service.impl;

import co.intella.model.PaymentAccount;
import co.intella.model.QPaymentAccount;
import co.intella.model.QrcodeParameter;
import co.intella.net.HttpRequestUtil;
import co.intella.repository.PaymentAccountCustomRepository;
import co.intella.repository.PaymentAccountRepository;
import co.intella.service.BankService;
import co.intella.service.MerchantService;
import co.intella.service.PaymentAccountService;
import co.intella.service.QrcodeService;
import co.intella.utility.GeneralUtil;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Alexchiu on 2017/3/20.
 */
@Service
@Transactional
public class PaymentAccountServiceImpl implements PaymentAccountService {

    private final Logger LOGGER = LoggerFactory.getLogger(PaymentAccountServiceImpl.class);


    @Autowired
    private MerchantService merchantService;

    @Autowired
    private BankService bankService;

    @Resource
    private PaymentAccountRepository paymentAccountRepository;

    @Autowired
    private QrcodeService qrcodeService;

    @Autowired
    private PaymentAccountCustomRepository paymentAccountCustomRepository;

    private Locale locale = new Locale("zh_TW");

    @Autowired
    private MessageSource messageSource;

    private static String HOST_DAO = "http://localhost:8080/scan2pay-data/";


    public PaymentAccount getOne(String merchantId, long bankId) {

        QPaymentAccount qPaymentAccount = QPaymentAccount.paymentAccount;

        BooleanExpression predicate = qPaymentAccount.isDefault.eq(true)
                .and(qPaymentAccount.merchant.eq(merchantService.getOne(merchantId)))
                .and(qPaymentAccount.bank.eq(bankService.getOne(bankId)));
        return paymentAccountRepository.findOne(predicate);
    }

    public PaymentAccount getOneByDao(String merchantId, long bankId) {
        String responseEntity = null;
        PaymentAccount paymentAccount = null;
        try {
            responseEntity = HttpRequestUtil.httpGet(HOST_DAO + "api/paymentaccount/get/" + merchantId + "/" + bankId);
            paymentAccount = new Gson().fromJson(GeneralUtil.getMessage(responseEntity), PaymentAccount.class);

        } catch (Exception e) {
            e.printStackTrace();
            paymentAccount = null;
        }
        return paymentAccount;

    }

    public List<PaymentAccount> listByBankId(long bankId) {

        QPaymentAccount qPaymentAccount = QPaymentAccount.paymentAccount;

        BooleanExpression predicate = qPaymentAccount.isDefault.eq(true)
                .and(qPaymentAccount.bank.eq(bankService.getOne(bankId)));
        return Lists.newArrayList(paymentAccountRepository.findAll(predicate));
    }

    public List<PaymentAccount> groupAcerTexiIpass() {

        return paymentAccountRepository.groupAcerTexiIpass();
    }


    public List<PaymentAccount> listAllByAccount(String accountId) {
        QPaymentAccount qPaymentAccount = QPaymentAccount.paymentAccount;

        BooleanExpression predicate = qPaymentAccount.merchant.eq(merchantService.getOne(accountId));


        return Lists.newArrayList(paymentAccountRepository.findAll(predicate));
    }


    public List<PaymentAccount> listByQrcode(String qrcode) {
        QPaymentAccount qPaymentAccount = QPaymentAccount.paymentAccount;

        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(qrcode);

        BooleanExpression predicate = qPaymentAccount.isDefault.eq(true)
                .and(qPaymentAccount.merchant.eq(merchantService.getOne(qrcodeParameter.getMchId())));

        List<PaymentAccount> list = Lists.newArrayList(paymentAccountRepository.findAll(predicate));
        if (list.stream().anyMatch(paymentAccount -> paymentAccount.getBank().getBankId() == 59)) {
            //  20210224 新pi正掃模式上線/測試，避免重複顯示支付按鈕，有新pi就將舊的濾掉
            list = list.stream().filter(paymentAccount -> paymentAccount.getBank().getBankId() != 3).collect(Collectors.toList());
        }
        return list;
    }

    public PaymentAccount save(PaymentAccount paymentAccount) {
        return paymentAccountRepository.save(paymentAccount);
    }

    public String paymentAccountExist(String terminalId) {
        LOGGER.info("[REQUEST] paymentAccountExist. " + terminalId);
        try {
            String result = HttpRequestUtil.httpGet(HOST_DAO + "api/paymentaccount/paymentAccountExist?terminalId=" + terminalId);
            return result;
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    public List<PaymentAccount> listByPayAccount(String payAccount) {
        QPaymentAccount qPaymentAccount = QPaymentAccount.paymentAccount;

        BooleanExpression predicate = qPaymentAccount.isDefault.eq(true)
                .and(qPaymentAccount.account.eq(payAccount));
        return Lists.newArrayList(paymentAccountRepository.findAll(predicate));
    }

    @Override
    public Map<String, Object> checkQrcodePayment(String accountId) {
        Map<String, Object> result = new HashMap<String, Object>();
        Integer count = paymentAccountCustomRepository.checkQrcodePayment(accountId);
        if (count > 0) {
            result.put("msgCode", "0000");
            result.put("msg", "");
        }
        if (count.equals(0)) {
            result.put("msgCode", "9998");
            result.put("msg", messageSource.getMessage("co.intella.payment.account.no.qrcode", null, locale));
        }
        return result;
    }

    @Override
    public Map<String, Object> checkTicketPayment(String accountId) {
        Map<String, Object> result = new HashMap<String, Object>();
        Integer count = paymentAccountCustomRepository.checkTicketPayment(accountId);
        if (count > 0) {
            result.put("msgCode", "0000");
            result.put("msg", "");
        }
        if (count.equals(0)) {
            result.put("msgCode", "9998");
            result.put("msg", messageSource.getMessage("co.intella.payment.account.no.ticket", null, locale));
        }
        return result;
    }
}
