package co.intella.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import co.intella.net.HttpRequestUtil;
import co.intella.service.TokenService;

@Service
public class TokenServiceImpl implements TokenService{
	
    @Value("${host.dao}")
    private String DAO_URL;

	@Override
	public JSONArray getMerchantByTokenIdAndAccountId(String tokenId, String accountId) throws Exception {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("tokenId", tokenId);
		jsonObject.put("accountId", accountId);
		String response = HttpRequestUtil.post(DAO_URL + "api/token/getMerchantByTokenIdAndAccountId", jsonObject.toString());
		JSONArray jsonArray = new JSONArray(response);
		return jsonArray;
	}

	@Override
	public JSONObject checkToken(String tokenId) throws Exception {
		String response = HttpRequestUtil.httpGet(DAO_URL + "api/token/tokenId/" + tokenId);
		JSONObject jsonObject = null;
		if (StringUtils.isNotEmpty(response)) {
			jsonObject = new JSONObject(response);
		} else {
			jsonObject = new JSONObject();
		}
		return jsonObject;
	}
	
}
