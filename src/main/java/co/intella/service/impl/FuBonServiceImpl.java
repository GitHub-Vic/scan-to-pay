package co.intella.service.impl;

import co.intella.constant.Bank;
import co.intella.constant.ServiceType;
import co.intella.domain.*;
import co.intella.domain.focas.CreditCancelAuthReq;
import co.intella.domain.fubon.ChgPermit;
import co.intella.domain.fubon.ECCapture;
import co.intella.domain.fubon.FuBonParamBean;
import co.intella.domain.integration.IntegratedRefundRequestData;
import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.exception.InvokeAPIException;
import co.intella.model.*;
import co.intella.service.*;
import co.intella.utility.FubonUtil;
import co.intella.utility.GeneralUtil;
import co.intella.utility.JsonUtil;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service("fuBonService")
public class FuBonServiceImpl implements PaymentService {

    private final Logger LOGGER = LoggerFactory.getLogger(FuBonServiceImpl.class);

    @Resource
    private PaymentAccountService paymentAccountService;
    @Resource
    private MerchantService merchantService;
    @Resource
    private QrcodeService qrcodeService;
    @Resource
    private AppNotificationService appNotificationService;
    @Resource
    private ApplePayService applePayService;
    @Resource
    private OrderService orderService;
    @Resource
    private TradeProcedureService tradeProcedureService;
    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private RefundDetailService refundDetailService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private RestTemplate restTemplate;

    @Value("${payment.gateway.api.origin}")
    private String PAYMENT_GATEWAY_API_ORIGIN;

//    private String PAYMENT_GATEWAY_API_ORIGIN = "http://34.80.101.124:30393";

    @Value("${api.url}")
    private String API_URL;
    private Locale locale = new Locale("zh_TW");

    @Value("${iCash.TestMode}")
    private String TEST_MODE;

    @Override
    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {

        String generalResponseData = SystemInstance.EMPTY_STRING;
        String integrateMchId = requestHeader.getMerchantId();
        PaymentAccount paymentAccount = paymentAccountService.getOne(integrateMchId, Bank.APPLE_PAY.getId());

        //因富邦目前皆用refund api接口，因此不管為cancel或refund serviceType 皆使用refund
        if (ServiceType.CANCEL.equals(requestHeader.getServiceType())) {

            requestHeader.setServiceType(ServiceType.REFUND);

            LOGGER.info("[FUBON][Refund][data]" + data.getAsString());

            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

            JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
            CreditCancelAuthReq creditCancelAuthReq = new CreditCancelAuthReq();

            creditCancelAuthReq.setLidm(requestData.get("StoreOrderNo").getAsString());
            Gson gson = new Gson();
            String platformResponse = refundPayment(paymentAccount, tradeDetail, gson);

            FuBonParamBean fuBonParamBeanRes = gson.fromJson(platformResponse, FuBonParamBean.class);
            ResponseGeneralHeader responseGeneralHeader = getResponseHeader(fuBonParamBeanRes, requestHeader);

            IntegratedRefundResponseData integratedRefundResponseData = getIntegratedRefundResponseData(
                    fuBonParamBeanRes, requestHeader, tradeDetail, gson.fromJson(data.getAsString(), IntegratedRefundRequestData.class));
            integratedRefundResponseData.setPlatformRsp(platformResponse);

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedRefundResponseData);

            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, platformResponse, data);

            this.fubonECCapture(paymentAccount, tradeDetail, ServiceType.REFUND);

            generalResponseData = gson.toJson(responseGeneralBody);
            LOGGER.info("FUBON integrate response ready :" + generalResponseData);
        }

        return generalResponseData;
    }

    @Override
    public PaymentResponse doApplePayPayment(ApplePayTokenData applePayTokenData, PaymentRequest paymentRequest, String qrcodeId) throws InvokeAPIException {

        LOGGER.info("[APPLE][REQUEST] Token received from tokenService : " + applePayTokenData + ", qrcode=" + qrcodeId);

        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(qrcodeId);

        PaymentRequest paymentRequestData = new PaymentRequest();
        paymentRequestData.getHeader().setServiceType("ApplePay");
        paymentRequestData.getHeader().setMchId(qrcodeParameter.getMchId());
        paymentRequestData.getHeader().setCreateTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        paymentRequestData.getHeader().setMethod(paymentRequest.getHeader().getMethod());

        paymentRequestData.getData().setBody(qrcodeService.getRandomBody(qrcodeParameter));
        FuBonParamBean fuBonParamBean = new FuBonParamBean();

        if (qrcodeParameter.getCodeType().equals("3") || qrcodeParameter.getCodeType().equals("2") || qrcodeParameter.getCodeType().equals("5") || qrcodeParameter.getCodeType().equals("6")) {
            fuBonParamBean.setOrderID(orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter));
        } else if (qrcodeParameter.getCodeType().equals("1")) {
            fuBonParamBean.setOrderID(qrcodeParameter.getStoreOrderNo());
        }
        paymentRequestData.getData().setStoreOrderNo(fuBonParamBean.getOrderID());
        paymentRequestData.getData().setTotalFee(applePayTokenData.getTransactionAmount() / 100);

        TradeDetail tradeDetail = applePayService.setApplepayTradeDetail(paymentRequestData, qrcodeParameter, paymentRequest.getHeader().getMethod());

        if (tradeDetail == null) {
            LOGGER.error("[ERROR][APPLE][OrderIdExist]");
            return null;
        }
        tradeDetail.setMethod("23240");
        tradeDetail.setSystemOrderId(fuBonParamBean.getOrderID());

        PaymentResponse paymentResponse = null;
        try {

            mapPaymentAccountToFuBonParamBean(fuBonParamBean, paymentRequestData, Bank.APPLE_PAY);
            mapApplePayTokenToFuBonParamBean(applePayTokenData, fuBonParamBean);
            LOGGER.info("[doApplePayPayment]   fuBonParamReq" + JsonUtil.toJsonStr(fuBonParamBean));
            FuBonParamBean fuBonParamRes = restTemplate.postForObject(PAYMENT_GATEWAY_API_ORIGIN + "/fubon/api/v1/apple-pay/auth", fuBonParamBean, FuBonParamBean.class);
            LOGGER.info("[doApplePayPayment]   fuBonParamRes" + JsonUtil.toJsonStr(fuBonParamRes));
            paymentResponse = mapToPaymentResponse(fuBonParamRes);
            tradeDetail.setPlatformPaidDate(String.format("%s %s", fuBonParamRes.getTransDate(), fuBonParamRes.getTransTime()));
            tradeDetail.setTxParams(fuBonParamRes.getApproveCode().trim());
            tradeDetail.setUserId(fuBonParamBean.getPAN().substring(0, 6) + "*************" + fuBonParamBean.getPAN().substring(fuBonParamBean.getPAN().length() - 4));
            PaymentAccount paymentAccount = paymentAccountService.getOne(paymentRequest.getHeader().getMchId(), Bank.APPLE_PAY.getId());
            boolean is17Pay = this.checkPaymentAccountIs17Pay(paymentAccount);
            tradeDetail.setComment(is17Pay ? "NewTaipei17Pay" : null);
            applePayService.updateApplePayTradeResult(tradeDetail, paymentResponse);

            if (SystemInstance.TRADE_SUCCESS.equals(paymentResponse.getHeader().getStatusCode())) {
                if (!is17Pay) {
                    paymentAccount = this.configedPermitCode(paymentAccount);
                    this.fubonECCapture(paymentAccount, tradeDetail, "APPLEPAY");
                }
                // TODO do this request async...
                appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
            }
        } catch (Exception e) {
            LOGGER.error("[ERROR][APPLE] " + e.toString());
            throw new InvokeAPIException(this.getClass().getSimpleName(), e.getMessage());
        }

        return paymentResponse;
    }


    private FuBonParamBean mapPaymentAccountToFuBonParamBean(FuBonParamBean fuBonParamBean, PaymentRequest paymentRequest, Bank bank) {

        PaymentAccount paymentAccount = paymentAccountService.getOne(paymentRequest.getHeader().getMchId(), bank.getId());

        fuBonParamBean.setMerchantID(paymentAccount.getAccount());
        fuBonParamBean.setTerminalID(paymentAccount.getAccountTerminalId());
        fuBonParamBean.setPermitCode(paymentAccount.getHashIV());
        return fuBonParamBean;
    }

    private FuBonParamBean mapApplePayTokenToFuBonParamBean(ApplePayTokenData applePayTokenData, FuBonParamBean fuBonParamBean) {

        fuBonParamBean.setPAN(applePayTokenData.getApplicationPrimaryAccountNumber());
        fuBonParamBean.setMobilePayCrypto(applePayTokenData.getPaymentData().getOnlinePaymentCryptogram());
        fuBonParamBean.setMobilePayECI(StringUtils.defaultIfEmpty(applePayTokenData.getPaymentData().getEciIndicator(), ""));
        String expirationDate = applePayTokenData.getApplicationExpirationDate();
        fuBonParamBean.setExpireDate(expirationDate.substring(2, 4) + expirationDate.substring(0, 2)); // YYMMDD -> MMYY
        fuBonParamBean.setTransAmt(applePayTokenData.getTransactionAmount() / 100);
        fuBonParamBean.setCurrencyCode(String.valueOf(Currency.getInstance(Locale.TAIWAN).getNumericCode()));

        return fuBonParamBean;
    }

    private PaymentResponse mapToPaymentResponse(FuBonParamBean fuBonParamRes) {

        PaymentResponse paymentResponse = new PaymentResponse();
        PaymentResponseHeader header = paymentResponse.getHeader();
        PaymentResponseData data = paymentResponse.getData();

        if (FubonUtil.isPaymentApproved(fuBonParamRes)) {
            header.setStatusCode(SystemInstance.TRADE_SUCCESS);
        } else {
            header.setStatusCode(SystemInstance.TRADE_FAIL);
        }

        data.setTotalFee(fuBonParamRes.getTransAmt());
        data.setStoreOrderNo(fuBonParamRes.getOrderID());
        data.setSysOrderNo(fuBonParamRes.getOrderID());
        data.setCallbackUrl(GeneralUtil.getOrderResultUrl(API_URL, fuBonParamRes.getOrderID()));

        return paymentResponse;
    }

    public String refundPayment(PaymentAccount paymentAccount, TradeDetail tradeDetail, Gson gson) {

        Map<String, String> fuBonParam = new HashMap<String, String>();
        fuBonParam.put("merchantID", paymentAccount.getAccount());
        fuBonParam.put("terminalID", paymentAccount.getAccountTerminalId());
        fuBonParam.put("permitCode", paymentAccount.getHashIV());
        fuBonParam.put("orderID", tradeDetail.getOrderId());

        if (tradeDetail.getPlatformPaidDate() != null) {
            String[] dateTime = tradeDetail.getPlatformPaidDate().split(" ");
            if (dateTime != null && dateTime.length == 2) {
                fuBonParam.put("transDate", dateTime[0]); // yyyyMMdd
                fuBonParam.put("transTime", dateTime[1]); // hhmmss
            }
        }

        fuBonParam.put("userDefine", tradeDetail.getTxParams());
        fuBonParam.put("transAmt", String.valueOf(tradeDetail.getPayment()));
        fuBonParam.put("currencyCode", String.valueOf(Currency.getInstance(Locale.TAIWAN).getNumericCode()));

        String result = restTemplate.postForObject(PAYMENT_GATEWAY_API_ORIGIN + "/fubon/api/v1/auth/refund", fuBonParam, String.class);
        LOGGER.info("[refundPayment] result =>" + result);
        return result;
    }

    private ResponseGeneralHeader getResponseHeader(FuBonParamBean fuBonParamBeanRes, RequestHeader requestHeader) {

        ResponseGeneralHeader responseHeader = mapResponseToResponseGeneralHeader(fuBonParamBeanRes, messageSource);
        responseHeader.setMethod(requestHeader.getMethod());
        responseHeader.setMchId(requestHeader.getMerchantId());
        responseHeader.setServiceType(requestHeader.getServiceType());
        responseHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        return responseHeader;
    }

    private ResponseGeneralHeader mapResponseToResponseGeneralHeader(FuBonParamBean fuBonParamRes, MessageSource messageSource) {

        ResponseGeneralHeader responseHeader = new ResponseGeneralHeader();
        if (FubonUtil.isPaymentApproved(fuBonParamRes)) {
            responseHeader.setStatusCode("0000");
            responseHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        } else {
            responseHeader.setStatusCode("9998");
            responseHeader.setStatusDesc(messageSource.getMessage("response.9998", null, locale));
        }
        return responseHeader;
    }

    private IntegratedRefundResponseData getIntegratedRefundResponseData(FuBonParamBean fuBonParamBeanRes, RequestHeader requestHeader, TradeDetail tradeDetail, IntegratedRefundRequestData integratedRefundRequestData) {

        IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();
        if (FubonUtil.isPaymentApproved(fuBonParamBeanRes)) {
            integratedRefundResponseData.setStoreOrderNo(tradeDetail.getOrderId());
            integratedRefundResponseData.setStoreRefundNo(integratedRefundRequestData.getOutRefundNo());
            integratedRefundResponseData.setSysRefundNo(fuBonParamBeanRes.getOrderID());
            integratedRefundResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
            integratedRefundResponseData.setRefundedAt(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        }

        return integratedRefundResponseData;
    }

    @Override
    public boolean checkPaymentAccountIs17Pay(PaymentAccount paymentAccount) {
        return "12|32|38".contains(String.valueOf(paymentAccount.getBank().getBankId())) && "NewTaipei17Pay".equals(paymentAccount.getPlatformCheckCode());
    }


    @Override
    public PaymentAccount configedPermitCode(PaymentAccount paymentAccount) {

        if (StringUtils.isBlank(paymentAccount.getHashKey())) {
            ChgPermit chgPermit = new ChgPermit(TEST_MODE, paymentAccount);
            LOGGER.info("[configedPermitCode]  chgPermit =>" + JsonUtil.toJsonStr(chgPermit));
            String result = restTemplate.postForObject(PAYMENT_GATEWAY_API_ORIGIN + "/fubon/api/v1/auth/permitCode", chgPermit, String.class);
            LOGGER.info("[configedPermitCode]  ChgPermit  response =>" + result);
            ChgPermit responseChgPermit = JsonUtil.parseJson(result, ChgPermit.class);
            if (Objects.nonNull(responseChgPermit) && "0000".equals(responseChgPermit.getResultCode())) {
                final String permitCodeNew = chgPermit.getPermitCodeNew();
                final long bankId = paymentAccount.getBank().getBankId();
                List<PaymentAccount> paymentAccountList = paymentAccountService.listByPayAccount(paymentAccount.getAccount());
                paymentAccountList.parallelStream().filter(this::checkPaymentAccountIs17Pay)
                        .filter(_paymentAccount -> _paymentAccount.getBank().getBankId() == bankId && StringUtils.isBlank(_paymentAccount.getHashKey()))
                        .forEach(_paymentAccount -> {
                            _paymentAccount.setHashKey(permitCodeNew);
                            paymentAccountService.save(_paymentAccount);
                        });
            }
        }

        return paymentAccountService.getOne(paymentAccount.getMerchant().getAccountId(), paymentAccount.getBank().getBankId());
    }

    @Override
    public void fubonECCapture(PaymentAccount paymentAccount, TradeDetail tradeDetail, String methodStr) {
        TradeDetail _tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());
        if (!this.checkPaymentAccountIs17Pay(_tradeDetail.getPaymentAccount())) {
            ECCapture ecCapture = new ECCapture(paymentAccount, _tradeDetail, methodStr);

            LOGGER.info("[fubonECCapture]  ecCapture =>" + JsonUtil.toJsonStr(ecCapture));
            String result = restTemplate.postForObject(PAYMENT_GATEWAY_API_ORIGIN + "/fubon/api/v1/auth/ECCapture", ecCapture, String.class);
            LOGGER.info("[fubonECCapture]  ecCapture  response =>" + result);
            ECCapture responseECCapture = JsonUtil.parseJson(result, ECCapture.class);

            if (Objects.nonNull(responseECCapture) &&
                    ("0000".equals(responseECCapture.getResultCode()) || "E032".equals(responseECCapture.getResultCode()))) {
                if (ServiceType.REFUND.equals(methodStr) && (SystemInstance.TRADE_REFUND_SUCCESS.equals(_tradeDetail.getStatus()) || SystemInstance.TRADE_CANCEL_SUCCESS.equals(_tradeDetail.getStatus()))) {
                    RefundDetail refundDetail = refundDetailService.getTopOneByStoreRefundId(_tradeDetail.getAccountId(), _tradeDetail.getStoreRefundId());
                    refundDetail.setBatchNo(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
                    refundDetailService.save(refundDetail);
                } else {
                    _tradeDetail.setBatchNo(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
                    tradeDetailService.save(_tradeDetail);
                }
            }
        }
    }

    @Override
    public void doECCapture() {
        List<TradeDetail> tradeDetailList = tradeDetailService.getFubonOrderList();

        LOGGER.info("[doECCapture] tradeDetailList size " + tradeDetailList.size());
        int total = tradeDetailList.parallelStream().mapToInt(tradeDetail -> {
                    int c = 0;
                    try {
                        PaymentAccount paymentAccount = this.configedPermitCode(tradeDetail.getPaymentAccount());
                        if (StringUtils.isEmpty(tradeDetail.getBatchNo())) {
                            this.fubonECCapture(paymentAccount, tradeDetail, "doECCapture");
                        }
                        if (SystemInstance.TRADE_REFUND_SUCCESS.equals(tradeDetail.getRefundStatus())) {
                            RefundDetail refundDetail = refundDetailService.getTopOneByStoreRefundId(tradeDetail.getAccountId(), tradeDetail.getStoreRefundId());
                            if (Objects.nonNull(refundDetail) && StringUtils.isEmpty(refundDetail.getBatchNo())) {
                                this.fubonECCapture(paymentAccount, tradeDetail, ServiceType.REFUND);
                            }
                        }
                        c = 1;
                    } catch (Exception e) {
                        LOGGER.error("[doECCapture] error  orderID = " + tradeDetail.getOrderId());
                        LOGGER.error("[doECCapture] [EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
                    }
                    return c;
                }
        ).sum();
        LOGGER.info("[doECCapture]  OK.  sum = " + total);

    }
}
