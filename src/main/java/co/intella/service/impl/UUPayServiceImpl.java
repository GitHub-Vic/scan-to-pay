package co.intella.service.impl;

import co.intella.domain.integration.IntegratedMicropayResponseData;
import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.uupay.*;
import co.intella.exception.ServiceTypeNotSupportException;
import co.intella.model.*;
import co.intella.service.*;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import co.intella.utility.SystemInstance;
import co.intella.utility.UUPayUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;

@Service
public class UUPayServiceImpl implements UUPayService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Autowired
    private Environment env;

    @Autowired
    private QrcodeService qrcodeService;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private PaymentAccountService paymentAccountService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private TradeDetailService tradeDetailService;

    @Autowired
    private AppNotificationService appNotificationService;

    @Autowired
    private MessageSource messageSource;

    private static final Locale locale = new Locale("zh_TW");

    @Value("${host.request.uupay}")
    private String UUPAY_URL;

    @Value("${api.url}")
    private String API_URL;

    @Value("${iPass.TestMode}")
    private String testMode;
    // Prod = 0
    // TestMode = 1

    private static final String ALG = "EC-HMAC-SHA256";
    private static final String CREATE_ORDER_URL = "/createECOrder";
    private static final String CREATE_POS_ORDER_URL = "/createPOSOrder";
    private static final String QUERY_MERCHANT_ORDER = "/queryMerchantOrder";
    private static final String REFUND_EC_ORDER = "/refundECOrder";
    private static final String REFUND_POS_ORDER = "/refundPOSOrder";

    @Override
    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {
        String generalResponseData = SystemInstance.EMPTY_STRING;

        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        Merchant merchant = merchantService.getOne(requestHeader.getMerchantId());
        PaymentAccount paymentAccount = paymentAccountService.getOneByDao(integrateMchId, bankId);
        if (Objects.isNull(paymentAccount)) {
            throw new ServiceTypeNotSupportException("Exception: The store's account isn't opened.");
        }

        String serviceType = requestHeader.getServiceType();
        switch (serviceType) {
            case SystemInstance.TYPE_MICROPAY:
                generalResponseData = this.doMicropay(requestHeader, data, merchant, paymentAccount);
                break;
            case "Refund":
                generalResponseData = this.doRefund(requestHeader, data, merchant, paymentAccount);
                break;
            case "SingleOrderQuery":
                this.queryMerchantOrder(requestHeader, data, merchant, paymentAccount); //由IntegratedController 那邊重新抓取訂單狀態回復
                break;
            default:
                LOGGER.error("Service type not support. " + serviceType);
                generalResponseData = "Service type not support.";
        }

        return generalResponseData;
    }

    @Override
    public String doOLPay(String totalFee, String token) throws Exception {
        String redirectUrl = SystemInstance.EMPTY_STRING;

        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(token);
        Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());
        PaymentAccount paymentAccount = paymentAccountService.getOne(String.valueOf(merchant.getAccountId()), 31);

        if (Objects.isNull(paymentAccount)) {
            throw new ServiceTypeNotSupportException("Exception: The store's account isn't opened.");
        }

        String storeOrderNoNew = this.getStoreOrderNoNew(qrcodeParameter);
        TradeDetail tradeDetail = this.createUUPayTradeDetail(storeOrderNoNew, merchant, totalFee, "13100", qrcodeParameter);

        UUPayOrderItem orderItem = new UUPayOrderItem(qrcodeParameter.getBody(), totalFee, tradeDetail);
        UUPayOrder uuOrder = new UUPayOrder(tradeDetail, paymentAccount);
        uuOrder.setOrderItems(new Object[]{orderItem});
        UUPayOLPayRequest uuOLPAYReq = new UUPayOLPayRequest(API_URL, storeOrderNoNew);
        uuOLPAYReq.setOrder(uuOrder);


        String reqData = JsonUtil.getGson().toJson(uuOLPAYReq);
        LOGGER.info("[UUPay] [doOLPay] request = " + reqData);
        String resData = this.sendReq(reqData, UUPAY_URL + CREATE_ORDER_URL, paymentAccount);
        LOGGER.info("[UUPay] [doOLPay] response = " + resData);

        UUPayGeneralResponse response = JsonUtil.parseJson(resData, UUPayGeneralResponse.class);

        if ("00000".equals(response.getReturnCode())) {
            JsonNode resDataJsonObject = response.getData();
            if (Objects.nonNull(resDataJsonObject)) {
                redirectUrl = resDataJsonObject.has("redirectPaymentUrl") ? resDataJsonObject.get("redirectPaymentUrl").asText() : "";
                LOGGER.info("[UUPay] [doOLPay] redirectUrl = " + redirectUrl);
            }
        }

        return redirectUrl;
    }

    @Override
    public void doNotify(String json, String authorization) throws Exception {
        LOGGER.info("[UUPay] [doNotify] json = " + json);
        LOGGER.info("[UUPay] [doNotify] authorization = " + authorization);
        UUPayNotifyRequest request = JsonUtil.parseJson(json, UUPayNotifyRequest.class);
        TradeDetail tradeDetail = tradeDetailService.getOne(request.getMerchantOrderNo());
        if (tradeDetail != null) {
            String sign = UUPayUtil.getSign(json, tradeDetail.getPaymentAccount());
            LOGGER.info("[UUPay][doNotify] [sign] " + sign);
            if (sign.equalsIgnoreCase(authorization)) {
                try {
                    tradeDetail.setTxParams(json);
                    this.updateUUPayTradeDetail(request, tradeDetail);
                } catch (Exception e) {
                    LOGGER.info("[UUPay][doNotify] [EXCEPTION] " + " updateTradeDetailFail - " + e.getMessage());
                }
            }
        }
    }

    private void queryMerchantOrder(RequestHeader requestHeader, JsonPrimitive data, Merchant merchant, PaymentAccount paymentAccount) throws Exception {
//TODO 退款失敗 訂單狀態？ 交易失敗訂單狀態？
        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
        UUPayQueryRequest uuQueryReq = new UUPayQueryRequest(tradeDetail.getOrderId(), paymentAccount.getAccountTerminalId());

        String reqData = JsonUtil.getGson().toJson(uuQueryReq);
        LOGGER.info("[UUPay] [queryMerchantOrder] request = " + reqData);
        String resData = this.sendReq(reqData, UUPAY_URL + QUERY_MERCHANT_ORDER, paymentAccount);
        LOGGER.info("[UUPay] [queryMerchantOrder] response = " + resData);

        UUPayGeneralResponse response = JsonUtil.parseJson(resData, UUPayGeneralResponse.class);
        if ("00000".equals(response.getReturnCode())) {
            JsonNode resDataJsonObject = response.getData();
            if (Objects.nonNull(resDataJsonObject)) {
                String uupayOrderStatus = resDataJsonObject.has("orderStatus") ? resDataJsonObject.get("orderStatus").asText() : "";
                String orderNo = resDataJsonObject.has("orderNo") ? resDataJsonObject.get("orderNo").asText() : "";
                String refundPaymentNo = resDataJsonObject.has("refundPaymentNo") ? resDataJsonObject.get("refundPaymentNo").asText() : "";
                String status = UUPayUtil.transformUUpayStatus(uupayOrderStatus);

                if (SystemInstance.TRADE_REFUND_SUCCESS.equals(status)) {
                    tradeDetail.setRefundStatus(status);
                    tradeDetail.setSystemRefundId(refundPaymentNo);
                } else if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setSystemOrderId(orderNo);
                    tradeDetail.setStatus(status);
                }
                tradeDetailService.save(tradeDetail);
            }
        }
    }

    private String doMicropay(RequestHeader requestHeader, JsonPrimitive data, Merchant merchant, PaymentAccount paymentAccount) throws Exception {

        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

//        UUPayOrderItem orderItem = new UUPayOrderItem(tradeDetail.getDescription(), String.valueOf(tradeDetail.getPayment()), tradeDetail);
        UUPayOrder uuOrder = new UUPayOrder(tradeDetail, paymentAccount);
        uuOrder.setPaymentBarCode(tradeDetail.getBarcode());
//        uuOrder.setOrderItems(new Object[]{orderItem});
        UUPayMicropayRequest uuOLPAYReq = new UUPayMicropayRequest();
        uuOLPAYReq.setOrder(uuOrder);

        String reqData = JsonUtil.getGson().toJson(uuOLPAYReq);
        LOGGER.info("[UUPay] [doMicropay] request = " + reqData);
        String resData = this.sendReq(reqData, UUPAY_URL + CREATE_POS_ORDER_URL, paymentAccount);
        LOGGER.info("[UUPay] [doMicropay] response = " + resData);

        UUPayGeneralResponse response = JsonUtil.parseJson(resData, UUPayGeneralResponse.class);

        if ("00000".equals(response.getReturnCode())) {
            JsonNode resDataJsonObject = response.getData();
            if (Objects.nonNull(resDataJsonObject)) {
                String uupayOrderStatus = resDataJsonObject.has("orderStatus") ? resDataJsonObject.get("orderStatus").asText() : "";
                String orderNo = resDataJsonObject.has("orderNo") ? resDataJsonObject.get("orderNo").asText() : "";
                String stauts = UUPayUtil.transformUUpayStatus(uupayOrderStatus);
                tradeDetail.setStatus(stauts);
                tradeDetail.setSystemOrderId(orderNo);
            }
        } else if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
        }
        tradeDetail = tradeDetailService.save(tradeDetail);
        if (SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus()))
            appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));

        ResponseGeneralBody responseGeneralBody = UUPayUtil.getResponseBody(requestHeader, tradeDetail, response.getReturnCode(), messageSource);
        IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();
        integratedMicropayResponseData.setPlatformRsp(resData);
        integratedMicropayResponseData.setAuthCode(tradeDetail.getBarcode());
        integratedMicropayResponseData.setStoreOrderNo(tradeDetail.getOrderId());
        integratedMicropayResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
        integratedMicropayResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
        responseGeneralBody.setData(integratedMicropayResponseData);

        return JsonUtil.toJsonStr(responseGeneralBody);

    }

    private String doRefund(RequestHeader requestHeader, JsonPrimitive data, Merchant merchant, PaymentAccount paymentAccount) throws Exception {
        String generalResponseData = SystemInstance.EMPTY_STRING;
        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

        if (SystemInstance.TYPE_MICROPAY.equals(tradeDetail.getServiceType())) {
            generalResponseData = this.refundPOSOrder(requestHeader, tradeDetail, merchant, paymentAccount, new Gson().fromJson(data.getAsString(), JsonObject.class));
        } else {
            generalResponseData = this.refundECOrder(requestHeader, tradeDetail, merchant, paymentAccount, new Gson().fromJson(data.getAsString(), JsonObject.class));
        }

        return generalResponseData;
    }

    private String refundECOrder(RequestHeader requestHeader, TradeDetail tradeDetail, Merchant merchant, PaymentAccount paymentAccount, JsonObject data) throws Exception {

        UUPayRefundECRequest refundECRequest = new UUPayRefundECRequest(tradeDetail, paymentAccount, testMode);
        String reqData = JsonUtil.getGson().toJson(refundECRequest);
        LOGGER.info("[UUPay] [refundECOrder] request = " + reqData);
        String resData = this.sendReq(reqData, UUPAY_URL + REFUND_EC_ORDER, paymentAccount);
        LOGGER.info("[UUPay] [refundECOrder] response = " + resData);

        UUPayGeneralResponse response = JsonUtil.parseJson(resData, UUPayGeneralResponse.class);

        if ("00000".equals(response.getReturnCode())) {
            JsonNode resDataJsonObject = response.getData();
            if (Objects.nonNull(resDataJsonObject)) {
                String uupayOrderStatus = resDataJsonObject.has("orderStatus") ? resDataJsonObject.get("orderStatus").asText() : "";
                String refundPaymentNo = resDataJsonObject.has("refundPaymentNo") ? resDataJsonObject.get("refundPaymentNo").asText() : "";
                String status = UUPayUtil.transformUUpayStatus(uupayOrderStatus);
                if (SystemInstance.TRADE_REFUND_SUCCESS.equals(status)) {
                    if (StringUtils.isNotBlank(refundPaymentNo)) {
                        tradeDetail.setSystemRefundId(refundPaymentNo);
                    }
                    tradeDetail.setRefundStatus(status);
                } else if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
                }
            }
        } else if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
        }

        tradeDetail = tradeDetailService.save(tradeDetail);
        tradeProcedureService.updateRefundDeatil(tradeDetail, data, tradeDetail.getRefundStatus());
        ResponseGeneralBody responseGeneralBody = UUPayUtil.getResponseBody(requestHeader, tradeDetail, response.getReturnCode(), messageSource);
        if ("00000".equals(response.getReturnCode()))
            responseGeneralBody.setData(this.getIntegratedRefundResponseData(tradeDetail, resData));

        return JsonUtil.toJsonStr(responseGeneralBody);
    }

    private String refundPOSOrder(RequestHeader requestHeader, TradeDetail tradeDetail, Merchant merchant, PaymentAccount paymentAccount, JsonObject data) throws Exception {
        UUPayRefundPOSRequest refundECRequest = new UUPayRefundPOSRequest(tradeDetail, paymentAccount);
        String reqData = JsonUtil.getGson().toJson(refundECRequest);
        LOGGER.info("[UUPay] [refundPOSOrder] request = " + reqData);
        String resData = this.sendReq(reqData, UUPAY_URL + REFUND_POS_ORDER, paymentAccount);
        LOGGER.info("[UUPay] [refundPOSOrder] response = " + resData);

        UUPayGeneralResponse response = JsonUtil.parseJson(resData, UUPayGeneralResponse.class);

        if ("00000".equals(response.getReturnCode())) {
            JsonNode resDataJsonObject = response.getData();
            if (Objects.nonNull(resDataJsonObject)) {
                String uupayOrderStatus = resDataJsonObject.has("orderStatus") ? resDataJsonObject.get("orderStatus").asText() : "";
                String refundPaymentNo = resDataJsonObject.has("refundPaymentNo") ? resDataJsonObject.get("refundPaymentNo").asText() : "";
                String status = UUPayUtil.transformUUpayStatus(uupayOrderStatus);
                if (SystemInstance.TRADE_REFUND_SUCCESS.equals(status)) {
                    if (StringUtils.isNotBlank(refundPaymentNo)) {
                        tradeDetail.setSystemRefundId(refundPaymentNo);
                    }
                    tradeDetail.setRefundStatus(status);
                } else {
                    tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
                }
            }
        } else if ("20129".equals(response.getReturnCode())) {
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
        } else if (!SystemInstance.TRADE_REFUND_SUCCESS.equals(tradeDetail.getRefundStatus())) {
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
        }

        tradeDetail = tradeDetailService.save(tradeDetail);
        tradeProcedureService.updateRefundDeatil(tradeDetail, data, tradeDetail.getRefundStatus());
        ResponseGeneralBody responseGeneralBody = UUPayUtil.getResponseBody(requestHeader, tradeDetail, response.getReturnCode(), messageSource);
        if ("00000".equals(response.getReturnCode()))
            responseGeneralBody.setData(this.getIntegratedRefundResponseData(tradeDetail, resData));

        return JsonUtil.toJsonStr(responseGeneralBody);
    }


    private IntegratedRefundResponseData getIntegratedRefundResponseData(TradeDetail tradeDetail, String respJson) {
        IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();
        integratedRefundResponseData.setPlatformRsp(respJson);
        integratedRefundResponseData.setRefundedAt(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        integratedRefundResponseData.setStoreOrderNo(tradeDetail.getOrderId());
        integratedRefundResponseData.setStoreRefundNo(tradeDetail.getStoreRefundId());
        integratedRefundResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
        integratedRefundResponseData.setSysRefundNo(tradeDetail.getSystemRefundId());
        return integratedRefundResponseData;
    }

    private String getStoreOrderNoNew(QrcodeParameter qrcodeParameter) {
        String storeOrderNoNew;
        if (qrcodeParameter.getCodeType().equals("1")) {
            storeOrderNoNew = qrcodeParameter.getStoreOrderNo();
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            storeOrderNoNew = orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            storeOrderNoNew = orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
        } else {
            storeOrderNoNew = orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
        }
        return storeOrderNoNew;
    }

    private TradeDetail createUUPayTradeDetail(String storeOrderNoNew, Merchant merchant, String totalFee, String method, QrcodeParameter qrcodeParameter) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        LocalDateTime time = LocalDateTime.now();
        String nowDate = df.format(time);
        String bodyRandom = String.format("%04d", new Random().nextInt(9999));
        String bankId = method.substring(1, 3);
        TradeDetail tradeDetail = new TradeDetail();
        tradeDetail.setMethod(method);
        tradeDetail.setServiceType("OLPay");
        tradeDetail.setAccountId(merchant.getAccountId());
        tradeDetail.setCreateDate(nowDate);
        tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
        tradeDetail.setOrderId(storeOrderNoNew);
        tradeDetail.setDeviceRandomId(deviceService.getOne(merchant, "APP-001"));
        tradeDetail.setPaymentAccount(paymentAccountService.getOne(String.valueOf(merchant.getAccountId()), Long.parseLong(bankId)));
        tradeDetail.setPayment(Long.parseLong(totalFee));
        tradeDetail.setDescription(qrcodeParameter.getBody());
        tradeDetail.setQrcodeToken(qrcodeParameter.getShortId());
        tradeDetail.setOnSale(false);
        tradeDetail.setOriginalPrice(Double.parseDouble(totalFee));
        tradeDetail = tradeDetailService.save(tradeDetail);
        LOGGER.info("[UUPay" + bankId + "] TradeDetail save success");
        return tradeDetail;
    }

    public void updateUUPayTradeDetail(UUPayNotifyRequest request, TradeDetail tradeDetail) {

        String status = "";
        if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
            tradeDetail.setPlatformPaidDate(request.getOrderCreateDateTime());
            tradeDetail.setSystemOrderId(StringUtils.defaultString(request.getOrderNo(), ""));
            if (tradeDetail.getPayment() == Double.valueOf(request.getOrderAmount()).longValue()) {
                status = UUPayUtil.transformUUpayStatus(request.getOrderStatus());
                tradeDetail.setStatus(status);
            }
        }
        tradeDetailService.save(tradeDetail);

        if (SystemInstance.TRADE_SUCCESS.equals(status)) {
            appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
        }
    }

    private String sendReq(String reqJson, String url, PaymentAccount paymentAccount) throws Exception {
        HashMap<String, String> header = new HashMap<>();
        header.put("Authorization", UUPayUtil.encyption(reqJson, paymentAccount));
        String resData = HttpUtil.doPostJson(header, url, reqJson);

        return resData;
    }

}
