package co.intella.service.impl;

import co.intella.constant.Bank;
import co.intella.domain.integration.IntegratedMicropayResponseData;
import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.domain.newPi.*;
import co.intella.exception.OtherAPIException;
import co.intella.exception.ServiceTypeNotSupportException;
import co.intella.model.*;
import co.intella.service.*;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import co.intella.utility.OrderUtil;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

@Service
public class NewPiServiceImpl implements NewPiService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Resource
    private MerchantService merchantService;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private OrderUtil orderUtil;

    @Autowired
    private Environment env;

    private Locale locale = new Locale("zh_TW");

    @Autowired
    private MessageSource messageSource;

    @Value("${new.pi.url}")
    String PI_URL;

    @Value("${host.pi.offline.url}")
    private String PI_OFFLINE_API_URL;

    @Value("${api.url}")
    private String API_URL;

    @Override
    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {
        String generalResponseData = SystemInstance.EMPTY_STRING;

        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        Merchant merchant = merchantService.getOne(requestHeader.getMerchantId());
        PaymentAccount paymentAccount = paymentAccountService.getOneByDao(integrateMchId, bankId);
        if (Objects.isNull(paymentAccount)) {
            throw new ServiceTypeNotSupportException("Exception: The store's account isn't opened.");
        }

        String serviceType = requestHeader.getServiceType();
        switch (serviceType) {
            case SystemInstance.TYPE_MICROPAY:
                generalResponseData = this.doMicropay(requestHeader, data, merchant, paymentAccount);
                break;
            case "Refund":
                generalResponseData = this.doRefund(requestHeader, data, merchant, paymentAccount);
                break;
            case "SingleOrderQuery":
                this.queryMerchantOrder(requestHeader, data, merchant, paymentAccount); //由IntegratedController 那邊重新抓取訂單狀態回復
                break;
            default:
                LOGGER.error("Service type not support. " + serviceType);
                generalResponseData = "Service type not support.";
        }

        return generalResponseData;
    }

    @Override
    public String getPaymentUrl(String amount, String shortId, String agent) throws Exception {
        //      notify 由PI設定，API：/api/pi/paymentNotify
        String result = "";
        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(shortId);

        if (Objects.nonNull(qrcodeParameter)) {
            Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());
            PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), Bank.NEW_PI.getId());

            String nowStr = LocalDateTime.now().format(DateTimeFormatter.ofPattern(SystemInstance.DATE_PATTERN));
            if (StringUtils.isNotBlank(qrcodeParameter.getTimeExpire()) && Long.valueOf(nowStr) > Long.valueOf(qrcodeParameter.getTimeExpire())) {
                throw new OtherAPIException("QrCode is invalid");
            }
            if (Objects.isNull(paymentAccount)) {
                throw new ServiceTypeNotSupportException("Exception: The store's account isn't opened.");
            }

            TradeDetail tradeDetail = orderUtil.setOnlineTradeDetailRequest(amount, merchant, paymentAccount, qrcodeParameter, "15900");
            CreateRequest createRequest = new CreateRequest(paymentAccount, tradeDetail,API_URL);

            String requestJsonStr = JsonUtil.toJsonStr(createRequest);
            LOGGER.info("[NewPi][getPaymentUrl]  requestJsonStr" + requestJsonStr);
            String resultStr = HttpUtil.doPostJson(PI_URL + "/bill/create", requestJsonStr);
            LOGGER.info("[NewPi][getPaymentUrl]  resultStr" + resultStr);

            CreateResponse createResponse = JsonUtil.parseJson(resultStr, CreateResponse.class);
            if (Objects.nonNull(createResponse) && "0000".equals(createResponse.getReturn_code())) {
                CreateResponse.TransData transData = createResponse.getTrans_data();
                if (Objects.nonNull(transData)) {
                    result = transData.getIos_url();
                }
            }
        } else {
            LOGGER.error("[REQUEST][GamaPay] qrcode not found");
        }

        return result;
    }

    @Override
    public void updateAuthorization(PaymentAccount paymentAccount) {

        AuthorizationRequest authorizationRequest = new AuthorizationRequest(paymentAccount);

        String requestJsonStr = JsonUtil.toJsonStr(authorizationRequest);
        LOGGER.info("[NewPi][updateAuthorization]  requestJsonStr" + requestJsonStr);
        String resultStr = HttpUtil.doPostJson(PI_URL + "/authorization", requestJsonStr);
        LOGGER.info("[NewPi][updateAuthorization]  resultStr" + resultStr);

        AuthorizationResponse authorizationResponse = JsonUtil.parseJson(resultStr, AuthorizationResponse.class);

        if (Objects.nonNull(authorizationResponse) && "0000".equals(authorizationResponse.getReturn_code())) {
            AuthorizationResponse.AuthData authData = authorizationResponse.getAuth_data();
            if (Objects.nonNull(authData)) {
                final String token = authData.getToken();
                List<PaymentAccount> paymentAccountList = paymentAccountService.listByPayAccount(paymentAccount.getAccount());
                paymentAccountList.stream().peek(pa -> pa.setAccountTerminalId(token)).forEach(pa ->
                        paymentAccountService.save(pa)
                );
            }
        }
    }

    private String doMicropay(RequestHeader requestHeader, JsonPrimitive data, Merchant merchant, PaymentAccount paymentAccount) throws Exception {
        String mchId = requestHeader.getMerchantId();
        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
        requestHeader.setMerchantId(mchId);

        MicropayRequest micropayRequest = new MicropayRequest(tradeDetail, paymentAccount, new Gson().fromJson(data.getAsString(), JsonObject.class));

        String requestJsonStr = JsonUtil.toJsonStr(micropayRequest);
        LOGGER.info("[NewPi][doMicropay]  requestJsonStr" + requestJsonStr);
        String resultStr = HttpUtil.doPostJson(PI_OFFLINE_API_URL + "pay", requestJsonStr);
        LOGGER.info("[NewPi][doMicropay]  resultStr" + resultStr);

        MicropayResponse micropayResponse = JsonUtil.parseJson(resultStr, MicropayResponse.class);
        if ("0000".equals(micropayResponse.getError_code()) && "success".equals(micropayResponse.getStatus())) {
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetail.setSystemOrderId(micropayResponse.getWallet_transaction_id());
            appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
        } else {
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
        }

        tradeDetailService.save(tradeDetail);

        ResponseGeneralBody responseGeneralBody = this.getResponseBody(requestHeader, tradeDetail, micropayResponse.getError_code());
        IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();
        integratedMicropayResponseData.setPlatformRsp(micropayResponse.getTransaction_time());
        integratedMicropayResponseData.setAuthCode(tradeDetail.getBarcode());
        integratedMicropayResponseData.setStoreOrderNo(tradeDetail.getOrderId());
        integratedMicropayResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
        integratedMicropayResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
        responseGeneralBody.setData(integratedMicropayResponseData);

        return JsonUtil.toJsonStr(responseGeneralBody);
    }

    private String doRefund(RequestHeader requestHeader, JsonPrimitive data, Merchant merchant, PaymentAccount paymentAccount) throws Exception {
        //TODO  黃的明顯 ========================    有分 主被掃退款
        String mchId = requestHeader.getMerchantId();
        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
        requestHeader.setMerchantId(mchId);

        if (SystemInstance.TYPE_MICROPAY.equalsIgnoreCase(tradeDetail.getServiceType())) {
            return this.refundOffLine(paymentAccount, merchant, requestHeader, data, tradeDetail);
        } else {
            return this.refundOnLine(paymentAccount, merchant, requestHeader, data, tradeDetail);
        }
    }

    private String refundOffLine(PaymentAccount paymentAccount, Merchant merchant, RequestHeader requestHeader, JsonPrimitive data, TradeDetail tradeDetail) throws Exception {

        RefundMicropayRequest refundMicropayRequest = new RefundMicropayRequest(tradeDetail, paymentAccount);
        String requestJsonStr = JsonUtil.toJsonStr(refundMicropayRequest);
        LOGGER.info("[NewPi][refundOffLine]  requestJsonStr" + requestJsonStr);
        String resultStr = HttpUtil.doPostJson(PI_OFFLINE_API_URL + "refund", requestJsonStr);
        LOGGER.info("[NewPi][refundOffLine]  resultStr" + resultStr);

        RefundMicropayResponse refundMicropayResponse = JsonUtil.parseJson(resultStr, RefundMicropayResponse.class);

        if ("0000".equals(refundMicropayResponse.getError_code()) && "success".equals(refundMicropayResponse.getStatus())) {
            if ("refunded".equals(refundMicropayResponse.getTrans_status())) {
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                tradeDetail.setSystemRefundId(refundMicropayResponse.getWallet_transaction_id());
            } else {
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
            }
        } else {
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
        }
        tradeDetailService.save(tradeDetail);
        tradeProcedureService.updateRefundDeatil(tradeDetail, new Gson().fromJson(data.getAsString(), JsonObject.class), tradeDetail.getRefundStatus());

        //TODO 以下建立回應這部分，之後要整理起來
        IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();

        if ("refunded".equals(refundMicropayResponse.getTrans_status())) {
            integratedRefundResponseData.setSysRefundNo(tradeDetail.getSystemRefundId());
            integratedRefundResponseData.setRefundedAt(LocalDateTime.now().format(DateTimeFormatter.ofPattern(SystemInstance.DATE_PATTERN)));
        } else {
            integratedRefundResponseData.setSysRefundNo(SystemInstance.EMPTY_STRING);
            integratedRefundResponseData.setRefundedAt(SystemInstance.EMPTY_STRING);
        }
        integratedRefundResponseData.setStoreRefundNo(tradeDetail.getStoreRefundId());
        integratedRefundResponseData.setStoreOrderNo(tradeDetail.getOrderId());
        integratedRefundResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());


        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setMchId(tradeDetail.getAccountId());
        responseGeneralHeader.setMethod(tradeDetail.getMethod());
        responseGeneralHeader.setServiceType("Refund");
        responseGeneralHeader.setStatusCode(SystemInstance.STATUS_SUCCESS);
        responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        responseGeneralBody.setHeader(responseGeneralHeader);
        responseGeneralBody.setData(integratedRefundResponseData);

        String generalResponseData = new Gson().toJson(responseGeneralBody);

        return generalResponseData;
    }

    private String refundOnLine(PaymentAccount paymentAccount, Merchant merchant, RequestHeader requestHeader, JsonPrimitive data, TradeDetail tradeDetail) throws Exception {
        RefundRequest refundRequest = new RefundRequest(tradeDetail, paymentAccount);

        String requestJsonStr = JsonUtil.toJsonStr(refundRequest);
        LOGGER.info("[NewPi][refundOnLine]  requestJsonStr" + requestJsonStr);
        String resultStr = HttpUtil.doPostJson(PI_URL + "/order/refund", requestJsonStr);
        LOGGER.info("[NewPi][refundOnLine]  resultStr" + resultStr);

        RefundResponse refundResponse = JsonUtil.parseJson(resultStr, RefundResponse.class);

        if ("0000".equals(refundResponse.getReturn_code())) {
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
            tradeDetail.setSystemRefundId(refundResponse.getRefunded_data().getRefund_id());
        } else {
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
        }
        tradeDetailService.save(tradeDetail);
        tradeProcedureService.updateRefundDeatil(tradeDetail, new Gson().fromJson(data.getAsString(), JsonObject.class), tradeDetail.getRefundStatus());

        //TODO 以下建立回應這部分，之後要整理起來
        IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();

        if ("0000".equals(refundResponse.getReturn_code())) {
            integratedRefundResponseData.setSysRefundNo(tradeDetail.getSystemRefundId());
            integratedRefundResponseData.setRefundedAt(LocalDateTime.now().format(DateTimeFormatter.ofPattern(SystemInstance.DATE_PATTERN)));
        } else {
            integratedRefundResponseData.setSysRefundNo(SystemInstance.EMPTY_STRING);
            integratedRefundResponseData.setRefundedAt(SystemInstance.EMPTY_STRING);
        }
        integratedRefundResponseData.setStoreRefundNo(tradeDetail.getStoreRefundId());
        integratedRefundResponseData.setStoreOrderNo(tradeDetail.getOrderId());
        integratedRefundResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());


        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setMchId(tradeDetail.getAccountId());
        responseGeneralHeader.setMethod(tradeDetail.getMethod());
        responseGeneralHeader.setServiceType("Refund");
        responseGeneralHeader.setStatusCode(SystemInstance.STATUS_SUCCESS);
        responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        responseGeneralBody.setHeader(responseGeneralHeader);
        responseGeneralBody.setData(integratedRefundResponseData);

        String generalResponseData = new Gson().toJson(responseGeneralBody);

        return generalResponseData;
    }


    private void queryMerchantOrder(RequestHeader requestHeader, JsonPrimitive data, Merchant merchant, PaymentAccount paymentAccount) throws Exception {
        //TODO  黃的明顯 ========================      有分 主被掃查單
        String mchId = requestHeader.getMerchantId();
        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
        requestHeader.setMerchantId(mchId);

        if (SystemInstance.TYPE_MICROPAY.equalsIgnoreCase(tradeDetail.getServiceType())) {
            this.orderQueryMicropay(paymentAccount, merchant, requestHeader, data, tradeDetail);
        } else {
            this.orderQueryOLPay(paymentAccount, merchant, requestHeader, data, tradeDetail);
        }
    }

    private void orderQueryMicropay(PaymentAccount paymentAccount, Merchant merchant, RequestHeader requestHeader, JsonPrimitive data, TradeDetail tradeDetail) throws Exception {

        OrderQueryMicropayRequest queryMicropayRequest = new OrderQueryMicropayRequest(tradeDetail, paymentAccount);

        String requestJsonStr = JsonUtil.toJsonStr(queryMicropayRequest);
        LOGGER.info("[NewPi][orderQueryMicropay]  requestJsonStr" + requestJsonStr);
        String resultStr = HttpUtil.doPostJson(PI_OFFLINE_API_URL + "ask", requestJsonStr);
        LOGGER.info("[NewPi][orderQueryMicropay]  resultStr" + resultStr);

        OrderQueryMicropayResponse queryMicropayResponse = JsonUtil.parseJson(resultStr, OrderQueryMicropayResponse.class);
        if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
            if ("0000".equals(queryMicropayResponse.getError_code()) && "success".equals(queryMicropayResponse.getStatus())) {
                String status = queryMicropayResponse.getTrans_status();
                switch (status) {
                    case "created":
                        tradeDetail.setStatus(SystemInstance.TRADE_CREATING_ORDER);
                        break;
                    case "accepted":
                        tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                        tradeDetail.setSystemOrderId(queryMicropayResponse.getWallet_transaction_id());
                        break;
                    case "canceled":
                    case "refunded":
                        tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                        tradeDetail.setSystemRefundId(queryMicropayResponse.getWallet_transaction_id());
                        tradeProcedureService.updateRefundDeatil(tradeDetail, new Gson().fromJson(data.getAsString(), JsonObject.class), SystemInstance.TRADE_REFUND_SUCCESS);
                        break;
                    default:
                        tradeDetail.setRefundStatus(SystemInstance.TRADE_FAIL);
                }
                tradeDetailService.save(tradeDetail);
            }
        }
    }

    private void orderQueryOLPay(PaymentAccount paymentAccount, Merchant merchant, RequestHeader requestHeader, JsonPrimitive data, TradeDetail tradeDetail) throws Exception {

        StatusRequest statusRequest = new StatusRequest(paymentAccount, tradeDetail);

        String requestJsonStr = JsonUtil.toJsonStr(statusRequest);
        LOGGER.info("[NewPi][orderQueryOLPay]  requestJsonStr" + requestJsonStr);
        String resultStr = HttpUtil.doPostJson(PI_URL + "/order/status", requestJsonStr);
        LOGGER.info("[NewPi][orderQueryOLPay]  resultStr" + resultStr);

        StatusResponse statusResponse = JsonUtil.parseJson(resultStr, StatusResponse.class);

        StatusResponse.OrderData orderData = statusResponse.getOrder_data();
        String piStatus = orderData.getStatus();

        if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
            if ("success".equals(piStatus) || "completed".equals(piStatus)) {
                // 交易玩成，授權完成          交易完成，請款完成
                tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                tradeDetail.setSystemOrderId(orderData.getTransaction_id());
            } else if ("failure".equals(piStatus) || "expired".equals(piStatus) || "unexpected_error".equals(piStatus)) {
                //     交易失敗                     交易已過期                      預期外錯誤
                tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            } else if ("created".equals(piStatus) || "paying".equals(piStatus)) {
                //   建立完成，尚未付款             交易付款中
                tradeDetail.setStatus(SystemInstance.TRADE_CREATING_ORDER);
            }
        } else if ("refunded".equals(piStatus)) {
            //  交易已退款
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
            tradeDetail.setSystemRefundId(orderData.getRefunded_info().get(0).getRefund_id());
            tradeProcedureService.updateRefundDeatil(tradeDetail, new Gson().fromJson(data.getAsString(), JsonObject.class), SystemInstance.TRADE_REFUND_SUCCESS);
        }
        tradeDetailService.save(tradeDetail);
    }

    private ResponseGeneralBody getResponseBody(RequestHeader requestHeader, TradeDetail tradeDetail, String returnCode) {
        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        ResponseGeneralHeader header = new ResponseGeneralHeader();

        String intellaCode = this.changeScan2payErrCode(returnCode);
        header.setStatusCode(intellaCode);
        header.setStatusDesc(messageSource.getMessage("response." + intellaCode, null, locale));
        header.setMethod(tradeDetail.getMethod());
        header.setMchId(tradeDetail.getPaymentAccount().getMerchant().getAccountId());
        header.setServiceType(requestHeader.getServiceType());
        header.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        responseGeneralBody.setHeader(header);

        return responseGeneralBody;
    }

    private String changeScan2payErrCode(String hnErrCode) {
        String scan2PayCode;
        switch (hnErrCode) {
            case "0000":
                scan2PayCode = SystemInstance.STATUS_SUCCESS;
                break;
            case "2001":
                scan2PayCode = "7120";
                break;
            default:
                scan2PayCode = "9998";
                break;
        }
        return scan2PayCode;
    }

}
