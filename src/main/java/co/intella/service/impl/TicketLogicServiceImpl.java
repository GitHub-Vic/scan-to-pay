package co.intella.service.impl;

import co.intella.constant.TicketLogin;
import co.intella.constant.TicketPayment;
import co.intella.domain.acertaxi.AcerTaxiPaymentResponse;
import co.intella.domain.acertaxi.EzCardEDCAPaymentReq;
import co.intella.domain.easycard.*;
import co.intella.domain.iPass.IPassBaseResponse;
import co.intella.domain.iPass.IPassOfflineInquiryCardRequest;
import co.intella.domain.iPass.IPassTradeRefundRequest;
import co.intella.domain.iPass.IPassTradeSaleRequest;
import co.intella.domain.icash.*;
import co.intella.domain.ticket.TicketFastTransRequest;
import co.intella.domain.ticket.TicketFastTransResponse;
import co.intella.domain.ticket.TicketFindCardRequest;
import co.intella.domain.ticket.TicketFindCardResponse;
import co.intella.exception.*;
import co.intella.model.*;
import co.intella.net.HttpRequestUtil;
import co.intella.service.*;
import co.intella.utility.HttpUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import javax.annotation.Nonnull;
import javax.annotation.Resource;
import javax.mail.MessagingException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class TicketLogicServiceImpl implements TicketLogicService {

    private final Logger LOGGER = LoggerFactory.getLogger(TicketLogicServiceImpl.class);

    private Locale locale = new Locale("zh_TW");

    @Value("${host.dao}")
    private String HOST_DAO;

    @Value("${host.scan2pay.api}")
    private String HOST_SCAN2PAY_API;

    @Value("${intella.public.key}")
    private String PUBLIC_KEY;

    @Resource
    private MailService mailService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private LookupCodeService lookupCodeService;

    @Resource
    private IPassService iPassService;

    @Resource
    private EzCardService ezCardService;

    @Resource
    private ICashService iCashService;

    @Autowired
    private MessageSource messageSource;

    @Resource
    private DeviceService deviceService;

    @Resource
    private AbnormalRuleService abnormalRuleService;

    @Resource
    private AbnormalRuleCtrlService abnormalRuleCtrlService;

    @Resource
    private TaxiTransactionDetailService taxiTransactionDetailService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private ReserveDetailService reserveDetailService;

    @Resource
    private ReserveCancelService reserveCancelService;

    @Resource
    private TsmcService tsmcService;

    @Value("${host.ticket.url}")
    String TICKET_URL;

    //TODO
    private static final List<String> signOnList = Arrays.asList("SignOn", "EDCASignOn", "TaxiLogin");

//    private static final List<Long> ticketBankList = Arrays.asList(18L, 21L, 28L, 37L);

    @Override
    public String doRequest(RequestHeader header, JsonObject requestData) throws Exception {

        String result;
        Merchant merchant = merchantService.getOne(header.getMerchantId());
        if (Objects.isNull(merchant)) {
            throw new NoSuchMerchantException("Merchant error");
        }

        String deviceId = requestData.get("DeviceId").getAsString();
        Device device = deviceService.getOne(merchant, deviceId);

        //TODO   新的API  尋卡+交易，預設SwVersion = 1.0   ，之後新的非1.0則可以直接尋卡+交易
        if (Objects.isNull(device) || "taxi".equals(device.getType()) || "1.0".equals(device.getSwVersion())) {
            result = this.doRequest_Old(header, requestData, merchant, device);
        } else {
            result = this.doRequest_New(header, requestData, merchant, device);
        }
        return result;
    }

    @Override
    public List<TaxiTransactionDetail> getAbnormalRuleMsgByReport(String startDate, String endDate, String terminalId) {

        final List<IntTbAbnormalRule> abRList = abnormalRuleService.findAll();
        final List<TaxiTransactionDetail> cycleList = taxiTransactionDetailService.getCycleList(startDate, endDate, "", "", terminalId);

        return cycleList.parallelStream().filter(ttd -> StringUtils.isNotBlank(ttd.getLicensePlate()) && StringUtils.isNotBlank(ttd.getCardId()))
                .peek(ttd -> {
                    if (Objects.isNull(ttd.getTransMsg()) && Objects.nonNull(ttd.getArcSeq()) && ttd.getArcSeq() > 0) {
                        ttd.setTransMsg(verificationRule(
                                cycleList.parallelStream().filter(ttd2 -> ttd.getLicensePlate().equals(ttd2.getLicensePlate()) && ttd.getCardId().equals(ttd2.getCardId()))
                                , ttd.getSystemTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()
                                , abRList.parallelStream().filter(abr -> abr.getArcSeq().intValue() == ttd.getArcSeq().intValue()).collect(Collectors.toList())
                        ));
                    }
                    ttd.setCardType(ttd.getTransactionType() + ttd.getCardArea() + ttd.getCardType() + ttd.getIdentityInfo());
                }).collect(Collectors.toList());
    }


    @Override
    public String getAbnormalRuleMsg(final List<IntTbAbnormalRule> abRList, String licensePlate, String cardId) {

        final LocalDateTime today = LocalDateTime.now();
        final LocalDate firstday = LocalDate.of(today.getYear(), today.getMonth(), 1);
        List<TaxiTransactionDetail> cycleList = taxiTransactionDetailService.getCycleList(firstday.format(DateTimeFormatter.ofPattern("yyyyMMdd"))
                , today.toLocalDate().format(DateTimeFormatter.ofPattern("yyyyMMdd")), licensePlate, cardId, "");

        return verificationRule(cycleList.parallelStream(), today, abRList);
    }

    /**
     * 報表跟交易共用，驗證營運規則
     *
     * @param ttdListAll    同月同車同卡
     * @param localDateTime 比較基準時間
     * @param abRList       營運規則List
     * @return 錯誤信息字串或空字串
     */
    private String verificationRule(Stream<TaxiTransactionDetail> ttdListAll, final LocalDateTime localDateTime, final List<IntTbAbnormalRule> abRList) {

        final List<TaxiTransactionDetail> ttdList = ttdListAll.filter(ttd -> {
            LocalDateTime orderDateTime = ttd.getSystemTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            return ttd.getCarePoint() > 0 && orderDateTime.isBefore(localDateTime);
        }).collect(Collectors.toList());

        return abRList.stream().map(abr -> {
            String resule = "";
            switch (abr.getAbnormalRule()) {
                case "RULE001":
                    if (verificationRule001(ttdList, localDateTime, abr) > 0) {
                        resule = abr.getType1().replace("{parameter1}", abr.getParameter1());
                    }
                    break;
                case "RULE002":
                    if (verificationRule002(ttdList, localDateTime) >= Integer.parseInt(abr.getParameter1())) {
                        resule = abr.getType1().replace("{parameter1}", abr.getParameter1());
                    }
                    break;
                case "RULE003":
                    if (verificationRule003(ttdList, localDateTime, abr) >= Integer.parseInt(abr.getParameter2())) {
                        resule = abr.getType1().replace("{parameter2}", abr.getParameter2());
                    }
                    break;
            }
            return resule;
        }).filter(StringUtils::isNotBlank).collect(Collectors.joining(","));
    }


    /**
     * 驗證001營運規則
     * 同一車輛，同一卡號，連續刷卡間隔小於{mins}分鐘 筆數
     *
     * @param ttdList       同月同車同卡
     * @param localDateTime 比較基準時間
     * @param abr           營運規則
     * @return int 不合規則總筆數
     */
    private int verificationRule001(final List<TaxiTransactionDetail> ttdList, final LocalDateTime localDateTime, final IntTbAbnormalRule abr) {

        final LocalDate localDate = localDateTime.toLocalDate();
        final long min = Long.valueOf(abr.getParameter1());

        long count = ttdList.stream().filter(ttd -> {
            LocalDateTime orderDateTime = ttd.getSystemTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            return localDate.equals(orderDateTime.toLocalDate()) && Duration.between(orderDateTime, localDateTime).toMinutes() <= min;
        }).count();

        return Long.valueOf(count).intValue();
    }

    /**
     * 驗證002營運規則
     * 同一車輛，同一卡號，當日刷卡次數
     *
     * @param ttdList       同月同車同卡
     * @param localDateTime 比較基準時間
     * @return int 不合規則總筆數
     */
    private int verificationRule002(final List<TaxiTransactionDetail> ttdList, final LocalDateTime localDateTime) {

        final LocalDate localDate = localDateTime.toLocalDate();
        long count = ttdList.stream().filter(ttd -> {
            LocalDateTime orderDateTime = ttd.getSystemTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            return localDate.equals(orderDateTime.toLocalDate());
        }).count();

        return Long.valueOf(count).intValue();
    }

    /**
     * 驗證003營運規則
     * 同一車輛，同一卡號，撥款週期內總刷卡次數
     *
     * @param ttdList       同月同車同卡
     * @param localDateTime 比較基準時間
     * @param abr           營運規則
     * @return int  不合規則總筆數
     */
    private int verificationRule003(List<TaxiTransactionDetail> ttdList, final LocalDateTime localDateTime, final IntTbAbnormalRule abr) {
        final LocalDate localDate = localDateTime.toLocalDate();
        String[] cycleTimes = abr.getParameter1().split(",");
        Arrays.sort(cycleTimes, Comparator.comparing(Integer::valueOf));
        for (String cycleTime : cycleTimes) {
            LocalDate specifyDate = LocalDate.of(localDate.getYear(), localDate.getMonth(), Integer.valueOf(cycleTime));
            if (localDate.isAfter(specifyDate)) {
                ttdList = ttdList.stream().filter(ttd -> {
                    LocalDateTime orderDateTime = ttd.getSystemTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
                    return orderDateTime.toLocalDate().isAfter(specifyDate) && orderDateTime.isBefore(localDateTime);
                }).collect(Collectors.toList());
            }
        }
        return ttdList.size();
    }

    /**
     * 替換 交易類型的請求 serviceType  ，由DB設定的device.Type為主
     *
     * @param header
     * @param device
     */
    private void changeServiceByPayment(RequestHeader header, Device device) {

        LOGGER.info("[changeServiceByPayment][ device.type = " + device.getType() + " ]");
        //交易時，以DB設定device.type為主 更改serviceType
        TicketPayment ticketPayment = TicketPayment.getTicketPayment(header.getMethod(), device.getType());

        LOGGER.info("[changeServiceByPayment][ ServiceType = " + header.getServiceType() + " to " + ticketPayment.getServiceType() + " ]");
        header.setServiceType(ticketPayment.getServiceType());

    }


    /**
     * 循卡一次,依據卡別替換成正確method
     *
     * @param header
     * @param requestData
     * @param merchant
     * @throws Exception
     */
    private void checkMethod(RequestHeader header, JsonObject requestData, Merchant merchant) throws Exception {
        Map<String, Object> verificationResultMap = this.verification(header, requestData, merchant);
        Device device = (Device) verificationResultMap.get("device");
        PaymentAccount paymentAccount = ((List<PaymentAccount>) verificationResultMap.get("paymentAccountList")).get(0);
        LOGGER.info("[TICKET][checkMethod][" + paymentAccount.getAccount() + "][" + device.getOwnerDeviceId() + "]");

        TicketFindCardRequest request = new TicketFindCardRequest(TEST_MODE, device, paymentAccount);
        String requestJsonStr = new ObjectMapper().writeValueAsString(request);

        LOGGER.info("[TICKET][checkMethod][findCard]  request => " + requestJsonStr);
        String result = this.sendPost(TICKET_URL, requestJsonStr);
        LOGGER.info("[TICKET][checkMethod][findCard]  response => " + result);

        TicketFindCardResponse response = new Gson().fromJson(result, TicketFindCardResponse.class);

        if (StringUtils.isNotEmpty(response.getNewAESKey())) {
            device.setAesKey(response.getNewAESKey());
            deviceService.save(device);
        }

        if (!"Success".equals(response.getTxnResult())) {
            throw new OtherAPIException(response.getChineseErrorMessage());
        }

        String cardId = StringUtils.defaultIfEmpty(response.getCardID(), "");

        String method = header.getMethod();
        switch (cardId) {
            case "00":  //悠遊卡(ezcard)
                method = "31800";
                break;
            case "01":  //一卡通(ipass)
                method = "32100";
                break;
            case "02": // HappyCash
                method = "33700";
                break;
            case "03": //ICASH
                method = "32800";
                break;
        }

        LOGGER.info("[TICKET][changeMethod][cardId = " + cardId + "][ " + header.getMethod() + " to " + method + " ]");
        header.setMethod(method);
    }

    /**
     * 小黃APP  登入改由這邊做最後判定要登入那些
     * 非小黃APP傳入的，以悠遊卡的回應為主替換信息 (因 店家POS機跟APP不更新)
     *
     * @param header
     * @param data
     * @return
     */
    private String ticketLogin(RequestHeader header, JsonObject data, Merchant merchant, Device device) {

        List<PaymentAccount> paymentAccountList = paymentAccountService.listAllByAccount(merchant.getAccountId()).stream()
                .filter(paymentAccount -> Objects.nonNull(paymentAccount.getIsDefault()) && paymentAccount.getIsDefault())
                .filter(pa -> ticketBankList.contains(pa.getBank().getBankId())).collect(Collectors.toList());
        Comparator<PaymentAccount> comparator = Comparator.comparing(e -> e.getBank().getBankId());
        paymentAccountList.sort(comparator.reversed());
        String result = "";
        String msg = "";
        String code = "9999";
        String reqServiceType = header.getServiceType();
        StringBuilder bankListStr = new StringBuilder();

        for (PaymentAccount pa : paymentAccountList) {
            IntTbLookupCode lookupCode = lookupCodeService.findOne("ticketType", Long.toString(pa.getBank().getBankId()));
//            LOGGER.info("[taxiLogin] lookupCode = " + new Gson().toJson(lookupCode));
            if (Objects.nonNull(lookupCode) && Objects.nonNull(lookupCode.getLookupType())) {
                bankListStr.append(StringUtils.isEmpty(bankListStr.toString()) ? "" : "|").append(pa.getBank().getBankId());
                TicketLogin ticketLogin = TicketLogin.getTicketLogin(String.valueOf(lookupCode.getLookupCode()), device.getType());
                msg += (StringUtils.isEmpty(msg) ? "" : "\n") + lookupCode.getDscr();
                header.setMethod(ticketLogin.getMethodId());
                header.setServiceType(ticketLogin.getSerivceType());
                try {
                    result = this.changeServiceImpl(header, data, merchant);
                    JsonObject jsonObject = new Gson().fromJson(new Gson().fromJson(result, JsonObject.class).get("Data"), JsonObject.class);
                    LOGGER.info("[ticketLogin]  result = " + result);
                    if ("Success".equals(jsonObject.get("TXNResult").getAsString())) {
                        msg += messageSource.getMessage("taxi_login_success", null, locale);
                        code = "0000".equals(code) || "9999".equals(code) ? "0000" : code;
                    } else {
                        code = "0001";
                        msg += messageSource.getMessage("taxi_login_fail", null, locale);
                    }
                } catch (Exception e) {
                    code = "0001";
                    msg += messageSource.getMessage("taxi_login_fail", null, locale);
                }
            }
        }
        header.setServiceType(reqServiceType);
        JsonObject resultJson = new Gson().fromJson(result, JsonObject.class);
        LOGGER.info("[ticketLogin][change result json ]  result = " + result);
        LOGGER.info("[ticketLogin][change result json ]  resultJson = " + resultJson);
        JsonObject headerJsosnObject = Objects.isNull(resultJson.get("Header")) || resultJson.get("Header").isJsonNull() ? new JsonObject() : new Gson().fromJson(resultJson.get("Header"), JsonObject.class);
        headerJsosnObject.addProperty("ServiceType", reqServiceType);
        JsonObject dataJsonObject = Objects.isNull(resultJson.get("Data")) || resultJson.get("Data").isJsonNull() ? new JsonObject() : new Gson().fromJson(resultJson.get("Data"), JsonObject.class);
        dataJsonObject.addProperty("ErrorCode", reqServiceType.contains("SignOn") ? "00" + code : code);
        dataJsonObject.addProperty("Message", "9999".equals(code) ? messageSource.getMessage("taxi_login_error", null, locale) : msg);
        dataJsonObject.addProperty("TXNResult", "0000".equals(code) ? "Success" : "Fail");
        dataJsonObject.addProperty("BankListStr", bankListStr.toString());
        resultJson.add("Header", new Gson().fromJson(headerJsosnObject.toString(), JsonElement.class));
        resultJson.add("Data", new Gson().fromJson(dataJsonObject.toString(), JsonElement.class));
        return new Gson().toJson(resultJson);
    }

    /**
     * 將請求丟置各至的service 做事
     *
     * @param header
     * @param requestData
     * @param merchant
     * @return
     * @throws JsonProcessingException
     * @throws DeviceNotFoundException
     * @throws InvalidDataFormatException
     * @throws OrderNotFoundException
     * @throws OrderAlreadyRefundedException
     * @throws ServiceTypeNotSupportException
     * @throws RefundKeyException
     */
    private String changeServiceImpl(RequestHeader header, JsonObject requestData, Merchant merchant) throws Exception {
        String result;
        switch (header.getMethod()) {
            case "31800":       //悠遊卡
                result = ezCardService.ezRequest(header, requestData, merchant);
                break;
            case "32100":       //一卡通
                result = iPassService.ezRequest(header, requestData, merchant);
                break;
            case "32800":       //Icash
                result = iCashService.ezRequest(header, requestData, merchant);
                break;
            case "33700":       //HappyCash
                result = "";
                break;
            default:
                result = "TicketLogic no transfer control";
        }
        return result;
    }

    @Value("${iCash.TestMode}")
    private String TEST_MODE;
    @Autowired
    private Environment env;

    @Override
    public Map<String, Object> verification(RequestHeader requestHeader, JsonObject requestData, Merchant merchant) throws DeviceNotFoundException, InvalidDataFormatException {

        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        List<PaymentAccount> paymentAccountList = paymentAccountService.listAllByAccount(merchant.getAccountId()).stream()
                .filter(pa -> pa.getIsDefault() && ticketBankList.contains(pa.getBank().getBankId()))
                .sorted(Comparator.comparing(pa -> pa.getBank().getBankId())).collect(Collectors.toList());

        String deviceId = requestData.get("DeviceId").getAsString();
        Device device = deviceService.getOne(merchant, deviceId);

        if (Objects.isNull(device) && !deviceId.equals("00000000")) {
            LOGGER.error("[verification][Ticket] Devicer  not found. " + deviceId + ", " + merchant.getAccountId());
            throw new DeviceNotFoundException("Devicer  not found");
        } else if (!deviceId.equals("00000000") && "1".equals(device.getBan())) {
            LOGGER.error("[verification][Ticket] device lock. " + deviceId + ", " + merchant.getAccountId());
            throw new DeviceNotFoundException("Device Locked");
        } else if (CollectionUtils.isEmpty(paymentAccountList)) {
            LOGGER.error("[verification][Ticket] payment Account not found. " + integrateMchId + ", " + bankId);
            throw new InvalidDataFormatException("payment account not found");
        }
        LOGGER.info("[verification][Ticket] " + merchant.getAccountId() + ", " + device.getOwner());
        Map<String, Object> map = new HashMap<>();
        map.put("device", device);
        map.put("paymentAccountList", paymentAccountList);
        return map;
    }

    /**
     * 發送請求
     *
     * @param url
     * @param jsonStr
     * @return
     */
    private String sendPost(String url, String jsonStr) throws InvokeAPIException {
        String result = "";
        try {
            result = HttpUtil.doPostJson(url, jsonStr);
        } catch (Exception e) {
            LOGGER.error("[ICASH][EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            throw new InvokeAPIException(this.getClass().getSimpleName(), e.getMessage());
        }
        if (StringUtils.isEmpty(result)) {
            throw new InvokeAPIException(this.getClass().getSimpleName(), messageSource.getMessage("response.7350", null, locale));
        }
        return result;
    }


    @Override
    public Boolean autoLogin() {
        //目前只有 歐特儀，但先弄個list再說，date那邊查詢包含了所有type IN ( 'ezc' ,'taxi' , 'traffic') 、bankId in ('18','21','28')
        final List<IntTbLookupCode> lookupCodeList = lookupCodeService.findAllByLookType("autoSignOnMail");
        lookupCodeList.forEach(lookupCode -> {
            LOGGER.info("[autoLogin]  account =  " + lookupCode.getLookupCode());
            try {
                List<Map<String, Object>> autoLoginDeviceList = this.getAutoLoginDeviceList(lookupCode.getLookupCode());
                String loginResult = autoLoginDeviceList.parallelStream().map(stringObjectMap -> {
                    Merchant merchant = merchantService.getOne(stringObjectMap.get("accountId").toString());
                    List<PaymentAccount> paymentAccountList = paymentAccountService.listAllByAccount(stringObjectMap.get("accountId").toString())
                            .stream().filter(paymentAccount -> ticketBankList.contains(paymentAccount.getBank().getBankId()))
                            .filter(paymentAccount -> Objects.nonNull(paymentAccount.getIsDefault()) && paymentAccount.getIsDefault()).collect(Collectors.toList());
                    return this.doAutoLogin(stringObjectMap, merchant, paymentAccountList);
                }).collect(Collectors.joining("^"));

                this.seanAutoSignOnEmail(loginResult, autoLoginDeviceList, lookupCode);
            } catch (Exception e) {
                // 因有做些處理，所以只印LOG
                LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            }
        });
        return true;
    }

    private List<Map<String, Object>> getAutoLoginDeviceList(String account) throws Exception {
        String result = HttpRequestUtil.httpGet(HOST_DAO + "api/crm/ticket/getAutoLoginList/" + account);
        List<Map<String, Object>> resultList = new Gson().fromJson(result,
                new TypeToken<List<Map<String, Object>>>() {
                }.getType());
        LOGGER.info("[getAutoLoginDeviceList]  AutoLoginDeviceList size  =  " + resultList.size());
        LOGGER.info("[getAutoLoginDeviceList]  AutoLoginDeviceList =  " +
                resultList.parallelStream().filter(stringObjectMap -> Objects.nonNull(stringObjectMap.get("ownerDeviceId")))
                        .map(stringObjectMap -> stringObjectMap.get("ownerDeviceId").toString()).collect(Collectors.joining(",", "[", "]")));
        return resultList;
    }


    private String doAutoLogin(@Nonnull final Map<String, Object> stringObjectMap, final Merchant merchant, final List<PaymentAccount> paymentAccountList) {
        return paymentAccountList.stream().map(paymentAccount -> {
            String decryptResponse = "";
            RequestHeader header = new RequestHeader();
            header.setMerchantId(stringObjectMap.get("accountId").toString());
            header.setTradeKey(stringObjectMap.get("tradePin").toString());
            header.setCreateTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
            JsonObject data = new JsonObject();
            data.addProperty("DeviceId", stringObjectMap.get("ownerDeviceId").toString());
            try {
                int retry = 0;
                boolean again;
                TicketLogin ticketLogin = TicketLogin.getTicketLogin(String.valueOf(paymentAccount.getBank().getBankId()), stringObjectMap.get("type").toString());
                header.setMethod(ticketLogin.getMethodId());
                header.setServiceType(ticketLogin.getSerivceType());
                do {
                    data.addProperty("Retry", ++retry);
                    decryptResponse = this.changeServiceImpl(header, data, merchant);
                    again = this.checkAutoLogin(decryptResponse);

                    JsonObject jsonObject = new Gson().fromJson(decryptResponse, JsonObject.class);
                    JsonObject dataJson = new Gson().fromJson(jsonObject.get("Data").toString(), JsonObject.class);
                    dataJson.addProperty("Retry", retry);
                    dataJson.addProperty("DeviceID", stringObjectMap.get("ownerDeviceId").toString());
                    jsonObject.add("Data", new Gson().fromJson(dataJson.toString(), JsonElement.class));
                    decryptResponse = new Gson().toJson(jsonObject);

                    Thread.sleep(4 * 1000);
                } while (again && retry < 3);
            } catch (Exception e) {
                LOGGER.error("[doAutoLogin] [Exception]" + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            }
            return decryptResponse;
        }).collect(Collectors.joining("^"));

    }

    private boolean checkAutoLogin(@Nonnull String loginResultStr) {
        //{"Header":{"StatusCode":"0000","StatusDesc":"STATUS_SUCCESS","Method":"31800","ServiceType":"SignOn","MchId":"AltobA02801A01","ResponseTime":"20190902034451"},
        // "Data":{"TXNResult":"Fail","Retry":"0","ErrorCode":"010124","NewAESKey":""}}
        //{"Header":{"StatusCode":"0000","StatusDesc":"STATUS_SUCCESS","Method":"31800","ServiceType":"SignOn","MchId":"AltobA06100A03","ResponseTime":"20190902042406"},
        // "Data":{"DeviceID":"0221130898","NewDeviceID":"0852000001304310","TXNResult":"Success","Retry":"0","ErrorCode":"000000","DongleDeviceID":"D6100D400182","NewAESKey":"ee4b339329489ebf4a771cb2baec8e64"}}
        boolean again = true;
        JsonObject jsonObject = new Gson().fromJson(loginResultStr, JsonObject.class);
        JsonObject dataJson = new Gson().fromJson(jsonObject.get("Data").toString(), JsonObject.class);
        String txnResul = dataJson.get("TXNResult").getAsString();

        if (SystemInstance.STATUS_DESCRIPTION_SUCCESS.equalsIgnoreCase(txnResul)) {
            again = false;
        } else if (SystemInstance.STATUS_DESCRIPTION_FAIL.equalsIgnoreCase(txnResul) && dataJson.get("ErrorCode").getAsString().startsWith("90000")) {
            again = false;
        }
        return again;
    }

    private void seanAutoSignOnEmail(String result, List<Map<String, Object>> autoLoginDeviceList, IntTbLookupCode lookupCode) {
        //轉換成List
        List<JsonObject> signOnResultList = Arrays.asList(result.split("\\^")).parallelStream().map(loginResultStr -> new Gson().fromJson(loginResultStr, JsonObject.class)).collect(Collectors.toList());

        //濾出錯誤的信息以及加入name
        List<Map<String, Object>> errorSignOnResultList = signOnResultList.parallelStream().filter(jsonObject -> {
            JsonObject dataJson = new Gson().fromJson(jsonObject.get("Data").toString(), JsonObject.class);
            String txnResul = dataJson.get("TXNResult").getAsString();
            Integer retry = Integer.valueOf(dataJson.get("Retry").getAsString());
            return SystemInstance.STATUS_DESCRIPTION_FAIL.equalsIgnoreCase(txnResul) || dataJson.get("ErrorCode").getAsString().startsWith("90000") || retry >= 3;
        }).map(jsonObject -> {
            JsonObject headerJson = new Gson().fromJson(jsonObject.get("Header").toString(), JsonObject.class);
            final String accountId = headerJson.get("MchId").getAsString();
            String method = headerJson.get("Method").getAsString();
            JsonObject dataJson = new Gson().fromJson(jsonObject.get("Data").toString(), JsonObject.class);
            Integer retry = Integer.valueOf(dataJson.get("Retry").getAsString());

            final String deviceID = dataJson.get("DeviceID").getAsString().length() > 8 ?
                    dataJson.get("NewDeviceID").getAsString().substring(dataJson.get("NewDeviceID").getAsString().length() - 8) : dataJson.get("DeviceID").getAsString();
            Map<String, Object> soMap = autoLoginDeviceList.parallelStream()
                    .filter(map -> map.get("accountId").toString().equals(accountId) && map.get("ownerDeviceId").toString().equals(deviceID))
                    .collect(Collectors.toList()).get(0);
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.putAll(soMap);
            resultMap.put("signOnResult", jsonObject.toString());
            resultMap.put("retry", retry);
            resultMap.put("ticketType", "31800".equals(method) ? "EZC" : "32100".equals(method) ? "IPASS" : "ICASH");
            resultMap.put("responseTime", headerJson.get("ResponseTime").getAsString());
            return resultMap;
        }).collect(Collectors.toList());

        MailInformation mailInformation = new MailInformation();
        mailInformation.setMailTo(lookupCode.getType1().split(";"));
        mailInformation.setSubject("Auto SignOn Failed");
        Context ctx = new Context();
        ctx.setVariable("errorSignOnResultList", errorSignOnResultList);
        try {
            mailService.send(mailInformation, ctx, "autoSignOnMail");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    /**
     * 舊的共同入口  swVersion !=  1.0
     *
     * @param header
     * @param requestData
     * @return
     * @throws Exception
     */
    private String doRequest_Old(RequestHeader header, JsonObject requestData, Merchant merchant, Device device) throws Exception {
        //TODO 因不修改之前POS的情況下，未能確認POS打的那些serviceType，故將交易相關(''''''都需要卡片''''''，不分新舊)的serviceType就加在這邊，做判斷用
        final List<String> ezcTransactionList = Arrays.asList("OrderQuery", "Payment", "Refund", "EZCRefund", "Reserve", "PaymentForTaxi", "BalanceQuery", "EZCardDataInQueryForTaxi", "EZCardEDCAPayment", "EZCardEDCADataInQuery", "Cancel", SystemInstance.ServiceType.TICKET_LOGIC);
        final List<String> iPassTransactionList = Arrays.asList("QueryCardInfo", "TradeSale", SystemInstance.ServiceType.TICKET_LOGIC);
        final List<String> iPassNONTransactionList = Arrays.asList("Blacklist", "Upload", "Checkout");  //這三個回應 前人沒有分Header Data
        final List<String> iCashTransactionList = Arrays.asList("OrderQuery", "Payment", "Refund", "QueryCardInfo", "Reserve", "TradeChargeCancel");

        //TODO 不分票證，是做交易(扣款)的serviceType這邊也要加入
        final List<String> transactionList = Arrays.asList("Payment", "TradeSale", "PaymentForTaxi", "EZCardEDCAPayment", SystemInstance.ServiceType.TICKET_LOGIC);
        String serviceType = header.getServiceType();
        //目前只針對要做交易先介入更改method
        if (ezcTransactionList.contains(serviceType) || iPassTransactionList.contains(serviceType) || iCashTransactionList.contains(serviceType)) {
            List<PaymentAccount> paymentAccountList = paymentAccountService.listAllByAccount(merchant.getAccountId()).parallelStream()
                    .filter(pa -> Boolean.valueOf(pa.getIsDefault()) && ticketBankList.contains(pa.getBank().getBankId())).collect(Collectors.toList());
            long posCount = paymentAccountList.size();
            //多卡卡機才有新尋卡
            if (posCount > 1) {
                this.checkMethod(header, requestData, merchant);
            } else if (CollectionUtils.isNotEmpty(paymentAccountList)) {
                header.setMethod("3" + paymentAccountList.get(0).getBank().getBankId() + "00");
            }
            if (transactionList.contains(serviceType)) {
                this.changeServiceByPayment(header, device);
            }
        }

        String result;
        // 新APP 登入由這控制要登入那些 或 一般特店登入
        if (signOnList.contains(serviceType)) {
            result = this.ticketLogin(header, requestData, merchant, device);
        } else {
            result = this.changeServiceImpl(header, requestData, merchant);
        }

        if (!SystemInstance.ServiceType.TICKET_LOGIC.equals(serviceType) && !iPassNONTransactionList.contains(serviceType)) {
            //因小黃版本TICKET_LOGIC 會進入運算金額迴圈，固排除
            //其他因APP前人都是用serviceType 區分回應轉換，固其他要替換
            JsonObject jsonObject = new JsonParser().parse(result).getAsJsonObject();
            JsonObject headerJson = jsonObject.getAsJsonObject("Header");
            headerJson.addProperty("ServiceType", serviceType);
            jsonObject.add("Header", headerJson);
            result = new Gson().toJson(jsonObject);

        }

        return result;
    }

    /**
     * 新的共同入口邏輯，不再重新setMethod，只看是不是交易，是交易則  尋卡+交易API，其餘都過水
     *
     * @param header
     * @param requestData
     * @param merchant
     * @param device
     * @return
     * @throws Exception
     */
    private String doRequest_New(RequestHeader header, JsonObject requestData, Merchant merchant, Device device) throws Exception {
        //目前只有特店APP請求的service，兩個list都是只是區分要不要走共同入口
        final List<String> transactionList = Arrays.asList("Payment", "Refund", "BalanceQuery", "EZCardEDCAPayment");
        final List<String> appList = Arrays.asList("QueryCardInfo", "IdQuery", "Reserve");
        final List<String> findCardList = Arrays.asList("IdQuery", "OrderQuery", "TradeChargeCancel");
        String result;
        long retry = requestData.has("Retry") ? Long.valueOf(requestData.get("Retry").getAsString()) : 0;

        String serviceType = header.getServiceType();
        if (retry == 0 && transactionList.contains(serviceType)) {
            result = this.doFastTrans(header, requestData, merchant, device);
        } else if (signOnList.contains(serviceType)) {
            result = this.ticketLogin(header, requestData, merchant, device);
        } else {
            if (appList.contains(serviceType)) {
                this.checkMethod(header, requestData, merchant);
            }
            if (retry == 0 && findCardList.contains(serviceType)) {
                // 因建立訂單時，用的是卡片內碼，一般查卡API並無內碼，會導致退款或取消無法取得對應的卡號查詢訂單
                // 這邊走尋卡+交易 API 拿到內碼再往下做
                this.doFastTrans(header, requestData, merchant, device);
                if ("TradeChargeCancel".equals(header.getServiceType())) {
                    List<ReserveDetail> listTopOneByUserId = reserveDetailService.listTopOneByUserId(merchant.getAccountId(), device.getOwnerDeviceId(), requestData.get("UserId").getAsString());
                    ReserveDetail reserveDetail = CollectionUtils.isNotEmpty(listTopOneByUserId) ? listTopOneByUserId.get(0) : null;
                    if (Objects.nonNull(reserveDetail)) {
                        requestData.addProperty(SystemInstance.FIELD_NAME_STORE_ORDER_NO, reserveDetail.getReserveOrderId());
                    }
                }

            } else if (retry > 0) {
                String orderId = requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString();
                TradeDetail tradeDetail = tradeDetailService.getOne(orderId);
                ReserveDetail reserveDetail = reserveDetailService.getOne(header.getMerchantId(), orderId);
                if ("Payment".equals(serviceType) && Objects.nonNull(tradeDetail)) {
                    header.setMethod(tradeDetail.getMethod());
                    this.changeServiceByPayment(header, device);
                } else if ("Refund".equals(serviceType)) {
                    header.setMethod(tradeDetail.getMethod());
                } else if ("Reserve".equals(serviceType) && Objects.nonNull(reserveDetail)) {
                    header.setMethod(reserveDetail.getMethod());
                } else if (Objects.nonNull(reserveDetail)) {
                    //TradeChargeCancel
                    header.setMethod(reserveDetail.getMethod());
                    if (!requestData.has("UserId")) {
                        requestData.addProperty("UserId", reserveDetail.getUserId());
                    }
                }
            }
            if ("TradeChargeCancel".equals(serviceType)) {
                requestData.addProperty("ActionType", "CANCEL_RESERVE");
                requestData.addProperty("RefundKey", merchant.getRefundPin());
            }
            result = this.changeServiceImpl(header, requestData, merchant);
        }

        return result;
    }

    private TradeDetail getTradeDetail(Device device, PaymentAccount paymentAccount, RequestHeader requestHeader, JsonObject requestData) throws OtherAPIException, OrderAlreadyExistedException, AlreadyRetryException, OrderAlreadyRefundedException {
        TradeDetail tradeDetail = new TradeDetail();
        tradeDetail.setAccountId(requestHeader.getMerchantId());
        if (requestHeader.getServiceType().contains("Payment")) {
            LocalDateTime now = LocalDateTime.now();
            JsonElement orderIdJE = requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO);
            if (orderIdJE.isJsonNull() || StringUtils.isBlank(orderIdJE.getAsString())) {
                throw new OtherAPIException(messageSource.getMessage("response.7142", null, locale));
            }

            tradeDetail = tradeDetailService.getOne(orderIdJE.getAsString());
            if (Objects.nonNull(tradeDetail)) {
                throw new OrderAlreadyExistedException("Trade exist");
            } else {
                tradeDetail = new TradeDetail();
                tradeDetail.setStatus(SystemInstance.TRADE_CREATING_ORDER);
                tradeDetail.setOrderId(orderIdJE.getAsString());
                tradeDetail.setCreateDate(now.format(DateTimeFormatter.ofPattern(SystemInstance.DATE_PATTERN)));
                tradeDetail.setAccountId(requestHeader.getMerchantId());
                tradeDetail.setPayment(Long.valueOf(requestData.get(SystemInstance.FIELD_NAME_AMOUNT).getAsString()));
                tradeDetail.setDeviceRandomId(device);
                tradeDetail.setPaymentAccount(paymentAccount);
                tradeDetail.setMethod("Ticket");
                tradeDetail.setSystemOrderId(now.format(DateTimeFormatter.ofPattern("HHmmss")));
                tradeDetail.setEzcTxnDate(now.format(DateTimeFormatter.ofPattern(SystemInstance.DATE_PATTERN)));
                tradeDetail.setComment(paymentAccount.getAccount() + device.getOwnerDeviceId());
                if (requestData.has("Body")) {
                    tradeDetail.setDescription(requestData.get("Body").getAsString());
                }
                tradeDetail = tradeDetailService.save(tradeDetail);
                if (Objects.isNull(tradeDetail)) {
                    throw new OtherAPIException(messageSource.getMessage("response.7142", null, locale));
                }
            }
        } else if ("Refund".equals(requestHeader.getServiceType())) {
            JsonElement orderIdJE = requestData.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO);
            if (orderIdJE.isJsonNull() || StringUtils.isBlank(orderIdJE.getAsString())) {
                throw new OtherAPIException(messageSource.getMessage("response.7127", null, locale));
            }
            tradeDetail = tradeDetailService.getOne(orderIdJE.getAsString());
            if (Objects.isNull(tradeDetail)) {
                throw new OtherAPIException(messageSource.getMessage("response.7002", null, locale));
            }
            if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                throw new OtherAPIException(messageSource.getMessage("response.7351", null, locale));
            }
            if (SystemInstance.TRADE_REFUND_SUCCESS.equals(tradeDetail.getRefundStatus())) {
                throw new OrderAlreadyRefundedException("tradeDetail already refund.");
            }
        }

        return tradeDetail;
    }

    private TicketFastTransRequest getTicketFastTransRequest(RequestHeader header, JsonObject requestData, Device device, PaymentAccount paymentAccount, Merchant merchant, TradeDetail tradeDetail) throws Exception {
        List<PaymentAccount> paymentAccountList = paymentAccountService.listAllByAccount(merchant.getAccountId()).stream()
                .filter(pa -> Boolean.valueOf(pa.getIsDefault()) && ticketBankList.contains(pa.getBank().getBankId())).collect(Collectors.toList());

        TicketFastTransRequest request = new TicketFastTransRequest(TEST_MODE, device, paymentAccount, paymentAccountList);
        TicketPayment ticketPayment = TicketPayment.getTicketPayment(header.getMethod(), device.getType());
        request.setServiceTxType(ticketPayment.getServiceTxType());
        String amount = String.valueOf(tradeDetail.getPayment());
//        long retry = requestData.has("Retry") ? Long.valueOf(requestData.get("Retry").getAsString()) : 0;

        request.setIcashData("[" + this.getIcashData(device, amount, merchant, header.getServiceType(), requestData) + "]");
        request.setEzcData("[" + this.getEzcData(device, amount, merchant, header.getServiceType(), 0, tradeDetail, requestData) + "]");
        request.setIpassData("[" + this.getIpassData(device, amount, merchant, header.getServiceType(), requestData, tradeDetail) + "]");

        return request;
    }

    private String getIcashData(Device device, String amount, Merchant merchant, String serviceType, JsonObject requestData) throws Exception {
        PaymentAccount paymentAccount = paymentAccountService.getOne(merchant.getAccountId(), 28);
        String result = "{}";
        if (Objects.nonNull(paymentAccount)) {
            switch (serviceType) {
                case "Payment":
                case "EZCardEDCAPayment":
                    IcashTradeSaleRequest icashTradeSaleRequest = new IcashTradeSaleRequest(TEST_MODE, device, paymentAccount);
                    icashTradeSaleRequest.setAmount(amount);
                    if (requestData.has("Cashier") && StringUtils.isNotEmpty(requestData.get("Cashier").getAsString())) {
                        icashTradeSaleRequest.setCashierCode(requestData.get("Cashier").getAsString());
                    }
                    result = new ObjectMapper().writeValueAsString(icashTradeSaleRequest);
                    break;
                case "Refund":
                    IcashTradeRefundRequest icashTradeRefundRequest = new IcashTradeRefundRequest(TEST_MODE, device, paymentAccount);
                    icashTradeRefundRequest.setAmount(amount);
                    result = new ObjectMapper().writeValueAsString(icashTradeRefundRequest);
                    break;
                case "TradeChargeCancel":
                case "BalanceQuery":
                case "OrderQuery":
                    IcashQueryCardInfoRequest icashQueryCardInfoRequest = new IcashQueryCardInfoRequest(TEST_MODE, device, paymentAccount);
                    result = new ObjectMapper().writeValueAsString(icashQueryCardInfoRequest);
                    break;
            }
        }
        return result;
    }

    private String getEzcData(Device device, String amount, Merchant merchant, String serviceType, long retry, TradeDetail tradeDetail, JsonObject requestData) throws Exception {
        PaymentAccount paymentAccount = paymentAccountService.getOne(merchant.getAccountId(), 18);
        String result = "{}";
        if (Objects.nonNull(paymentAccount)) {

            LocalDateTime now = LocalDateTime.now();
            String date;
            String time;
            String batchNo = device.getBatchNumber().toString();
            if (!Strings.isNullOrEmpty(batchNo) && retry == 0) {
                date = now.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
                time = now.format(DateTimeFormatter.ofPattern("HHmmss"));
                batchNo = date.substring(2) + String.format("%02d", device.getBatchNumber());
            } else {
                date = StringUtils.defaultIfEmpty(tradeDetail.getEzcTxnDate(), now.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"))).substring(0, 8);
                time = StringUtils.defaultIfEmpty(tradeDetail.getEzcTxnDate(), now.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"))).substring(8);
                batchNo = tradeDetail.getBatchNo();
            }

            switch (serviceType) {
                case "Payment":
                    EzCardPayment ezCardPayment = ezCardService.getEzCardPayment(device, paymentAccount, amount, time, date, batchNo, String.valueOf(retry), time);
                    result = new Gson().toJson(ezCardPayment);
                    break;
                case "Refund":
                    EzCardRefund ezCardRefund = ezCardService.getEzCardRefund(device, paymentAccount, time, date, batchNo, String.valueOf(retry), time);
                    ezCardRefund.setAmount(amount);
                    result = new Gson().toJson(ezCardRefund);
                    break;
                case "BalanceQuery":
                    EzCardInfoQuery ezCardInfoQuery = ezCardService.getEzCardInfoQuery(device, paymentAccount, date, time);
                    result = new Gson().toJson(ezCardInfoQuery);
                    break;
                case "EZCardEDCAPayment":
                    EzCardEDCAPaymentReq ezCardEDCAPaymentReq = ezCardService.getEzCardEDCAPaymentReq(device, paymentAccount, amount, time, date, batchNo, String.valueOf(retry));
                    result = new Gson().toJson(ezCardEDCAPaymentReq);
                    break;
                case "TradeChargeCancel":
                case "OrderQuery":
                    EzCardIdQuery ezCardIdQuery = new EzCardIdQuery(device, paymentAccount);
                    result = new ObjectMapper().writeValueAsString(ezCardIdQuery);
                    break;
            }
        }
        return result;
    }

    private TradeDetail updateUserIdAndMethod(Merchant merchant, TradeDetail tradeDetail, JsonObject fastTransData, Device device) {
        String userId = fastTransData.get("TicketCardNumber").getAsString();
        String method;
        switch (fastTransData.get("TicketCardID").getAsString()) {
            case "01":  //一卡通(ipass)
                method = "32100";
                break;
            case "02": // HappyCash
                method = "33700";
                break;
            case "03": //ICASH
                method = "32800";
                break;
            case "00":  //悠遊卡(ezcard)
            default:
                method = "31800";
                break;
        }

//        if (lookupCodeService.isTsmcAccount(tradeDetail.getAccountId()) && StringUtils.isNotEmpty(tradeDetail.getOrderId())) {
//            // 這邊為了 台積電判斷，若會員卡機先回調
//            TradeDetail _tradeDetail1 = tradeDetailService.getOne(tradeDetail.getOrderId());
//            tradeDetail.setMethod(_tradeDetail1.getMethod());
//
//            tradeDetail.setStatus(_tradeDetail1.getStatus());
//            tradeDetail.setUserId(_tradeDetail1.getUserId());
//            if (!SystemInstance.TRADE_SUCCESS.equals(_tradeDetail1.getStatus())) {
//                // 台積電，若多卡卡機比會員卡機回復早      ||           未交易成功(比會員卡機回復慢)  則蓋method userId
//                if (!"14900".equals(_tradeDetail1.getMethod())) {
//                    tradeDetail.setMethod(method);
//                }
//                tradeDetail.setUserId(userId);
//            }
//        } else {
        tradeDetail.setMethod(method);
        tradeDetail.setUserId(userId);
//        }
//        long bankId = Long.valueOf(env.getProperty("bank.id." + tradeDetail.getMethod()));
//        PaymentAccount paymentAccount = paymentAccountService.getOne(merchant.getAccountId(), bankId);
//        tradeDetail.setPaymentAccount(paymentAccount);
//        if (StringUtils.isNotBlank(tradeDetail.getOrderId())) {
//            tradeDetail = tradeDetailService.save(tradeDetail);
//        }
        return tradeDetail;
    }

    private String updateTradeDetail(RequestHeader header, JsonObject requestData, Merchant merchant, Device device, TicketFastTransRequest request, JsonObject ticketData, TradeDetail tradeDetail) {
        String result = "";
        String method = tradeDetail.getMethod();
        if (lookupCodeService.isTsmcAccount(tradeDetail.getAccountId()) && StringUtils.isNotEmpty(tradeDetail.getOrderId())) {
            // 這邊為了 台積電判斷，若會員卡機先回調
            TradeDetail _tradeDetail1 = tradeDetailService.getOne(tradeDetail.getOrderId());
            if ("14900".equals(_tradeDetail1.getMethod())) {
                tradeDetail.setMethod(_tradeDetail1.getMethod());
                if (SystemInstance.TRADE_SUCCESS.equals(_tradeDetail1.getStatus())) {
                    tradeDetail.setStatus(_tradeDetail1.getStatus());
                    tradeDetail.setUserId(_tradeDetail1.getUserId());
                }
            }
        }
        long bankId = Long.valueOf(env.getProperty("bank.id." + tradeDetail.getMethod()));
        PaymentAccount paymentAccount = paymentAccountService.getOne(merchant.getAccountId(), bankId);
        tradeDetail.setPaymentAccount(paymentAccount);
        switch (method) {
            case "31800":
                switch (header.getServiceType()) {
                    case "EZCardEDCAPayment":
                        EzCardEDCAPaymentReq ezCardEDCAPaymentReq = new Gson().fromJson(request.getEzcData().substring(1, request.getEzcData().length() - 1), EzCardEDCAPaymentReq.class);
                        AcerTaxiPaymentResponse response = new Gson().fromJson(ticketData.toString(), AcerTaxiPaymentResponse.class);
                        result = ezCardService.updateEDCAPayment(header, requestData, device, merchant, ezCardEDCAPaymentReq, response, tradeDetail, paymentAccount);
                        break;
                    case "Refund":
                        EzCardRefund ezCardRefund = new Gson().fromJson(request.getEzcData().substring(1, request.getEzcData().length() - 1), EzCardRefund.class);
                        EzCardRefundResponse ezCardRefundResponse = new Gson().fromJson(ticketData.toString(), EzCardRefundResponse.class);
                        result = ezCardService.updateDoRefund(header, requestData, device, merchant, ezCardRefund, ezCardRefundResponse, tradeDetail, paymentAccount, ezCardRefund.getBatchNumber());
                        break;
                    case "BalanceQuery":
                        EzCardInfoQuery ezCardInfoQuery = new Gson().fromJson(request.getEzcData().substring(1, request.getEzcData().length() - 1), EzCardInfoQuery.class);
                        EzCardInfoQueryResponse ezCardInfoQueryResponse = new Gson().fromJson(ticketData.toString(), EzCardInfoQueryResponse.class);
                        result = ezCardService.upodateBalanceQuery(device, header, ezCardInfoQuery, ezCardInfoQueryResponse);
                        break;
                    case "Payment":
                        EzCardPayment ezCardPayment = new Gson().fromJson(request.getEzcData().substring(1, request.getEzcData().length() - 1), EzCardPayment.class);
                        EzCardPaymentResponse ezCardPaymentResponse = new Gson().fromJson(ticketData.toString(), EzCardPaymentResponse.class);
                        if (StringUtils.isEmpty(ezCardPaymentResponse.getOrderId())) {
                            ezCardPaymentResponse.setOrderId(tradeDetail.getOrderId());
                        }
                        result = ezCardService.updateDoPayment(header, requestData, device, merchant, ezCardPayment, ezCardPaymentResponse, tradeDetail, paymentAccount);
                        break;
                }
                break;
            case "32100":
                switch (header.getServiceType()) {
                    case "EZCardEDCAPayment":
                    case "Payment":
                        IPassTradeSaleRequest iPassTradeSaleRequest = new Gson().fromJson(request.getIpassData().substring(1, request.getIpassData().length() - 1), IPassTradeSaleRequest.class);
                        IPassBaseResponse paymentResponse = new Gson().fromJson(ticketData.toString(), IPassBaseResponse.class);
                        if (StringUtils.isEmpty(paymentResponse.getOrderId())) {
                            paymentResponse.setOrderId(tradeDetail.getOrderId());
                        }
                        result = iPassService.updateDoTradeSale(header, requestData, device, merchant, iPassTradeSaleRequest, paymentResponse, tradeDetail, paymentAccount);
                        break;
                    case "Refund":
                        IPassBaseResponse refundResponse = new Gson().fromJson(ticketData.toString(), IPassBaseResponse.class);
                        if (!"Success".equals(refundResponse.gettXNResult())) {
                            String errCode = StringUtils.defaultIfEmpty(refundResponse.getMcErrorCode(), "900000");
                            errCode = errCode.length() > 6 ? errCode.substring(errCode.length() - 4) : errCode;
                            refundResponse.setErrorCode(errCode + " - " + refundResponse.getChineseErrorMessage());
                        }
                        result = iPassService.updateDoRefund(header, requestData, device, merchant, refundResponse, tradeDetail, paymentAccount);
                        break;
                    case "BalanceQuery":
                        IPassBaseResponse balanceQueryResponse = new Gson().fromJson(ticketData.toString(), IPassBaseResponse.class);
                        if (!"Success".equals(balanceQueryResponse.gettXNResult())) {
                            String errCode = StringUtils.defaultIfEmpty(balanceQueryResponse.getMcErrorCode(), "900000");
                            errCode = errCode.length() > 6 ? errCode.substring(errCode.length() - 4) : errCode;
                            balanceQueryResponse.setErrorCode(errCode + " - " + balanceQueryResponse.getChineseErrorMessage());
                        }
                        result = iPassService.getGeneralResponse(header, balanceQueryResponse, SystemInstance.STATUS_SUCCESS);
                        break;
                }
                break;
            case "32800":
                switch (header.getServiceType()) {
                    case "Payment":
                    case "EZCardEDCAPayment":
                        IcashTradeSaleRequest icashTradeSaleRequest = new Gson().fromJson(request.getIcashData().substring(1, request.getIcashData().length() - 1), IcashTradeSaleRequest.class);
                        IcashTradeSaleResponse response = new Gson().fromJson(ticketData.toString(), IcashTradeSaleResponse.class);
                        if (StringUtils.isEmpty(response.getOrderId())) {
                            response.setOrderId(tradeDetail.getOrderId());
                        }
                        result = iCashService.updateDoTradeSale(header, requestData, device, merchant, icashTradeSaleRequest, response, tradeDetail, paymentAccount);
                        break;
                    case "Refund":
                        IcashTradeRefundResponse icashTradeRefundResponse = new Gson().fromJson(ticketData.toString(), IcashTradeRefundResponse.class);
                        result = iCashService.updateDoRefund(header, requestData, device, merchant, icashTradeRefundResponse, tradeDetail, paymentAccount);
                        break;
                    case "BalanceQuery":
                        IcashQueryCardInfoResponse icashQueryCardInfoResponse = new Gson().fromJson(ticketData.toString(), IcashQueryCardInfoResponse.class);
                        result = iCashService.getGeneralResponse(header, icashQueryCardInfoResponse, SystemInstance.STATUS_SUCCESS);
                        break;
                }
                break;
            case "33700":
                break;
        }
        return result;
    }

    /**
     * 走共同APII  尋卡+直接交易
     *
     * @param header
     * @param requestData
     * @param merchant
     * @param device
     * @return
     * @throws Exception
     */
    private String doFastTrans(RequestHeader header, JsonObject requestData, Merchant merchant, Device device) throws Exception {

        Map<String, Object> verificationResultMap = this.verification(header, requestData, merchant);
        PaymentAccount paymentAccount = ((List<PaymentAccount>) verificationResultMap.get("paymentAccountList")).get(0);

        TradeDetail tradeDetail = this.getTradeDetail(device, paymentAccount, header, requestData);

        TicketFastTransRequest request = this.getTicketFastTransRequest(header, requestData, device, paymentAccount, merchant, tradeDetail);

        String requestJsonStr = new ObjectMapper().writeValueAsString(request).replaceAll("\\\\\"", "\"").replaceAll("\"\\[", "[")
                .replaceAll("\\]\"", "]");
        LOGGER.info("[TICKET][doRequest_New]  request => " + requestJsonStr);
        String result = this.sendPost(TICKET_URL, requestJsonStr);
        LOGGER.info("[TICKET][doRequest_New]  response => " + result);
        TicketFastTransResponse response = new Gson().fromJson(result, TicketFastTransResponse.class);

        if (StringUtils.isNotEmpty(response.getNewAESKey())) {
            device.setAesKey(response.getNewAESKey());
            deviceService.save(device);
        }

        //最主要是 {"FastTransData":[{xxxxx}],"TicketData":{}}

        List<JsonObject> fastTransDataList = response.getFastTransDataList();
        JsonObject ticketData = response.getTicketData();
        if (CollectionUtils.isEmpty(fastTransDataList) || Objects.isNull(ticketData) || ticketData.isJsonNull()) {
            throw new InvokeAPIException(this.getClass().getSimpleName(), messageSource.getMessage("response.7350", null, locale));
        }

        JsonObject fastTransData = fastTransDataList.get(0);
        // 因台積電，所以這邊需跳過台積電帳號判斷
        if (!lookupCodeService.isTsmcAccount(tradeDetail.getAccountId())) {
            if ("Fail".equals(fastTransData.get("TXNResult").getAsString())) {
                throw new OtherAPIException(fastTransData.get("ErrorCode").getAsString() + " - " + fastTransData.get("ChineseErrorMessage").getAsString());
            }
        }

        tradeDetail = this.updateUserIdAndMethod(merchant, tradeDetail, fastTransData, device);
//        paymentAccount = tradeDetail.getPaymentAccount();
        header.setMethod(tradeDetail.getMethod());
        requestData.addProperty("UserId", fastTransData.get("TicketCardNumber").getAsString());
        result = this.updateTradeDetail(header, requestData, merchant, device, request, ticketData, tradeDetail);

        return result;
    }

    private String getIpassData(Device device, String amount, Merchant merchant, String serviceType, JsonObject requestData, TradeDetail tradeDetail) throws Exception {
        PaymentAccount paymentAccount = paymentAccountService.getOne(merchant.getAccountId(), 21);
        String result = "{}";
        if (Objects.nonNull(paymentAccount)) {
            switch (serviceType) {
                case "Payment":
                case "EZCardEDCAPayment":
                    IPassTradeSaleRequest iPassTradeSaleRequest = new IPassTradeSaleRequest(TEST_MODE, device, paymentAccount);
                    iPassTradeSaleRequest.setAmount(amount);
                    if (requestData.has("Cashier") && StringUtils.isNotEmpty(requestData.get("Cashier").getAsString())) {
                        iPassTradeSaleRequest.setPosOperatorID(requestData.get("Cashier").getAsString());
                    }
                    result = new ObjectMapper().writeValueAsString(iPassTradeSaleRequest);
                    break;
                case "Refund":
                    IPassTradeRefundRequest iPassTradeRefundRequest = new IPassTradeRefundRequest(TEST_MODE, device, paymentAccount);
                    iPassTradeRefundRequest.setAmount(amount);
                    iPassTradeRefundRequest.setCancelAddRrn(tradeDetail.getRrn());
                    result = new ObjectMapper().writeValueAsString(iPassTradeRefundRequest);
                    break;
                case "BalanceQuery":
                case "OrderQuery":
                case "TradeChargeCancel":
                    IPassOfflineInquiryCardRequest iPassOfflineInquiryCardRequest = new IPassOfflineInquiryCardRequest(TEST_MODE, device, paymentAccount);
                    result = new ObjectMapper().writeValueAsString(iPassOfflineInquiryCardRequest);
                    break;
            }
        }
        return result;
    }
}
