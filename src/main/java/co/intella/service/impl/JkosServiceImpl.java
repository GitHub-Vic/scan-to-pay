package co.intella.service.impl;

import co.intella.domain.integration.IntegratedMicropayResponseData;
import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.domain.jkos.*;
import co.intella.exception.ServiceTypeNotSupportException;
import co.intella.model.*;
import co.intella.net.HttpRequestUtil;
import co.intella.service.*;
import co.intella.utility.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Alex
 */
@Service
@Transactional
public class JkosServiceImpl implements JkosService {

    private final Logger LOGGER = LoggerFactory.getLogger(JkosServiceImpl.class);

    @Autowired
    private Environment env;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Autowired
    private MessageSource messageSource;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private OrderService orderService;

    @Resource
    private DeviceService deviceService;

    @Autowired
    private INT_TB_CancelTradeDetailService intTbCancelTradeDetailService;

    @Value("${api.url}")
    private String API_URL;

    @Value("${host.request.jkos}")
    private String JKOS_URL_ONLINE;

    @Value("${host.request.jkos.offline}")
    private String JKOS_URL_OFFLINE;

    private Locale locale = new Locale("zh_TW");

    /**
     * 被掃，這是之前寫的,規格書上沒看見,故沒有改過
     *
     * @param paymentAccount
     * @param merchant
     * @param requestHeader
     * @param data
     * @return
     * @throws Exception
     */
    @Override
    public String micropay(PaymentAccount paymentAccount, Merchant merchant, RequestHeader requestHeader, JsonPrimitive data) throws Exception {
        JkosMicropayRequestData jkosMicropayRequestData = new Gson().fromJson(data.getAsString(), JkosMicropayRequestData.class);
        String generalResponseData;
        String integrateMchId = requestHeader.getMerchantId();

        DateTime dt = DateTimeFormat.forPattern("yyyyMMddHHmmss").parseDateTime(requestHeader.getCreateTime());
        jkosMicropayRequestData.setPosTradeTime(dt.toString("yyyy/MM/dd HH:mm:ss"));
        jkosMicropayRequestData.setUnRedeem(0);
        jkosMicropayRequestData.setSendTime(DateTime.now().toString("yyyyMMddHHmmss"));

        jkosMicropayRequestData.setStoreName(merchant.getName());
        jkosMicropayRequestData.setMerchantId(paymentAccount.getAccount());
        jkosMicropayRequestData.setStoreId(paymentAccount.getAppId());
        jkosMicropayRequestData.setSign(getSign(jkosMicropayRequestData, paymentAccount.getHashKey()));
        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
        JkosUtil jkosUtil = new JkosUtil();

        try {
            LOGGER.info("[Jkos][Micropay][Request].." + new ObjectMapper().writeValueAsString(jkosMicropayRequestData));
            String result = this.senJkosdRequest(new ObjectMapper().writeValueAsString(jkosMicropayRequestData), requestHeader.getServiceType(), paymentAccount);
            LOGGER.info("[Jkos][Micropay][Response].." + result);

            if (StringUtils.isBlank(result)) {
                throw new Exception("result is blank");
            }

            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);

            JkosMicropayResponseData jkosMicropayResponseData = new Gson().fromJson(result, JkosMicropayResponseData.class);

            ResponseGeneralHeader responseGeneralHeader = jkosUtil.getJkosResponseHeader(jkosMicropayResponseData, requestHeader, integrateMchId, messageSource);

            //IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();
            IntegratedMicropayResponseData integratedMicropayResponseData = jkosUtil.getGeneralResponseData(jkosMicropayResponseData, requestHeader);
            integratedMicropayResponseData.setAuthCode(jkosMicropayRequestData.getCardToken());
            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedMicropayResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);
        } catch (Exception e) {
            LOGGER.info("[Jkos][Micropay][ERROR].." + Arrays.toString(e.getStackTrace()));
//                this.doCancel(tradeDetail);

            ResponseGeneralHeader responseGeneralHeader = jkosUtil.getJkosResponseHeader(null, requestHeader, integrateMchId, messageSource);
            JkosMicropayResponseData jkosMicropayResponseData = new JkosMicropayResponseData();
            jkosMicropayResponseData.setMerchantTradeNo(tradeDetail.getOrderId());
            IntegratedMicropayResponseData integratedMicropayResponseData = jkosUtil.getGeneralResponseData(jkosMicropayResponseData, requestHeader);

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedMicropayResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);
        }


        LOGGER.info("integrate response ready :" + generalResponseData);
        return generalResponseData;
    }

    @Override
    public String refund(PaymentAccount paymentAccount, Merchant merchant, RequestHeader requestHeader, JsonPrimitive data) throws Exception {
        String mchId = requestHeader.getMerchantId();
        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
        requestHeader.setMerchantId(mchId);

        if (SystemInstance.TYPE_MICROPAY.equalsIgnoreCase(tradeDetail.getServiceType())) {
            //反掃走 之前的線下退款
            return this.refundOffLine(paymentAccount, merchant, requestHeader, data, tradeDetail);
        } else {
            return this.refundOnLine(paymentAccount, merchant, requestHeader, data, tradeDetail);
        }

    }

    @Override
    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {


        JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
        String generalResponseData;

        LOGGER.info("[Jkos][doRequest]");


        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        PaymentAccount paymentAccount = paymentAccountService.getOne(integrateMchId, bankId);
        Merchant merchant = merchantService.getOne(integrateMchId);

        switch (requestHeader.getServiceType()) {
            case SystemInstance.TYPE_MICROPAY:
                generalResponseData = this.micropay(paymentAccount, merchant, requestHeader, data);
                break;
            case "Refund":
                generalResponseData = this.refund(paymentAccount, merchant, requestHeader, data);
                break;
            case "SingleOrderQuery":
                generalResponseData = this.singleOrderQuery(paymentAccount, merchant, requestHeader, data);
                break;
            default:
                generalResponseData = "Service Type not found";
                break;
        }
        return generalResponseData;

    }


    private String getSign(JkosBasicRequest request, String hashKey) throws Exception {
        StringBuilder checkStr = new StringBuilder();

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(request);
        TreeMap map = new Gson().fromJson(json, TreeMap.class);

        //Gson convert int to double
        String a = "" + map.get("TradeAmount");
        map.put("TradeAmount", Integer.valueOf(a.substring(0, a.length() - 2)));

        if (map.get("UnRedeem") != null) {
            String b = "" + map.get("UnRedeem");
            map.put("UnRedeem", Integer.valueOf(b.substring(0, b.length() - 2)));
        } else {
            map.remove("UnRedeem");
        }
        map.remove("Sign");
        String sortedJson = new Gson().toJson(map);
        sortedJson = sortedJson + hashKey;
        return getHash(sortedJson);
    }

    private String getHash(String pin) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return byte2Hex(digest.digest(pin.getBytes()));

    }

    private String byte2Hex(byte[] inputBytes) {
        String result = "";
        for (byte currentByte : inputBytes) result += Integer.toString((currentByte & 0xff) + 0x100, 16).substring(1);
        return result;
    }


    @Override
    public void doCancel(TradeDetail tradeDetail) throws InterruptedException {

        INT_TB_CancelTradeDetail intTbCancelTradeDetail = intTbCancelTradeDetailService.getOne(tradeDetail);
        if (Objects.isNull(intTbCancelTradeDetail)) {
            intTbCancelTradeDetail = new INT_TB_CancelTradeDetail();
            intTbCancelTradeDetail.setCrDate(new Date());
        }

        LOGGER.info("[doCancel] satart :" + tradeDetail.getOrderId());
        JkosCancelRequestData requestData = new JkosCancelRequestData(tradeDetail);
        LOGGER.info("[doCancel] requestData :" + requestData);
        String result = "";
        try {
            result = this.cancel(requestData, tradeDetail.getPaymentAccount().getHashKey());
            LOGGER.info("[doCancel] result :" + result);
            JkosCancelResponseData responseData = new Gson().fromJson(result, JkosCancelResponseData.class);
            if ("000".equals(responseData.getStatusCode())) {
                intTbCancelTradeDetail.setCancelStatus("S");
            } else {
                intTbCancelTradeDetail.setCancelStatus("F");
            }

            intTbCancelTradeDetail.setResponse(result);

        } catch (Exception e) {
            LOGGER.debug("[doCancel] ERROR " + Arrays.toString(e.getStackTrace()));
            intTbCancelTradeDetail.setCancelStatus("F");
            intTbCancelTradeDetail.setResponse("Exception : " + e.getMessage());
        } finally {
            if (StringUtils.isBlank(intTbCancelTradeDetail.getOrderId())) {
                intTbCancelTradeDetail.setOrderId(tradeDetail.getOrderId());
            }
            if (StringUtils.isBlank(intTbCancelTradeDetail.getAccountId())) {
                intTbCancelTradeDetail.setAccountId(tradeDetail.getAccountId());
            }
            if (StringUtils.isBlank(intTbCancelTradeDetail.getMethod())) {
                intTbCancelTradeDetail.setMethod(tradeDetail.getMethod());
            }
            intTbCancelTradeDetail.setRequest(JsonUtil.toJsonStr(requestData));
            intTbCancelTradeDetail.setDateStamp(new Date());
            intTbCancelTradeDetailService.save(intTbCancelTradeDetail);
            LOGGER.info("[doCancel] intTbCancelTradeDetail  :" + JsonUtil.toJsonStr(intTbCancelTradeDetail));
        }
    }

    public String cancel(JkosCancelRequestData request, String hashKey) throws Exception {

        request.setSign(getSign(request, hashKey));
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(request);
        LOGGER.info("[Jkos][cancel][Request].." + json);

        return HttpRequestUtil.post(env.getProperty("host.request.jkos.offline") + "Cancel", json);
    }


    @Override
    public String getPaymentUrl(String amount, String shortId, String agen) throws Exception {

        String result = null;
        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(shortId);

        if (!Objects.isNull(qrcodeParameter)) {

            String storeOrderNo = "1".equals(qrcodeParameter.getCodeType()) ? qrcodeParameter.getStoreOrderNo() : orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);

            Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());
            PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 36);

            if (Objects.isNull(paymentAccount)) {
                throw new ServiceTypeNotSupportException("Exception: The store's account isn't opened.");
            }

            JkosEntryRequestData jkosEntryRequestData = new JkosEntryRequestData();
            jkosEntryRequestData.setPlatformOrderId(storeOrderNo);
            jkosEntryRequestData.setStoreId(paymentAccount.getAccount());
            jkosEntryRequestData.setCurrency("TWD");
            jkosEntryRequestData.setTotalPrice(Integer.valueOf(amount));
            jkosEntryRequestData.setFinalPrice(Integer.valueOf(amount));
            jkosEntryRequestData.setResultUrl(String.format("%sallpaypass/api/jkos/notify", API_URL));
            jkosEntryRequestData.setResultDisplayUrl(String.format("%sallpaypass/api/payment/order/result?orderId=%s", API_URL, EncryptUtil.encryptSimple(storeOrderNo)));
            jkosEntryRequestData.setValidtime(DateTime.now().plusMinutes(30).toString("yyyy-MM-dd HH:mm"));

            this.setOnlineTradeDetailRequest(jkosEntryRequestData, merchant, paymentAccount, qrcodeParameter);

            String request = new ObjectMapper().writeValueAsString(jkosEntryRequestData);
            LOGGER.info("[JKOS][OnLine][Request]: " + request);
            for (int i = 0; i < 2; i++) {
                try {
                    result = this.senJkosdRequest(request, "OLPay", paymentAccount);

                    if (org.apache.commons.lang.StringUtils.isNotBlank(result)) {
                        JkosEntryResponseData jkosEntryResponseData = JsonUtil.parseJson(request, JkosEntryResponseData.class);
                        if ("000".equals(jkosEntryResponseData.getResult())) {
                            TradeDetail tradeDetail = tradeDetailService.getOne(merchant.getAccountId(),
                                    jkosEntryRequestData.getPlatformOrderId());
                            tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
                            tradeDetailService.save(tradeDetail);
                        }
                        break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            LOGGER.info("[JKOS][OnLine][Response]: " + result);
        } else {
            LOGGER.error("[REQUEST][JKOS] qrcode not found");
        }

        return result;

    }

    private TradeDetail setOnlineTradeDetailRequest(JkosEntryRequestData jkosEntryRequestData,
                                                    Merchant merchant, PaymentAccount paymentAccount, QrcodeParameter qrcodeParameter) {
        LOGGER.info("enter trade detail request:" + new Gson().toJson(jkosEntryRequestData));

        TradeDetail tradeDetail = tradeDetailService.getOne(merchant.getAccountId(),
                jkosEntryRequestData.getPlatformOrderId());

        if (Objects.isNull(tradeDetail)) {

            tradeDetail = new TradeDetail();
            tradeDetail.setAccountId(merchant.getAccountId());
            tradeDetail.setMethod("13600");
            tradeDetail.setPaymentAccount(paymentAccount);
            tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
            tradeDetail.setStatus(SystemInstance.TRADE_CREATING_ORDER);
            tradeDetail.setOrderId(jkosEntryRequestData.getPlatformOrderId());
            tradeDetail.setDeviceRandomId(deviceService.getOne(merchant, "APP-001"));
            tradeDetail.setPayment(jkosEntryRequestData.getTotalPrice());
            tradeDetail.setServiceType("OLPay");
            tradeDetail.setQrcodeToken(qrcodeParameter.getShortId());
            tradeDetail.setDescription(qrcodeParameter.getBody());

            if (qrcodeParameter.getOnSaleTotalFee() == jkosEntryRequestData.getTotalPrice()) {
                tradeDetail.setOnSale(true);
            } else {
                tradeDetail.setOnSale(false);
            }
            tradeDetail.setOriginalPrice(qrcodeParameter.getTotalFee());

            tradeDetail = tradeDetailService.save(tradeDetail);
            tradeProcedureService.captureOrderSave(tradeDetail);
        }
        LOGGER.info("save online trade detail : " + new Gson().toJson(tradeDetail));
        return tradeDetail;
    }

    private String senJkosdRequest(String requestData, String serviceType, PaymentAccount paymentAccount) throws Exception {

        switch (serviceType) {
            case SystemInstance.TYPE_MICROPAY:
                return HttpRequestUtil.post(JKOS_URL_OFFLINE + "/Payment", requestData);
            case "Refund_OFFLINE":
                return HttpRequestUtil.post(JKOS_URL_OFFLINE + "/Refund", requestData);
            case "OLPay":
                return this.postJson(JKOS_URL_ONLINE + "/entry", requestData, paymentAccount.getHashKey(), paymentAccount.getHashIV());
            case "Refund_ONLINE":
                return this.postJson(JKOS_URL_ONLINE + "/refund", requestData, paymentAccount.getHashKey(), paymentAccount.getHashIV());
            case "SingleOrderQuery":
                return this.doGet(JKOS_URL_ONLINE + "/inquiry?platform_order_ids=" + requestData, "platform_order_ids=" + requestData, paymentAccount.getHashKey(), paymentAccount.getHashIV());
            default:
                return null;
        }
    }

    private String postJson(String url, String data, String apiKey, String secretKey) throws Exception {

        Map<String, String> headerMap = this.getHeaderMap(data, apiKey, secretKey);
        return HttpUtil.doPostJson(headerMap, url, data);
    }

    private String doGet(String url, String data, String apiKey, String secretKey) throws Exception {

        Map<String, String> headerMap = this.getHeaderMap(data, apiKey, secretKey);
        return HttpUtil.doGet(url, headerMap);
    }

    private Map<String, String> getHeaderMap(String data, String apiKey, String secretKey) throws Exception {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("DIGEST", GeneralUtil.getHmacSha256(data, secretKey.getBytes("UTF-8")));
        headerMap.put("API-KEY", apiKey);
        return headerMap;
    }

    /**
     * 被掃退款，一樣，直接按照之前的，原封不動搬過來
     *
     * @param paymentAccount
     * @param merchant
     * @param requestHeader
     * @param data
     * @param tradeDetail
     * @return
     * @throws Exception
     */
    private String refundOffLine(PaymentAccount paymentAccount, Merchant merchant, RequestHeader requestHeader, JsonPrimitive data, TradeDetail tradeDetail) throws Exception {
        JkosRefundRequestData jkosRefundRequestData = new Gson().fromJson(data.getAsString(), JkosRefundRequestData.class);

        DateTime dt = DateTimeFormat.forPattern("yyyyMMddHHmmss").parseDateTime(requestHeader.getCreateTime());
        jkosRefundRequestData.setPosTradeTime(dt.toString("yyyy/MM/dd HH:mm:ss"));
        jkosRefundRequestData.setSendTime(DateTime.now().toString("yyyyMMddHHmmss"));

        jkosRefundRequestData.setStoreName(merchant.getName());
        jkosRefundRequestData.setMerchantId(paymentAccount.getAccount());
        jkosRefundRequestData.setStoreId(paymentAccount.getAppId());

        String ms5Str = GeneralUtil.getMD5(jkosRefundRequestData.getMerchantTradeNo() + new SimpleDateFormat("yyyyMMddhhmmss").format(new Date())).toLowerCase();
        jkosRefundRequestData.setExtra1(jkosRefundRequestData.getMerchantTradeNo());
        jkosRefundRequestData.setMerchantTradeNo(ms5Str);

        jkosRefundRequestData.setTradeNo(tradeDetail.getSystemOrderId());
        jkosRefundRequestData.setSign(getSign(jkosRefundRequestData, paymentAccount.getHashKey()));

        LOGGER.info("[Jkos][Refund_OFFLINE][Request].." + new ObjectMapper().writeValueAsString(jkosRefundRequestData));
        String result = this.senJkosdRequest(new ObjectMapper().writeValueAsString(jkosRefundRequestData), requestHeader.getServiceType() + "_OFFLINE", paymentAccount);
        LOGGER.info("[Jkos][Refund_OFFLINE][Response].." + result);
        tradeProcedureService.setTradeDetailResponse(requestHeader.getMerchantId(), requestHeader, result, data);

        JkosRefundResponseData jkosRefundResponseData = new Gson().fromJson(result, JkosRefundResponseData.class);

        JkosUtil jkosUtil = new JkosUtil();

        ResponseGeneralHeader responseGeneralHeader = jkosUtil.getJkosResponseHeader(jkosRefundResponseData, requestHeader, requestHeader.getMerchantId(), messageSource);

        //IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();
        IntegratedRefundResponseData integratedRefundResponseData = jkosUtil.getRefundGeneralResponseData(jkosRefundResponseData, requestHeader, requestHeader.getMerchantId(), tradeDetail);

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        responseGeneralBody.setHeader(responseGeneralHeader);
        responseGeneralBody.setData(integratedRefundResponseData);

        String generalResponseData = new Gson().toJson(responseGeneralBody);
        LOGGER.info("integrate response ready :" + generalResponseData);

        return generalResponseData;
    }

    private String refundOnLine(PaymentAccount paymentAccount, Merchant merchant, RequestHeader requestHeader, JsonPrimitive data, TradeDetail tradeDetail) throws Exception {

        Map<String, String> reqMap = new HashMap<>();

        reqMap.put("platform_order_id", tradeDetail.getOrderId());
        reqMap.put("refund_amount", String.valueOf(tradeDetail.getPayment()));

        String reqJsonStr = JsonUtil.toJsonStr(reqMap);

        LOGGER.info("[Jkos][Refund_ONLINE][Request].." + reqJsonStr);
        String result = this.senJkosdRequest(reqJsonStr, requestHeader.getServiceType() + "_ONLINE", paymentAccount);
        LOGGER.info("[Jkos][Refund_ONLINE][Response].." + result);

        tradeProcedureService.setTradeDetailResponse(requestHeader.getMerchantId(), requestHeader, result, data);


        //TODO 以下建立回應這部分，之後要整理起來
        IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();
        Map<String, String> respMap = JsonUtil.parseJson(result, Map.class);
        Map<String, String> resultMap = JsonUtil.parseJson(JsonUtil.toJsonStr(respMap.get("result_object")), Map.class);

        if ("000".equals(String.valueOf(respMap.get("result")))) {
            integratedRefundResponseData.setSysRefundNo(resultMap.get("refund_tradeNo"));
            integratedRefundResponseData.setRefundedAt(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        } else {
            integratedRefundResponseData.setSysRefundNo(SystemInstance.EMPTY_STRING);
            integratedRefundResponseData.setRefundedAt(SystemInstance.EMPTY_STRING);
        }
        integratedRefundResponseData.setStoreRefundNo(tradeDetail.getStoreRefundId());
        integratedRefundResponseData.setStoreOrderNo(tradeDetail.getOrderId());
        integratedRefundResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());

        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setMchId(tradeDetail.getAccountId());
        responseGeneralHeader.setMethod(tradeDetail.getMethod());
        responseGeneralHeader.setServiceType("Refund");
        responseGeneralHeader.setStatusCode(SystemInstance.STATUS_SUCCESS);
        responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        responseGeneralBody.setHeader(responseGeneralHeader);
        responseGeneralBody.setData(integratedRefundResponseData);

        String generalResponseData = new Gson().toJson(responseGeneralBody);

        return generalResponseData;
    }

    private String singleOrderQuery(PaymentAccount paymentAccount, Merchant merchant, RequestHeader requestHeader, JsonPrimitive data) throws Exception {


        JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
//        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

        String result = this.senJkosdRequest(requestData.get("StoreOrderNo").getAsString(), requestHeader.getServiceType(), paymentAccount);
        LOGGER.info("[Jkos][singleOrderQuery].." + result);
        tradeProcedureService.setTradeDetailResponse(requestHeader.getMerchantId(), requestHeader, result, data);//TODO

        return "";
    }

}
