package co.intella.service.impl;

import co.intella.model.IntTbAbnormalRuleCtrl;
import co.intella.net.HttpRequestUtil;
import co.intella.service.AbnormalRuleCtrlService;
import co.intella.utility.JsonUtil;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

@Service
public class AbnormalRuleCtrlServiceImpl implements AbnormalRuleCtrlService {

    @Value("${host.dao}")
    private String DAO_URL;

    @Override
    public IntTbAbnormalRuleCtrl findOneMaxVersionByLocation(String merchantSeqId, String location) {

        List<IntTbAbnormalRuleCtrl> list = null;
        try {
            Gson gson = JsonUtil.getGson();
            String responseEntity = HttpRequestUtil.httpGet(DAO_URL + "api/crm/AbnormalRuleCtrl/findAll/" + merchantSeqId + "/" + location);

            list = gson.fromJson(responseEntity, new TypeToken<List<IntTbAbnormalRuleCtrl>>() {
            }.getType());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return CollectionUtils.isNotEmpty(list) ? list.get(0) : null;
    }

}
