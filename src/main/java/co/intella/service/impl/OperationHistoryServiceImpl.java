package co.intella.service.impl;

import co.intella.domain.contratStore.OperationHistory;
import co.intella.net.HttpRequestUtil;
import co.intella.service.OperationHistoryService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @author Miles
 */
@Service
public class OperationHistoryServiceImpl implements OperationHistoryService {

    private final Logger LOGGER = LoggerFactory.getLogger(OperationHistoryServiceImpl.class);

    private static String HOST_DAO = "http://localhost:8080/scan2pay-data/";

    public List<OperationHistory> list(String merchantId) {

        try {
            String result = HttpRequestUtil.httpGet(HOST_DAO + "api/opHistory/list/" + merchantId);
            return new Gson().fromJson(result, new TypeToken<List<OperationHistory>>(){}.getType());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return null;
        }
    }

    public void save(OperationHistory operationHistory) {
        try {
            String result = HttpRequestUtil.post(HOST_DAO + "api/opHistory/save", new Gson().toJson(operationHistory));
            LOGGER.info("save " + result);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
        }
    }
}
