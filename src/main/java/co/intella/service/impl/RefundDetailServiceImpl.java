package co.intella.service.impl;

import co.intella.domain.contratStore.RefundDetailVo;
import co.intella.model.QRefundDetail;
import co.intella.model.RefundDetail;
import co.intella.net.HttpRequestUtil;
import co.intella.repository.RefundDetailRepository;
import co.intella.service.RefundDetailService;
import co.intella.utility.HttpUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Iterables;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.apache.commons.collections.CollectionUtils;
import org.assertj.core.util.Lists;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Alex
 */
@Service
@Transactional
public class RefundDetailServiceImpl implements RefundDetailService {

    private final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(RefundDetailServiceImpl.class);

    @Resource
    private RefundDetailRepository refundDetailRepository;

    @Value("${host.dao}")
    private String DAO_URL;

    public RefundDetail save(RefundDetail refundDetail) {
        try {
            String json = new ObjectMapper().writeValueAsString(refundDetail);
            LOGGER.info("[SQL] save. " + json);
            String response = HttpUtil.post(DAO_URL + "api/refund/saveOrUpdate", json);
            return new Gson().fromJson(response, RefundDetail.class);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] save() " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return null;
        }
    }

    public RefundDetail getOne(String orderId) {
        QRefundDetail qRefundDetail = QRefundDetail.refundDetail;
        BooleanExpression predicate = qRefundDetail.orderId.eq(orderId).and((qRefundDetail.status.eq("Refund success").or(qRefundDetail.status.eq("Cancel success"))));

        LOGGER.info("[SQL] getOne. " + predicate.toString());
        return refundDetailRepository.findOne(predicate);
    }

    public RefundDetail getOneByStoreRefundId(String accountId, String storeRefundId) {

        QRefundDetail qRefundDetail = QRefundDetail.refundDetail;
        BooleanExpression predicate = qRefundDetail.storeRefundId.eq(storeRefundId)
                .and(qRefundDetail.accountId.eq(accountId));

        LOGGER.info("[SQL] getOneByStoreRefundId. " + predicate.toString());
        return refundDetailRepository.findOne(predicate);
    }

    public RefundDetail getTopOneByStoreRefundId(String accountId, String storeRefundId) {

        QRefundDetail qRefundDetail = QRefundDetail.refundDetail;
        BooleanExpression predicate = qRefundDetail.storeRefundId.eq(storeRefundId)
                .and(qRefundDetail.accountId.eq(accountId));

        LOGGER.info("[SQL] getTopOneByStoreRefundId. " + predicate.toString());

        Iterable<RefundDetail> a = refundDetailRepository.findAll(predicate, new Sort(Sort.Direction.DESC, "sysTime"));
        if (Iterables.size(a) == 0)
            return null;
        if (a instanceof List) {
            //return (List<RefundDetail>) a;
        }
        ArrayList<RefundDetail> list = new ArrayList<RefundDetail>();
        if (a != null) {
            for (RefundDetail e : a) {
                list.add(e);
            }
        }

        return list.get(0);
    }

    public RefundDetail getOneByOrderIdAndMethod(String orderId, String method) {
        QRefundDetail qRefundDetail = QRefundDetail.refundDetail;
        BooleanExpression predicate = qRefundDetail.orderId.eq(orderId)
                .and(qRefundDetail.method.eq(method))
                .and((qRefundDetail.status.eq("Refund success").or(qRefundDetail.status.eq("Cancel success"))));

        LOGGER.info("[SQL] getOneByOrderIdAndMethod. " + predicate.toString());
        List<RefundDetail> list = Lists.newArrayList(refundDetailRepository.findAll(predicate));
        if (list != null && list.size() == 1) {
            return list.get(0);
        }

        if (list != null && list.size() >= 2) {
            for (RefundDetail refundDetail : list) {
                if (refundDetail.getStatus().equals("Cancel success") || refundDetail.getStatus().equals("Refund success")) {
                    return refundDetail;
                }
            }
        }

        return null;
    }

    public List<RefundDetailVo> listByBatchNo(String merchantId, String deviceId, String batchNo) {
        try {
            String result = HttpRequestUtil.httpGet(DAO_URL + "api/refund/list/" + merchantId + "/" + deviceId + "/" + batchNo);

            LOGGER.info("listByBatchNo : " + result);
            return new Gson().fromJson(result, new TypeToken<List<RefundDetailVo>>() {
            }.getType());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return null;
        }
    }

    public List<RefundDetailVo> listGroupEZC(String terminalId, String deviceId, String batchNo) {
        try {
            String result = HttpRequestUtil.httpGet(DAO_URL + "api/refund/listEzc/" + terminalId + "/" + deviceId + "/" + batchNo);

            LOGGER.info("listGroupEZC : " + result);
            return new Gson().fromJson(result, new TypeToken<List<RefundDetailVo>>() {
            }.getType());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return null;
        }
    }


    public RefundDetail getOneByOrderId(String orderId) {
        QRefundDetail qRefundDetail = QRefundDetail.refundDetail;
        BooleanExpression predicate = qRefundDetail.orderId.eq(orderId);

        LOGGER.info("[SQL] getOneByOrderId. " + predicate.toString());
        List<RefundDetail> list = Lists.newArrayList(refundDetailRepository.findAll(predicate));
//        if(list != null && list.size() == 1) {
//            return list.get(0);
//        }

        if (list != null && list.size() >= 2) {
            for (RefundDetail refundDetail : list) {
                if (refundDetail.getStatus().equals("Cancel success") || refundDetail.getStatus().equals("Refund success")) {
                    return refundDetail;
                }
            }
        }

        return CollectionUtils.isNotEmpty(list) ? list.get(0) : null;
    }
}
