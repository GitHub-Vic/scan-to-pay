package co.intella.service.impl;


import co.intella.model.DiscountCtrlDetail;
import co.intella.service.DiscountCtrlDetailService;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DiscountCtrlDetailServiceImpl implements DiscountCtrlDetailService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Value("${host.dao}")
    private String DAO_URL;


    @Override
    public List<DiscountCtrlDetail> getDetailList(long dicountCtrlSeq) {
        String result = HttpUtil.doGet(DAO_URL + "api/discountCtrlDetail/getDetailList/" + dicountCtrlSeq);
        return JsonUtil.parseJsonList(result, DiscountCtrlDetail.class);
    }
}
