package co.intella.service.impl;

import co.intella.domain.iCashPay.*;
import co.intella.domain.integration.IntegratedMicropayResponseData;
import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.exception.OrderAlreadyExistedException;
import co.intella.exception.OtherAPIException;
import co.intella.exception.ServiceTypeNotSupportException;
import co.intella.model.*;
import co.intella.service.*;
import co.intella.utility.HttpUtil;
import co.intella.utility.IcashPayUtil;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
public class IcashPayServiceImpl implements IcashPayService {

    private final String pubkey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApEJCwwG/Rv9mihUSd83/" +
            "mSZSHVwuxhnSsJUwyN8dkCYQIhwyh/PPZc6gVxcKsdH7JP/5AQUeWgNtFsOunMcn" +
            "q1yb0FTKQ7mu3BBdcGTkIm88yC1FUN4uXjiYqRG7osGEgS/JQlI55yawqF51os8O" +
            "Nc6Cx4NYBc70YBU3oRler4VOb0Ct8Ju7UP46pFQv1xXYbyTJZA/mTrCR5yItSZyn" +
            "VrXFUNCjokEnl+xv90MbIrD5r9EVs18K5OaWHM7sZ8STCHScDL//FDJRMGPYfAzr" +
            "m9bVydqugBMWrgk/onDZMQ2BSiJwXYNpwlzL6X5avzE5UzYqmPH6GiBc304mMh5a" +
            "nQIDAQAB";
    private final String privateKey = "MIIEogIBAAKCAQEAsBLMsBp1Sz5TX5HnZ5BJiu8jskQuLwZA2LqB7bfHzOHkx/mT" +
            "0bNlfnhRnCWTH9yTdqUDP3Jf/0FKoPh0+eF/essoOHtjv8VjW7QZSdq3skp5oVhr" +
            "o3SIPoo4zaqwSJw4YGO3/tltcI5Ny1VtBHuxy2zJGEKgDNR7CrQntnYfTS/LT18w" +
            "HgQ5aW4A2tsWrWL0ZB2f1DKCCnbTevlN/5jhPw5GuGm7zyMVImT7hGQtRlwhqqNi" +
            "YmJvfC2oQmb2AZ+RBcl1szPf88QtPIAx5rNJXI2M8H+oMYpOZy2RLyhj6Bvd3Fpd" +
            "WmCbHoK3yWcd7ipS30+YXC6bZCYJwdr/O4cHMwIDAQABAoIBAFJnegSxXVPpx3ti" +
            "/boqfUDYywwTfMnlU1QbddObv5bT4g71b4klPMsD5K3/p+uxgBJ3CB4ZmXQNSrAr" +
            "mBOaxd7iVeCZXwXRi77Pe0ZlZrO6x1IL39SQJXOfnRYkXHyfIIN8KBDOEbor35za" +
            "Tjta6MscDsGb7OwURcGsryll/yBQBysHS9hCf+QAoggXT1lXZPKNZYDncj3wsmCi" +
            "lAk4RPa09MGZyoXcH7PTO33/f5u/pqkdu09TqVFt4IJ32BO8KTUbS5KQR/mcWWrg" +
            "0qXnBgCm3U02MTmVPW8XEmVN2BtZKlP7/PGIFKFaBtWuD+jc9aTHomRENMeBALVv" +
            "wfWGpIECgYEA3CxQ6vV56VA1GUI2j7El4T5YZzDxVfRf1HWxP5bBZZwTTzlKiJq4" +
            "n3aXTLCgSyWG5HFKjNif/dhaR2miUNA/J6dT5OpC8MAQ/gWi/9wOiWbAY51gtgM0" +
            "8QLZrhBtaMss7MY73Su4/pQ+H9mu88NdOGxUAX1vOynCXwXqVplV+b8CgYEAzLlv" +
            "1PyeaOIQqH3bN6fq08P/gwjnzm2rOaJrRbsiakjj5uH0f7orYaPmZNWtM364G11d" +
            "3y19mP4KP3VX2qWF1DEmovbqCSj7ATct4B/Lxz2sU8HMS+xPS/7MVGTWgx2XKENW" +
            "UELpQ0Fdf1379RfdRjyFNhYcx6AZnboRDDY3x40CgYBVr0V6ezgLrbRMdTFIsJde" +
            "VmYPGsQBQI5+36KwbZosUhZNsa0pBpWtMXjmVqK7PAqIIvLxZ2i0YJuZySlrK/A4" +
            "HW2P2/9bNFQol8b8rgZWQM/EtL2reAoFdaeDj08VBmUgbHFlvJtNMCIRQSVOJozD" +
            "5Zy3y7obG0BSvLmbJCeKfwKBgAI2LqMaZ4uyhYOMeIXNZpgLEgOUYQy12pewzX/f" +
            "WavURcIExuNqW3Wek6qbNFfOEqcZBx6WGzk/5VRyLBTMnPmB3mz8ReL7clq9Ad3d" +
            "+hGY9PeirFZVozWq9wIiZL7d3vgsYg5T6rkzR5NMrKAlNdrAZKAE9SKi9CPkErMT" +
            "Et8xAoGAIQhLahumFYxEN6ZqX3MH5E3kHoU2cSylwhlSomIahqZGpv0ZXSVyQJIs" +
            "nefNCFpLrmOuJx5qKH8O+sN/UvwUr4yHtbXyQielNVhJ2VBHeRHOkkWGvAay9l1U" +
            "b6stLXusYG4rOEntsItGgdyi7tKWqxCE5Mps12RziVqg/v9LP94=";

    private final Logger LOGGER = LoggerFactory.getLogger(IcashPayServiceImpl.class);
    private final String micropayUrl = "Pos/DeductICPOF";
    private final String refundUrl = "Cashier/RefundICPO";
    private final String queryOrderUrl = "Cashier/QueryTradeICPO";
    private final String onlinepayUrl = "Cashier/CreateTradeICPO";

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private OrderService orderService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private DeviceService deviceService;

    @Autowired
    private Environment env;

    @Autowired
    private AppNotificationService appNotificationService;

    @Value("${api.url}")
    private String API_URL;

    @Value("${host.request.iCashPayUrl}")
    private String ICASHPAY_URL;

    @Override
    public String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {
        String generalResponseData = SystemInstance.EMPTY_STRING;

        String integrateMchId = requestHeader.getMerchantId();
        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        Merchant merchant = merchantService.getOne(requestHeader.getMerchantId());

        PaymentAccount paymentAccount = paymentAccountService.getOneByDao(integrateMchId, bankId);
        if (Objects.isNull(paymentAccount)) {
            throw new ServiceTypeNotSupportException("Exception: The store's account isn't opened.");
        }

        String serviceType = requestHeader.getServiceType();
        switch (serviceType) {
            case SystemInstance.TYPE_MICROPAY:
                generalResponseData = this.doMicropay(requestHeader, data, merchant, paymentAccount);
                break;
            case "Refund":
                generalResponseData = this.doRefund(requestHeader, data, merchant, paymentAccount);
                break;
            case "SingleOrderQuery":
                this.queryMerchantOrder(requestHeader, data, merchant, paymentAccount); //由IntegratedController 那邊重新抓取訂單狀態回復
                break;
            default:
                LOGGER.error("Service type not support. " + serviceType);
                generalResponseData = "Service type not support.";
        }

        return generalResponseData;
    }

    private Map<String, String> sendPost(String serviceType, String encData, PaymentAccount paymentAccount, String Signaturekey, String serviceTypeUrl) throws Exception {

        HashMap<String, String> headerData = new HashMap<>();
        headerData.put("PlatformID", "1682122000000059");//平台電支帳號
        headerData.put("MerchantID", paymentAccount.getAccount());//特店業者電支帳號
        headerData.put("Version", "1.0");//主版號，目前固定1.0
        headerData.put("X-iCP-EncKeyID", "63083");//AES加密金鑰ID
        headerData.put("X-iCP-Signature", Signaturekey);//ClientPrivateKey

        HashMap<String, String> dataBody = new HashMap<>();
        dataBody.put("EncData", encData);
        CloseableHttpClient httpclient = HttpClients.createDefault();
        CloseableHttpResponse result = HttpUtil.doPost(httpclient, ICASHPAY_URL + serviceTypeUrl, dataBody, headerData);
        String responseStr = HttpUtil.getResponseStr(result);
        String resSignature = result.getFirstHeader("X-iCP-Signature").getValue();

        LOGGER.info("[icashpay][" + serviceType + "][Response][result]:  " + result);
        LOGGER.info("[icashpay][" + serviceType + "][Response][resultbody]:  " + responseStr);
        LOGGER.info("[icashpay][" + serviceType + "][Response][resSignature]:  " + resSignature);

        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("responseStr", responseStr);
        resultMap.put("resSignature", resSignature);

        return resultMap;
    }

    private String doMicropay(RequestHeader requestHeader, JsonPrimitive data, Merchant merchant, PaymentAccount paymentAccount) throws Exception {
        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

        IcpMicRequestEncData icpMicRequestEncData = new IcpMicRequestEncData(paymentAccount, tradeDetail);

        String micEncData = new Gson().toJson(icpMicRequestEncData);
        LOGGER.info("[icashpay][doMicropay][Request][DataToJson]:  " + micEncData);
        IcashPayUtil icashPayUtil = new IcashPayUtil();
        String encrypt = icashPayUtil.encrypt(micEncData, paymentAccount.getHashKey(), paymentAccount.getHashIV());
        String Signaturekey = icashPayUtil.makeSign(encrypt, privateKey);
        Map<String, String> resultMap = this.sendPost(requestHeader.getServiceType(), encrypt, paymentAccount, Signaturekey, micropayUrl);
        String responseStr = resultMap.get("responseStr");
        String resSignature = resultMap.get("resSignature");

        JsonObject responseObject = new Gson().fromJson(responseStr, JsonObject.class);
        String resEncData = icashPayUtil.decrypt(responseObject.get("EncData").toString(), paymentAccount.getHashKey(), paymentAccount.getHashIV());
        LOGGER.info("[icashpay][doMicropay][Response][decrypt][resEncData]:  " + resEncData);
        JsonObject resEncDataObj = new Gson().fromJson(resEncData, JsonObject.class);

        //驗簽
        boolean bool = icashPayUtil.verifySign(pubkey, responseStr, resSignature);
        LOGGER.info("[icashpay][doMicropay][Response][verifySign][bool]:  " + bool);

        if (!bool) {
            LOGGER.error("[icashpay][doMicropay][Response][verifySign]: verify sign is invalid");
            throw new OtherAPIException("doMicropay verifySign is fail");
        }
        String statusCode = responseObject.get("StatusCode").getAsString();
        String statusMessage = responseObject.get("StatusMessage").getAsString();
        String paymentDate = resEncDataObj.get("PaymentDate").getAsString();

        IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();

        if ("0001".equals(statusCode) && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {

            String amount = resEncDataObj.get("Amount").getAsString();
            String transactionID = resEncDataObj.get("TransactionID").getAsString();
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetail.setSystemOrderId(transactionID);
            tradeDetail.setTxParams(resEncData);
            integratedMicropayResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
            integratedMicropayResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
            appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
        } else {
            if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                integratedMicropayResponseData.setSysOrderNo(SystemInstance.EMPTY_STRING);
                integratedMicropayResponseData.setTotalFee(SystemInstance.EMPTY_STRING);
            }
        }
        tradeDetail = tradeDetailService.save(tradeDetail);

        ResponseGeneralHeader responseHeader = new ResponseGeneralHeader();
        responseHeader.setStatusCode(tradeDetail.getStatus());
        responseHeader.setStatusDesc(statusMessage);
        responseHeader.setMethod(requestHeader.getMethod());
        responseHeader.setServiceType(requestHeader.getServiceType());
        responseHeader.setMchId(requestHeader.getMerchantId());
        responseHeader.setResponseTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));

        integratedMicropayResponseData.setStoreOrderNo(tradeDetail.getOrderId());
        integratedMicropayResponseData.setPlatformRsp(resEncData);

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        responseGeneralBody.setHeader(responseHeader);
        responseGeneralBody.setData(integratedMicropayResponseData);

        LOGGER.info("integrate response ready :" + new Gson().toJson(responseGeneralBody));
        return new Gson().toJson(responseGeneralBody);

    }

    private void queryMerchantOrder(RequestHeader requestHeader, JsonPrimitive data, Merchant merchant, PaymentAccount paymentAccount) throws Exception {
        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
        IcpQueryRequertEncData icpQueryEncData = new IcpQueryRequertEncData(paymentAccount, tradeDetail);

        String queryEncData = new Gson().toJson(icpQueryEncData);
        IcashPayUtil icashPayUtil = new IcashPayUtil();
        String encrypt = icashPayUtil.encrypt(queryEncData, paymentAccount.getHashKey(), paymentAccount.getHashIV());

        String Signaturekey = icashPayUtil.makeSign(encrypt, privateKey);
        Map<String, String> resultMap = this.sendPost(requestHeader.getServiceType(), encrypt, paymentAccount, Signaturekey, queryOrderUrl);
        String responseStr = resultMap.get("responseStr");
        String resSignature = resultMap.get("resSignature");

        //EncData
        JsonObject responseObject = new Gson().fromJson(responseStr, JsonObject.class);
        String resEncData = icashPayUtil.decrypt(responseObject.get("EncData").toString(), paymentAccount.getHashKey(), paymentAccount.getHashIV());
        LOGGER.info("[icashpay][SingleOrderQuery][Response][decrypt][resEncData]:  " + resEncData);
        JsonObject resEncDataObj = new Gson().fromJson(resEncData, JsonObject.class);

        //驗簽
        boolean bool = icashPayUtil.verifySign(pubkey, responseStr, resSignature);
        LOGGER.info("[icashpay][SingleOrderQuery][Response][verifySign][bool]:  " + bool);

        if (!bool) {
            LOGGER.error("[icashpay][SingleOrderQuery][Response][verifySign]: verify sign is invalid");
            throw new OtherAPIException("SingleOrderQuery verifySign is fail");
        }

        String rtnCode = responseObject.get("RtnCode").getAsString();
        //RtnCode "1" 成功
        //金額"TradeAmount"
        //TradeStatus 交易狀態回傳 1 是交易成功
        if ("1".equals(rtnCode) &&
                String.valueOf(tradeDetail.getPayment()).equals(resEncDataObj.get("TotalAmount").getAsString()) &&
                "1".equals(resEncDataObj.get("TradeStatus").getAsString())
        ) {
            tradeDetail.setSystemOrderId(resEncDataObj.get("TransactionID").getAsString());
            tradeDetail.setTxParams(resEncData);
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            LOGGER.info("[icashpay][REQUEST][SingleOrderQuery]: tradedetail  is success.");
        } else {
            if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            }
            LOGGER.error("[icashpay][SingleOrderQuery][Response][rtnCode]:  " + rtnCode);
            LOGGER.error("[icashpay][REQUEST][SingleOrderQuery]: save tradeDetail fail.");

        }
        tradeDetailService.save(tradeDetail);
    }

    private String doRefund(RequestHeader requestHeader, JsonPrimitive data, Merchant merchant, PaymentAccount paymentAccount) throws Exception {

        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
        IcpRefundEncData icpRefundEncData = new IcpRefundEncData(paymentAccount, tradeDetail);

        String refundReq = new Gson().toJson(icpRefundEncData);
        LOGGER.info("[REQUEST][icashpay][refund]: " + refundReq);
        IcashPayUtil icashPayUtil = new IcashPayUtil();

        String encrypt = icashPayUtil.encrypt(refundReq, paymentAccount.getHashKey(), paymentAccount.getHashIV());
        String Signaturekey = icashPayUtil.makeSign(encrypt, privateKey);
        Map<String, String> resultMap = this.sendPost(requestHeader.getServiceType(), encrypt, paymentAccount, Signaturekey, refundUrl);
        String responseStr = resultMap.get("responseStr");
        String resSignature = resultMap.get("resSignature");

        //EncData
        JsonObject responseObject = new Gson().fromJson(responseStr, JsonObject.class);
        String resEncData = icashPayUtil.decrypt(responseObject.get("EncData").toString(), paymentAccount.getHashKey(), paymentAccount.getHashIV());
        LOGGER.info("[icashpay][refund][Response][decrypt][resEncData]:  " + resEncData);
        JsonObject resEncDataObj = new Gson().fromJson(resEncData, JsonObject.class);
        LOGGER.info("[icashpay][refund][Response][decrypt][resEncDataObj]:  " + resEncDataObj.toString());

        //驗簽
        boolean bool = icashPayUtil.verifySign(pubkey, responseStr, resSignature);
        LOGGER.info("[icashpay][refund][Response][verifySign][bool]:  " + bool);

        if (!bool) {
            LOGGER.error("[icashpay][refund][Response][verifySign] verify sign is invalid");
            return null;
        }

        String rtnCode = responseObject.get("RtnCode").getAsString();
        String rtnMsg = responseObject.get("RtnMsg").getAsString();
        String rtnTime = resEncDataObj.get("Timestamp").getAsString();

        //判斷退款是否成功
        if ("1".equals(rtnCode)) {
            tradeDetail.setSystemRefundId(resEncDataObj.get("TransactionID").getAsString());
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
            tradeProcedureService.updateRefundDeatil(tradeDetail, new Gson().fromJson(data.getAsString(), JsonObject.class), SystemInstance.TRADE_REFUND_SUCCESS);
            tradeDetail = tradeDetailService.save(tradeDetail);
            LOGGER.info("[icashpay][refund][Response][status]:  trade refund success");
        } else if ("2032".equals(rtnCode)) {
            //交易已退款過了
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
            tradeProcedureService.updateRefundDeatil(tradeDetail, new Gson().fromJson(data.getAsString(), JsonObject.class), SystemInstance.TRADE_REFUND_FAIL);

            LOGGER.info("[icashpay][refund][Response][rtnCode]:  " + rtnCode);
            LOGGER.info("[icashpay][refund][Response][rtnMsg]:  " + rtnMsg);

            tradeDetail = tradeDetailService.save(tradeDetail);

            LOGGER.info("[icashpay][refund][Response][status]:  trade refund repeat");
        } else {
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
            tradeProcedureService.updateRefundDeatil(tradeDetail, new Gson().fromJson(data.getAsString(), JsonObject.class), SystemInstance.TRADE_REFUND_FAIL);
            LOGGER.error("[icashpay][refund][Response][rtnCode]:  " + rtnCode);
            LOGGER.info("[icashpay][refund][Response][rtnMsg]:  " + rtnMsg);

            tradeDetail = tradeDetailService.save(tradeDetail);

            LOGGER.error("[icashpay][refund][Response][status]:  trade refund fail");
        }

        ResponseGeneralHeader responseHeader = new ResponseGeneralHeader();
        responseHeader.setStatusCode(tradeDetail.getRefundStatus());
        responseHeader.setStatusDesc(rtnMsg);
        responseHeader.setMethod(requestHeader.getMethod());
        responseHeader.setServiceType(requestHeader.getServiceType());
        responseHeader.setMchId(requestHeader.getMerchantId());
        responseHeader.setResponseTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));

        IntegratedRefundResponseData responseData = new IntegratedRefundResponseData();
        responseData.setPlatformRsp(resEncData);
        responseData.setRefundedAt(rtnTime);//icp response data裡的時間
        responseData.setStoreOrderNo(tradeDetail.getOrderId());
        responseData.setStoreRefundNo(tradeDetail.getStoreRefundId());
        responseData.setSysOrderNo(tradeDetail.getSystemOrderId());
        responseData.setSysRefundNo(tradeDetail.getSystemRefundId());

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        responseGeneralBody.setHeader(responseHeader);
        responseGeneralBody.setData(responseData);

        return new Gson().toJson(responseGeneralBody);
    }

    @Override
    public String getPaymentUrl(String amount, String shortId) throws Exception {
        String result = null;
        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(shortId);
        if (Objects.nonNull(qrcodeParameter)) {

            Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());
            PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 58);

            String nowStr = LocalDateTime.now().format(DateTimeFormatter.ofPattern(SystemInstance.DATE_PATTERN));
            if (StringUtils.isNotBlank(qrcodeParameter.getTimeExpire()) && Long.valueOf(nowStr) > Long.valueOf(qrcodeParameter.getTimeExpire())) {
                throw new OtherAPIException("QrCode is invalid");
            }
            if (Objects.isNull(paymentAccount)) {
                throw new ServiceTypeNotSupportException("Exception: The store's account isn't opened.");
            }

            String storeOrderNo = "1".equals(qrcodeParameter.getCodeType()) ? qrcodeParameter.getStoreOrderNo() : orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
            IcpRequestEncData icpRequestEncData = new IcpRequestEncData();
            icpRequestEncData.setPlatformID(paymentAccount.getAccount());
            icpRequestEncData.setMerchantID(paymentAccount.getAccount());//特店業者編號
            icpRequestEncData.setMerchantTradeNo(storeOrderNo);//特店業者交易序號
            icpRequestEncData.setTradeTool("2");//交易模式
            DateTime dateTime = DateTime.now();
            icpRequestEncData.setMerchantTradeDate(dateTime.toString("yyyy/MM/dd HH:mm:ss"));//交易日期
            icpRequestEncData.setAmount(amount);//交易金額
            icpRequestEncData.setItemNo(storeOrderNo);//商品序號
            icpRequestEncData.setItemName(qrcodeParameter.getBody());//商品名稱
            icpRequestEncData.setQuantity("1");//商品數量
            icpRequestEncData.setRemark(qrcodeParameter.getBody());//備註
            icpRequestEncData.setReturnURL(String.format("%sallpaypass/api/IcashPay/notify/" + qrcodeParameter.getMchId(), API_URL));//回傳網址
            icpRequestEncData.setStoreName(paymentAccount.getAppId());
            icpRequestEncData.setItemAmt(amount);//一般交易金額

            TradeDetail tradeDetail = setOnlineTradeDetailRequest(icpRequestEncData, merchant, paymentAccount, qrcodeParameter);

            String request = new Gson().toJson(icpRequestEncData);
            LOGGER.info("[REQUEST][icashpay][OnLine]: " + request);

            result = createOrder(request, paymentAccount);

        } else if (null == qrcodeParameter) {
            LOGGER.error("[REQUEST][icashpay] qrcode not found");
            return null;
        }
        return result;
    }


    public String createOrder(String request, PaymentAccount paymentAccount) throws Exception {
        LOGGER.info("[sendingRequest][icashpay][createOrder]..");
        IcashPayUtil icashPayUtil = new IcashPayUtil();
        String encrypt = icashPayUtil.encrypt(request, paymentAccount.getHashKey(), paymentAccount.getHashIV());
        String Signaturekey = icashPayUtil.makeSign(encrypt, privateKey);
        Map<String, String> resultMap = this.sendPost("createOrder", encrypt, paymentAccount, Signaturekey, onlinepayUrl);
        String responseStr = resultMap.get("responseStr");
        String resSignature = resultMap.get("resSignature");

        //解析EncData
        JsonObject responseObject = new Gson().fromJson(responseStr, JsonObject.class);
        String resEncData = icashPayUtil.decrypt(responseObject.get("EncData").toString(), paymentAccount.getHashKey(), paymentAccount.getHashIV());
        LOGGER.info("[icashpay][createOrder][Response][decrypt][resEncData]:  " + resEncData);
        JsonObject resEncDataObj = new Gson().fromJson(resEncData, JsonObject.class);

        //驗簽
        boolean bool = icashPayUtil.verifySign(pubkey, responseStr, resSignature);
        LOGGER.info("[icashpay][createOrder][Response][verifySign][bool]:  " + bool);

        if (!bool) {
            LOGGER.error("[icashpay][createOrder][Response][verifySign]: verify sign is invalid");
            return null;
        }

        //判斷 "RtnCode" = 1
        String rtnCode = responseObject.get("RtnCode").toString();
        if (!"1".equals(rtnCode)) {
            LOGGER.error("[icashpay][createOrder][Response][rtnCode]:  " + rtnCode);
            return null;
        }

        //接收TradeToken然後呼叫APP
        String tradeToken = resEncDataObj.get("TradeToken").getAsString();
        LOGGER.info("[icashpay][createOrder][Response][decrypt][TradeToken]:  " + tradeToken);
        String toApp = "icashpay://www.icashpay.com.tw/ICP?Action=Mainaction&Event=ICPO002&Value=" + tradeToken + "&Valuetype=1&" +
                "redirectURL=" + String.format("%sallpaypass/api/IcashPay/backUrl", API_URL);
        LOGGER.info("[icashpay][createOrder][Response] ICP payment URL Scheme:  " + toApp);
        return toApp;
    }


    @Override
    public String notifly(String signature, String body, String mchId) throws Exception {

        PaymentAccount paymentAccount = paymentAccountService.getOne(mchId, 58);

        JsonObject bodyObject = new Gson().fromJson(body, JsonObject.class);
        String encData = bodyObject.get("EncData").getAsString();

        IcashPayUtil icashPayUtil = new IcashPayUtil();
        String decData = icashPayUtil.decrypt(encData, paymentAccount.getHashKey(), paymentAccount.getHashIV());
        LOGGER.info("[icashpay][notify][EncData]:  " + decData);
        IcpNotifyEncData notifyData = new Gson().fromJson(decData, IcpNotifyEncData.class);

        //驗簽

        boolean bool = icashPayUtil.verifySign(pubkey, encData, signature);
        LOGGER.info("[icashpay][notify][verifySign][bool]:  " + bool);

        if (!bool) {
            LOGGER.error("[icashpay][notify][Response][verifySign]: verify sign is invalid");
            return "0";
        }

        TradeDetail tradeDetail = tradeDetailService.getOne(notifyData.getMerchantTradeNo());
        if (Objects.isNull(tradeDetail)) {
            LOGGER.error("[icashpay][notify][tradeDetail]:  is not found");
            return "0";
        } else {
            tradeDetail.setMethod("15800");
            tradeDetail.setPaymentAccount(paymentAccount);
            tradeDetail.setTxParams(decData);
            tradeDetail.setSystemOrderId(notifyData.getTransactionID());
        }

        //"MerchantTradeNo" 訂單號
        //"TradeAmount" 金額
        // RtnCode 0建立成功, 其餘為失敗
        if (tradeDetail.getOrderId().equals(notifyData.getMerchantTradeNo()) &&
                tradeDetail.getPayment() == notifyData.getTradeAmount() &&
                0 == notifyData.getRtnCode()
        ) {
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetailService.save(tradeDetail);
            LOGGER.info("[icashpay][REQUEST][notify] : tradedetail  is success.");
            return "1";
        } else {
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            tradeDetailService.save(tradeDetail);
            LOGGER.error("[icashpay][REQUEST][notify] : save tradeDetail fail.");
            return "0";
        }
    }

    private TradeDetail setOnlineTradeDetailRequest(IcpRequestEncData icpRequestEncData, Merchant merchant, PaymentAccount paymentAccount, QrcodeParameter qrcodeParameter) throws Exception {
        LOGGER.info("enter trade detail request:" + new Gson().toJson(icpRequestEncData));
        TradeDetail tradeDetail = tradeDetailService.getOne(merchant.getAccountId(), icpRequestEncData.getMerchantTradeNo());
        LOGGER.info("tradeDetailService.getOne is null  OK  " + tradeDetail);
        if (Objects.isNull(tradeDetail)) {
            tradeDetail = new TradeDetail();
            tradeDetail.setAccountId(merchant.getAccountId());
            tradeDetail.setMethod("15800");
            tradeDetail.setPaymentAccount(paymentAccount);
            tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
            tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
            tradeDetail.setOrderId(icpRequestEncData.getMerchantTradeNo());
            tradeDetail.setDeviceRandomId(deviceService.getOne(merchant, "APP-001"));
            tradeDetail.setPayment(Long.parseLong(icpRequestEncData.getAmount()));
            tradeDetail.setServiceType("OLPay");
            tradeDetail.setQrcodeToken(qrcodeParameter.getShortId());
            tradeDetail.setDescription(qrcodeParameter.getBody());
            tradeDetail.setOriginalPrice(qrcodeParameter.getTotalFee());
            tradeDetail = tradeDetailService.save(tradeDetail);
            tradeProcedureService.captureOrderSave(tradeDetail);
        } else {
            throw new OrderAlreadyExistedException("tradeDetail is existed");
        }
        LOGGER.info("[icashpay]save online trade detail : " + new Gson().toJson(tradeDetail));
        return tradeDetail;
    }
}