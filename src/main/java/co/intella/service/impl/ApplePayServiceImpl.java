package co.intella.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Map;

import javax.annotation.Resource;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyAgreement;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.params.KDFParameters;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.util.encoders.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import co.intella.domain.ApplePayTokenData;
import co.intella.domain.PaymentRequest;
import co.intella.domain.PaymentRequestData;
import co.intella.domain.PaymentRequestHeader;
import co.intella.domain.PaymentResponse;
import co.intella.model.Merchant;
import co.intella.model.QrcodeParameter;
import co.intella.model.TradeDetail;
import co.intella.service.ApplePayService;
import co.intella.service.DeviceService;
import co.intella.service.MerchantService;
import co.intella.service.OrderService;
import co.intella.service.PaymentAccountService;
import co.intella.service.TradeDetailService;
import co.intella.service.TradeProcedureService;
import co.intella.utility.KDFConcatGenerator;
import co.intella.utility.SystemInstance;

/**
 * @author Miles
 */
@Service
public class ApplePayServiceImpl implements ApplePayService {

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private OrderService orderService;


    private final Logger LOGGER = LoggerFactory.getLogger(ApplePayServiceImpl.class);

    // Apple Pay uses an 0s for the IV
    private static final byte[] IV = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    // precompute prefix bytes for the 'other' parameter of the NIST contact KDF
    private static final byte[] KDF_OTHER_BYTES_PREFIX;

    static {
        try {
            KDF_OTHER_BYTES_PREFIX = ((char) 0x0D + "id-aes256-GCM" + "Apple").getBytes("ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public String getToken(String encryptToken, String merchant) {

        LOGGER.info("getToken() " + merchant);
        LOGGER.debug("encrypt token() " + encryptToken);
        try {
            if (Security.getProvider("BC") == null) {
                LOGGER.info("getToken() add provider BouncyCastleProvider");
                Security.addProvider(new BouncyCastleProvider());
            } else {
                LOGGER.info("getToken() not add provider BouncyCastleProvider");
                Security.addProvider(new BouncyCastleProvider());
            }

            // todo made key file as a parameter

//            File file = new ClassPathResource("key/" + "merchant_id.cer").getFile();
            File file = new File("/opt/config_scan2pay/applepay_cert/super.cer");
            // read json payment token
            Map paymentToken = new Gson().fromJson(encryptToken, Map.class);

            // data is the ciphertext
            byte[] data = Base64.decode((String) paymentToken.get("data"));

            // read the ephemeral public key. it's a PEM file without header/footer -- add it back to make our lives easy
            Map header = (Map) paymentToken.get("header");
            String ephemeralPubKeyStr = "-----BEGIN PUBLIC KEY-----\n" + header.get("ephemeralPublicKey") + "\n-----END PUBLIC KEY-----";
            PEMReader pemReaderPublic = new PEMReader(new StringReader(ephemeralPubKeyStr));
            ECPublicKey ephemeralPublicKey = (ECPublicKey) pemReaderPublic.readObject();

            // Apple assigns a merchant identifier and places it in an extension (OID 1.2.840.113635.100.6.32)
            final X509Certificate merchantCertificate = readDerEncodedX509Certificate(new FileInputStream(file));
            byte[] merchantIdentifier = extractMerchantIdentifier(merchantCertificate);

            // load the merchant EC private key
            // WARNING: this key should live permanently in an HSM in a production environment!
            // export it from e.g. mac's keychain
            ECPrivateKey merchantPrivateKey = loadPrivateKey(merchant);

            // now we have all the data we need -- decrypt per Apple Pay spec
            final byte[] plaintext = decrypt(data, merchantPrivateKey, ephemeralPublicKey, merchantIdentifier);

            return new String(plaintext, "ASCII");
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] getToken() " + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return e.toString();
        }

    }

    private byte[] decrypt(byte[] ciphertext, ECPrivateKey merchantPrivateKey, ECPublicKey ephemeralPublicKey, byte[] merchantIdentifier) throws InvalidKeyException, NoSuchProviderException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, BadPaddingException, IllegalBlockSizeException {
        // ECDH key agreement
        final KeyAgreement keyAgreement = KeyAgreement.getInstance("ECDH");
        keyAgreement.init(merchantPrivateKey);
        keyAgreement.doPhase(ephemeralPublicKey, true);
        final byte[] sharedSecret = keyAgreement.generateSecret();

        // NIST key derivation function w/ Apple Pay specific parameters
        byte[] partyV = merchantIdentifier;
        byte[] other = new byte[KDF_OTHER_BYTES_PREFIX.length + partyV.length];
        System.arraycopy(KDF_OTHER_BYTES_PREFIX, 0, other, 0, KDF_OTHER_BYTES_PREFIX.length);
        System.arraycopy(partyV, 0, other, KDF_OTHER_BYTES_PREFIX.length, partyV.length);

        final Digest digest = new SHA256Digest();
        KDFConcatGenerator kdfConcatGenerator = new KDFConcatGenerator(digest, other);
        kdfConcatGenerator.init(new KDFParameters(sharedSecret, null));
        byte[] aesKey = new byte[32];
        kdfConcatGenerator.generateBytes(aesKey, 0, aesKey.length);

        final Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding", "BC");
        final SecretKeySpec keySpec = new SecretKeySpec(aesKey, "AES");
        cipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(IV));
        return cipher.doFinal(ciphertext);
    }

    private ECPrivateKey loadPrivateKey(String merchant) throws KeyStoreException, NoSuchProviderException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException {

        // todo made key file as a parameter

//        File file = new ClassPathResource("key/" + "apple_pay_pw.p12").getFile();
        File file = new File("/opt/config_scan2pay/applepay_cert/superECCprivate.p12");

        KeyStore keystore = KeyStore.getInstance("PKCS12");
        keystore.load(new FileInputStream(file), "test".toCharArray());
        assert keystore.size() == 1 : "wrong number of entries in keychain";
        Enumeration<String> aliases = keystore.aliases();
        String alias = null;
        while (aliases.hasMoreElements()) {
            alias = aliases.nextElement();
        }

        LOGGER.info("loadPrivateKey() alias : " + alias);
        //Apple Pay Payment Processing:merchant.com.intella.test
        //Key a =  keystore.getKey(alias, new char[]="test");
        return (ECPrivateKey) keystore.getKey(alias, "test".toCharArray());
    }

    private byte[] extractMerchantIdentifier(X509Certificate merchantCertificate) throws UnsupportedEncodingException {
        byte[] merchantIdentifierTlv = merchantCertificate.getExtensionValue("1.2.840.113635.100.6.32");
        assert merchantIdentifierTlv.length == 68;
        byte[] merchantIdentifier = new byte[64];
        System.arraycopy(merchantIdentifierTlv, 4, merchantIdentifier, 0, 64);
        return hexStringToByteArray(new String(merchantIdentifier, "ASCII"));
    }

    private X509Certificate readDerEncodedX509Certificate(InputStream in) throws FileNotFoundException, CertificateException {
        CertificateFactory factory = CertificateFactory.getInstance("X.509");
        return (X509Certificate) factory.generateCertificate(in);
    }

    private byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    @Override
    public TradeDetail setApplepayTradeDetail(PaymentRequest paymentRequest, QrcodeParameter qrcodeParameter, String method) {

        LOGGER.info("[setApplepayTradeDetail]");

        PaymentRequestHeader requestHeader = paymentRequest.getHeader();
        PaymentRequestData paymentRequestData = paymentRequest.getData();

        TradeDetail tradeDetail = tradeDetailService.getOne(paymentRequestData.getStoreOrderNo());

        if (tradeDetail != null) {
            return null;
        } else {
            tradeDetail = new TradeDetail();
        }

        Merchant merchant = merchantService.getOne(requestHeader.getMchId());

        tradeDetail.setMethod(method);
        tradeDetail.setServiceType(requestHeader.getServiceType());
        tradeDetail.setAccountId(requestHeader.getMchId());
        tradeDetail.setCreateDate(requestHeader.getCreateTime());

        tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
        tradeDetail.setOrderId(paymentRequestData.getStoreOrderNo());
        tradeDetail.setDeviceRandomId(deviceService.getOne(merchant, "APP-001"));
        tradeDetail.setPaymentAccount(paymentAccountService.getOne(requestHeader.getMchId(), 12));
        Integer amount = paymentRequestData.getTotalFee();
        tradeDetail.setPayment(amount);
        tradeDetail.setDescription(paymentRequestData.getBody());

        if (qrcodeParameter.getOnSaleTotalFee() == amount) {
            tradeDetail.setOnSale(true);
        } else {
            tradeDetail.setOnSale(false);
        }

        tradeDetail.setOriginalPrice(qrcodeParameter.getTotalFee());
        tradeDetail.setQrcodeToken(qrcodeParameter.getShortId());
        tradeDetail = tradeDetailService.save(tradeDetail);
        tradeProcedureService.captureOrderSave(tradeDetail);

        return tradeDetail;
    }

    @Override
    public ApplePayTokenData decryptToken(String encryptToken, String merchant) {

        String decryptTokenData = getToken(encryptToken, merchant);
        LOGGER.info("[decryptToken] decryptTokenData =>" + decryptTokenData);
        ApplePayTokenData applePayTokenData = null;

        try {
            applePayTokenData = new Gson().fromJson(decryptTokenData, ApplePayTokenData.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }

        return applePayTokenData;
    }

    @Override
    public void updateApplePayTradeResult(TradeDetail tradeDetail, PaymentResponse paymentResponse) {
        LOGGER.info("[updateApplepayTradeDetail]");
        if (paymentResponse == null) {
            LOGGER.info("[paymentResponse is null]");
            return;
        }

        if (SystemInstance.TRADE_SUCCESS.equals(paymentResponse.getHeader().getStatusCode())) {
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
        } else {
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
        }
        tradeDetailService.save(tradeDetail);
        LOGGER.info("[updateApplepayTradeDetail][SAVED]");
    }
}
