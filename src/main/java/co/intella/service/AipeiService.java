package co.intella.service;

import co.intella.domain.aipei.AipeiPaymentRequestData;
import co.intella.domain.aipei.AipeiRequestData;
import co.intella.domain.line.LinePaymentRequestData;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import com.google.gson.JsonPrimitive;

/**
 * @author Alex
 */

public interface AipeiService {

    String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;

    String payment(AipeiPaymentRequestData request) throws Exception;

    String refundPayment(AipeiPaymentRequestData request) throws Exception;

    String query(AipeiPaymentRequestData request) throws Exception;

}
