package co.intella.service;

import java.util.Calendar;
import java.util.Map;

import co.intella.domain.CheckoutCtrlRequest;
import co.intella.model.RequestHeader;

public interface CheckoutService {
	
	public String checkout(String accountId , String ownerDeviceId, String batchNo , RequestHeader requestHeader, Calendar createDate, String flag);
	
	public String doSendFailEmail();

	public Map<String, Object> listByAccountId(CheckoutCtrlRequest request);
}
