package co.intella.service;

import co.intella.domain.ApplePayTokenData;
import co.intella.domain.PaymentRequest;
import co.intella.domain.PaymentResponse;
import co.intella.exception.InvokeAPIException;
import co.intella.exception.OtherAPIException;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import com.google.gson.JsonPrimitive;

public interface PaymentService {

    PaymentResponse doApplePayPayment(ApplePayTokenData applePayTokenData, PaymentRequest paymentRequest, String qrcodeId) throws Exception;

    String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;

    /**
     * 判別是否為新北一起pay專案，有的話在檢驗驗證參數是否測定後，打請款API
     *
     * @return
     * @throws OtherAPIException
     * @throws InvokeAPIException
     */
    boolean checkPaymentAccountIs17Pay(PaymentAccount paymentAccount);

    /**
     * 設定資料傳輸驗證碼
     *
     * @param paymentAccount
     * @return
     * @throws OtherAPIException
     * @throws InvokeAPIException
     */
    PaymentAccount configedPermitCode(PaymentAccount paymentAccount);

    /**
     * 非新北一起pay  要多打請款API，成功的話額外押上batchNo(當天日期)做判斷是否已經請款
     *
     * @param paymentAccount
     * @param tradeDetail
     * @param methodStr
     */
    void fubonECCapture(PaymentAccount paymentAccount, TradeDetail tradeDetail, String methodStr);

    /**
     * 富邦 apple pay 以及 富邦3D   自動補請款
     */
    void doECCapture();
}
