package co.intella.service;

import co.intella.domain.aliShangHai.AliShangHaiRequest;
import co.intella.model.*;
import com.google.gson.JsonPrimitive;

/**
 * @author Andy Lin
 */
public interface AlipayShangHaiService {

    String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;

    String preCreate(AliShangHaiRequest aliShangHaiRequest) throws Exception;

    String query(String aliShangHaiRequest) throws Exception;

    String cancel(String aliShangHaiRequest) throws Exception;

    String refund(String aliShangHaiRequest) throws Exception;

    String spotPay(String aliShangHaiRequest) throws Exception;

    String sendRequest(String request) throws Exception;

    AliShangHaiRequest prepareAliPayShangHaiReserveRequestData(Integer totalFee, String orderId, PaymentAccount paymentAccount) throws Exception;

    String createOrder(TradeDetail tradeDetail, QrcodeParameter qrcodeParameter, Integer totalFee, String storeOrderNoNew, PaymentAccount paymentAccount) throws Exception;
}
