package co.intella.service;

import co.intella.domain.pi.PiBasicRequestData;
import co.intella.domain.pi.PiOLRefundRequestData;
import co.intella.domain.pi.PiOLSingleOrderQueryRequestData;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.JsonPrimitive;

/**
 * @author Miles
 */
public interface PiService {
    String doRequest(PiBasicRequestData request, RequestHeader header, TradeDetail tradeDetail, String integrateMchId) throws JsonProcessingException;

    String doRequest(RequestHeader header, JsonPrimitive data) throws JsonProcessingException;

    String micropay(PiBasicRequestData request) throws JsonProcessingException;

    String refund(PiBasicRequestData request) throws JsonProcessingException;

    String refundOLPay(PiOLRefundRequestData request);

    String singleOrderQueryMicropay(PiBasicRequestData request) throws JsonProcessingException;

    String singleOrderQueryOLpay(PiOLSingleOrderQueryRequestData request);

    String reconciliation(String partnerId, String partnerKey, String date) throws Exception;
}
