package co.intella.service;

import co.intella.domain.contratStore.CustomizePageRequest;
import co.intella.model.CaptureOrderDetail;

import java.util.List;

/**
 * @author Andy Lin
 */
public interface CaptureOrderDetailService {

    List<CaptureOrderDetail> save(List<CaptureOrderDetail> list);

    CaptureOrderDetail save(CaptureOrderDetail captureDetail);

    CaptureOrderDetail getOne(String mchId);

    CaptureOrderDetail getOne(String mchId, String storeOrderNo);

    List<CaptureOrderDetail> listByMerchantIdAndDate(String mchId, String date);

    List<CaptureOrderDetail> list(String mchId, String from, String to, String status);

    List<CaptureOrderDetail> listByMerchantIdAndDate(String mchId, String from, String to, String type);

    List<CaptureOrderDetail> listUnSettleCaptureOrder(String mchId, String from, String to, String type);

    List<CaptureOrderDetail> listSettleCaptureOrder(String mchId, String from, String to, String type);

    List<CaptureOrderDetail> listAll();

    List<CaptureOrderDetail> pageList(CustomizePageRequest request);

    long pageCount(CustomizePageRequest request);
}
