package co.intella.service;

import co.intella.model.RequestHeader;
import org.springframework.http.ResponseEntity;

import javax.crypto.SecretKey;

public interface TradeDetailMultiService {
    ResponseEntity<String> doService(String body, SecretKey secretKey,  RequestHeader requestHeader) throws Exception;
}
