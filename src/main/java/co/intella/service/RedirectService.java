package co.intella.service;

import co.intella.model.*;
import co.intella.repository.QrType7Repository;
import co.intella.repository.QrType8Repository;
import co.intella.utility.JsonUtil;
import co.intella.utility.SystemInstance;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
public class RedirectService {

    @Resource
    private RedisService redisService;

    @Resource
    private TradeDetailOtherInfoService tradeDetailOtherInfoService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private LookupCodeService lookupCodeService;

    @Resource
    private QrType7Repository qrType7Repository;

    @Resource
    private QrType8Repository qrType8Repository;

    private final Logger LOGGER = LoggerFactory.getLogger(RedirectService.class);

    /**
     * 跳轉結果畫面 or CallBackUrl
     *
     * @param model
     * @return
     */
    public ModelAndView confirm(ModelAndView model) {
        Map map = model.getModelMap();
        String orderId = map.get("MerchantTradeNo").toString();
        String orderStatus = map.get("RtnCode").toString();
        String totalFee = map.get("TradeAmt").toString();
        if (orderStatus.startsWith("00")) {
            orderStatus = "0000";
        } else if (orderStatus.startsWith("99")) {
            orderStatus = "9999";
        }
        LOGGER.info("orderId + orderStatus + totalFee :" + orderId + orderStatus + totalFee);
        String redirectUrl = redisService.getRedis(orderId);
        if (redirectUrl != null) {
            model.clear();
            model.setViewName("redirect:" + redirectUrl + "?orderId=" + orderId + "&orderStatus=" + orderStatus + "&totalFee=" + totalFee);
        }


        TradeDetail tradeDetail = tradeDetailService.getOne(orderId);
        TradeDetailOtherInfo tradeDetailOtherInfo = tradeDetailOtherInfoService.getOneByUNHEXUuid(tradeDetail.getTradeDetailRandomId().toString());
        Merchant merchant = merchantService.getOne(tradeDetail.getAccountId());
        this.saveIntTbQrType7(tradeDetail, tradeDetailOtherInfo);
        long delay = this.checkCallBackDelay(tradeDetailOtherInfo, merchant);

        if (delay == 0 && StringUtils.isNotBlank(tradeDetailOtherInfo.getCallBackUrl())) {
            model.clear();
            model.setViewName("redirect:" + tradeDetailOtherInfo.getCallBackUrl());
        } else if (delay > 0) {
            model.addObject("callBackUrl", tradeDetailOtherInfo.getCallBackUrl());
            model.addObject("callBackDelay", delay);
        }

        if (Objects.nonNull(merchant) && StringUtils.isNotEmpty(merchant.getButtonText()) && StringUtils.isNotEmpty(merchant.getButtonURL())) {
            model.addObject("MerchantID", merchant.getName());
            model.addObject("buttonText", merchant.getButtonText());
            model.addObject("buttonURL", String.format(merchant.getButtonURL(), tradeDetail.getOrderId()));
        }

        if ("0000".equals(orderStatus)) {
            if (lookupCodeService.isIMotoAccount(merchant.getAccountId())) {
                IntTbLookupCode lookupCode = lookupCodeService.findOne("imoto", "invoiceUrl");
                // 台大停車場，交易成功直接跳轉去 發票頁面
                model.clear();
                model.setViewName("sendForm");
                model.addObject("url", lookupCode.getType1());

                Map<String, String> dataMap = new HashMap<>();
                dataMap.put("Identifier", merchant.getComId());
                dataMap.put("TransDateTime", LocalDateTime.now().toString("yyyy/MM/dd HH:mm:ss"));
                dataMap.put("TransAmount", Long.valueOf(tradeDetail.getPayment()).toString());
                dataMap.put("DeviceID", lookupCode.getType2());
                LOGGER.info("[IMoto] 發票 =>" + dataMap);
                model.addObject("dataJsonStr", JsonUtil.toJsonStr(dataMap));
            } else if (lookupCodeService.isIParkingAccount(merchant.getAccountId())) {
                IntTbLookupCode invoiceUrlLookupCode = lookupCodeService.findOne("iParking", "invoiceUrl");
                model.clear();
                model.setViewName("sendForm");
                model.addObject("url", invoiceUrlLookupCode.getType1());
                Map<String, String> tradeDetailOtherInfoMap = JsonUtil.parseJson(tradeDetailOtherInfo.getRequestDetail(), Map.class);

                Map<String, String> dataMap = new HashMap<>();
                String place = StringUtils.defaultIfBlank(tradeDetailOtherInfoMap.get("Place"), "");
                IntTbLookupCode placeLookupCode = lookupCodeService.findOne("iParking", place);

                dataMap.put("Identifier", Objects.nonNull(placeLookupCode) ? placeLookupCode.getType1() : merchant.getComId());
                dataMap.put("TransDateTime", LocalDateTime.now().toString("yyyy/MM/dd HH:mm:ss"));
                dataMap.put("TransAmount", Long.valueOf(tradeDetail.getPayment()).toString());
                dataMap.put("DeviceID", invoiceUrlLookupCode.getType2());
                dataMap.put("Email", StringUtils.defaultIfBlank(tradeDetailOtherInfoMap.get("Email"), ""));
                dataMap.put("CarrierID", StringUtils.defaultIfBlank(tradeDetailOtherInfoMap.get("CarrierID"), ""));
                dataMap.put("LocationID", place);
                dataMap.put("CarPlateNum",  StringUtils.defaultIfBlank(tradeDetailOtherInfoMap.get("LicensePlate"), ""));
                dataMap.put("OrderID", tradeDetail.getOrderId());

                String taxType = StringUtils.defaultIfBlank(tradeDetailOtherInfoMap.get("TaxType"), "");
                if (!"3".equals(taxType)) {
                    // 3 為免稅，免稅不給統編
                    dataMap.put("BuyerIdentifier", StringUtils.defaultIfBlank(tradeDetailOtherInfoMap.get("BuyerIdentifier"), ""));
                }
                dataMap.put("TaxType", taxType);
                LOGGER.info("[IParking] 發票 =>" + dataMap);
                model.addObject("dataJsonStr", JsonUtil.toJsonStr(dataMap));
            }
        }

        return model;
    }

    /**
     * 依據客人傳入 延遲，最多10秒，超過強制為3秒
     *
     * @param tradeDetailOtherInfo
     * @param merchant
     * @return
     */
    private long checkCallBackDelay(TradeDetailOtherInfo tradeDetailOtherInfo, Merchant merchant) {
        long delay = -1;
        if (Objects.nonNull(tradeDetailOtherInfo) && tradeDetailOtherInfo.getExeTime().intValue() == 0) {
            // 有特別的Detail
            Map<String, String> tradeDetailOtherInfoMap = JsonUtil.parseJson(tradeDetailOtherInfo.getRequestDetail(), Map.class);
            if (StringUtils.isEmpty(tradeDetailOtherInfo.getCallBackUrl()) && !tradeDetailOtherInfoMap.containsKey("Delay")) {
                delay = -1;
            } else if (StringUtils.isNotEmpty(merchant.getCallBackDelay())) {
                delay = Long.valueOf(merchant.getCallBackDelay());
            } else if (tradeDetailOtherInfoMap.containsKey("Delay")) {
                delay = Long.valueOf(tradeDetailOtherInfoMap.get("Delay"));
            } else {
                delay = 3;
            }
            delay = delay > 10 ? 3 : delay;     //超過十秒 強制為3秒
            tradeDetailOtherInfo.setExeTime(1L);
            tradeDetailOtherInfoService.save(tradeDetailOtherInfo);
        }
        return delay;
    }

    /**
     * qrCode type 7  當交易成功 qrType要額外新增IntTbQrType7
     *
     * @param tradeDetail
     * @param tradeDetailOtherInfo
     */
    private void saveIntTbQrType7(TradeDetail tradeDetail, TradeDetailOtherInfo tradeDetailOtherInfo) {
        if (Objects.nonNull(tradeDetailOtherInfo) && tradeDetailOtherInfo.getExeTime().intValue() == 0) {
            if (!SystemInstance.TYPE_MICROPAY.equals(tradeDetail.getServiceType()) && StringUtils.isNotEmpty(tradeDetail.getQrcodeToken())) {
                String requestDetail = tradeDetailOtherInfo.getRequestDetail();
                QrType7 qrType7 = JsonUtil.parseJson(requestDetail, QrType7.class);
                if (StringUtils.isNotEmpty(qrType7.getShortId())) {
                    QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(qrType7.getShortId());
                    if (Objects.nonNull(qrcodeParameter) && "7".equals(qrcodeParameter.getCodeType())) {
                        qrType7.setOrderId(tradeDetail.getOrderId());
                        qrType7.setShortId(tradeDetail.getQrcodeToken());
                        qrType7.setAccountId(tradeDetail.getAccountId());
                        qrType7Repository.save(qrType7);
                    } else if (Objects.nonNull(qrcodeParameter) && "8".equals(qrcodeParameter.getCodeType())) {
                        QrType8 qrType8 = JsonUtil.parseJson(requestDetail, QrType8.class);
                        qrType8.setOrderId(tradeDetail.getOrderId());
                        qrType8.setShortId(tradeDetail.getQrcodeToken());
                        qrType8.setAccountId(tradeDetail.getAccountId());
                        qrType8Repository.save(qrType8);
                    }
                }
            }
        }
    }
}
