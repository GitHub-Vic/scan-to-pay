package co.intella.service;

import co.intella.domain.jkos.JkosMicropayRequestData;
import co.intella.domain.jkos.JkosRefundRequestData;
import co.intella.model.Merchant;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import com.google.gson.JsonPrimitive;

/**
 * @author Alex
 */

public interface JkosService {

    String micropay(PaymentAccount paymentAccount, Merchant merchant, RequestHeader requestHeader, JsonPrimitive data) throws Exception;

    String refund(PaymentAccount paymentAccount, Merchant merchant, RequestHeader requestHeader, JsonPrimitive data) throws Exception;

    String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;

    void doCancel(TradeDetail tradeDetail) throws InterruptedException;

    String getPaymentUrl(String amount, String shortId, String agen) throws Exception;

}
