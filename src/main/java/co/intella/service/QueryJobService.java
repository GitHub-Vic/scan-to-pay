package co.intella.service;

import co.intella.domain.weixin.ScheduledQueryJob;

import java.util.List;

/**
 * @author Miles
 */
public interface QueryJobService {

    void addJob(String orderId, String merchantId, String paymentAccount, long bankId);

    List<ScheduledQueryJob> list();

    void doJobs();

}
