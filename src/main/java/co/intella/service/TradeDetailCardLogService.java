package co.intella.service;

import co.intella.model.TradeDetailCardLog;

public interface TradeDetailCardLogService {

    TradeDetailCardLog save(TradeDetailCardLog tradeDetailCardLog);
    
}
