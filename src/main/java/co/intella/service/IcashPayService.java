package co.intella.service;


import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import com.google.gson.JsonPrimitive;


public interface IcashPayService {
    String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;

    String getPaymentUrl(String amount, String shortId) throws Exception;

    String notifly(String signature, String body , String mchId) throws Exception;
}
