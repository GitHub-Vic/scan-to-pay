package co.intella.service;

import co.intella.model.PaymentMethod;

public interface PaymentMethodService {
	PaymentMethod getOne(String method);
}
