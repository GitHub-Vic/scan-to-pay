package co.intella.service;

import co.intella.model.MailInformation;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * @author Alex
 */
@Service
public class MailService {

    @Autowired
    private JavaMailSender mailSender;

    private TemplateEngine templateEngine;

    @Value("${mail.from}")
    private String MAIL_FROM;

    @Autowired
    public MailService(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    public String build(Context context, String templateName) {
        return templateEngine.process("mail/" + templateName, context);
    }

    public void send(MailInformation information, Context ctx, String templateName) throws MessagingException {

        MimeMessage message = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        String htmlContent = build(ctx, templateName);

        helper.setFrom(MAIL_FROM);
        helper.setTo(information.getMailTo());
        helper.setSubject(information.getSubject());
        helper.setText(htmlContent, true);

        mailSender.send(message);
    }
    public void send(MailInformation information, Context ctx) throws MessagingException {

        MimeMessage message = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(message, true);

        helper.setFrom(MAIL_FROM);
        helper.setTo(information.getMailTo());
        helper.setSubject(information.getSubject());
        try {
            helper.setText(new ObjectMapper().writeValueAsString(ctx));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        mailSender.send(message);
    }
}
