package co.intella.service;

import co.intella.domain.ApplePayTokenData;
import co.intella.domain.PaymentRequest;
import co.intella.domain.PaymentResponse;
import co.intella.model.QrcodeParameter;
import co.intella.model.TradeDetail;

/**
 * @author Miles
 */
public interface ApplePayService {

    String getToken(String encryptToken, String merchant);

    ApplePayTokenData decryptToken(String encryptToken, String merchant);
    
    TradeDetail setApplepayTradeDetail(PaymentRequest PaymentRequestData, QrcodeParameter qrcodeParameter, String method);

	void updateApplePayTradeResult(TradeDetail tradeDetail, PaymentResponse paymentResponse);
}
