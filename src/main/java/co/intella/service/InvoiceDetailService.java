package co.intella.service;

import co.intella.model.InvoiceDetail;

/**
 * @author Alex
 */

public interface InvoiceDetailService {

    InvoiceDetail save(InvoiceDetail invoiceDetail);

}
