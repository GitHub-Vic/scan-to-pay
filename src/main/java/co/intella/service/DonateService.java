package co.intella.service;

import co.intella.model.TradeDetail;
import com.google.gson.JsonObject;

import java.util.List;

public interface DonateService {

    List<JsonObject> getNotifyList(TradeDetail tradeDetail);
}
