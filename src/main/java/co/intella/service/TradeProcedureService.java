package co.intella.service;

import co.intella.domain.aipei.AipeiPaymentResponseData;
import co.intella.domain.aipei.AipeiPaymentResponseParaData;
import co.intella.domain.ali.AliMicropayResponseData;
import co.intella.domain.ali.AliRefundResponseData;
import co.intella.domain.ali.AliResponseBody;
import co.intella.domain.aliShangHai.*;
import co.intella.domain.allpay.AllpayMicropayResponseData;
import co.intella.domain.allpay.AllpayOLCreditRefundResponse;
import co.intella.domain.allpay.AllpayRefundResponseData;
import co.intella.domain.allpay.AllpaySingleQueryResponseData;
import co.intella.domain.ecpay.ECPayOrderResponse;
import co.intella.domain.focas.FOCASPayResponse;
import co.intella.domain.fubon.FuBonParamBean;
import co.intella.domain.gama.GamaCreateOrderResponseData;
import co.intella.domain.gama.GamaResponseData;
import co.intella.domain.integration.IntegratedInAppRequest;
import co.intella.domain.integration.IntegratedMicropayResponseData;
import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.jkos.JkosMicropayResponseData;
import co.intella.domain.jkos.JkosRefundResponseData;
import co.intella.domain.line.LineInfoData;
import co.intella.domain.line.LineQueryResponseData;
import co.intella.domain.line.LineResponseData;
import co.intella.domain.luckypay.LuckyPayQueryOrderResponseData;
import co.intella.domain.megatwpay.SingleOrderRes;
import co.intella.domain.megatwpay.TxnCancelRes;
import co.intella.domain.megatwpay.TxnPaymentRes;
import co.intella.domain.megaweixin.MegaWeiXinResponse;
import co.intella.domain.megaweixin.MegaWeiXinResponseData;
import co.intella.domain.megaweixin.MegaWeiXinResponseHeader;
import co.intella.domain.pi.PiServerOrderQueryResponse;
import co.intella.domain.pi.PiServerRefundResponse;
import co.intella.domain.tcbank.TCBCreditCardResponseData;
import co.intella.domain.tsbank.TSBOtherResponseData;
import co.intella.domain.tsgw.GwMerchantApiPayResponse;
import co.intella.domain.tsgw.GwMerchantApiQueryResponse;
import co.intella.domain.tsgw.GwMerchantApiRefundResponse;
import co.intella.domain.twpay.TwPayRequestData;
import co.intella.domain.weixin.WeixinSingleQueryResponseData;
import co.intella.domain.yuantaoffline.*;
import co.intella.exception.*;
import co.intella.model.*;
import co.intella.utility.FubonUtil;
import co.intella.utility.JsonUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import org.apache.commons.collections.CollectionUtils;
import org.assertj.core.util.Strings;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

/**
 * @author Miles
 */
@Service
@Transactional
public class TradeProcedureService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TradeProcedureService.class);

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private DeviceService deviceService;

    @Autowired
    private Environment env;

    @Resource
    private CaptureOrderDetailService captureOrderDetailService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private RefundDetailService refundDetailService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private OrderService orderService;

    @Autowired
    private MessageSource messageSource;

    @Resource
    private QrcodeService qrcodeService;

    private Locale locale = new Locale("zh_TW");

    /**
     * 這邊會把requestHeader.MerchantId換掉，我不知道為啥，我怕之前某個支付會有需要。我不敢改。所以請注意    by   Sol
     *
     * @param requestHeader
     * @param data
     * @return
     * @throws Exception
     */
    public TradeDetail setTradeDetailRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception {

        LOGGER.info("enter trade detail request:" + new Gson().toJson(requestHeader));

        TradeDetail tradeDetail = null;
        JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);

        //one of these would be null
        if (requestData.has("StoreOrderNo")) {
            tradeDetail = tradeDetailService.getOne(requestData.get("StoreOrderNo").getAsString());
        }

        if (!"InApp".equals(requestHeader.getServiceType())) {

            if (requestData.has("SysOrderNo") && tradeDetail == null) {
                tradeDetail = tradeDetailService.getOneBySysOrder(requestHeader.getMerchantId(), requestData.get("SysOrderNo").getAsString());
            }

        }

        if ("Cancel".equals(requestHeader.getServiceType())) {
            if (tradeDetail == null) {
                throw new OrderNotFoundException(SystemInstance.ORDER_NOT_FOUND);
            }

            if (tradeDetail.getRefundStatus() != null) {
                if (tradeDetail.getRefundStatus().equals(SystemInstance.TRADE_CANCEL_SUCCESS) || tradeDetail.getRefundStatus().equals(SystemInstance.TRADE_REFUND_SUCCESS)) {
                    throw new OrderAlreadyCanceledException("Already canceled");
                }
            }
            if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                LOGGER.info("tradeDetail not TRADE_SUCCESS \t" + tradeDetail.getOrderId() + "\t" + tradeDetail.getStatus());
                throw new OtherAPIException(messageSource.getMessage("response.7351", null, locale));
            }
            setRefundDetail(requestHeader, requestData, tradeDetail);

            tradeDetail.setRefundStatus("Canceling");
            if (requestData.has("StoreRefundNo")) {
                tradeDetail.setStoreRefundId(requestData.get("StoreRefundNo").getAsString());
            } else {
                tradeDetail.setStoreRefundId("R" + requestData.get("StoreOrderNo").getAsString());
            }

        } else if ("Refund".equals(requestHeader.getServiceType())) {
            if (tradeDetail == null) {
                throw new OrderNotFoundException(SystemInstance.ORDER_NOT_FOUND);
            }

            if (tradeDetail.getRefundStatus() != null) {
                if (tradeDetail.getRefundStatus().equals(SystemInstance.TRADE_CANCEL_SUCCESS) || tradeDetail.getRefundStatus().equals(SystemInstance.TRADE_REFUND_SUCCESS)) {
                    throw new OrderAlreadyRefundedException("Already refunded");
                }
            }

            if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                LOGGER.info("tradeDetail not TRADE_SUCCESS \t" + tradeDetail.getOrderId() + "\t" + tradeDetail.getStatus());
                throw new OtherAPIException(messageSource.getMessage("response.7351", null, locale));
            }

            //insert refund detail
            setRefundDetail(requestHeader, requestData, tradeDetail);

            tradeDetail.setRefundStatus("Refunding");
            tradeDetail.setStoreRefundId(requestData.get("StoreRefundNo").getAsString());

        } else if ("SingleOrderQuery".equals(requestHeader.getServiceType())) {
            if (tradeDetail == null) {
                throw new OrderNotFoundException(SystemInstance.ORDER_NOT_FOUND);
            }
            requestHeader.setMerchantId(tradeDetail.getPaymentAccount().getAccount());
            return tradeDetail;
        } else if (SystemInstance.TYPE_MICROPAY.equals(requestHeader.getServiceType()) || "OLPay".equals(requestHeader.getServiceType()) || "Payment".equals(requestHeader.getServiceType())) {
            if (tradeDetail != null) throw new OrderAlreadyExistedException("Trade exist");

            Merchant merchant = merchantService.getOne(requestHeader.getMerchantId());
            tradeDetail = new TradeDetail();

            long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
            tradeDetail.setAccountId(requestHeader.getMerchantId());
            tradeDetail.setMethod(requestHeader.getMethod());
            if (SystemInstance.TYPE_MICROPAY.equals(requestHeader.getServiceType()) && "10220".equals(requestHeader.getMethod())) {
                tradeDetail.setPaymentAccount(paymentAccountService.getOne(requestHeader.getMerchantId(), 11));
            } else {
                tradeDetail.setPaymentAccount(paymentAccountService.getOne(requestHeader.getMerchantId(), bankId));
            }
            //tradeDetail.setPaymentAccount(paymentAccountService.getOne(requestHeader.getMerchantId(), bankId));
            tradeDetail.setCreateDate(requestHeader.getCreateTime());//DateTime.now().toString("yyyyMMddHHmmss")
            if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
            }
            tradeDetail.setOrderId(requestData.get("StoreOrderNo").getAsString());

            if ("14200".equals(requestHeader.getMethod()) ||
                    "14300".equals(requestHeader.getMethod()) ||
                    "14400".equals(requestHeader.getMethod()) ||
                    "15000".equals(requestHeader.getMethod()) ||
                    "15600".equals(requestHeader.getMethod()) ||
                    "12000".equals(requestHeader.getMethod())) {
                String systemOrderId = orderService.createOrderIdBySequence(requestHeader.getMerchantId());
                tradeDetail.setSystemOrderId(systemOrderId);
            }

            tradeDetail.setDeviceRandomId(deviceService.getOne(merchant, requestData.get("DeviceInfo").getAsString()));
            tradeDetail.setPayment(Long.parseLong(requestData.get("TotalFee").getAsString()));

            tradeDetail.setDescription(requestData.has("Body") ? requestData.get("Body").getAsString() : null);
            if (requestData.has("AuthCode")) {
                tradeDetail.setBarcode(requestData.get("AuthCode").getAsString());
            }
            if (requestData.has("Cashier")) {
                tradeDetail.setCashier(requestData.get("Cashier").getAsString());
            }
            //union
            if (requestData.has("CardId") && requestData.get("CardId").getAsString().startsWith("62")) {
                tradeDetail.setType(1);
            }

            tradeDetail.setServiceType(requestHeader.getServiceType());

            if (requestData.has("DeviceOS")) {
                tradeDetail.setDeviceOS(requestData.get("DeviceOS").getAsString());
            }

            StringBuilder storeInfo = new StringBuilder();
            if (requestData.has("StoreInfo")) {
                storeInfo.append(requestData.get("StoreInfo").getAsString());
            }
            storeInfo.append("|");
            if (requestData.has("StoreType")) {
                storeInfo.append(requestData.get("StoreType").getAsString());
            }
            storeInfo.append("|");
            if (requestData.has("StoreName")) {
                storeInfo.append(requestData.get("StoreName").getAsString());
            }
            if (requestData.has("token")) {
                tradeDetail.setQrcodeToken(requestData.get("token").getAsString());
                QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(tradeDetail.getQrcodeToken());
                if (Objects.nonNull(qrcodeParameter)) {
                    tradeDetail.setDetail(qrcodeParameter.getDetail());
                }
            }
            if (requestData.has("Detail")) {
                tradeDetail.setDetail(requestData.get("Detail").getAsString());
            }
            tradeDetail.setStoreInfo(storeInfo.toString());
            captureOrderSave(tradeDetail);

        } else if ("InApp".equals(requestHeader.getServiceType())) {

            if (tradeDetail != null) throw new OrderAlreadyExistedException("Trade exist");

            tradeDetail = new TradeDetail();
            IntegratedInAppRequest integratedInAppRequest = new Gson().fromJson(data.getAsString(), IntegratedInAppRequest.class);
            long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
            Merchant merchant = merchantService.getOne(requestHeader.getMerchantId());

            tradeDetail.setMethod(requestHeader.getMethod());
            tradeDetail.setUserId(integratedInAppRequest.getUserId());
            tradeDetail.setOrderId(integratedInAppRequest.getStoreOrderNo());
            tradeDetail.setServiceType(requestHeader.getServiceType());
            tradeDetail.setAccountId(requestHeader.getMerchantId());
            tradeDetail.setCreateDate(requestHeader.getCreateTime());
            tradeDetail.setPayment(Long.parseLong(integratedInAppRequest.getTotalFee()));
            tradeDetail.setMchName(integratedInAppRequest.getMchName());
            tradeDetail.setMchKey(integratedInAppRequest.getMchKey());
            tradeDetail.setDescription(integratedInAppRequest.getBody());
            tradeDetail.setPaymentAccount(paymentAccountService.getOne(requestHeader.getMerchantId(), bankId));
            tradeDetail.setDeviceRandomId(deviceService.getOne(merchant, "APP-001"));
            if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
            }
            if (requestData.has("DeviceOS")) {
                tradeDetail.setDeviceOS(requestData.get("DeviceOS").getAsString());
            }

            if (requestData.has("Detail")) {
                tradeDetail.setDetail(requestData.get("Detail").getAsString());
            }

            StringBuilder storeInfo = new StringBuilder();
            if (requestData.has("StoreInfo")) {
                storeInfo.append(requestData.get("StoreInfo").getAsString());
            }
            storeInfo.append("|");
            if (requestData.has("StoreType")) {
                storeInfo.append(requestData.get("StoreType").getAsString());

            }
            storeInfo.append("|");
            if (requestData.has("StoreName")) {
                storeInfo.append(requestData.get("StoreName").getAsString());

            }

            tradeDetail.setStoreInfo(storeInfo.toString());
            captureOrderSave(tradeDetail);


        } else if ("Charge".equals(requestHeader.getServiceType())) {
            if (tradeDetail == null) throw new OrderNotFoundException(SystemInstance.ORDER_NOT_FOUND);
        } else if ("Confirm".equals(requestHeader.getServiceType())) {
            if (tradeDetail == null) throw new OrderNotFoundException(SystemInstance.ORDER_NOT_FOUND);
            return tradeDetail;
        }

        requestHeader.setMerchantId(tradeDetail.getPaymentAccount().getAccount());

        LOGGER.info("save trade detail : " + new ObjectMapper().writeValueAsString(tradeDetail));

        tradeDetail = tradeDetailService.save(tradeDetail);
//        tradeDetail.getTradeDetailRandomId();
        return tradeDetail;
    }

    private void setRefundDetail(RequestHeader requestHeader, JsonObject requestData, TradeDetail tradeDetail) {
        LOGGER.info("[setRefundDetail]");

        RefundDetail refundDetail = new RefundDetail();
        Merchant merchant = merchantService.getOne(requestHeader.getMerchantId());

        refundDetail.setTradeDetailRandomId(tradeDetail);
        refundDetail.setCreateTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        refundDetail.setAccountId(tradeDetail.getAccountId());
        refundDetail.setAmount(tradeDetail.getPayment());
        refundDetail.setCashier(requestData.has("Cashier") ? requestData.get("Cashier").getAsString() : null);
        if (requestData.has("DeviceInfo") && deviceService.getOne(merchant, requestData.get("DeviceInfo").getAsString()) != null) {
            refundDetail.setDeviceRandomId(deviceService.getOne(merchant, requestData.get("DeviceInfo").getAsString()));
        } else {
            refundDetail.setDeviceRandomId(null);
        }
        refundDetail.setOrderId(tradeDetail.getOrderId());
        refundDetail.setStatus("Refunding");
        if (requestData.has("StoreRefundNo")) {
            refundDetail.setStoreRefundId(requestData.get("StoreRefundNo").getAsString());
        } else {
            refundDetail.setStoreRefundId("R" + requestData.get("StoreOrderNo").getAsString());
        }
        requestData.addProperty("StoreRefundNo", String.valueOf(refundDetail.getStoreRefundId()));
        refundDetail.setReason(requestData.has("RefundedMsg") ? requestData.get("RefundedMsg").getAsString() : null);
        refundDetail.setMethod(tradeDetail.getMethod());
        refundDetailService.save(refundDetail);
        LOGGER.info("[setRefundDetail][End]");

    }


    public void setTradeDetailResponse(String mchId, RequestHeader requestHeader, String platformResponse, JsonPrimitive data, TradeDetail tradeDetail) throws OrderNotFoundException, OtherAPIException {

        JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
        updateTradeDetailByServiceType(mchId, requestHeader, requestData, platformResponse, tradeDetail);
    }

    public void setTradeDetailResponse(String mchId, RequestHeader requestHeader, String platformResponse, JsonPrimitive data) throws Exception {

        LOGGER.info("enter trade detail response:" + platformResponse);

        TradeDetail tradeDetail = null;
        JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);

        if (requestData.has("StoreOrderNo")) {
            tradeDetail = tradeDetailService.getOne(mchId, requestData.get("StoreOrderNo").getAsString());
        } else if (requestData.has("orderNo")) {
            tradeDetail = tradeDetailService.getOne(mchId, requestData.get("orderNo").getAsString());
        } else if (requestData.has("SysOrderNo")) {
            tradeDetail = tradeDetailService.getOneBySysOrder(mchId, requestData.get("SysOrderNo").getAsString());
        } else if (requestData.has("store_order_no")) {
            tradeDetail = tradeDetailService.getOne(mchId, requestData.get("store_order_no").getAsString());
        }

        updateTradeDetailByServiceType(mchId, requestHeader, requestData, platformResponse, tradeDetail);
    }

    private void updateTradeDetailByServiceType(String mchId, RequestHeader requestHeader, JsonObject requestData,
                                                String platformResponse, TradeDetail tradeDetail) throws OrderNotFoundException, OtherAPIException {

        if (tradeDetail == null) {
            throw new OrderNotFoundException(SystemInstance.ORDER_NOT_FOUND);
        } else {
            tradeDetail.setMethod(requestHeader.getMethod());
        }
        LOGGER.info("RefunderviceType : " + requestHeader.getServiceType());
        if ("Refund".equals(requestHeader.getServiceType())) {
            if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                LOGGER.info("tradeDetail not TRADE_SUCCESS \t" + tradeDetail.getOrderId() + "\t" + tradeDetail.getStatus());
                throw new OtherAPIException(messageSource.getMessage("response.7351", null, locale));
            }
            updateRefundResponse(requestHeader, requestData, platformResponse, tradeDetail);
        } else if ("Cancel".equals(requestHeader.getServiceType())) {
            if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                LOGGER.info("tradeDetail not TRADE_SUCCESS \t" + tradeDetail.getOrderId() + "\t" + tradeDetail.getStatus());
                throw new OtherAPIException(messageSource.getMessage("response.7351", null, locale));
            }
            updateCancelResponse(requestHeader, requestData, platformResponse, tradeDetail);
        } else if ("SingleOrderQuery".equals(requestHeader.getServiceType())) {

            updateSingleOrderQueryResponse(mchId, requestHeader, requestData, platformResponse, tradeDetail);
        } else if (SystemInstance.TYPE_MICROPAY.equals(requestHeader.getServiceType())
                || SystemInstance.TYPE_OLPAY.equals(requestHeader.getServiceType())
                || SystemInstance.TYPE_PAYMENT.equals(requestHeader.getServiceType())) {

            updateTradeResponse(mchId, requestHeader, requestData, platformResponse, tradeDetail);
        }

    }

    private void updateTradeResponse(String mchId, RequestHeader requestHeader, JsonObject requestData, String platformResponse, TradeDetail tradeDetail) {
        if ("10110".equals(requestHeader.getMethod())) {
            ResponseGeneralBody responseGeneralBody = new Gson().fromJson(platformResponse, ResponseGeneralBody.class);
            JsonObject jsonObject = new JsonParser().parse(platformResponse).getAsJsonObject();
            JsonObject responseGeneraldata = jsonObject.getAsJsonObject("Data");

            if (responseGeneralBody.getHeader().getStatusCode().equals("0000") && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                tradeDetail.setSystemOrderId(new JsonParser().parse(responseGeneraldata.getAsJsonPrimitive("DataValue").getAsString()).getAsJsonObject().get("SYS_ORDER_NO").getAsString());
                tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
//                captureOrderSave(tradeDetail);
                appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
            } else if (responseGeneralBody.getHeader().getStatusCode().equals("9997")) {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
                }
            } else {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }
        } else if ("12710".equals(requestHeader.getMethod())) {
            JsonObject jsonObject = new JsonParser().parse(platformResponse).getAsJsonObject();
            if ("0000".equals(jsonObject.get("status").getAsString()) && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                tradeDetail.setSystemOrderId(jsonObject.get("thirdPartyOrderNo").getAsString());
                tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
            } else if ("9997".equals(jsonObject.get("status").getAsString())) {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
                }
            } else {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }
        } else if ("14010".equals(requestHeader.getMethod())) {
            JsonObject jsonObject = new JsonParser().parse(platformResponse).getAsJsonObject();
            JsonObject jsonHeader = jsonObject.get("Header").getAsJsonObject();
            JsonObject jsonData = jsonObject.get("Data").getAsJsonObject();
            String status = jsonHeader.has("StatusCode") ? jsonHeader.get("StatusCode").getAsString() : "";
            String dataValue = jsonData.has("DataValue") ? jsonData.get("DataValue").getAsString() : "";

            String deviceInfo = requestData.has("DeviceInfo") ? requestData.get("DeviceInfo").getAsString() : "";
            tradeDetail.setTxParams(deviceInfo);
            if ("0000".equals(status) && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                tradeDetail.setSystemOrderId(dataValue.replaceAll(".*\"SYS_ORDER_NO\":\"(\\w*)\".*", "$1"));
                tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
            } else if ("9997".equals(status)) {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
                }
            } else {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }
        } else if ("10220".equals(requestHeader.getMethod())) {
            AliResponseBody aliResponseBody = new Gson().fromJson(platformResponse, AliResponseBody.class);
            if (aliResponseBody.getStatus().equals("S") && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                AliMicropayResponseData aliMicropayResponseData = new Gson().fromJson(aliResponseBody.getSecureData(), AliMicropayResponseData.class);
                tradeDetail.setSystemOrderId(aliMicropayResponseData.getBankOrderNo());
                tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
//                captureOrderSave(tradeDetail);
                appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
            } else {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }
        } else if ("10300".equals(requestHeader.getMethod())) {
            ResponseGeneralBody responseGeneralBody = new Gson().fromJson(platformResponse, ResponseGeneralBody.class);
            JsonObject jsonObject = new JsonParser().parse(platformResponse).getAsJsonObject();
            JsonObject responseGeneraldata = jsonObject.getAsJsonObject("Data");

            IntegratedMicropayResponseData integratedMicropayResponseData = new Gson().fromJson(responseGeneraldata.toString(), IntegratedMicropayResponseData.class);
            if (responseGeneralBody.getHeader().getStatusCode().equals("0000")) {
                tradeDetail.setSystemOrderId(integratedMicropayResponseData.getSysOrderNo());
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                }
//                captureOrderSave(tradeDetail);
            } else {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }
        } else if ("20400".equals(requestHeader.getMethod())) {
            TCBCreditCardResponseData tcbCreditCardResponseData = new Gson().fromJson(platformResponse, TCBCreditCardResponseData.class);
            tradeDetail.setSystemOrderId(tcbCreditCardResponseData.getOrderId());


            if (Strings.isNullOrEmpty(tcbCreditCardResponseData.getApproveCode())) {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
                }
            } else {
                // todo
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                }

            }

//            captureOrderSave(tradeDetail);

        } else if ("10500".equals(requestHeader.getMethod())) {
            AllpayMicropayResponseData allpayMicropayResponseData = new Gson().fromJson(platformResponse, AllpayMicropayResponseData.class);
            if (allpayMicropayResponseData.getRtnCode() != null) {
                if (allpayMicropayResponseData.getRtnCode().equals("1") && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setSystemOrderId(allpayMicropayResponseData.getAllpayTradeNo());
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    //                captureOrderSave(tradeDetail);
                    appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                } else if (allpayMicropayResponseData.getRtnCode().equals("130")) {
                    tradeDetail.setSystemOrderId(allpayMicropayResponseData.getAllpayTradeNo());
                    if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                        tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
                    }
                } else {
                    if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                        tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                    }
                }
            } else {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }

        } else if ("10900".equals(requestHeader.getMethod())) {
            JkosMicropayResponseData jkosMicropayResponseData = new Gson().fromJson(platformResponse, JkosMicropayResponseData.class);
            if ("000".equals(jkosMicropayResponseData.getStatusCode())) {
                tradeDetail.setSystemOrderId(jkosMicropayResponseData.getTradeNo());
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    tradeDetail.setPlatformPaidDate(jkosMicropayResponseData.getTradeTime());
                    appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                }
//                captureOrderSave(tradeDetail);

            } else {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }
        } else if ("21100".equals(requestHeader.getMethod())) {
            ECPayOrderResponse ecPayOrderResponse = new Gson().fromJson(platformResponse, ECPayOrderResponse.class);
            if (ecPayOrderResponse.getReturnCode() == 1) {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(ecPayOrderResponse.getReturnMessage());
                    tradeDetail.setTradeToken(ecPayOrderResponse.getSpToken());
                    appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                }
//                captureOrderSave(tradeDetail);

            } else {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }
        } else if ("11300".equals(requestHeader.getMethod())) {
            GamaCreateOrderResponseData gamaCreateOrderResponseData = new Gson().fromJson(platformResponse, GamaCreateOrderResponseData.class);
            if (gamaCreateOrderResponseData.getResultCode().equals("0000")) {
                if (gamaCreateOrderResponseData.getStatusCode().equals("100") && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    tradeDetail.setSystemOrderId(gamaCreateOrderResponseData.getTransactionId());
                    tradeDetail.setPlatformPaidDate(gamaCreateOrderResponseData.getTransDate());
                    appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                } else if (gamaCreateOrderResponseData.getStatusCode().equals("1")) {
                    if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                        tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
                    }
                } else {
                    if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                        tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                    }
                }
            } else {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }
        } else if ("11400".equals(requestHeader.getMethod())) {
            AipeiPaymentResponseData aipeiPaymentResponseData = new Gson().fromJson(platformResponse, AipeiPaymentResponseData.class);
            if (aipeiPaymentResponseData.getErrorCode().equals("") && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                AipeiPaymentResponseParaData aipeiPaymentResponseParaData = new Gson().fromJson(aipeiPaymentResponseData.getData(), AipeiPaymentResponseParaData.class);
                tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                tradeDetail.setSystemOrderId(aipeiPaymentResponseParaData.getApTransId());
                tradeDetail.setPlatformPaidDate(aipeiPaymentResponseParaData.getApPayTime());
                appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
            } else {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }
        } else if ("11500".equals(requestHeader.getMethod())) {
            LineResponseData lineResponseData = new Gson().fromJson(platformResponse, LineResponseData.class);
            LineInfoData lineInfoData = lineResponseData.getInfo();
            if (Objects.nonNull(lineInfoData) && Objects.nonNull(lineInfoData.getPayInfos())) {
                tradeDetail.setTxParams(new Gson().toJson(lineInfoData.getPayInfos()));
            }
            if (lineResponseData.getReturnCode().equals("0000") && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                tradeDetail.setSystemOrderId(lineResponseData.getInfo().getTransactionId().toString());
                tradeDetail.setPlatformPaidDate(DateTime.parse(lineResponseData.getInfo().getTransactionDate()).plusHours(8).toString(SystemInstance.DATE_PATTERN));
                appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
            } else {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }
        } else if ("14200".equals(requestHeader.getMethod()) || "15000".equals(requestHeader.getMethod()) || "14300".equals(requestHeader.getMethod())
                || "14400".equals(requestHeader.getMethod()) || "12000".equals(requestHeader.getMethod()) || "15600".equals(requestHeader.getMethod())) {
            TwPayRequestData responseData = new Gson().fromJson(platformResponse, TwPayRequestData.class);

            if ("0000".equals(responseData.getResponseCode())) {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    tradeDetail.setSystemOrderId(responseData.getOrderNumber());
                    tradeDetail.setPlatformPaidDate(responseData.getSystemDateTime());
                    appNotificationService.notifyApp(tradeDetail, merchantService.getOne(mchId));
                }

            } else {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }
        } else if ("11100".equals(requestHeader.getMethod())) {
            YuanTaOfflineResponse yuanTaOfflineResponse = new Gson().fromJson(platformResponse, YuanTaOfflineResponse.class);
            if ("S".equals(yuanTaOfflineResponse.getStatus()) && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                YuanTaOfflinePaymentResponseDataObject yuanTaOfflinePaymentResponseDataObject = new Gson()
                        .fromJson(yuanTaOfflineResponse.getSecureData(),
                                YuanTaOfflinePaymentResponseDataObject.class);
                tradeDetail.setSystemOrderId(yuanTaOfflinePaymentResponseDataObject.getBankOrderNo());
                tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
//                captureOrderSave(tradeDetail);
                appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
            }
        } else if ("13500".equals(requestHeader.getMethod())) {
            TxnPaymentRes txnPaymentRes = new Gson().fromJson(platformResponse, TxnPaymentRes.class);
            if ("00".equals(txnPaymentRes.getTxnPaymentHeaderRes().getsCode()) && "00".equals(txnPaymentRes.getTxnPaymentDataRes().getTxnResult())) {
                tradeDetail.setSystemOrderId(txnPaymentRes.getTxnPaymentDataRes().getTxnSeqNo());
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    tradeDetail.setSystemOrderId(txnPaymentRes.getTxnPaymentDataRes().getTxnSeqNo());
                    tradeDetail.setPlatformPaidDate(txnPaymentRes.getTxnPaymentDataRes().getTxnTime());
                    appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                }
//                appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
            } else {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }
        } else if ("13320".equals(requestHeader.getMethod()) || "13410".equals(requestHeader.getMethod())) {
            GwMerchantApiPayResponse responseData = new Gson().fromJson(platformResponse, GwMerchantApiPayResponse.class);

            if ("000".equals(responseData.getReturncode())) {
                tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                tradeDetail.setPlatformPaidDate(responseData.getTimestamp());
                appNotificationService.notifyApp(tradeDetail, merchantService.getOne(mchId));
            } else {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }
        } else if ("12520".equals(requestHeader.getMethod())) {
            JsonObject jo = new Gson().fromJson(platformResponse, JsonObject.class);
            AliShangHaiMicropayResponse aliShangHaiMicropayResponse = new Gson().fromJson(jo.get("response"), AliShangHaiMicropayResponse.class);
            AliShangHaiResponse aliShangHaiResponse = new Gson().fromJson(platformResponse, AliShangHaiResponse.class);

            if ("SUCCESS".equals(aliShangHaiMicropayResponse.getResultCode())) {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    tradeDetail.setSystemOrderId(aliShangHaiMicropayResponse.getOutTradeNo());
                    tradeDetail.setPlatformPaidDate(aliShangHaiResponse.getResponseTime());
                    appNotificationService.notifyApp(tradeDetail, merchantService.getOne(mchId));
                }

            } else {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
            }
        } else if ("23800".equals(requestHeader.getMethod())) {
            FuBonParamBean fuBonParamRes = new Gson().fromJson(platformResponse, FuBonParamBean.class);

            if (FubonUtil.isPaymentApproved(fuBonParamRes)) {
                tradeDetail.setSystemOrderId(fuBonParamRes.getOrderID());
                tradeDetail.setTxParams(fuBonParamRes.getApproveCode().trim());
                tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                appNotificationService.notifyApp(tradeDetail, merchantService.getOne(mchId));
            }
        }

        setTradeDetailStatusByGeneralResponse(tradeDetail, platformResponse);
        tradeDetailService.save(tradeDetail);
    }

    private void setTradeDetailStatusByGeneralResponse(TradeDetail tradeDetail, String platformResponse) {

        try {

            JsonObject jsonObject = new JsonParser().parse(platformResponse).getAsJsonObject();
            JsonObject responseGeneralHeader = jsonObject.getAsJsonObject("Header");
            if (responseGeneralHeader == null || responseGeneralHeader.get("StatusCode") == null) return;

            if ("0000".equals(responseGeneralHeader.get("StatusCode").getAsString())) {
                if (!"20800".equals(tradeDetail.getMethod())) {
                    if ("22600".equals(tradeDetail.getMethod())) {
                        String systemOrderId = tradeDetail.getOrderId().length() > 19 ? tradeDetail.getOrderId().substring(tradeDetail.getOrderId().length() - 19) : tradeDetail.getOrderId();
                        tradeDetail.setSystemOrderId(systemOrderId);
                    }
                    if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                        tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                        appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                    }
                }
            } else {
                tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("setTradeDetailResponseCode fail, status is not modified");
        }

    }

    private void updateSingleOrderQueryResponse(String mchId, RequestHeader requestHeader, JsonObject requestData, String platformResponse, TradeDetail tradeDetail) {
        LOGGER.info("[updateSingleOrderQueryResponse]");
        if ("10110".equals(requestHeader.getMethod())) {
            JsonObject jsonObject = new JsonParser().parse(platformResponse).getAsJsonObject();
            JsonObject responseGeneraldata = jsonObject.getAsJsonObject("Data");

            JsonObject responseGeneralHeader = jsonObject.getAsJsonObject("Header");
            if (responseGeneralHeader.get("StatusCode").getAsString().equals("0000")) {

                if (responseGeneraldata.has("DataValue")) {

                    WeixinSingleQueryResponseData weixinSingleQueryResponseData = new Gson().fromJson(responseGeneraldata.get("DataValue").getAsString(), WeixinSingleQueryResponseData.class);
                    if (weixinSingleQueryResponseData.getSysStatus().equals("1")) {

                        LOGGER.info("weixin query success");
                        tradeDetail.setSystemOrderId(weixinSingleQueryResponseData.getSysOrderNo());
                        //20190606 edit: Sol  查詢不推播
//                        if (!tradeDetail.getStatus().equals(SystemInstance.TRADE_SUCCESS)) {
//                            appNotificationService.notifyApp(tradeDetail, merchantService.getOne(mchId));
//                        }
                        tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);

//                        captureOrderSave(tradeDetail);


                    } else if (weixinSingleQueryResponseData.getSysStatus().equals("4")) {
                        tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                        tradeDetail.setRefundStatus("Refund success");
//                        captureOrderSave(tradeDetail);

                    } else if (weixinSingleQueryResponseData.getSysStatus().equals("2") || weixinSingleQueryResponseData.getSysStatus().equals("15")) {
                        if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                        }
                    }
                    tradeDetailService.save(tradeDetail);
                }
            } else if (responseGeneralHeader.get("StatusCode").getAsString().equals("7002")) {
                DateTime qDateTime = DateTime.parse(responseGeneralHeader.get("ResponseTime").getAsString(), DateTimeFormat.forPattern("yyyy-MM-ddHH:mm:ss"));

                DateTime cDateTime = DateTime.parse(tradeDetail.getCreateDate(), DateTimeFormat.forPattern("yyyyMMddHHmmss"));

                if (qDateTime.getMillis() > cDateTime.plusMinutes(15).getMillis()) {
                    if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                        tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                    }
                    tradeDetailService.save(tradeDetail);
                }
            }

        } else if ("12710".equals(requestHeader.getMethod())) {

            JsonObject dataJson = new Gson().fromJson(platformResponse, JsonObject.class);


            if ("0000".equals(dataJson.get("status").getAsString()) && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                //20190606 edit: Sol  查詢不推播
//                appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                tradeDetailService.save(tradeDetail);
            }

            if ("9998".equals(dataJson.get("status").getAsString()) && !SystemInstance.TRADE_FAIL.equals(tradeDetail.getStatus())) {
                tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                tradeDetailService.save(tradeDetail);
            }


        } else if ("14010".equals(requestHeader.getMethod())) {

            MegaWeiXinResponse queryRequest = new Gson().fromJson(platformResponse, MegaWeiXinResponse.class);
            MegaWeiXinResponseHeader queryRequestHeader = queryRequest.getHeader();
            MegaWeiXinResponseData queryRequestData = queryRequest.getData();

            if ("0000".equals(queryRequestHeader.getStatusCode()) && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                String sys_status = queryRequestData.getDataValue().replaceAll(".*\"SYS_STATUS\":\"(\\w*)\".*", "$1");
                LOGGER.info("sys_status : " + sys_status);
                switch (sys_status) {
                    case "0":
                        tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
                        break;
                    case "1":
                        tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                        break;
                    case "2":
                        tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                        break;
                    case "4":
                        tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                        break;
                    case "5":
                        tradeDetail.setStatus(SystemInstance.TRADE_CANCEL_SUCCESS);
                        break;
                }
            }
            tradeDetailService.save(tradeDetail);
        } else if ("10220".equals(requestHeader.getMethod())) {
            //todo Ali
        } else if ("10300".equals(requestHeader.getMethod())) {
            //todo Pi
        } else if ("10500".equals(requestHeader.getMethod())) {
            AllpaySingleQueryResponseData allpaySingleQueryResponseData = new Gson().fromJson(platformResponse, AllpaySingleQueryResponseData.class);
            if (allpaySingleQueryResponseData.getRtnCode().equals("1")) {
                if (allpaySingleQueryResponseData.getTradeStatus().equals("110")) {
                    if (!tradeDetail.getStatus().equals(SystemInstance.TRADE_SUCCESS)) {
                        tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                        tradeDetail.setSystemOrderId(allpaySingleQueryResponseData.getAllpayTradeNo());
                        Merchant merchant = merchantService.getOne(mchId);
                        //20190606 edit: Sol  查詢不推播
//                        appNotificationService.notifyApp(tradeDetail, merchant);
                    }
                } else if (allpaySingleQueryResponseData.getTradeStatus().equals("0") || allpaySingleQueryResponseData.getTradeStatus().equals("220")) {
                    if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                        tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                    }
                }
                tradeDetailService.save(tradeDetail);
            }


        } else if ("60300".equals(requestHeader.getMethod())) {
            LOGGER.info("Pi Inapp  singleorder query trade detatil");
            //platformResponse ="{\"Header\":{\"StatusCode\":\"0000\",\"StatusDesc\":\"success\",\"Method\":\"60300\",\"ServiceType\":\"SingleOrderQuery\",\"MchId\":\"mwd\"},\"Data\":{\"SysOrderNo\":\"GP34398C3UBPB88W\",\"StoreOrderNo\":\"iOS2899282444\",\"TotalFee\":\"1\",\"FeeType\":\"TWD\",\"PlatformRsp\":\"{\\\"result\\\":\\\"success\\\",\\\"err_code\\\":\\\"0\\\",\\\"rn\\\":1,\\\"id\\\":\\\"GP34398C3UBPB88W\\\",\\\"partner_id\\\":\\\"UE00000000000019\\\",\\\"partner_name\\\":\\\"\\\\u9ea5\\\\u5473\\\\u767b\\\",\\\"partner_user_id\\\":\\\"benson\\\",\\\"partner_order_id\\\":\\\"iOS2899282444\\\",\\\"money\\\":1,\\\"fee\\\":0,\\\"mobile\\\":\\\"0952851310\\\",\\\"mobile_validated\\\":\\\"Y\\\",\\\"status\\\":\\\"3\\\",\\\"paid_at\\\":\\\"2017-06-2814:17:18\\\",\\\"refunded_at\\\":\\\"1970-01-0100:00:00\\\"}\",\"PaidAt\":\"2017-06-2814:17:18\",\"orderStatus\":\"3\",\"UserId\":\"benson\",\"Mobile\":\"0952851310\"}}";
            JsonObject json = new Gson().fromJson(platformResponse, JsonObject.class);
            JsonObject data1 = json.getAsJsonObject("Data");
            JsonPrimitive data2 = data1.getAsJsonPrimitive("PlatformRsp");
            if (data2 == null) {
                return;
            }
            PiServerOrderQueryResponse piServerOrderQueryResponse = new Gson().fromJson(data2.getAsString(), PiServerOrderQueryResponse.class);
            ResponseGeneralBody responseGeneralBody = new Gson().fromJson(platformResponse, ResponseGeneralBody.class);

            LOGGER.info("[PI][platformresp]" + new Gson().toJson(piServerOrderQueryResponse));

            if (responseGeneralBody.getHeader().getStatusCode().equals("9998")) {
                //tradeDetail.setStatus("Trade fail");
            } else {
                //PiServerOrderQueryResponse piServerOrderQueryResponse = new Gson().fromJson(integratedSingleOrderQueryResponseData.getPlatformRsp(), PiServerOrderQueryResponse.class);
                if ("success".equals(piServerOrderQueryResponse.getResult()) && "0".equals(piServerOrderQueryResponse.getErrorCode())) {
                    if ("3".equals(piServerOrderQueryResponse.getStatus())) {
                        //20190606 edit: Sol  查詢不推播
//                        if(!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus()))
//                            appNotificationService.notifyApp(tradeDetail, merchantService.getOne(mchId));
                        tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    } else if ("A".equals(piServerOrderQueryResponse.getStatus())) {
                        if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                        }
                    } else if ("D".equals(piServerOrderQueryResponse.getStatus())) {
                        tradeDetail.setRefundStatus("Refund success");
                    }
                } else {
                    //tradeDetail.setStatus("Waiting");

                }
            }
            tradeDetailService.save(tradeDetail);

        } else if ("11300".equals(requestHeader.getMethod())) {
            GamaResponseData gamaResponseData = new Gson().fromJson(platformResponse, GamaResponseData.class);

            if (gamaResponseData.getResultCode().equals("0000")) {
                if (gamaResponseData.getStatusCode().equals("100")) {
                    if (!tradeDetail.getStatus().equals(SystemInstance.TRADE_SUCCESS)) {
                        tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                        tradeDetail.setSystemOrderId(gamaResponseData.getTransactionId());
                        tradeDetail.setPlatformPaidDate(gamaResponseData.getTransDate());
                        //20190606 edit: Sol  查詢不推播
//                        appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                    }
                } else if (gamaResponseData.getStatusCode().equals("0") || gamaResponseData.getStatusCode().equals("")) {
                    if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                        tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
                    }
                } else if (gamaResponseData.getStatusCode().equals("200")) {
                    if (!tradeDetail.getRefundStatus().equals(SystemInstance.TRADE_REFUND_SUCCESS)) {
                        tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                        updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_SUCCESS);
                    }
                } else {
                    if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                        tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                    }

                }
            }
            tradeDetailService.save(tradeDetail);
        } else if ("11400".equals(requestHeader.getMethod())) {
            //todo update trade
            AipeiPaymentResponseData response = new Gson().fromJson(platformResponse, AipeiPaymentResponseData.class);

            if (response.getErrorCode().equals("")) {
                AipeiPaymentResponseParaData data = new Gson().fromJson(response.getData(), AipeiPaymentResponseParaData.class);
                if (data.getApTransResult().equals("SUCCESSFUL") && !tradeDetail.getStatus().equals(SystemInstance.TRADE_SUCCESS)) {
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    tradeDetail.setSystemOrderId(data.getApTransId());
                    tradeDetail.setPlatformPaidDate(data.getApPayTime());
                    //20190606 edit: Sol  查詢不推播
//                    appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                } else if (data.getApTransResult().equals("REFUNDED") && (tradeDetail.getRefundStatus() == null || !tradeDetail.getRefundStatus().equals(SystemInstance.TRADE_REFUND_SUCCESS))) {
                    tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                    updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_SUCCESS);
                }
            }
            tradeDetailService.save(tradeDetail);
        } else if ("12520".equals(requestHeader.getMethod())) {
            LOGGER.info("[12520]" + platformResponse);
            JsonObject jo = new Gson().fromJson(platformResponse, JsonObject.class);
            AliShangHaiQueryResponse aliShangHaiQueryResponse = new Gson().fromJson(jo.get("response"), AliShangHaiQueryResponse.class);
            LOGGER.info("[aliShangHaiQueryResponse] " + aliShangHaiQueryResponse);
            //SingleOrderQuery
            if ("SUCCESS".equals(aliShangHaiQueryResponse.getResultCode()) && "TRADE_SUCCESS".equals(aliShangHaiQueryResponse.getAlipayTransStatus())) {
                //call API success
//				if("1".equals(aliShangHaiQueryResponse.getResultCode())){
//					//trade success
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus()))
                    //20190606 edit: Sol  查詢不推播
//                    appNotificationService.notifyApp(tradeDetail, merchantService.getOne(mchId));
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
//				}else if("2".equals(aliShangHaiQueryResponse.getTransactionStatus())) {
//					//trade fail
//					tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
//				}else if("3".equals(aliShangHaiQueryResponse.getTransactionStatus())) {
//					//trade cancel
//					tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
//					tradeDetail.setRefundStatus(SystemInstance.TRADE_CANCEL_SUCCESS);
//				}else if("4".equals(aliShangHaiQueryResponse.getTransactionStatus())) {
//					//trade refunded
//					tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
//					tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
//				}

                tradeDetailService.save(tradeDetail);
            } else {
                LOGGER.error("[ERROR]");
                //call API fail
            }
        } else if ("13500".equals(requestHeader.getMethod())) {
            LOGGER.info("[13500]" + platformResponse);
            SingleOrderRes singleOrderRes = new Gson().fromJson(platformResponse, SingleOrderRes.class);
            if ("00".equals(singleOrderRes.getSingleOrderHeaderRes().getsCode()) && "00".equals(singleOrderRes.getSingleOrderDataRes().getOrderStatus())) {
                //call API success
//					//trade success
                //20190606 edit: Sol  查詢不推播
//                if(!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus()))
//                    appNotificationService.notifyApp(tradeDetail, merchantService.getOne(mchId));
                tradeDetail.setSystemOrderId(singleOrderRes.getSingleOrderDataRes().getTxnSeqNo());
                tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                tradeDetailService.save(tradeDetail);
            }
        } else if ("11500".equals(requestHeader.getMethod())) {
            LOGGER.info("[11500]" + platformResponse);
            //{"returnCode":"0000","returnMessage":"Success.","info":[{"transactionId":2019050579620291910,"transactionDate":"2019-05-05T03:11:15Z",
            // "transactionType":"PAYMENT","productName":"85摨列-9544","currency":"TWD","payInfo":[{"method":"CREDIT_CARD","amount":68}],
            // "orderId":"20190505111112TWW030081002416"}]}
            LineQueryResponseData lineQueryResponseData = new Gson()
                    .fromJson(platformResponse, LineQueryResponseData.class);

            List<LineInfoData> lineInfoDataList = lineQueryResponseData.getInfo();

            LineInfoData lineInfoData = CollectionUtils.isNotEmpty(lineInfoDataList) ? lineInfoDataList.get(0) : null;
            if (Objects.nonNull(lineInfoData) && Objects.nonNull(lineInfoData.getPayInfos())) {
                tradeDetail.setTxParams(new Gson().toJson(lineInfoData.getPayInfos()));
            }
            if ("0000".equals(lineQueryResponseData.getReturnCode())
                    && "Success.".equals(lineQueryResponseData.getReturnMessage())) {
                String transactionType = lineInfoData.getTransactionType();
                if ("PAYMENT".equals(transactionType)) {
                    //20190606 edit: Sol  查詢不推播
//                    if(!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus()))
//                        appNotificationService.notifyApp(tradeDetail, merchantService.getOne(mchId));
                    tradeDetail.setSystemOrderId(String.valueOf(lineInfoData.getTransactionId()));
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                } else if ("PAYMENT_REFUND".equals(transactionType)) {
                    tradeDetail.setSystemRefundId(String.valueOf(lineInfoData.getTransactionId()));
                    tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                    updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_SUCCESS);
                }
                tradeDetailService.save(tradeDetail);
            }

        } else if ("13000".equals(requestHeader.getMethod())) {
            LuckyPayQueryOrderResponseData response = new Gson().fromJson(platformResponse, LuckyPayQueryOrderResponseData.class);
            if (response.getReturnCode().equals("000000")) {
                if (("01".equals(response.getStatus()) || "03".equals(response.getStatus()) || "05".equals(response.getStatus())) && !tradeDetail.getStatus().equals(SystemInstance.TRADE_SUCCESS)) {
                    //20190606 edit: Sol  查詢不推播
//                    if(!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus()))
//                        appNotificationService.notifyApp(tradeDetail, merchantService.getOne(mchId));
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    tradeDetail.setSystemOrderId(response.getTransactionId());
                } else if ("02".equals(response.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                } else if ("00".equals(response.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
                } else if ("04".equals(response.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                    updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_SUCCESS);
                } else if (("01".equals(response.getStatus()) || "03".equals(response.getStatus())) && (tradeDetail.getRefundStatus() == null || !tradeDetail.getRefundStatus().equals(SystemInstance.TRADE_REFUND_SUCCESS)) && !"0".equals(Integer.toString(response.getRefundAmount()))) {
                    tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                    updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_SUCCESS);
                }
            }
            tradeDetailService.save(tradeDetail);
        } else if ("11100".equals(requestHeader.getMethod())) {
            YuanTaOfflineResponse yuanTaOfflineResponse = new Gson().fromJson(platformResponse, YuanTaOfflineResponse.class);
            if ("S".equals(yuanTaOfflineResponse.getStatus())) {
                YuanTaOfflineQueryResponseDataObject yuanTaOfflineQueryResponseDataObject = new Gson().fromJson(yuanTaOfflineResponse.getSecureData(), YuanTaOfflineQueryResponseDataObject.class);
                if ("1".equals(yuanTaOfflineQueryResponseDataObject.getOrderStatus())) {
                    //call API success
//					//trade success
                    //20190606 edit: Sol  查詢不推播
//                    if(!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus()))
//                        appNotificationService.notifyApp(tradeDetail, merchantService.getOne(mchId));
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                } else if ("0".equals(yuanTaOfflineQueryResponseDataObject.getOrderStatus())) {
                    //trade waiting
                    tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
                } else if ("2".equals(yuanTaOfflineQueryResponseDataObject.getOrderStatus())) {
                    //trade fail
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                } else if ("3".equals(yuanTaOfflineQueryResponseDataObject.getOrderStatus())) {
                    //trade canceling
                    tradeDetail.setStatus(SystemInstance.TRADE_CANCEL_INIT);
                } else if ("4".equals(yuanTaOfflineQueryResponseDataObject.getOrderStatus())) {
                    //trade cancel
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    tradeDetail.setRefundStatus(SystemInstance.TRADE_CANCEL_SUCCESS);
                } else if ("5".equals(yuanTaOfflineQueryResponseDataObject.getOrderStatus())) {
                    //trade cancel fail
                    tradeDetail.setRefundStatus(SystemInstance.TRADE_CANCEL_FAIL);
                } else if ("6".equals(yuanTaOfflineQueryResponseDataObject.getOrderStatus())) {
                    //trade refunding
                    tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_INIT);
                } else if ("7".equals(yuanTaOfflineQueryResponseDataObject.getOrderStatus())) {
                    //trade refunded
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                    updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_SUCCESS);
                } else if ("8".equals(yuanTaOfflineQueryResponseDataObject.getOrderStatus())) {
                    //trade refunded fail
                    tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
                }

                tradeDetailService.save(tradeDetail);

            }
        } else if ("13320".equals(requestHeader.getMethod()) || "13410".equals(requestHeader.getMethod())) {
            GwMerchantApiQueryResponse response = new Gson().fromJson(platformResponse, GwMerchantApiQueryResponse.class);
            if ("000".equals(response.getReturnCode())) {
                if ("0".equals(response.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
                } else {
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                }
                if (response.getRefundId() != null && !"".equals(response.getRefundId())) {
                    tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                    updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_SUCCESS);
                }
            } else if ("001".equals(response.getReturnCode())) {
                tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            }
            tradeDetailService.save(tradeDetail);
        } else if ("13600".equals(tradeDetail.getMethod())) {
            Map<String, String> respMap = JsonUtil.parseJson(platformResponse, Map.class);
            String result = respMap.get("result");
            if ("000".equals(result)) {
                Map<String, String> resultObjectMap = JsonUtil.parseJson(JsonUtil.toJsonStr(respMap.get("result_object")), Map.class);
                List<JsonNode> tranactionsList = JsonUtil.parseJsonList(JsonUtil.toJsonStr(resultObjectMap.get("transactions")), JsonNode.class);
                if (CollectionUtils.isNotEmpty(tranactionsList)) {
                    JsonNode jsonNode = tranactionsList.get(0);
                    String status = String.valueOf(jsonNode.get("status").asInt());
                    if (jsonNode.has("refund_history")) {
                        List<JsonNode> refundList = JsonUtil.parseJsonList(JsonUtil.toJsonStr(jsonNode.get("refund_history")), JsonNode.class);
                        JsonNode refundJsonNode = refundList.get(0);
                        RefundDetail refundDetail = refundDetailService.getOne(tradeDetail.getOrderId());
                        if ("0".equals(status)) {
                            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                            tradeDetail.setSystemRefundId(refundJsonNode.get("refund_tradeNo").asText());

                            refundDetail.setStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                            refundDetail.setSystemRefundId(tradeDetail.getSystemRefundId());
                            refundDetail.setPlatformRefundDate(refundJsonNode.get("time").asText());
                        } else {
                            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
                        }
                        refundDetailService.save(refundDetail);
                    } else if ("0".equals(status)) {
                        tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                        tradeDetail.setSystemOrderId(jsonNode.get("tradeNo").asText());
                    } else if ("101".equals(status)) {
                        tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
                    } else {
                        tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                    }
                    tradeDetailService.save(tradeDetail);
                }
            }
        }

        LOGGER.info("[END]");
    }

    private void updateCancelResponse(RequestHeader requestHeader, JsonObject requestData, String platformResponse, TradeDetail tradeDetail) {
        if ("10300".equals(requestHeader.getMethod())) {
            ResponseGeneralBody responseGeneralBody = new Gson().fromJson(platformResponse, ResponseGeneralBody.class);
            JsonObject jsonObject = new JsonParser().parse(platformResponse).getAsJsonObject();
            JsonObject responseGeneraldata = jsonObject.getAsJsonObject("Data");

            IntegratedRefundResponseData integratedRefundResponseData = new Gson().fromJson(responseGeneraldata.toString(), IntegratedRefundResponseData.class);
            if (responseGeneralBody.getHeader().getStatusCode().equals("0000")) {
                tradeDetail.setSystemRefundId(integratedRefundResponseData.getSysRefundNo());
                tradeDetail.setRefundStatus(SystemInstance.TRADE_CANCEL_SUCCESS);
            } else {
                tradeDetail.setRefundStatus(SystemInstance.TRADE_CANCEL_FAIL);
            }
        } else if ("20400".equals(requestHeader.getMethod())) {
            TCBCreditCardResponseData tcbCreditCardResponseData = new Gson().fromJson(platformResponse, TCBCreditCardResponseData.class);
            tradeDetail.setSystemRefundId(tcbCreditCardResponseData.getOrderId());
            tradeDetail.setRefundStatus(SystemInstance.TRADE_CANCEL_SUCCESS);
        } else if ("20800".equals(requestHeader.getMethod())) {
            JsonObject jsonObject = new JsonParser().parse(platformResponse).getAsJsonObject();
            JsonObject responseGeneraldata = jsonObject.getAsJsonObject("Data");
            JsonPrimitive data2 = responseGeneraldata.getAsJsonPrimitive("PlatformRsp");
            TSBOtherResponseData tsbOtherResponseData = new Gson().fromJson(data2.getAsString(), TSBOtherResponseData.class);

            LOGGER.info("[Tsbank platformResponse]" + new Gson().toJson(tsbOtherResponseData));
            if (tsbOtherResponseData.getParams().getRetCode().equals("00") || tsbOtherResponseData.getParams().getRetCode().equals("-318")) {
                tradeDetail.setSystemRefundId(tsbOtherResponseData.getParams().getRrn());
                tradeDetail.setRefundStatus(SystemInstance.TRADE_CANCEL_SUCCESS);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_CANCEL_SUCCESS);
            } else {
                tradeDetail.setRefundStatus(SystemInstance.TRADE_CANCEL_FAIL);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_CANCEL_FAIL);
            }

        } else if ("21100".equals(requestHeader.getMethod())) {
            JsonObject jsonObject = new JsonParser().parse(platformResponse).getAsJsonObject();
            LOGGER.info("[ECPay platformResponse]" + new Gson().toJson(requestData));

            if ("1".equals(jsonObject.get("RtnCode").getAsString())) {
                tradeDetail.setSystemRefundId(jsonObject.get("MerchantTradeNo").getAsString());
                tradeDetail.setRefundStatus(SystemInstance.TRADE_CANCEL_SUCCESS);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_CANCEL_SUCCESS);
            } else {
                tradeDetail.setRefundStatus(SystemInstance.TRADE_CANCEL_FAIL);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_CANCEL_FAIL);
            }
        } else if ("22600".equals(requestHeader.getMethod()) || "23900".equals(requestHeader.getMethod())) {

            FOCASPayResponse fOCASPayResponse = new Gson().fromJson(platformResponse, FOCASPayResponse.class);

            if ("0000".equals(fOCASPayResponse.getErrCode())) {
                tradeDetail.setSystemRefundId(fOCASPayResponse.getLidm());
                tradeDetail.setRefundStatus(SystemInstance.TRADE_CANCEL_SUCCESS);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_CANCEL_SUCCESS);
            } else {
                tradeDetail.setRefundStatus(SystemInstance.TRADE_CANCEL_FAIL);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_CANCEL_FAIL);
            }

        } else if ("11100".equals(requestHeader.getMethod())) {
            YuanTaOfflineResponse yuanTaOfflineResponse = new Gson().fromJson(platformResponse, YuanTaOfflineResponse.class);
            if ("S".equals(yuanTaOfflineResponse.getStatus())) {
                YuanTaOfflineCancelResponseDataObject yuanTaOfflineCancelResponseDataObject = new Gson().fromJson(yuanTaOfflineResponse.getSecureData(), YuanTaOfflineCancelResponseDataObject.class);
                tradeDetail.setSystemRefundId(yuanTaOfflineCancelResponseDataObject.getBankCancelOrderNo());
                tradeDetail.setRefundStatus(SystemInstance.TRADE_CANCEL_SUCCESS);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_CANCEL_SUCCESS);
            }

        } else if ("12520".equals(requestHeader.getMethod())) {
            LOGGER.info("[AliPayShangHai platformResponse]" + new Gson().toJson(requestData));
            JsonObject jo = new Gson().fromJson(platformResponse, JsonObject.class);
            AliShangHaiCancelResponse aliShangHaiCancelResponse = new Gson().fromJson(jo.get("response"), AliShangHaiCancelResponse.class);
            LOGGER.info("[aliShangHaiCancelResponse] " + aliShangHaiCancelResponse);

            if ("SUCCESS".equals(aliShangHaiCancelResponse.getResultCode())) {
                tradeDetail.setSystemRefundId(aliShangHaiCancelResponse.getOutTradeNo());
                tradeDetail.setRefundStatus(SystemInstance.TRADE_CANCEL_SUCCESS);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_CANCEL_SUCCESS);
            } else {
                tradeDetail.setRefundStatus(SystemInstance.TRADE_CANCEL_FAIL);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_CANCEL_FAIL);

            }
        } else if ("23240".equals(requestHeader.getMethod()) || "23800".equals(requestHeader.getMethod())) {

            FuBonParamBean fuBonParamRes = new Gson().fromJson(platformResponse, FuBonParamBean.class);
            if (FubonUtil.isPaymentApproved(fuBonParamRes)) {
                tradeDetail.setSystemRefundId(fuBonParamRes.getOrderID());
                tradeDetail.setRefundStatus(SystemInstance.TRADE_CANCEL_SUCCESS);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_CANCEL_SUCCESS);
            } else {
                tradeDetail.setRefundStatus(SystemInstance.TRADE_CANCEL_FAIL);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_CANCEL_FAIL);
            }
        }

        tradeDetailService.save(tradeDetail);

    }

    private void updateRefundResponse(RequestHeader requestHeader, JsonObject requestData, String platformResponse, TradeDetail tradeDetail) {
        if ("10110".equals(requestHeader.getMethod())) {
            ResponseGeneralBody responseGeneralBody = new Gson()
                    .fromJson(platformResponse, ResponseGeneralBody.class);
            JsonObject jsonObject = new JsonParser().parse(platformResponse).getAsJsonObject();
            JsonObject responseGeneraldata = jsonObject.getAsJsonObject("Data");

            if (responseGeneraldata.has("DataValue") && !responseGeneraldata.get("DataValue")
                    .isJsonNull()) {
                JsonPrimitive jsonPrimitive = responseGeneraldata.get("DataValue")
                        .getAsJsonPrimitive();

                JsonObject DataValueJson = JsonUtil.convertJsonPrimitiveToJsonObject(jsonPrimitive);
                if (responseGeneralBody.getHeader().getStatusCode()
                        .equals(SystemInstance.STATUS_SUCCESS)) {
                    tradeDetail.setSystemRefundId(DataValueJson.get("SYS_REFUND_NO").getAsString());
                    tradeDetail.setRefundStatus("Refund success");
                    updateRefundDeatil(tradeDetail, requestData, "Refund success");
                } else {
                    tradeDetail.setRefundStatus("Refund fail");
                    updateRefundDeatil(tradeDetail, requestData, "Refund fail");
                }
                tradeDetailService.save(tradeDetail);
            } else if (responseGeneralBody.getHeader().getStatusCode().equals("7112")) {
                tradeDetail.setRefundStatus("Refund success");
                tradeDetailService.save(tradeDetail);
                updateRefundDeatil(tradeDetail, requestData, "Refund success");
            }
        } else if ("12710".equals(requestHeader.getMethod())) {
            ResponseGeneralBody responseGeneralBody = new Gson()
                    .fromJson(platformResponse, ResponseGeneralBody.class);
            JsonObject jsonObject = new JsonParser().parse(platformResponse).getAsJsonObject();

            if (jsonObject.has("refundOrderNo") && SystemInstance.STATUS_SUCCESS
                    .equals(jsonObject.get("status").getAsString())) {
                tradeDetail.setSystemRefundId(jsonObject.get("refundOrderNo").getAsString());
                tradeDetail.setRefundStatus("Refund success");
                updateRefundDeatil(tradeDetail, requestData, "Refund success");
            } else {
                tradeDetail.setRefundStatus("Refund fail");
                updateRefundDeatil(tradeDetail, requestData, "Refund fail");
            }
            tradeDetailService.save(tradeDetail);

        } else if ("14010".equals(requestHeader.getMethod())) {
            ResponseGeneralBody responseGeneralBody = new Gson().fromJson(platformResponse, ResponseGeneralBody.class);
            JsonObject jsonObject = new JsonParser().parse(platformResponse).getAsJsonObject();
            JsonObject responseGeneraldata = jsonObject.getAsJsonObject("Data");
            LOGGER.info("Data :" + new Gson().toJson(responseGeneraldata));
            if (responseGeneraldata.has("DataValue") && !responseGeneraldata.get("DataValue").isJsonNull()) {
                LOGGER.info("responseGeneralBody.getHeader() :" + new Gson().toJson(responseGeneralBody.getHeader()));
                String Is_refund = new Gson().fromJson(responseGeneraldata.get("DataValue").getAsString(), JsonObject.class).get("IS_REFUND").getAsString();
                if ("Y".equals(Is_refund)) {
                    tradeDetail.setRefundStatus("Refund success");
                    updateRefundDeatil(tradeDetail, requestData, "Refund success");
                } else {
                    tradeDetail.setRefundStatus("Refund fail");
                    updateRefundDeatil(tradeDetail, requestData, "Refund fail");
                }
                tradeDetailService.save(tradeDetail);
            }
        } else if ("10220".equals(requestHeader.getMethod())) {
            AliResponseBody aliResponseBody = new Gson()
                    .fromJson(platformResponse, AliResponseBody.class);
            if (aliResponseBody.getStatus().equals("S")) {
                AliRefundResponseData aliRefundResponseData = new Gson()
                        .fromJson(aliResponseBody.getSecureData(), AliRefundResponseData.class);
                tradeDetail.setSystemRefundId(aliRefundResponseData.getBankRefundOrderNo());
                tradeDetail.setRefundStatus("Refund success");
                updateRefundDeatil(tradeDetail, requestData, "Refund success");

            } else {
                tradeDetail.setRefundStatus("Refund fail");
                updateRefundDeatil(tradeDetail, requestData, "Refund fail");

            }
            tradeDetailService.save(tradeDetail);
        } else if ("10300".equals(requestHeader.getMethod())) {
            ResponseGeneralBody responseGeneralBody = new Gson()
                    .fromJson(platformResponse, ResponseGeneralBody.class);
            JsonObject jsonObject = new JsonParser().parse(platformResponse).getAsJsonObject();
            JsonObject responseGeneraldata = jsonObject.getAsJsonObject("Data");

            IntegratedRefundResponseData integratedRefundResponseData = new Gson()
                    .fromJson(responseGeneraldata.toString(), IntegratedRefundResponseData.class);
            if (responseGeneralBody.getHeader().getStatusCode()
                    .equals(SystemInstance.STATUS_SUCCESS) || responseGeneralBody.getHeader()
                    .getStatusCode().equals("7112")) {
                tradeDetail.setSystemRefundId(integratedRefundResponseData.getSysRefundNo());
                tradeDetail.setRefundStatus("Refund success");
                updateRefundDeatil(tradeDetail, requestData, "Refund success");

            } else {
                tradeDetail.setRefundStatus("Refund fail");
                updateRefundDeatil(tradeDetail, requestData, "Refund fail");

            }
            tradeDetailService.save(tradeDetail);
        } else if ("10500".equals(requestHeader.getMethod())) {

            if (tradeDetail.getServiceType().equals("OLPay")) {

                if (tradeDetail.getType() == 1) {

                    if (platformResponse.startsWith("1")) {
                        tradeDetail.setRefundStatus("Refund success");
                        updateRefundDeatil(tradeDetail, requestData, "Refund success");
                    } else {
                        tradeDetail.setRefundStatus("Refund fail");
                        updateRefundDeatil(tradeDetail, requestData, "Refund fail");
                    }

                } else {
                    AllpayOLCreditRefundResponse allpayOLCreditRefundResponse = new Gson()
                            .fromJson(platformResponse, AllpayOLCreditRefundResponse.class);
                    if (allpayOLCreditRefundResponse.getRtnCode().equals("1")) {
                        tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                        tradeDetail
                                .setSystemRefundId(allpayOLCreditRefundResponse.getRefundTradeNo());
                        updateRefundDeatil(tradeDetail, requestData,
                                SystemInstance.TRADE_REFUND_SUCCESS);

                    } else {
                        tradeDetail.setRefundStatus("Refund fail");
                        updateRefundDeatil(tradeDetail, requestData, "Refund fail");
                    }
                }
                tradeDetailService.save(tradeDetail);


            } else {
                AllpayRefundResponseData allpayRefundResponseData = new Gson()
                        .fromJson(platformResponse, AllpayRefundResponseData.class);
                if (allpayRefundResponseData.getRtnCode().equals("1")) {
                    tradeDetail.setSystemRefundId(allpayRefundResponseData.getRefundTradeNo());
                    tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                    updateRefundDeatil(tradeDetail, requestData,
                            SystemInstance.TRADE_REFUND_SUCCESS);

                } else {
                    tradeDetail.setRefundStatus("Refund fail");
                    updateRefundDeatil(tradeDetail, requestData, "Refund fail");

                }
                tradeDetailService.save(tradeDetail);
            }
        } else if ("60300".equals(requestHeader.getMethod())) {
            JsonObject jsonObject = new JsonParser().parse(platformResponse).getAsJsonObject();
            JsonObject responseGeneraldata = jsonObject.getAsJsonObject("Data");
            if (!responseGeneraldata.has("PlatformRsp")) {
                tradeDetail.setRefundStatus("Refund fail");
                updateRefundDeatil(tradeDetail, requestData, "Refund fail");
            } else {
                JsonPrimitive data2 = responseGeneraldata.getAsJsonPrimitive("PlatformRsp");
                PiServerRefundResponse piServerRefundResponse = new Gson()
                        .fromJson(data2.getAsString(), PiServerRefundResponse.class);

                LOGGER.info("piServerRefundResponse" + new Gson().toJson(piServerRefundResponse));

                if ("success".equals(piServerRefundResponse.getResult()) && "0"
                        .equals(piServerRefundResponse.getErrCode())) {
                    tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                    updateRefundDeatil(tradeDetail, requestData,
                            SystemInstance.TRADE_REFUND_SUCCESS);

                } else {
                    tradeDetail.setRefundStatus("Refund fail");
                    updateRefundDeatil(tradeDetail, requestData, "Refund fail");
                }
            }
            tradeDetailService.save(tradeDetail);

        } else if ("10900".equals(requestHeader.getMethod())) {
            JkosRefundResponseData jkosRefundResponseData = new Gson()
                    .fromJson(platformResponse, JkosRefundResponseData.class);
            if ("000".equals(jkosRefundResponseData.getStatusCode())) {
                tradeDetail.setSystemRefundId(jkosRefundResponseData.getRefundTradeNo());
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_SUCCESS);
            } else {
                tradeDetail.setRefundStatus("Refund fail");
                updateRefundDeatil(tradeDetail, requestData, "Refund fail");
            }
            tradeDetailService.save(tradeDetail);

        } else if ("11300".equals(requestHeader.getMethod())) {
            GamaResponseData gamaResponseData = new Gson().fromJson(platformResponse, GamaResponseData.class);
            if ("0000".equals(gamaResponseData.getResultCode())) {
                tradeDetail.setSystemRefundId(gamaResponseData.getRelationID());

                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_SUCCESS);
            } else {
                tradeDetail.setRefundStatus("Refund fail");
                updateRefundDeatil(tradeDetail, requestData, "Refund fail");
            }
            tradeDetailService.save(tradeDetail);
        } else if ("11400".equals(requestHeader.getMethod())) {
            AipeiPaymentResponseData aipeiPaymentResponseData = new Gson()
                    .fromJson(platformResponse, AipeiPaymentResponseData.class);
            if ("".equals(aipeiPaymentResponseData.getErrorCode())) {
                AipeiPaymentResponseParaData data = new Gson()
                        .fromJson(aipeiPaymentResponseData.getData(),
                                AipeiPaymentResponseParaData.class);
                if (data.getApTransResult().equals("REFUNDED")) {
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    tradeDetail.setSystemRefundId(data.getApTransId());
                    tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                    updateRefundDeatil(tradeDetail, requestData,
                            SystemInstance.TRADE_REFUND_SUCCESS);
                }
            } else {
                tradeDetail.setRefundStatus("Refund fail");
                updateRefundDeatil(tradeDetail, requestData, "Refund fail");
            }
            tradeDetailService.save(tradeDetail);
        } else if ("11500".equals(requestHeader.getMethod()) || "61500"
                .equals(requestHeader.getMethod())) {
            LineResponseData lineResponseData = new Gson()
                    .fromJson(platformResponse, LineResponseData.class);
            if ("0000".equals(lineResponseData.getReturnCode())) {
                tradeDetail.setSystemRefundId(
                        lineResponseData.getInfo().getRefundTransactionId().toString());
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_SUCCESS);
            } else if ("1165".equals(lineResponseData.getReturnCode())) {
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                updateRefundDeatil(tradeDetail, requestData, "Refund success");
            } else {
                tradeDetail.setRefundStatus("Refund fail");
                updateRefundDeatil(tradeDetail, requestData, "Refund fail");
            }
            tradeDetailService.save(tradeDetail);
        } else if ("13000".equals(requestHeader.getMethod())) {
            LuckyPayQueryOrderResponseData response = new Gson()
                    .fromJson(platformResponse, LuckyPayQueryOrderResponseData.class);
            if ("000000".equals(response.getReturnCode())) {
                tradeDetail.setSystemRefundId(response.getTransactionId());
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_SUCCESS);
            } else {
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_FAIL);
            }
            tradeDetailService.save(tradeDetail);
        } else if ("14200".equals(requestHeader.getMethod()) || "15000".equals(requestHeader.getMethod()) || "14300".equals(requestHeader.getMethod())
                || "14400".equals(requestHeader.getMethod()) || "12000".equals(requestHeader.getMethod()) || "15600".equals(requestHeader.getMethod())) {

            TwPayRequestData twPayRequestData = new Gson()
                    .fromJson(platformResponse, TwPayRequestData.class);

            if ("0000".equals(twPayRequestData.getResponseCode())) {
                tradeDetail.setSystemRefundId(tradeDetail.getSystemOrderId());
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_SUCCESS);
            } else if (!SystemInstance.TRADE_REFUND_SUCCESS.equals(tradeDetail.getRefundStatus())) {
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_FAIL);
            }
        } else if ("11100".equals(requestHeader.getMethod())) {
            YuanTaOfflineResponse yuanTaOfflineResponse = new Gson()
                    .fromJson(platformResponse, YuanTaOfflineResponse.class);
            if ("S".equals(yuanTaOfflineResponse.getStatus())) {
                YuanTaOfflineRefundResponseDataObject yuanTaOfflineRefundResponseDataObject = new Gson()
                        .fromJson(yuanTaOfflineResponse.getSecureData(),
                                YuanTaOfflineRefundResponseDataObject.class);
                tradeDetail.setSystemRefundId(
                        yuanTaOfflineRefundResponseDataObject.getBankRefundOrderNo());
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_SUCCESS);

            } else {
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_FAIL);
            }

        } else if ("13500".equals(requestHeader.getMethod())) {
            LOGGER.info("[13500]" + platformResponse);
            TxnCancelRes txnCancelRes = new Gson()
                    .fromJson(platformResponse, TxnCancelRes.class);
            if ("00".equals(txnCancelRes.getTxnCancelHeaderRes().getsCode()) && "00".equals(txnCancelRes.getTxnCancelDataRes().getTxnResult())) {
                tradeDetail.setSystemRefundId(
                        txnCancelRes.getTxnCancelDataRes().getOrderNo());
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_SUCCESS);

            } else {
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_FAIL);
            }

        } else if ("12520".equals(requestHeader.getMethod())) {
            LOGGER.info("[12520]" + platformResponse);
            JsonObject jo = new Gson().fromJson(platformResponse, JsonObject.class);
            AliShangHaiRefundResponse aliShangHaiRefundResponse = new Gson()
                    .fromJson(jo.get("response"), AliShangHaiRefundResponse.class);
            LOGGER.info("[aliShangHaiRefundResponse] " + aliShangHaiRefundResponse);
            //Refund
            if ("SUCCESS".equals(aliShangHaiRefundResponse.getResultCode())) {
                //call API success
                //refund success
//                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                tradeDetail.setSystemRefundId(aliShangHaiRefundResponse.getPartnerRefundId());
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_SUCCESS);

            } else {
                LOGGER.error("[ERROR]");
                //call API fail
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_FAIL);
            }
        } else if ("23240".equals(requestHeader.getMethod()) || "23800".equals(requestHeader.getMethod())) {

            FuBonParamBean fuBonParamRes = new Gson().fromJson(platformResponse, FuBonParamBean.class);
            if (FubonUtil.isPaymentApproved(fuBonParamRes)) {
                tradeDetail.setSystemRefundId(fuBonParamRes.getOrderID());
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_SUCCESS);
            } else {
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_FAIL);
            }
        } else if ("13320".equals(requestHeader.getMethod()) || "13410".equals(requestHeader.getMethod())) {
            GwMerchantApiRefundResponse response = new Gson().fromJson(platformResponse, GwMerchantApiRefundResponse.class);
            if ("000".equals(response.getReturnCode())) {
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_SUCCESS);
            } else {
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_FAIL);
            }
        } else if ("15300".equals(tradeDetail.getMethod())) {
            tradeDetail.setStoreRefundId(requestData.get("StoreRefundNo").getAsString());
            tradeDetail.setSystemRefundId(requestData.get("StoreRefundNo").getAsString());
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
            updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_SUCCESS);
        } else if ("13600".equals(tradeDetail.getMethod())) {
            Map<String, String> respMap = JsonUtil.parseJson(platformResponse, Map.class);
            Map<String, String> resultMap = JsonUtil.parseJson(JsonUtil.toJsonStr(respMap.get("result_object")), Map.class);
            if ("000".equals(respMap.get("result"))) {
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
                tradeDetail.setSystemRefundId(resultMap.get("refund_tradeNo"));
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_SUCCESS);
            } else {
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_FAIL);
                updateRefundDeatil(tradeDetail, requestData, SystemInstance.TRADE_REFUND_FAIL);
            }
        }

        tradeDetailService.save(tradeDetail);
    }


    public RefundDetail updateRefundDeatil(TradeDetail tradeDetail, JsonObject requestData, String result) {
        LOGGER.info("[updateRefundDeatil][requestData]" + new Gson().toJson(requestData));
        RefundDetail refundDetail = null;

        if (requestData.has("StoreRefundNo")) {
            refundDetail = refundDetailService.getTopOneByStoreRefundId(tradeDetail.getAccountId(), requestData.get("StoreRefundNo").getAsString());
        } else {
            refundDetail = refundDetailService.getTopOneByStoreRefundId(tradeDetail.getAccountId(), "R" + requestData.get("StoreOrderNo").getAsString());
        }
        if (refundDetail != null
                && !(SystemInstance.TRADE_REFUND_SUCCESS.equals(refundDetail.getStatus())
                && SystemInstance.TRADE_CANCEL_SUCCESS.equals(refundDetail.getStatus()))) {
            refundDetail.setStatus(result);
            refundDetail.setSystemRefundId(tradeDetail.getSystemRefundId());
            refundDetailService.save(refundDetail);
        } else {
            refundDetail = new RefundDetail();
            Merchant merchant = merchantService.getOne(tradeDetail.getAccountId());

            refundDetail.setTradeDetailRandomId(tradeDetail);
            refundDetail.setCreateTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
            refundDetail.setAccountId(tradeDetail.getAccountId());
            refundDetail.setAmount(tradeDetail.getPayment());
            refundDetail.setCashier(requestData.has("Cashier") ? requestData.get("Cashier").getAsString() : null);
            if (requestData.has("DeviceInfo") && deviceService.getOne(merchant, requestData.get("DeviceInfo").getAsString()) != null) {
                refundDetail.setDeviceRandomId(deviceService.getOne(merchant, requestData.get("DeviceInfo").getAsString()));
            } else {
                refundDetail.setDeviceRandomId(null);
            }
            refundDetail.setOrderId(tradeDetail.getOrderId());
            refundDetail.setStatus(result);
            if (requestData.has("StoreRefundNo")) {
                refundDetail.setStoreRefundId(requestData.get("StoreRefundNo").getAsString());
            } else {
                refundDetail.setStoreRefundId("R" + requestData.get("StoreOrderNo").getAsString());
            }
            refundDetail.setReason(requestData.has("RefundedMsg") ? requestData.get("RefundedMsg").getAsString() : null);
            refundDetail.setMethod(tradeDetail.getMethod());
            RefundDetail entity = refundDetailService.save(refundDetail);
            LOGGER.info("[setRefundDetail][End]");
        }
        LOGGER.info("[updateRefundDeatil][End]");
        return refundDetail;

    }

    public void captureOrderSave(TradeDetail tradeDetail) {

        if (captureOrderDetailService.getOne(tradeDetail.getAccountId(), tradeDetail.getOrderId()) == null) {
            CaptureOrderDetail captureOrderDetail = new CaptureOrderDetail();
            //captureOrderDetail.setCaptureDetail(captureDetail);
            captureOrderDetail.setStoreOrderNo(tradeDetail.getOrderId());
            captureOrderDetail.setMerchantId(tradeDetail.getAccountId());
            captureOrderDetail.setTotalFee(tradeDetail.getPayment());
            captureOrderDetail.setTradeDetail(tradeDetail);
            captureOrderDetail.setStatus("wait");
            captureOrderDetail.setMethod(tradeDetail.getMethod());
            captureOrderDetail.setCreateTime(DateTime.now().toString("yyyyMMdd"));
            captureOrderDetail.setTradeDate(tradeDetail.getCreateDate());
            captureOrderDetailService.save(captureOrderDetail);
            LOGGER.info("captureOrderDetailService SAVED");
        }
    }

    public void updateRefundDeatil(TradeDetail tradeDetail, JsonObject jsonObject, String tradeRefundSuccess, String refundTime) {
        RefundDetail refundDetail = updateRefundDeatil(tradeDetail, jsonObject, tradeRefundSuccess);

        refundDetail.setPlatformRefundDate(refundTime);
        refundDetailService.save(refundDetail);
    }
}
