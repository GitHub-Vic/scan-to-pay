package co.intella.service;

import co.intella.domain.line.LineRefundRequestData;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import com.google.gson.JsonPrimitive;

import javax.crypto.SecretKey;
import java.util.Map;

/**
 * @author Alex
 */

public interface Pay2GoService {

    Boolean verify(Map<String,String> map, PaymentAccount paymentAccount);

    void updateNotify(Map<String,String> map, PaymentAccount paymentAccount) throws Exception;

    String aesDecrypt(SecretKey secretKey, byte[] iv, byte[] encryptText) throws Exception;

    String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;

    String refund(String dataString);

    String prepareRefund( PaymentAccount paymentAccount, TradeDetail tradeDetail);

}
