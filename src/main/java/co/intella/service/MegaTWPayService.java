package co.intella.service;

import co.intella.model.RequestHeader;
import com.google.gson.JsonPrimitive;

public interface MegaTWPayService {

  String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;

  String sendRequest(String url, String request) throws Exception;
}
