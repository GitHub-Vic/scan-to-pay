package co.intella.service;

import co.intella.domain.sp.SpRequestData;
import co.intella.domain.sp.SpSingleQueryRequestData;
import co.intella.model.RequestHeader;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.JsonPrimitive;

/**
 * @author Andy Lin
 */
public interface SpService {
    String doRequest(RequestHeader header, JsonPrimitive data) throws Exception;
    String payment(SpRequestData request) throws JsonProcessingException;
    String refundAndCapture(SpRequestData request) throws JsonProcessingException;
    String singleQuery(SpSingleQueryRequestData request) throws JsonProcessingException;
}
