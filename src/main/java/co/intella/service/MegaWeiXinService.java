package co.intella.service;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonPrimitive;

import co.intella.domain.megaweixin.OrderPaymentJson;
import co.intella.model.Merchant;
import co.intella.model.QrcodeParameter;
import co.intella.model.RequestHeader;

public interface MegaWeiXinService {
	
    public String doRequest(RequestHeader requestHeader, JsonPrimitive data, String gw, String clientIP) throws Exception;
    public ModelAndView doOLPay(QrcodeParameter qrcodeParameter, Merchant merchant, String totalFee, HttpServletResponse response);
    public String doNotify(OrderPaymentJson orderPaymentJson);
}
