package co.intella.service;

import co.intella.domain.ntuImoto.IMotoValuation;
import co.intella.model.TradeDetail;

public interface NtuImotoService {

    String doValuation(IMotoValuation iMotoValuationRequest);

    Long checkoutImotoAmount(String shortId);

    boolean doNotify(TradeDetail tradeDetail);
}
