package co.intella.service.qrcodeImage.impl;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64.Encoder;
import java.util.EnumMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import co.intella.model.IntTbLookupCodeNoFK;
import co.intella.repository.INT_TB_LookupCodeRepository;
import co.intella.request.QrcodeGetImageRequest;
import co.intella.response.GeneralResponse;
import co.intella.service.qrcodeImage.QrcodeGetImageService;

@Service
public class QrcodeGetImageServiceImpl implements QrcodeGetImageService{
	
	@Autowired
	private INT_TB_LookupCodeRepository lookupCodeRepository;

	@Override
	public GeneralResponse doService(QrcodeGetImageRequest qrcodeParameter) throws Exception {
		getQrcodeImage(qrcodeParameter);
		GeneralResponse generalResponse = new GeneralResponse();
		generalResponse.setData(qrcodeParameter);
		generalResponse.setMsg("");
		generalResponse.setMsgCode("0000");
		return generalResponse;
	}
	
	private void getQrcodeImage(QrcodeGetImageRequest qrcodeParameter) throws Exception {
		String qrcodeImageBack = generateQrcodeImage(qrcodeParameter, "static/images/qrcode/back.jpg" ,"jpeg");
		String qrcodeImage = generateQrcodeImage(qrcodeParameter, "static/images/qrcode/white-back.png", "png");
	    qrcodeParameter.setQrcodeImageBack(qrcodeImageBack);
	    qrcodeParameter.setQrcodeImage(qrcodeImage);
	}

	private String generateQrcodeImage(QrcodeGetImageRequest qrcodeParameter, String backImagePath, String imageType)
			throws WriterException, IOException {
		IntTbLookupCodeNoFK lookupCode = lookupCodeRepository.getByLookTypeAndLookupCode("serverDomain", "allpaypass");
		String myCodeText = lookupCode.getType1() + qrcodeParameter.getShortId();
		int size = 160;
		Map<EncodeHintType, Object> hintMap = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
        hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");

        hintMap.put(EncodeHintType.MARGIN, 1);
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix byteMatrix = qrCodeWriter.encode(myCodeText, BarcodeFormat.QR_CODE, size, size, hintMap);
        int width = byteMatrix.getWidth();
        BufferedImage image = new BufferedImage(width, width, BufferedImage.TYPE_INT_RGB);
        image.createGraphics();

        Graphics2D graphics = (Graphics2D) image.getGraphics();
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, width, width);
        graphics.setColor(Color.BLACK);

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < width; j++) {
                if (byteMatrix.get(i, j)) {
                    graphics.fillRect(i, j, 1, 1);
                }
            }
        }
        //Add back ground
        ClassLoader classLoader = getClass().getClassLoader();
        final BufferedImage backImage = ImageIO.read( new File(classLoader.getResource(backImagePath).getFile()));
        Graphics backGraphics=backImage.getGraphics();
        backGraphics.drawImage(image,backImage.getWidth()/2-image.getWidth()/2,backImage.getHeight()/2-image.getHeight()/2,null);

        //Add text
        FontMetrics metrics = graphics.getFontMetrics(new Font("Microsoft JhengHei",Font.PLAIN,18));
        int merchantNameWidth = metrics.stringWidth(qrcodeParameter.getBody());
        backGraphics.setColor(Color.BLACK);
        backGraphics.setFont(new Font("Microsoft JhengHei",Font.BOLD,18));
        backGraphics.drawString(qrcodeParameter.getBody(),(110 - Math.round(merchantNameWidth/2)),backImage.getHeight()-28);


        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(backImage, imageType, os);
        ByteArrayInputStream is = new ByteArrayInputStream(os.toByteArray());
	    byte[] result = IOUtils.toByteArray(is);
	    Encoder encoder = java.util.Base64.getEncoder();
	    String qrcodeImage = encoder.encodeToString(result);
		return qrcodeImage;
	}

	@Override
	public Map<String, Object> doValidate(QrcodeGetImageRequest obj) throws Exception {
		return null;
	}

}
