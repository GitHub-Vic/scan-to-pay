package co.intella.service.qrcodeImage;

import co.intella.request.QrcodeGetImageRequest;
import co.intella.service.GeneralService;

public interface QrcodeGetImageService extends GeneralService<QrcodeGetImageRequest>{
}