package co.intella.service;

import co.intella.model.Role;

import java.util.List;

/**
 * @author Andy Lin
 */
public interface RoleService {

//    List<Role> getListByIsEnable(int isEnable);

    List<Role> getListAll();

    Role getOneByName(String name);

    Role getOneById(Long id);

    Role save(Role role);

    void delete(Role role);

    List<Role> getListByIsPersonal(int isPersonal);
}
