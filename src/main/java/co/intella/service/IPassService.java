package co.intella.service;

import co.intella.domain.iPass.IPassBaseResponse;
import co.intella.domain.iPass.IPassGeneralRequest;
import co.intella.domain.iPass.IPassTradeSaleRequest;
import co.intella.domain.integration.ResponseGeneralData;
import co.intella.model.*;
import com.google.gson.JsonObject;

public interface IPassService {

    String doRequest(IPassGeneralRequest ezCardBasicRequest, String aesKey);

    String ezRequest(RequestHeader requestHeader, JsonObject requestData, Merchant merchant) throws Exception;

    String updateDoTradeSale(RequestHeader requestHeader, JsonObject requestData, Device device, Merchant merchant, IPassTradeSaleRequest iPassTradeSaleRequest, IPassBaseResponse response, TradeDetail tradeDetail, PaymentAccount paymentAccount);

    String getGeneralResponse(RequestHeader requestHeader, ResponseGeneralData response, String errorCode);

    String updateDoRefund(RequestHeader requestHeader, JsonObject requestData, Device device, Merchant merchant, IPassBaseResponse response, TradeDetail tradeDetail, PaymentAccount paymentAccount);

    void settlementMain(String createDate, String deviceId, String mchAccount);

    void settlementMain(String createDate, String endDate, String deviceId, String mchAccount);

}
