package co.intella.service;



import co.intella.model.EdgeToken;
import co.intella.model.EncryptType;

public interface EdgeService {

    EncryptType findEncryptType(String mchId);
    
    EdgeToken findToken(String token);
}
