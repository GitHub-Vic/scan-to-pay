package co.intella.service;

import co.intella.model.QrcodeParameter;

import java.util.Map;

/**
 * @author Alex
 */

public interface OrderService {

    String createOrderId(String merchantId, QrcodeParameter qrcodeParameter);
    String createOrderId(String merchantId);
    String createDGpayOrderId(String merchantId);

    String createOrderIdBySequence(String merchantId);

    Map<String, String> createOrderIdByMchId(String merchantId);

    String createFGSOrderId(String merchantId);
}
