package co.intella.service;

import co.intella.model.IntTbLookupType;

public interface LookupTypeService {


    IntTbLookupType findOneByLookType(String lookupType);

}
