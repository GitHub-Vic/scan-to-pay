package co.intella.service;

import co.intella.model.Merchant;
import co.intella.model.TradeDetail;

/**
 * @author Miles
 */
public interface AppNotificationService {
    /**
     *  tradeDetail 會被先save  換成 save後
     * @param tradeDetail
     * @param merchantPid
     */
    void notifyApp(TradeDetail tradeDetail, Merchant merchantPid);

    /**
     * tradeDetail 會被先save  換成 save後
     * @param mchId
     * @param orderId
     */
    void notifyApp(String mchId,String orderId);

}
