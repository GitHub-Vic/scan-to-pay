package co.intella.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Service
public class RedisService {

    private final Logger LOGGER = LoggerFactory.getLogger(RedisService.class);

    @Resource
    private RedisTemplate redisTemplate;

    //set redis
    public void setRedis(String key, String value, int timeOut) {
        LOGGER.info("setRedis");
        redisTemplate.opsForValue().set(key, value, timeOut, TimeUnit.SECONDS);
        String redis = redisTemplate.opsForValue().get(key).toString();
        LOGGER.info("redis" + redis);
    }

    //get redis
    public String getRedis(String key) {
        LOGGER.info("getRedis :" + key);
        Object redis = redisTemplate.opsForValue().get(key);
        if (redis == null) {
            LOGGER.info("redis == null");
            return null;
        }
        LOGGER.info("redis :" + redis.toString());

        return redis.toString();
    }
}

