package co.intella.service;

import org.json.JSONArray;
import org.json.JSONObject;

public interface TokenService {
	public JSONArray getMerchantByTokenIdAndAccountId(String tokenId, String accountId) throws Exception;
	
	public JSONObject checkToken(String tokenId) throws Exception;
}
