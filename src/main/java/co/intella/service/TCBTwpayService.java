package co.intella.service;

import co.intella.model.RequestHeader;
import com.google.gson.JsonPrimitive;

public interface TCBTwpayService {
    String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;
}
