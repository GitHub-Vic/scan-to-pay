package co.intella.service;

import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import com.google.gson.JsonPrimitive;

public interface NewPiService {

    String doRequest(RequestHeader requestHeader, JsonPrimitive data) throws Exception;

    String getPaymentUrl(String amount, String shortId, String agent) throws Exception;

    void updateAuthorization(PaymentAccount paymentAccount);
}
