package co.intella.service;

import java.util.Map;

import co.intella.model.QrType7;

public interface QrType7Service {
	
	public Map<String,Object>saveQrType7(QrType7 qrType7) throws InterruptedException;
	
	public int getDemoInfoAmount(String shortId);

}
