package co.intella.service;

import java.util.Map;

import co.intella.response.GeneralResponse;

public interface GeneralService<T> {
	public GeneralResponse doService(T obj) throws Exception;
	
	Map<String, Object> doValidate(T obj) throws Exception;
}
