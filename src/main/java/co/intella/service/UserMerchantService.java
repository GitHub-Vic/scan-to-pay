package co.intella.service;

import java.util.Map;

import co.intella.domain.UserMerchantRequest;

public interface UserMerchantService {
	public Map<String, Object> getByUserId(String getByUserId) throws Exception;

	public Map<String, Object> insert(UserMerchantRequest userMerchant) throws Exception;

	public Map<String, Object> delete(UserMerchantRequest userMerchant) throws Exception;

	public Map<String, Object> update(UserMerchantRequest userMerchant) throws Exception;
}
