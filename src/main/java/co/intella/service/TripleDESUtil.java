package co.intella.service;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.Key;

/**
 * @author Alex
 */

public class TripleDESUtil {

    public static byte[] des3EncodeCBCNoPadding(SecretKey secretKey, byte[] keyiv, byte[] data) throws Exception {

        Cipher cipher = Cipher.getInstance("desede" + "/CBC/NoPadding");
        IvParameterSpec ips = new IvParameterSpec(keyiv);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, ips);
        byte[] bOut = cipher.doFinal(data);

        return bOut;
    }

    public static byte[] des3DecodeCBC(SecretKey key, byte[] keyiv, byte[] data) throws Exception {

        Cipher cipher = Cipher.getInstance("desede" + "/CBC/NoPadding");
        IvParameterSpec ips = new IvParameterSpec(keyiv);
        cipher.init(Cipher.DECRYPT_MODE, key, ips);
        byte[] bOut = cipher.doFinal(data);

        return bOut;

    }
    public static SecretKey convertTripleDESSecretKey(byte[] key) {
        return new SecretKeySpec(key, 0, key.length, "TripleDES");
    }


    public static String des3DecodeECB(byte[] key, byte[] data)
            throws Exception {
        Key deskey = null;
        DESedeKeySpec spec = new DESedeKeySpec(key);
        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("desede");
        deskey = keyfactory.generateSecret(spec);
        Cipher cipher = Cipher.getInstance("desede" + "/ECB/nopadding");
        cipher.init(Cipher.DECRYPT_MODE, deskey);
        byte[] bOut = cipher.doFinal(data);
        return new String(bOut, StandardCharsets.UTF_8);
    }

}
