package co.intella.service;

import co.intella.model.INT_TB_CancelTradeDetail;
import co.intella.model.TradeDetail;

import java.util.List;

public interface INT_TB_CancelTradeDetailService {

    INT_TB_CancelTradeDetail save (INT_TB_CancelTradeDetail cancelTradeDetail);

    List<INT_TB_CancelTradeDetail>  getFailList ();

    INT_TB_CancelTradeDetail getOne (TradeDetail tradeDetail);
}
