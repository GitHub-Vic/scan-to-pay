package co.intella.service;

import co.intella.model.DiscountTrade;

public interface DiscountTradeService {

    DiscountTrade save(DiscountTrade discountTrade);
}
