package co.intella.service;

import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.querydsl.core.types.Predicate;

import co.intella.domain.contratStore.MerchantAppLogin;
import co.intella.model.Merchant;
import co.intella.utility.JsapiParamMediator;

/**
 * @author Miles Wu
 */
public interface MerchantService {

    List<Merchant> listByHierarchy(String hierarchy);

    List<Merchant> queryWithLike(String accountId, String name);

    Merchant getOne(long id);

    Merchant getOne(String accountId);

    MerchantAppLogin getLoginMerchantByLoginId(String loginId);

    Merchant getOneByLoginId(String loginId);

    List<Merchant> listAll();

    Merchant create(Merchant merchant);

    void editPassword(Merchant merchant);

    Iterable<Merchant> findBy(Predicate predicate);

    Page findAll(Pageable pageable);

    long count(Predicate predicate);

    long count();

    boolean activateAccount(String accountId, String activateToken);

    Merchant save(Merchant merchant);

    void registerJsapiMediator(JsapiParamMediator jsapiParamMediator);

    JsapiParamMediator getJsapiMediator();
    
    Map<String, String> checkPin (String accountId, String pin) throws Exception;

    JSONObject getAppImg(String accountId);


}
