package co.intella.service;

import co.intella.domain.contratStore.CustomizePageRequest;
import co.intella.model.CaptureDetail;

import java.util.List;

/**
 * @author Andy Lin
 */
public interface CaptureDetailService {

    CaptureDetail settle(String merchantId, String method, String from, String to);

    List<CaptureDetail> save( List<CaptureDetail> list);

    CaptureDetail save(CaptureDetail captureDetail);

    CaptureDetail getOne(String id);

    CaptureDetail getOne(String mchId, String fromDate, String method);

    List<CaptureDetail> listByMerchantIdAndDate(String mchId, String date);

//    List<CaptureDetail> listByDate(String date);

    List<CaptureDetail> listByStatus(String status);

    List<CaptureDetail> listAll();

    List<CaptureDetail> pageList(CustomizePageRequest request);

    List<CaptureDetail> list(CustomizePageRequest request);

    Long pageCount(CustomizePageRequest request);
}
