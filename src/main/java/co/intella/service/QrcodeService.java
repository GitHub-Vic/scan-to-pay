package co.intella.service;

import co.intella.domain.contratStore.CustomizePageRequest;
import co.intella.exception.NoSuchMerchantException;
import co.intella.exception.OrderAlreadyExistedException;
import co.intella.model.QrcodeParameter;
import co.intella.utility.URLMediator;

import java.util.List;
import java.util.UUID;

/**
 * @author Alex
 */

public interface QrcodeService {

    QrcodeParameter getOne(UUID uuid);

    QrcodeParameter getOneByOrderId(String orderId);

    QrcodeParameter getOneByShortId(String shortId);

    QrcodeParameter getOriginOneByShortId(String shortId);

    QrcodeParameter getOneByShortIdAndMchId(String mchId, String shortId);

    QrcodeParameter modifyShortId(QrcodeParameter qrcodeParameter, String shortId);

    QrcodeParameter save(QrcodeParameter qrcodeParameter);

    List<QrcodeParameter> listAll(String mchId);

    void registerMediator(URLMediator urlMediator);

    String getURLMediator();

    List<QrcodeParameter> pageList(CustomizePageRequest request);

    long pageCount(CustomizePageRequest request);

    boolean checkQrcodeExpired(QrcodeParameter qrcodeParameter);

    String getOrderId(QrcodeParameter qrcodeParameter);

    String getRandomBody(QrcodeParameter qrcodeParameter);

    QrcodeParameter createQrcode(QrcodeParameter qrcodeParameter) throws NoSuchMerchantException, OrderAlreadyExistedException;

    void deleteQrcodeTemplate(QrcodeParameter qrcodeParameter);

    List<QrcodeParameter> qrcodeListByMchId(String MchId);
}


