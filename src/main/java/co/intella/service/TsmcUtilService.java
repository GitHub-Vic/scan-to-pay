package co.intella.service;

import co.intella.model.RequestHeader;
import com.google.gson.JsonObject;
import org.springframework.scheduling.annotation.Async;

import java.util.concurrent.Future;

/**
 * 單存用來異步票證交易
 */
public interface TsmcUtilService {

    @Async
    Future<String> asyncTicket(RequestHeader requestHeader, JsonObject requestData) throws Exception;
}
