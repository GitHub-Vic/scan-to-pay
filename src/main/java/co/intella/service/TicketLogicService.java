package co.intella.service;

import co.intella.exception.DeviceNotFoundException;
import co.intella.exception.InvalidDataFormatException;
import co.intella.model.IntTbAbnormalRule;
import co.intella.model.Merchant;
import co.intella.model.RequestHeader;
import co.intella.model.TaxiTransactionDetail;
import com.google.gson.JsonObject;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public interface TicketLogicService {

    //為了給登入APP用，故搬來這邊
    List<Long> ticketBankList = Arrays.asList(18L, 21L, 28L, 37L);

    List<String> ticketMethodList = Arrays.asList("31800", "32800", "32100", "33700");

    /**
     * 所有票證都共同入口
     *
     * @param header
     * @param requestData
     * @return
     */
    String doRequest(RequestHeader header, JsonObject requestData) throws Exception;


    /**
     * 驗證 並同時取得  PaymentAccount、Device
     *
     * @param requestHeader
     * @param requestData
     * @param merchant
     * @return Map<String, Object>
     * @throws DeviceNotFoundException
     * @throws InvalidDataFormatException
     */
    Map<String, Object> verification(RequestHeader requestHeader, JsonObject requestData, Merchant merchant) throws DeviceNotFoundException, InvalidDataFormatException;

    /**
     * 驗證營運規則，報表用
     *
     * @param startDate  運算日期(起)
     * @param endDate    運算日期(訖)
     * @param terminalId 運算車隊terminalId
     * @return List<TaxiTransactionDetail>
     */
    List<TaxiTransactionDetail> getAbnormalRuleMsgByReport(String startDate, String endDate, String terminalId);

    /**
     * 運算營運規則，以當天為基準
     *
     * @param abRList      驗證規則List
     * @param licensePlate 要驗證的車牌
     * @param cardId       要驗證的卡號
     * @return 錯誤信息字串或空字串
     */
    String getAbnormalRuleMsg(List<IntTbAbnormalRule> abRList, String licensePlate, String cardId);

    /**
     * 自動登入，原由 歐特儀那邊工程師跑光，凹我們幫他登入
     * @return
     */
    Boolean autoLogin();
}