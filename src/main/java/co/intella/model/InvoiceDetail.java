package co.intella.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * @author Alex
 */
@Entity(name = "InvoiceDetail")
public class InvoiceDetail {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid.hex")
    @Column(columnDefinition = "VARCHAR(32)")
    private String invoiceDetailRandomId;

    @ManyToOne
    @JoinColumn(name = "tradeDetailRandomId")
    private TradeDetail tradeDetailRandomId;

    @ManyToOne
    @JoinColumn(name = "refundDetailRandomId")
    private RefundDetail refundDetailRandomId;

    private String orderId;

    private double amount;

    private String createDate;

    public String getInvoiceDetailRandomId() {
        return invoiceDetailRandomId;
    }

    public void setInvoiceDetailRandomId(String invoiceDetailRandomId) {
        this.invoiceDetailRandomId = invoiceDetailRandomId;
    }

    public TradeDetail getTradeDetailRandomId() {
        return tradeDetailRandomId;
    }

    public void setTradeDetailRandomId(TradeDetail tradeDetailRandomId) {
        this.tradeDetailRandomId = tradeDetailRandomId;
    }

    public RefundDetail getRefundDetailRandomId() {
        return refundDetailRandomId;
    }

    public void setRefundDetailRandomId(RefundDetail refundDetailRandomId) {
        this.refundDetailRandomId = refundDetailRandomId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }


}
