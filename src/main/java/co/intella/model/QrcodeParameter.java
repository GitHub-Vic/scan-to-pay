package co.intella.model;

import com.google.gson.annotations.SerializedName;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

/**
 * @author Alex
 */
@Entity(name = "QrcodeParameter")
public class QrcodeParameter implements Cloneable, Serializable {

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(columnDefinition = "BINARY(16)")
	private UUID id;

	private String ver;

	private String method;

	private String mchId;

	@SerializedName("TimeExpire")
	private String timeExpire;

	@SerializedName("StoreOrderNo")
	private String storeOrderNo;

	@SerializedName("Body")
	private String body;

	private String feeType;

	@SerializedName("TotalFee")
	private double totalFee;

	@SerializedName("Detail")
	private String detail;

	private String codeType;

	private String shortId;

	@SerializedName("DeviceInfo")
	private String deviceInfo;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "parentId")
	private QrcodeParameter parentId;

	private int status;

	private String createDate;

	private String storeInfo;

	private String onSaleStart;

	private String onSaleEnd;

	private double onSaleTotalFee;

	private String customizedTemplate;
	
	@SerializedName("Cashier")
	private String cashier;

	@OneToMany(mappedBy = "qrcodeParameter", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Recipient> recipients;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getMchId() {
		return mchId;
	}

	public void setMchId(String mchId) {
		this.mchId = mchId;
	}

	public String getStoreOrderNo() {
		return storeOrderNo;
	}

	public void setStoreOrderNo(String storeOrderNo) {
		this.storeOrderNo = storeOrderNo;
	}

	public String getTimeExpire() {
		return timeExpire;
	}

	public void setTimeExpire(String timeExpire) {
		this.timeExpire = timeExpire;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getFeeType() {
		return feeType;
	}

	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}

	public double getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(double totalFee) {
		this.totalFee = totalFee;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getCodeType() {
		return codeType;
	}

	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}

	public String getShortId() {
		return shortId;
	}

	public void setShortId(String shortId) {
		this.shortId = shortId;
	}

	public QrcodeParameter getParentId() {
		return parentId;
	}

	public void setParentId(QrcodeParameter parentId) {
		this.parentId = parentId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public Set<Recipient> getRecipients() {
		return recipients;
	}

	public void setRecipients(Set<Recipient> recipients) {
		this.recipients = recipients;
	}

	public String getStoreInfo() {
		return storeInfo;
	}

	public void setStoreInfo(String storeInfo) {
		this.storeInfo = storeInfo;
	}

	public String getOnSaleStart() {
		return onSaleStart;
	}

	public void setOnSaleStart(String onSaleStart) {
		this.onSaleStart = onSaleStart;
	}

	public String getOnSaleEnd() {
		return onSaleEnd;
	}

	public void setOnSaleEnd(String onSaleEnd) {
		this.onSaleEnd = onSaleEnd;
	}

	public double getOnSaleTotalFee() {
		return onSaleTotalFee;
	}

	public void setOnSaleTotalFee(double onSaleTotalFee) {
		this.onSaleTotalFee = onSaleTotalFee;
	}

	public String getCustomizedTemplate() {
		return customizedTemplate;
	}

	public void setCustomizedTemplate(String customizedTemplate) {
		this.customizedTemplate = customizedTemplate;
	}

	public String getCashier() {
		return cashier;
	}

	public void setCashier(String cashier) {
		this.cashier = cashier;
	}
	
}
