package co.intella.model;


import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class DiscountMch {

    private long discountMchSeq;

    private DiscountCtrl dicountCtrl;

    private String accountId;

    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss")
    private Date startDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss")
    private Date endDate;

    public long getDiscountMchSeq() {
        return discountMchSeq;
    }

    public void setDiscountMchSeq(long discountMchSeq) {
        this.discountMchSeq = discountMchSeq;
    }

    public DiscountCtrl getDicountCtrl() {
        return dicountCtrl;
    }

    public void setDicountCtrl(DiscountCtrl dicountCtrl) {
        this.dicountCtrl = dicountCtrl;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "DiscountMch{" +
                "discountMchSeq=" + discountMchSeq +
                ", dicountCtrl=" + dicountCtrl +
                ", accountId='" + accountId + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
