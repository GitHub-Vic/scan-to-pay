package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QReserveDetail is a Querydsl query type for ReserveDetail
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QReserveDetail extends EntityPathBase<ReserveDetail> {

    private static final long serialVersionUID = 1145588195L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QReserveDetail reserveDetail = new QReserveDetail("reserveDetail");

    public final StringPath accountId = createString("accountId");

    public final NumberPath<Long> amount = createNumber("amount", Long.class);

    public final StringPath batchNumber = createString("batchNumber");

    public final StringPath cancelDate = createString("cancelDate");

    public final StringPath cancelStatus = createString("cancelStatus");

    public final StringPath cashier = createString("cashier");

    public final StringPath createTime = createString("createTime");

    public final StringPath deviceId = createString("deviceId");

    public final QDevice deviceRandomId;

    public final StringPath ezcTxnDate = createString("ezcTxnDate");

    public final StringPath id = createString("id");

    public final NumberPath<Integer> isAuto = createNumber("isAuto", Integer.class);

    public final StringPath method = createString("method");

    public final StringPath reserveOrderId = createString("reserveOrderId");

    public final StringPath rrn = createString("rrn");

    public final StringPath status = createString("status");

    public final DateTimePath<java.util.Date> sysTime = createDateTime("sysTime", java.util.Date.class);

    public final StringPath txParams = createString("txParams");

    public final StringPath userId = createString("userId");

    public QReserveDetail(String variable) {
        this(ReserveDetail.class, forVariable(variable), INITS);
    }

    public QReserveDetail(Path<? extends ReserveDetail> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QReserveDetail(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QReserveDetail(PathMetadata metadata, PathInits inits) {
        this(ReserveDetail.class, metadata, inits);
    }

    public QReserveDetail(Class<? extends ReserveDetail> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.deviceRandomId = inits.isInitialized("deviceRandomId") ? new QDevice(forProperty("deviceRandomId"), inits.get("deviceRandomId")) : null;
    }

}

