package co.intella.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * @author Miles Wu
 */
@Entity(name = "Merchant")
public class Merchant implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long merchantSeqId;

    private String name;

    private String email;

    private MerchantType type;

//    @ManyToOne
//    @JoinColumn(name = "parentMerchant")
//    private Merchant parentMerchant;
//

    //@JoinColumn(name="creator")
    //@ManyToOne(cascade={CascadeType.ALL})
    private Long creator;

    private String pin;

    private String accountId;

    private String comId;

    private String phone;

    private String signDate;

    private String status;

    private String paycodeToken;

    private String activateToken;

    private String refundPin;

    private String buttonText;

    private String buttonURL;

    @OneToMany(mappedBy = "merchant", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Recipient> recipients;

    @ManyToOne
    @JoinColumn(name = "saleId")
    private Sale saleId;

    private String notifyUrl;

    private String randomId;

    private String tradePin;

    private String loginId;

    private Integer accountType;
//
//    public Merchant getParentMerchant() {
//        return parentMerchant;
//    }
//
//    public void setParentMerchant(Merchant parentMerchant) {
//        this.parentMerchant = parentMerchant;
//    }

    private Integer storeType;

    private Integer franchiseNo;

    private Integer regularChainNo;

    private Integer assistNo;

    private String companyPhone;

    private String postalCode;

    private String startDate;

    private String endDate;

    private String storeCode;

    private String storeName;

    private String address;

    private String paymentBanner;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    private String companyName;


    private String parentMerchantAccount;

    private String brandName;

    private String salesDate;

    private String closeDate;

    private String openDate;

    private String mgtPermission;

    private String callBackDelay;
    /*
    private String companyName;
    private String companyAddress;
    private String companyPhone;
    private String companyContactName;
    private String companyContactMobile;
    private String companyContactEmail;
    private String principalNationality;
    private String principalName;
    private String principalIdentity;
    private String registryLicenseType;
    private String address;
    private String description;
    private String salesCategory;
    private String salesType;
    private String url;
    private String financialInstitution;
    private String bankName;
    private String bankBranch;
    private String bankAccountName;
    private String bankAccountId;
    private String fax;
    private String bankContactInfo;
    private String bankContactPhone;
    private String bankContactMobile;
    private String bankContactEmail;
    private String invoiceEmail;
    private String registrationForm;
    private String declarationOfSalesAndBusinessTaxByABusinessEntity;
    private String extension;
    private String taxId;
    */

    public long getMerchantSeqId() {
        return merchantSeqId;
    }

    public void setMerchantSeqId(long merchantSeqId) {
        this.merchantSeqId = merchantSeqId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public MerchantType getType() {
        return type;
    }

    public void setType(MerchantType type) {
        this.type = type;
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getComId() {
        return comId;
    }

    public void setComId(String comId) {
        this.comId = comId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSignDate() {
        return signDate;
    }

    public void setSignDate(String signDate) {
        this.signDate = signDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getPaycodeToken() {
        return paycodeToken;
    }

    public void setPaycodeToken(String paycodeToken) {
        this.paycodeToken = paycodeToken;
    }

    public String getActivateToken() {
        return activateToken;
    }

    public void setActivateToken(String activateToken) {
        this.activateToken = activateToken;
    }

    public Set<Recipient> getRecipients() {
        return recipients;
    }

    public void setRecipients(Set<Recipient> recipients) {
        this.recipients = recipients;
    }

    public String getRefundPin() {
        return refundPin;
    }

    public void setRefundPin(String refundPin) {
        this.refundPin = refundPin;
    }

    public Sale getSaleId() {
        return saleId;
    }

    public void setSaleId(Sale saleId) {
        this.saleId = saleId;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getRandomId() {
        return randomId;
    }

    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }

    public String getTradePin() {
        return tradePin;
    }

    public void setTradePin(String tradePin) {
        this.tradePin = tradePin;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }

    public Integer getStoreType() {
        return storeType;
    }

    public void setStoreType(Integer storeType) {
        this.storeType = storeType;
    }

    public Integer getFranchiseNo() {
        return franchiseNo;
    }

    public void setFranchiseNo(Integer franchiseNo) {
        this.franchiseNo = franchiseNo;
    }

    public Integer getRegularChainNo() {
        return regularChainNo;
    }

    public void setRegularChainNo(Integer regularChainNo) {
        this.regularChainNo = regularChainNo;
    }

    public Integer getAssistNo() {
        return assistNo;
    }

    public void setAssistNo(Integer assistNo) {
        this.assistNo = assistNo;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public String getParentMerchantAccount() {
        return parentMerchantAccount;
    }

    public void setParentMerchantAccount(String parentMerchantAccount) {
        this.parentMerchantAccount = parentMerchantAccount;
    }
    /*

    /*public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }




    /*
    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getCompanyContactName() {
        return companyContactName;
    }

    public void setCompanyContactName(String companyContactName) {
        this.companyContactName = companyContactName;
    }

    public String getCompanyContactMobile() {
        return companyContactMobile;
    }

    public void setCompanyContactMobile(String companyContactMobile) {
        this.companyContactMobile = companyContactMobile;
    }

    public String getCompanyContactEmail() {
        return companyContactEmail;
    }

    public void setCompanyContactEmail(String companyContactEmail) {
        this.companyContactEmail = companyContactEmail;
    }

    public String getPrincipalNationality() {
        return principalNationality;
    }

    public void setPrincipalNationality(String principalNationality) {
        this.principalNationality = principalNationality;
    }

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    public String getPrincipalIdentity() {
        return principalIdentity;
    }

    public void setPrincipalIdentity(String principalIdentity) {
        this.principalIdentity = principalIdentity;
    }

    public String getRegistryLicenseType() {
        return registryLicenseType;
    }

    public void setRegistryLicenseType(String registryLicenseType) {
        this.registryLicenseType = registryLicenseType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSalesCategory() {
        return salesCategory;
    }

    public void setSalesCategory(String salesCategory) {
        this.salesCategory = salesCategory;
    }

    public String getSalesType() {
        return salesType;
    }

    public void setSalesType(String salesType) {
        this.salesType = salesType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFinancialInstitution() {
        return financialInstitution;
    }

    public void setFinancialInstitution(String financialInstitution) {
        this.financialInstitution = financialInstitution;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(String bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getBankContactInfo() {
        return bankContactInfo;
    }

    public void setBankContactInfo(String bankContactInfo) {
        this.bankContactInfo = bankContactInfo;
    }

    public String getBankContactPhone() {
        return bankContactPhone;
    }

    public void setBankContactPhone(String bankContactPhone) {
        this.bankContactPhone = bankContactPhone;
    }

    public String getBankContactMobile() {
        return bankContactMobile;
    }

    public void setBankContactMobile(String bankContactMobile) {
        this.bankContactMobile = bankContactMobile;
    }

    public String getBankContactEmail() {
        return bankContactEmail;
    }

    public void setBankContactEmail(String bankContactEmail) {
        this.bankContactEmail = bankContactEmail;
    }

    public String getInvoiceEmail() {
        return invoiceEmail;
    }

    public void setInvoiceEmail(String invoiceEmail) {
        this.invoiceEmail = invoiceEmail;
    }

    public String getRegistrationForm() {
        return registrationForm;
    }

    public void setRegistrationForm(String registrationForm) {
        this.registrationForm = registrationForm;
    }

    public String getDeclarationOfSalesAndBusinessTaxByABusinessEntity() {
        return declarationOfSalesAndBusinessTaxByABusinessEntity;
    }

    public void setDeclarationOfSalesAndBusinessTaxByABusinessEntity(String declarationOfSalesAndBusinessTaxByABusinessEntity) {
        this.declarationOfSalesAndBusinessTaxByABusinessEntity = declarationOfSalesAndBusinessTaxByABusinessEntity;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }
    */

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getSalesDate() {
        return salesDate;
    }

    public void setSalesDate(String salesDate) {
        this.salesDate = salesDate;
    }

    public String getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    public String getButtonURL() {
        return buttonURL;
    }

    public void setButtonURL(String buttonURL) {
        this.buttonURL = buttonURL;
    }

    public String getMgtPermission() {
        return mgtPermission;
    }

    public void setMgtPermission(String mgtPermission) {
        this.mgtPermission = mgtPermission;
    }

    public String getPaymentBanner() {
        return paymentBanner;
    }

    public void setPaymentBanner(String paymentBanner) {
        this.paymentBanner = paymentBanner;
    }

    public String getCallBackDelay() {
        return callBackDelay;
    }

    public void setCallBackDelay(String callBackDelay) {
        this.callBackDelay = callBackDelay;
    }
}
