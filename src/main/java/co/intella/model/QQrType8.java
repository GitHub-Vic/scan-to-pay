package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QQrType8 is a Querydsl query type for QrType8
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QQrType8 extends EntityPathBase<QrType8> {

    private static final long serialVersionUID = 973847763L;

    public static final QQrType8 qrType8 = new QQrType8("qrType8");

    public final StringPath accountId = createString("accountId");

    public final NumberPath<Integer> amount = createNumber("amount", Integer.class);

    public final StringPath mobile = createString("mobile");

    public final StringPath name = createString("name");

    public final StringPath orderId = createString("orderId");

    public final NumberPath<Integer> qrType8Sql = createNumber("qrType8Sql", Integer.class);

    public final StringPath remark = createString("remark");

    public final StringPath shortId = createString("shortId");

    public final DateTimePath<java.sql.Timestamp> sysTime = createDateTime("sysTime", java.sql.Timestamp.class);

    public QQrType8(String variable) {
        super(QrType8.class, forVariable(variable));
    }

    public QQrType8(Path<? extends QrType8> path) {
        super(path.getType(), path.getMetadata());
    }

    public QQrType8(PathMetadata metadata) {
        super(QrType8.class, metadata);
    }

}

