package co.intella.model;

/**
 * @author Miles Wu
 */
public enum MerchantType {

    MAIN(1), BRANCH(2), DISTRIBUTOR(3), DISTRBUTOR_BRANCH(4);

    private int value;

    private MerchantType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
