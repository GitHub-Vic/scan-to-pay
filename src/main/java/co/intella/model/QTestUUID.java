package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTestUUID is a Querydsl query type for TestUUID
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTestUUID extends EntityPathBase<TestUUID> {

    private static final long serialVersionUID = -1007573033L;

    public static final QTestUUID testUUID = new QTestUUID("testUUID");

    public final StringPath com = createString("com");

    public final StringPath id = createString("id");

    public final DateTimePath<org.joda.time.DateTime> ts = createDateTime("ts", org.joda.time.DateTime.class);

    public QTestUUID(String variable) {
        super(TestUUID.class, forVariable(variable));
    }

    public QTestUUID(Path<? extends TestUUID> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTestUUID(PathMetadata metadata) {
        super(TestUUID.class, metadata);
    }

}

