package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QRefundDetail is a Querydsl query type for RefundDetail
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QRefundDetail extends EntityPathBase<RefundDetail> {

    private static final long serialVersionUID = -172219021L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QRefundDetail refundDetail = new QRefundDetail("refundDetail");

    public final StringPath accountId = createString("accountId");

    public final NumberPath<Long> amount = createNumber("amount", Long.class);

    public final StringPath batchNo = createString("batchNo");

    public final StringPath cashier = createString("cashier");

    public final StringPath createTime = createString("createTime");

    public final QDevice deviceRandomId;

    public final BooleanPath ezcCancel = createBoolean("ezcCancel");

    public final StringPath ezcTxnDate = createString("ezcTxnDate");

    public final BooleanPath isPartialRefund = createBoolean("isPartialRefund");

    public final StringPath method = createString("method");

    public final StringPath orderId = createString("orderId");

    public final StringPath platformRefundDate = createString("platformRefundDate");

    public final StringPath reason = createString("reason");

    public final BooleanPath reconciliation = createBoolean("reconciliation");

    public final StringPath refundDetailRandomId = createString("refundDetailRandomId");

    public final StringPath rrn = createString("rrn");

    public final StringPath status = createString("status");

    public final StringPath storeRefundId = createString("storeRefundId");

    public final StringPath systemRefundId = createString("systemRefundId");

    public final DateTimePath<java.util.Date> sysTime = createDateTime("sysTime", java.util.Date.class);

    public final QTradeDetail tradeDetailRandomId;

    public final StringPath txParams = createString("txParams");

    public QRefundDetail(String variable) {
        this(RefundDetail.class, forVariable(variable), INITS);
    }

    public QRefundDetail(Path<? extends RefundDetail> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QRefundDetail(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QRefundDetail(PathMetadata metadata, PathInits inits) {
        this(RefundDetail.class, metadata, inits);
    }

    public QRefundDetail(Class<? extends RefundDetail> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.deviceRandomId = inits.isInitialized("deviceRandomId") ? new QDevice(forProperty("deviceRandomId"), inits.get("deviceRandomId")) : null;
        this.tradeDetailRandomId = inits.isInitialized("tradeDetailRandomId") ? new QTradeDetail(forProperty("tradeDetailRandomId"), inits.get("tradeDetailRandomId")) : null;
    }

}

