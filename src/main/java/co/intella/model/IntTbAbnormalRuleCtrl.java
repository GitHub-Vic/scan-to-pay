package co.intella.model;

import java.util.Date;

public class IntTbAbnormalRuleCtrl {

    private Integer arcSeq;

    private Integer merchantSeqId;

    private String location;

    private Integer version;

    private Date effDate;

    private String crUser;

    private Date crDate;

    private String userStamp;

    private Date dateStamp;

    public Integer getArcSeq() {
        return arcSeq;
    }

    public void setArcSeq(Integer arcSeq) {
        this.arcSeq = arcSeq;
    }

    public Integer getMerchantSeqId() {
        return merchantSeqId;
    }

    public void setMerchantSeqId(Integer merchantSeqId) {
        this.merchantSeqId = merchantSeqId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Date getEffDate() {
        return effDate;
    }

    public void setEffDate(Date effDate) {
        this.effDate = effDate;
    }

    public String getCrUser() {
        return crUser;
    }

    public void setCrUser(String crUser) {
        this.crUser = crUser;
    }

    public Date getCrDate() {
        return crDate;
    }

    public void setCrDate(Date crDate) {
        this.crDate = crDate;
    }

    public String getUserStamp() {
        return userStamp;
    }

    public void setUserStamp(String userStamp) {
        this.userStamp = userStamp;
    }

    public Date getDateStamp() {
        return dateStamp;
    }

    public void setDateStamp(Date dateStamp) {
        this.dateStamp = dateStamp;
    }
}
