package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QQrcodeParameter is a Querydsl query type for QrcodeParameter
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QQrcodeParameter extends EntityPathBase<QrcodeParameter> {

    private static final long serialVersionUID = -1544269839L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QQrcodeParameter qrcodeParameter = new QQrcodeParameter("qrcodeParameter");

    public final StringPath body = createString("body");

    public final StringPath cashier = createString("cashier");

    public final StringPath codeType = createString("codeType");

    public final StringPath createDate = createString("createDate");

    public final StringPath customizedTemplate = createString("customizedTemplate");

    public final StringPath detail = createString("detail");

    public final StringPath deviceInfo = createString("deviceInfo");

    public final StringPath feeType = createString("feeType");

    public final ComparablePath<java.util.UUID> id = createComparable("id", java.util.UUID.class);

    public final StringPath mchId = createString("mchId");

    public final StringPath method = createString("method");

    public final StringPath onSaleEnd = createString("onSaleEnd");

    public final StringPath onSaleStart = createString("onSaleStart");

    public final NumberPath<Double> onSaleTotalFee = createNumber("onSaleTotalFee", Double.class);

    public final QQrcodeParameter parentId;

    public final SetPath<Recipient, QRecipient> recipients = this.<Recipient, QRecipient>createSet("recipients", Recipient.class, QRecipient.class, PathInits.DIRECT2);

    public final StringPath shortId = createString("shortId");

    public final NumberPath<Integer> status = createNumber("status", Integer.class);

    public final StringPath storeInfo = createString("storeInfo");

    public final StringPath storeOrderNo = createString("storeOrderNo");

    public final StringPath timeExpire = createString("timeExpire");

    public final NumberPath<Double> totalFee = createNumber("totalFee", Double.class);

    public final StringPath ver = createString("ver");

    public QQrcodeParameter(String variable) {
        this(QrcodeParameter.class, forVariable(variable), INITS);
    }

    public QQrcodeParameter(Path<? extends QrcodeParameter> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QQrcodeParameter(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QQrcodeParameter(PathMetadata metadata, PathInits inits) {
        this(QrcodeParameter.class, metadata, inits);
    }

    public QQrcodeParameter(Class<? extends QrcodeParameter> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.parentId = inits.isInitialized("parentId") ? new QQrcodeParameter(forProperty("parentId"), inits.get("parentId")) : null;
    }

}

