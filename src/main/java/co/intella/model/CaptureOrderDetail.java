package co.intella.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * @author Andy Lin
 */
@Entity(name = "CaptureOrderDetail")
public class CaptureOrderDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "tradeDetailRandomId")
    private TradeDetail tradeDetail;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "captureDetailRandomId")
    private CaptureDetail captureDetail;

    private String storeOrderNo;

    private String merchantId;

    private String method;

    private long totalFee;

    private String captureDate;

    private String status;

    private String createTime;

    private String tradeDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CaptureDetail getCaptureDetail() {
        return captureDetail;
    }

    public void setCaptureDetail(CaptureDetail captureDetail) {
        this.captureDetail = captureDetail;
    }

    public TradeDetail getTradeDetail() {
        return tradeDetail;
    }

    public void setTradeDetail(TradeDetail tradeDetail) {
        this.tradeDetail = tradeDetail;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public long getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(long totalFee) {
        this.totalFee = totalFee;
    }

    public String getCaptureDate() {
        return captureDate;
    }

    public void setCaptureDate(String captureDate) {
        this.captureDate = captureDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }
}
