package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QINT_TB_CancelTradeDetail is a Querydsl query type for INT_TB_CancelTradeDetail
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QINT_TB_CancelTradeDetail extends EntityPathBase<INT_TB_CancelTradeDetail> {

    private static final long serialVersionUID = 1811805988L;

    public static final QINT_TB_CancelTradeDetail iNT_TB_CancelTradeDetail = new QINT_TB_CancelTradeDetail("iNT_TB_CancelTradeDetail");

    public final StringPath accountId = createString("accountId");

    public final StringPath cancelStatus = createString("cancelStatus");

    public final DateTimePath<java.util.Date> crDate = createDateTime("crDate", java.util.Date.class);

    public final DateTimePath<java.util.Date> dateStamp = createDateTime("dateStamp", java.util.Date.class);

    public final StringPath method = createString("method");

    public final StringPath orderId = createString("orderId");

    public final StringPath request = createString("request");

    public final StringPath response = createString("response");

    public QINT_TB_CancelTradeDetail(String variable) {
        super(INT_TB_CancelTradeDetail.class, forVariable(variable));
    }

    public QINT_TB_CancelTradeDetail(Path<? extends INT_TB_CancelTradeDetail> path) {
        super(path.getType(), path.getMetadata());
    }

    public QINT_TB_CancelTradeDetail(PathMetadata metadata) {
        super(INT_TB_CancelTradeDetail.class, metadata);
    }

}

