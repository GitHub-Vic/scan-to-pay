package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMerchantResetToken is a Querydsl query type for MerchantResetToken
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMerchantResetToken extends EntityPathBase<MerchantResetToken> {

    private static final long serialVersionUID = 1504648668L;

    public static final QMerchantResetToken merchantResetToken = new QMerchantResetToken("merchantResetToken");

    public final StringPath accountId = createString("accountId");

    public final BooleanPath isUsed = createBoolean("isUsed");

    public final StringPath resetTimeout = createString("resetTimeout");

    public final StringPath resetToken = createString("resetToken");

    public final NumberPath<Long> seq = createNumber("seq", Long.class);

    public QMerchantResetToken(String variable) {
        super(MerchantResetToken.class, forVariable(variable));
    }

    public QMerchantResetToken(Path<? extends MerchantResetToken> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMerchantResetToken(PathMetadata metadata) {
        super(MerchantResetToken.class, metadata);
    }

}

