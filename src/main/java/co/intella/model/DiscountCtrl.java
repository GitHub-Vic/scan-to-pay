package co.intella.model;


import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

public class DiscountCtrl {

    private long dicountCtrlSeq;

    private String promotionCode;

    private String promotionName;

    private String methods;

    private String discountCode;

    private BigDecimal budget;

    private BigDecimal grandTotal;

    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss")
    private Date startDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss")
    private Date endDate;

    public long getDicountCtrlSeq() {
        return dicountCtrlSeq;
    }

    public void setDicountCtrlSeq(long dicountCtrlSeq) {
        this.dicountCtrlSeq = dicountCtrlSeq;
    }

    public String getPromotionCode() {
        return promotionCode;
    }

    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public BigDecimal getBudget() {
        return budget;
    }

    public void setBudget(BigDecimal budget) {
        this.budget = budget;
    }

    public BigDecimal getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(BigDecimal grandTotal) {
        this.grandTotal = grandTotal;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getMethods() {
        return methods;
    }

    public void setMethods(String methods) {
        this.methods = methods;
    }

    @Override
    public String toString() {
        return "DiscountCtrl{" +
                "dicountCtrlSeq=" + dicountCtrlSeq +
                ", promotionCode='" + promotionCode + '\'' +
                ", promotionName='" + promotionName + '\'' +
                ", methods='" + methods + '\'' +
                ", discountCode='" + discountCode + '\'' +
                ", budget=" + budget +
                ", grandTotal=" + grandTotal +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
