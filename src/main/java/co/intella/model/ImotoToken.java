package co.intella.model;

public class ImotoToken {

    private long imotoTokenSeq;
    private long merchantSeqId;
    private String tokenInternalCode;
    private String tokenOutercode;
    private String shortId;
    private String effective;

    public long getImotoTokenSeq() {
        return imotoTokenSeq;
    }

    public void setImotoTokenSeq(long imotoTokenSeq) {
        this.imotoTokenSeq = imotoTokenSeq;
    }

    public long getMerchantSeqId() {
        return merchantSeqId;
    }

    public void setMerchantSeqId(long merchantSeqId) {
        this.merchantSeqId = merchantSeqId;
    }

    public String getTokenInternalCode() {
        return tokenInternalCode;
    }

    public void setTokenInternalCode(String tokenInternalCode) {
        this.tokenInternalCode = tokenInternalCode;
    }

    public String getTokenOutercode() {
        return tokenOutercode;
    }

    public void setTokenOutercode(String tokenOutercode) {
        this.tokenOutercode = tokenOutercode;
    }

    public String getShortId() {
        return shortId;
    }

    public void setShortId(String shortId) {
        this.shortId = shortId;
    }

    public String getEffective() {
        return effective;
    }

    public void setEffective(String effective) {
        this.effective = effective;
    }
}
