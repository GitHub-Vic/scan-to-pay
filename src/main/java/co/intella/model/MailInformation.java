package co.intella.model;

/**
 * @author Alex
 */

public class MailInformation {

    private String[] mailTo;

    private String subject;

    public String[] getMailTo() {
        return mailTo;
    }

    public void setMailTo(String[] mailTo) {
        this.mailTo = mailTo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

}
