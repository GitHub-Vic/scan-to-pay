package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QTicketHistory is a Querydsl query type for TicketHistory
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTicketHistory extends EntityPathBase<TicketHistory> {

    private static final long serialVersionUID = -1776647778L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QTicketHistory ticketHistory = new QTicketHistory("ticketHistory");

    public final EnumPath<co.intella.utility.WorkflowAction> action = createEnum("action", co.intella.utility.WorkflowAction.class);

    public final QSale actorId;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath message = createString("message");

    public final QTicket ticketId;

    public QTicketHistory(String variable) {
        this(TicketHistory.class, forVariable(variable), INITS);
    }

    public QTicketHistory(Path<? extends TicketHistory> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QTicketHistory(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QTicketHistory(PathMetadata metadata, PathInits inits) {
        this(TicketHistory.class, metadata, inits);
    }

    public QTicketHistory(Class<? extends TicketHistory> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.actorId = inits.isInitialized("actorId") ? new QSale(forProperty("actorId"), inits.get("actorId")) : null;
        this.ticketId = inits.isInitialized("ticketId") ? new QTicket(forProperty("ticketId"), inits.get("ticketId")) : null;
    }

}

