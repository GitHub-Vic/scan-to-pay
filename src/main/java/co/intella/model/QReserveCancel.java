package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QReserveCancel is a Querydsl query type for ReserveCancel
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QReserveCancel extends EntityPathBase<ReserveCancel> {

    private static final long serialVersionUID = 1113088012L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QReserveCancel reserveCancel = new QReserveCancel("reserveCancel");

    public final StringPath accountId = createString("accountId");

    public final NumberPath<Long> amount = createNumber("amount", Long.class);

    public final StringPath batchNo = createString("batchNo");

    public final StringPath cashier = createString("cashier");

    public final StringPath createTime = createString("createTime");

    public final StringPath deviceId = createString("deviceId");

    public final QDevice deviceRandomId;

    public final StringPath ezcTxnDate = createString("ezcTxnDate");

    public final StringPath id = createString("id");

    public final StringPath method = createString("method");

    public final StringPath orderId = createString("orderId");

    public final StringPath platformCancelDate = createString("platformCancelDate");

    public final StringPath reason = createString("reason");

    public final BooleanPath reconciliation = createBoolean("reconciliation");

    public final QReserveDetail reserveDetail;

    public final StringPath rrn = createString("rrn");

    public final StringPath status = createString("status");

    public final StringPath txParams = createString("txParams");

    public final StringPath userId = createString("userId");

    public QReserveCancel(String variable) {
        this(ReserveCancel.class, forVariable(variable), INITS);
    }

    public QReserveCancel(Path<? extends ReserveCancel> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QReserveCancel(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QReserveCancel(PathMetadata metadata, PathInits inits) {
        this(ReserveCancel.class, metadata, inits);
    }

    public QReserveCancel(Class<? extends ReserveCancel> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.deviceRandomId = inits.isInitialized("deviceRandomId") ? new QDevice(forProperty("deviceRandomId"), inits.get("deviceRandomId")) : null;
        this.reserveDetail = inits.isInitialized("reserveDetail") ? new QReserveDetail(forProperty("reserveDetail"), inits.get("reserveDetail")) : null;
    }

}

