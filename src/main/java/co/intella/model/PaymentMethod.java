package co.intella.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Austin
 */
@Entity(name = "PaymentMethod")
public class PaymentMethod implements Cloneable, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String method;
	private Integer paymentAccountBankId;
	private String name;

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Integer getPaymentAccountBankId() {
		return paymentAccountBankId;
	}

	public void setPaymentAccountBankId(Integer paymentAccountBankId) {
		this.paymentAccountBankId = paymentAccountBankId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
