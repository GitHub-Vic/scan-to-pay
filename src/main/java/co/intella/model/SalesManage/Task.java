package co.intella.model.SalesManage;

import java.beans.IntrospectionException;
import java.util.List;

/**
 * @author Alex
 */

public class Task {

    private List<String> status;

    private Integer action;

    private Integer to;

    private List<String> assignment;

    public List<String> getStatus() {
        return status;
    }

    public void setStatus(List<String> status) {
        this.status = status;
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }

    public List<String> getAssignment() {
        return assignment;
    }

    public void setAssignment(List<String> assignment) {
        this.assignment = assignment;
    }
}
