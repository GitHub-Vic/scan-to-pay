package co.intella.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ImotoTokenLog {

    private long imotoTokenLogSeq;
    private long imotoTokenSeq;
    private String tokenOutercode;
    private long amount;
    private String phoneNum;
    private String promoCode;
    private String promoCodeValid;

    @JsonFormat( pattern="yyyy-MM-dd HH:mm:ss")
    private Date enterTime;
    @JsonFormat( pattern="yyyy-MM-dd HH:mm:ss")
    private Date exitTime;
    @JsonFormat( pattern="yyyy-MM-dd HH:mm:ss")
    private Date lastExitTime;
    @JsonFormat( pattern="yyyy-MM-dd HH:mm:ss")
    private Date crDate;

    public long getImotoTokenLogSeq() {
        return imotoTokenLogSeq;
    }

    public void setImotoTokenLogSeq(long imotoTokenLogSeq) {
        this.imotoTokenLogSeq = imotoTokenLogSeq;
    }

    public long getImotoTokenSeq() {
        return imotoTokenSeq;
    }

    public void setImotoTokenSeq(long imotoTokenSeq) {
        this.imotoTokenSeq = imotoTokenSeq;
    }

    public String getTokenOutercode() {
        return tokenOutercode;
    }

    public void setTokenOutercode(String tokenOutercode) {
        this.tokenOutercode = tokenOutercode;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoCodeValid() {
        return promoCodeValid;
    }

    public void setPromoCodeValid(String promoCodeValid) {
        this.promoCodeValid = promoCodeValid;
    }

    public Date getEnterTime() {
        return enterTime;
    }

    public void setEnterTime(Date enterTime) {
        this.enterTime = enterTime;
    }

    public Date getExitTime() {
        return exitTime;
    }

    public void setExitTime(Date exitTime) {
        this.exitTime = exitTime;
    }

    public Date getLastExitTime() {
        return lastExitTime;
    }

    public void setLastExitTime(Date lastExitTime) {
        this.lastExitTime = lastExitTime;
    }

    public Date getCrDate() {
        return crDate;
    }

    public void setCrDate(Date crDate) {
        this.crDate = crDate;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }
}
