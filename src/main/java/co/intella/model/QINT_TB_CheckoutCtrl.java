package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QINT_TB_CheckoutCtrl is a Querydsl query type for INT_TB_CheckoutCtrl
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QINT_TB_CheckoutCtrl extends EntityPathBase<INT_TB_CheckoutCtrl> {

    private static final long serialVersionUID = 1463765000L;

    public static final QINT_TB_CheckoutCtrl iNT_TB_CheckoutCtrl = new QINT_TB_CheckoutCtrl("iNT_TB_CheckoutCtrl");

    public final StringPath accountId = createString("accountId");

    public final StringPath batchNo = createString("batchNo");

    public final NumberPath<Long> ccSeqId = createNumber("ccSeqId", Long.class);

    public final StringPath comment = createString("comment");

    public final DateTimePath<java.util.Date> crDate = createDateTime("crDate", java.util.Date.class);

    public final DateTimePath<java.util.Date> dateStamp = createDateTime("dateStamp", java.util.Date.class);

    public final StringPath method = createString("method");

    public final StringPath ownerDeviceId = createString("ownerDeviceId");

    public final NumberPath<Long> refundAmount = createNumber("refundAmount", Long.class);

    public final NumberPath<Long> reserveAmount = createNumber("reserveAmount", Long.class);

    public final NumberPath<Long> reserveCancelAmount = createNumber("reserveCancelAmount", Long.class);

    public final StringPath status = createString("status");

    public final NumberPath<Long> tradeAmount = createNumber("tradeAmount", Long.class);

    public final StringPath type = createString("type");

    public QINT_TB_CheckoutCtrl(String variable) {
        super(INT_TB_CheckoutCtrl.class, forVariable(variable));
    }

    public QINT_TB_CheckoutCtrl(Path<? extends INT_TB_CheckoutCtrl> path) {
        super(path.getType(), path.getMetadata());
    }

    public QINT_TB_CheckoutCtrl(PathMetadata metadata) {
        super(INT_TB_CheckoutCtrl.class, metadata);
    }

}

