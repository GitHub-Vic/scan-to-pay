package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QRecipient is a Querydsl query type for Recipient
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QRecipient extends EntityPathBase<Recipient> {

    private static final long serialVersionUID = 840397519L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QRecipient recipient = new QRecipient("recipient");

    public final StringPath apnsToken = createString("apnsToken");

    public final StringPath appId = createString("appId");

    public final DateTimePath<java.util.Date> crDate = createDateTime("crDate", java.util.Date.class);

    public final DateTimePath<java.util.Date> dateStamp = createDateTime("dateStamp", java.util.Date.class);

    public final ComparablePath<java.util.UUID> id = createComparable("id", java.util.UUID.class);

    public final QMerchant merchant;

    public final QQrcodeParameter qrcodeParameter;

    public final StringPath serialId = createString("serialId");

    public final StringPath userId = createString("userId");

    public QRecipient(String variable) {
        this(Recipient.class, forVariable(variable), INITS);
    }

    public QRecipient(Path<? extends Recipient> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QRecipient(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QRecipient(PathMetadata metadata, PathInits inits) {
        this(Recipient.class, metadata, inits);
    }

    public QRecipient(Class<? extends Recipient> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.merchant = inits.isInitialized("merchant") ? new QMerchant(forProperty("merchant"), inits.get("merchant")) : null;
        this.qrcodeParameter = inits.isInitialized("qrcodeParameter") ? new QQrcodeParameter(forProperty("qrcodeParameter"), inits.get("qrcodeParameter")) : null;
    }

}

