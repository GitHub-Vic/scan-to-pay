package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QConsumerToken is a Querydsl query type for ConsumerToken
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QConsumerToken extends EntityPathBase<ConsumerToken> {

    private static final long serialVersionUID = 1854408025L;

    public static final QConsumerToken consumerToken = new QConsumerToken("consumerToken");

    public final StringPath accountId = createString("accountId");

    public final StringPath algorithm = createString("algorithm");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath keys = createString("keys");

    public final StringPath token = createString("token");

    public QConsumerToken(String variable) {
        super(ConsumerToken.class, forVariable(variable));
    }

    public QConsumerToken(Path<? extends ConsumerToken> path) {
        super(path.getType(), path.getMetadata());
    }

    public QConsumerToken(PathMetadata metadata) {
        super(ConsumerToken.class, metadata);
    }

}

