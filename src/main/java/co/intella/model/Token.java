package co.intella.model;

import java.util.Date;

public class Token {
	private String tokenId;
	private String oldTokenId;
	private String refreshToken;
	private Date lastTouchDate;
	private String userId;
	public String getTokenId() {
		return tokenId;
	}
	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
	
	public String getOldTokenId() {
		return oldTokenId;
	}
	public void setOldTokenId(String oldTokenId) {
		this.oldTokenId = oldTokenId;
	}
	public String getRefreshToken() {
		return refreshToken;
	}
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	public Date getLastTouchDate() {
		return lastTouchDate;
	}
	public void setLastTouchDate(Date lastTouchDate) {
		this.lastTouchDate = lastTouchDate;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
}
