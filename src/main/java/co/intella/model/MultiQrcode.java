package co.intella.model;

public class MultiQrcode {
    private String shortId;
    private String totalFee;
    private String body;
    private String detail;
    private String mergeShortId;
    private String payerId;
	public String getShortId() {
		return shortId;
	}
	public void setShortId(String shortId) {
		this.shortId = shortId;
	}
	public String getTotalFee() {
		return totalFee;
	}
	public void setTotalFee(String totalFee) {
		this.totalFee = totalFee;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getMergeShortId() {
		return mergeShortId;
	}
	public void setMergeShortId(String mergeShortId) {
		this.mergeShortId = mergeShortId;
	}
	public String getPayerId() {
		return payerId;
	}
	public void setPayerId(String payerId) {
		this.payerId = payerId;
	}
	@Override
	public String toString() {
		return "MultiQRCode [shortId=" + shortId + ", totalFee=" + totalFee + ", body=" + body + ", detail=" + detail
				+ ", mergeShortId=" + mergeShortId + "]";
	}	
}
