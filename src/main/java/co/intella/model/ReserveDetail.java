package co.intella.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.GenericGenerator;
import co.intella.model.Device;

/**
 * @author Miles
 */
@Entity(name = "ReserveDetail")
public class ReserveDetail {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid.hex")
    @Column(columnDefinition = "VARCHAR(32)")
    private String id;
    
    @ManyToOne
    @JoinColumn(name = "deviceRandomId")
    private Device deviceRandomId;

    private String deviceId;

    private String reserveOrderId;

    private long amount;

    private String accountId;

    private String method;

    private String cashier;

    private String createTime;

    private Date sysTime;

    private String batchNumber;

    private String status;

    private String userId;

    private String rrn;

    private String ezcTxnDate;

    private String cancelStatus;

    private String cancelDate;

    private int isAuto;

    private String txParams;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Device getDeviceRandomId() {
      return deviceRandomId;
    }

    public void setDeviceRandomId(Device deviceRandomId) {
      this.deviceRandomId = deviceRandomId;
    }
    
    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getReserveOrderId() {
        return reserveOrderId;
    }

    public void setReserveOrderId(String reserveOrderId) {
        this.reserveOrderId = reserveOrderId;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getCashier() {
        return cashier;
    }

    public void setCashier(String cashier) {
        this.cashier = cashier;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Date getSysTime() {
        return sysTime;
    }

    public void setSysTime(Date sysTime) {
        this.sysTime = sysTime;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getEzcTxnDate() {
        return ezcTxnDate;
    }

    public void setEzcTxnDate(String ezcTxnDate) {
        this.ezcTxnDate = ezcTxnDate;
    }

    public String getCancelStatus() {
        return cancelStatus;
    }

    public void setCancelStatus(String cancelStatus) {
        this.cancelStatus = cancelStatus;
    }

    public String getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(String cancelDate) {
        this.cancelDate = cancelDate;
    }

    public int getIsAuto() {
        return isAuto;
    }

    public void setIsAuto(int isAuto) {
        this.isAuto = isAuto;
    }

    public String getTxParams() {
        return txParams;
    }

    public void setTxParams(String txParams) {
        this.txParams = txParams;
    }
}
