package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCaptureOrderDetail is a Querydsl query type for CaptureOrderDetail
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCaptureOrderDetail extends EntityPathBase<CaptureOrderDetail> {

    private static final long serialVersionUID = -943686109L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCaptureOrderDetail captureOrderDetail = new QCaptureOrderDetail("captureOrderDetail");

    public final StringPath captureDate = createString("captureDate");

    public final QCaptureDetail captureDetail;

    public final StringPath createTime = createString("createTime");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath merchantId = createString("merchantId");

    public final StringPath method = createString("method");

    public final StringPath status = createString("status");

    public final StringPath storeOrderNo = createString("storeOrderNo");

    public final NumberPath<Long> totalFee = createNumber("totalFee", Long.class);

    public final StringPath tradeDate = createString("tradeDate");

    public final QTradeDetail tradeDetail;

    public QCaptureOrderDetail(String variable) {
        this(CaptureOrderDetail.class, forVariable(variable), INITS);
    }

    public QCaptureOrderDetail(Path<? extends CaptureOrderDetail> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QCaptureOrderDetail(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QCaptureOrderDetail(PathMetadata metadata, PathInits inits) {
        this(CaptureOrderDetail.class, metadata, inits);
    }

    public QCaptureOrderDetail(Class<? extends CaptureOrderDetail> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.captureDetail = inits.isInitialized("captureDetail") ? new QCaptureDetail(forProperty("captureDetail")) : null;
        this.tradeDetail = inits.isInitialized("tradeDetail") ? new QTradeDetail(forProperty("tradeDetail"), inits.get("tradeDetail")) : null;
    }

}

