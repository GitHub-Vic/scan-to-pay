package co.intella.model;

public class User {
	private String userId;
	private String accountId;

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	@Override
	public String toString() {
		return "User{" +
				"userId='" + userId + '\'' +
				", accountId='" + accountId + '\'' +
				'}';
	}
}
