package co.intella.model;

import co.intella.utility.WorkflowAction;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

/**
 * @author Alex
 */
@Entity(name = "TicketHistory")
public class TicketHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "ticketId")
    @JsonManagedReference
    private Ticket ticketId;

    @ManyToOne
    @JoinColumn(name = "actorId")
    @JsonManagedReference
    private Sale actorId;

    private WorkflowAction action;

    private String message;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Ticket getTicketId() {
        return ticketId;
    }

    public void setTicketId(Ticket ticketId) {
        this.ticketId = ticketId;
    }

    public Sale getActorId() {
        return actorId;
    }

    public void setActorId(Sale actorId) {
        this.actorId = actorId;
    }

    public WorkflowAction getAction() {
        return action;
    }

    public void setAction(WorkflowAction action) {
        this.action = action;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
