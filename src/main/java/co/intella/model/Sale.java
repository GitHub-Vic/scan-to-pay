package co.intella.model;


import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Andy Lin
 */
//@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
@Entity(name = "Sale")
public class Sale implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String accountId;

    private String pin;

    @ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "SaleRoleRelation",
            joinColumns = @JoinColumn(name = "saleId"),
            inverseJoinColumns = @JoinColumn(name = "roleId")
    )
    @JsonBackReference
    private List<Role> roles = new ArrayList<Role>();

    private int isEnable;

    private int type;

    private String name;

    private String mobile;

    private String email;

    @ManyToOne
    @JoinColumn(name = "manager")
    @JsonBackReference
    private Sale manager;

    @ManyToOne
    @JoinColumn(name = "creator")
    @JsonBackReference
    private Sale creator;

    private String createTime;

    private String lastLogin;

    @ManyToOne
    @JoinColumn(name = "modifier")
    @JsonBackReference
    private Sale modifier;

    private String modifiedTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public int getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(int isEnable) {
        this.isEnable = isEnable;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Sale getManager() {
        return manager;
    }

    public void setManager(Sale manager) {
        this.manager = manager;
    }

    public Sale getCreator() {
        return creator;
    }

    public void setCreator(Sale creator) {
        this.creator = creator;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Sale getModifier() {
        return modifier;
    }

    public void setModifier(Sale modifier) {
        this.modifier = modifier;
    }

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
}
