package co.intella.model;


import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

public class DiscountTradeOLPay {

    private long discountTradeOLPaySeq;

    private DiscountCtrl dicountCtrl;

    private String accountId;

    private String orderId;

    private String shortId;

    private BigDecimal oriAmount;

    private BigDecimal discountAmount;

    private BigDecimal tradeAmount;

    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss")
    private Date createDate;

    @Override
    public String toString() {
        return "DiscountTradeOLPay{" +
                "discountTradeOLPaySeq=" + discountTradeOLPaySeq +
                ", dicountCtrl=" + dicountCtrl +
                ", accountId='" + accountId + '\'' +
                ", orderId='" + orderId + '\'' +
                ", shortId='" + shortId + '\'' +
                ", oriAmount=" + oriAmount +
                ", discountAmount=" + discountAmount +
                ", tradeAmount=" + tradeAmount +
                ", createDate=" + createDate +
                '}';
    }

    public long getDiscountTradeOLPaySeq() {
        return discountTradeOLPaySeq;
    }

    public void setDiscountTradeOLPaySeq(long discountTradeOLPaySeq) {
        this.discountTradeOLPaySeq = discountTradeOLPaySeq;
    }

    public DiscountCtrl getDicountCtrl() {
        return dicountCtrl;
    }

    public void setDicountCtrl(DiscountCtrl dicountCtrl) {
        this.dicountCtrl = dicountCtrl;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getShortId() {
        return shortId;
    }

    public void setShortId(String shortId) {
        this.shortId = shortId;
    }

    public BigDecimal getOriAmount() {
        return oriAmount;
    }

    public void setOriAmount(BigDecimal oriAmount) {
        this.oriAmount = oriAmount;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(BigDecimal tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
