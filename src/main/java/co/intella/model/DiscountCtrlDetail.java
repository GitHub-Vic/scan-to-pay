package co.intella.model;


import java.math.BigDecimal;


public class DiscountCtrlDetail {

    private long discountDetailSeq;

    private DiscountCtrl dicountCtrl;

    private BigDecimal calUnit;

    private BigDecimal calRatio;

    private BigDecimal calLimit;

    public long getDiscountDetailSeq() {
        return discountDetailSeq;
    }

    public void setDiscountDetailSeq(long discountDetailSeq) {
        this.discountDetailSeq = discountDetailSeq;
    }

    public DiscountCtrl getDicountCtrl() {
        return dicountCtrl;
    }

    public void setDicountCtrl(DiscountCtrl dicountCtrl) {
        this.dicountCtrl = dicountCtrl;
    }

    public BigDecimal getCalUnit() {
        return calUnit;
    }

    public void setCalUnit(BigDecimal calUnit) {
        this.calUnit = calUnit;
    }

    public BigDecimal getCalRatio() {
        return calRatio;
    }

    public void setCalRatio(BigDecimal calRatio) {
        this.calRatio = calRatio;
    }

    public BigDecimal getCalLimit() {
        return calLimit;
    }

    public void setCalLimit(BigDecimal calLimit) {
        this.calLimit = calLimit;
    }

    @Override
    public String toString() {
        return "DiscountCtrlDetail{" +
                "discountDetailSeq=" + discountDetailSeq +
                ", dicountCtrl=" + dicountCtrl +
                ", calUnit=" + calUnit +
                ", calRatio=" + calRatio +
                ", calLimit=" + calLimit +
                '}';
    }
}
