package co.intella.model;

import java.util.Date;

public class TaxiTransactionDetail {

    private Integer id;
    private String orderId;
    private String accountId;
    private String terminalId;
    private String deviceId;
    private String licensePlate;
    private Integer needBalance;
    private Integer cash;
    private Integer carePoint;
    private String transactionDetail;
    private String status;
    private String transactionType;
    private String cardId;
    private String cardType;
    private String identityInfo;

    private String cardArea;

    private Date systemTime;

    private Integer arcSeq;

    private String transMsg;

    private String personalIdentity;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public Integer getNeedBalance() {
        return needBalance;
    }

    public void setNeedBalance(Integer needBalance) {
        this.needBalance = needBalance;
    }

    public Integer getCash() {
        return cash;
    }

    public void setCash(Integer cash) {
        this.cash = cash;
    }

    public Integer getCarePoint() {
        return carePoint;
    }

    public void setCarePoint(Integer carePoint) {
        this.carePoint = carePoint;
    }

    public String getTransactionDetail() {
        return transactionDetail;
    }

    public void setTransactionDetail(String transactionDetail) {
        this.transactionDetail = transactionDetail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getIdentityInfo() {
        return identityInfo;
    }

    public void setIdentityInfo(String identityInfo) {
        this.identityInfo = identityInfo;
    }

    public String getCardArea() {
        return cardArea;
    }

    public void setCardArea(String cardArea) {
        this.cardArea = cardArea;
    }

    public Date getSystemTime() {
        return systemTime;
    }

    public void setSystemTime(Date systemTime) {
        this.systemTime = systemTime;
    }

    public Integer getArcSeq() {
        return arcSeq;
    }

    public void setArcSeq(Integer arcSeq) {
        this.arcSeq = arcSeq;
    }

    public String getTransMsg() {
        return transMsg;
    }

    public void setTransMsg(String transMsg) {
        this.transMsg = transMsg;
    }

    public String getPersonalIdentity() {
        return personalIdentity;
    }

    public void setPersonalIdentity(String personalIdentity) {
        this.personalIdentity = personalIdentity;
    }

    @Override
    public String toString() {
        return "TaxiTransactionDetail{" +
                "id=" + id +
                ", orderId='" + orderId + '\'' +
                ", accountId='" + accountId + '\'' +
                ", terminalId='" + terminalId + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", licensePlate='" + licensePlate + '\'' +
                ", needBalance=" + needBalance +
                ", cash=" + cash +
                ", carePoint=" + carePoint +
                ", transactionDetail='" + transactionDetail + '\'' +
                ", status='" + status + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", cardId='" + cardId + '\'' +
                ", cardType='" + cardType + '\'' +
                ", identityInfo='" + identityInfo + '\'' +
                ", cardArea='" + cardArea + '\'' +
                ", systemTime=" + systemTime +
                ", arcSeq=" + arcSeq +
                ", transMsg='" + transMsg + '\'' +
                ", personalIdentity='" + personalIdentity + '\'' +
                '}';
    }
}
