package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QInvoiceDetail is a Querydsl query type for InvoiceDetail
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QInvoiceDetail extends EntityPathBase<InvoiceDetail> {

    private static final long serialVersionUID = -2074466412L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QInvoiceDetail invoiceDetail = new QInvoiceDetail("invoiceDetail");

    public final NumberPath<Double> amount = createNumber("amount", Double.class);

    public final StringPath createDate = createString("createDate");

    public final StringPath invoiceDetailRandomId = createString("invoiceDetailRandomId");

    public final StringPath orderId = createString("orderId");

    public final QRefundDetail refundDetailRandomId;

    public final QTradeDetail tradeDetailRandomId;

    public QInvoiceDetail(String variable) {
        this(InvoiceDetail.class, forVariable(variable), INITS);
    }

    public QInvoiceDetail(Path<? extends InvoiceDetail> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QInvoiceDetail(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QInvoiceDetail(PathMetadata metadata, PathInits inits) {
        this(InvoiceDetail.class, metadata, inits);
    }

    public QInvoiceDetail(Class<? extends InvoiceDetail> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.refundDetailRandomId = inits.isInitialized("refundDetailRandomId") ? new QRefundDetail(forProperty("refundDetailRandomId"), inits.get("refundDetailRandomId")) : null;
        this.tradeDetailRandomId = inits.isInitialized("tradeDetailRandomId") ? new QTradeDetail(forProperty("tradeDetailRandomId"), inits.get("tradeDetailRandomId")) : null;
    }

}

