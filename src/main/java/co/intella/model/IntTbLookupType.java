package co.intella.model;


import org.joda.time.DateTime;

import java.io.Serializable;

public class IntTbLookupType implements Serializable {

    private String lookupType;

    private String dscr;

    private String valueDscr;

    private String type1Dscr;

    private String type2Dscr;

    private String type3Dscr;

    private String crUser;

    private DateTime crDate;

    private String userStamp;

    private DateTime dateStamp;


    public String getLookupType() {
        return lookupType;
    }

    public void setLookupType(String lookupType) {
        this.lookupType = lookupType;
    }

    public String getDscr() {
        return dscr;
    }

    public void setDscr(String dscr) {
        this.dscr = dscr;
    }

    public String getValueDscr() {
        return valueDscr;
    }

    public void setValueDscr(String valueDscr) {
        this.valueDscr = valueDscr;
    }

    public String getType1Dscr() {
        return type1Dscr;
    }

    public void setType1Dscr(String type1Dscr) {
        this.type1Dscr = type1Dscr;
    }

    public String getType2Dscr() {
        return type2Dscr;
    }

    public void setType2Dscr(String type2Dscr) {
        this.type2Dscr = type2Dscr;
    }

    public String getType3Dscr() {
        return type3Dscr;
    }

    public void setType3Dscr(String type3Dscr) {
        this.type3Dscr = type3Dscr;
    }

    public String getCrUser() {
        return crUser;
    }

    public void setCrUser(String crUser) {
        this.crUser = crUser;
    }

    public DateTime getCrDate() {
        return crDate;
    }

    public void setCrDate(DateTime crDate) {
        this.crDate = crDate;
    }

    public String getUserStamp() {
        return userStamp;
    }

    public void setUserStamp(String userStamp) {
        this.userStamp = userStamp;
    }

    public DateTime getDateStamp() {
        return dateStamp;
    }

    public void setDateStamp(DateTime dateStamp) {
        this.dateStamp = dateStamp;
    }
}
