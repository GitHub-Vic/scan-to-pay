package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QTicket is a Querydsl query type for Ticket
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTicket extends EntityPathBase<Ticket> {

    private static final long serialVersionUID = -1137502698L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QTicket ticket = new QTicket("ticket");

    public final DateTimePath<java.util.Date> createDate = createDateTime("createDate", java.util.Date.class);

    public final QSale creator;

    public final StringPath firstReviewDate = createString("firstReviewDate");

    public final QSale firstReviewer;

    public final QMerchant merchant;

    public final StringPath secondReviewDate = createString("secondReviewDate");

    public final QSale secondReviewer;

    public final NumberPath<Integer> status = createNumber("status", Integer.class);

    public final NumberPath<Long> ticketId = createNumber("ticketId", Long.class);

    public final StringPath title = createString("title");

    public QTicket(String variable) {
        this(Ticket.class, forVariable(variable), INITS);
    }

    public QTicket(Path<? extends Ticket> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QTicket(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QTicket(PathMetadata metadata, PathInits inits) {
        this(Ticket.class, metadata, inits);
    }

    public QTicket(Class<? extends Ticket> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.creator = inits.isInitialized("creator") ? new QSale(forProperty("creator"), inits.get("creator")) : null;
        this.firstReviewer = inits.isInitialized("firstReviewer") ? new QSale(forProperty("firstReviewer"), inits.get("firstReviewer")) : null;
        this.merchant = inits.isInitialized("merchant") ? new QMerchant(forProperty("merchant"), inits.get("merchant")) : null;
        this.secondReviewer = inits.isInitialized("secondReviewer") ? new QSale(forProperty("secondReviewer"), inits.get("secondReviewer")) : null;
    }

}

