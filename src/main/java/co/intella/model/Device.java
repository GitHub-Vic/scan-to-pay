package co.intella.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

/**
 * @author Miles Wu
 */
@Entity(name = "Device")
public class Device {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "BINARY(16)")
    private UUID deviceRandomId;

    @ManyToOne
    @JoinColumn(name = "owner")
    private Merchant owner;

    private String description;

    private String type;

    private String name;

    private String ownerDeviceId;

    private Long batchNumber;

    private String ezcDongleId;

    private String postradesn;

    private String lastusedate;

    private String aesKey;

    private String ban;

    private String hostSerialNumber;

    private String licensePlate;

    private String location;

    private String ipasslocation;

    private String checkoutSerial;

    private Long filesn;

    private String swVersion;

    private String posCode;

    private String apiComPort;

    private String serviceComPort;

    public UUID getDeviceRandomId() {
        return deviceRandomId;
    }

    public void setDeviceRandomId(UUID deviceRandomId) {
        this.deviceRandomId = deviceRandomId;
    }

    public Merchant getOwner() {
        return owner;
    }

    public void setOwner(Merchant owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerDeviceId() {
        return ownerDeviceId;
    }

    public void setOwnerDeviceId(String ownerDeviceId) {
        this.ownerDeviceId = ownerDeviceId;
    }

    public Long getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(Long batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getEzcDongleId() {
        return ezcDongleId;
    }

    public void setEzcDongleId(String ezcDongleId) {
        this.ezcDongleId = ezcDongleId;
    }

    public String getAesKey() {
        return aesKey;
    }

    public void setAesKey(String aesKey) {
        this.aesKey = aesKey;
    }

    public String getBan() {
        return ban;
    }

    public void setBan(String ban) {
        this.ban = ban;
    }

    public String getHostSerialNumber() {
        return hostSerialNumber;
    }

    public void setHostSerialNumber(String hostSerialNumber) {
        this.hostSerialNumber = hostSerialNumber;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getIpasslocation() {
        return ipasslocation;
    }

    public void setIpasslocation(String ipasslocation) {
        this.ipasslocation = ipasslocation;
    }

    public String getPostradesn() {
        return postradesn;
    }

    public void setPostradesn(String postradesn) {
        this.postradesn = postradesn;
    }

    public String getLastusedate() {
        return lastusedate;
    }

    public void setLastusedate(String lastusedate) {
        this.lastusedate = lastusedate;
    }

    public String getCheckoutSerial() {
        return checkoutSerial;
    }

    public void setCheckoutSerial(String checkoutSerial) {
        this.checkoutSerial = checkoutSerial;
    }

    public Long getFilesn() {
        return filesn;
    }

    public void setFilesn(Long filesn) {
        this.filesn = filesn;
    }

    public String getSwVersion() {
        return swVersion;
    }

    public void setSwVersion(String swVersion) {
        this.swVersion = swVersion;
    }

    public String getPosCode() {
        return posCode;
    }

    public void setPosCode(String posCode) {
        this.posCode = posCode;
    }

    public String getApiComPort() {
        return apiComPort;
    }

    public void setApiComPort(String apiComPort) {
        this.apiComPort = apiComPort;
    }

    public String getServiceComPort() {
        return serviceComPort;
    }

    public void setServiceComPort(String serviceComPort) {
        this.serviceComPort = serviceComPort;
    }

    @Override
    public String toString() {
        return "Device{" +
                "deviceRandomId=" + deviceRandomId +
                ", owner=" + owner +
                ", description='" + description + '\'' +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", ownerDeviceId='" + ownerDeviceId + '\'' +
                ", batchNumber=" + batchNumber +
                ", ezcDongleId='" + ezcDongleId + '\'' +
                ", postradesn='" + postradesn + '\'' +
                ", lastusedate='" + lastusedate + '\'' +
                ", aesKey='" + aesKey + '\'' +
                ", ban='" + ban + '\'' +
                ", hostSerialNumber='" + hostSerialNumber + '\'' +
                ", licensePlate='" + licensePlate + '\'' +
                ", location='" + location + '\'' +
                ", ipasslocation='" + ipasslocation + '\'' +
                ", checkoutSerial='" + checkoutSerial + '\'' +
                ", filesn=" + filesn +
                ", swVersion='" + swVersion + '\'' +
                ", posCode='" + posCode + '\'' +
                ", apiComPort='" + apiComPort + '\'' +
                ", serviceComPort='" + serviceComPort + '\'' +
                '}';
    }
}
