package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QINT_TB_CheckoutLog is a Querydsl query type for INT_TB_CheckoutLog
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QINT_TB_CheckoutLog extends EntityPathBase<INT_TB_CheckoutLog> {

    private static final long serialVersionUID = 1848342023L;

    public static final QINT_TB_CheckoutLog iNT_TB_CheckoutLog = new QINT_TB_CheckoutLog("iNT_TB_CheckoutLog");

    public final StringPath accountId = createString("accountId");

    public final StringPath batchNo = createString("batchNo");

    public final NumberPath<Long> ccLogSeq = createNumber("ccLogSeq", Long.class);

    public final NumberPath<Long> ccSeqId = createNumber("ccSeqId", Long.class);

    public final StringPath comment = createString("comment");

    public final DateTimePath<java.util.Date> crDate = createDateTime("crDate", java.util.Date.class);

    public final DateTimePath<java.util.Date> dateStamp = createDateTime("dateStamp", java.util.Date.class);

    public final StringPath ownerDeviceId = createString("ownerDeviceId");

    public final StringPath status = createString("status");

    public QINT_TB_CheckoutLog(String variable) {
        super(INT_TB_CheckoutLog.class, forVariable(variable));
    }

    public QINT_TB_CheckoutLog(Path<? extends INT_TB_CheckoutLog> path) {
        super(path.getType(), path.getMetadata());
    }

    public QINT_TB_CheckoutLog(PathMetadata metadata) {
        super(INT_TB_CheckoutLog.class, metadata);
    }

}

