package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QQrType7 is a Querydsl query type for QrType7
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QQrType7 extends EntityPathBase<QrType7> {

    private static final long serialVersionUID = 973847762L;

    public static final QQrType7 qrType7 = new QQrType7("qrType7");

    public final StringPath accountId = createString("accountId");

    public final NumberPath<Integer> amount = createNumber("amount", Integer.class);

    public final StringPath licensePlate = createString("licensePlate");

    public final StringPath mobile = createString("mobile");

    public final StringPath orderId = createString("orderId");

    public final NumberPath<Integer> qrType7Sql = createNumber("qrType7Sql", Integer.class);

    public final StringPath remark = createString("remark");

    public final StringPath shortId = createString("shortId");

    public final DateTimePath<java.sql.Timestamp> sysTime = createDateTime("sysTime", java.sql.Timestamp.class);

    public QQrType7(String variable) {
        super(QrType7.class, forVariable(variable));
    }

    public QQrType7(Path<? extends QrType7> path) {
        super(path.getType(), path.getMetadata());
    }

    public QQrType7(PathMetadata metadata) {
        super(QrType7.class, metadata);
    }

}

