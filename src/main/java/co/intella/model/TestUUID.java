package co.intella.model;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.id.UUIDHexGenerator;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.UUID;

/**
 * @author Alex
 */
@Entity(name = "test")
public class TestUUID {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid.hex")
    @Column(columnDefinition = "VARCHAR(255)")
    private String id;

    private String com;

    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime ts;


    public String getCom() {
        return com;
    }

    public void setCom(String com) {
        this.com = com;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DateTime getTs() {
        return ts;
    }

    public void setTs(DateTime ts) {
        this.ts = ts;
    }
}
