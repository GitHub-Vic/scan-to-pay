package co.intella.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

/**
 * @author Andy Lin
 */
@Entity(name = "CaptureDetail")
public class CaptureDetail {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "BINARY(16)")
    private UUID captureDetailRandomId;

    private String merchantId;

    private long totalCount;

    private long totalFee;

    private double feeRate;

    private String status;

    private String captureDate;

    private String sequentialNo;

    private String createTime;

    private String method;

    @OneToMany(mappedBy = "captureDetail", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<CaptureOrderDetail> captureOrderDetails;

    public UUID getCaptureDetailRandomId() {
        return captureDetailRandomId;
    }

    public void setCaptureDetailRandomId(UUID captureDetailRandomId) {
        this.captureDetailRandomId = captureDetailRandomId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public long getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(long totalFee) {
        this.totalFee = totalFee;
    }

    public String getCaptureDate() {
        return captureDate;
    }

    public void setCaptureDate(String captureDate) {
        this.captureDate = captureDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSequentialNo() {
        return sequentialNo;
    }

    public void setSequentialNo(String sequentialNo) {
        this.sequentialNo = sequentialNo;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public double getFeeRate() {
        return feeRate;
    }

    public void setFeeRate(double feeRate) {
        this.feeRate = feeRate;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Set<CaptureOrderDetail> getCaptureOrderDetails() {
        return captureOrderDetails;
    }

    public void setCaptureOrderDetails(Set<CaptureOrderDetail> captureOrderDetails) {
        this.captureOrderDetails = captureOrderDetails;
    }
}
