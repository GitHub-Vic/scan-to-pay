package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCaptureDetail is a Querydsl query type for CaptureDetail
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCaptureDetail extends EntityPathBase<CaptureDetail> {

    private static final long serialVersionUID = -625183507L;

    public static final QCaptureDetail captureDetail = new QCaptureDetail("captureDetail");

    public final StringPath captureDate = createString("captureDate");

    public final ComparablePath<java.util.UUID> captureDetailRandomId = createComparable("captureDetailRandomId", java.util.UUID.class);

    public final SetPath<CaptureOrderDetail, QCaptureOrderDetail> captureOrderDetails = this.<CaptureOrderDetail, QCaptureOrderDetail>createSet("captureOrderDetails", CaptureOrderDetail.class, QCaptureOrderDetail.class, PathInits.DIRECT2);

    public final StringPath createTime = createString("createTime");

    public final NumberPath<Double> feeRate = createNumber("feeRate", Double.class);

    public final StringPath merchantId = createString("merchantId");

    public final StringPath method = createString("method");

    public final StringPath sequentialNo = createString("sequentialNo");

    public final StringPath status = createString("status");

    public final NumberPath<Long> totalCount = createNumber("totalCount", Long.class);

    public final NumberPath<Long> totalFee = createNumber("totalFee", Long.class);

    public QCaptureDetail(String variable) {
        super(CaptureDetail.class, forVariable(variable));
    }

    public QCaptureDetail(Path<? extends CaptureDetail> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCaptureDetail(PathMetadata metadata) {
        super(CaptureDetail.class, metadata);
    }

}

