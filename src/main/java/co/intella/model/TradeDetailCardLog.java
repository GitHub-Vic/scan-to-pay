package co.intella.model;

public class TradeDetailCardLog {

    private String orderId;
    private String issuBank;
    private String cardNumber;
    private String beneficiaryBank;
    private String cardType;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getIssuBank() {
        return issuBank;
    }

    public void setIssuBank(String issuBank) {
        this.issuBank = issuBank;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getBeneficiaryBank() {
        return beneficiaryBank;
    }

    public void setBeneficiaryBank(String beneficiaryBank) {
        this.beneficiaryBank = beneficiaryBank;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    @Override
    public String toString() {
        return "TradeDetailCardLog{" +
                "orderId='" + orderId + '\'' +
                ", issuBank='" + issuBank + '\'' +
                ", cardNumber='" + cardNumber + '\'' +
                ", beneficiaryBank='" + beneficiaryBank + '\'' +
                ", cardType='" + cardType + '\'' +
                '}';
    }
}
