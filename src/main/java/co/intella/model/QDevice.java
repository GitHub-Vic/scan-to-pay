package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QDevice is a Querydsl query type for Device
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDevice extends EntityPathBase<Device> {

    private static final long serialVersionUID = -1598699168L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QDevice device = new QDevice("device");

    public final StringPath aesKey = createString("aesKey");

    public final StringPath apiComPort = createString("apiComPort");

    public final StringPath ban = createString("ban");

    public final NumberPath<Long> batchNumber = createNumber("batchNumber", Long.class);

    public final StringPath checkoutSerial = createString("checkoutSerial");

    public final StringPath description = createString("description");

    public final ComparablePath<java.util.UUID> deviceRandomId = createComparable("deviceRandomId", java.util.UUID.class);

    public final StringPath ezcDongleId = createString("ezcDongleId");

    public final NumberPath<Long> filesn = createNumber("filesn", Long.class);

    public final StringPath hostSerialNumber = createString("hostSerialNumber");

    public final StringPath ipasslocation = createString("ipasslocation");

    public final StringPath lastusedate = createString("lastusedate");

    public final StringPath licensePlate = createString("licensePlate");

    public final StringPath location = createString("location");

    public final StringPath name = createString("name");

    public final QMerchant owner;

    public final StringPath ownerDeviceId = createString("ownerDeviceId");

    public final StringPath posCode = createString("posCode");

    public final StringPath postradesn = createString("postradesn");

    public final StringPath serviceComPort = createString("serviceComPort");

    public final StringPath swVersion = createString("swVersion");

    public final StringPath type = createString("type");

    public QDevice(String variable) {
        this(Device.class, forVariable(variable), INITS);
    }

    public QDevice(Path<? extends Device> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QDevice(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QDevice(PathMetadata metadata, PathInits inits) {
        this(Device.class, metadata, inits);
    }

    public QDevice(Class<? extends Device> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.owner = inits.isInitialized("owner") ? new QMerchant(forProperty("owner"), inits.get("owner")) : null;
    }

}

