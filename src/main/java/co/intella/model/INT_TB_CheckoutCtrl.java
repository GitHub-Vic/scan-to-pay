package co.intella.model;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 * @author Nick Lian
 */
@Entity(name = "INT_TB_CheckoutCtrl")
public class INT_TB_CheckoutCtrl {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long ccSeqId;
	
	private String accountId;
	
	private String batchNo;
	
	private String type;

	private String comment;
	
	private String ownerDeviceId;
	
	private String status;
	
	private Date crDate;
	
	private Date dateStamp;
	
	private Long tradeAmount;
	
	private Long refundAmount;
	
	private Long reserveAmount;
	
	private Long reserveCancelAmount;

	private String method;

	public long getCcSeqId() {
		return ccSeqId;
	}

	public void setCcSeqId(long ccSeqId) {
		this.ccSeqId = ccSeqId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getOwnerDeviceId() {
		return ownerDeviceId;
	}

	public void setOwnerDeviceId(String ownerDeviceId) {
		this.ownerDeviceId = ownerDeviceId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCrDate() {
		return crDate;
	}

	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	public Date getDateStamp() {
		return dateStamp;
	}

	public void setDateStamp(Date dateStamp) {
		this.dateStamp = dateStamp;
	}

	public Long getTradeAmount() {
		return tradeAmount;
	}

	public void setTradeAmount(Long tradeAmount) {
		this.tradeAmount = tradeAmount;
	}

	public Long getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Long refundAmount) {
		this.refundAmount = refundAmount;
	}

	public Long getReserveAmount() {
		return reserveAmount;
	}

	public void setReserveAmount(Long reserveAmount) {
		this.reserveAmount = reserveAmount;
	}

	public Long getReserveCancelAmount() {
		return reserveCancelAmount;
	}

	public void setReserveCancelAmount(Long reserveCancelAmount) {
		this.reserveCancelAmount = reserveCancelAmount;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	@Override
	public String toString() {
		return "INT_TB_CheckoutCtrl{" +
				"ccSeqId=" + ccSeqId +
				", accountId='" + accountId + '\'' +
				", batchNo='" + batchNo + '\'' +
				", type='" + type + '\'' +
				", comment='" + comment + '\'' +
				", ownerDeviceId='" + ownerDeviceId + '\'' +
				", status='" + status + '\'' +
				", crDate=" + crDate +
				", dateStamp=" + dateStamp +
				", tradeAmount=" + tradeAmount +
				", refundAmount=" + refundAmount +
				", reserveAmount=" + reserveAmount +
				", reserveCancelAmount=" + reserveCancelAmount +
				", method='" + method + '\'' +
				'}';
	}
}
