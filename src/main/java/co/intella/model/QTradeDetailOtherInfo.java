package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTradeDetailOtherInfo is a Querydsl query type for TradeDetailOtherInfo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTradeDetailOtherInfo extends EntityPathBase<TradeDetailOtherInfo> {

    private static final long serialVersionUID = 1567229459L;

    public static final QTradeDetailOtherInfo tradeDetailOtherInfo = new QTradeDetailOtherInfo("tradeDetailOtherInfo");

    public final StringPath callBackUrl = createString("callBackUrl");

    public final NumberPath<Long> exeTime = createNumber("exeTime", Long.class);

    public final StringPath requestDetail = createString("requestDetail");

    public final ComparablePath<java.util.UUID> tradeDetailRandomId = createComparable("tradeDetailRandomId", java.util.UUID.class);

    public QTradeDetailOtherInfo(String variable) {
        super(TradeDetailOtherInfo.class, forVariable(variable));
    }

    public QTradeDetailOtherInfo(Path<? extends TradeDetailOtherInfo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTradeDetailOtherInfo(PathMetadata metadata) {
        super(TradeDetailOtherInfo.class, metadata);
    }

}

