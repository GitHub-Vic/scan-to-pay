package co.intella.model;

public class INT_VW_IpassCheckOut {

    private String accountId;
    private String ownerDeviceId;

    @Override
    public String toString() {
        return "INT_VW_IpassCheckOut{" +
                "accountId='" + accountId + '\'' +
                ", ownerDeviceId='" + ownerDeviceId + '\'' +
                '}';
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getOwnerDeviceId() {
        return ownerDeviceId;
    }

    public void setOwnerDeviceId(String ownerDeviceId) {
        this.ownerDeviceId = ownerDeviceId;
    }
}
