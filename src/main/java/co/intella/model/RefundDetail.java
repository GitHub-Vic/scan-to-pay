package co.intella.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Alex
 */
@Entity(name = "RefundDetail")
public class RefundDetail {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid.hex")
    @Column(columnDefinition = "VARCHAR(32)")
    private String refundDetailRandomId;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "tradeDetailRandomId")
    private TradeDetail tradeDetailRandomId;

    @ManyToOne
    @JoinColumn(name = "deviceRandomId")
    private Device deviceRandomId;

    private String orderId;

    private long amount;

    private String accountId;

    private String storeRefundId;

    private String systemRefundId;

    private String cashier;

    private String status;

    private String createTime;

    private String reason;

    private String method;

    private String platformRefundDate;

    private boolean reconciliation;

    private Date sysTime;

    private boolean isPartialRefund;

    private String batchNo;

    private String rrn;

    private String ezcTxnDate;

    private Boolean ezcCancel = false;

    private String txParams;

    public String getTxParams() {
        return txParams;
    }

    public void setTxParams(String txParams) {
        this.txParams = txParams;
    }

    public String getRefundDetailRandomId() {
        return refundDetailRandomId;
    }

    public void setRefundDetailRandomId(String refundDetailRandomId) {
        this.refundDetailRandomId = refundDetailRandomId;
    }

    public TradeDetail getTradeDetailRandomId() {
        return tradeDetailRandomId;
    }

    public void setTradeDetailRandomId(TradeDetail tradeDetailRandomId) {
        this.tradeDetailRandomId = tradeDetailRandomId;
    }

    public Device getDeviceRandomId() {
        return deviceRandomId;
    }

    public void setDeviceRandomId(Device deviceRandomId) {
        this.deviceRandomId = deviceRandomId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getStoreRefundId() {
        return storeRefundId;
    }

    public void setStoreRefundId(String storeRefundId) {
        this.storeRefundId = storeRefundId;
    }

    public String getSystemRefundId() {
        return systemRefundId;
    }

    public void setSystemRefundId(String systemRefundId) {
        this.systemRefundId = systemRefundId;
    }

    public String getCashier() {
        return cashier;
    }

    public void setCashier(String cashier) {
        this.cashier = cashier;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPlatformRefundDate() {
        return platformRefundDate;
    }

    public void setPlatformRefundDate(String platformRefundDate) {
        this.platformRefundDate = platformRefundDate;
    }

    public boolean isReconciliation() {
        return reconciliation;
    }

    public void setReconciliation(boolean reconciliation) {
        this.reconciliation = reconciliation;
    }

    public boolean isPartialRefund() {
        return isPartialRefund;
    }

    public void setPartialRefund(boolean partialRefund) {
        isPartialRefund = partialRefund;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getEzcTxnDate() {
        return ezcTxnDate;
    }

    public void setEzcTxnDate(String ezcTxnDate) {
        this.ezcTxnDate = ezcTxnDate;
    }

    @PrePersist
    protected void onCreate() {
        sysTime = new Date();
    }

    public Date getSysTime() {
        return sysTime;
    }

    public void setSysTime(Date sysTime) {
        this.sysTime = sysTime;
    }

    public Boolean getEzcCancel() {
        return ezcCancel;
    }

    public void setEzcCancel(Boolean ezcCancel) {
        this.ezcCancel = ezcCancel;
    }
}
