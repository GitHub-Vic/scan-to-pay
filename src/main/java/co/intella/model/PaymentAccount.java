package co.intella.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

/**
 * @author Miles Wu
 */
@Entity(name = "PaymentAccount")
public class PaymentAccount {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "BINARY(16)")
    private UUID paymentAccountRandomId;

    @ManyToOne
    @JoinColumn(name = "merchantSeqId")
    private Merchant merchant;

    @ManyToOne
    @JoinColumn(name = "bankId")
    private Bank bank;

    private String account;

    private String description;

    private Boolean isDefault;

    private String platformCheckCode;

    private String hashKey;

    private String hashIV;

    private String keyFilePath;

    private String appId;

    private String bankName;
    
	private String bankCode;

    private String depositNo;
    
    private String accountTerminalId;

    private Integer seqBy;

    private String systemID;

    private String spID;

    private String sftpUser;

    private String sftpPasswd;

    public UUID getPaymentAccountRandomId() {
        return paymentAccountRandomId;
    }

    public void setPaymentAccountRandomId(UUID paymentAccountRandomId) {
        this.paymentAccountRandomId = paymentAccountRandomId;
    }

    public PaymentAccount() {
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Boolean getDefault() {
        return isDefault;
    }

    public void setDefault(Boolean aDefault) {
        isDefault = aDefault;
    }

    public String getPlatformCheckCode() {
        return platformCheckCode;
    }

    public void setPlatformCheckCode(String platformCheckCode) {
        this.platformCheckCode = platformCheckCode;
    }

    public String getHashKey() {
        return hashKey;
    }

    public void setHashKey(String hashKey) {
        this.hashKey = hashKey;
    }

    public String getHashIV() {
        return hashIV;
    }

    public void setHashIV(String hashIV) {
        this.hashIV = hashIV;
    }

    public String getKeyFilePath() {
        return keyFilePath;
    }

    public void setKeyFilePath(String keyFilePath) {
        this.keyFilePath = keyFilePath;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
    
    public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

    public String getDepositNo() {
        return depositNo;
    }

    public void setDepositNo(String depositNo) {
        this.depositNo = depositNo;
    }

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public String getAccountTerminalId() {
		return accountTerminalId;
	}

	public void setAccountTerminalId(String accountTerminalId) {
		this.accountTerminalId = accountTerminalId;
	}

    public Integer getSeqBy() {
        return seqBy;
    }

    public void setSeqBy(Integer seqBy) {
        this.seqBy = seqBy;
    }

    public String getSystemID() {
        return systemID;
    }

    public void setSystemID(String systemID) {
        this.systemID = systemID;
    }

    public String getSpID() {
        return spID;
    }

    public void setSpID(String spID) {
        this.spID = spID;
    }

    public String getSftpUser() {
        return sftpUser;
    }

    public void setSftpUser(String sftpUser) {
        this.sftpUser = sftpUser;
    }

    public String getSftpPasswd() {
        return sftpPasswd;
    }

    public void setSftpPasswd(String sftpPasswd) {
        this.sftpPasswd = sftpPasswd;
    }

    public PaymentAccount(String systemID, String spID, String sftpUser, String sftpPasswd) {
        this.systemID = systemID;
        this.spID = spID;
        this.sftpUser = sftpUser;
        this.sftpPasswd = sftpPasswd;
    }

	@Override
	public String toString() {
		return "PaymentAccount [paymentAccountRandomId=" + paymentAccountRandomId + ", merchant=" + merchant + ", bank="
				+ bank + ", account=" + account + ", description=" + description + ", isDefault=" + isDefault
				+ ", platformCheckCode=" + platformCheckCode + ", hashKey=" + hashKey + ", hashIV=" + hashIV
				+ ", keyFilePath=" + keyFilePath + ", appId=" + appId + ", bankName=" + bankName + ", bankCode="
				+ bankCode + ", depositNo=" + depositNo + ", accountTerminalId=" + accountTerminalId + ", seqBy="
				+ seqBy + ", systemID=" + systemID + ", spID=" + spID + ", sftpUser=" + sftpUser + ", sftpPasswd="
				+ sftpPasswd + "]";
	}
    
    
}
