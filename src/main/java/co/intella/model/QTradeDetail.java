package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QTradeDetail is a Querydsl query type for TradeDetail
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTradeDetail extends EntityPathBase<TradeDetail> {

    private static final long serialVersionUID = -426375669L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QTradeDetail tradeDetail = new QTradeDetail("tradeDetail");

    public final StringPath accountId = createString("accountId");

    public final StringPath barcode = createString("barcode");

    public final StringPath batchNo = createString("batchNo");

    public final StringPath buyerTax = createString("buyerTax");

    public final NumberPath<Long> carePoint = createNumber("carePoint", Long.class);

    public final StringPath carrierNum = createString("carrierNum");

    public final StringPath cashier = createString("cashier");

    public final StringPath comment = createString("comment");

    public final StringPath createDate = createString("createDate");

    public final StringPath creditCardType = createString("creditCardType");

    public final StringPath creditOrderId = createString("creditOrderId");

    public final StringPath description = createString("description");

    public final StringPath detail = createString("detail");

    public final StringPath deviceOS = createString("deviceOS");

    public final QDevice deviceRandomId;

    public final StringPath ezcTxnDate = createString("ezcTxnDate");

    public final StringPath intellaOrderId = createString("intellaOrderId");

    public final StringPath invoiced = createString("invoiced");

    public final BooleanPath isOnSale = createBoolean("isOnSale");

    public final StringPath mchKey = createString("mchKey");

    public final StringPath mchName = createString("mchName");

    public final StringPath method = createString("method");

    public final StringPath onlinePayUrl = createString("onlinePayUrl");

    public final StringPath orderId = createString("orderId");

    public final NumberPath<Double> originalPrice = createNumber("originalPrice", Double.class);

    public final StringPath payCode = createString("payCode");

    public final NumberPath<Long> payment = createNumber("payment", Long.class);

    public final QPaymentAccount paymentAccount;

    public final StringPath platformPaidDate = createString("platformPaidDate");

    public final StringPath qrcodeToken = createString("qrcodeToken");

    public final BooleanPath reconciliation = createBoolean("reconciliation");

    public final SetPath<RefundDetail, QRefundDetail> refundDetails = this.<RefundDetail, QRefundDetail>createSet("refundDetails", RefundDetail.class, QRefundDetail.class, PathInits.DIRECT2);

    public final StringPath refundStatus = createString("refundStatus");

    public final NumberPath<Long> remainCarePoint = createNumber("remainCarePoint", Long.class);

    public final StringPath remark = createString("remark");

    public final StringPath rrn = createString("rrn");

    public final StringPath serviceType = createString("serviceType");

    public final StringPath status = createString("status");

    public final StringPath storeInfo = createString("storeInfo");

    public final StringPath storeRefundId = createString("storeRefundId");

    public final StringPath systemOrderId = createString("systemOrderId");

    public final StringPath systemRefundId = createString("systemRefundId");

    public final ComparablePath<java.util.UUID> tradeDetailRandomId = createComparable("tradeDetailRandomId", java.util.UUID.class);

    public final StringPath tradeToken = createString("tradeToken");

    public final StringPath txParams = createString("txParams");

    public final NumberPath<Integer> type = createNumber("type", Integer.class);

    public final StringPath userId = createString("userId");

    public QTradeDetail(String variable) {
        this(TradeDetail.class, forVariable(variable), INITS);
    }

    public QTradeDetail(Path<? extends TradeDetail> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QTradeDetail(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QTradeDetail(PathMetadata metadata, PathInits inits) {
        this(TradeDetail.class, metadata, inits);
    }

    public QTradeDetail(Class<? extends TradeDetail> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.deviceRandomId = inits.isInitialized("deviceRandomId") ? new QDevice(forProperty("deviceRandomId"), inits.get("deviceRandomId")) : null;
        this.paymentAccount = inits.isInitialized("paymentAccount") ? new QPaymentAccount(forProperty("paymentAccount"), inits.get("paymentAccount")) : null;
    }

}

