package co.intella.model;


import co.intella.model.Merchant;
import co.intella.model.Sale;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.Date;


/**
 * @author Alex
 */
@Entity(name = "Ticket")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long ticketId;

    private String title;

    @ManyToOne
    @JoinColumn(name = "merchant")
    private Merchant merchant;

    private Integer status;

    @ManyToOne
    @JoinColumn(name = "creator")
    private Sale creator;

    private Date createDate;

    @ManyToOne
    @JoinColumn(name = "firstReviewer")
    private Sale firstReviewer;


    private String firstReviewDate;

    @ManyToOne
    @JoinColumn(name = "secondReviewer")
    private Sale secondReviewer;

    private String secondReviewDate;

    public long getId() {
        return ticketId;
    }

    public void setId(long ticketId) {
        this.ticketId = ticketId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Sale getCreator() {
        return creator;
    }

    public void setCreator(Sale creator) {
        this.creator = creator;
    }

    public Sale getFirstReviewer() {
        return firstReviewer;
    }

    public void setFirstReviewer(Sale firstReviewer) {
        this.firstReviewer = firstReviewer;
    }

    public String getFirstReviewDate() {
        return firstReviewDate;
    }

    public void setFirstReviewDate(String firstReviewDate) {
        this.firstReviewDate = firstReviewDate;
    }

    public Sale getSecondReviewer() {
        return secondReviewer;
    }

    public void setSecondReviewer(Sale secondReviewer) {
        this.secondReviewer = secondReviewer;
    }

    public String getSecondReviewDate() {
        return secondReviewDate;
    }

    public void setSecondReviewDate(String secondReviewDate) {
        this.secondReviewDate = secondReviewDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @PrePersist
    protected void onCreate() {
        createDate = new Date();
    }

}
