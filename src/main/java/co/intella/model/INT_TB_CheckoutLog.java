package co.intella.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;

/**
 * @author Nick Lian
 */
@Entity(name = "INT_TB_CheckoutLog")
public class INT_TB_CheckoutLog {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long ccLogSeq;
	
	private long ccSeqId;
	
	private String accountId;
	
	private String batchNo;
	
	private String comment;
	
	private String ownerDeviceId;
	
	private String status;

	private Date crDate;
	
	private Date dateStamp;

	public long getCcLogSeq() {
		return ccLogSeq;
	}

	public void setCcLogSeq(long ccLogSeq) {
		this.ccLogSeq = ccLogSeq;
	}

	public long getCcSeqId() {
		return ccSeqId;
	}

	public void setCcSeqId(long ccSeqId) {
		this.ccSeqId = ccSeqId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getOwnerDeviceId() {
		return ownerDeviceId;
	}

	public void setOwnerDeviceId(String ownerDeviceId) {
		this.ownerDeviceId = ownerDeviceId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCrDate() {
		return crDate;
	}

	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}

	public Date getDateStamp() {
		return dateStamp;
	}

	public void setDateStamp(Date dateStamp) {
		this.dateStamp = dateStamp;
	}

}
