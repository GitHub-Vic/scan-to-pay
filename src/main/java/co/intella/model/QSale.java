package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSale is a Querydsl query type for Sale
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSale extends EntityPathBase<Sale> {

    private static final long serialVersionUID = 1147381201L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSale sale = new QSale("sale");

    public final StringPath accountId = createString("accountId");

    public final StringPath createTime = createString("createTime");

    public final QSale creator;

    public final StringPath email = createString("email");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Integer> isEnable = createNumber("isEnable", Integer.class);

    public final StringPath lastLogin = createString("lastLogin");

    public final QSale manager;

    public final StringPath mobile = createString("mobile");

    public final StringPath modifiedTime = createString("modifiedTime");

    public final QSale modifier;

    public final StringPath name = createString("name");

    public final StringPath pin = createString("pin");

    public final ListPath<Role, QRole> roles = this.<Role, QRole>createList("roles", Role.class, QRole.class, PathInits.DIRECT2);

    public final NumberPath<Integer> type = createNumber("type", Integer.class);

    public QSale(String variable) {
        this(Sale.class, forVariable(variable), INITS);
    }

    public QSale(Path<? extends Sale> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QSale(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QSale(PathMetadata metadata, PathInits inits) {
        this(Sale.class, metadata, inits);
    }

    public QSale(Class<? extends Sale> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.creator = inits.isInitialized("creator") ? new QSale(forProperty("creator"), inits.get("creator")) : null;
        this.manager = inits.isInitialized("manager") ? new QSale(forProperty("manager"), inits.get("manager")) : null;
        this.modifier = inits.isInitialized("modifier") ? new QSale(forProperty("modifier"), inits.get("modifier")) : null;
    }

}

