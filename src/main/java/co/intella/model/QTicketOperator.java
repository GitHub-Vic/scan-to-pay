package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QTicketOperator is a Querydsl query type for TicketOperator
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTicketOperator extends EntityPathBase<TicketOperator> {

    private static final long serialVersionUID = 1587752154L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QTicketOperator ticketOperator = new QTicketOperator("ticketOperator");

    public final QRole availableRole;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final QTicket ticketId;

    public QTicketOperator(String variable) {
        this(TicketOperator.class, forVariable(variable), INITS);
    }

    public QTicketOperator(Path<? extends TicketOperator> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QTicketOperator(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QTicketOperator(PathMetadata metadata, PathInits inits) {
        this(TicketOperator.class, metadata, inits);
    }

    public QTicketOperator(Class<? extends TicketOperator> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.availableRole = inits.isInitialized("availableRole") ? new QRole(forProperty("availableRole")) : null;
        this.ticketId = inits.isInitialized("ticketId") ? new QTicket(forProperty("ticketId"), inits.get("ticketId")) : null;
    }

}

