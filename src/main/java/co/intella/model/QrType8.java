package co.intella.model;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;

/**
 * @author Austin Chen
 */
@Entity(name = "INT_TB_QrType8")
@DynamicUpdate
@DynamicInsert
public class QrType8 {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer qrType8Sql;
    
    private String  orderId;
    
    private String accountId;
    
    private String shortId;

    private String name;

    private String mobile;

    private int amount;

    private String remark;
    
    private Timestamp  sysTime;

	public Integer getQrType8Sql() {
		return qrType8Sql;
	}

	public void setQrType8Sql(Integer qrType8Sql) {
		this.qrType8Sql = qrType8Sql;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getShortId() {
		return shortId;
	}

	public void setShortId(String shortId) {
		this.shortId = shortId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Timestamp getSysTime() {
		return sysTime;
	}

	public void setSysTime(Timestamp sysTime) {
		this.sysTime = sysTime;
	}
}
