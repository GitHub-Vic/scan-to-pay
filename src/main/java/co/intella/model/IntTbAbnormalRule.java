package co.intella.model;


import java.util.Date;

public class IntTbAbnormalRule {

    private Integer arSeq;
    private Integer arcSeq;
    private String abnormalRule;
    private String parameter1;
    private String parameter2;
    private String parameter3;
    private String crUser;
    private Date crDate;
    private String userStamp;
    private Date dateStamp;
    private String abnormalRuleDscr;
    private String dscr;
    private String type1;
    private Integer value;

    public Integer getArSeq() {
        return arSeq;
    }

    public void setArSeq(Integer arSeq) {
        this.arSeq = arSeq;
    }

    public Integer getArcSeq() {
        return arcSeq;
    }

    public void setArcSeq(Integer arcSeq) {
        this.arcSeq = arcSeq;
    }

    public String getAbnormalRule() {
        return abnormalRule;
    }

    public void setAbnormalRule(String abnormalRule) {
        this.abnormalRule = abnormalRule;
    }

    public String getParameter1() {
        return parameter1;
    }

    public void setParameter1(String parameter1) {
        this.parameter1 = parameter1;
    }

    public String getParameter2() {
        return parameter2;
    }

    public void setParameter2(String parameter2) {
        this.parameter2 = parameter2;
    }

    public String getParameter3() {
        return parameter3;
    }

    public void setParameter3(String parameter3) {
        this.parameter3 = parameter3;
    }

    public String getCrUser() {
        return crUser;
    }

    public void setCrUser(String crUser) {
        this.crUser = crUser;
    }

    public Date getCrDate() {
        return crDate;
    }

    public void setCrDate(Date crDate) {
        this.crDate = crDate;
    }

    public String getUserStamp() {
        return userStamp;
    }

    public void setUserStamp(String userStamp) {
        this.userStamp = userStamp;
    }

    public Date getDateStamp() {
        return dateStamp;
    }

    public void setDateStamp(Date dateStamp) {
        this.dateStamp = dateStamp;
    }

    public String getAbnormalRuleDscr() {
        return abnormalRuleDscr;
    }

    public void setAbnormalRuleDscr(String abnormalRuleDscr) {
        this.abnormalRuleDscr = abnormalRuleDscr;
    }

    public String getDscr() {
        return dscr;
    }

    public void setDscr(String dscr) {
        this.dscr = dscr;
    }

    public String getType1() {
        return type1;
    }

    public void setType1(String type1) {
        this.type1 = type1;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
