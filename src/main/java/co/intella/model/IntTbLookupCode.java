package co.intella.model;


import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.Objects;

public class IntTbLookupCode implements Serializable {

    private IntTbLookupType lookupType;

    private String lookupCode;

    private String dscr;

    private int value;

    private String type1;

    private String type2;

    private String type3;

    private DateTime crDate;

    private String userStamp;

    private DateTime dateStamp;

    public IntTbLookupType getLookupType() {
        return lookupType;
    }

    public void setLookupType(IntTbLookupType lookupType) {
        this.lookupType = lookupType;
    }

    public String getLookupCode() {
        return lookupCode;
    }

    public void setLookupCode(String lookupCode) {
        this.lookupCode = lookupCode;
    }

    public String getDscr() {
        return dscr;
    }

    public void setDscr(String dscr) {
        this.dscr = dscr;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getType1() {
        return type1;
    }

    public void setType1(String type1) {
        this.type1 = type1;
    }

    public String getType2() {
        return type2;
    }

    public void setType2(String type2) {
        this.type2 = type2;
    }

    public String getType3() {
        return type3;
    }

    public void setType3(String type3) {
        this.type3 = type3;
    }

    public DateTime getCrDate() {
        return crDate;
    }

    public void setCrDate(DateTime crDate) {
        this.crDate = crDate;
    }

    public String getUserStamp() {
        return userStamp;
    }

    public void setUserStamp(String userStamp) {
        this.userStamp = userStamp;
    }

    public DateTime getDateStamp() {
        return dateStamp;
    }

    public void setDateStamp(DateTime dateStamp) {
        this.dateStamp = dateStamp;
    }

    @Override
    public String toString() {

        return "{lookupType = " + (Objects.nonNull(lookupType) ? lookupType.getLookupType() : "null") + ",lookupCode = " + lookupCode + ",dscr = " + dscr + ",value = " + value + ",type1 = " + type1 +
                ",type2 = " + type2 + ",type3 = " + type3 + ",crDate = " + crDate + ",userStamp = " + userStamp + ",dateStamp = " + dateStamp + "}";
    }
}
