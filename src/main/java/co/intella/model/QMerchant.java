package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QMerchant is a Querydsl query type for Merchant
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMerchant extends EntityPathBase<Merchant> {

    private static final long serialVersionUID = -366093742L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QMerchant merchant = new QMerchant("merchant");

    public final StringPath accountId = createString("accountId");

    public final NumberPath<Integer> accountType = createNumber("accountType", Integer.class);

    public final StringPath activateToken = createString("activateToken");

    public final StringPath address = createString("address");

    public final NumberPath<Integer> assistNo = createNumber("assistNo", Integer.class);

    public final StringPath brandName = createString("brandName");

    public final StringPath buttonText = createString("buttonText");

    public final StringPath buttonURL = createString("buttonURL");

    public final StringPath callBackDelay = createString("callBackDelay");

    public final StringPath closeDate = createString("closeDate");

    public final StringPath comId = createString("comId");

    public final StringPath companyName = createString("companyName");

    public final StringPath companyPhone = createString("companyPhone");

    public final NumberPath<Long> creator = createNumber("creator", Long.class);

    public final StringPath email = createString("email");

    public final StringPath endDate = createString("endDate");

    public final NumberPath<Integer> franchiseNo = createNumber("franchiseNo", Integer.class);

    public final StringPath loginId = createString("loginId");

    public final NumberPath<Long> merchantSeqId = createNumber("merchantSeqId", Long.class);

    public final StringPath mgtPermission = createString("mgtPermission");

    public final StringPath name = createString("name");

    public final StringPath notifyUrl = createString("notifyUrl");

    public final StringPath openDate = createString("openDate");

    public final StringPath parentMerchantAccount = createString("parentMerchantAccount");

    public final StringPath paycodeToken = createString("paycodeToken");

    public final StringPath paymentBanner = createString("paymentBanner");

    public final StringPath phone = createString("phone");

    public final StringPath pin = createString("pin");

    public final StringPath postalCode = createString("postalCode");

    public final StringPath randomId = createString("randomId");

    public final SetPath<Recipient, QRecipient> recipients = this.<Recipient, QRecipient>createSet("recipients", Recipient.class, QRecipient.class, PathInits.DIRECT2);

    public final StringPath refundPin = createString("refundPin");

    public final NumberPath<Integer> regularChainNo = createNumber("regularChainNo", Integer.class);

    public final QSale saleId;

    public final StringPath salesDate = createString("salesDate");

    public final StringPath signDate = createString("signDate");

    public final StringPath startDate = createString("startDate");

    public final StringPath status = createString("status");

    public final StringPath storeCode = createString("storeCode");

    public final StringPath storeName = createString("storeName");

    public final NumberPath<Integer> storeType = createNumber("storeType", Integer.class);

    public final StringPath tradePin = createString("tradePin");

    public final EnumPath<MerchantType> type = createEnum("type", MerchantType.class);

    public QMerchant(String variable) {
        this(Merchant.class, forVariable(variable), INITS);
    }

    public QMerchant(Path<? extends Merchant> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QMerchant(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QMerchant(PathMetadata metadata, PathInits inits) {
        this(Merchant.class, metadata, inits);
    }

    public QMerchant(Class<? extends Merchant> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.saleId = inits.isInitialized("saleId") ? new QSale(forProperty("saleId"), inits.get("saleId")) : null;
    }

}

