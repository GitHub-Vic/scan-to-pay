package co.intella.model;

import java.io.Serializable;
import java.util.Date;


public class IntTbLookupCodeNoFK implements Serializable {
	private String lookupType;

    private String lookupCode;

    private String dscr;

    private Integer value;

    private String type1;

    private String type2;

    private String type3;
    
    private String crUser;
    
    private Date crDate;

    private String userStamp;

    private Date dateStamp;

    public String getLookupType() {
        return lookupType;
    }

    public void setLookupType(String lookupType) {
        this.lookupType = lookupType;
    }

    public String getLookupCode() {
        return lookupCode;
    }

    public void setLookupCode(String lookupCode) {
        this.lookupCode = lookupCode;
    }

    public String getDscr() {
        return dscr;
    }

    public void setDscr(String dscr) {
        this.dscr = dscr;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getType1() {
        return type1;
    }

    public void setType1(String type1) {
        this.type1 = type1;
    }

    public String getType2() {
        return type2;
    }

    public void setType2(String type2) {
        this.type2 = type2;
    }

    public String getType3() {
        return type3;
    }

    public void setType3(String type3) {
        this.type3 = type3;
    }
    
    public String getCrUser() {
        return crUser;
    }

    public void setCrUser(String crUser) {
        this.crUser = crUser;
    }
    
    public Date getCrDate() {
        return crDate;
    }

    public void setCrDate(Date crDate) {
        this.crDate = crDate;
    }

    public String getUserStamp() {
        return userStamp;
    }

    public void setUserStamp(String userStamp) {
        this.userStamp = userStamp;
    }

    public Date getDateStamp() {
        return dateStamp;
    }

    public void setDateStamp(Date dateStamp) {
        this.dateStamp = dateStamp;
    }

    @Override
    public String toString() {

        return "{lookupType = " + lookupType +
		    ",lookupCode = " + lookupCode +
		    ",dscr = " + dscr +
		    ",value = " + value +
		    ",type1 = " + type1 +
		    ",type2 = " + type2 +
		    ",type3 = " + type3 +
		    ",crUser = " + crUser +
		    ",crDate = " + crDate +
		    ",userStamp = " + userStamp +
		    ",dateStamp = " + dateStamp +
		    "}";
	    }
}
