package co.intella.model;

import javax.persistence.*;

/**
 * @author Alex
 */
@Entity(name = "TicketOperator")
public class TicketOperator {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "ticketId")
    private Ticket ticketId;

    @ManyToOne
    @JoinColumn(name = "availableRole")
    private Role availableRole;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Ticket getTicketId() {
        return ticketId;
    }

    public void setTicketId(Ticket ticketId) {
        this.ticketId = ticketId;
    }

    public Role getAvailableRole() {
        return availableRole;
    }

    public void setAvailableRole(Role availableRole) {
        this.availableRole = availableRole;
    }
}
