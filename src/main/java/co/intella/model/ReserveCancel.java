package co.intella.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import org.hibernate.annotations.GenericGenerator;
import co.intella.model.Device;

/**
 * @author Miles
 */
@Entity(name = "ReserveCancelDetail")
public class ReserveCancel {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid.hex")
    @Column(columnDefinition = "VARCHAR(32)")
    private String id;

    @OneToOne
    @JoinColumn(name = "reserveId")
    private ReserveDetail reserveDetail;
    
    @ManyToOne
    @JoinColumn(name = "deviceRandomId")
    private Device deviceRandomId;

    private String deviceId;

    private String orderId;

    private long amount;

    private String accountId;

    private String cashier;

    private String status;

    private String createTime;

    private String reason;

    private String method;

    private String platformCancelDate;

    private boolean reconciliation;

//    private String sysTime;

    private String batchNo;

    private String rrn;

    private String userId;

    private String ezcTxnDate;

    private String txParams;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ReserveDetail getReserveDetail() {
        return reserveDetail;
    }

    public void setReserveDetail(ReserveDetail reserveDetail) {
        this.reserveDetail = reserveDetail;
    }

    public Device getDeviceRandomId() {
      return deviceRandomId;
    }

    public void setDeviceRandomId(Device deviceRandomId) {
      this.deviceRandomId = deviceRandomId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getCashier() {
        return cashier;
    }

    public void setCashier(String cashier) {
        this.cashier = cashier;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPlatformCancelDate() {
        return platformCancelDate;
    }

    public void setPlatformCancelDate(String platformCancelDate) {
        this.platformCancelDate = platformCancelDate;
    }

    public boolean isReconciliation() {
        return reconciliation;
    }

    public void setReconciliation(boolean reconciliation) {
        this.reconciliation = reconciliation;
    }

//    public String getSysTime() {
//        return sysTime;
//    }

//    public void setSysTime(String sysTime) {
//        this.sysTime = sysTime;
//    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEzcTxnDate() {
        return ezcTxnDate;
    }

    public void setEzcTxnDate(String ezcTxnDate) {
        this.ezcTxnDate = ezcTxnDate;
    }

    public String getTxParams() {
        return txParams;
    }

    public void setTxParams(String txParams) {
        this.txParams = txParams;
    }
}
