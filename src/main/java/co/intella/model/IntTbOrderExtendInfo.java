package co.intella.model;

import java.io.Serializable;

public class IntTbOrderExtendInfo implements Serializable {

    private long merchantRandomId;

    private String secMerchantName;
    private String secMerchantId;
    private String secMerchantIndustry;
    private String storeName;
    private String storeId;
    private String businessType;
    private String shopId;
    private String patnerId;

    public long getMerchantRandomId() {
        return merchantRandomId;
    }

    public void setMerchantRandomId(long merchantRandomId) {
        this.merchantRandomId = merchantRandomId;
    }

    public String getSecMerchantName() {
        return secMerchantName;
    }

    public void setSecMerchantName(String secMerchantName) {
        this.secMerchantName = secMerchantName;
    }

    public String getSecMerchantId() {
        return secMerchantId;
    }

    public void setSecMerchantId(String secMerchantId) {
        this.secMerchantId = secMerchantId;
    }

    public String getSecMerchantIndustry() {
        return secMerchantIndustry;
    }

    public void setSecMerchantIndustry(String secMerchantIndustry) {
        this.secMerchantIndustry = secMerchantIndustry;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getPatnerId() {
        return patnerId;
    }

    public void setPatnerId(String patnerId) {
        this.patnerId = patnerId;
    }
}
