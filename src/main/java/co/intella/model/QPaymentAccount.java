package co.intella.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPaymentAccount is a Querydsl query type for PaymentAccount
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPaymentAccount extends EntityPathBase<PaymentAccount> {

    private static final long serialVersionUID = 93765201L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPaymentAccount paymentAccount = new QPaymentAccount("paymentAccount");

    public final StringPath account = createString("account");

    public final StringPath accountTerminalId = createString("accountTerminalId");

    public final StringPath appId = createString("appId");

    public final QBank bank;

    public final StringPath bankCode = createString("bankCode");

    public final StringPath bankName = createString("bankName");

    public final StringPath depositNo = createString("depositNo");

    public final StringPath description = createString("description");

    public final StringPath hashIV = createString("hashIV");

    public final StringPath hashKey = createString("hashKey");

    public final BooleanPath isDefault = createBoolean("isDefault");

    public final StringPath keyFilePath = createString("keyFilePath");

    public final QMerchant merchant;

    public final ComparablePath<java.util.UUID> paymentAccountRandomId = createComparable("paymentAccountRandomId", java.util.UUID.class);

    public final StringPath platformCheckCode = createString("platformCheckCode");

    public final NumberPath<Integer> seqBy = createNumber("seqBy", Integer.class);

    public final StringPath sftpPasswd = createString("sftpPasswd");

    public final StringPath sftpUser = createString("sftpUser");

    public final StringPath spID = createString("spID");

    public final StringPath systemID = createString("systemID");

    public QPaymentAccount(String variable) {
        this(PaymentAccount.class, forVariable(variable), INITS);
    }

    public QPaymentAccount(Path<? extends PaymentAccount> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPaymentAccount(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPaymentAccount(PathMetadata metadata, PathInits inits) {
        this(PaymentAccount.class, metadata, inits);
    }

    public QPaymentAccount(Class<? extends PaymentAccount> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.bank = inits.isInitialized("bank") ? new QBank(forProperty("bank")) : null;
        this.merchant = inits.isInitialized("merchant") ? new QMerchant(forProperty("merchant"), inits.get("merchant")) : null;
    }

}

