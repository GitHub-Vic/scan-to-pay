package co.intella.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Austin Chen
 */
@Entity(name = "INT_TB_QrType7")
@DynamicUpdate
@DynamicInsert
public class QrType7 {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer qrType7Sql;
    
    private String  orderId;
    
    private String accountId;
    
    private String shortId;

    private String licensePlate;

    private String mobile;

    private int amount;

    private String remark;
    
    private Timestamp  sysTime;

	public Integer getQrType7Sql() {
		return qrType7Sql;
	}

	public void setQrType7Sql(Integer qrType7Sql) {
		this.qrType7Sql = qrType7Sql;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getShortId() {
		return shortId;
	}

	public void setShortId(String shortId) {
		this.shortId = shortId;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Timestamp getSysTime() {
		return sysTime;
	}

	public void setSysTime(Timestamp sysTime) {
		this.sysTime = sysTime;
	}

    
}
