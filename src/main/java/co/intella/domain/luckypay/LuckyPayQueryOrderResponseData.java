package co.intella.domain.luckypay;

import com.google.gson.annotations.SerializedName;

public class LuckyPayQueryOrderResponseData {

	@SerializedName("return_code")
	private String returnCode;
	
	@SerializedName("return_msg")
	private String returnMsg;
	
	private String type;
	
	private String store;
	
	private String terminal;
	
	@SerializedName("order_no")
	private String orderNo;
	
	private String currency;
	
	private int amount;
	
	@SerializedName("transaction_id")
	private String transactionId;
	
	@SerializedName("transaction_time")
	private String transactionTime;
	
	@SerializedName("transaction_fee")
	private int transactionFee;
	
	@SerializedName("refund_amount")
	private int refundAmount;
	
	private String status;
	
	@SerializedName("submit_reverse")
	private int submitReverse;
	
	@SerializedName("submit_refund")
	private int submitRefund;
	
	@SerializedName("result_code")
	private String resultCode;
	
	private String nonce;
	
	@SerializedName("payment_type")
	private String paymentType;
	
	@SerializedName("payment_desc")
	private String paymentDesc;
	
	@SerializedName("payment_lastno")
	private String paymentLastno;
	
	@SerializedName("invoice_carrier")
	private String invoiceCarrier;
	
	@SerializedName("carrier_type")
	private String carrierType;
	
	@SerializedName("carrier_data")
	private String carrierData;
	
	@SerializedName("include_smoke")
	private String includeSmoke;
	
	@SerializedName("include_smoke_amount")
	private int includeSmokeAmount;
	
	@SerializedName("channel_error_code")
	private String channelErrorCode;
	
	@SerializedName("channel_error_msg")
	private String channelErrorMsg;
	
	private String time;
	
	private String version;
	
	private String sign;
	
	private String key;
	
	@SerializedName("key_expiry")
	private String keyExpiry;
	
	@SerializedName("store_key")
	private String storeKey;

	public String getReturnCode() {
		return returnCode;
	}


	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}


	public String getReturnMsg() {
		return returnMsg;
	}


	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getStore() {
		return store;
	}


	public void setStore(String store) {
		this.store = store;
	}


	public String getTerminal() {
		return terminal;
	}


	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}


	public String getOrderNo() {
		return orderNo;
	}


	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}


	public String getCurrency() {
		return currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public int getAmount() {
		return amount;
	}


	public void setAmount(int amount) {
		this.amount = amount;
	}


	public String getTransactionId() {
		return transactionId;
	}


	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}


	public String getTransactionTime() {
		return transactionTime;
	}


	public void setTransactionTime(String transactionTime) {
		this.transactionTime = transactionTime;
	}


	public int getTransactionFee() {
		return transactionFee;
	}


	public void setTransactionFee(int transactionFee) {
		this.transactionFee = transactionFee;
	}


	public int getRefundAmount() {
		return refundAmount;
	}


	public void setRefundAmount(int refundAmount) {
		this.refundAmount = refundAmount;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public int getSubmitReverse() {
		return submitReverse;
	}


	public void setSubmitReverse(int submitReverse) {
		this.submitReverse = submitReverse;
	}


	public int getSubmitRefund() {
		return submitRefund;
	}


	public void setSubmitRefund(int submitRefund) {
		this.submitRefund = submitRefund;
	}


	public String getResultCode() {
		return resultCode;
	}


	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}


	public String getNonce() {
		return nonce;
	}


	public void setNonce(String nonce) {
		this.nonce = nonce;
	}


	public String getPaymentType() {
		return paymentType;
	}


	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}


	public String getPaymentDesc() {
		return paymentDesc;
	}


	public void setPaymentDesc(String paymentDesc) {
		this.paymentDesc = paymentDesc;
	}


	public String getPaymentLastno() {
		return paymentLastno;
	}


	public void setPaymentLastno(String paymentLastno) {
		this.paymentLastno = paymentLastno;
	}


	public String getInvoiceCarrier() {
		return invoiceCarrier;
	}


	public void setInvoiceCarrier(String invoiceCarrier) {
		this.invoiceCarrier = invoiceCarrier;
	}


	public String getCarrierType() {
		return carrierType;
	}


	public void setCarrierType(String carrierType) {
		this.carrierType = carrierType;
	}


	public String getCarrierData() {
		return carrierData;
	}


	public void setCarrierData(String carrierData) {
		this.carrierData = carrierData;
	}


	public String getIncludeSmoke() {
		return includeSmoke;
	}


	public void setIncludeSmoke(String includeSmoke) {
		this.includeSmoke = includeSmoke;
	}


	public int getIncludeSmokeAmount() {
		return includeSmokeAmount;
	}


	public void setIncludeSmokeAmount(int includeSmokeAmount) {
		this.includeSmokeAmount = includeSmokeAmount;
	}


	public String getChannelErrorCode() {
		return channelErrorCode;
	}


	public void setChannelErrorCode(String channelErrorCode) {
		this.channelErrorCode = channelErrorCode;
	}


	public String getChannelErrorMsg() {
		return channelErrorMsg;
	}


	public void setChannelErrorMsg(String channelErrorMsg) {
		this.channelErrorMsg = channelErrorMsg;
	}


	public String getTime() {
		return time;
	}


	public void setTime(String time) {
		this.time = time;
	}


	public String getVersion() {
		return version;
	}


	public void setVersion(String version) {
		this.version = version;
	}


	public String getSign() {
		return sign;
	}


	public void setSign(String sign) {
		this.sign = sign;
	}


	public String getKey() {
		return key;
	}


	public void setKey(String key) {
		this.key = key;
	}


	public String getKeyExpiry() {
		return keyExpiry;
	}


	public void setKeyExpiry(String keyExpiry) {
		this.keyExpiry = keyExpiry;
	}


	public String getStoreKey() {
		return storeKey;
	}


	public void setStoreKey(String storeKey) {
		this.storeKey = storeKey;
	}
	
}
