package co.intella.domain.luckypay;

import com.google.gson.annotations.SerializedName;


public class LuckyPayErrorResponseData {

    @SerializedName("return_code")
    private String returnCode;

    @SerializedName("error_message")
    private String errorMessage;

    @SerializedName("qrcode")
    private String qrCode;

    @SerializedName("payment_gateway")
    private String paymentGateway;
    

    public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

}
