package co.intella.domain.luckypay;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;


public class LuckyPayNotifyResponseData {

    private String store;

    private String orderId;

    private String amount;
    
    private String returnCode;

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}


	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
    
}
