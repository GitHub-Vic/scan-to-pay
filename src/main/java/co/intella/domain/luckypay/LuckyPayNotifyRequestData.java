package co.intella.domain.luckypay;

import com.google.gson.annotations.SerializedName;


public class LuckyPayNotifyRequestData {

    private String token;
    
    private String store;

    @SerializedName("order_no")
    private String orderNo;

    private String amount;
    
    @SerializedName("return_code")
    private String returnCode;
    
    @SerializedName("result_code")
    private String resultCode;
    
    @SerializedName("transaction_id")
    private String transactionId;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
}
