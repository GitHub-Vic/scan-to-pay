package co.intella.domain.pi;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class PiOrderQueryRequestData extends PiBasicRequestData {

    @SerializedName("channel_id")
    @JsonProperty(value = "channel_id", required = true)
    private String mchId;

    @SerializedName("stare_date")
    @JsonProperty(value = "stare_date", required = true)
    private String stareDate;

    @SerializedName("end_date")
    @JsonProperty(value = "end_date", required = true)
    private String endDate;

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getStareDate() {
        return stareDate;
    }

    public void setStareDate(String stareDate) {
        this.stareDate = stareDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
