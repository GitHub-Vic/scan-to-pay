package co.intella.domain.pi;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class PiOLRefundRequestData extends PiBasicRequestData {

    @JsonProperty("partner_id")
    private String partnerId;


    @JsonProperty("pi_tx_id")
    private String txId;

    @SerializedName("StoreOrderNo")
    @JsonProperty("order_id")
    private String orderId;

    @SerializedName("RefundFee")
    @JsonProperty("refund_amount")
    private Integer refundmount;

    @JsonProperty("created_at")
    private String createdAt;

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getRefundmount() {
        return refundmount;
    }

    public void setRefundmount(Integer refundmount) {
        this.refundmount = refundmount;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }
}
