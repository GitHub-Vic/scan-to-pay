package co.intella.domain.pi;

import co.intella.domain.integration.ResponseGeneralData;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class PiRefundResponseData extends ResponseGeneralData{

    @SerializedName("status")
    @JsonProperty(value = "status")
    private String statusDesc;

    @SerializedName("error_code")
    @JsonProperty(value = "error_code")
    private String statusCode;

    @SerializedName("bill_id")
    @JsonProperty(value = "bill_id")
    private String storeOrderNo;

    @SerializedName("transaction_id")
    @JsonProperty(value = "transaction_id")
    private String sysOrderNo;

    @SerializedName("refund_time")
    @JsonProperty(value = "refund_time")
    private String refundedAt;

    @SerializedName("payment_type")
    @JsonProperty(value = "payment_type")
    private String paymentType;

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getSysOrderNo() {
        return sysOrderNo;
    }

    public void setSysOrderNo(String sysOrderNo) {
        this.sysOrderNo = sysOrderNo;
    }

    public String getRefundedAt() {
        return refundedAt;
    }

    public void setRefundedAt(String refundedAt) {
        this.refundedAt = refundedAt;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
}
