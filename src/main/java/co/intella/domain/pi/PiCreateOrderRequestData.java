package co.intella.domain.pi;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class PiCreateOrderRequestData {

    @SerializedName("partner_id")
    @JsonProperty(value = "partner_id", required = true)
    private String partnerId;

    @SerializedName("qrcode")
    @JsonProperty(value = "qrcode", required = true)
    private String qrcode;

    @SerializedName("payment_gateway")
    @JsonProperty(value = "payment_gateway", required = true)
    private String paymentGateway;

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }
}
