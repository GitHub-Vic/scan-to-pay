package co.intella.domain.pi;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class PiOLSingleOrderQueryRequestData extends PiBasicRequestData{

    @JsonProperty("partner_id")
    private String partnerId;

    @JsonProperty("partner_key")
    private String partnerKey;

    @SerializedName("StoreOrderNo")
    @JsonProperty("order_id")
    private String orderId;

    @JsonProperty("payment_gateway")
    private String paymentGateway;

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerKey() {
        return partnerKey;
    }

    public void setPartnerKey(String partnerKey) {
        this.partnerKey = partnerKey;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }
}
