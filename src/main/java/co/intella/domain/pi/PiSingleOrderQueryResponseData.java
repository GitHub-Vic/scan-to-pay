package co.intella.domain.pi;

import co.intella.domain.integration.ResponseGeneralData;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class PiSingleOrderQueryResponseData extends ResponseGeneralData{

    @SerializedName("status")
    @JsonProperty(value = "status")
    private String statusDesc;

    @SerializedName("error_code")
    @JsonProperty(value = "error_code")
    private String statusCode;

    @SerializedName("bill_id")
    @JsonProperty(value = "bill_id")
    private String storeOrderNo;

    @SerializedName("psp_id")
    @JsonProperty(value = "psp_id")
    private String pspId;

    @SerializedName("transaction_id")
    @JsonProperty(value = "transaction_id")
    private String sysOrderNo;

    @SerializedName("transaction_status")
    @JsonProperty(value = "transaction_status")
    private String transStatus;

    @SerializedName("amt")
    @JsonProperty(value = "amt")
    private String totalFee;

    @SerializedName("carrier_id_2")
    @JsonProperty(value = "carrier_id_2")
    private String carrierId2;

    @SerializedName("channel_id")
    @JsonProperty(value = "channel_id")
    private String mchId;

    @SerializedName("store_no")
    @JsonProperty(value = "store_no")
    private String storeInfo;

    @SerializedName("reg_id")
    @JsonProperty(value = "reg_id")
    private String regId;

    @SerializedName("items")
    @JsonProperty(value = "items")
    private String productDesc;

    @SerializedName("create_time")
    @JsonProperty(value = "create_time")
    private String responseTime;

    @SerializedName("member_card_id")
    @JsonProperty(value = "member_card_id")
    private String memberCardId;

    @SerializedName("member_card_type")
    @JsonProperty(value = "member_card_type")
    private String memberCardType;

    @SerializedName("payment_type")
    @JsonProperty(value = "payment_type")
    private String paymentType;

    @SerializedName("balance_amount")
    @JsonProperty(value = "balance_amount")
    private String balanceAmount;

    @SerializedName("2nd_funding_amount")
    @JsonProperty(value = "2nd_funding_amount")
    private String secondFundingAmount;

    @SerializedName("2nd_funding_type")
    @JsonProperty(value = "2nd_funding_type")
    private String secondFundingType;

    @SerializedName("2nd_funding_source_code")
    @JsonProperty(value = "2nd_funding_source_code")
    private String secondFundingSourceCode;

    @SerializedName("merchant_discount_code")
    @JsonProperty(value = "merchant_discount_code")
    private String merchantDiscountCode;

    @SerializedName("merchant_discount_amount")
    @JsonProperty(value = "merchant_discount_amount")
    private String merchantDiscountAmount;

    @SerializedName("psp_discount_code")
    @JsonProperty(value = "psp_discount_code")
    private String pspDiscountCode;

    @SerializedName("psp_discount_amount")
    @JsonProperty(value = "psp_discount_amount")
    private String pspDiscountAmount;

    @SerializedName("psp_bonuspoint")
    @JsonProperty(value = "psp_bonuspoint")
    private String pspBonuspoint;

    @SerializedName("psp_bonuspoint_amount")
    @JsonProperty(value = "psp_bonuspoint_amount")
    private String pspBonuspointAmount;

    @SerializedName("clearing_by")
    @JsonProperty(value = "clearing_by")
    private String clearingBy;

    @SerializedName("barcode")
    @JsonProperty(value = "barcode")
    private String AuthCode;

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getPspId() {
        return pspId;
    }

    public void setPspId(String pspId) {
        this.pspId = pspId;
    }

    public String getSysOrderNo() {
        return sysOrderNo;
    }

    public void setSysOrderNo(String sysOrderNo) {
        this.sysOrderNo = sysOrderNo;
    }

    public String getTransStatus() {
        return transStatus;
    }

    public void setTransStatus(String transStatus) {
        this.transStatus = transStatus;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getCarrierId2() {
        return carrierId2;
    }

    public void setCarrierId2(String carrierId2) {
        this.carrierId2 = carrierId2;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getStoreInfo() {
        return storeInfo;
    }

    public void setStoreInfo(String storeInfo) {
        this.storeInfo = storeInfo;
    }

    public String getRegId() {
        return regId;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }

    public String getMemberCardId() {
        return memberCardId;
    }

    public void setMemberCardId(String memberCardId) {
        this.memberCardId = memberCardId;
    }

    public String getMemberCardType() {
        return memberCardType;
    }

    public void setMemberCardType(String memberCardType) {
        this.memberCardType = memberCardType;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getSecondFundingAmount() {
        return secondFundingAmount;
    }

    public void setSecondFundingAmount(String secondFundingAmount) {
        this.secondFundingAmount = secondFundingAmount;
    }

    public String getSecondFundingType() {
        return secondFundingType;
    }

    public void setSecondFundingType(String secondFundingType) {
        this.secondFundingType = secondFundingType;
    }

    public String getSecondFundingSourceCode() {
        return secondFundingSourceCode;
    }

    public void setSecondFundingSourceCode(String secondFundingSourceCode) {
        this.secondFundingSourceCode = secondFundingSourceCode;
    }

    public String getMerchantDiscountCode() {
        return merchantDiscountCode;
    }

    public void setMerchantDiscountCode(String merchantDiscountCode) {
        this.merchantDiscountCode = merchantDiscountCode;
    }

    public String getMerchantDiscountAmount() {
        return merchantDiscountAmount;
    }

    public void setMerchantDiscountAmount(String merchantDiscountAmount) {
        this.merchantDiscountAmount = merchantDiscountAmount;
    }

    public String getPspDiscountCode() {
        return pspDiscountCode;
    }

    public void setPspDiscountCode(String pspDiscountCode) {
        this.pspDiscountCode = pspDiscountCode;
    }

    public String getPspDiscountAmount() {
        return pspDiscountAmount;
    }

    public void setPspDiscountAmount(String pspDiscountAmount) {
        this.pspDiscountAmount = pspDiscountAmount;
    }

    public String getPspBonuspoint() {
        return pspBonuspoint;
    }

    public void setPspBonuspoint(String pspBonuspoint) {
        this.pspBonuspoint = pspBonuspoint;
    }

    public String getPspBonuspointAmount() {
        return pspBonuspointAmount;
    }

    public void setPspBonuspointAmount(String pspBonuspointAmount) {
        this.pspBonuspointAmount = pspBonuspointAmount;
    }

    public String getClearingBy() {
        return clearingBy;
    }

    public void setClearingBy(String clearingBy) {
        this.clearingBy = clearingBy;
    }

    public String getAuthCode() {
        return AuthCode;
    }

    public void setAuthCode(String authCode) {
        AuthCode = authCode;
    }
}
