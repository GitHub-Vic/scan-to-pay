package co.intella.domain.pi;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class PiServerRefundResponse {

    @SerializedName("result")
    private String result;

    @SerializedName("err_code")
    private String errCode;

    @JsonProperty(value = "StoreOrderNo", required = true)
    @SerializedName("partner_order_id")
    private String partnerOrderId;

    @JsonProperty(value = "partner_user_id", required = true)
    @SerializedName("partner_user_id")
    private String partnerUserId;

    @JsonProperty(value = "partner_id", required = true)
    @SerializedName("partner_id")
    private String partnerId;

    @SerializedName("partner_name")
    private String partnerName;

    @SerializedName("money")
    private String money;

    @SerializedName("refunded_at")
    private String refundedAt;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getPartnerOrderId() {
        return partnerOrderId;
    }

    public void setPartnerOrderId(String partnerOrderId) {
        this.partnerOrderId = partnerOrderId;
    }

    public String getPartnerUserId() {
        return partnerUserId;
    }

    public void setPartnerUserId(String partnerUserId) {
        this.partnerUserId = partnerUserId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getRefundedAt() {
        return refundedAt;
    }

    public void setRefundedAt(String refundedAt) {
        this.refundedAt = refundedAt;
    }
}
