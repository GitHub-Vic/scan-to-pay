package co.intella.domain.pi;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class PiInAppOrderQueryResponse {

    @SerializedName("partner_id")
    private String partnerId;

    @SerializedName("partner_name")
    private String partnerName;

    @SerializedName("partner_user_id")
    private String partnerUserId;

    @SerializedName("partner_order_id")
    private String partnerOrderId;

    @SerializedName("money")
    private long money;

    @SerializedName("paid")
    private boolean paid;

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getPartnerUserId() {
        return partnerUserId;
    }

    public void setPartnerUserId(String partnerUserId) {
        this.partnerUserId = partnerUserId;
    }

    public String getPartnerOrderId() {
        return partnerOrderId;
    }

    public void setPartnerOrderId(String partnerOrderId) {
        this.partnerOrderId = partnerOrderId;
    }

    public long getMoney() {
        return money;
    }

    public void setMoney(long money) {
        this.money = money;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }
}
