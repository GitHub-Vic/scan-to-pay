package co.intella.domain.pi;

import co.intella.domain.integration.ResponseGeneralData;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class PiOLSingleOrderQueryResponseData  extends ResponseGeneralData {


    @SerializedName("order_id")
    private String orderId;

    @SerializedName("payment_gateway")
    private String paymentGateway;

    @SerializedName("result")
    private String result;

    @SerializedName("message")
    private String message;

    @SerializedName("total_amount")
    private Integer totalAmount;

    @SerializedName("paid_at")
    private String paidAt;

    @SerializedName("pi_tx_id")
    private String piTxId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getPaidAt() {
        return paidAt;
    }

    public void setPaidAt(String paidAt) {
        this.paidAt = paidAt;
    }

    public String getPiTxId() {
        return piTxId;
    }

    public void setPiTxId(String piTxId) {
        this.piTxId = piTxId;
    }
}
