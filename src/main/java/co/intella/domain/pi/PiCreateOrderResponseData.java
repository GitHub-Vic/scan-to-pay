package co.intella.domain.pi;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import javax.persistence.criteria.CriteriaBuilder;

/**
 * @author Alex
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PiCreateOrderResponseData {

    @JsonProperty(value = "partner_id")
    private String partnerId;

    @JsonProperty(value = "payment_account")
    private String paymentAccount;

    @JsonProperty(value = "order_id")
    private String orderId;

    @JsonProperty(value = "store_name")
    private String storeName;

    @JsonProperty(value = "created_at")
    private String createdAt;

    @JsonProperty(value = "total_amount")
    private Integer totalAmount;

    @JsonProperty(value = "fee")
    private Integer fee;

    @JsonProperty(value = "detail")
    private String detail;

    @JsonProperty(value = "app_trade_info")
    private String appTradeInfo;

    @JsonProperty(value = "error_code")
    private String errorCode;

    @JsonProperty(value = "error_message")
    private String errorMessage;

    @JsonProperty(value = "qrcode")
    private String qrcode;

    @JsonProperty(value = "payment_gateway")
    private String paymentGateway;

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getPaymentAccount() {
        return paymentAccount;
    }

    public void setPaymentAccount(String paymentAccount) {
        this.paymentAccount = paymentAccount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getFee() {
        return fee;
    }

    public void setFee(Integer fee) {
        this.fee = fee;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getAppTradeInfo() {
        return appTradeInfo;
    }

    public void setAppTradeInfo(String appTradeInfo) {
        this.appTradeInfo = appTradeInfo;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }
}
