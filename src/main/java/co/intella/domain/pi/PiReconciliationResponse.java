package co.intella.domain.pi;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Andy Lin
 */
public class PiReconciliationResponse {

    @JsonProperty("result")
    @SerializedName("result")
    private String result;

    @JsonProperty("err_code")
    @SerializedName("err_code")
    private String errorCode;

    @JsonProperty("accepted_subtotal")
    @SerializedName("accepted_subtotal")
    private String acceptedSubtotal;

    @JsonProperty("refunded_subtotal")
    @SerializedName("refunded_subtotal")
    private String refundedSubtotal;

    @JsonProperty("fee")
    @SerializedName("fee")
    private String fee;

    @JsonProperty("appropriation_date")
    @SerializedName("appropriation_date")
    private String appropriationDate;

    @JsonProperty("accept_record")
    @SerializedName("accept_record")
    private List <PiReconciliationAcceptRecord> acceptRecord;


    @JsonProperty("refund_record")
    @SerializedName("refund_record")
    private List <PiReconciliationRefundRecord> refundRecord;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getAcceptedSubtotal() {
        return acceptedSubtotal;
    }

    public void setAcceptedSubtotal(String acceptedSubtotal) {
        this.acceptedSubtotal = acceptedSubtotal;
    }

    public String getRefundedSubtotal() {
        return refundedSubtotal;
    }

    public void setRefundedSubtotal(String refundedSubtotal) {
        this.refundedSubtotal = refundedSubtotal;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getAppropriationDate() {
        return appropriationDate;
    }

    public void setAppropriationDate(String appropriationDate) {
        this.appropriationDate = appropriationDate;
    }

    public List<PiReconciliationAcceptRecord> getAcceptRecord() {
        return acceptRecord;
    }

    public void setAcceptRecord(List<PiReconciliationAcceptRecord> acceptRecord) {
        this.acceptRecord = acceptRecord;
    }

    public List<PiReconciliationRefundRecord> getRefundRecord() {
        return refundRecord;
    }

    public void setRefundRecord(List<PiReconciliationRefundRecord> refundRecord) {
        this.refundRecord = refundRecord;
    }
}
