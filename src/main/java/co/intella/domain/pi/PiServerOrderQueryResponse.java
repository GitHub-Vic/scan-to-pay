package co.intella.domain.pi;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class PiServerOrderQueryResponse {

    public static final String NORMAL = "0";

    public static final String INVALID_DATA = "20001";

    public static final String ORDER_EXCEPTION_FROM_INTELLA = "20002";

    public static final String ORDER_PAID_ALREADY = "20003";

    public static final String BANK_EXCEPTION = "20004";

    public static final String ORDER_NOT_FOUND = "20005";

    public static final String REFUND_AMOUNT_INCORRECT = "20006";

    public static final String REFUND_DATE_OVERDUE = "20007";

    public static final String PARTNER_KEY_NOT_FOUND = "30001";

    public static final String PARTNER_KEY_INCORRECT = "30002";

    public static final String PARTNER_ID_NOT_FOUND = "30003";

    public static final String PARTNER_CREATED_FAILED_WITH_PARTNER_ID = "30004";

    public static final String PAID_FAIL = "1010";

    public static final String BANK_AUTH_FAILED = "1012";

    public static final String BANK_CONNECT_FAILED = "1016";

    public static final String TRADE_FAILED_OR_NOT_FOUND = "1020";

    public static final String AUTH_NOT_GET = "1022";

    public static final String USER_CANNOT_PAY = "1024";

    private String rn;

    private String id;

    private String result;

    @SerializedName("partner_id")
    private String partnerId;

    @SerializedName("partner_name")
    private String partnerName;

    @SerializedName("partner_user_id")
    private String partnerUserId;

    @SerializedName("partner_order_id")
    private String partnerOrderId;

    private String status;

    private String money;

    private String fee;

    private String mobile;

    @SerializedName("mobile_validated")
    private String mobileValidated;

    @SerializedName("err_code")
    private String errorCode;

    @SerializedName("paid_at")
    private String paidAt;

    @SerializedName("refunded_at")
    private String refundAt;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getPartnerUserId() {
        return partnerUserId;
    }

    public void setPartnerUserId(String partnerUserId) {
        this.partnerUserId = partnerUserId;
    }

    public String getPartnerOrderId() {
        return partnerOrderId;
    }

    public void setPartnerOrderId(String partnerOrderId) {
        this.partnerOrderId = partnerOrderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobileValidated() {
        return mobileValidated;
    }

    public void setMobileValidated(String mobileValidated) {
        this.mobileValidated = mobileValidated;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getRn() {
        return rn;
    }

    public void setRn(String rn) {
        this.rn = rn;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPaidAt() {
        return paidAt;
    }

    public void setPaidAt(String paidAt) {
        this.paidAt = paidAt;
    }

    public String getRefundAt() {
        return refundAt;
    }

    public void setRefundAt(String refundAt) {
        this.refundAt = refundAt;
    }
}
