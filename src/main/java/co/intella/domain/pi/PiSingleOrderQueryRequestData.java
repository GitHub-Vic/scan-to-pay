package co.intella.domain.pi;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class PiSingleOrderQueryRequestData extends PiBasicRequestData {

    @SerializedName("SysOrderNo")
    @JsonProperty(value = "transaction_id")
    private String sysOrderNo;

    @SerializedName("StoreOrderNo")
    @JsonProperty(value = "bill_id", required = true)
    private String StoreOrderNo;

    @SerializedName("channel_id")
    @JsonProperty(value = "channel_id", required = true)
    private String mchId;

    public String getSysOrderNo() {
        return sysOrderNo;
    }

    public void setSysOrderNo(String sysOrderNo) {
        this.sysOrderNo = sysOrderNo;
    }

    public String getStoreOrderNo() {
        return StoreOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        StoreOrderNo = storeOrderNo;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }
}
