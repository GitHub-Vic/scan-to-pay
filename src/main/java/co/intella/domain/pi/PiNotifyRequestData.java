package co.intella.domain.pi;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Alex
 */

public class PiNotifyRequestData {

    @JsonProperty("partner_id")
    private String partnerId;

    @JsonProperty("order_id")
    private String orderId;

    @JsonProperty("payment_gateway")
    private String paymentGateway;

    @JsonProperty("result")
    private String result;

    @JsonProperty("message")
    private String message;

    @JsonProperty("total_amount")
    private Integer totalAmount;

    @JsonProperty("paid_at")
    private String paidAt;

    @JsonProperty("pi_tx_id")
    private String txId;

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getPaidAt() {
        return paidAt;
    }

    public void setPaidAt(String paidAt) {
        this.paidAt = paidAt;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }
}
