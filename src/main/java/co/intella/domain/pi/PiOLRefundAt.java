package co.intella.domain.pi;

import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class PiOLRefundAt {

    @SerializedName("date")
    private String date;

    @SerializedName("timezone_type")
    private String timezoneType;

    @SerializedName("timezone")
    private String timeZone;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimezoneType() {
        return timezoneType;
    }

    public void setTimezoneType(String timezoneType) {
        this.timezoneType = timezoneType;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }
}
