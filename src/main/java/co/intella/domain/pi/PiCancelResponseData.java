package co.intella.domain.pi;

import co.intella.domain.integration.ResponseGeneralData;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class PiCancelResponseData extends ResponseGeneralData {

    @SerializedName("bill_id")
    @JsonProperty(value = "bill_id", required = true)
    private String storeOrderNo;

    @SerializedName("status")
    @JsonProperty(value = "status")
    private String statusDesc;

    @SerializedName("error_code")
    @JsonProperty(value = "error_code")
    private String statusCode;

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
