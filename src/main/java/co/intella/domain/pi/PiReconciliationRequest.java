package co.intella.domain.pi;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Andy Lin
 */
public class PiReconciliationRequest {

    @JsonProperty("partner_id")
    private String partnerId;

    @JsonProperty("partner_key")
    private String partnerKey;

    @JsonProperty("date")
    private String date;

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerKey() {
        return partnerKey;
    }

    public void setPartnerKey(String partnerKey) {
        this.partnerKey = partnerKey;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
