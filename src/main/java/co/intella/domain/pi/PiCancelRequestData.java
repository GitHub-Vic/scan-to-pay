package co.intella.domain.pi;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class PiCancelRequestData extends PiBasicRequestData{
    @SerializedName("channel_id")
    @JsonProperty(value = "channel_id", required = true)
    private String mchId;

    @SerializedName("StoreOrderNo")
    @JsonProperty(value = "bill_id", required = true)
    private String storeOrderNo;

    @SerializedName("StoreInfo")
    @JsonProperty(value = "store_no")
    private String storeInfo;

    @SerializedName("DeviceInfo")
    @JsonProperty(value = "reg_id")
    private String deviceInfo;

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getStoreInfo() {
        return storeInfo;
    }

    public void setStoreInfo(String storeInfo) {
        this.storeInfo = storeInfo;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }
}
