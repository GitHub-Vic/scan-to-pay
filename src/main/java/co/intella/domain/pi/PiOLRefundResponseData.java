package co.intella.domain.pi;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class PiOLRefundResponseData {

    @SerializedName("order_id")
    private String orderId;

    @SerializedName("result")
    private String result;

    @SerializedName("error_code")
    private Integer errorCode;

    @SerializedName("refund_amount")
    private String refundAmount;

    @SerializedName("refund_at")
    private PiOLRefundAt refundAt;

    @SerializedName("pi_tx_id")
    private String txId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    public PiOLRefundAt getRefundAt() {
        return refundAt;
    }

    public void setRefundAt(PiOLRefundAt refundAt) {
        this.refundAt = refundAt;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }
}
