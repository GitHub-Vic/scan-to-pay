package co.intella.domain.pi;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class PiReconciliationAcceptRecord {

    @JsonProperty("partner_order_id")
    @SerializedName("partner_order_id")
    private String partnerOrderId;

    @JsonProperty("payer_mobile_phone")
    @SerializedName("payer_mobile_phone")
    private String payerMobilePhone;

    @JsonProperty("money")
    @SerializedName("money")
    private String money;

    @JsonProperty("created_at")
    @SerializedName("created_at")
    private String createdAt;

    @JsonProperty("paid_at")
    @SerializedName("paid_at")
    private String paidAt;

    public String getPartnerOrderId() {
        return partnerOrderId;
    }

    public void setPartnerOrderId(String partnerOrderId) {
        this.partnerOrderId = partnerOrderId;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getPaidAt() {
        return paidAt;
    }

    public void setPaidAt(String paidAt) {
        this.paidAt = paidAt;
    }

    public String getPayerMobilePhone() {
        return payerMobilePhone;
    }

    public void setPayerMobilePhone(String payerMobilePhone) {
        this.payerMobilePhone = payerMobilePhone;
    }
}
