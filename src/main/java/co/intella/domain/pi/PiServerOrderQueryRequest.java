package co.intella.domain.pi;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class PiServerOrderQueryRequest extends PiBasicRequestData {

    @JsonProperty(value = "partner_id", required = true)
    @SerializedName("partner_id")
    private String partnerId;

    @JsonProperty(value = "partner_order_id", required = true)
    @SerializedName("StoreOrderNo")
    private String partnerOrderId;

    @JsonProperty(value = "partner_key", required = true)
    @SerializedName("partner_key")
    private String partnerKey;

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerOrderId() {
        return partnerOrderId;
    }

    public void setPartnerOrderId(String partnerOrderId) {
        this.partnerOrderId = partnerOrderId;
    }

    public String getPartnerKey() {
        return partnerKey;
    }

    public void setPartnerKey(String partnerKey) {
        this.partnerKey = partnerKey;
    }
}
