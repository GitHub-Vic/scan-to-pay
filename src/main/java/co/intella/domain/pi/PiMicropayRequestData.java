package co.intella.domain.pi;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PiMicropayRequestData extends PiBasicRequestData {

    @SerializedName("channel_id")
    @JsonProperty(value = "channel_id", required = true)
    private String mchId;

    @SerializedName("create_time")
    @JsonProperty(value = "create_time", required = true)
    private String createTime;

    @SerializedName("StoreOrderNo")
    @JsonProperty(value = "bill_id", required = true)
    private String storeOrderNo;

    @SerializedName("DeviceInfo")
    @JsonProperty(value = "reg_id", required = true)
    private String deviceInfo;

    @SerializedName("Body")
    @JsonProperty(value = "items", required = true)
    private String body;

    @SerializedName("TotalFee")
    @JsonProperty(value = "amt", required = true)
    private String totalFee;

    @SerializedName("StoreInfo")
    @JsonProperty(value = "store_no")
    private String storeInfo;

    @SerializedName("Attach")
    @JsonProperty(value = "seq_no")
    private String attach;

    @SerializedName("AuthCode")
    @JsonProperty(value = "barcode", required = true)
    private String authCode;

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getStoreInfo() {
        return storeInfo;
    }

    public void setStoreInfo(String storeInfo) {
        this.storeInfo = storeInfo;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
