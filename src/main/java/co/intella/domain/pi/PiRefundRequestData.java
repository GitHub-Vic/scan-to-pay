package co.intella.domain.pi;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class PiRefundRequestData extends PiBasicRequestData {

    @SerializedName("StoreOrderNo")
    @JsonProperty(value = "bill_id")
    private String storeOrderNo;

    @SerializedName("SysOrderNo")
    @JsonProperty(value = "transaction_id")
    private String sysOrderNo;

    @SerializedName("RefundFee")
    @JsonProperty(value = "refund_amount")
    private String refundFee;

//    @SerializedName("RefundedMsg")
//    @JsonProperty(value = "refunded_message")
//    private String refundedMsg;

    @SerializedName("MchId")
    @JsonProperty(value = "channel_id")
    private String mchId;

    @SerializedName("CreateTime")
    @JsonProperty(value = "create_time")
    private String createTime;

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getSysOrderNo() {
        return sysOrderNo;
    }

    public void setSysOrderNo(String sysOrderNo) {
        this.sysOrderNo = sysOrderNo;
    }

    public String getRefundFee() {
        return refundFee;
    }

    public void setRefundFee(String refundFee) {
        this.refundFee = refundFee;
    }

//    public String getRefundedMsg() {
//        return refundedMsg;
//    }
//
//    public void setRefundedMsg(String refundedMsg) {
//        this.refundedMsg = refundedMsg;
//    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
