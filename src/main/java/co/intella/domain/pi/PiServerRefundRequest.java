package co.intella.domain.pi;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class PiServerRefundRequest extends PiBasicRequestData{

    @JsonProperty(value = "partner_id", required = true)
    @SerializedName("partner_id")
    private String partnerId;

    @JsonProperty(value = "partner_order_id", required = true)
    @SerializedName("StoreOrderNo")
    private String partnerOrderId;

    @JsonProperty(value = "partner_money", required = true)
    @SerializedName("TotalFee")
    private String partnerMoney;

    @JsonProperty(value = "partner_key", required = true)
    @SerializedName("MchKey")
    private String partnerKey;

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerOrderId() {
        return partnerOrderId;
    }

    public void setPartnerOrderId(String partnerOrderId) {
        this.partnerOrderId = partnerOrderId;
    }

    public String getPartnerMoney() {
        return partnerMoney;
    }

    public void setPartnerMoney(String partnerMoney) {
        this.partnerMoney = partnerMoney;
    }

    public String getPartnerKey() {
        return partnerKey;
    }

    public void setPartnerKey(String partnerKey) {
        this.partnerKey = partnerKey;
    }
}
