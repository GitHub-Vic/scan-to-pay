package co.intella.domain.pi;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class PiInAppOrderQueryRequestData extends PiBasicRequestData {

    @JsonProperty(value = "partner_key", required = true)
    private String partnerKey;

    @JsonProperty(value = "partner_order_id", required = true)
    @SerializedName("partner_order_id")
    private String partnerOrderId;

    @JsonProperty(value = "partner_user_id", required = true)
    private String partnerUserId;

    public String getPartnerKey() {
        return partnerKey;
    }

    public void setPartnerKey(String partnerKey) {
        this.partnerKey = partnerKey;
    }

    public String getPartnerOrderId() {
        return partnerOrderId;
    }

    public void setPartnerOrderId(String partnerOrderId) {
        this.partnerOrderId = partnerOrderId;
    }

    public String getPartnerUserId() {
        return partnerUserId;
    }

    public void setPartnerUserId(String partnerUserId) {
        this.partnerUserId = partnerUserId;
    }
}
