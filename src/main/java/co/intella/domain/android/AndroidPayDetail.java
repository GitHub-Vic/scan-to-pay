package co.intella.domain.android;

/**
 * @author Miles
 */
public class AndroidPayDetail {

    private String cardholderName;

    private String cardNumber;

    private String expiryMonth;

    private String expiryYear;

    private String cardSecurityCode;

    private AndroidPayBillingAddress billingAddress;

    public String getCardholderName() {
        return cardholderName;
    }

    public void setCardholderName(String cardholderName) {
        this.cardholderName = cardholderName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpiryMonth() {
        return expiryMonth;
    }

    public void setExpiryMonth(String expiryMonth) {
        this.expiryMonth = expiryMonth;
    }

    public String getExpiryYear() {
        return expiryYear;
    }

    public void setExpiryYear(String expiryYear) {
        this.expiryYear = expiryYear;
    }

    public String getCardSecurityCode() {
        return cardSecurityCode;
    }

    public void setCardSecurityCode(String cardSecurityCode) {
        this.cardSecurityCode = cardSecurityCode;
    }

    public AndroidPayBillingAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(AndroidPayBillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }
}
