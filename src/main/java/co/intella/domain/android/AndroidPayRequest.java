package co.intella.domain.android;

/**
 * @author Miles
 */
public class AndroidPayRequest {

    private String methodName;

    private String shippingAddress;

    private String shippingOption;

    private String payerName;

    private String payerEmail;

    private String payerPhone;

    private AndroidPayDetail details;

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getShippingOption() {
        return shippingOption;
    }

    public void setShippingOption(String shippingOption) {
        this.shippingOption = shippingOption;
    }

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public String getPayerEmail() {
        return payerEmail;
    }

    public void setPayerEmail(String payerEmail) {
        this.payerEmail = payerEmail;
    }

    public String getPayerPhone() {
        return payerPhone;
    }

    public void setPayerPhone(String payerPhone) {
        this.payerPhone = payerPhone;
    }

    public AndroidPayDetail getDetails() {
        return details;
    }

    public void setDetails(AndroidPayDetail details) {
        this.details = details;
    }
}
