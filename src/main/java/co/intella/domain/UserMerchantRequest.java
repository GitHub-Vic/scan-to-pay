package co.intella.domain;

public class UserMerchantRequest {
	private String accountId;
	private String userId;
	private Long merchantSeqId;
	private String isDefault;
	private String pin;
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Long getMerchantSeqId() {
		return merchantSeqId;
	}
	public void setMerchantSeqId(Long merchantSeqId) {
		this.merchantSeqId = merchantSeqId;
	}
	public String getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
}
