package co.intella.domain.aipei;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class AipeiRequestData {

    @JsonProperty("app_id")
    @SerializedName("app_id")
    private String appId;

    @JsonProperty("app_key")
    @SerializedName("app_key")
    private String appKey;

    @JsonProperty("order_id")
    @SerializedName("StoreOrderNo")
    private String orderId;

    @JsonProperty("source")
    @SerializedName("source")
    private String source;

    @JsonProperty("return_url")
    @SerializedName("return_url")
    private String returnUrl;

    @JsonProperty("capture")
    @SerializedName("capture")
    private String detailed;

    @JsonProperty("order_validity")
    @SerializedName("order_validity")
    private String orderValidity;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getDetailed() {
        return detailed;
    }

    public void setDetailed(String detailed) {
        this.detailed = detailed;
    }

    public String getOrderValidity() {
        return orderValidity;
    }

    public void setOrderValidity(String orderValidity) {
        this.orderValidity = orderValidity;
    }
}
