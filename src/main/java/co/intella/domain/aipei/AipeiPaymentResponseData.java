package co.intella.domain.aipei;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AipeiPaymentResponseData {
    @JsonProperty("partner_id")
    @SerializedName("partner_id")
    private String partnerId;

    @JsonProperty("service")
    @SerializedName("service")
    private String service;

    @JsonProperty("data")
    @SerializedName("data")
    private String data;

    @JsonProperty("sign_type")
    @SerializedName("sign_type")
    private String signType;

    @JsonProperty("sign")
    @SerializedName("sign")
    private String sign;

    @JsonProperty("error_code")
    @SerializedName("error_code")
    private String errorCode;

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getSignType() {
        return signType;
    }

    public void setSignType(String signType) {
        this.signType = signType;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
