package co.intella.domain.aipei;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class AipeiPaymentResponseParaData {

    private state state;


    @JsonProperty("partner_trans_id")
    @SerializedName("partner_trans_id")
    private String partnerTransId;


    @JsonProperty("ap_trans_result")
    @SerializedName("ap_trans_result")
    private String apTransResult;

    @JsonProperty("ap_trans_status")
    @SerializedName("ap_trans_status")
    private String apTransStatus;

    @JsonProperty("ap_trans_id")
    @SerializedName("ap_trans_id")
    private String apTransId;

    @JsonProperty("ap_buyer_ref")
    @SerializedName("ap_buyer_ref")
    private String apBuyerRef;

    @JsonProperty("ap_pay_time")
    @SerializedName("ap_pay_time")
    private String apPayTime;

    @JsonProperty("trans_amount")
    @SerializedName("trans_amount")
    private Integer transAmount;

    @JsonProperty("trans_deduct")
    @SerializedName("trans_deduct")
    private Integer transDeduct;

    @JsonProperty("trans_points")
    @SerializedName("trans_points")
    private Integer transPoints;

    @JsonProperty("trans_discount")
    @SerializedName("trans_discount")
    private Integer transDiscount;

    @JsonProperty("trans_afford")
    @SerializedName("trans_afford")
    private Integer transAfford;

    @JsonProperty("trans_waived")
    @SerializedName("trans_waived")
    private Integer transWaived;

    @JsonProperty("currency")
    @SerializedName("currency")
    private String currency;

    @JsonProperty("partner_refund_id")
    @SerializedName("partner_refund_id")
    private String partnerRefundId;

    @JsonProperty("trans_refund")
    @SerializedName("trans_refund")
    private Integer transRefund;

    public String getPartnerTransId() {
        return partnerTransId;
    }

    public void setPartnerTransId(String partnerTransId) {
        this.partnerTransId = partnerTransId;
    }

    public String getApTransResult() {
        return apTransResult;
    }

    public void setApTransResult(String apTransResult) {
        this.apTransResult = apTransResult;
    }

    public String getApTransStatus() {
        return apTransStatus;
    }

    public void setApTransStatus(String apTransStatus) {
        this.apTransStatus = apTransStatus;
    }

    public String getApTransId() {
        return apTransId;
    }

    public void setApTransId(String apTransId) {
        this.apTransId = apTransId;
    }

    public String getApBuyerRef() {
        return apBuyerRef;
    }

    public void setApBuyerRef(String apBuyerRef) {
        this.apBuyerRef = apBuyerRef;
    }

    public String getApPayTime() {
        return apPayTime;
    }

    public void setApPayTime(String apPayTime) {
        this.apPayTime = apPayTime;
    }

    public Integer getTransAmount() {
        return transAmount;
    }

    public void setTransAmount(Integer transAmount) {
        this.transAmount = transAmount;
    }

    public Integer getTransDeduct() {
        return transDeduct;
    }

    public void setTransDeduct(Integer transDeduct) {
        this.transDeduct = transDeduct;
    }

    public Integer getTransPoints() {
        return transPoints;
    }

    public void setTransPoints(Integer transPoints) {
        this.transPoints = transPoints;
    }

    public Integer getTransDiscount() {
        return transDiscount;
    }

    public void setTransDiscount(Integer transDiscount) {
        this.transDiscount = transDiscount;
    }

    public Integer getTransAfford() {
        return transAfford;
    }

    public void setTransAfford(Integer transAfford) {
        this.transAfford = transAfford;
    }

    public Integer getTransWaived() {
        return transWaived;
    }

    public void setTransWaived(Integer transWaived) {
        this.transWaived = transWaived;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getTransRefund() {
        return transRefund;
    }

    public void setTransRefund(Integer transRefund) {
        this.transRefund = transRefund;
    }

    public String getPartnerRefundId() {
        return partnerRefundId;
    }

    public void setPartnerRefundId(String partnerRefundId) {
        this.partnerRefundId = partnerRefundId;
    }

    public void setState(state state){
        this.state = state;
    }

    public state getState(){
        return state;
    }
}
