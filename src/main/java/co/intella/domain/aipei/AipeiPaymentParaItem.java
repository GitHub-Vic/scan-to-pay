package co.intella.domain.aipei;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Alex
 */

public class AipeiPaymentParaItem {

    @JsonProperty("item_flags")
    @SerializedName("item_flags")
    private List<String> itemFlags;

    @JsonProperty("item_name")
    @SerializedName("item_name")
    private String itemName;

    @JsonProperty("item_memo")
    @SerializedName("item_memo")
    private String itemMemo;

    @JsonProperty("item_quantity")
    @SerializedName("item_quantity")
    private Integer itemQuantity;

    @JsonProperty("item_amount")
    @SerializedName("item_amount")
    private Integer itemAmount;

    @JsonProperty("delivery_id")
    @SerializedName("delivery_id")
    private String deliveryId;

    @JsonProperty("shopee_delivery_id")
    @SerializedName("shopee_delivery_id")
    private String shopeeDeliveryId;

    @JsonProperty("shopee_user_ref")
    @SerializedName("shopee_user_ref")
    private String shopeeUserRef;

    @JsonProperty("shopee_user_name")
    @SerializedName("shopee_user_name")
    private String shopeeUserName;

    public List<String> getItemFlags() {
        return itemFlags;
    }

    public void setItemFlags(List<String> itemFlags) {
        this.itemFlags = itemFlags;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemMemo() {
        return itemMemo;
    }

    public void setItemMemo(String itemMemo) {
        this.itemMemo = itemMemo;
    }

    public Integer getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(Integer itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    public Integer getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(Integer itemAmount) {
        this.itemAmount = itemAmount;
    }

    public String getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(String deliveryId) {
        this.deliveryId = deliveryId;
    }

    public String getShopeeDeliveryId() {
        return shopeeDeliveryId;
    }

    public void setShopeeDeliveryId(String shopeeDeliveryId) {
        this.shopeeDeliveryId = shopeeDeliveryId;
    }

    public String getShopeeUserRef() {
        return shopeeUserRef;
    }

    public void setShopeeUserRef(String shopeeUserRef) {
        this.shopeeUserRef = shopeeUserRef;
    }

    public String getShopeeUserName() {
        return shopeeUserName;
    }

    public void setShopeeUserName(String shopeeUserName) {
        this.shopeeUserName = shopeeUserName;
    }
}
