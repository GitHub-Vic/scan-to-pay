package co.intella.domain.aipei;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AipeiOrder {

    @JsonProperty("app_id")
    @SerializedName("app_id")
    private String appId;

    @JsonProperty("currency")
    @SerializedName("currency")
    private String currency;

    @JsonProperty("order_id")
    @SerializedName("StoreOrderNo")
    private String orderId;

    @JsonProperty("payable_amount")
    @SerializedName("payable_amount")
    private Integer payableAmount;

    @JsonProperty("expiry_time")
    @SerializedName("expiry_time")
    private Integer expiryTime;

    @JsonProperty("item_name")
    @SerializedName("item_name")
    private String itemName;

    @JsonProperty("item_image")
    @SerializedName("item_image")
    private String itemImage;

    @JsonProperty("extra_data")
    @SerializedName("extra_data")
    private String extraData;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(Integer payableAmount) {
        this.payableAmount = payableAmount;
    }

    public Integer getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(Integer expiryTime) {
        this.expiryTime = expiryTime;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public String getExtraData() {
        return extraData;
    }

    public void setExtraData(String extraData) {
        this.extraData = extraData;
    }
}
