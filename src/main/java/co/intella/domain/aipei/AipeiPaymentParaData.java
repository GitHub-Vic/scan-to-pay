package co.intella.domain.aipei;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Alex
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AipeiPaymentParaData {
    @JsonProperty("partner_trans_id")
    @SerializedName("StoreOrderNo")
    private String partnerTransId;

    @JsonProperty("partner_refund_id")
    @SerializedName("StoreRefundNo")
    private String partnerRefundId;

    @JsonProperty("refund_amount")
    @SerializedName("refund_amount")
    private Integer refundAmount;


    @JsonProperty("buyer_code_type")
    @SerializedName("buyer_code_type")
    private String buyerCodeType;

    @JsonProperty("buyer_code")
    @SerializedName("AuthCode")
    private String buyerCode;

    @JsonProperty("trans_create_time")
    @SerializedName("trans_create_time")
    private String transCreateTime;

    @JsonProperty("trans_name")
    @SerializedName("trans_name")
    private String transName;

    @JsonProperty("trans_amount")
    @SerializedName("trans_amount")
    private Integer transAmount;

    @JsonProperty("currency")
    @SerializedName("currency")
    private String currency;

    @JsonProperty("memo")
    @SerializedName("memo")
    private String memo;

    @JsonProperty("merchant_type")
    @SerializedName("merchant_type")
    private String merchantType;

    @JsonProperty("merchant_id")
    @SerializedName("merchant_id")
    private String merchantId;

    @JsonProperty("merchant_name")
    @SerializedName("merchant_name")
    private String merchantName;

    @JsonProperty("store_id")
    @SerializedName("store_id")
    private String storeId;

    @JsonProperty("store_name")
    @SerializedName("store_name")
    private String storeName;

    @JsonProperty("terminal_id")
    @SerializedName("DeviceInfo")
    private String terminalId;

    @JsonProperty("terminal_name")
    @SerializedName("terminal_name")
    private String terminalName;

    @JsonProperty("items")
    @SerializedName("items")
    private List<AipeiPaymentParaItem> items;

    public String getPartnerTransId() {
        return partnerTransId;
    }

    public void setPartnerTransId(String partnerTransId) {
        this.partnerTransId = partnerTransId;
    }

    public String getBuyerCodeType() {
        return buyerCodeType;
    }

    public void setBuyerCodeType(String buyerCodeType) {
        this.buyerCodeType = buyerCodeType;
    }

    public String getBuyerCode() {
        return buyerCode;
    }

    public void setBuyerCode(String buyerCode) {
        this.buyerCode = buyerCode;
    }

    public String getTransCreateTime() {
        return transCreateTime;
    }

    public void setTransCreateTime(String transCreateTime) {
        this.transCreateTime = transCreateTime;
    }

    public String getTransName() {
        return transName;
    }

    public void setTransName(String transName) {
        this.transName = transName;
    }

    public Integer getTransAmount() {
        return transAmount;
    }

    public void setTransAmount(Integer transAmount) {
        this.transAmount = transAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getMerchantType() {
        return merchantType;
    }

    public void setMerchantType(String merchantType) {
        this.merchantType = merchantType;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getTerminalName() {
        return terminalName;
    }

    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }

    public List<AipeiPaymentParaItem> getItems() {
        return items;
    }

    public void setItems(List<AipeiPaymentParaItem> items) {
        this.items = items;
    }

    public String getPartnerRefundId() {
        return partnerRefundId;
    }

    public void setPartnerRefundId(String partnerRefundId) {
        this.partnerRefundId = partnerRefundId;
    }

    public Integer getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(Integer refundAmount) {
        this.refundAmount = refundAmount;
    }
}
