package co.intella.domain.aipei;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AipeiGetOrderDetail {

    @JsonProperty("order_validity")
    @SerializedName("order_validity")
    private Integer orderValidity;

    @JsonProperty("order")
    @SerializedName("order")
    private AipeiOrder order;

    public Integer getOrderValidity() {
        return orderValidity;
    }

    public void setOrderValidity(Integer orderValidity) {
        this.orderValidity = orderValidity;
    }

    public AipeiOrder getOrder() {
        return order;
    }

    public void setOrder(AipeiOrder order) {
        this.order = order;
    }
}
