package co.intella.domain.focas;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.google.gson.annotations.SerializedName;

public class CreditAuthReq extends FOCASPayBaseReq {

	private String payType;

	private String merchantName;

	private String setCurrencyNote;

	private String authResURL;

	@SerializedName(value = "pan", alternate = {"CardId"})
	private String pan;

	@SerializedName(value = "expiry", alternate = {"ExpireDate"})
	private String expiry;

	@SerializedName(value = "cvv", alternate = {"ExtenNo"})
	private String cvv;

	private String periodNum;

	private String extend;

	private String customize;

	private String autoCap;

	public CreditAuthReq() {
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getSetCurrencyNote() {
		return setCurrencyNote;
	}

	public void setSetCurrencyNote(String setCurrencyNote) {
		this.setCurrencyNote = setCurrencyNote;
	}

	public String getAuthResURL() {
		return authResURL;
	}

	public void setAuthResURL(String authResURL) {
		this.authResURL = authResURL;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getExpiry() {
		return expiry;
	}

	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getPeriodNum() {
		return periodNum;
	}

	public void setPeriodNum(String periodNum) {
		this.periodNum = periodNum;
	}

	public String getExtend() {
		return extend;
	}

	public void setExtend(String extend) {
		this.extend = extend;
	}

	public String getCustomize() {
		return customize;
	}

	public void setCustomize(String customize) {
		this.customize = customize;
	}

	public String getAutoCap() {
		return autoCap;
	}

	public void setAutoCap(String autoCap) {
		this.autoCap = autoCap;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
