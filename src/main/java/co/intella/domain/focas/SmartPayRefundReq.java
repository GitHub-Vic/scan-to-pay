package co.intella.domain.focas;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "")
public class SmartPayRefundReq extends FOCASPayBaseReq {
	
	@ApiModelProperty(value = "交易的類別; 12：消費扣款退費", required = true)
	private int apiTransType;
	
	@JsonIgnore
	protected String purchAmt;

	public int getApiTransType() {
		return apiTransType;
	}

	public void setApiTransType(int apiTransType) {
		this.apiTransType = apiTransType;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
