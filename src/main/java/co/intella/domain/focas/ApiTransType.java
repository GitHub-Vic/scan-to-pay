package co.intella.domain.focas;

public interface ApiTransType {

	public static final int CREDIT_AUTH = 1; 
	public static final int CREDIT_REDIRECT_AUTH = 4; 
	public static final int CREDIT_CANCEL = 5; 
	public static final int CREDIT_REFUND = 8;

	int SMART_PAY_AUTH = 10;
	int SMART_PAY_CANCEL = 11;

	int UPOP_AUTH = 13;
	int UPOP_CANCEL = 14;
	int UPOP_REFOUNＤ = 15;

}
