package co.intella.domain.focas;

import com.google.gson.annotations.SerializedName;

public class FOCASPayBaseReq {

	protected int apiTransType;
	protected String merchantID;
	
	@SerializedName(value = "lidm", alternate = {"StoreOrderNo"})
	protected String lidm;
	
	@SerializedName(value = "purchAmt", alternate = {"TotalFee"})
	protected String purchAmt;

	public FOCASPayBaseReq() {
		super();
	}

	public int getApiTransType() {
		return apiTransType;
	}

	public void setApiTransType(int apiTransType) {
		this.apiTransType = apiTransType;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public String getLidm() {
		return lidm;
	}

	public void setLidm(String lidm) {
		this.lidm = lidm;
	}

	public String getPurchAmt() {
		return purchAmt;
	}

	public void setPurchAmt(String purchAmt) {
		this.purchAmt = purchAmt;
	}

}