package co.intella.domain.focas;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "")
public class CreditCancelAuthReq extends FOCASPayBaseReq {
	
	@ApiModelProperty(value = "交易的類別\n" + 
							  "5：取消授權\n" + 
							  "7：轉出請款檔\n" + 
							  "9：取消退貨", required = false)
	private int apiTransType;
	
	@JsonIgnore
	private String purchAmt;

	public int getApiTransType() {
		return apiTransType;
	}

	public void setApiTransType(int apiTransType) {
		this.apiTransType = apiTransType;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
