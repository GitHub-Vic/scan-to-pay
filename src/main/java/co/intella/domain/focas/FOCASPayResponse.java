package co.intella.domain.focas;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "交易執行結果")
@JsonInclude(Include.NON_NULL)
public class FOCASPayResponse {

	@ApiModelProperty(value = "交易狀態碼")
	private String status;

	@ApiModelProperty(value = "錯誤代碼")
	private String errCode;

	@ApiModelProperty(value = "失敗原因")
	private String errDesc;
	
	@ApiModelProperty(value = "回傳予客戶進行後續轉址")
	private String html;
	
	@ApiModelProperty(value = "收單銀行授權使用的特店代號")
	private String merchantID;
	
	@ApiModelProperty(value = "訂單編號")
	private String lidm;
	
	@ApiModelProperty(value = "卡號末四碼")
	private String lastPan4;
	
	@ApiModelProperty(value = "交易授權碼")
	private String authCode;
	
	@ApiModelProperty(value = "授權金額")
	private String authAmt;
	
	@ApiModelProperty(value = "授權之交易序號")
	private String xid;
	
	@ApiModelProperty(value = "信用卡卡別")
	private String cardBrand;
	
	@ApiModelProperty(value = "交易時間")
	private String authRespTime;
	
	@ApiModelProperty(value = "首期分期金額")
	private String downPayments;
	
	@ApiModelProperty(value = "其他每期金額")
	private String installmentPayments;
	
	@ApiModelProperty(value = "紅利交易活動代碼")
	private String bonusActionCode;
	
	@ApiModelProperty(value =
			"紅利折抵說明\n" +
			"0：依發卡行決定 (預設)\n" + 
			"1：不折抵\n" + 
			"2：全數折抵\n" + 
			"3：部份折抵")
	private String bonusDesc;
	
	@ApiModelProperty(value =
			"交易授權結果請依errcode為主，本欄位僅供參考。\n" + 
			"05：發卡行拒絕交易。\n" + 
			"L1：紅利點數不足")
	private String bonusRespCode;
	
	@ApiModelProperty(value = "紅利剩餘點數正負符號; P：紅利餘額點數為正數; N：紅利餘額點數為負數")
	private String bonusSign;
	
	@ApiModelProperty(value = "紅利餘額點數")
	private String bonusBalance;
	
	@ApiModelProperty(value = "扣抵紅利點數")
	private String bonusDeduct;
	
	@ApiModelProperty(value = "扣抵後消費金額")
	private String bonusDeductAmt;
	
	@ApiModelProperty(value = "商店金鑰到期日")
	private String keyExpiryDate;
	
	@ApiModelProperty(value = 
			"該筆訂單在FOCAS帳務系統的狀態，正整數型\n" + 
			"0表示僅授權成功\n" + 
			"1表示已經轉入請款檔\n" + 
			"2表已轉出請款檔\n" + 
			"3表取消訂單\n" + 
			"4表已退貨成功\n" + 
			"5表已取消退貨\n" + 
			"8表已退貨結帳\n" + 
			"9表已請款結帳")
	private String txstate;
	
	@ApiModelProperty(value = "退貨累積次數")
	private String refundCount;
	
	@ApiModelProperty(value = "退貨累積總金額")
	private String amtRefundTotal;
	
	@ApiModelProperty(value = "查詢訂單授權總金額")
	private String purchAmt; 

	@ApiModelProperty(value = "授權結果狀態")
	private String txstatus;
	
	@ApiModelProperty(value = "交易類別")
	private String trxType;
	
	@ApiModelProperty(value = "此次交易的序號")
	private String qid;
	
	@ApiModelProperty(value = "回應結果狀態")
	private String rescode;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrDesc() {
		return errDesc;
	}

	public void setErrDesc(String errDesc) {
		this.errDesc = errDesc;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public String getLidm() {
		return lidm;
	}

	public void setLidm(String lidm) {
		this.lidm = lidm;
	}

	public String getLastPan4() {
		return lastPan4;
	}

	public void setLastPan4(String lastPan4) {
		this.lastPan4 = lastPan4;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getAuthAmt() {
		return authAmt;
	}

	public void setAuthAmt(String authAmt) {
		this.authAmt = authAmt;
	}

	public String getXid() {
		return xid;
	}

	public void setXid(String xid) {
		this.xid = xid;
	}

	public String getCardBrand() {
		return cardBrand;
	}

	public void setCardBrand(String cardBrand) {
		this.cardBrand = cardBrand;
	}

	public String getAuthRespTime() {
		return authRespTime;
	}

	public void setAuthRespTime(String authRespTime) {
		this.authRespTime = authRespTime;
	}

	public String getDownPayments() {
		return downPayments;
	}

	public void setDownPayments(String downPayments) {
		this.downPayments = downPayments;
	}

	public String getInstallmentPayments() {
		return installmentPayments;
	}

	public void setInstallmentPayments(String installmentPayments) {
		this.installmentPayments = installmentPayments;
	}

	public String getBonusActionCode() {
		return bonusActionCode;
	}

	public void setBonusActionCode(String bonusActionCode) {
		this.bonusActionCode = bonusActionCode;
	}

	public String getBonusDesc() {
		return bonusDesc;
	}

	public void setBonusDesc(String bonusDesc) {
		this.bonusDesc = bonusDesc;
	}

	public String getBonusRespCode() {
		return bonusRespCode;
	}

	public void setBonusRespCode(String bonusRespCode) {
		this.bonusRespCode = bonusRespCode;
	}

	public String getBonusSign() {
		return bonusSign;
	}

	public void setBonusSign(String bonusSign) {
		this.bonusSign = bonusSign;
	}

	public String getBonusBalance() {
		return bonusBalance;
	}

	public void setBonusBalance(String bonusBalance) {
		this.bonusBalance = bonusBalance;
	}

	public String getBonusDeduct() {
		return bonusDeduct;
	}

	public void setBonusDeduct(String bonusDeduct) {
		this.bonusDeduct = bonusDeduct;
	}

	public String getBonusDeductAmt() {
		return bonusDeductAmt;
	}

	public void setBonusDeductAmt(String bonusDeductAmt) {
		this.bonusDeductAmt = bonusDeductAmt;
	}

	public String getKeyExpiryDate() {
		return keyExpiryDate;
	}

	public void setKeyExpiryDate(String keyExpiryDate) {
		this.keyExpiryDate = keyExpiryDate;
	}

	public String getTxstate() {
		return txstate;
	}

	public void setTxstate(String txstate) {
		this.txstate = txstate;
	}

	public String getRefundCount() {
		return refundCount;
	}

	public void setRefundCount(String refundCount) {
		this.refundCount = refundCount;
	}

	public String getAmtRefundTotal() {
		return amtRefundTotal;
	}

	public void setAmtRefundTotal(String amtRefundTotal) {
		this.amtRefundTotal = amtRefundTotal;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
