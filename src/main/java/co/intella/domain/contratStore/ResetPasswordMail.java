package co.intella.domain.contratStore;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Alex
 */

public class ResetPasswordMail {
    @JsonProperty(value = "username")
    private String userName;

    @JsonProperty(value = "subject")
    private String subject;

    @JsonProperty(value = "message")
    private String message;

    @JsonProperty(value = "email")
    private String email;

    @JsonProperty(value = "merchant")
    private String merchant;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }
}
