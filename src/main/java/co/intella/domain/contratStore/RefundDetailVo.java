package co.intella.domain.contratStore;

/**
 * @author Miles
 */
public class RefundDetailVo {

    private String refundDetailRandomId;

    private String orderId;

    private long amount;

    private String accountId;

    private String storeRefundId;

    private String systemRefundId;

    private String cashier;

    private String status;

    private String createTime;

    private String reason;

    private String method;

    private String platformRefundDate;

    private boolean reconciliation;

    private boolean isPartialRefund;

    private String batchNo;

    private String rrn;

    private String ezcTxnDate;

    private Boolean ezcCancel;

    private String txParams;

    public String getRefundDetailRandomId() {
        return refundDetailRandomId;
    }

    public void setRefundDetailRandomId(String refundDetailRandomId) {
        this.refundDetailRandomId = refundDetailRandomId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getStoreRefundId() {
        return storeRefundId;
    }

    public void setStoreRefundId(String storeRefundId) {
        this.storeRefundId = storeRefundId;
    }

    public String getSystemRefundId() {
        return systemRefundId;
    }

    public void setSystemRefundId(String systemRefundId) {
        this.systemRefundId = systemRefundId;
    }

    public String getCashier() {
        return cashier;
    }

    public void setCashier(String cashier) {
        this.cashier = cashier;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPlatformRefundDate() {
        return platformRefundDate;
    }

    public void setPlatformRefundDate(String platformRefundDate) {
        this.platformRefundDate = platformRefundDate;
    }

    public boolean isReconciliation() {
        return reconciliation;
    }

    public void setReconciliation(boolean reconciliation) {
        this.reconciliation = reconciliation;
    }

    public boolean isPartialRefund() {
        return isPartialRefund;
    }

    public void setPartialRefund(boolean partialRefund) {
        isPartialRefund = partialRefund;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getEzcTxnDate() {
        return ezcTxnDate;
    }

    public void setEzcTxnDate(String ezcTxnDate) {
        this.ezcTxnDate = ezcTxnDate;
    }

    public Boolean getEzcCancel() {
        return ezcCancel;
    }

    public void setEzcCancel(Boolean ezcCancel) {
        this.ezcCancel = ezcCancel;
    }

    public String getTxParams() {
        return txParams;
    }

    public void setTxParams(String txParams) {
        this.txParams = txParams;
    }
}
