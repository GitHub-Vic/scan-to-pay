package co.intella.domain.contratStore;

/**
 * @author Alex
 */

public class QrcodeAlias {

    private String aliasCurrent;

    private String aliasNew;

    private String merchant;

    private String storeOrderNo;

    private String name;

    public String getAliasCurrent() {
        return aliasCurrent;
    }

    public void setAliasCurrent(String aliasCurrent) {
        this.aliasCurrent = aliasCurrent;
    }

    public String getAliasNew() {
        return aliasNew;
    }

    public void setAliasNew(String aliasNew) {
        this.aliasNew = aliasNew;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
