package co.intella.domain.contratStore;

import co.intella.model.Merchant;

/**
 * @author Andy Lin
 */
public class MerchantAppLogin extends Merchant{

    private String parentMerchant;
    
    private String appImg;

    public String getParentMerchant() {
        return parentMerchant;
    }

    public void setParentMerchant(String parentMerchant) {
        this.parentMerchant = parentMerchant;
    }

	public String getAppImg() {
		return appImg;
	}

	public void setAppImg(String appImg) {
		this.appImg = appImg;
	}
    
}
