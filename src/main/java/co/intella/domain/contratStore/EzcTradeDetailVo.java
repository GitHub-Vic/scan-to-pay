package co.intella.domain.contratStore;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class EzcTradeDetailVo {

    @SerializedName("OrderId")
    private String orderId;

    @SerializedName("Payment")
    private long payment;

    @SerializedName("CreateDate")
    private String createDate;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public long getPayment() {
        return payment;
    }

    public void setPayment(long payment) {
        this.payment = payment;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}
