package co.intella.domain.contratStore;

/**
 * @author Miles
 */
public class TradeDetailVo {

    private String tradeDetailRandomId;

    private DeviceVo deviceRandomId;

    private String orderId;

    private int type;

    private String method;

    private long payment;

    private String description;

    private String createDate;

    private String payCode;

    private String status;

    private String accountId;

    private String storeRefundId;

    private String systemRefundId;

    private String serviceType;

    private String cashier;

    private String tradeToken;

    private String storeInfo;

    private String systemOrderId;

    private String refundStatus;

    private String onlinePayUrl;

    private String barcode;

    private String mchName;

    private String MchKey;

    private String userId;

    private String qrcodeToken;

    private String platformPaidDate;

    private boolean reconciliation;

    private String deviceOS;

    private String creditOrderId;

    private boolean isOnSale;

    private double originalPrice;

    private String comment;

    private String batchNo;

    private String rrn;

    private String ezcTxnDate;

    private long carePoint;

    private String txParams;

    public long getCarePoint() {
        return carePoint;
    }

    public void setCarePoint(long carePoint) {
        this.carePoint = carePoint;
    }

    public String getTradeDetailRandomId() {
        return tradeDetailRandomId;
    }

    public void setTradeDetailRandomId(String tradeDetailRandomId) {
        this.tradeDetailRandomId = tradeDetailRandomId;
    }

    public DeviceVo getDeviceRandomId() {
        return deviceRandomId;
    }

    public void setDeviceRandomId(DeviceVo deviceRandomId) {
        this.deviceRandomId = deviceRandomId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public long getPayment() {
        return payment;
    }

    public void setPayment(long payment) {
        this.payment = payment;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getPayCode() {
        return payCode;
    }

    public void setPayCode(String payCode) {
        this.payCode = payCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getStoreRefundId() {
        return storeRefundId;
    }

    public void setStoreRefundId(String storeRefundId) {
        this.storeRefundId = storeRefundId;
    }

    public String getSystemRefundId() {
        return systemRefundId;
    }

    public void setSystemRefundId(String systemRefundId) {
        this.systemRefundId = systemRefundId;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getCashier() {
        return cashier;
    }

    public void setCashier(String cashier) {
        this.cashier = cashier;
    }

    public String getTradeToken() {
        return tradeToken;
    }

    public void setTradeToken(String tradeToken) {
        this.tradeToken = tradeToken;
    }

    public String getStoreInfo() {
        return storeInfo;
    }

    public void setStoreInfo(String storeInfo) {
        this.storeInfo = storeInfo;
    }

    public String getSystemOrderId() {
        return systemOrderId;
    }

    public void setSystemOrderId(String systemOrderId) {
        this.systemOrderId = systemOrderId;
    }

    public String getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(String refundStatus) {
        this.refundStatus = refundStatus;
    }

    public String getOnlinePayUrl() {
        return onlinePayUrl;
    }

    public void setOnlinePayUrl(String onlinePayUrl) {
        this.onlinePayUrl = onlinePayUrl;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getMchName() {
        return mchName;
    }

    public void setMchName(String mchName) {
        this.mchName = mchName;
    }

    public String getMchKey() {
        return MchKey;
    }

    public void setMchKey(String mchKey) {
        MchKey = mchKey;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getQrcodeToken() {
        return qrcodeToken;
    }

    public void setQrcodeToken(String qrcodeToken) {
        this.qrcodeToken = qrcodeToken;
    }

    public String getPlatformPaidDate() {
        return platformPaidDate;
    }

    public void setPlatformPaidDate(String platformPaidDate) {
        this.platformPaidDate = platformPaidDate;
    }

    public boolean isReconciliation() {
        return reconciliation;
    }

    public void setReconciliation(boolean reconciliation) {
        this.reconciliation = reconciliation;
    }

    public String getDeviceOS() {
        return deviceOS;
    }

    public void setDeviceOS(String deviceOS) {
        this.deviceOS = deviceOS;
    }

    public String getCreditOrderId() {
        return creditOrderId;
    }

    public void setCreditOrderId(String creditOrderId) {
        this.creditOrderId = creditOrderId;
    }

    public boolean isOnSale() {
        return isOnSale;
    }

    public void setOnSale(boolean onSale) {
        isOnSale = onSale;
    }

    public double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getEzcTxnDate() {
        return ezcTxnDate;
    }

    public void setEzcTxnDate(String ezcTxnDate) {
        this.ezcTxnDate = ezcTxnDate;
    }

    public String getTxParams() {
        return txParams;
    }

    public void setTxParams(String txParams) {
        this.txParams = txParams;
    }
}
