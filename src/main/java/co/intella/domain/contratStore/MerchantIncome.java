package co.intella.domain.contratStore;

/**
 * Created by Alexchiu on 2017/3/17.
 */
public class MerchantIncome {

	private String name;

	private String email;

	private String type;

	private Long creator;

	private String pin;

	private String accountId;

	private String comId;

	private String phone;

	private String signDate;

	private String status;

	private String newPin;

	private String token;

	private String refundPin;

	private String serialId;

	private String appId;

	private String platform;

	private String tokenId;

	private String userId;

	public String getSerialId() {
		return serialId;
	}

	public void setSerialId(String serialId) {
		this.serialId = serialId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getCreator() {
		return creator;
	}

	public void setCreator(Long creator) {
		this.creator = creator;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getComId() {
		return comId;
	}

	public void setComId(String comId) {
		this.comId = comId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSignDate() {
		return signDate;
	}

	public void setSignDate(String signDate) {
		this.signDate = signDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNewPin() {
		return newPin;
	}

	public void setNewPin(String newPin) {
		this.newPin = newPin;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getRefundPin() {
		return refundPin;
	}

	public void setRefundPin(String refundPin) {
		this.refundPin = refundPin;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "MerchantIncome [name=" + name + ", email=" + email + ", type=" + type + ", creator=" + creator
				+ ", pin=" + pin + ", accountId=" + accountId + ", comId=" + comId + ", phone=" + phone + ", signDate="
				+ signDate + ", status=" + status + ", newPin=" + newPin + ", token=" + token + ", refundPin="
				+ refundPin + ", serialId=" + serialId + ", appId=" + appId + ", platform=" + platform + ", tokenId="
				+ tokenId + ", userId=" + userId + "]";
	}
}
