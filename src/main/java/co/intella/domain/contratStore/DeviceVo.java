package co.intella.domain.contratStore;

/**
 * @author Miles
 */
public class DeviceVo {

    private String description;

    private String type;

    private String name;

    private String ownerDeviceId;

    private long batchNumber;

    private String ezcDongleId;

    private String aesKey;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerDeviceId() {
        return ownerDeviceId;
    }

    public void setOwnerDeviceId(String ownerDeviceId) {
        this.ownerDeviceId = ownerDeviceId;
    }

    public long getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(long batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getEzcDongleId() {
        return ezcDongleId;
    }

    public void setEzcDongleId(String ezcDongleId) {
        this.ezcDongleId = ezcDongleId;
    }

    public String getAesKey() {
        return aesKey;
    }

    public void setAesKey(String aesKey) {
        this.aesKey = aesKey;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aesKey == null) ? 0 : aesKey.hashCode());
		result = prime * result + (int) (batchNumber ^ (batchNumber >>> 32));
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((ezcDongleId == null) ? 0 : ezcDongleId.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((ownerDeviceId == null) ? 0 : ownerDeviceId.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeviceVo other = (DeviceVo) obj;
		if (aesKey == null) {
			if (other.aesKey != null)
				return false;
		} else if (!aesKey.equals(other.aesKey))
			return false;
		if (batchNumber != other.batchNumber)
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (ezcDongleId == null) {
			if (other.ezcDongleId != null)
				return false;
		} else if (!ezcDongleId.equals(other.ezcDongleId))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (ownerDeviceId == null) {
			if (other.ownerDeviceId != null)
				return false;
		} else if (!ownerDeviceId.equals(other.ownerDeviceId))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DeviceVo [description=" + description + ", type=" + type + ", name=" + name + ", ownerDeviceId="
				+ ownerDeviceId + ", batchNumber=" + batchNumber + ", ezcDongleId=" + ezcDongleId + ", aesKey=" + aesKey
				+ "]";
	}
	
}
