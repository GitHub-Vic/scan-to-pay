package co.intella.domain.contratStore;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class EzcReserveDetailVo {

    @SerializedName("OrderId")
    private String orderId;

    @SerializedName("Amount")
    private long amount;

    @SerializedName("CreateTime")
    private String createTime;

    @SerializedName("Status")
    private String status;

    @SerializedName("UserId")
    private String userId;

    @SerializedName("CancelStatus")
    private String cancelStatus;

    @SerializedName("CancelDate")
    private String cancelDate;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCancelStatus() {
        return cancelStatus;
    }

    public void setCancelStatus(String cancelStatus) {
        this.cancelStatus = cancelStatus;
    }

    public String getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(String cancelDate) {
        this.cancelDate = cancelDate;
    }
}
