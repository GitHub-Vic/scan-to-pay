package co.intella.domain.ntuImoto;

public class IMotoValuation {

    private String tokenOutercode;
    private String shortId;
    private String phoneNum;
    private String promoCode;

    public String getTokenOutercode() {
        return tokenOutercode;
    }

    public void setTokenOutercode(String tokenOutercode) {
        this.tokenOutercode = tokenOutercode;
    }

    public String getShortId() {
        return shortId;
    }

    public void setShortId(String shortId) {
        this.shortId = shortId;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    @Override
    public String toString() {
        return "IMotoValuation{" +
                "tokenOutercode='" + tokenOutercode + '\'' +
                ", shortId='" + shortId + '\'' +
                ", phoneNum='" + phoneNum + '\'' +
                ", promoCode='" + promoCode + '\'' +
                '}';
    }
}
