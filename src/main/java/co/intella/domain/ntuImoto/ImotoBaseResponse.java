package co.intella.domain.ntuImoto;

public class ImotoBaseResponse {

    private String enterTime;
    private String exitTime;
    private String lastExitTime;
    private String amount;
    private String promoCodeValid;
    private String msgCode;
    private String msg;
    private String sign;

    public String getEnterTime() {
        return enterTime;
    }

    public void setEnterTime(String enterTime) {
        this.enterTime = enterTime;
    }

    public String getExitTime() {
        return exitTime;
    }

    public void setExitTime(String exitTime) {
        this.exitTime = exitTime;
    }

    public String getLastExitTime() {
        return lastExitTime;
    }

    public void setLastExitTime(String lastExitTime) {
        this.lastExitTime = lastExitTime;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPromoCodeValid() {
        return promoCodeValid;
    }

    public void setPromoCodeValid(String promoCodeValid) {
        this.promoCodeValid = promoCodeValid;
    }

    public String getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(String msgCode) {
        this.msgCode = msgCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
