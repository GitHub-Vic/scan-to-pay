package co.intella.domain.ntuImoto;

import co.intella.model.ImotoToken;
import co.intella.model.IntTbLookupCode;
import co.intella.utility.JsonUtil;
import co.intella.utility.MD5Util;
import com.google.gson.annotations.SerializedName;

import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

public class ImotoValuationRquest {


    @SerializedName("TransData")
    private String transData;

    public ImotoValuationRquest() {
    }

    public ImotoValuationRquest(IMotoValuation iMotoValuation, ImotoToken imotoToken, IntTbLookupCode intTbLookupCode) {

        Map<String, String> map = new TreeMap<>();
        map.put("tokenInternalCode", imotoToken.getTokenInternalCode());
        map.put("promoCode", iMotoValuation.getPromoCode());
        boolean checkLookupCode = Objects.isNull(intTbLookupCode);
        map.put("intella", checkLookupCode ? "883650CCDB112" : intTbLookupCode.getType1());
        map.put("pssGroup", checkLookupCode ? "12681939@PSS" : intTbLookupCode.getType2());
        this.setSign(map);

    }

    private void setSign(Map<String, String> map) {
        String mapStr = JsonUtil.toJsonStr(map);
        String sign = MD5Util.getMD5(mapStr);
        map.remove("intella");
        map.remove("pssGroup");
        map.put("sign", sign);
    }

    public String getTransData() {
        return transData;
    }

    public void setTransData(String transData) {
        this.transData = transData;
    }
}
