package co.intella.domain;

import java.util.Date;

public class CheckoutCtrlRequest {
	private String accountId;
	private String startDate;
	private String endDate;
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	@Override
	public String toString() {
		return "CheckoutCtrlRequest [accountId=" + accountId + ", startDate=" + startDate + ", endDate=" + endDate
				+ "]";
	}
}
