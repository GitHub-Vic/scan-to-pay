package co.intella.domain;

/**
 * @author Miles
 */
public class StatusMessage {

    private String status;

    private String message;

    private String validateURL;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getValidateURL() {
        return validateURL;
    }

    public void setValidateURL(String validateURL) {
        this.validateURL = validateURL;
    }
}
