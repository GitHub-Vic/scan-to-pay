package co.intella.domain;

public class ApplePayTokenData {

	private String epochTimestamp;
	private String applicationPrimaryAccountNumber;
	private String applicationExpirationDate;
	private String currencyCode;
	private Integer transactionAmount;
	private String deviceManufacturerIdentifier;
	private String paymentDataType;
	private PaymentData paymentData;

	public class PaymentData {
		
		String onlinePaymentCryptogram;
		String eciIndicator;
		
		public String getOnlinePaymentCryptogram() {
			return onlinePaymentCryptogram;
		}
		public void setOnlinePaymentCryptogram(String onlinePaymentCryptogram) {
			this.onlinePaymentCryptogram = onlinePaymentCryptogram;
		}
		public String getEciIndicator() {
			return eciIndicator;
		}
		public void setEciIndicator(String eciIndicator) {
			this.eciIndicator = eciIndicator;
		}
	}

	public String getEpochTimestamp() {
		return epochTimestamp;
	}

	public void setEpochTimestamp(String epochTimestamp) {
		this.epochTimestamp = epochTimestamp;
	}

	public String getApplicationPrimaryAccountNumber() {
		return applicationPrimaryAccountNumber;
	}

	public void setApplicationPrimaryAccountNumber(String applicationPrimaryAccountNumber) {
		this.applicationPrimaryAccountNumber = applicationPrimaryAccountNumber;
	}

	public String getApplicationExpirationDate() {
		return applicationExpirationDate;
	}

	public void setApplicationExpirationDate(String applicationExpirationDate) {
		this.applicationExpirationDate = applicationExpirationDate;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Integer getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(Integer transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public String getDeviceManufacturerIdentifier() {
		return deviceManufacturerIdentifier;
	}

	public void setDeviceManufacturerIdentifier(String deviceManufacturerIdentifier) {
		this.deviceManufacturerIdentifier = deviceManufacturerIdentifier;
	}

	public String getPaymentDataType() {
		return paymentDataType;
	}

	public void setPaymentDataType(String paymentDataType) {
		this.paymentDataType = paymentDataType;
	}

	public PaymentData getPaymentData() {
		return paymentData;
	}

	public void setPaymentData(PaymentData paymentData) {
		this.paymentData = paymentData;
	}
}