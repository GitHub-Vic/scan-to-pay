package co.intella.domain;

public class PaymentRequestHeader extends PaymentHeader {

	public String tradeKey;
	public String createTime;

	public String getTradeKey() {
		return tradeKey;
	}

	public void setTradeKey(String tradeKey) {
		this.tradeKey = tradeKey;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

}
