package co.intella.domain.integration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class ResponseGeneralBody {

    @SerializedName("Header")
    @JsonProperty(value = "Header", required = true)
    private ResponseGeneralHeader header;

    @SerializedName("Data")
    @JsonProperty(value = "Data", required = true)
    private ResponseGeneralData data;

    public ResponseGeneralHeader getHeader() {
        return header;
    }

    public void setHeader(ResponseGeneralHeader header) {
        this.header = header;
    }

    public ResponseGeneralData getData() {
        return data;
    }

    public void setData(ResponseGeneralData data) {
        this.data = data;
    }
}
