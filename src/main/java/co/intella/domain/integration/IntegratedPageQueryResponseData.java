package co.intella.domain.integration;

/**
 * @author Miles
 */
public class IntegratedPageQueryResponseData extends ResponseGeneralData {

    private String list;

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

}
