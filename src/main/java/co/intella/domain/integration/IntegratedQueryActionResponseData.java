package co.intella.domain.integration;

/**
 * @author Alex
 */

public class IntegratedQueryActionResponseData extends ResponseGeneralData {

    private String orderNo;

    private String isCancelEnable;

    private String isRefundEnable;

    private double refundMax;

    private double refundMin;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getIsCancelEnable() {
        return isCancelEnable;
    }

    public void setIsCancelEnable(String isCancelEnable) {
        this.isCancelEnable = isCancelEnable;
    }

    public String getIsRefundEnable() {
        return isRefundEnable;
    }

    public void setIsRefundEnable(String isRefundEnable) {
        this.isRefundEnable = isRefundEnable;
    }

    public double getRefundMax() {
        return refundMax;
    }

    public void setRefundMax(double refundMax) {
        this.refundMax = refundMax;
    }

    public double getRefundMin() {
        return refundMin;
    }

    public void setRefundMin(double refundMin) {
        this.refundMin = refundMin;
    }
}
