package co.intella.domain.integration;

/**
 * @author Miles
 */
public class IntegratedPaymentResponseData extends ResponseGeneralData{

    private String sysOrderNo;

    private String storeOrderNo;

    private String totalFee;

    private String feeType;

    private String storeFeeRate;

    private String deviceInfo;

    private String body;

    private String barcodeMode;

    private String detail;

    private String storeInfo;

    private String cashier;

    private String reconcileTime;

    private String settlementTime;

    private String appropriationTime;

    private String establishedAt;

    private String paidAt;

    private String acceptedAt;

    private String rejectedAt;

    private String refundedAt;

    private String refundedMsg;

    private String extended;

    private String approveCode;

    private String transMode;

    private String secureStatus;

    private String firstAmount;

    private String eachAmount;

    private String fee;

    private String redeemType;

    private String redeemUsed;

    private String redeemBalance;

    private String riskMask;

    private String foreign;

    private String platformRsp;

    private String token;

    private long serialNumber;
    
    private String redirectUrl;

    public String getSysOrderNo() {
        return sysOrderNo;
    }

    public void setSysOrderNo(String sysOrderNo) {
        this.sysOrderNo = sysOrderNo;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getStoreFeeRate() {
        return storeFeeRate;
    }

    public void setStoreFeeRate(String storeFeeRate) {
        this.storeFeeRate = storeFeeRate;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getBarcodeMode() {
        return barcodeMode;
    }

    public void setBarcodeMode(String barcodeMode) {
        this.barcodeMode = barcodeMode;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getStoreInfo() {
        return storeInfo;
    }

    public void setStoreInfo(String storeInfo) {
        this.storeInfo = storeInfo;
    }

    public String getCashier() {
        return cashier;
    }

    public void setCashier(String cashier) {
        this.cashier = cashier;
    }

    public String getReconcileTime() {
        return reconcileTime;
    }

    public void setReconcileTime(String reconcileTime) {
        this.reconcileTime = reconcileTime;
    }

    public String getSettlementTime() {
        return settlementTime;
    }

    public void setSettlementTime(String settlementTime) {
        this.settlementTime = settlementTime;
    }

    public String getAppropriationTime() {
        return appropriationTime;
    }

    public void setAppropriationTime(String appropriationTime) {
        this.appropriationTime = appropriationTime;
    }

    public String getEstablishedAt() {
        return establishedAt;
    }

    public void setEstablishedAt(String establishedAt) {
        this.establishedAt = establishedAt;
    }

    public String getPaidAt() {
        return paidAt;
    }

    public void setPaidAt(String paidAt) {
        this.paidAt = paidAt;
    }

    public String getAcceptedAt() {
        return acceptedAt;
    }

    public void setAcceptedAt(String acceptedAt) {
        this.acceptedAt = acceptedAt;
    }

    public String getRejectedAt() {
        return rejectedAt;
    }

    public void setRejectedAt(String rejectedAt) {
        this.rejectedAt = rejectedAt;
    }

    public String getRefundedAt() {
        return refundedAt;
    }

    public void setRefundedAt(String refundedAt) {
        this.refundedAt = refundedAt;
    }

    public String getRefundedMsg() {
        return refundedMsg;
    }

    public void setRefundedMsg(String refundedMsg) {
        this.refundedMsg = refundedMsg;
    }

    public String getExtended() {
        return extended;
    }

    public void setExtended(String extended) {
        this.extended = extended;
    }

    public String getApproveCode() {
        return approveCode;
    }

    public void setApproveCode(String approveCode) {
        this.approveCode = approveCode;
    }

    public String getTransMode() {
        return transMode;
    }

    public void setTransMode(String transMode) {
        this.transMode = transMode;
    }

    public String getSecureStatus() {
        return secureStatus;
    }

    public void setSecureStatus(String secureStatus) {
        this.secureStatus = secureStatus;
    }

    public String getPlatformRsp() {
        return platformRsp;
    }

    public void setPlatformRsp(String platformRsp) {
        this.platformRsp = platformRsp;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public long getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(long serialNumber) {
        this.serialNumber = serialNumber;
    }

    public void setRedeemBalance(String redeemBalance) {
        this.redeemBalance = redeemBalance;
    }

    public String getForeign() {
        return foreign;
    }

    public void setForeign(String foreign) {
        this.foreign = foreign;
    }

    public String getFirstAmount() {
        return firstAmount;
    }

    public void setFirstAmount(String firstAmount) {
        this.firstAmount = firstAmount;
    }

    public String getEachAmount() {
        return eachAmount;
    }

    public void setEachAmount(String eachAmount) {
        this.eachAmount = eachAmount;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getRedeemType() {
        return redeemType;
    }

    public void setRedeemType(String redeemType) {
        this.redeemType = redeemType;
    }

    public String getRedeemUsed() {
        return redeemUsed;
    }

    public void setRedeemUsed(String redeemUsed) {
        this.redeemUsed = redeemUsed;
    }

    public String getRedeemBalance() {
        return redeemBalance;
    }

    public String getRiskMask() {
        return riskMask;
    }

    public void setRiskMask(String riskMask) {
        this.riskMask = riskMask;
    }

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
}
