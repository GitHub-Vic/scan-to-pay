package co.intella.domain.integration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class IntegratedMicropayResponseData extends ResponseGeneralData {
    @SerializedName("SysOrderNo")
    @JsonProperty(value = "SysOrderNo", required = true)
    private String sysOrderNo;

    @SerializedName("StoreOrderNo")
    @JsonProperty(value = "StoreOrderNo", required = true)
    private String storeOrderNo;

    @SerializedName("TotalFee")
    @JsonProperty(value = "TotalFee")
    private String totalFee;

    @SerializedName("SysRefundNo")
    @JsonProperty(value = "SysRefundNo")
    private String sysRefundNo;

    @SerializedName("AuthCode")
    @JsonProperty(value = "AuthCode")
    private String authCode;

    @SerializedName("Extended")
    @JsonProperty(value = "Extended")
    private String extended;

    @SerializedName("PlatformRsp")
    @JsonProperty(value = "PlatformRsp")
    private String platformRsp;

    @SerializedName("StoreRefundNo")
    @JsonProperty(value = "StoreRefundNo")
    private String storeRefundNo;

    public String getExtended() {
        return extended;
    }

    public void setExtended(String extended) {
        this.extended = extended;
    }

    public String getSysOrderNo() {
        return sysOrderNo;
    }

    public void setSysOrderNo(String sysOrderNo) {
        this.sysOrderNo = sysOrderNo;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getPlatformRsp() {
        return platformRsp;
    }

    public void setPlatformRsp(String platformRsp) {
        this.platformRsp = platformRsp;
    }

    public String getSysRefundNo() {
        return sysRefundNo;
    }

    public void setSysRefundNo(String sysRefundNo) {
        this.sysRefundNo = sysRefundNo;
    }

    public String getStoreRefundNo() {
        return storeRefundNo;
    }

    public void setStoreRefundNo(String storeRefundNo) {
        this.storeRefundNo = storeRefundNo;
    }
}
