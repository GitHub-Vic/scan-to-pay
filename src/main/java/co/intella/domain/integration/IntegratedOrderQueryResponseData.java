package co.intella.domain.integration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Andy Lin
 */
public class IntegratedOrderQueryResponseData extends ResponseGeneralData {

    @SerializedName("DataValue")
    @JsonProperty(value = "DataValue")
    private List<IntegratedOrder> list;

    public List<IntegratedOrder> getList() {
        return list;
    }

    public void setList(List<IntegratedOrder> list) {
        this.list = list;
    }
}
