package co.intella.domain.integration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class IntegratedCancelResponseData extends ResponseGeneralData{
    @SerializedName("StoreOrderNo")
    @JsonProperty(value = "StoreOrderNo", required = true)
    private String storeOrderNo;

    @SerializedName("PlatformRsp")
    @JsonProperty(value = "PlatformRsp")
    private String platformRsp;

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getPlatformRsp() {
        return platformRsp;
    }

    public void setPlatformRsp(String platformRsp) {
        this.platformRsp = platformRsp;
    }
}
