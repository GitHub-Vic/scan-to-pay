package co.intella.domain.integration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex Chiu
 */
public class IntegratedPartialRefundResponseData extends ResponseGeneralData {

    @SerializedName("RefundFee")
    @JsonProperty(value = "RefundFee", required = true)
    private String refundFee;

    @SerializedName("OrderFee")
    @JsonProperty(value = "OrderFee", required = true)
    private String orderFee;

    @SerializedName("StoreOrderNo")
    @JsonProperty(value = "StoreOrderNo", required = true)
    private String storeOrderNo;

    @SerializedName("StoreRefundNo")
    @JsonProperty(value = "StoreRefundNo", required = true)
    private String storeRefundNo;

    @SerializedName("RefundedAt")
    @JsonProperty(value = "RefundedAt")
    private String refundedAt;

    @SerializedName("PlatformRsp")
    @JsonProperty(value = "PlatformRsp")
    private String platformRsp;


    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getStoreRefundNo() {
        return storeRefundNo;
    }

    public void setStoreRefundNo(String storeRefundNo) {
        this.storeRefundNo = storeRefundNo;
    }

    public String getRefundedAt() {
        return refundedAt;
    }

    public void setRefundedAt(String refundedAt) {
        this.refundedAt = refundedAt;
    }

    public String getPlatformRsp() {
        return platformRsp;
    }

    public void setPlatformRsp(String platformRsp) {
        this.platformRsp = platformRsp;
    }

    public String getRefundFee() {
        return refundFee;
    }

    public void setRefundFee(String refundFee) {
        this.refundFee = refundFee;
    }

    public String getOrderFee() {
        return orderFee;
    }

    public void setOrderFee(String orderFee) {
        this.orderFee = orderFee;
    }
}
