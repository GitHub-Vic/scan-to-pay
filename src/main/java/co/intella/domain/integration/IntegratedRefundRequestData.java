package co.intella.domain.integration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class IntegratedRefundRequestData {

    @JsonProperty(value = "OutRefundNo", required = true)
    @SerializedName("StoreRefundNo")
    private String outRefundNo;

    @JsonProperty(value = "StoreOrderNo", required = true)
    @SerializedName("StoreOrderNo")
    private String storeOrderNo;

    @JsonProperty(value = "SysOrderNo", required = true)
    private String sysOrderNo;

    @JsonProperty(value = "DeviceInfo", required = true)
    private String deviceInfo;

    @JsonProperty(value = "StoreInfo", required = true)
    private String storeInfo;

    @JsonProperty(value = "Cashier", required = true)
    private String cashier;

    @JsonProperty(value = "TotalFee", required = true)
    private String totalFee;

    @JsonProperty(value = "RefundFeeType", required = true)
    private String refundFeeType;

    @JsonProperty(value = "RefundFee", required = true)
    @SerializedName("RefundFee")
    private String refundFee;

    @JsonProperty(value = "RefundedMsg", required = true)
    private String refundMsg;

    public String getOutRefundNo() {
        return outRefundNo;
    }

    public void setOutRefundNo(String outRefundNo) {
        this.outRefundNo = outRefundNo;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getSysOrderNo() {
        return sysOrderNo;
    }

    public void setSysOrderNo(String sysOrderNo) {
        this.sysOrderNo = sysOrderNo;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getRefundFeeType() {
        return refundFeeType;
    }

    public void setRefundFeeType(String refundFeeType) {
        this.refundFeeType = refundFeeType;
    }

    public String getRefundFee() {
        return refundFee;
    }

    public void setRefundFee(String refundFee) {
        this.refundFee = refundFee;
    }

    public String getRefundMsg() {
        return refundMsg;
    }

    public void setRefundMsg(String refundMsg) {
        this.refundMsg = refundMsg;
    }

    public String getStoreInfo() {
        return storeInfo;
    }

    public void setStoreInfo(String storeInfo) {
        this.storeInfo = storeInfo;
    }

    public String getCashier() {
        return cashier;
    }

    public void setCashier(String cashier) {
        this.cashier = cashier;
    }
}
