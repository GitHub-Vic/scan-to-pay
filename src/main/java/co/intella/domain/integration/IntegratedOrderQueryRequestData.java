package co.intella.domain.integration;

import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class IntegratedOrderQueryRequestData {

    @SerializedName(value = "DeviceInfo")
    private String deviceInfo;

    @SerializedName(value = "StartDate")
    private String startDate;

    @SerializedName(value = "EndDate")
    private String endDate;

    @SerializedName(value = "OrderStatus")
    private String orderStatus;

    @SerializedName(value = "Platform")
    private String platform;

    @SerializedName(value = "StoreOrderNo")
    private String storeOrderNo;

    @SerializedName(value = "StoreInfo")
    private String storeInfo;

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getStoreInfo() {
        return storeInfo;
    }

    public void setStoreInfo(String storeInfo) {
        this.storeInfo = storeInfo;
    }
}
