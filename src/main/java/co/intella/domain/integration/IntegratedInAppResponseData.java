package co.intella.domain.integration;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Alex
 */

public class IntegratedInAppResponseData extends ResponseGeneralData{

    @JsonProperty(value = "PartnerId")
    private String partnerId;

    @JsonProperty(value = "TradeToken")
    private String tradeToken;

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getTradeToken() {
        return tradeToken;
    }

    public void setTradeToken(String tradeToken) {
        this.tradeToken = tradeToken;
    }
}
