package co.intella.domain.integration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class IntegratedInAppRequest {

    @SerializedName("StoreOrderNo")
    @JsonProperty(value ="StoreOrderNo", required = true)
    private String storeOrderNo;

    @SerializedName("UserId")
    @JsonProperty(value = "UserId", required = true)
    private String userId;

    @SerializedName("TotalFee")
    @JsonProperty(value = "TotalFee", required = true)
    private String totalFee;

    @SerializedName("MchName")
    @JsonProperty(value = "MchName", required = true)
    private String mchName;

    @SerializedName("MchKey")
    @JsonProperty(value = "MchKey", required = true)
    private String mchKey;

    @SerializedName("Scheme")
    @JsonProperty(value = "Scheme")
    private String scheme;

    @SerializedName("Body")
    @JsonProperty(value = "Body", required = true)
    private String body;

    @SerializedName("Detail")
    @JsonProperty(value = "Detail")
    private String detail;

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getMchName() {
        return mchName;
    }

    public void setMchName(String mchName) {
        this.mchName = mchName;
    }

    public String getMchKey() {
        return mchKey;
    }

    public void setMchKey(String mchKey) {
        this.mchKey = mchKey;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
