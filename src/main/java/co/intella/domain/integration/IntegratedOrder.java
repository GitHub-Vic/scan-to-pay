package co.intella.domain.integration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy
 */
public class IntegratedOrder {

    @SerializedName("StoreOrderNo")
    @JsonProperty(value = "StoreOrderNo", required = true)
    private String storeOrderNo;

    @SerializedName("SysOrderNo")
    @JsonProperty(value = "SysOrderNo", required = true)
    private String sysOrderNo;

    @SerializedName("TotalFee")
    @JsonProperty(value = "TotalFee", required = true)
    private long totalFee;

    @SerializedName("FeeType")
    @JsonProperty(value = "FeeType")
    private String feeType;

    @SerializedName("StoreFeeRate")
    @JsonProperty(value = "StoreFeeRate")
    private String storeFeeRate;

    @SerializedName("DeviceInfo")
    @JsonProperty(value = "DeviceInfo", required = true)
    private String deviceInfo;

    @SerializedName("Detail")
    @JsonProperty(value = "Detail")
    private String detail;

    @SerializedName("Cashier")
    @JsonProperty(value = "Cashier")
    private String cashier;

    @SerializedName("Status")
    @JsonProperty(value = "Status")
    private String status;

    @SerializedName("RefundStatus")
    @JsonProperty(value = "RefundStatus", required = true)
    private String refundStatus;

    @SerializedName("Description")
    @JsonProperty(value = "Description")
    private String description;

    @SerializedName("Method")
    @JsonProperty(value = "Method")
    private String method;

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getSysOrderNo() {
        return sysOrderNo;
    }

    public void setSysOrderNo(String sysOrderNo) {
        this.sysOrderNo = sysOrderNo;
    }

    public long getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(long totalFee) {
        this.totalFee = totalFee;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getStoreFeeRate() {
        return storeFeeRate;
    }

    public void setStoreFeeRate(String storeFeeRate) {
        this.storeFeeRate = storeFeeRate;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getCashier() {
        return cashier;
    }

    public void setCashier(String cashier) {
        this.cashier = cashier;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(String refundStatus) {
        this.refundStatus = refundStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
