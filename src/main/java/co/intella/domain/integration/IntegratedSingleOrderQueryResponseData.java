package co.intella.domain.integration;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class IntegratedSingleOrderQueryResponseData extends ResponseGeneralData {

    @SerializedName("SysOrderNo")
    @JsonProperty(value = "SysOrderNo", required = true)
    private String sysOrderNo;

    @SerializedName("StoreOrderNo")
    @JsonProperty(value = "StoreOrderNo", required = true)
    private String storeOrderNo;

    @SerializedName("TotalFee")
    @JsonProperty(value = "TotalFee", required = true)
    private String totalFee;

    @SerializedName("FeeType")
    @JsonProperty(value = "FeeType", required = true)
    private String feeType;

    @SerializedName("StoreFeeRate")
    @JsonProperty(value = "StoreFeeRate", required = true)
    private String storeFeeRate;

    @SerializedName("DeviceInfo")
    @JsonProperty(value = "DeviceInfo", required = true)
    private String deviceInfo;

    @SerializedName("Body")
    @JsonProperty(value = "Body", required = true)
    private String body;

    @SerializedName("BarcodeMode")
    @JsonProperty(value = "BarcodeMode")
    private String barcodeMode;

    @SerializedName("Detail")
    @JsonProperty(value = "Detail")
    private String detail;

    @SerializedName("StoreInfo")
    @JsonProperty(value = "StoreInfo")
    private String storeInfo;

    @SerializedName("Cashier")
    @JsonProperty(value = "Cashier")
    private String cashier;

    @SerializedName("PlatformRsp")
    @JsonProperty(value = "PlatformRsp")
    private String platformRsp;

    @SerializedName("AuthCode")
    @JsonProperty(value = "AuthCode", required = true)
    private String authCode;

    @SerializedName("EstablishedAt")
    @JsonProperty(value = "EstablishedAt", required = true)
    private String establishedAt;

    @SerializedName("PaidAt")
    @JsonProperty(value = "PaidAt")
    private String paidAt;

    @SerializedName("AcceptedAt")
    @JsonProperty(value = "AcceptedAt")
    private String acceptedAt;

    @SerializedName("RejectedAt")
    @JsonProperty(value = "RejectedAt")
    private String rejectedAt;

    @SerializedName("RefundedAt")
    @JsonProperty(value = "RefundedAt")
    private String refundedAt;

    @SerializedName("RefundedMsg")
    @JsonProperty(value = "RefundedMsg")
    private String refundedMsg;

    @SerializedName("Extended")
    @JsonProperty(value = "Extended")
    private String extended;

    @SerializedName("OrderStatus")
    private String orderStatus;

    @SerializedName("OrderStatusDesc")
    private String orderStatusDesc;

    private String approveCode;

    private String intallmentType;

    private String firstAmt;

    private String eachAmt;

    private String fee;

    private String redeemBalance;

    private String redeemType;

    private String foreign;

    private String secureStatus;

    private String riskMask;

    // below field only used for in app response

    @SerializedName("MchName")
    private String mchName;

    @SerializedName("UserId")
    private String userId;

    @SerializedName("Mobile")
    private String mobile;

    @SerializedName("MobileValidated")
    private String mobileValidated;

    @SerializedName("LastFour")
    private String lastFour;

    @SerializedName("CardNum")
    private String cardNum;

    public String getExtended() {
        return extended;
    }

    public void setExtended(String extended) {
        this.extended = extended;
    }

    public String getSysOrderNo() {
        return sysOrderNo;
    }

    public void setSysOrderNo(String sysOrderNo) {
        this.sysOrderNo = sysOrderNo;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getStoreFeeRate() {
        return storeFeeRate;
    }

    public void setStoreFeeRate(String storeFeeRate) {
        this.storeFeeRate = storeFeeRate;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getBarcodeMode() {
        return barcodeMode;
    }

    public void setBarcodeMode(String barcodeMode) {
        this.barcodeMode = barcodeMode;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getStoreInfo() {
        return storeInfo;
    }

    public void setStoreInfo(String storeInfo) {
        this.storeInfo = storeInfo;
    }

    public String getPlatformRsp() {
        return platformRsp;
    }

    public void setPlatformRsp(String platformRsp) {
        this.platformRsp = platformRsp;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getEstablishedAt() {
        return establishedAt;
    }

    public void setEstablishedAt(String establishedAt) {
        this.establishedAt = establishedAt;
    }

    public String getPaidAt() {
        return paidAt;
    }

    public void setPaidAt(String paidAt) {
        this.paidAt = paidAt;
    }

    public String getAcceptedAt() {
        return acceptedAt;
    }

    public void setAcceptedAt(String acceptedAt) {
        this.acceptedAt = acceptedAt;
    }

    public String getRejectedAt() {
        return rejectedAt;
    }

    public void setRejectedAt(String rejectedAt) {
        this.rejectedAt = rejectedAt;
    }

    public String getRefundedAt() {
        return refundedAt;
    }

    public void setRefundedAt(String refundedAt) {
        this.refundedAt = refundedAt;
    }

    public String getRefundedMsg() {
        return refundedMsg;
    }

    public void setRefundedMsg(String refundedMsg) {
        this.refundedMsg = refundedMsg;
    }

    public String getCashier() {
        return cashier;
    }

    public void setCashier(String cashier) {
        this.cashier = cashier;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderStatusDesc() {
        return orderStatusDesc;
    }

    public void setOrderStatusDesc(String orderStatusDesc) {
        this.orderStatusDesc = orderStatusDesc;
    }

    public String getMchName() {
        return mchName;
    }

    public void setMchName(String mchName) {
        this.mchName = mchName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobileValidated() {
        return mobileValidated;
    }

    public void setMobileValidated(String mobileValidated) {
        this.mobileValidated = mobileValidated;
    }

    public String getApproveCode() {
        return approveCode;
    }

    public void setApproveCode(String approveCode) {
        this.approveCode = approveCode;
    }

    public String getIntallmentType() {
        return intallmentType;
    }

    public void setIntallmentType(String intallmentType) {
        this.intallmentType = intallmentType;
    }

    public String getFirstAmt() {
        return firstAmt;
    }

    public void setFirstAmt(String firstAmt) {
        this.firstAmt = firstAmt;
    }

    public String getEachAmt() {
        return eachAmt;
    }

    public void setEachAmt(String eachAmt) {
        this.eachAmt = eachAmt;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getRedeemBalance() {
        return redeemBalance;
    }

    public void setRedeemBalance(String redeemBalance) {
        this.redeemBalance = redeemBalance;
    }

    public String getRedeemType() {
        return redeemType;
    }

    public void setRedeemType(String redeemType) {
        this.redeemType = redeemType;
    }

    public String getForeign() {
        return foreign;
    }

    public void setForeign(String foreign) {
        this.foreign = foreign;
    }

    public String getSecureStatus() {
        return secureStatus;
    }

    public void setSecureStatus(String secureStatus) {
        this.secureStatus = secureStatus;
    }

    public String getRiskMask() {
        return riskMask;
    }

    public void setRiskMask(String riskMask) {
        this.riskMask = riskMask;
    }

    public String getLastFour() {
        return lastFour;
    }

    public void setLastFour(String lastFour) {
        this.lastFour = lastFour;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }
}
