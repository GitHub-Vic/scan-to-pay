package co.intella.domain.integration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class IntegratedRefundResponseData extends ResponseGeneralData {
    @SerializedName("SysOrderNo")
    @JsonProperty(value = "SysOrderNo", required = true)
    private String sysOrderNo;

    @SerializedName("SysRefundNo")
    @JsonProperty(value = "SysRefundNo", required = true)
    private String sysRefundNo;

    @SerializedName("StoreOrderNo")
    @JsonProperty(value = "StoreOrderNo", required = true)
    private String storeOrderNo;

    @SerializedName("StoreRefundNo")
    @JsonProperty(value = "StoreRefundNo", required = true)
    private String storeRefundNo;

    @SerializedName("RefundedAt")
    @JsonProperty(value = "RefundedAt")
    private String refundedAt;

    @SerializedName("PlatformRsp")
    @JsonProperty(value = "PlatformRsp")
    private String platformRsp;

    public String getSysOrderNo() {
        return sysOrderNo;
    }

    public void setSysOrderNo(String sysOrderNo) {
        this.sysOrderNo = sysOrderNo;
    }

    public String getSysRefundNo() {
        return sysRefundNo;
    }

    public void setSysRefundNo(String sysRefundNo) {
        this.sysRefundNo = sysRefundNo;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getStoreRefundNo() {
        return storeRefundNo;
    }

    public void setStoreRefundNo(String storeRefundNo) {
        this.storeRefundNo = storeRefundNo;
    }

    public String getRefundedAt() {
        return refundedAt;
    }

    public void setRefundedAt(String refundedAt) {
        this.refundedAt = refundedAt;
    }

    public String getPlatformRsp() {
        return platformRsp;
    }

    public void setPlatformRsp(String platformRsp) {
        this.platformRsp = platformRsp;
    }
}
