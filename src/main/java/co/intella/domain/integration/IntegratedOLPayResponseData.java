package co.intella.domain.integration;

/**
 * @author Alex
 */

public class IntegratedOLPayResponseData extends ResponseGeneralData{
    private String urlToken;

    public String getUrlToken() {
        return urlToken;
    }

    public void setUrlToken(String urlToken) {
        this.urlToken = urlToken;
    }
}
