package co.intella.domain.integration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class ResponseGeneralHeader {

    @SerializedName("StatusCode")
    @JsonProperty(value = "StatusCode", required = true)
    private String statusCode;

    @SerializedName("StatusDesc")
    @JsonProperty(value = "StatusDesc", required = true)
    private String statusDesc;

    @SerializedName("Method")
    @JsonProperty(value = "Method", required = true)
    private String method;

    @SerializedName("ServiceType")
    @JsonProperty(value = "ServiceType", required = true)
    private String serviceType;

    @SerializedName("MchId")
    @JsonProperty(value = "MchId", required = true)
    private String mchId;

    @SerializedName("ResponseTime")
    @JsonProperty(value = "ResponseTime", required = true)
    private String responseTime;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }
}
