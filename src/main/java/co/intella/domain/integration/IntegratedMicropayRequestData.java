package co.intella.domain.integration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @Author Andy Lin
 */
public class IntegratedMicropayRequestData {

    @JsonProperty(value = "StoreOrderNo", required = true)
    @SerializedName("StoreOrderNo")
    private String storeOrderNo;

    @JsonProperty(value = "DeviceInfo", required = true)
    @SerializedName("DeviceInfo")
    private String deviceInfo;

    @JsonProperty(value = "Body", required = true)
    @SerializedName("Body")
    private String body;

    @JsonProperty(value = "TotalFee", required = true)
    @SerializedName("TotalFee")
    private String totalFee;

    @JsonProperty(value = "AuthCode", required = true)
    @SerializedName("AuthCode")
    private String authCode;

    @JsonProperty(value = "FeeType", required = false)
    @SerializedName("FeeType")
    private String feeType;

    @JsonProperty(value = "Detail", required = false)
    @SerializedName("Detail")
    private String detail;

    @JsonProperty(value = "Attach", required = false)
    @SerializedName("Attach")
    private String attach;

    @JsonProperty(value = "GoodsTag", required = false)
    @SerializedName("GoodsTag")
    private String goodsTag;

    @JsonProperty(value = "StoreInfo", required = false)
    @SerializedName("StoreInfo")
    private String storeInfo;

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getGoodsTag() {
        return goodsTag;
    }

    public void setGoodsTag(String goodsTag) {
        this.goodsTag = goodsTag;
    }

    public String getStoreInfo() {
        return storeInfo;
    }

    public void setStoreInfo(String storeInfo) {
        this.storeInfo = storeInfo;
    }
}
