package co.intella.domain.integration;

import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.google.gson.annotations.SerializedName;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class InvoiceResponse extends ResponseGeneralData {

  @JsonProperty("SellerTAX")
  @SerializedName("SellerTAX")
  private String sellerTAX;

  @JsonProperty("TerminalID")
  @SerializedName("TerminalID")
  private String terminalID;

  @JsonProperty("PrinterID")
  @SerializedName("PrinterID")
  private String printerID;

  @JsonProperty("Invoice")
  @SerializedName("InvoiceData")
  private Map invoiceData;

  @JsonProperty("Receipt")
  @SerializedName("Receipt")
  private List<Map> receipt;

  @JsonProperty("status")
  @SerializedName("Status")
  private String status;

  @JsonInclude(Include.NON_ABSENT)
  @JsonProperty("takeNumber")
  @SerializedName("TakeNumber")
  private String takeNumber;

  public String getSellerTAX() {
    return sellerTAX;
  }

  public void setSellerTAX(String sellerTAX) {
    this.sellerTAX = sellerTAX;
  }

  public String getTerminalID() {
    return terminalID;
  }

  public void setTerminalID(String terminalID) {
    this.terminalID = terminalID;
  }

  public String getPrinterID() {
    return printerID;
  }

  public void setPrinterID(String printerID) {
    this.printerID = printerID;
  }

  public Map getInvoiceData() {
    return invoiceData;
  }

  public void setInvoiceData(Map invoiceData) {
    this.invoiceData = invoiceData;
  }

  public List<Map> getReceipt() {
    return receipt;
  }

  public void setReceipt(List<Map> receipt) {
    this.receipt = receipt;
  }

  public String getTakeNumber() {
    return takeNumber;
  }

  public void setTakeNumber(String takeNumber) {
    this.takeNumber = takeNumber;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }


}
