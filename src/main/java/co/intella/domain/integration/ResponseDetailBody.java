package co.intella.domain.integration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import co.intella.domain.easycard.EzCardBasicResponse;

public class ResponseDetailBody {
	@SerializedName("Header")
    @JsonProperty(value = "Header", required = true)
    private ResponseGeneralHeader header;

    @SerializedName("Data")
    @JsonProperty(value = "Data", required = true)
    private EzCardBasicResponse data;

    public ResponseGeneralHeader getHeader() {
        return header;
    }

    public void setHeader(ResponseGeneralHeader header) {
        this.header = header;
    }

    public EzCardBasicResponse getData() {
        return data;
    }

    public void setData(EzCardBasicResponse data) {
        this.data = data;
    }
}
