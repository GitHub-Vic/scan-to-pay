package co.intella.domain.integration;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Andy Lin
 */
public class IntegratedSingleOrderQueryRequestData {

    @JsonProperty(value = "OutTradeNo", required = true)
    private String outTradeNo;

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }
}
