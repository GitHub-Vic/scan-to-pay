package co.intella.domain.integration;

import java.util.List;

/**
 * @author Miles
 */
public class IntegratedOrderQuery1ResponseData extends ResponseGeneralData {

    private List list;

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

}
