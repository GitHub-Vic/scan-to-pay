package co.intella.domain;

import java.math.BigInteger;
import java.util.Date;

public class CheckoutCtrlVo {
	private String accountId;
	private String type;
	private String batchNo;
	private String comment;
	private String ownerDeviceId;
	private String method;
	private String status;
	private String source;
	private BigInteger ccSeqId; 
	private Date crDate;
	private Long TradeAmount;
	private Long RefundAmount;
	private Long ReserveAmount;
	private Long ReserveCancelAmount;
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getBatchNo() {
		return batchNo;
	}
	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getOwnerDeviceId() {
		return ownerDeviceId;
	}
	public void setOwnerDeviceId(String ownerDeviceId) {
		this.ownerDeviceId = ownerDeviceId;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public BigInteger getCcSeqId() {
		return ccSeqId;
	}
	public void setCcSeqId(BigInteger ccSeqId) {
		this.ccSeqId = ccSeqId;
	}
	public Date getCrDate() {
		return crDate;
	}
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}
	public Long getTradeAmount() {
		return TradeAmount;
	}
	public void setTradeAmount(Long tradeAmount) {
		TradeAmount = tradeAmount;
	}
	public Long getRefundAmount() {
		return RefundAmount;
	}
	public void setRefundAmount(Long refundAmount) {
		RefundAmount = refundAmount;
	}
	public Long getReserveAmount() {
		return ReserveAmount;
	}
	public void setReserveAmount(Long reserveAmount) {
		ReserveAmount = reserveAmount;
	}
	public Long getReserveCancelAmount() {
		return ReserveCancelAmount;
	}
	public void setReserveCancelAmount(Long reserveCancelAmount) {
		ReserveCancelAmount = reserveCancelAmount;
	}
	@Override
	public String toString() {
		return "CheckoutCtrlVo [accountId=" + accountId + ", type=" + type + ", batchNo=" + batchNo + ", comment="
				+ comment + ", ownerDeviceId=" + ownerDeviceId + ", status=" + status + ", source=" + source
				+ ", ccSeqId=" + ccSeqId + ", crDate=" + crDate + ", TradeAmount=" + TradeAmount + ", RefundAmount="
				+ RefundAmount + ", ReserveAmount=" + ReserveAmount + ", ReserveCancelAmount=" + ReserveCancelAmount
				+ "]";
	}
}