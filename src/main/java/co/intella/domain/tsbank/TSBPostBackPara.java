package co.intella.domain.tsbank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TSBPostBackPara {

    @JsonProperty("s_mid")
    @SerializedName("s_mid")
    private String sMid;

    @JsonProperty("ret_code")
    @SerializedName("ret_code")
    private String retCode;

    @JsonProperty("tx_type")
    @SerializedName("tx_type")
    private Integer txType;

    @JsonProperty("order_no")
    @SerializedName("order_no")
    private String orderNo;

    @JsonProperty("ret_msg")
    @SerializedName("ret_msg")
    private String retMsg;

    @JsonProperty("auth_id_resp")
    @SerializedName("auth_id_resp")
    private String authIdResp;


    public String getsMid() {
        return sMid;
    }

    public void setsMid(String sMid) {
        this.sMid = sMid;
    }

    public String getRetCode() {
        return retCode;
    }

    public void setRetCode(String retCode) {
        this.retCode = retCode;
    }

    public Integer getTxType() {
        return txType;
    }

    public void setTxType(Integer txType) {
        this.txType = txType;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getRetMsg() {
        return retMsg;
    }

    public void setRetMsg(String retMsg) {
        this.retMsg = retMsg;
    }

    public String getAuthIdResp() {
        return authIdResp;
    }

    public void setAuthIdResp(String authIdResp) {
        this.authIdResp = authIdResp;
    }
}
