package co.intella.domain.tsbank;

import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class TSBUnionResponseData {

    private String ver;

    private String mid;

    private String tid;

    @SerializedName("pay_type")
    private Integer payType;

    @SerializedName("tx_type")
    private Integer txType;


    private TSBUnionResponseDataPara params;



    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Integer getTxType() {
        return txType;
    }

    public void setTxType(Integer txType) {
        this.txType = txType;
    }

    public TSBUnionResponseDataPara getParams() {
        return params;
    }

    public void setParams(TSBUnionResponseDataPara params) {
        this.params = params;
    }


}
