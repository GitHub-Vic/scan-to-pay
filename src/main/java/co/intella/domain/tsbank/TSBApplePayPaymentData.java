package co.intella.domain.tsbank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Alex
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TSBApplePayPaymentData {

    @JsonProperty("onlinePaymentCryptogram")
    private String onlinePaymentCryptogram;

    @JsonProperty("eciIndicator")
    private String eciIndicator;

    public String getOnlinePaymentCryptogram() {
        return onlinePaymentCryptogram;
    }

    public void setOnlinePaymentCryptogram(String onlinePaymentCryptogram) {
        this.onlinePaymentCryptogram = onlinePaymentCryptogram;
    }

    public String getEciIndicator() {
        return eciIndicator;
    }

    public void setEciIndicator(String eciIndicator) {
        this.eciIndicator = eciIndicator;
    }
}
