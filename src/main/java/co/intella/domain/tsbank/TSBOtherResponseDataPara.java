package co.intella.domain.tsbank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TSBOtherResponseDataPara {

    @JsonProperty("ret_code")
    @SerializedName("ret_code")
    private String retCode;

    @JsonProperty("ret_msg")
    @SerializedName("ret_msg")
    private String retMsg;

    @JsonProperty("auth_id_resp")
    @SerializedName("auth_id_resp")
    private String authIdResp;

    @JsonProperty("order_no")
    @SerializedName("order_no")
    private String orderNo;

    private String rrn;

    @JsonProperty("order_status")
    @SerializedName("order_status")
    private String orderStatus;

    @JsonProperty("auth_type")
    @SerializedName("auth_type")
    private String authType;


    private String cur;

    //union
    private String qid;

    @JsonProperty("purchase_date")
    @SerializedName("purchase_date")
    private String purchaseDate;

    @JsonProperty("tx_amt")
    @SerializedName("tx_amt")
    private String txAmt;

    @JsonProperty("settle_amt")
    @SerializedName("settle_amt")
    private String settleAmt;

    @JsonProperty("settle_seq")
    @SerializedName("settle_seq")
    private String settleSeq;

    @JsonProperty("settle_date")
    @SerializedName("settle_date")
    private String settleDate;

    @JsonProperty("refund_trans_amt")
    @SerializedName("refund_trans_amt")
    private String refundTransAmt;

    @JsonProperty("refund_rrn")
    @SerializedName("refund_rrn")
    private String refundRrn;

    @JsonProperty("refund_auth_id_resp")
    @SerializedName("refund_auth_id_resp")
    private String refundAuthIdResp;

    @JsonProperty("refund_date")
    @SerializedName("refund_date")
    private String refundDate;

    @JsonProperty("redeem_order_no")
    @SerializedName("redeem_order_no")
    private String redeemOrderNo;


    @JsonProperty("redeem_pt")
    @SerializedName("redeem_pt")
    private String redeemPt;

    @JsonProperty("redeem_amt")
    @SerializedName("redeem_amt")
    private String redeemAmt;
    @JsonProperty("post_redeem_amt")
    @SerializedName("post_redeem_amt")
    private String postRedeemAmt;

    @JsonProperty("post_redeem_pt")
    @SerializedName("post_redeem_pt")
    private String postRedeemPt;

    @JsonProperty("install_order_no")
    @SerializedName("install_order_no")
    private String installOrderNo;

    @JsonProperty("install_period")
    @SerializedName("install_period")
    private String installPeriod;

    @JsonProperty("install_down_pay")
    @SerializedName("install_down_pay")
    private String installDownPay;

    @JsonProperty("install_pay")
    @SerializedName("install_pay")
    private String installPay;

    @JsonProperty("install_down_pay_fee")
    @SerializedName("install_down_pay_fee")
    private String installDownPayFee;

    @JsonProperty("install_pay_fee")
    @SerializedName("install_pay_fee")
    private String installPayFee;


    public String getRetCode() {
        return retCode;
    }

    public void setRetCode(String retCode) {
        this.retCode = retCode;
    }

    public String getRetMsg() {
        return retMsg;
    }

    public void setRetMsg(String retMsg) {
        this.retMsg = retMsg;
    }

    public String getAuthIdResp() {
        return authIdResp;
    }

    public void setAuthIdResp(String authIdResp) {
        this.authIdResp = authIdResp;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public String getCur() {
        return cur;
    }

    public void setCur(String cur) {
        this.cur = cur;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getTxAmt() {
        return txAmt;
    }

    public void setTxAmt(String txAmt) {
        this.txAmt = txAmt;
    }

    public String getSettleAmt() {
        return settleAmt;
    }

    public void setSettleAmt(String settleAmt) {
        this.settleAmt = settleAmt;
    }

    public String getSettleSeq() {
        return settleSeq;
    }

    public void setSettleSeq(String settleSeq) {
        this.settleSeq = settleSeq;
    }

    public String getSettleDate() {
        return settleDate;
    }

    public void setSettleDate(String settleDate) {
        this.settleDate = settleDate;
    }

    public String getRefundTransAmt() {
        return refundTransAmt;
    }

    public void setRefundTransAmt(String refundTransAmt) {
        this.refundTransAmt = refundTransAmt;
    }

    public String getRefundRrn() {
        return refundRrn;
    }

    public void setRefundRrn(String refundRrn) {
        this.refundRrn = refundRrn;
    }

    public String getRefundAuthIdResp() {
        return refundAuthIdResp;
    }

    public void setRefundAuthIdResp(String refundAuthIdResp) {
        this.refundAuthIdResp = refundAuthIdResp;
    }

    public String getRefundDate() {
        return refundDate;
    }

    public void setRefundDate(String refundDate) {
        this.refundDate = refundDate;
    }

    public String getRedeemOrderNo() {
        return redeemOrderNo;
    }

    public void setRedeemOrderNo(String redeemOrderNo) {
        this.redeemOrderNo = redeemOrderNo;
    }

    public String getRedeemPt() {
        return redeemPt;
    }

    public void setRedeemPt(String redeemPt) {
        this.redeemPt = redeemPt;
    }

    public String getRedeemAmt() {
        return redeemAmt;
    }

    public void setRedeemAmt(String redeemAmt) {
        this.redeemAmt = redeemAmt;
    }

    public String getPostRedeemAmt() {
        return postRedeemAmt;
    }

    public void setPostRedeemAmt(String postRedeemAmt) {
        this.postRedeemAmt = postRedeemAmt;
    }

    public String getPostRedeemPt() {
        return postRedeemPt;
    }

    public void setPostRedeemPt(String postRedeemPt) {
        this.postRedeemPt = postRedeemPt;
    }

    public String getInstallOrderNo() {
        return installOrderNo;
    }

    public void setInstallOrderNo(String installOrderNo) {
        this.installOrderNo = installOrderNo;
    }

    public String getInstallPeriod() {
        return installPeriod;
    }

    public void setInstallPeriod(String installPeriod) {
        this.installPeriod = installPeriod;
    }

    public String getInstallDownPay() {
        return installDownPay;
    }

    public void setInstallDownPay(String installDownPay) {
        this.installDownPay = installDownPay;
    }

    public String getInstallPay() {
        return installPay;
    }

    public void setInstallPay(String installPay) {
        this.installPay = installPay;
    }

    public String getInstallDownPayFee() {
        return installDownPayFee;
    }

    public void setInstallDownPayFee(String installDownPayFee) {
        this.installDownPayFee = installDownPayFee;
    }

    public String getInstallPayFee() {
        return installPayFee;
    }

    public void setInstallPayFee(String installPayFee) {
        this.installPayFee = installPayFee;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getQid() {
        return qid;
    }

    public void setQid(String qid) {
        this.qid = qid;
    }
}
