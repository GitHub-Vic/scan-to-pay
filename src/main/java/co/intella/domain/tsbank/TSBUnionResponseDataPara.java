package co.intella.domain.tsbank;

import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class TSBUnionResponseDataPara {

    @SerializedName("ret_code")
    private String retCode;

    @SerializedName("hpp_url")
    private String hppUrl;

    public String getRetCode() {
        return retCode;
    }

    public void setRetCode(String retCode) {
        this.retCode = retCode;
    }

    public String getHppUrl() {
        return hppUrl;
    }

    public void setHppUrl(String hppUrl) {
        this.hppUrl = hppUrl;
    }
}
