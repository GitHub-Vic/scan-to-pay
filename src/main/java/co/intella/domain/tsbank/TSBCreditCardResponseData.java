package co.intella.domain.tsbank;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class TSBCreditCardResponseData {
    private String sender;

    private String ver;


    private String mid;



    @SerializedName("s_mid")
    private String sMid;


    private String tid;

    @SerializedName("pay_type")
    private Integer payType;

    @SerializedName("tx_type")
    private Integer txType;


    private TSBCreditCardResponseDataPara params;


    public TSBCreditCardResponseDataPara getParams() {
        return params;
    }

    public void setParams(TSBCreditCardResponseDataPara params) {
        this.params = params;
    }
}
