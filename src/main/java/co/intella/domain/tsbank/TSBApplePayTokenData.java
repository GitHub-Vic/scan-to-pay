package co.intella.domain.tsbank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Alex
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TSBApplePayTokenData {

    private String merchantId;


    @JsonProperty("applicationPrimaryAccountNumber")
    private String applicationPrimaryAccountNumber;

    @JsonProperty("applicationExpirationDate")
    private String applicationExpirationDate;

    @JsonProperty("currencyCode")
    private String currencyCode;

    @JsonProperty("transactionAmount")
    private Integer transactionAmount;

    @JsonProperty("deviceManufacturerIdentifier")
    private String deviceManufacturerIdentifier;

    @JsonProperty("paymentDataType")
    private String paymentDataType;

    @JsonProperty("paymentData")
    private TSBApplePayPaymentData paymentData;


    public String getApplicationPrimaryAccountNumber() {
        return applicationPrimaryAccountNumber;
    }

    public void setApplicationPrimaryAccountNumber(String applicationPrimaryAccountNumber) {
        this.applicationPrimaryAccountNumber = applicationPrimaryAccountNumber;
    }

    public String getApplicationExpirationDate() {
        return applicationExpirationDate;
    }

    public void setApplicationExpirationDate(String applicationExpirationDate) {
        this.applicationExpirationDate = applicationExpirationDate;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Integer getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Integer transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getDeviceManufacturerIdentifier() {
        return deviceManufacturerIdentifier;
    }

    public void setDeviceManufacturerIdentifier(String deviceManufacturerIdentifier) {
        this.deviceManufacturerIdentifier = deviceManufacturerIdentifier;
    }

    public String getPaymentDataType() {
        return paymentDataType;
    }

    public void setPaymentDataType(String paymentDataType) {
        this.paymentDataType = paymentDataType;
    }

    public TSBApplePayPaymentData getPaymentData() {
        return paymentData;
    }

    public void setPaymentData(TSBApplePayPaymentData paymentData) {
        this.paymentData = paymentData;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }
}
