package co.intella.domain.tsbank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TSBCreditCardRequestDataPara {

    private String layout;

    @JsonProperty(value = "order_no")
    @SerializedName("StoreOrderNo")
    private String orderNo;

    @SerializedName("TotalFee")
    private String amt;

    private String cur;

    @JsonProperty(value = "order_desc")
    @SerializedName("Detail")
    private String orderDesc;

    @JsonProperty("capt_flag")
    private String captFlag;

    @JsonProperty("result_flag")
    private String resultFlag;

    @JsonProperty("post_back_url")
    private String postBackUrl;

    @JsonProperty("result_url")
    private String resultUrl;

    @JsonProperty("ticket_no")
    private String ticketNo;

    @SerializedName("CardId")
    private String pan;

    @SerializedName("ExpireDate")
    @JsonProperty("exp_date")
    private String expDate;

    @SerializedName("ExtenNo")
    private String cvv2;

    @JsonProperty("install_period")
    @SerializedName("install_period")
    private String installPeriod;

    @JsonProperty("use_redeem")
    @SerializedName("use_redeem")
    private String useRedeem;

    @JsonProperty("city")
    private String city;

    @JsonProperty("start_date")
    private String startDate;

    @JsonProperty("end_date")
    private String endDate;

    @JsonProperty("cust_id")
    private String custId;

    @JsonProperty("b_day")
    private String bDay;

    @JsonProperty("cell_phone_no")
    private String cellPhoneNo;

    @JsonProperty("home_tel_no")
    private String homeTelNo;

    @JsonProperty("office_tel_no")
    private String officeTelNo;

    //applepay
    @JsonProperty("payTokenData")
    private TSBApplePayTokenData payTokenData;

    //union
    @JsonProperty("cardholder_ip")
    private String cardholderIp;

    @JsonProperty("order_timeout")
    private String orderTimeout;


    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getCur() {
        return cur;
    }

    public void setCur(String cur) {
        this.cur = cur;
    }

    public String getOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(String orderDesc) {
        this.orderDesc = orderDesc;
    }

    public String getCaptFlag() {
        return captFlag;
    }

    public void setCaptFlag(String captFlag) {
        this.captFlag = captFlag;
    }

    public String getResultFlag() {
        return resultFlag;
    }

    public void setResultFlag(String resultFlag) {
        this.resultFlag = resultFlag;
    }

    public String getPostBackUrl() {
        return postBackUrl;
    }

    public void setPostBackUrl(String postBackUrl) {
        this.postBackUrl = postBackUrl;
    }

    public String getResultUrl() {
        return resultUrl;
    }

    public void setResultUrl(String resultUrl) {
        this.resultUrl = resultUrl;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getCvv2() {
        return cvv2;
    }

    public void setCvv2(String cvv2) {
        this.cvv2 = cvv2;
    }

    public String getInstallPeriod() {
        return installPeriod;
    }

    public void setInstallPeriod(String installPeriod) {
        this.installPeriod = installPeriod;
    }

    public String getUseRedeem() {
        return useRedeem;
    }

    public void setUseRedeem(String useRedeem) {
        this.useRedeem = useRedeem;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getbDay() {
        return bDay;
    }

    public void setbDay(String bDay) {
        this.bDay = bDay;
    }

    public String getCellPhoneNo() {
        return cellPhoneNo;
    }

    public void setCellPhoneNo(String cellPhoneNo) {
        this.cellPhoneNo = cellPhoneNo;
    }

    public String getHomeTelNo() {
        return homeTelNo;
    }

    public void setHomeTelNo(String homeTelNo) {
        this.homeTelNo = homeTelNo;
    }

    public String getOfficeTelNo() {
        return officeTelNo;
    }

    public void setOfficeTelNo(String officeTelNo) {
        this.officeTelNo = officeTelNo;
    }

    public String getCardholderIp() {
        return cardholderIp;
    }

    public void setCardholderIp(String cardholderIp) {
        this.cardholderIp = cardholderIp;
    }

    public String getOrderTimeout() {
        return orderTimeout;
    }

    public void setOrderTimeout(String orderTimeout) {
        this.orderTimeout = orderTimeout;
    }

    public TSBApplePayTokenData getPayTokenData() {
        return payTokenData;
    }

    public void setPayTokenData(TSBApplePayTokenData payTokenData) {
        this.payTokenData = payTokenData;
    }
}
