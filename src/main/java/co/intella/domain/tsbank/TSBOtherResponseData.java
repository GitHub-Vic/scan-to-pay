package co.intella.domain.tsbank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */
@JsonIgnoreProperties
public class TSBOtherResponseData {

    private String ver;


    private String mid;


    @JsonProperty("s_mid")
    @SerializedName("s_mid")
    private String sMid;


    private String tid;

    @JsonProperty("pay_type")
    @SerializedName("pay_type")
    private Integer payType;

    @JsonProperty("tx_type")
    @SerializedName("tx_type")
    private Integer txType;


    @JsonProperty("ret_value")
    @SerializedName("ret_value")
    private String retValue;


    private TSBOtherResponseDataPara params;

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getsMid() {
        return sMid;
    }

    public void setsMid(String sMid) {
        this.sMid = sMid;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Integer getTxType() {
        return txType;
    }

    public void setTxType(Integer txType) {
        this.txType = txType;
    }

    public String getRetValue() {
        return retValue;
    }

    public void setRetValue(String retValue) {
        this.retValue = retValue;
    }

    public TSBOtherResponseDataPara getParams() {
        return params;
    }

    public void setParams(TSBOtherResponseDataPara params) {
        this.params = params;
    }
}
