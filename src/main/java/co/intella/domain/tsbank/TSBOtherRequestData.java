package co.intella.domain.tsbank;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class TSBOtherRequestData extends  TSBCreditCardBasicRequestData{
    private String sender;

    private String ver;


    private String mid;



    @JsonProperty("s_mid")
    private String sMid;


    private String tid;


    @JsonProperty("pay_type")
    private Integer payType;

    @JsonProperty("tx_type")
    private Integer txType;


    private TSBOtherRequestDataPara params;


    public TSBOtherRequestDataPara getParams() {
        return params;
    }

    public void setParams(TSBOtherRequestDataPara params) {
        this.params = params;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getsMid() {
        return sMid;
    }

    public void setsMid(String sMid) {
        this.sMid = sMid;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Integer getTxType() {
        return txType;
    }

    public void setTxType(Integer txType) {
        this.txType = txType;
    }
}
