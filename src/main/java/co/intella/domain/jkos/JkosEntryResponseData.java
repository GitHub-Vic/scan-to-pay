package co.intella.domain.jkos;

public class JkosEntryResponseData {


    private String result;

    private String message;

    private ResultObject result_object;

    public class ResultObject {
        private String payment_url;
        private String qr_img;
        private String qr_timeout;

        public String getPayment_url() {
            return payment_url;
        }

        public void setPayment_url(String payment_url) {
            this.payment_url = payment_url;
        }

        public String getQr_img() {
            return qr_img;
        }

        public void setQr_img(String qr_img) {
            this.qr_img = qr_img;
        }

        public String getQr_timeout() {
            return qr_timeout;
        }

        public void setQr_timeout(String qr_timeout) {
            this.qr_timeout = qr_timeout;
        }

        @Override
        public String toString() {
            return "ResultObject{" +
                    "payment_url='" + payment_url + '\'' +
                    ", qr_img='" + qr_img + '\'' +
                    ", qr_timeout='" + qr_timeout + '\'' +
                    '}';
        }
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResultObject getResult_object() {
        return result_object;
    }

    public void setResult_object(ResultObject result_object) {
        this.result_object = result_object;
    }
}
