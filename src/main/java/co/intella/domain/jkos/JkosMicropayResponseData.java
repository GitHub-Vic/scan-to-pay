package co.intella.domain.jkos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class JkosMicropayResponseData extends JkosBasicResponse {








    @SerializedName("TradeTime")
    private String tradeTime;


    public String getTradeTime() {
        return tradeTime;
    }

    public void setTradeTime(String tradeTime) {
        this.tradeTime = tradeTime;
    }
}
