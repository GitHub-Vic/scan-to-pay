package co.intella.domain.jkos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class JkosEntryRequestData extends JkosBasicRequest {

    @SerializedName("platform_order_id")
    @JsonProperty("platform_order_id")
    private String platformOrderId;

    @SerializedName("store_id")
    @JsonProperty("store_id")
    private String storeId;

    @SerializedName("currency")
    @JsonProperty("currency")
    private String currency;

    @SerializedName("total_price")
    @JsonProperty("total_price")
    private Integer totalPrice;

    @SerializedName("final_price")
    @JsonProperty("final_price")
    private Integer finalPrice;

    @SerializedName("result_url")
    @JsonProperty("result_url")
    private String resultUrl;

    @SerializedName("result_display_url")
    @JsonProperty("result_display_url")
    private String resultDisplayUrl;

    @SerializedName("valid_time")
    @JsonProperty("valid_time")
    private String validtime;

    public String getValidtime() {
        return validtime;
    }

    public void setValidtime(String validtime) {
        this.validtime = validtime;
    }

    public String getPlatformOrderId() {
        return platformOrderId;
    }

    public void setPlatformOrderId(String platformOrderId) {
        this.platformOrderId = platformOrderId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(Integer finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getResultUrl() {
        return resultUrl;
    }

    public void setResultUrl(String resultUrl) {
        this.resultUrl = resultUrl;
    }

    public String getResultDisplayUrl() {
        return resultDisplayUrl;
    }

    public void setResultDisplayUrl(String resultDisplayUrl) {
        this.resultDisplayUrl = resultDisplayUrl;
    }
}
