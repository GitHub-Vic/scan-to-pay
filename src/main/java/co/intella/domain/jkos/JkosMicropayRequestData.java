package co.intella.domain.jkos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JkosMicropayRequestData extends JkosBasicRequest{

    @SerializedName("MchId")
    @JsonProperty("MerchantID")
    private String merchantId;

    @SerializedName("StoreInfo")
    @JsonProperty("StoreID")
    private String storeId="001";

    @SerializedName("StoreName")
    @JsonProperty("StoreName")
    private String storeName;


    @JsonProperty("GatewayTradeNo")
    private String gatewayTradeNo="";

    @SerializedName("StoreOrderNo")
    @JsonProperty("MerchantTradeNo")
    private String merchantTradeNo;

    @SerializedName("DeviceInfo")
    @JsonProperty("PosID")
    private String posId="";

    @SerializedName("CreateTime")
    @JsonProperty("PosTradeTime")
    private String posTradeTime;

    @SerializedName("AuthCode")
    @JsonProperty("CardToken")
    private String cardToken;

    @SerializedName("TotalFee")
    @JsonProperty("TradeAmount")
    private int tradeAmount;


    @JsonProperty("UnRedeem")
    private Integer unRedeem;

    @SerializedName("Body")
    @JsonProperty("Remark")
    private String remark;

    @JsonProperty("Extra1")
    private String extra1="";

    @JsonProperty("Extra2")
    private String extra2="";

    @JsonProperty("Extra3")
    private String extra3="";

    @JsonProperty("SendTime")
    private String sendTime;

    @JsonProperty("Sign")
    private String sign;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getGatewayTradeNo() {
        return gatewayTradeNo;
    }

    public void setGatewayTradeNo(String gatewayTradeNo) {
        this.gatewayTradeNo = gatewayTradeNo;
    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getPosId() {
        return posId;
    }

    public void setPosId(String posId) {
        this.posId = posId;
    }

    public String getPosTradeTime() {
        return posTradeTime;
    }

    public void setPosTradeTime(String posTradeTime) {
        this.posTradeTime = posTradeTime;
    }

    public String getCardToken() {
        return cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    public int getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(int tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public Integer getUnRedeem() {
        return unRedeem;
    }

    public void setUnRedeem(Integer unRedeem) {
        this.unRedeem = unRedeem;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getExtra1() {
        return extra1;
    }

    public void setExtra1(String extra1) {
        this.extra1 = extra1;
    }

    public String getExtra2() {
        return extra2;
    }

    public void setExtra2(String extra2) {
        this.extra2 = extra2;
    }

    public String getExtra3() {
        return extra3;
    }

    public void setExtra3(String extra3) {
        this.extra3 = extra3;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
