package co.intella.domain.jkos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class JkosBasicResponse {
    @JsonProperty("MchId")
    @SerializedName("MerchantID")
    private String merchantId;

    @JsonProperty("StoreInfo")
    @SerializedName("StoreID")
    private String storeId;


    @SerializedName("GatewayTradeNo")
    private String gatewayTradeNo;

    @JsonProperty("StoreOrderNo")
    @SerializedName("MerchantTradeNo")
    private String merchantTradeNo;

    @JsonProperty("DeviceInfo")
    @SerializedName("PosID")
    private String posId;

    @JsonProperty("SysOrderNo")
    @SerializedName("TradeNo")
    private String tradeNo;

    @SerializedName("TradeAmount")
    private Integer tradeAmount;

    @SerializedName("DebitAmount")
    private Integer debitAmount;

    @SerializedName("AvailableAmount")
    private Integer availableAmount;

    @SerializedName("StatusCode")
    private String statusCode;

    @SerializedName("StatusDesc")
    private String statusDesc;

    @SerializedName("IsRep")
    private Integer isRep;

    @SerializedName("ActionCode")
    private String actionCode;

    @SerializedName("PaymentType")
    private Integer paymentType;

    @SerializedName("RedeemName")
    private String redeemName;

    @SerializedName("RedeemAmount")
    private Integer redeemAmount;

    @SerializedName("InvoiceVehicle")
    private String invoiceVehicle;

    @SerializedName("MerMemToken")
    private String merMemToken;

    @JsonProperty("Body")
    @SerializedName("Remark")
    private String remark;


    @SerializedName("Extra1")
    private String extra1;


    @SerializedName("Extra2")
    private String extra2;

    @JsonProperty("Extra3")
    @SerializedName("Extra3")
    private String extra3;


    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getGatewayTradeNo() {
        return gatewayTradeNo;
    }

    public void setGatewayTradeNo(String gatewayTradeNo) {
        this.gatewayTradeNo = gatewayTradeNo;
    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getPosId() {
        return posId;
    }

    public void setPosId(String posId) {
        this.posId = posId;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public Integer getIsRep() {
        return isRep;
    }

    public void setIsRep(Integer isRep) {
        this.isRep = isRep;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public String getRedeemName() {
        return redeemName;
    }

    public void setRedeemName(String redeemName) {
        this.redeemName = redeemName;
    }

    public Integer getRedeemAmount() {
        return redeemAmount;
    }

    public void setRedeemAmount(Integer redeemAmount) {
        this.redeemAmount = redeemAmount;
    }

    public String getInvoiceVehicle() {
        return invoiceVehicle;
    }

    public void setInvoiceVehicle(String invoiceVehicle) {
        this.invoiceVehicle = invoiceVehicle;
    }

    public String getMerMemToken() {
        return merMemToken;
    }

    public void setMerMemToken(String merMemToken) {
        this.merMemToken = merMemToken;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getExtra1() {
        return extra1;
    }

    public void setExtra1(String extra1) {
        this.extra1 = extra1;
    }

    public String getExtra2() {
        return extra2;
    }

    public void setExtra2(String extra2) {
        this.extra2 = extra2;
    }

    public String getExtra3() {
        return extra3;
    }

    public void setExtra3(String extra3) {
        this.extra3 = extra3;
    }

    public Integer getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(Integer tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public Integer getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(Integer debitAmount) {
        this.debitAmount = debitAmount;
    }

    public Integer getAvailableAmount() {
        return availableAmount;
    }

    public void setAvailableAmount(Integer availableAmount) {
        this.availableAmount = availableAmount;
    }
}
