package co.intella.domain.jkos;

import co.intella.model.Merchant;
import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class JkosCancelRequestData extends JkosBasicRequest {

    public JkosCancelRequestData() {
        super();
    }

    public JkosCancelRequestData(TradeDetail tradeDetail) {

        PaymentAccount paymentAccount = tradeDetail.getPaymentAccount();
        Merchant merchant = paymentAccount.getMerchant();
        LocalDateTime now = LocalDateTime.now();

        this.setMerchantId(paymentAccount.getAccount());
        this.setStoreId(paymentAccount.getAppId());
        this.setStoreName(merchant.getName());
        this.setMerchantTradeNo(tradeDetail.getOrderId());
        this.setPosTradeTime(now.format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss")));

        this.setCardToken(tradeDetail.getBarcode());
        this.setTradeAmount(Long.valueOf(tradeDetail.getPayment()).intValue());
        this.setSendTime(now.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));

    }

    @JsonProperty("MerchantID")
    private String merchantId;

    @JsonProperty("StoreID")
    private String storeId = "001";//must have value

    @JsonProperty("StoreName")
    private String storeName;

    @JsonProperty("GatewayTradeNo")
    private String gatewayTradeNo = "";

    @JsonProperty("MerchantTradeNo")
    private String merchantTradeNo;

    @JsonProperty("PosID")
    private String posId = "skb0001";

    @JsonProperty("PosTradeTime")
    private String posTradeTime;

    @JsonProperty("CardToken")
    private String cardToken;

    @JsonProperty("TradeAmount")
    private Integer tradeAmount;

    @JsonProperty("UnRedeem")
    private Integer unRedeem = 0;

    @JsonProperty("Remark")
    private String remark = "";

    @JsonProperty("Extra1")
    private String extra1 = "";

    @JsonProperty("Extra2")
    private String extra2 = "";

    @JsonProperty("Extra3")
    private String extra3 = "";

    @JsonProperty("SendTime")
    private String sendTime;

    @JsonProperty("Sign")
    private String sign;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getGatewayTradeNo() {
        return gatewayTradeNo;
    }

    public void setGatewayTradeNo(String gatewayTradeNo) {
        this.gatewayTradeNo = gatewayTradeNo;
    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getPosId() {
        return posId;
    }

    public void setPosId(String posId) {
        this.posId = posId;
    }

    public String getPosTradeTime() {
        return posTradeTime;
    }

    public void setPosTradeTime(String posTradeTime) {
        this.posTradeTime = posTradeTime;
    }

    public String getCardToken() {
        return cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    public Integer getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(Integer tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public Integer getUnRedeem() {
        return unRedeem;
    }

    public void setUnRedeem(Integer unRedeem) {
        this.unRedeem = unRedeem;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getExtra1() {
        return extra1;
    }

    public void setExtra1(String extra1) {
        this.extra1 = extra1;
    }

    public String getExtra2() {
        return extra2;
    }

    public void setExtra2(String extra2) {
        this.extra2 = extra2;
    }

    public String getExtra3() {
        return extra3;
    }

    public void setExtra3(String extra3) {
        this.extra3 = extra3;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    @Override
    public String toString() {
        return "JkosCancelRequestData{" +
                "merchantId='" + merchantId + '\'' +
                ", storeId='" + storeId + '\'' +
                ", storeName='" + storeName + '\'' +
                ", gatewayTradeNo='" + gatewayTradeNo + '\'' +
                ", merchantTradeNo='" + merchantTradeNo + '\'' +
                ", posId='" + posId + '\'' +
                ", posTradeTime='" + posTradeTime + '\'' +
                ", cardToken='" + cardToken + '\'' +
                ", tradeAmount=" + tradeAmount +
                ", unRedeem=" + unRedeem +
                ", remark='" + remark + '\'' +
                ", extra1='" + extra1 + '\'' +
                ", extra2='" + extra2 + '\'' +
                ", extra3='" + extra3 + '\'' +
                ", sendTime='" + sendTime + '\'' +
                ", sign='" + sign + '\'' +
                '}';
    }
}
