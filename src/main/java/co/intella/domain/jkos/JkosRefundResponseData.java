package co.intella.domain.jkos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class JkosRefundResponseData extends JkosBasicResponse{

//    @JsonProperty("MchId")
//    @SerializedName("MerchantID")
//    private String merchantId;
//
//    @JsonProperty("StoreInfo")
//    @SerializedName("StoreID")
//    private String storeId;
//
//
//    @SerializedName("GatewayTradeNo")
//    private String gatewayTradeNo;
//
//    @JsonProperty("StoreOrderNo")
//    @SerializedName("MerchantTradeNo")
//    private String merchantTradeNo;
//
//    @JsonProperty("DeviceInfo")
//    @SerializedName("PosID")
//    private String posId;


//    @SerializedName("TradeAmount")
//    private Integer tradeAmount;
//
//    @SerializedName("DebitAmount")
//    private Integer debitAmount;
//
//    @SerializedName("AvailableAmount")
//    private Integer availableAmount;


//    @JsonProperty("SysOrderNo")
//    @SerializedName("TradeNo")
//    private String tradeNo;


    @SerializedName("RefundTradeNo")
    private String RefundTradeNo;


    @SerializedName("RefundTradeTime")
    private String refundTradeTime;

    public String getRefundTradeNo() {
        return RefundTradeNo;
    }

    public void setRefundTradeNo(String refundTradeNo) {
        RefundTradeNo = refundTradeNo;
    }

    public String getRefundTradeTime() {
        return refundTradeTime;
    }

    public void setRefundTradeTime(String refundTradeTime) {
        this.refundTradeTime = refundTradeTime;
    }

//    @SerializedName("StatusCode")
//    private String statusCode;
//
//    @SerializedName("StatusDesc")
//    private String statusDesc;
//
//    @SerializedName("IsRep")
//    private Integer isRep;
//
//    @SerializedName("ActionCode")
//    private String actionCode;
//
//    @SerializedName("PaymentType")
//    private Integer paymentType;





//    @SerializedName("RedeemName")
//    private String redeemName;
//
//    @SerializedName("RedeemAmount")
//    private Integer redeemAmount;

//
//    @SerializedName("InvoiceVehicle")
//    private String invoiceVehicle;

//    @SerializedName("MerMemToken")
//    private String merMemToken;
//
//    @JsonProperty("Body")
//    @SerializedName("Remark")
//    private String remark;
//
//
//    @SerializedName("Extra1")
//    private String extra1;
//
//
//    @SerializedName("Extra2")
//    private String extra2;
//
//    @JsonProperty("Extra3")
//    @SerializedName("Extra3")
//    private String extra3;


}
