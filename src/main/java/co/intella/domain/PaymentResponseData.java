package co.intella.domain;

public class PaymentResponseData extends PaymentData {

	public String urlToken;
	public String authCode;
	public String deviceInfo;
	public String platformRsp;
	public String serialNumber;
	public String callbackUrl;
	public String storeRefundNo;
	public String sysRefundNo;
	public String refundedAt;

	public String getUrlToken() {
		return urlToken;
	}

	public void setUrlToken(String urlToken) {
		this.urlToken = urlToken;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public String getPlatformRsp() {
		return platformRsp;
	}

	public void setPlatformRsp(String platformRsp) {
		this.platformRsp = platformRsp;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	public String getStoreRefundNo() {
		return storeRefundNo;
	}

	public void setStoreRefundNo(String storeRefundNo) {
		this.storeRefundNo = storeRefundNo;
	}

	public String getSysRefundNo() {
		return sysRefundNo;
	}

	public void setSysRefundNo(String sysRefundNo) {
		this.sysRefundNo = sysRefundNo;
	}

	public String getRefundedAt() {
		return refundedAt;
	}

	public void setRefundedAt(String refundedAt) {
		this.refundedAt = refundedAt;
	}

	@Override
	public String toString() {
		return "PaymentResponseData [urlToken=" + urlToken + ", authCode=" + authCode + ", deviceInfo=" + deviceInfo
				+ ", platformRsp=" + platformRsp + ", serialNumber=" + serialNumber + ", callbackUrl=" + callbackUrl
				+ "]";
	}
	
}
