package co.intella.domain.pay2go;

import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class Pay2GoRefundRequest {
    @SerializedName("TimeStamp")
    private Long  timeStamp;

    @SerializedName("Status")
    private String status;

    @SerializedName("Message")
    private String message;

    @SerializedName("Result")
    private Pay2GoNotifyRequest.P2GResult result;


    public class RefundInfo {

        @SerializedName("OrderStatus")
        private String orderStatus;

        @SerializedName("MerchantID")
        private String merchantID;

        @SerializedName("MerchantOrderNo")
        private String merchantOrderNo;

        @SerializedName("TradeNo")
        private String tradeNo;

        @SerializedName("Currency")
        private String currency;

        @SerializedName("Amt")
        private String amt;

        @SerializedName("PaymentType")
        private String paymentType;

        @SerializedName("PaymentTime")
        private String paymentTime;

        @SerializedName("EscrowBank")
        private String escrowBank;

        @SerializedName("BuyerMemNo")
        private String buyerMemNo;

        @SerializedName("AccLinkMSGNo")
        private String accLinkMSGNo;
    }
}
