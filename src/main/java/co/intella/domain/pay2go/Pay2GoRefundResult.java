package co.intella.domain.pay2go;

import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class Pay2GoRefundResult {
    @SerializedName("RefundType")
    private String  refundType;

    @SerializedName("OrderStatus")
    private String orderStatus;

    @SerializedName("MerchantID")
    private String merchantID;


        @SerializedName("MerchantOrderNo")
        private String merchantOrderNo;

        @SerializedName("TradeNo")
        private String tradeNo;

        @SerializedName("Currency")
        private String currency;

        @SerializedName("RefundAmt")
        private String refundAmt;

        @SerializedName("RefundBarCode")
        private String refundBarCode;

        @SerializedName("RefundLimit")
        private String refundLimit;

        @SerializedName("RefundTime")
        private String refundTime;

    public String getRefundType() {
        return refundType;
    }

    public void setRefundType(String refundType) {
        this.refundType = refundType;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getMerchantOrderNo() {
        return merchantOrderNo;
    }

    public void setMerchantOrderNo(String merchantOrderNo) {
        this.merchantOrderNo = merchantOrderNo;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getRefundAmt() {
        return refundAmt;
    }

    public void setRefundAmt(String refundAmt) {
        this.refundAmt = refundAmt;
    }

    public String getRefundBarCode() {
        return refundBarCode;
    }

    public void setRefundBarCode(String refundBarCode) {
        this.refundBarCode = refundBarCode;
    }

    public String getRefundLimit() {
        return refundLimit;
    }

    public void setRefundLimit(String refundLimit) {
        this.refundLimit = refundLimit;
    }

    public String getRefundTime() {
        return refundTime;
    }

    public void setRefundTime(String refundTime) {
        this.refundTime = refundTime;
    }
}
