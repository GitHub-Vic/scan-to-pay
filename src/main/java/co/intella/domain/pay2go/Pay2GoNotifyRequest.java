package co.intella.domain.pay2go;

import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class Pay2GoNotifyRequest {
    
    @SerializedName("TimeStamp")
    private Long  timeStamp;

    @SerializedName("Status")
    private String status;

    @SerializedName("Message")
    private String message;

    @SerializedName("Result")
    private P2GResult result;


    public class P2GResult {

        @SerializedName("OrderStatus")
        private String orderStatus;

        @SerializedName("MerchantID")
        private String merchantID;

        @SerializedName("MerchantOrderNo")
        private String merchantOrderNo;

        @SerializedName("TradeNo")
        private String tradeNo;

        @SerializedName("Currency")
        private String currency;

        @SerializedName("Amt")
        private String amt;

        @SerializedName("PaymentType")
        private String paymentType;

        @SerializedName("PaymentTime")
        private String paymentTime;

        @SerializedName("EscrowBank")
        private String escrowBank;

        @SerializedName("BuyerMemNo")
        private String buyerMemNo;

        @SerializedName("AccLinkMSGNo")
        private String accLinkMSGNo;


        @SerializedName("AccLinkRTNCode")
        private String accLinkRTNCode;

        @SerializedName("AccLinkRTNMsg")
        private String accLinkRTNMsg;

        @SerializedName("AccLinkBank")
        private String accLinkBank;

        @SerializedName("AccLinkNo")
        private String accLinkNo;

        public String getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getMerchantID() {
            return merchantID;
        }

        public void setMerchantID(String merchantID) {
            this.merchantID = merchantID;
        }

        public String getMerchantOrderNo() {
            return merchantOrderNo;
        }

        public void setMerchantOrderNo(String merchantOrderNo) {
            this.merchantOrderNo = merchantOrderNo;
        }

        public String getTradeNo() {
            return tradeNo;
        }

        public void setTradeNo(String tradeNo) {
            this.tradeNo = tradeNo;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getAmt() {
            return amt;
        }

        public void setAmt(String amt) {
            this.amt = amt;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        public String getPaymentTime() {
            return paymentTime;
        }

        public void setPaymentTime(String paymentTime) {
            this.paymentTime = paymentTime;
        }

        public String getEscrowBank() {
            return escrowBank;
        }

        public void setEscrowBank(String escrowBank) {
            this.escrowBank = escrowBank;
        }

        public String getBuyerMemNo() {
            return buyerMemNo;
        }

        public void setBuyerMemNo(String buyerMemNo) {
            this.buyerMemNo = buyerMemNo;
        }

        public String getAccLinkMSGNo() {
            return accLinkMSGNo;
        }

        public void setAccLinkMSGNo(String accLinkMSGNo) {
            this.accLinkMSGNo = accLinkMSGNo;
        }

        public String getAccLinkRTNCode() {
            return accLinkRTNCode;
        }

        public void setAccLinkRTNCode(String accLinkRTNCode) {
            this.accLinkRTNCode = accLinkRTNCode;
        }

        public String getAccLinkRTNMsg() {
            return accLinkRTNMsg;
        }

        public void setAccLinkRTNMsg(String accLinkRTNMsg) {
            this.accLinkRTNMsg = accLinkRTNMsg;
        }

        public String getAccLinkBank() {
            return accLinkBank;
        }

        public void setAccLinkBank(String accLinkBank) {
            this.accLinkBank = accLinkBank;
        }

        public String getAccLinkNo() {
            return accLinkNo;
        }

        public void setAccLinkNo(String accLinkNo) {
            this.accLinkNo = accLinkNo;
        }
    }

    @SerializedName("ResponseType")
    private String responseType;

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public P2GResult getResult() {
        return result;
    }

    public void setResult(P2GResult result) {
        this.result = result;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }
}
