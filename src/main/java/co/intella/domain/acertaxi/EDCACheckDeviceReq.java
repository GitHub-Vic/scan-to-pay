package co.intella.domain.acertaxi;

import co.intella.domain.easycard.EzCardBasicRequest;

public class EDCACheckDeviceReq extends EzCardBasicRequest {

    public EDCACheckDeviceReq() {
        this.setServiceType("PrinterPaperStates");
    }
}
