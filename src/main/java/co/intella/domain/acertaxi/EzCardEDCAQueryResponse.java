package co.intella.domain.acertaxi;

import com.google.gson.annotations.SerializedName;

public class EzCardEDCAQueryResponse extends AcerTaxiQueryResponse {

    @SerializedName("TransferGroup")
    private String transferGroup;

    @SerializedName("NewTransferGroup")
    private String newTransferGroup;

    public String getTransferGroup() {
        return transferGroup;
    }

    public void setTransferGroup(String transferGroup) {
        this.transferGroup = transferGroup;
    }

    public String getNewTransferGroup() {
        return newTransferGroup;
    }

    public void setNewTransferGroup(String newTransferGroup) {
        this.newTransferGroup = newTransferGroup;
    }

}
