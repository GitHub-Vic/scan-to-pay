package co.intella.domain.acertaxi;

import co.intella.domain.easycard.EzCardPaymentResponse;
import com.google.gson.annotations.SerializedName;
import org.springframework.stereotype.Service;

public class AcerTaxiQueryResponse extends EzCardPaymentResponse {

    @SerializedName("AutoLoadFlag")
    private String autoLoadFlag;

    @SerializedName("EZCIDExpiryDate")
    private String ezcIDExpiryDate;

    @SerializedName("CPUSubAreaCode")
    private String cpuSubAreaCode;

    @SerializedName("PointID")
    private String pointID;

    @SerializedName("PointType")
    private String pointType;

    @SerializedName("CountOfDeductPoint")
    private String countOfDeductPoint;

    @SerializedName("PointLastUseDate")
    private String pointLastUseDate;

    @SerializedName("PointRefillAmount")
    private String pointRefillAmount;

    @SerializedName("Discount")
    private String discount;

    @SerializedName("PersonalProfileAuthorization")
    private String personalProfileAuthorization;

    @SerializedName("MaxPointPerTrip")
    private String maxPointPerTrip;

    @SerializedName("UsePointLimit")
    private String usePointLimit;


    public String getPointRefillAmount() {
        return pointRefillAmount;
    }

    public void setPointRefillAmount(String pointRefillAmount) {
        this.pointRefillAmount = pointRefillAmount;
    }

    public String getAutoLoadFlag() {
        return autoLoadFlag;
    }

    public void setAutoLoadFlag(String autoLoadFlag) {
        this.autoLoadFlag = autoLoadFlag;
    }

    public String getEzcIDExpiryDate() {
        return ezcIDExpiryDate;
    }

    public void setEzcIDExpiryDate(String ezcIDExpiryDate) {
        this.ezcIDExpiryDate = ezcIDExpiryDate;
    }

    public String getCpuSubAreaCode() {
        return cpuSubAreaCode;
    }

    public void setCpuSubAreaCode(String cpuSubAreaCode) {
        this.cpuSubAreaCode = cpuSubAreaCode;
    }

    public String getPointID() {
        return pointID;
    }

    public void setPointID(String pointID) {
        this.pointID = pointID;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public String getCountOfDeductPoint() {
        return countOfDeductPoint;
    }

    public void setCountOfDeductPoint(String countOfDeductPoint) {
        this.countOfDeductPoint = countOfDeductPoint;
    }

    public String getPointLastUseDate() {
        return pointLastUseDate;
    }

    public void setPointLastUseDate(String pointLastUseDate) {
        this.pointLastUseDate = pointLastUseDate;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getPersonalProfileAuthorization() {
        return personalProfileAuthorization;
    }

    public void setPersonalProfileAuthorization(String personalProfileAuthorization) {
        this.personalProfileAuthorization = personalProfileAuthorization;
    }

    public String getMaxPointPerTrip() {
        return maxPointPerTrip;
    }

    public void setMaxPointPerTrip(String maxPointPerTrip) {
        this.maxPointPerTrip = maxPointPerTrip;
    }

    public String getUsePointLimit() {
        return usePointLimit;
    }

    public void setUsePointLimit(String usePointLimit) {
        this.usePointLimit = usePointLimit;
    }
}
