package co.intella.domain.acertaxi;

import co.intella.domain.easycard.EzCardPaymentResponse;
import com.google.gson.annotations.SerializedName;

public class AcerTaxiPaymentResponse extends EzCardPaymentResponse {

    @SerializedName("AutoLoadFlag")
    private String autoLoadFlag;

    @SerializedName("DeviceID")
    private String deviceID;

    @SerializedName("CPUSubAreaCode")
    private String cpuSubAreaCode;

    @SerializedName("PointID")
    private String pointID;

    @SerializedName("DeductPoint")
    private String deductPoint;

    @SerializedName("PointBalance")
    private String pointBalance;

    @SerializedName("PointType")
    private String pointType;

    @SerializedName("CountOfDeductPoint")
    private String countOfDeductPoint;

    @SerializedName("PointLastUseDate")
    private String pointLastUseDate;

    @SerializedName("CarFleetName")
    private String carFleetName;

    @SerializedName("LicensePlate")
    private String licensePlate;

    @SerializedName("DriverNumber")
    private String driverNumber;

    @SerializedName("PersonalProfileAuthorization")
    private String personalProfileAuthorization;

    @SerializedName("cardId")
    private String cardId;

    @SerializedName("cardIdDscr")
    private String cardIdDscr;

    @SerializedName("NeedBalance")
    private String needBalance;

    @SerializedName("Beforepoint")
    private String beforepoint;

    @SerializedName("QueryCardInfoSultData")
    private String queryCardInfoSultData;

    @SerializedName("CardArea")
    private String cardArea;

    @SerializedName("IdentityInfo")
    private String identityInfo;

    @SerializedName("DeviceArea")
    private String deviceArea;


    public String getBeforepoint() {
        return beforepoint;
    }

    public void setBeforepoint(String beforepoint) {
        this.beforepoint = beforepoint;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    @SerializedName("DiscountAmount")

    private String discountAmount;

    public String getNeedBalance() {
        return needBalance;
    }

    public void setNeedBalance(String needBalance) {
        this.needBalance = needBalance;
    }

    public String getAutoLoadFlag() {
        return autoLoadFlag;
    }

    public void setAutoLoadFlag(String autoLoadFlag) {
        this.autoLoadFlag = autoLoadFlag;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getCpuSubAreaCode() {
        return cpuSubAreaCode;
    }

    public void setCpuSubAreaCode(String cpuSubAreaCode) {
        this.cpuSubAreaCode = cpuSubAreaCode;
    }

    public String getPointID() {
        return pointID;
    }

    public void setPointID(String pointID) {
        this.pointID = pointID;
    }

    public String getDeductPoint() {
        return deductPoint;
    }

    public void setDeductPoint(String deductPoint) {
        this.deductPoint = deductPoint;
    }

    public String getPointBalance() {
        return pointBalance;
    }

    public void setPointBalance(String pointBalance) {
        this.pointBalance = pointBalance;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public String getCountOfDeductPoint() {
        return countOfDeductPoint;
    }

    public void setCountOfDeductPoint(String countOfDeductPoint) {
        this.countOfDeductPoint = countOfDeductPoint;
    }

    public String getPointLastUseDate() {
        return pointLastUseDate;
    }

    public void setPointLastUseDate(String pointLastUseDate) {
        this.pointLastUseDate = pointLastUseDate;
    }

    public String getCarFleetName() {
        return carFleetName;
    }

    public void setCarFleetName(String carFleetName) {
        this.carFleetName = carFleetName;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }


    public String getDriverNumber() {
        return driverNumber;
    }

    public void setDriverNumber(String driverNumber) {
        this.driverNumber = driverNumber;
    }

    public String getPersonalProfileAuthorization() {
        return personalProfileAuthorization;
    }

    public void setPersonalProfileAuthorization(String personalProfileAuthorization) {
        this.personalProfileAuthorization = personalProfileAuthorization;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardIdDscr() {
        return cardIdDscr;
    }

    public void setCardIdDscr(String cardIdDscr) {
        this.cardIdDscr = cardIdDscr;
    }

    public String getQueryCardInfoSultData() {
        return queryCardInfoSultData;
    }

    public void setQueryCardInfoSultData(String queryCardInfoSultData) {
        this.queryCardInfoSultData = queryCardInfoSultData;
    }

    public String getCardArea() {
        return cardArea;
    }

    public void setCardArea(String cardArea) {
        this.cardArea = cardArea;
    }

    public String getIdentityInfo() {
        return identityInfo;
    }

    public void setIdentityInfo(String identityInfo) {
        this.identityInfo = identityInfo;
    }

    public String getDeviceArea() {
        return deviceArea;
    }

    public void setDeviceArea(String deviceArea) {
        this.deviceArea = deviceArea;
    }
}
