package co.intella.domain.acertaxi;

import co.intella.domain.easycard.EzCardBasicResponse;
import com.google.gson.annotations.SerializedName;

public class AcerTaxiCheckDeviceResponse extends EzCardBasicResponse {

    @SerializedName("NewDeviceID")
    private String newDeviceID;

    @SerializedName("AreaCode")
    private String areaCode;

    @SerializedName("IPassArea")
    private String ipassArea;

    @SerializedName("LicensePlate")
    private String licensePlate;

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getIpassArea() {
        return ipassArea;
    }

    public void setIpassArea(String ipassArea) {
        this.ipassArea = ipassArea;
    }

    public String getNewDeviceID() {
        return newDeviceID;
    }

    public void setNewDeviceID(String newDeviceID) {
        this.newDeviceID = newDeviceID;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }
}
