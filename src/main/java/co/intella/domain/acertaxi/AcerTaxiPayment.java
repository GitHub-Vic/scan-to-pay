package co.intella.domain.acertaxi;

import co.intella.domain.easycard.EzCardPayment;
import com.google.gson.annotations.SerializedName;

public class AcerTaxiPayment extends EzCardPayment {

    @SerializedName("PersonalProfile")
    private String personalProfile;

    @SerializedName("AreaCode")
    private String areaCode;

    @SerializedName("EZCardType")
    private String ezcardType;

    @SerializedName("PersonalDiscount")
    private String personalDiscount;

    @SerializedName("TransferDiscount")
    private String transferDiscount;

    @SerializedName("TransferGroup2")
    private String transferGroup;

    @SerializedName("PaymentType")
    private String paymentType;

    @SerializedName("PointID")
    private String pointID;

    @SerializedName("PointType")
    private String pointType;

    @SerializedName("DeductPoint")
    private String deductPoint;

    @SerializedName("PointBalance")
    private String pointBalance;

    @SerializedName("CountOfDeductPoint")
    private String countOfDeductPoint;

    @SerializedName("PointLastUseDate")
    private String pointLastUseDate;

    @SerializedName("MaxPointOnMonth")
    private String maxPointOnMonth;

    @SerializedName("NumberPlate")
    private String numberPlate;

    @SerializedName("DriverNumber")
    private String driverNumber;

    @SerializedName("CardNumberForPrint")
    private String cardNumberForPrint;

    @SerializedName("PointRefillAmount")
    private String pointRefillAmount;

    @SerializedName("TradeKey")
    private String tradeKey;

    @SerializedName("PersonalProfileAuthorization")
    private String personalProfileAuthorization;

    public AcerTaxiPayment() {
        this.setServiceType("PaymentForTaxi");
    }

    public String getPersonalProfile() {
        return personalProfile;
    }

    public void setPersonalProfile(String personalProfile) {
        this.personalProfile = personalProfile;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getEzcardType() {
        return ezcardType;
    }

    public void setEzcardType(String ezcardType) {
        this.ezcardType = ezcardType;
    }

    public String getPersonalDiscount() {
        return personalDiscount;
    }

    public void setPersonalDiscount(String personalDiscount) {
        this.personalDiscount = personalDiscount;
    }

    public String getTransferDiscount() {
        return transferDiscount;
    }

    public void setTransferDiscount(String transferDiscount) {
        this.transferDiscount = transferDiscount;
    }

    public String getTransferGroup() {
        return transferGroup;
    }

    public void setTransferGroup(String transferGroup) {
        this.transferGroup = transferGroup;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPointID() {
        return pointID;
    }

    public void setPointID(String pointID) {
        this.pointID = pointID;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public String getDeductPoint() {
        return deductPoint;
    }

    public void setDeductPoint(String deductPoint) {
        this.deductPoint = deductPoint;
    }

    public String getPointBalance() {
        return pointBalance;
    }

    public void setPointBalance(String pointBalance) {
        this.pointBalance = pointBalance;
    }

    public String getCountOfDeductPoint() {
        return countOfDeductPoint;
    }

    public void setCountOfDeductPoint(String countOfDeductPoint) {
        this.countOfDeductPoint = countOfDeductPoint;
    }

    public String getPointLastUseDate() {
        return pointLastUseDate;
    }

    public void setPointLastUseDate(String pointLastUseDate) {
        this.pointLastUseDate = pointLastUseDate;
    }

    public String getMaxPointOnMonth() {
        return maxPointOnMonth;
    }

    public void setMaxPointOnMonth(String maxPointOnMonth) {
        this.maxPointOnMonth = maxPointOnMonth;
    }

    public String getNumberPlate() {
        return numberPlate;
    }

    public void setNumberPlate(String numberPlate) {
        this.numberPlate = numberPlate;
    }

    public String getDriverNumber() {
        return driverNumber;
    }

    public void setDriverNumber(String driverNumber) {
        this.driverNumber = driverNumber;
    }

    public String getCardNumberForPrint() {
        return cardNumberForPrint;
    }

    public void setCardNumberForPrint(String cardNumberForPrint) {
        this.cardNumberForPrint = cardNumberForPrint;
    }

    public String getPointRefillAmount() {
        return pointRefillAmount;
    }

    public void setPointRefillAmount(String pointRefillAmount) {
        this.pointRefillAmount = pointRefillAmount;
    }

    public String getTradeKey() {
        return tradeKey;
    }

    public void setTradeKey(String tradeKey) {
        this.tradeKey = tradeKey;
    }

    public String getPersonalProfileAuthorization() {
        return personalProfileAuthorization;
    }

    public void setPersonalProfileAuthorization(String personalProfileAuthorization) {
        this.personalProfileAuthorization = personalProfileAuthorization;
    }
}
