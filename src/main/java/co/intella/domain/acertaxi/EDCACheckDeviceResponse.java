package co.intella.domain.acertaxi;

import co.intella.domain.easycard.EzCardBasicResponse;
import com.google.gson.annotations.SerializedName;

public class EDCACheckDeviceResponse extends EzCardBasicResponse {

    @SerializedName("PaperStates")
    private String paperStates;

    public String getPaperStates() {
        return paperStates;
    }

    public void setPaperStates(String paperStates) {
        this.paperStates = paperStates;
    }
}
