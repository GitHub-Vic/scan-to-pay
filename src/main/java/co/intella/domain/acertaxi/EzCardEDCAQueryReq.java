package co.intella.domain.acertaxi;

import com.google.gson.annotations.SerializedName;

public class EzCardEDCAQueryReq extends AcerTaxiQuery {

    public EzCardEDCAQueryReq() {
        this.setServiceType("EZCardEDCADataInQuery");
    }
}
