package co.intella.domain.acertaxi;

public class EzCardEDCAPaymentReq extends AcerTaxiPayment {

    public EzCardEDCAPaymentReq() {
        this.setServiceType("EZCardEDCAPayment");
    }
}
