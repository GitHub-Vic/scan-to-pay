package co.intella.domain.acertaxi;

import co.intella.domain.easycard.EzCardBasicRequest;
import com.google.gson.annotations.SerializedName;

public class AcerTaxiQuery extends EzCardBasicRequest {

    @SerializedName("NumberPlate")
    private String numberPlate;

    @SerializedName("DriverNumber")
    private String driverNumber;

    @SerializedName("Amount")
    private String amount;

    public AcerTaxiQuery() {
        this.setServiceType("EZCardDataInQueryForTaxi");
    }

    public String getNumberPlate() {
        return numberPlate;
    }

    public void setNumberPlate(String numberPlate) {
        this.numberPlate = numberPlate;
    }

    public String getDriverNumber() {
        return driverNumber;
    }

    public void setDriverNumber(String driverNumber) {
        this.driverNumber = driverNumber;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
