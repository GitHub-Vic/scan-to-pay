package co.intella.domain.DGPay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class OrderInquiry {

    @SerializedName("MessageTypeID")
    @JsonProperty("MessageTypeID")
    private String messageTypeID;

    @SerializedName("ProcessingCode")
    @JsonProperty("ProcessingCode")
    private String processingCode;

    @SerializedName("OriginalProcessingCode")
    @JsonProperty("OriginalProcessingCode")
    private String originalProcessingCode;

    @SerializedName("TransactionAmount")
    @JsonProperty("TransactionAmount")
    private String transactionAmount;

    @SerializedName("DeductionAmount")
    @JsonProperty("DeductionAmount")
    private String deductionAmount;

    @SerializedName("AddedOrRefundAmount")
    @JsonProperty("AddedOrRefundAmount")
    private String addedOrRefundAmount;

    @SerializedName("TMSerialNumber")
    @JsonProperty("TMSerialNumber")
    private String tmSerialNumber;

    @SerializedName("OrderNumber")
    @JsonProperty("OrderNumber")
    private String orderNumber;

    @SerializedName("MerchantOrderNumber")
    @JsonProperty("MerchantOrderNumber")
    private String merchantOrderNumber;

    @SerializedName("DigitalPaymentTransactionSerialNumber")
    @JsonProperty("DigitalPaymentTransactionSerialNumber")
    private String digitalPaymentTransactionSerialNumber;

    @SerializedName("LocalDate")
    @JsonProperty("LocalDate")
    private String localDate;

    @SerializedName("LocalTime")
    @JsonProperty("LocalTime")
    private String localTime;

    @SerializedName("RetrievalReferenceNumber")
    @JsonProperty("RetrievalReferenceNumber")
    private String retrievalReferenceNumber;

    @SerializedName("AuthorizationIdentificationResponse")
    @JsonProperty("AuthorizationIdentificationResponse")
    private String authorizationIdentificationResponse;

    @SerializedName("ResponseCode")
    @JsonProperty("ResponseCode")
    private String responseCode;

    @SerializedName("OriginalTransactionResponseCode")
    @JsonProperty("OriginalTransactionResponseCode")
    private String originalTransactionResponseCode;

    @SerializedName("TerminalID")
    @JsonProperty("TerminalID")
    private String terminalID;

    @SerializedName("SpecialStoreID")
    @JsonProperty("SpecialStoreID")
    private String specialStoreID;

    @SerializedName("OriginalSpecialStoreID")
    @JsonProperty("OriginalSpecialStoreID")
    private String originalSpecialStoreID;

    @SerializedName("BatchNumber")
    @JsonProperty("BatchNumber")
    private String batchNumber;

    @SerializedName("TransactionStatus")
    @JsonProperty("TransactionStatus")
    private String transactionStatus;

    @SerializedName("TMLocationID")
    @JsonProperty("TMLocationID")
    private String tmLocationID;

    @SerializedName("PosSystem")
    @JsonProperty("PosSystem")
    private String posSystem;

    @SerializedName("OriginalTransactionTerminalID")
    @JsonProperty("OriginalTransactionTerminalID")
    private String originalTransactionTerminalID;

    @SerializedName("OriginalTransactionRRN")
    @JsonProperty("OriginalTransactionRRN")
    private String originalTransactionRRN;

    @SerializedName("OriginalTransactionDate")
    @JsonProperty("OriginalTransactionDate")
    private String originalTransactionDate;

    @SerializedName("ConductionPagePosition")
    @JsonProperty("ConductionPagePosition")
    private String conductionPagePosition;

    @SerializedName("VendorAPIPassword")
    @JsonProperty("VendorAPIPassword")
    private String vendorAPIPassword;

    @SerializedName("APPIndustryCode")
    @JsonProperty("APPIndustryCode")
    private String appIndustryCode;

    public String getMessageTypeID() {
        return messageTypeID;
    }

    public void setMessageTypeID(String messageTypeID) {
        this.messageTypeID = messageTypeID;
    }

    public String getProcessingCode() {
        return processingCode;
    }

    public void setProcessingCode(String processingCode) {
        this.processingCode = processingCode;
    }

    public String getOriginalProcessingCode() {
        return originalProcessingCode;
    }

    public void setOriginalProcessingCode(String originalProcessingCode) {
        this.originalProcessingCode = originalProcessingCode;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getDeductionAmount() {
        return deductionAmount;
    }

    public void setDeductionAmount(String deductionAmount) {
        this.deductionAmount = deductionAmount;
    }

    public String getAddedOrRefundAmount() {
        return addedOrRefundAmount;
    }

    public void setAddedOrRefundAmount(String addedOrRefundAmount) {
        this.addedOrRefundAmount = addedOrRefundAmount;
    }

    public String getTmSerialNumber() {
        return tmSerialNumber;
    }

    public void setTmSerialNumber(String tmSerialNumber) {
        this.tmSerialNumber = tmSerialNumber;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getMerchantOrderNumber() {
        return merchantOrderNumber;
    }

    public void setMerchantOrderNumber(String merchantOrderNumber) {
        this.merchantOrderNumber = merchantOrderNumber;
    }

    public String getDigitalPaymentTransactionSerialNumber() {
        return digitalPaymentTransactionSerialNumber;
    }

    public void setDigitalPaymentTransactionSerialNumber(String digitalPaymentTransactionSerialNumber) {
        this.digitalPaymentTransactionSerialNumber = digitalPaymentTransactionSerialNumber;
    }

    public String getLocalDate() {
        return localDate;
    }

    public void setLocalDate(String localDate) {
        this.localDate = localDate;
    }

    public String getLocalTime() {
        return localTime;
    }

    public void setLocalTime(String localTime) {
        this.localTime = localTime;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = retrievalReferenceNumber;
    }

    public String getAuthorizationIdentificationResponse() {
        return authorizationIdentificationResponse;
    }

    public void setAuthorizationIdentificationResponse(String authorizationIdentificationResponse) {
        this.authorizationIdentificationResponse = authorizationIdentificationResponse;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getOriginalTransactionResponseCode() {
        return originalTransactionResponseCode;
    }

    public void setOriginalTransactionResponseCode(String originalTransactionResponseCode) {
        this.originalTransactionResponseCode = originalTransactionResponseCode;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getSpecialStoreID() {
        return specialStoreID;
    }

    public void setSpecialStoreID(String specialStoreID) {
        this.specialStoreID = specialStoreID;
    }

    public String getOriginalSpecialStoreID() {
        return originalSpecialStoreID;
    }

    public void setOriginalSpecialStoreID(String originalSpecialStoreID) {
        this.originalSpecialStoreID = originalSpecialStoreID;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getTmLocationID() {
        return tmLocationID;
    }

    public void setTmLocationID(String tmLocationID) {
        this.tmLocationID = tmLocationID;
    }

    public String getPosSystem() {
        return posSystem;
    }

    public void setPosSystem(String posSystem) {
        this.posSystem = posSystem;
    }

    public String getOriginalTransactionTerminalID() {
        return originalTransactionTerminalID;
    }

    public void setOriginalTransactionTerminalID(String originalTransactionTerminalID) {
        this.originalTransactionTerminalID = originalTransactionTerminalID;
    }

    public String getOriginalTransactionRRN() {
        return originalTransactionRRN;
    }

    public void setOriginalTransactionRRN(String originalTransactionRRN) {
        this.originalTransactionRRN = originalTransactionRRN;
    }

    public String getOriginalTransactionDate() {
        return originalTransactionDate;
    }

    public void setOriginalTransactionDate(String originalTransactionDate) {
        this.originalTransactionDate = originalTransactionDate;
    }

    public String getConductionPagePosition() {
        return conductionPagePosition;
    }

    public void setConductionPagePosition(String conductionPagePosition) {
        this.conductionPagePosition = conductionPagePosition;
    }

    public String getVendorAPIPassword() {
        return vendorAPIPassword;
    }

    public void setVendorAPIPassword(String vendorAPIPassword) {
        this.vendorAPIPassword = vendorAPIPassword;
    }

    public String getAppIndustryCode() {
        return appIndustryCode;
    }

    public void setAppIndustryCode(String appIndustryCode) {
        this.appIndustryCode = appIndustryCode;
    }
}
