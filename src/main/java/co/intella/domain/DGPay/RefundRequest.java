package co.intella.domain.DGPay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class RefundRequest {

    @SerializedName("MessageTypeID")
    @JsonProperty("MessageTypeID")
    private String messageTypeID;

    @SerializedName("PurseID")
    @JsonProperty("PurseID")
    private String purseID;

    @SerializedName("ProcessingCode")
    @JsonProperty("ProcessingCode")
    private String processingCode;

    @SerializedName("TransactionAmount")
    @JsonProperty("TransactionAmount")
    private String transactionAmount;

    @SerializedName("AddedOrRefundAmount")
    @JsonProperty("AddedOrRefundAmount")
    private String addedOrRefundAmount;

    @SerializedName("TMSerialNumber")
    @JsonProperty("TMSerialNumber")
    private String tmSerialNumber;

    @SerializedName("OrderNumber")
    @JsonProperty("OrderNumber")
    private String orderNumber;

    @SerializedName("MerchantOrderNumber")
    @JsonProperty("MerchantOrderNumber")
    private String merchantOrderNumber;

    @SerializedName("OriginalOrderNumber")
    @JsonProperty("OriginalOrderNumber")
    private String originalOrderNumber;

    @SerializedName("LocalTime")
    @JsonProperty("LocalTime")
    private String localTime;

    @SerializedName("LocalDate")
    @JsonProperty("LocalDate")
    private String localDate;

    @SerializedName("CardExpiryDate")
    @JsonProperty("CardExpiryDate")
    private String cardExpiryDate;

    @SerializedName("RetrievalReferenceNumber")
    @JsonProperty("RetrievalReferenceNumber")
    private String retrievalReferenceNumber;

    @SerializedName("TerminalID")
    @JsonProperty("TerminalID")
    private String terminalID;

    @SerializedName("TerminalIP")
    @JsonProperty("TerminalIP")
    private String terminalIP;

    @SerializedName("SpecialStoreID")
    @JsonProperty("SpecialStoreID")
    private String specialStoreID;

    @SerializedName("BatchNumber")
    @JsonProperty("BatchNumber")
    private String batchNumber;

    @SerializedName("TMLocationID")
    @JsonProperty("TMLocationID")
    private String tmLocationID;

    @SerializedName("PosNumber")
    @JsonProperty("PosNumber")
    private String posNumber;

    @SerializedName("PosSystem")
    @JsonProperty("PosSystem")
    private String posSystem;

    @SerializedName("CashierID")
    @JsonProperty("CashierID")
    private String cashierID;

    @SerializedName("OperatorID")
    @JsonProperty("OperatorID")
    private String operatorID;

    @SerializedName("VendorAPIPassword")
    @JsonProperty("VendorAPIPassword")
    private String vendorAPIPassword;

    public String getMessageTypeID() {
        return messageTypeID;
    }

    public void setMessageTypeID(String messageTypeID) {
        this.messageTypeID = messageTypeID;
    }

    public String getProcessingCode() {
        return processingCode;
    }

    public void setProcessingCode(String processingCode) {
        this.processingCode = processingCode;
    }

    public String getLocalTime() {
        return localTime;
    }

    public void setLocalTime(String localTime) {
        this.localTime = localTime;
    }

    public String getLocalDate() {
        return localDate;
    }

    public void setLocalDate(String localDate) {
        this.localDate = localDate;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = retrievalReferenceNumber;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getTerminalIP() {
        return terminalIP;
    }

    public void setTerminalIP(String terminalIP) {
        this.terminalIP = terminalIP;
    }

    public String getSpecialStoreID() {
        return specialStoreID;
    }

    public void setSpecialStoreID(String specialStoreID) {
        this.specialStoreID = specialStoreID;
    }

    public String getVendorAPIPassword() {
        return vendorAPIPassword;
    }

    public void setVendorAPIPassword(String vendorAPIPassword) {
        this.vendorAPIPassword = vendorAPIPassword;
    }

    public String getTmSerialNumber() {
        return tmSerialNumber;
    }

    public void setTmSerialNumber(String tmSerialNumber) {
        this.tmSerialNumber = tmSerialNumber;
    }

    public String getTmLocationID() {
        return tmLocationID;
    }

    public void setTmLocationID(String tmLocationID) {
        this.tmLocationID = tmLocationID;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getPosNumber() {
        return posNumber;
    }

    public void setPosNumber(String posNumber) {
        this.posNumber = posNumber;
    }

    public String getPosSystem() {
        return posSystem;
    }

    public void setPosSystem(String posSystem) {
        this.posSystem = posSystem;
    }

    public String getCashierID() {
        return cashierID;
    }

    public void setCashierID(String cashierID) {
        this.cashierID = cashierID;
    }

    public String getOperatorID() {
        return operatorID;
    }

    public void setOperatorID(String operatorID) {
        this.operatorID = operatorID;
    }

    public String getCardExpiryDate() {
        return cardExpiryDate;
    }

    public void setCardExpiryDate(String cardExpiryDate) {
        this.cardExpiryDate = cardExpiryDate;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getPurseID() {
        return purseID;
    }

    public void setPurseID(String purseID) {
        this.purseID = purseID;
    }

    public String getAddedOrRefundAmount() {
        return addedOrRefundAmount;
    }

    public void setAddedOrRefundAmount(String addedOrRefundAmount) {
        this.addedOrRefundAmount = addedOrRefundAmount;
    }

    public String getMerchantOrderNumber() {
        return merchantOrderNumber;
    }

    public void setMerchantOrderNumber(String merchantOrderNumber) {
        this.merchantOrderNumber = merchantOrderNumber;
    }

    public String getOriginalOrderNumber() {
        return originalOrderNumber;
    }

    public void setOriginalOrderNumber(String originalOrderNumber) {
        this.originalOrderNumber = originalOrderNumber;
    }

    public String encryptVendorAPIPassword(String processingCode) {

        String value = null;

        byte[] bytes = new byte[]{
                (byte) 0x0E, (byte) 0xDD, (byte) 0x25, (byte) 0xC9, (byte) 0x3A, (byte) 0x28, (byte) 0xD7, (byte) 0xB5, (byte) 0xFF, (byte) 0x5E, (byte) 0x45,
                (byte) 0xDA, (byte) 0x42, (byte) 0xF8, (byte) 0xA1, (byte) 0xB8
        };

        String cKey = "774A64C441C3A37184CAD1C2D340E182";
        //640063 WEB 扣款取消 650063 WEB 退貨
        if (processingCode.equals("650063") || processingCode.equals("640063")) {
            //T0300, T0400, T0437, T1100, T1200, T1300, T3700, T4100, T4200, T5503串接

            value = (new StringBuilder()
                    .append(processingCode)
                    .append(transactionAmount)
                    .append(addedOrRefundAmount)
                    .append(tmSerialNumber)
                    .append(localTime)
                    .append(localDate)
                    .append(retrievalReferenceNumber)
                    .append(terminalID)
                    .append(specialStoreID)
                    .append(tmLocationID)
                    .toString());
            System.out.println(value);

            //加密
//            vendorAPIPassword = (Base64.getEncoder().encodeToString(encrypt(cKey, bytes, value)));
            //取尾部 8 byte並轉成大寫
            vendorAPIPassword = (vendorAPIPassword.substring((vendorAPIPassword.length()-8))).toUpperCase();
        } else {
            vendorAPIPassword = null;
        }
        return vendorAPIPassword;
    }
}
