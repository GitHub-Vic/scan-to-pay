package co.intella.domain.DGPay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class OrderNumberWebDeduction {

    @SerializedName("MessageTypeID")
    @JsonProperty("MessageTypeID")
    private String messageTypeID;

    @SerializedName("ProcessingCode")
    @JsonProperty("ProcessingCode")
    private String processingCode;

    @SerializedName("TransactionAmount")
    @JsonProperty("TransactionAmount")
    private String transactionAmount;

    @SerializedName("TMSerialNumber")
    @JsonProperty("TMSerialNumber")
    private String tmSerialNumber;

    @SerializedName("OrderNumber")
    @JsonProperty("OrderNumber")
    private String orderNumber;

    @SerializedName("LocalDate")
    @JsonProperty("LocalDate")
    private String localDate;

    @SerializedName("LocalTime")
    @JsonProperty("LocalTime")
    private String localTime;

    @SerializedName("RetrievalReferenceNumber")
    @JsonProperty("RetrievalReferenceNumber")
    private String retrievalReferenceNumber;

    @SerializedName("AuthorizationIdentificationResponse")
    @JsonProperty("AuthorizationIdentificationResponse")
    private String authorizationIdentificationResponse;

    @SerializedName("ResponseCode")
    @JsonProperty("ResponseCode")
    private String responseCode;

    @SerializedName("TerminalID")
    @JsonProperty("TerminalID")
    private String terminalID;

    @SerializedName("SpecialStoreID")
    @JsonProperty("SpecialStoreID")
    private String specialStoreID;

    @SerializedName("BatchNumber")
    @JsonProperty("BatchNumber")
    private String batchNumber;

    @SerializedName("TMLocationID")
    @JsonProperty("TMLocationID")
    private String tmLocationID;

    @SerializedName("VendorAPIPassword")
    @JsonProperty("VendorAPIPassword")
    private String vendorAPIPassword;

    @SerializedName("APPIndustryCode")
    @JsonProperty("APPIndustryCode")
    private String appIndustryCode;

    public String getMessageTypeID() {
        return messageTypeID;
    }

    public void setMessageTypeID(String messageTypeID) {
        this.messageTypeID = messageTypeID;
    }

    public String getProcessingCode() {
        return processingCode;
    }

    public void setProcessingCode(String processingCode) {
        this.processingCode = processingCode;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTmSerialNumber() {
        return tmSerialNumber;
    }

    public void setTmSerialNumber(String tmSerialNumber) {
        this.tmSerialNumber = tmSerialNumber;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getLocalDate() {
        return localDate;
    }

    public void setLocalDate(String localDate) {
        this.localDate = localDate;
    }

    public String getLocalTime() {
        return localTime;
    }

    public void setLocalTime(String localTime) {
        this.localTime = localTime;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = retrievalReferenceNumber;
    }

    public String getAuthorizationIdentificationResponse() {
        return authorizationIdentificationResponse;
    }

    public void setAuthorizationIdentificationResponse(String authorizationIdentificationResponse) {
        this.authorizationIdentificationResponse = authorizationIdentificationResponse;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getSpecialStoreID() {
        return specialStoreID;
    }

    public void setSpecialStoreID(String specialStoreID) {
        this.specialStoreID = specialStoreID;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getTmLocationID() {
        return tmLocationID;
    }

    public void setTmLocationID(String tmLocationID) {
        this.tmLocationID = tmLocationID;
    }

    public String getVendorAPIPassword() {
        return vendorAPIPassword;
    }

    public void setVendorAPIPassword(String vendorAPIPassword) {
        this.vendorAPIPassword = vendorAPIPassword;
    }

    public String getAppIndustryCode() {
        return appIndustryCode;
    }

    public void setAppIndustryCode(String appIndustryCode) {
        this.appIndustryCode = appIndustryCode;
    }
}
