package co.intella.domain.DGPay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class Refund {

    @SerializedName("MessageTypeID")
    @JsonProperty("MessageTypeID")
    private String messageTypeID;

    @SerializedName("PurseID")
    @JsonProperty("PurseID")
    private String purseID;

    @SerializedName("ProcessingCode")
    @JsonProperty("ProcessingCode")
    private String processingCode;

    @SerializedName("TransactionAmount")
    @JsonProperty("TransactionAmount")
    private String transactionAmount;

    @SerializedName("DigitalPayTradeBalance")
    @JsonProperty("DigitalPayTradeBalance")
    private String digitalPayTradeBalance;

    @SerializedName("DigitalPayPreTradeBalance")
    @JsonProperty("DigitalPayPreTradeBalance")
    private String digitalPayPreTradeBalance;

    @SerializedName("TMSerialNumber")
    @JsonProperty("TMSerialNumber")
    private String tmSerialNumber;

    @SerializedName("OrderNumber")
    @JsonProperty("OrderNumber")
    private String orderNumber;

    @SerializedName("DigitalPaymentTransactionSerialNumber")
    @JsonProperty("DigitalPaymentTransactionSerialNumber")
    private String digitalPaymentTransactionSerialNumber;

    @SerializedName("LocalDate")
    @JsonProperty("LocalDate")
    private String localDate;

    @SerializedName("LocalTime")
    @JsonProperty("LocalTime")
    private String localTime;

    @SerializedName("CardExpiryDate")
    @JsonProperty("CardExpiryDate")
    private String cardExpiryDate;

    @SerializedName("RetrievalReferenceNumber")
    @JsonProperty("RetrievalReferenceNumber")
    private String retrievalReferenceNumber;

    @SerializedName("AuthorizationIdentificationResponse")
    @JsonProperty("AuthorizationIdentificationResponse")
    private String authorizationIdentificationResponse;

    @SerializedName("ResponseCode")
    @JsonProperty("ResponseCode")
    private String responseCode;

    @SerializedName("ResponseDescription")
    @JsonProperty("ResponseDescription")
    private String responseDescription;

    @SerializedName("TerminalID")
    @JsonProperty("TerminalID")
    private String terminalID;

    @SerializedName("SpecialStoreID")
    @JsonProperty("SpecialStoreID")
    private String specialStoreID;

    @SerializedName("BatchNumber")
    @JsonProperty("BatchNumber")
    private String batchNumber;

    @SerializedName("TMLocationID")
    @JsonProperty("TMLocationID")
    private String tmLocationID;

    public String getMessageTypeID() {
        return messageTypeID;
    }

    public void setMessageTypeID(String messageTypeID) {
        this.messageTypeID = messageTypeID;
    }

    public String getPurseID() {
        return purseID;
    }

    public void setPurseID(String purseID) {
        this.purseID = purseID;
    }

    public String getProcessingCode() {
        return processingCode;
    }

    public void setProcessingCode(String processingCode) {
        this.processingCode = processingCode;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getDigitalPayTradeBalance() {
        return digitalPayTradeBalance;
    }

    public void setDigitalPayTradeBalance(String digitalPayTradeBalance) {
        this.digitalPayTradeBalance = digitalPayTradeBalance;
    }

    public String getDigitalPayPreTradeBalance() {
        return digitalPayPreTradeBalance;
    }

    public void setDigitalPayPreTradeBalance(String digitalPayPreTradeBalance) {
        this.digitalPayPreTradeBalance = digitalPayPreTradeBalance;
    }

    public String getTmSerialNumber() {
        return tmSerialNumber;
    }

    public void setTmSerialNumber(String tmSerialNumber) {
        this.tmSerialNumber = tmSerialNumber;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getDigitalPaymentTransactionSerialNumber() {
        return digitalPaymentTransactionSerialNumber;
    }

    public void setDigitalPaymentTransactionSerialNumber(String digitalPaymentTransactionSerialNumber) {
        this.digitalPaymentTransactionSerialNumber = digitalPaymentTransactionSerialNumber;
    }

    public String getLocalDate() {
        return localDate;
    }

    public void setLocalDate(String localDate) {
        this.localDate = localDate;
    }

    public String getLocalTime() {
        return localTime;
    }

    public void setLocalTime(String localTime) {
        this.localTime = localTime;
    }

    public String getCardExpiryDate() {
        return cardExpiryDate;
    }

    public void setCardExpiryDate(String cardExpiryDate) {
        this.cardExpiryDate = cardExpiryDate;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = retrievalReferenceNumber;
    }

    public String getAuthorizationIdentificationResponse() {
        return authorizationIdentificationResponse;
    }

    public void setAuthorizationIdentificationResponse(String authorizationIdentificationResponse) {
        this.authorizationIdentificationResponse = authorizationIdentificationResponse;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDescription() {
        return responseDescription;
    }

    public void setResponseDescription(String responseDescription) {
        this.responseDescription = responseDescription;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getSpecialStoreID() {
        return specialStoreID;
    }

    public void setSpecialStoreID(String specialStoreID) {
        this.specialStoreID = specialStoreID;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getTmLocationID() {
        return tmLocationID;
    }

    public void setTmLocationID(String tmLocationID) {
        this.tmLocationID = tmLocationID;
    }
}
