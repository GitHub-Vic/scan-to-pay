package co.intella.domain.DGPay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class OrderInquiryRequest {

    @SerializedName("MessageTypeID")
    @JsonProperty("MessageTypeID")
    private String messageTypeID;

    @SerializedName("ProcessingCode")
    @JsonProperty("ProcessingCode")
    private String processingCode;

    @SerializedName("TMSerialNumber")
    @JsonProperty("TMSerialNumber")
    private String tmSerialNumber;

    @SerializedName("OrderNumber")
    @JsonProperty("OrderNumber")
    private String orderNumber;

    @SerializedName("LocalDate")
    @JsonProperty("LocalDate")
    private String localDate;

    @SerializedName("LocalTime")
    @JsonProperty("LocalTime")
    private String localTime;

    @SerializedName("RetrievalReferenceNumber")
    @JsonProperty("RetrievalReferenceNumber")
    private String retrievalReferenceNumber;

    @SerializedName("TerminalID")
    @JsonProperty("TerminalID")
    private String terminalID;

    @SerializedName("SpecialStoreID")
    @JsonProperty("SpecialStoreID")
    private String specialStoreID;

    @SerializedName("TMLocationID")
    @JsonProperty("TMLocationID")
    private String tmLocationID;

    @SerializedName("PosSystem")
    @JsonProperty("PosSystem")
    private String posSystem;

    @SerializedName("VendorAPIPassword")
    @JsonProperty("VendorAPIPassword")
    private String vendorAPIPassword;

    @SerializedName("APPIndustryCode")
    @JsonProperty("APPIndustryCode")
    private String appIndustryCode;

    public String getMessageTypeID() {
        return messageTypeID;
    }

    public void setMessageTypeID(String messageTypeID) {
        this.messageTypeID = messageTypeID;
    }

    public String getProcessingCode() {
        return processingCode;
    }

    public void setProcessingCode(String processingCode) {
        this.processingCode = processingCode;
    }

    public String getTmSerialNumber() {
        return tmSerialNumber;
    }

    public void setTmSerialNumber(String tmSerialNumber) {
        this.tmSerialNumber = tmSerialNumber;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getLocalDate() {
        return localDate;
    }

    public void setLocalDate(String localDate) {
        this.localDate = localDate;
    }

    public String getLocalTime() {
        return localTime;
    }

    public void setLocalTime(String localTime) {
        this.localTime = localTime;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = retrievalReferenceNumber;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getSpecialStoreID() {
        return specialStoreID;
    }

    public void setSpecialStoreID(String specialStoreID) {
        this.specialStoreID = specialStoreID;
    }

    public String getTmLocationID() {
        return tmLocationID;
    }

    public void setTmLocationID(String tmLocationID) {
        this.tmLocationID = tmLocationID;
    }

    public String getPosSystem() {
        return posSystem;
    }

    public void setPosSystem(String posSystem) {
        this.posSystem = posSystem;
    }

    public String getVendorAPIPassword() {
        return vendorAPIPassword;
    }

    public void setVendorAPIPassword(String vendorAPIPassword) {
        this.vendorAPIPassword = vendorAPIPassword;
    }

    public String getAppIndustryCode() {
        return appIndustryCode;
    }

    public void setAppIndustryCode(String appIndustryCode) {
        this.appIndustryCode = appIndustryCode;
    }

    public String encryptVendorAPIPassword(String processingCode) {

        String value = null;

        byte[] bytes = new byte[]{
                (byte) 0x0E, (byte) 0xDD, (byte) 0x25, (byte) 0xC9, (byte) 0x3A, (byte) 0x28, (byte) 0xD7, (byte) 0xB5, (byte) 0xFF, (byte) 0x5E, (byte) 0x45,
                (byte) 0xDA, (byte) 0x42, (byte) 0xF8, (byte) 0xA1, (byte) 0xB8
        };

        String cKey = "774A64C441C3A37184CAD1C2D340E182";
        if (processingCode.equals("306300") && posSystem.equals("W")) {
            //T0300, T1100, T1103, T1200, T1300, T3700, T4100, T4200, T5503串接

            value = (new StringBuilder()
                    .append(processingCode)
                    .append(tmSerialNumber)
                    .append(orderNumber)
                    .append(localTime)
                    .append(localDate)
                    .append(retrievalReferenceNumber)
                    .append(terminalID)
                    .append(specialStoreID)
                    .append(tmLocationID)
                    .toString());
            System.out.println(value);

            //加密
//            vendorAPIPassword = (Base64.getEncoder().encodeToString(encrypt(cKey, bytes, value)));
            //取尾部 8 byte並轉成大寫
            vendorAPIPassword = (vendorAPIPassword.substring((vendorAPIPassword.length() - 8))).toUpperCase();
        } else if (processingCode.equals("306300") && posSystem.equals("I") || posSystem.equals("A") || posSystem.equals("E")) {
            //T0300, T1100, T1200, T1300, T3700, T4100, T4200, T5503串接

            value = (new StringBuilder()
                    .append(processingCode)
                    .append(tmSerialNumber)
                    .append(localTime)
                    .append(localDate)
                    .append(retrievalReferenceNumber)
                    .append(terminalID)
                    .append(specialStoreID)
                    .append(tmLocationID)
                    .toString());
            System.out.println(value);

            //加密
//            vendorAPIPassword = (Base64.getEncoder().encodeToString(encrypt(cKey, bytes, value)));
            //取尾部 8 byte並轉成大寫
            vendorAPIPassword = (vendorAPIPassword.substring((vendorAPIPassword.length() - 8))).toUpperCase();
        }else {
            vendorAPIPassword = null;
        }
        return vendorAPIPassword;
    }
}
