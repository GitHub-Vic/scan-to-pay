package co.intella.domain.TSMC;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class QueryForCardIdRequest {

    @JsonProperty("CardId")
    @SerializedName("CardId")
    private String cardId;

    @JsonProperty("StartDate")
    @SerializedName("StartDate")
    private String startDate;

    @JsonProperty("EndDate")
    @SerializedName("EndDate")
    private String endDate;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
