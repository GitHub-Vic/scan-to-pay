package co.intella.domain.TSMC;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class RefundNotifyRequest {

    @JsonProperty("StoreOrderNo")
    @SerializedName("StoreOrderNo")
    private String storeOrderNo;

    @JsonProperty("StoreRefundNo")
    @SerializedName("StoreRefundNo")
    private String storeRefundNo;

    @JsonProperty("RefundKey")
    @SerializedName("RefundKey")
    private String refundKey;

    @JsonProperty("RefundFee")
    @SerializedName("RefundFee")
    private String refundFee;

    @JsonProperty("RefundMsg")
    @SerializedName("RefundMsg")
    private String refundMsg;

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getStoreRefundNo() {
        return storeRefundNo;
    }

    public void setStoreRefundNo(String storeRefundNo) {
        this.storeRefundNo = storeRefundNo;
    }

    public String getRefundKey() {
        return refundKey;
    }

    public void setRefundKey(String refundKey) {
        this.refundKey = refundKey;
    }

    public String getRefundFee() {
        return refundFee;
    }

    public void setRefundFee(String refundFee) {
        this.refundFee = refundFee;
    }

    public String getRefundMsg() {
        return refundMsg;
    }

    public void setRefundMsg(String refundMsg) {
        this.refundMsg = refundMsg;
    }
}
