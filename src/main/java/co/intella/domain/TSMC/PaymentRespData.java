package co.intella.domain.TSMC;

import co.intella.domain.integration.ResponseGeneralData;
import co.intella.model.TradeDetail;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

public class PaymentRespData extends ResponseGeneralData {
    public PaymentRespData() {
    }

    public PaymentRespData(TradeDetail tradeDetail) {
        this.setErrorCode(SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus()) ? "0000" : "9999");
        this.setErrorDesc(tradeDetail.getStatus());
        this.setMethod(tradeDetail.getMethod());
        this.setCardId(tradeDetail.getUserId());
        this.setFeeType("TWD");
        this.setBody(tradeDetail.getDescription());
        this.setTotalFee(String.valueOf(tradeDetail.getPayment()));
        this.setStoreOrderNo(tradeDetail.getOrderId());
        this.setPaidAt(tradeDetail.getCreateDate());
        this.setOrderStatus(this.convertSingleOrderQueryStatus(tradeDetail));
        this.setRefundedMsg(tradeDetail.getRefundStatus());
        this.setMchId(tradeDetail.getAccountId());
    }

    public PaymentRespData(List<TradeDetail> tradeDetailList) {
        List<PaymentRespData> list = tradeDetailList.parallelStream().map(PaymentRespData::new).collect(Collectors.toList());
        this.setList(list);
    }

    @SerializedName("ErrorCode")
    private String errorCode;

    @SerializedName("ErrorDesc")
    private String errorDesc;

    @SerializedName("Method")
    private String method;

    @SerializedName("MchId")
    private String mchId;

    @SerializedName("FeeType")
    private String feeType;

    @SerializedName("TotalFee")
    private String totalFee;

    @SerializedName("Body")
    private String body;

    @SerializedName("CardId")
    private String cardId;

    @SerializedName("StoreOrderNo")
    private String storeOrderNo;

    @SerializedName("PaidAt")
    private String paidAt;

    @SerializedName("RefundedMsg")
    @JsonProperty(value = "RefundedMsg")
    private String refundedMsg;

    @SerializedName("OrderStatus")
    private String orderStatus;


    @SerializedName("OrderList")
    private List<PaymentRespData> list;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPaidAt() {
        return paidAt;
    }

    public void setPaidAt(String paidAt) {
        this.paidAt = paidAt;
    }

    public List<PaymentRespData> getList() {
        return list;
    }

    public void setList(List<PaymentRespData> list) {
        this.list = list;
    }

    public String getRefundedMsg() {
        return refundedMsg;
    }

    public void setRefundedMsg(String refundedMsg) {
        this.refundedMsg = refundedMsg;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    private String convertSingleOrderQueryStatus(TradeDetail tradeDetail) {
        String status = "3";
        if (SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus()) && StringUtils.isEmpty(tradeDetail.getRefundStatus())) {
            status = "1";
        } else if (SystemInstance.TRADE_WAITING.equals(tradeDetail.getStatus())
                || SystemInstance.TRADE_CREATING_ORDER.equals(tradeDetail.getStatus())) {
            status = "0";
        } else if (SystemInstance.TRADE_FAIL.equals(tradeDetail.getStatus())) {
            status = "2";
        } else if (SystemInstance.TRADE_INTERRUPTED.equals(tradeDetail.getStatus())) {
            status = "4";
        }

        return status;
    }
}
