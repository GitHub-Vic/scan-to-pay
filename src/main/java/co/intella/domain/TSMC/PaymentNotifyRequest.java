package co.intella.domain.TSMC;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class PaymentNotifyRequest {

    @JsonProperty("StoreOrderNo")
    @SerializedName("StoreOrderNo")
    private String storeOrderNo;

    @JsonProperty("FeeType")
    @SerializedName("FeeType")
    private String feeType;

    @JsonProperty("TotalFee")
    @SerializedName("TotalFee")
    private String totalFee;

    @JsonProperty("Body")
    @SerializedName("Body")
    private String body;

    @JsonProperty("CardId")
    @SerializedName("CardId")
    private String cardId;

    @JsonProperty("ErrorCode")
    @SerializedName("ErrorCode")
    private String errorCode;

    @JsonProperty("ErorDesc")
    @SerializedName("ErrorDesc")
    private String errorDesc;

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }
}
