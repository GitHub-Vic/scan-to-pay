package co.intella.domain.weixin;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alexchiu on 2017/3/7.
 */
public class WeixinRefundRequestData extends WeixinBasicRequestData {

    private String tradeType;

    @SerializedName("StoreRefundNo")
    private String outRefundNo;

    @SerializedName("StoreOrderNo")
    private String outTradeNo;

    @SerializedName("SysOrderNo")
    private String transactionId;

    @SerializedName("DeviceInfo")
    private String deviceInfo;

    @SerializedName("TotalFee")
    private String totalFee;

    @SerializedName("RefundFeeType")
    private String refundFeeType;

    @SerializedName("RefundFee")
    private String refundFee;

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getOutRefundNo() {
        return outRefundNo;
    }

    public void setOutRefundNo(String outRefundNo) {
        this.outRefundNo = outRefundNo;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getRefundFeeType() {
        return refundFeeType;
    }

    public void setRefundFeeType(String refundFeeType) {
        this.refundFeeType = refundFeeType;
    }

    public String getRefundFee() {
        return refundFee;
    }

    public void setRefundFee(String refundFee) {
        this.refundFee = refundFee;
    }
}
