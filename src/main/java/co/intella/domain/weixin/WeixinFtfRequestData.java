package co.intella.domain.weixin;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Alexchiu on 2017/3/2.
 */
public class WeixinFtfRequestData extends WeixinBasicRequestData {

    @JsonProperty(value = "trade_type")
    private String trade_type;

    @JsonProperty(value = "time_expire")
    private String time_expire;



    public String getTrade_type() {
        return trade_type;
    }

    public void setTrade_type(String trade_type) {
        this.trade_type = trade_type;
    }

    public String getTime_expire() {
        return time_expire;
    }

    public void setTime_expire(String time_expire) {
        this.time_expire = time_expire;
    }
}
