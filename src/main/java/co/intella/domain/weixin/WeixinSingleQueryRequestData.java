package co.intella.domain.weixin;


import com.google.gson.annotations.SerializedName;

/**
 * Created by Alexchiu on 2017/3/7.
 */
public class WeixinSingleQueryRequestData extends WeixinBasicRequestData {


    private String tradeType;

    @SerializedName("StoreOrderNo")
    private String outTradeNo;

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }
}
