package co.intella.domain.weixin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

/**
 * @author Alex
 */

public class WeixinSingleQueryResponseData {
    @SerializedName("STORE_ID")
    @JsonProperty(value = "STORE_ID")
    private String storeId;

    @SerializedName("SYS_ORDER_NO")
    @JsonProperty(value = "SYS_ORDER_NO")
    private String sysOrderNo;

    @SerializedName("STORE_ORDER_NO")
    @JsonProperty(value = "STORE_ORDER_NO")
    private String storeOrderNo;

    @SerializedName("WEIXIN_TRANSACTION_ID")
    @JsonProperty(value = "WEIXIN_TRANSACTION_ID")
    private String weixinTransactionId;

    @SerializedName("SYS_STATUS")
    @JsonProperty(value = "SYS_STATUS")
    private String sysStatus;

    @SerializedName("WEIXIN_TRADE_STATE")
    @JsonProperty(value = "WEIXIN_TRADE_STATE")
    private String weixinTradeState;

    private String weixinTradeDesc;

    private BigDecimal amount;

    private String isRefund;

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getSysOrderNo() {
        return sysOrderNo;
    }

    public void setSysOrderNo(String sysOrderNo) {
        this.sysOrderNo = sysOrderNo;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getWeixinTransactionId() {
        return weixinTransactionId;
    }

    public void setWeixinTransactionId(String weixinTransactionId) {
        this.weixinTransactionId = weixinTransactionId;
    }

    public String getSysStatus() {
        return sysStatus;
    }

    public void setSysStatus(String sysStatus) {
        this.sysStatus = sysStatus;
    }

    public String getWeixinTradeState() {
        return weixinTradeState;
    }

    public void setWeixinTradeState(String weixinTradeState) {
        this.weixinTradeState = weixinTradeState;
    }

    public String getWeixinTradeDesc() {
        return weixinTradeDesc;
    }

    public void setWeixinTradeDesc(String weixinTradeDesc) {
        this.weixinTradeDesc = weixinTradeDesc;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getIsRefund() {
        return isRefund;
    }

    public void setIsRefund(String isRefund) {
        this.isRefund = isRefund;
    }
}
