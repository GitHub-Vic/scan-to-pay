package co.intella.domain.weixin;


/**
 * Created by Alexchiu on 2017/3/7.
 */
public class WeixinOrderQueryRequestData extends WeixinBasicRequestData {

    private String tradeType;
    private String deviceInfo;
    private String startDate;
    private String endDate;

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
