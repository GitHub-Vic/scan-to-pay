package co.intella.domain.weixin;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alexchiu on 2017/3/6.
 */
public class WeixinJsapiRequestData extends WeixinBasicRequestData {

//    @SerializedName("trade_type")
    private String tradeType;
    @SerializedName("TimeExpire")
    private String timeExpire;
    @SerializedName("StoreOrderNo")
    private String outTradeNo;
    @SerializedName("Body")
    private String body;
//    @SerializedName("fee_type")
    private String feeType;
    @SerializedName("TotalFee")
    private String totalFee;
    private String detail;
    @SerializedName("DeviceInfo")
    private String deviceInfo;

    @SerializedName("StoreInfo")
    private String storeId;


    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getTimeExpire() {
        return timeExpire;
    }

    public void setTimeExpire(String timeExpire) {
        this.timeExpire = timeExpire;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }
}
