package co.intella.domain.weixin;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by Alexchiu on 2017/3/6.
 */
public class WeixinBasicRequestData {

    // todo field name should not contain '_'

    //API common
    private String ver;
    private String method;
    @JsonProperty(value = "mch_id")
    private String mchId;
    @JsonProperty(value = "nonce_str")
    private String nonceStr;
    private String sign;
    @JsonProperty(value = "sign_type")
    private String signType;
    //APP item





    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getNonceStr() {
        return nonceStr;
    }

    public void setNonceStr(String nonceStr) {
        this.nonceStr = nonceStr;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getSignType() {
        return signType;
    }

    public void setSignType(String signType) {
        this.signType = signType;
    }
}
