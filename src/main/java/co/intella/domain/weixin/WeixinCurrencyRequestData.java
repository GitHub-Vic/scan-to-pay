package co.intella.domain.weixin;


/**
 * Created by Alexchiu on 2017/3/7.
 */
public class WeixinCurrencyRequestData extends WeixinBasicRequestData {

    // todo field name should not contain '_'

    private String tradeType;
    private String currency;
    private String exchangeCurrency;


    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getExchangeCurrency() {
        return exchangeCurrency;
    }

    public void setExchangeCurrency(String exchangeCurrency) {
        this.exchangeCurrency = exchangeCurrency;
    }
}
