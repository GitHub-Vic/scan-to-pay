package co.intella.domain.weixin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Andy on 2017/3/13.
 */
public class WeixinMicropayRequestData extends WeixinBasicRequestData {

        private String attach;

        @SerializedName("AuthCode")
		private String authCode;

        @SerializedName("Body")
		private String body;

		private String detail;

        @SerializedName("DeviceInfo")
		private String deviceInfo;

        @SerializedName("FeeType")
		private String feeType;

        @SerializedName("GoodsTag")
		private String goodsTag;

        @JsonProperty("OutTradeNo")
        @SerializedName("StoreOrderNo")
		private String outTradeNo;

        @SerializedName("TotalFee")
		private String totalFee;

		private String tradeType;

        public String getAttach() {
                return attach;
        }

        public void setAttach(String attach) {
                this.attach = attach;
        }

        public String getAuthCode() {
                return authCode;
        }

        public void setAuthCode(String authCode) {
                this.authCode = authCode;
        }

        public String getBody() {
                return body;
        }

        public void setBody(String body) {
                this.body = body;
        }

        public String getDetail() {
                return detail;
        }

        public void setDetail(String detail) {
                this.detail = detail;
        }

        public String getDeviceInfo() {
                return deviceInfo;
        }

        public void setDeviceInfo(String deviceInfo) {
                this.deviceInfo = deviceInfo;
        }

        public String getFeeType() {
                return feeType;
        }

        public void setFeeType(String feeType) {
                this.feeType = feeType;
        }

        public String getGoodsTag() {
                return goodsTag;
        }

        public void setGoodsTag(String goodsTag) {
                this.goodsTag = goodsTag;
        }

        public String getOutTradeNo() {
                return outTradeNo;
        }

        public void setOutTradeNo(String outTradeNo) {
                this.outTradeNo = outTradeNo;
        }

        public String getTotalFee() {
                return totalFee;
        }

        public void setTotalFee(String totalFee) {
                this.totalFee = totalFee;
        }

        public String getTradeType() {
                return tradeType;
        }

        public void setTradeType(String tradeType) {
                this.tradeType = tradeType;
        }

}
