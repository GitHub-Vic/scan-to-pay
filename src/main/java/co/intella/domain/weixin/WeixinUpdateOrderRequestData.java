package co.intella.domain.weixin;

import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class WeixinUpdateOrderRequestData extends  WeixinBasicRequestData {

    private String tradeType;


    @SerializedName("StoreOrderNo")
    private String outTradeNo;

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }
}
