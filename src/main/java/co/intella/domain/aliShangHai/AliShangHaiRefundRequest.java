package co.intella.domain.aliShangHai;

/**
 * @author Andy Lin
 */
public class AliShangHaiRefundRequest extends AliShangHaiRequestDetail {

    private String outTradeNo;

    private String alipayTransId;

    private String partnerRefundId;

    private String refundAmount;

    private String currency;

    private String refundReason;

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getAlipayTransId() {
        return alipayTransId;
    }

    public void setAlipayTransId(String alipayTransId) {
        this.alipayTransId = alipayTransId;
    }

    public String getPartnerRefundId() {
        return partnerRefundId;
    }

    public void setPartnerRefundId(String partnerRefundId) {
        this.partnerRefundId = partnerRefundId;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getRefundReason() {
        return refundReason;
    }

    public void setRefundReason(String refundReason) {
        this.refundReason = refundReason;
    }
}
