package co.intella.domain.aliShangHai;

/**
 * @author Andy Lin
 */
public class AliShangHaiRefundResponse extends AliShangHaiResponseDetail{

    private String resultCode;

    private String outTradeNo;

    private String alipayTransId;

    private String partnerRefundId;

    private String refundAmount;

    private String currency;

    private String exchangeRate;

    private String refundAmountCny;

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getAlipayTransId() {
        return alipayTransId;
    }

    public void setAlipayTransId(String alipayTransId) {
        this.alipayTransId = alipayTransId;
    }

    public String getPartnerRefundId() {
        return partnerRefundId;
    }

    public void setPartnerRefundId(String partnerRefundId) {
        this.partnerRefundId = partnerRefundId;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getRefundAmountCny() {
        return refundAmountCny;
    }

    public void setRefundAmountCny(String refundAmountCny) {
        this.refundAmountCny = refundAmountCny;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }
}
