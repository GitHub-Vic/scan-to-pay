package co.intella.domain.aliShangHai;

/**
 * @author Andy Lin
 */
public class AliShangHaiOLpayRequest extends AliShangHaiRequestDetail{

    private String notifyUrl;

    private String outTradeNo;

    private String subject;

    private String totalFee;

    private String sellerId;

    private String sellerEmail;

    private String body;

    private String showUrl;

    private String currency;

    private String transCurrency;

    private String price;

    private String quantity;

    private String goodsDetail;

    private AliShangHaiRequestExtendParams extendParams;

    private String itBPay;

    private String passbackParameters;

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerEmail() {
        return sellerEmail;
    }

    public void setSellerEmail(String sellerEmail) {
        this.sellerEmail = sellerEmail;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getShowUrl() {
        return showUrl;
    }

    public void setShowUrl(String showUrl) {
        this.showUrl = showUrl;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTransCurrency() {
        return transCurrency;
    }

    public void setTransCurrency(String transCurrency) {
        this.transCurrency = transCurrency;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getGoodsDetail() {
        return goodsDetail;
    }

    public void setGoodsDetail(String goodsDetail) {
        this.goodsDetail = goodsDetail;
    }

    public AliShangHaiRequestExtendParams getExtendParams() {
        return extendParams;
    }

    public void setExtendParams(AliShangHaiRequestExtendParams extendParams) {
        this.extendParams = extendParams;
    }

    public String getItBPay() {
        return itBPay;
    }

    public void setItBPay(String itBPay) {
        this.itBPay = itBPay;
    }

    public String getPassbackParameters() {
        return passbackParameters;
    }

    public void setPassbackParameters(String passbackParameters) {
        this.passbackParameters = passbackParameters;
    }
}
