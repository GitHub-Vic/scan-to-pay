package co.intella.domain.aliShangHai;

/**
 * @author Andy Lin
 */
public class AliShangHaiResponse {

    private String statusCode;

    private String statusDesc;

    private String service;

    private String mchId;

    private String sign;

    private String responseTime;

    private AliShangHaiResponseDetail response;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }

    public AliShangHaiResponseDetail getResponse() {
        return response;
    }

    public void setResponse(AliShangHaiResponseDetail response) {
        this.response = response;
    }
}
