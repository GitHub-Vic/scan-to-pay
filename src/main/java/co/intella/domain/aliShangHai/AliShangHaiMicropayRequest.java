package co.intella.domain.aliShangHai;

import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class AliShangHaiMicropayRequest extends AliShangHaiRequestDetail {

    private String quantity;

    private String transName;

    private String outTradeNo;

    private String currency;

    private String totalFee;

    private String buyerIdentityCode;

    private String identityCodeType;

    private String transCreateTime;

    private String memo;

    private AliShangHaiRequestExtendParams extendParams;

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTransName() {
        return transName;
    }

    public void setTransName(String transName) {
        this.transName = transName;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getBuyerIdentityCode() {
        return buyerIdentityCode;
    }

    public void setBuyerIdentityCode(String buyerIdentityCode) {
        this.buyerIdentityCode = buyerIdentityCode;
    }

    public String getIdentityCodeType() {
        return identityCodeType;
    }

    public void setIdentityCodeType(String identityCodeType) {
        this.identityCodeType = identityCodeType;
    }

    public String getTransCreateTime() {
        return transCreateTime;
    }

    public void setTransCreateTime(String transCreateTime) {
        this.transCreateTime = transCreateTime;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public AliShangHaiRequestExtendParams getExtendParams() {
        return extendParams;
    }

    public void setExtendParams(AliShangHaiRequestExtendParams extendParams) {
        this.extendParams = extendParams;
    }
}
