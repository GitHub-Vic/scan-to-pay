package co.intella.domain.aliShangHai;

/**
 * @author Andy Lin
 */
public class AliShangHaiQueryRequest extends AliShangHaiRequestDetail {

    private String outTradeNo;

    private String alipayTransId;

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getAlipayTransId() {
        return alipayTransId;
    }

    public void setAlipayTransId(String alipayTransId) {
        this.alipayTransId = alipayTransId;
    }
}
