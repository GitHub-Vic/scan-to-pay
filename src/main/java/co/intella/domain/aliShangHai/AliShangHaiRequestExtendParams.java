package co.intella.domain.aliShangHai;

/**
 * @author Andy Lin
 */
public class AliShangHaiRequestExtendParams {

    private String secondaryMerchantId;

    private String secondaryMerchantName;

    private String secondaryMerchantIndustry;

    private String storeName;

    private String storeId;

    private String terminalId;

    private String sysServiceProviderId;

    public String getSecondaryMerchantId() {
        return secondaryMerchantId;
    }

    public void setSecondaryMerchantId(String secondaryMerchantId) {
        this.secondaryMerchantId = secondaryMerchantId;
    }

    public String getSecondaryMerchantName() {
        return secondaryMerchantName;
    }

    public void setSecondaryMerchantName(String secondaryMerchantName) {
        this.secondaryMerchantName = secondaryMerchantName;
    }

    public String getSecondaryMerchantIndustry() {
        return secondaryMerchantIndustry;
    }

    public void setSecondaryMerchantIndustry(String secondaryMerchantIndustry) {
        this.secondaryMerchantIndustry = secondaryMerchantIndustry;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getSysServiceProviderId() {
        return sysServiceProviderId;
    }

    public void setSysServiceProviderId(String sysServiceProviderId) {
        this.sysServiceProviderId = sysServiceProviderId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }
}
