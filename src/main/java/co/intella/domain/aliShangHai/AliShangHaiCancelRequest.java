package co.intella.domain.aliShangHai;

/**
 * @author Andy Lin
 */
public class AliShangHaiCancelRequest extends AliShangHaiRequestDetail{

    private String outTradeNo;

    private String tradeNo;

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }
}
