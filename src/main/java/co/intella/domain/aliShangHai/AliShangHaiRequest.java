package co.intella.domain.aliShangHai;

/**
 * @author Andy Lin
 */
public class AliShangHaiRequest {

    private String service;

    private String mchId;

//    private String sign;

    private String createTime;

    private AliShangHaiRequestDetail request;

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

//    public String getSign() {
//        return sign;
//    }
//
//    public void setSign(String sign) {
//        this.sign = sign;
//    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public AliShangHaiRequestDetail getRequest() {
        return request;
    }

    public void setRequest(AliShangHaiRequestDetail request) {
        this.request = request;
    }
}
