package co.intella.domain.aliShangHai;

/**
 * @author Andy Lin
 */
public class AliShangHaiQueryResponse extends AliShangHaiResponseDetail {

    private String alipayTransStatus;

    private String resultCode;

    private String alipayBuyerLoginId;

    private String alipayBuyerUserId;

    private String outTradeNo;

    private String alipayTransId;

    private String alipayPayTime;

    private String currency;

    private String transAmount;

    private String exchangeRate;

    private String transAmountCny;

    private String mDscountForexAmount;

    public String getAlipayTransStatus() {
        return alipayTransStatus;
    }

    public void setAlipayTransStatus(String alipayTransStatus) {
        this.alipayTransStatus = alipayTransStatus;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getAlipayTransId() {
        return alipayTransId;
    }

    public void setAlipayTransId(String alipayTransId) {
        this.alipayTransId = alipayTransId;
    }

    public String getAlipayPayTime() {
        return alipayPayTime;
    }

    public void setAlipayPayTime(String alipayPayTime) {
        this.alipayPayTime = alipayPayTime;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTransAmount() {
        return transAmount;
    }

    public void setTransAmount(String transAmount) {
        this.transAmount = transAmount;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getTransAmountCny() {
        return transAmountCny;
    }

    public void setTransAmountCny(String transAmountCny) {
        this.transAmountCny = transAmountCny;
    }

    public String getmDscountForexAmount() {
        return mDscountForexAmount;
    }

    public void setmDscountForexAmount(String mDscountForexAmount) {
        this.mDscountForexAmount = mDscountForexAmount;
    }

    public String getAlipayBuyerLoginId() {
        return alipayBuyerLoginId;
    }

    public void setAlipayBuyerLoginId(String alipayBuyerLoginId) {
        this.alipayBuyerLoginId = alipayBuyerLoginId;
    }

    public String getAlipayBuyerUserId() {
        return alipayBuyerUserId;
    }

    public void setAlipayBuyerUserId(String alipayBuyerUserId) {
        this.alipayBuyerUserId = alipayBuyerUserId;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }
}
