package co.intella.domain.aliShangHai;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Peilun
 */

public class AlipayPreCreateAsync {
    // class Feild {
    // public String NOTIFY_TIME = "notify_time";
    // public String NOTIFY_TYPE = "notify_type";
    // public String SIGN_TYPE = "sign_type";
    // public String SIGN = "sign";
    // public String NOTIFY_ACTION_TYPE = "notify_action_type";
    // public String NOTIFY_ID = "notify_id";
    // public String OUT_TRADE_NO = "out_trade_no";
    // public String SUBJECT = "subject";
    // public String TRADE_NO = "trade_no";
    // public String TRADE_STATUS = "trade_status";
    // public String GMT_CREATE = "gmt_create";
    // public String GMT_PAYMENT = "gmt_payment";
    // public String SELLER_EMAIL = "seller_email";
    // public String BUYER_EMAIL = "buyer_email";
    // public String SELLER_ID = "seller_id";
    // public String BUYER_ID = "buyer_id";
    // public String PRICE = "price";
    // public String QUANTITY = "quantity";
    // public String TOTAL_FEE = "total_fee";
    // public String TRANS_AMOUNT = "trans_amount";
    // public String CURRENCY = "currency";
    // public String TRANS_CURRENCY = "trans_currency";
    // public String FOREX_RATE = "forex_rate";
    // public String BODY = "body";
    // public String REFUND_FEE = "refund_fee";
    // public String PAYTOOLS_PAY_AMOUNT = "paytools_pay_amount";
    // public String OUT_BIZ_NO = "out_biz_no";
    // public String EXTRA_COMMON_PARAM = "extra_common_param";
    // public String M_DISCOUNT_FOREX_AMOUNT = "m_discount_forex_amount";
    // }

    @SerializedName("notifyTime")
    @JsonProperty("notifyTime")
    private String notifyTime;

    @SerializedName("notifyType")
    @JsonProperty("notifyType")
    private String notifyType;

    @SerializedName("notifyId")
    @JsonProperty("notifyId")
    private String notifyId;

    @SerializedName("signType")
    @JsonProperty("signType")
    private String signType;

    @SerializedName("sign")
    @JsonProperty("sign")
    private String sign;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("notifyActionType")
    @JsonProperty("notifyActionType")
    private String notifyActionType;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("outTradeNo")
    @JsonProperty("outTradeNo")
    private String outTradeNo;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("subject")
    @JsonProperty("subject")
    private String subject;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("tradeNo")
    @JsonProperty("tradeNo")
    private String tradeNo;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("tradeStatus")
    @JsonProperty("tradeStatus")
    private String tradeStatus;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("gmtCreate")
    @JsonProperty("gmtCreate")
    private String gmtCreate;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("gmtPayment")
    @JsonProperty("gmtPayment")
    private String gmtPayment;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("sellerEmail")
    @JsonProperty("sellerEmail")
    private String sellerEmail;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("buyerEmail")
    @JsonProperty("buyerEmail")
    private String buyerEmail;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("sellerId")
    @JsonProperty("sellerId")
    private String sellerId;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("buyerId")
    @JsonProperty("buyerId")
    private String buyerId;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("price")
    @JsonProperty("price")
    private String price;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("quantity")
    @JsonProperty("quantity")
    private String quantity;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("totalFee")
    @JsonProperty("totalFee")
    private String totalFee;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("transAmount")
    @JsonProperty("transAmount")
    private String transAmount;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("currency")
    @JsonProperty("currency")
    private String currency;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("transCurrency")
    @JsonProperty("transCurrency")
    private String transCurrency;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("forexRate")
    @JsonProperty("forexRate")
    private String forexRate;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("body")
    @JsonProperty("body")
    private String body;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("refundFee")
    @JsonProperty("refundFee")
    private String refundFee;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("outBizNo")
    @JsonProperty("outBizNo")
    private String outBizNo;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("paytoolsPayAmount")
    @JsonProperty("paytoolsPayAmount")
    private String paytoolsPayAmount;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("extraCommonParam")
    @JsonProperty("extraCommonParam")
    private String extraCommonParam;

    @JsonInclude(Include.NON_NULL)
    @SerializedName("mDiscountForexAmount")
    @JsonProperty("mDiscountForexAmount")
    private String mDiscountForexAmount;

    public String getNotifyTime() {
        return notifyTime;
    }

    public void setNotifyTime(String notifyTime) {
        this.notifyTime = notifyTime;
    }

    public String getNotifyType() {
        return notifyType;
    }

    public void setNotifyType(String notifyType) {
        this.notifyType = notifyType;
    }

    public String getNotifyId() {
        return notifyId;
    }

    public void setNotifyId(String notifyId) {
        this.notifyId = notifyId;
    }

    public String getSignType() {
        return signType;
    }

    public void setSignType(String signType) {
        this.signType = signType;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getNotifyActionType() {
        return notifyActionType;
    }

    public void setNotifyActionType(String notifyActionType) {
        this.notifyActionType = notifyActionType;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public String getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    public String getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getGmtPayment() {
        return gmtPayment;
    }

    public void setGmtPayment(String gmtPayment) {
        this.gmtPayment = gmtPayment;
    }

    public String getSellerEmail() {
        return sellerEmail;
    }

    public void setSellerEmail(String sellerEmail) {
        this.sellerEmail = sellerEmail;
    }

    public String getBuyerEmail() {
        return buyerEmail;
    }

    public void setBuyerEmail(String buyerEmail) {
        this.buyerEmail = buyerEmail;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getTransAmount() {
        return transAmount;
    }

    public void setTransAmount(String transAmount) {
        this.transAmount = transAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTransCurrency() {
        return transCurrency;
    }

    public void setTransCurrency(String transCurrency) {
        this.transCurrency = transCurrency;
    }

    public String getForexRate() {
        return forexRate;
    }

    public void setForexRate(String forexRate) {
        this.forexRate = forexRate;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getRefundFee() {
        return refundFee;
    }

    public void setRefundFee(String refundFee) {
        this.refundFee = refundFee;
    }

    public String getOutBizNo() {
        return outBizNo;
    }

    public void setOutBizNo(String outBizNo) {
        this.outBizNo = outBizNo;
    }

    public String getPaytoolsPayAmount() {
        return paytoolsPayAmount;
    }

    public void setPaytoolsPayAmount(String paytoolsPayAmount) {
        this.paytoolsPayAmount = paytoolsPayAmount;
    }

    public String getExtraCommonParam() {
        return extraCommonParam;
    }

    public void setExtraCommonParam(String extraCommonParam) {
        this.extraCommonParam = extraCommonParam;
    }

    public String getmDiscountForexAmount() {
        return mDiscountForexAmount;
    }

    public void setmDiscountForexAmount(String mDiscountForexAmount) {
        this.mDiscountForexAmount = mDiscountForexAmount;
    }

    public static AlipayPreCreateAsync init(Map<String, Object> map) throws IllegalArgumentException, IllegalAccessException {

        AlipayPreCreateAsync vo = new AlipayPreCreateAsync();

        Field[] fields = AlipayPreCreateAsync.class.getDeclaredFields();
        Map<String, Field> fieldMap = new HashMap<String, Field>();
        for (Field f : fields) {
            String name = (f.getAnnotationsByType(SerializedName.class)[0]).value();
            fieldMap.put(name, f);
        }
        for (String key : map.keySet()) {
            Field f = fieldMap.get(key);
            if (f != null) {
                    f.set(vo, map.get(key).toString());
            }
        }

        return vo;
    }

}
