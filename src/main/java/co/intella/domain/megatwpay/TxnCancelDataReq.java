package co.intella.domain.megatwpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class TxnCancelDataReq extends RequestData{

  @SerializedName("Amt")
  @JsonProperty("Amt")
  private String amt;

  @SerializedName("TxnSeqno")
  @JsonProperty("TxnSeqno")
  private String txnSeqNo;

  @SerializedName("CancelAmt")
  @JsonProperty("CancelAmt")
  private String cancelAmt;

  @SerializedName("TerminalNo")
  @JsonProperty("TerminalNo")
  private String terminalNo;

  public String getAmt() {
    return amt;
  }

  public void setAmt(String amt) {
    this.amt = amt;
  }

  public String getTxnSeqNo() {
    return txnSeqNo;
  }

  public void setTxnSeqNo(String txnSeqNo) {
    this.txnSeqNo = txnSeqNo;
  }

  public String getCancelAmt() {
    return cancelAmt;
  }

  public void setCancelAmt(String cancelAmt) {
    this.cancelAmt = cancelAmt;
  }

  public String getTerminalNo() {
    return terminalNo;
  }

  public void setTerminalNo(String terminalNo) {
    this.terminalNo = terminalNo;
  }
}
