package co.intella.domain.megatwpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class TxnNotifyRes {

  @SerializedName("Header")
  @JsonProperty("Header")
  private TxnNotifyHeaderRes txnNotifyHeaderRes;

  public TxnNotifyHeaderRes getTxnNotifyHeaderRes() {
    return txnNotifyHeaderRes;
  }

  public void setTxnNotifyHeaderRes(TxnNotifyHeaderRes txnNotifyHeaderRes) {
    this.txnNotifyHeaderRes = txnNotifyHeaderRes;
  }
}
