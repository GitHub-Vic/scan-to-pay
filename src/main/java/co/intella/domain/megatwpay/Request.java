package co.intella.domain.megatwpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class Request {

  @SerializedName("Header")
  @JsonProperty("Header")
  private RequstHeader requstHeader;

  @SerializedName("Data")
  @JsonProperty("Data")
  private RequestData requestData;

  public RequstHeader getRequstHeader() {
    return requstHeader;
  }

  public void setRequstHeader(RequstHeader requstHeader) {
    this.requstHeader = requstHeader;
  }

  public RequestData getRequestData() {
    return requestData;
  }

  public void setRequestData(RequestData requestData) {
    this.requestData = requestData;
  }
}
