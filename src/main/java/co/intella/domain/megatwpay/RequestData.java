package co.intella.domain.megatwpay;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class RequestData {

  @SerializedName("Pscode")
  @JsonProperty("Pscode")
  private String pscode;

  @SerializedName("OrderNo")
  @JsonProperty("OrderNo")
  private String orderNo;

  public String getPscode() {
    return pscode;
  }

  public void setPscode(String pscode) {
    this.pscode = pscode;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }
}
