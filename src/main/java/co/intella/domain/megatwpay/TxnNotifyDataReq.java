package co.intella.domain.megatwpay;

import co.intella.domain.megatwpay.RequestData;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class TxnNotifyDataReq extends RequestData {

  @SerializedName("Type")
  @JsonProperty("Type")
  private String type;

  @SerializedName("Amt")
  @JsonProperty("Amt")
  private String amt;

  @SerializedName("TxnTime")
  @JsonProperty("TxnTime")
  private String txnTime;

  @SerializedName("TxnSeqno")
  @JsonProperty("TxnSeqno")
  private String txnSeqNo;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getAmt() {
    return amt;
  }

  public void setAmt(String amt) {
    this.amt = amt;
  }

  public String getTxnTime() {
    return txnTime;
  }

  public void setTxnTime(String txnTime) {
    this.txnTime = txnTime;
  }

  public String getTxnSeqNo() {
    return txnSeqNo;
  }

  public void setTxnSeqNo(String txnSeqNo) {
    this.txnSeqNo = txnSeqNo;
  }
}
