package co.intella.domain.megatwpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class SingleOrderDataRes extends ResponseData{

  @SerializedName("Type")
  @JsonProperty("Type")
  private String type;

  @SerializedName("TxnSeqno")
  @JsonProperty("TxnSeqno")
  private String txnSeqNo;

  @SerializedName("Amt")
  @JsonProperty("Amt")
  private String amt;

  @SerializedName("TxnTime")
  @JsonProperty("TxnTime")
  private String txnTime;

  @SerializedName("OrderStatus")
  @JsonProperty("OrderStatus")
  private String orderStatus;

  @SerializedName("TerminalNo")
  @JsonProperty("TerminalNo")
  private String terminalNo;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getTxnSeqNo() {
    return txnSeqNo;
  }

  public void setTxnSeqNo(String txnSeqNo) {
    this.txnSeqNo = txnSeqNo;
  }

  public String getAmt() {
    return amt;
  }

  public void setAmt(String amt) {
    this.amt = amt;
  }

  public String getTxnTime() {
    return txnTime;
  }

  public void setTxnTime(String txnTime) {
    this.txnTime = txnTime;
  }

  public String getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(String orderStatus) {
    this.orderStatus = orderStatus;
  }

  public String getTerminalNo() {
    return terminalNo;
  }

  public void setTerminalNo(String terminalNo) {
    this.terminalNo = terminalNo;
  }
}
