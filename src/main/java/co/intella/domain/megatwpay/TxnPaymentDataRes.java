package co.intella.domain.megatwpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class TxnPaymentDataRes extends ResponseData {

  @SerializedName("Amt")
  @JsonProperty("Amt")
  private String amt;

  @SerializedName("TxnTime")
  @JsonProperty("TxnTime")
  private String txnTime;

  @SerializedName("TxnResult")
  @JsonProperty("TxnResult")
  private String txnResult;

  @SerializedName("TxnSeqno")
  @JsonProperty("TxnSeqno")
  private String TxnSeqNo;

  public String getAmt() {
    return amt;
  }

  public void setAmt(String amt) {
    this.amt = amt;
  }

  public String getTxnTime() {
    return txnTime;
  }

  public void setTxnTime(String txnTime) {
    this.txnTime = txnTime;
  }

  public String getTxnResult() {
    return txnResult;
  }

  public void setTxnResult(String txnResult) {
    this.txnResult = txnResult;
  }

  public String getTxnSeqNo() {
    return TxnSeqNo;
  }

  public void setTxnSeqNo(String txnSeqNo) {
    TxnSeqNo = txnSeqNo;
  }
}
