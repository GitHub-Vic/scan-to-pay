package co.intella.domain.megatwpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class SingleOrderRes {

  @SerializedName("Header")
  @JsonProperty("Header")
  private SingleOrderHeaderRes singleOrderHeaderRes;

  @SerializedName("Data")
  @JsonProperty("Data")
  private SingleOrderDataRes singleOrderDataRes;

  public SingleOrderHeaderRes getSingleOrderHeaderRes() {
    return singleOrderHeaderRes;
  }

  public void setSingleOrderHeaderRes(SingleOrderHeaderRes singleOrderHeaderRes) {
    this.singleOrderHeaderRes = singleOrderHeaderRes;
  }

  public SingleOrderDataRes getSingleOrderDataRes() {
    return singleOrderDataRes;
  }

  public void setSingleOrderDataRes(SingleOrderDataRes singleOrderDataRes) {
    this.singleOrderDataRes = singleOrderDataRes;
  }
}
