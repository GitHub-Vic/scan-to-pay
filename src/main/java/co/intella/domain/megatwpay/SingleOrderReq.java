package co.intella.domain.megatwpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class SingleOrderReq {

  @SerializedName("Header")
  @JsonProperty("Header")
  private SingleOrderHeaderReq singleOrderHeaderReq;

  @SerializedName("Data")
  @JsonProperty("Data")
  private SingleOrderDataReq singleOrderDataReq;

  public SingleOrderHeaderReq getSingleOrderHeaderReq() {
    return singleOrderHeaderReq;
  }

  public void setSingleOrderHeaderReq(SingleOrderHeaderReq singleOrderHeaderReq) {
    this.singleOrderHeaderReq = singleOrderHeaderReq;
  }

  public SingleOrderDataReq getSingleOrderDataReq() {
    return singleOrderDataReq;
  }

  public void setSingleOrderDataReq(SingleOrderDataReq singleOrderDataReq) {
    this.singleOrderDataReq = singleOrderDataReq;
  }
}
