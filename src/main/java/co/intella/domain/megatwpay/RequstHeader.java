package co.intella.domain.megatwpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class RequstHeader {

  @SerializedName("Version")
  @JsonProperty("Version")
  private String version;

  @SerializedName("Sname")
  @JsonProperty("Sname")
  private String sName;

  @SerializedName("Tstamp")
  @JsonProperty("Tstamp")
  private String tStamp;

  @SerializedName("Sign")
  @JsonProperty("Sign")
  private String sign;

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getsName() {
    return sName;
  }

  public void setsName(String sName) {
    this.sName = sName;
  }

  public String gettStamp() {
    return tStamp;
  }

  public void settStamp(String tStamp) {
    this.tStamp = tStamp;
  }

  public String getSign() {
    return sign;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }
}
