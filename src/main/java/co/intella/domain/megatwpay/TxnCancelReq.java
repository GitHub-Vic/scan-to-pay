package co.intella.domain.megatwpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class TxnCancelReq {

  @SerializedName("Header")
  @JsonProperty("Header")
  private TxnCancelHeaderReq txnCancelHeaderReq;

  @SerializedName("Data")
  @JsonProperty("Data")
  private TxnCancelDataReq txnCancelDataReq;

  public TxnCancelHeaderReq getTxnCancelHeaderReq() {
    return txnCancelHeaderReq;
  }

  public void setTxnCancelHeaderReq(TxnCancelHeaderReq txnCancelHeaderReq) {
    this.txnCancelHeaderReq = txnCancelHeaderReq;
  }

  public TxnCancelDataReq getTxnCancelDataReq() {
    return txnCancelDataReq;
  }

  public void setTxnCancelDataReq(TxnCancelDataReq txnCancelDataReq) {
    this.txnCancelDataReq = txnCancelDataReq;
  }
}
