package co.intella.domain.megatwpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class TxnPaymentDataReq extends RequestData{

  @SerializedName("Amt")
  @JsonProperty("Amt")
  private String amt;

  @SerializedName("TxnData")
  @JsonProperty("TxnData")
  private String txnData;

  @SerializedName("AcctDate")
  @JsonProperty("AcctDate")
  private String acctDate;

  @SerializedName("TerminalNo")
  @JsonProperty("TerminalNo")
  private String terminalNo;

  public String getAmt() {
    return amt;
  }

  public void setAmt(String amt) {
    this.amt = amt;
  }

  public String getTxnData() {
    return txnData;
  }

  public void setTxnData(String txnData) {
    this.txnData = txnData;
  }

  public String getAcctDate() {
    return acctDate;
  }

  public void setAcctDate(String acctDate) {
    this.acctDate = acctDate;
  }

  public String getTerminalNo() {
    return terminalNo;
  }

  public void setTerminalNo(String terminalNo) {
    this.terminalNo = terminalNo;
  }
}
