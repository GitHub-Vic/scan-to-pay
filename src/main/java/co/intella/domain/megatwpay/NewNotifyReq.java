package co.intella.domain.megatwpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class NewNotifyReq {

    @SerializedName("storeId")
    @JsonProperty("storeId")
    private String storeId;

    @SerializedName("endpointCode")
    @JsonProperty("endpointCode")
    private String endpointCode;

    @SerializedName("terminalId")
    @JsonProperty("terminalId")
    private String terminalId;

    @SerializedName("txnSeqno")
    @JsonProperty("txnSeqno")
    private String txnSeqno;

    @SerializedName("orderNumber")
    @JsonProperty("orderNumber")
    private String orderNumber;

    @SerializedName("txnType")
    @JsonProperty("txnType")
    private String txnType;

    @SerializedName("txnDateTime")
    @JsonProperty("txnDateTime")
    private String txnDateTime;

    @SerializedName("txnAcctDate")
    @JsonProperty("txnAcctDate")
    private String txnAcctDate;

    @SerializedName("txnCurrency")
    @JsonProperty("txnCurrency")
    private String txnCurrency;

    @SerializedName("txnAmt")
    @JsonProperty("txnAmt")
    private String txnAmt;

    @SerializedName("txnCharge")
    @JsonProperty("txnCharge")
    private String txnCharge;

    @SerializedName("orgOrderNumber")
    @JsonProperty("orgOrderNumber")
    private String orgOrderNumber;

    @SerializedName("payType")
    @JsonProperty("payType")
    private String payType;

    @SerializedName("txnAccNO")
    @JsonProperty("txnAccNO")
    private String txnAccNO;

    @SerializedName("carrierType")
    @JsonProperty("carrierType")
    private String carrierType;

    @SerializedName("carrierId1")
    @JsonProperty("carrierId1")
    private String carrierId1;

    @SerializedName("storeMemo")
    @JsonProperty("storeMemo")
    private String storeMemo;

    @SerializedName("sign")
    @JsonProperty("sign")
    private String sign;

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getEndpointCode() {
        return endpointCode;
    }

    public void setEndpointCode(String endpointCode) {
        this.endpointCode = endpointCode;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getTxnSeqno() {
        return txnSeqno;
    }

    public void setTxnSeqno(String txnSeqno) {
        this.txnSeqno = txnSeqno;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getTxnDateTime() {
        return txnDateTime;
    }

    public void setTxnDateTime(String txnDateTime) {
        this.txnDateTime = txnDateTime;
    }

    public String getTxnAcctDate() {
        return txnAcctDate;
    }

    public void setTxnAcctDate(String txnAcctDate) {
        this.txnAcctDate = txnAcctDate;
    }

    public String getTxnCurrency() {
        return txnCurrency;
    }

    public void setTxnCurrency(String txnCurrency) {
        this.txnCurrency = txnCurrency;
    }

    public String getTxnAmt() {
        return txnAmt;
    }

    public void setTxnAmt(String txnAmt) {
        this.txnAmt = txnAmt;
    }

    public String getTxnCharge() {
        return txnCharge;
    }

    public void setTxnCharge(String txnCharge) {
        this.txnCharge = txnCharge;
    }

    public String getOrgOrderNumber() {
        return orgOrderNumber;
    }

    public void setOrgOrderNumber(String orgOrderNumber) {
        this.orgOrderNumber = orgOrderNumber;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getTxnAccNO() {
        return txnAccNO;
    }

    public void setTxnAccNO(String txnAccNO) {
        this.txnAccNO = txnAccNO;
    }

    public String getCarrierType() {
        return carrierType;
    }

    public void setCarrierType(String carrierType) {
        this.carrierType = carrierType;
    }

    public String getCarrierId1() {
        return carrierId1;
    }

    public void setCarrierId1(String carrierId1) {
        this.carrierId1 = carrierId1;
    }

    public String getStoreMemo() {
        return storeMemo;
    }

    public void setStoreMemo(String storeMemo) {
        this.storeMemo = storeMemo;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    @Override
    public String toString() {
        return "NewNotifyReq{" +
                "storeId='" + storeId + '\'' +
                ", endpointCode='" + endpointCode + '\'' +
                ", terminalId='" + terminalId + '\'' +
                ", txnSeqno='" + txnSeqno + '\'' +
                ", orderNumber='" + orderNumber + '\'' +
                ", txnType='" + txnType + '\'' +
                ", txnDateTime='" + txnDateTime + '\'' +
                ", txnAcctDate='" + txnAcctDate + '\'' +
                ", txnCurrency='" + txnCurrency + '\'' +
                ", txnAmt='" + txnAmt + '\'' +
                ", txnCharge='" + txnCharge + '\'' +
                ", orgOrderNumber='" + orgOrderNumber + '\'' +
                ", payType='" + payType + '\'' +
                ", txnAccNO='" + txnAccNO + '\'' +
                ", carrierType='" + carrierType + '\'' +
                ", carrierId1='" + carrierId1 + '\'' +
                ", storeMemo='" + storeMemo + '\'' +
                ", sign='" + sign + '\'' +
                '}';
    }
}
