package co.intella.domain.megatwpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class TxnPaymentReq{

  @SerializedName("Header")
  @JsonProperty("Header")
  private TxnPaymentHeaderReq txnPaymentHeaderReq;

  @SerializedName("Data")
  @JsonProperty("Data")
  private TxnPaymentDataReq txnPaymentDataReq;

  public TxnPaymentDataReq getTxnPaymentDataReq() {
    return txnPaymentDataReq;
  }

  public void setTxnPaymentDataReq(TxnPaymentDataReq txnPaymentDataReq) {
    this.txnPaymentDataReq = txnPaymentDataReq;
  }

  public TxnPaymentHeaderReq getTxnPaymentHeaderReq() {
    return txnPaymentHeaderReq;
  }

  public void setTxnPaymentHeaderReq(TxnPaymentHeaderReq txnPaymentHeaderReq) {
    this.txnPaymentHeaderReq = txnPaymentHeaderReq;
  }
}
