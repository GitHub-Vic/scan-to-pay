package co.intella.domain.megatwpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class TxnCancelDataRes extends ResponseData{


  @SerializedName("CancelAmt")
  @JsonProperty("CancelAmt")
  private String cancelAmt;

  @SerializedName("TxnTime")
  @JsonProperty("TxnTime")
  private String txnTime;

  @SerializedName("TxnResult")
  @JsonProperty("TxnResult")
  private String txnResult;

  public String getCancelAmt() {
    return cancelAmt;
  }

  public void setCancelAmt(String cancelAmt) {
    this.cancelAmt = cancelAmt;
  }

  public String getTxnTime() {
    return txnTime;
  }

  public void setTxnTime(String txnTime) {
    this.txnTime = txnTime;
  }

  public String getTxnResult() {
    return txnResult;
  }

  public void setTxnResult(String txnResult) {
    this.txnResult = txnResult;
  }
}
