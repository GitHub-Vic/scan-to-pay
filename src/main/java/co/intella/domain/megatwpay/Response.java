package co.intella.domain.megatwpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class Response {

  @SerializedName("Header")
  @JsonProperty("Header")
  private ResponseHeader responseHeader;

  @SerializedName("Data")
  @JsonProperty("Data")
  private ResponseData responseData;

  public ResponseHeader getResponseHeader() {
    return responseHeader;
  }

  public void setResponseHeader(ResponseHeader responseHeader) {
    this.responseHeader = responseHeader;
  }

  public ResponseData getResponseData() {
    return responseData;
  }

  public void setResponseData(ResponseData responseData) {
    this.responseData = responseData;
  }
}
