package co.intella.domain.megatwpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class TxnCancelRes {

  @SerializedName("Header")
  @JsonProperty("Header")
  private TxnCancelHeaderRes txnCancelHeaderRes;

  @SerializedName("Data")
  @JsonProperty("Data")
  private TxnCancelDataRes txnCancelDataRes;

  public TxnCancelHeaderRes getTxnCancelHeaderRes() {
    return txnCancelHeaderRes;
  }

  public void setTxnCancelHeaderRes(TxnCancelHeaderRes txnCancelHeaderRes) {
    this.txnCancelHeaderRes = txnCancelHeaderRes;
  }

  public TxnCancelDataRes getTxnCancelDataRes() {
    return txnCancelDataRes;
  }

  public void setTxnCancelDataRes(TxnCancelDataRes txnCancelDataRes) {
    this.txnCancelDataRes = txnCancelDataRes;
  }
}
