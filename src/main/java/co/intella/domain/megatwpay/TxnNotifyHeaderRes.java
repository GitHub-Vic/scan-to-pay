package co.intella.domain.megatwpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class TxnNotifyHeaderRes {

  @SerializedName("Scode")
  @JsonProperty("Scode")
  private String sCode;

  @SerializedName("Sdesc")
  @JsonProperty("Sdesc")
  private String sDesc;

  @SerializedName("Rtime")
  @JsonProperty("Rtime")
  private String rTime;

  public String getsCode() {
    return sCode;
  }

  public void setsCode(String sCode) {
    this.sCode = sCode;
  }

  public String getsDesc() {
    return sDesc;
  }

  public void setsDesc(String sDesc) {
    this.sDesc = sDesc;
  }

  public String getrTime() {
    return rTime;
  }

  public void setrTime(String rTime) {
    this.rTime = rTime;
  }
}
