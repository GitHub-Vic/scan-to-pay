package co.intella.domain.megatwpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class TxnPaymentRes {

  @SerializedName("Header")
  @JsonProperty("Header")
  private TxnPaymentHeaderRes txnPaymentHeaderRes;

  @SerializedName("Data")
  @JsonProperty("Data")
  private TxnPaymentDataRes txnPaymentDataRes;

  public TxnPaymentHeaderRes getTxnPaymentHeaderRes() {
    return txnPaymentHeaderRes;
  }

  public void setTxnPaymentHeaderRes(TxnPaymentHeaderRes txnPaymentHeaderRes) {
    this.txnPaymentHeaderRes = txnPaymentHeaderRes;
  }

  public TxnPaymentDataRes getTxnPaymentDataRes() {
    return txnPaymentDataRes;
  }

  public void setTxnPaymentDataRes(TxnPaymentDataRes txnPaymentDataRes) {
    this.txnPaymentDataRes = txnPaymentDataRes;
  }
}
