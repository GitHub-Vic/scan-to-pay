package co.intella.domain.megatwpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class TxnNotifyReq {

  @SerializedName("Header")
  @JsonProperty("Header")
  private TxnNotifyHeaderReq txnNotifyHeaderReq;

  @SerializedName("Data")
  @JsonProperty("Data")
  private TxnNotifyDataReq txnNotifyDataReq;

  public TxnNotifyHeaderReq getTxnNotifyHeaderReq() {
    return txnNotifyHeaderReq;
  }

  public void setTxnNotifyHeaderReq(TxnNotifyHeaderReq txnNotifyHeaderReq) {
    this.txnNotifyHeaderReq = txnNotifyHeaderReq;
  }

  public TxnNotifyDataReq getTxnNotifyDataReq() {
    return txnNotifyDataReq;
  }

  public void setTxnNotifyDataReq(TxnNotifyDataReq txnNotifyDataReq) {
    this.txnNotifyDataReq = txnNotifyDataReq;
  }
}
