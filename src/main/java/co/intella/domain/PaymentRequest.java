package co.intella.domain;

public class PaymentRequest {

	public PaymentRequestHeader header;
	public PaymentRequestData data;

	public PaymentRequest() {
		super();
		this.header = new PaymentRequestHeader();
		this.data = new PaymentRequestData();
	}

	public PaymentRequest(PaymentRequestHeader header, PaymentRequestData data) {
		super();
		this.header = header;
		this.data = data;
	}

	public PaymentRequestHeader getHeader() {
		return header;
	}

	public void setHeader(PaymentRequestHeader paymentHeader) {
		this.header = paymentHeader;
	}

	public PaymentRequestData getData() {
		return data;
	}

	public void setData(PaymentRequestData paymentData) {
		data = paymentData;
	}

}
