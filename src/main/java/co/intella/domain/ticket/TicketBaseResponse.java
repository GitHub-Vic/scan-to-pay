package co.intella.domain.ticket;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class TicketBaseResponse {

    @JsonProperty("NewAESKey")
    @SerializedName("NewAESKey")
    private String newAESKey;

    @JsonProperty("ChineseErrorMessage")
    @SerializedName("ChineseErrorMessage")
    private String chineseErrorMessage;

    @JsonProperty("ErrorMessage")
    @SerializedName("ErrorMessage")
    private String errorMessage;

    @JsonProperty("TXNResult")
    @SerializedName("TXNResult")
    private String txnResult;

    @JsonProperty("TerminalID")
    @SerializedName("TerminalID")
    private String terminalID;

    @JsonProperty("ErrorCode")
    @SerializedName("ErrorCode")
    private String errorCode;

    @JsonProperty("CardID")
    @SerializedName("CardID")
    private String cardID;

    @JsonProperty("DeviceID")
    @SerializedName("DeviceID")
    private String deviceID;

    @JsonProperty("Time")
    @SerializedName("Time")
    private String time;

    @JsonProperty("Date")
    @SerializedName("Date")
    private String date;

    public String getNewAESKey() {
        return newAESKey;
    }

    public void setNewAESKey(String newAESKey) {
        this.newAESKey = newAESKey;
    }

    public String getChineseErrorMessage() {
        return chineseErrorMessage;
    }

    public void setChineseErrorMessage(String chineseErrorMessage) {
        this.chineseErrorMessage = chineseErrorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getTxnResult() {
        return txnResult;
    }

    public void setTxnResult(String txnResult) {
        this.txnResult = txnResult;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
