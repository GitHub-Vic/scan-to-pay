package co.intella.domain.ticket;

import co.intella.model.Device;
import co.intella.model.PaymentAccount;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TicketBaseRequest {


    public TicketBaseRequest(String testMode, Device device, PaymentAccount paymentAccount) {
        LocalDateTime now = LocalDateTime.now();
        String date = now.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        String time = now.format(DateTimeFormatter.ofPattern("HHmmss"));
        this.setTestMode(testMode);
        this.setTerminalID(paymentAccount.getAccount());
        this.setDeviceID(device.getOwnerDeviceId());
        this.setTime(time);
        this.setDate(date);
        this.setUartMode("0");
        this.setAesKey(device.getAesKey());
        this.setServiceTxType("");
        this.setServiceType("");
        this.setTicketType("");

        //尋卡順序   0：悠遊卡   1：一卡通    2：HappyCash    3：愛金卡
        this.setTriggerCardOrder("0312");

        //1：悠遊卡、2：一卡通、4：HappyCash、8：愛金卡、15：4種卡片都可尋卡。(請以10進制方式相加，例只開啟 愛金卡及一卡通，8+2 = 10)
        this.setCheckCardFlag("15");

        this.setEzcData("[{}]");
        this.setIpassData("[{}]");
        this.setIcashData("[{}]");
        this.setHappyCashData("[{}]");
    }

    @JsonProperty("TestMode")
    @SerializedName("TestMode")
    private String testMode;

    @JsonProperty("UARTMode")
    @SerializedName("UARTMode")
    private String uartMode;

    @JsonProperty("TicketServiceType")
    @SerializedName("TicketServiceType")
    private String ticketServiceType;

    @JsonProperty("OnlyFindCard")
    @SerializedName("OnlyFindCard")
    private String onlyFindCard;

    @JsonProperty("TerminalID")
    @SerializedName("TerminalID")
    private String terminalID;

    @JsonProperty("DeviceID")
    @SerializedName("DeviceID")
    private String deviceID;

    @JsonProperty("Time")
    @SerializedName("Time")
    private String time;

    @JsonProperty("Date")
    @SerializedName("Date")
    private String date;

    @JsonProperty("AESKey")
    @SerializedName("AESKey")
    private String aesKey;

    @JsonProperty("ServiceType")
    @SerializedName("ServiceType")
    private String serviceType;

    @JsonProperty("TicketType")
    @SerializedName("TicketType")
    private String ticketType;

    @JsonProperty("ServiceTxType")
    @SerializedName("ServiceTxType")
    private String serviceTxType;

    @JsonProperty("TriggerCardOrder")
    @SerializedName("TriggerCardOrder")
    private String triggerCardOrder;

    @JsonProperty("CheckCardFlag")
    @SerializedName("CheckCardFlag")
    private String checkCardFlag;

    @JsonProperty("OnlyTransaction")
    @SerializedName("OnlyTransaction")
    private String onlyTransaction;

    @JsonProperty("EZCData")
    @SerializedName("EZCData")
    private String ezcData;

    @JsonProperty("IcashData")
    @SerializedName("IcashData")
    private String icashData;

    @JsonProperty("IpassData")
    @SerializedName("IpassData")
    private String ipassData;

    @JsonProperty("HappyCashData")
    @SerializedName("HappyCashData")
    private String happyCashData;

    public String getTestMode() {
        return testMode;
    }

    public void setTestMode(String testMode) {
        this.testMode = testMode;
    }

    public String getUartMode() {
        return uartMode;
    }

    public void setUartMode(String uartMode) {
        this.uartMode = uartMode;
    }

    public String getTicketServiceType() {
        return ticketServiceType;
    }

    public void setTicketServiceType(String ticketServiceType) {
        this.ticketServiceType = ticketServiceType;
    }

    public String getOnlyFindCard() {
        return onlyFindCard;
    }

    public void setOnlyFindCard(String onlyFindCard) {
        this.onlyFindCard = onlyFindCard;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAesKey() {
        return aesKey;
    }

    public void setAesKey(String aesKey) {
        this.aesKey = aesKey;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public String getServiceTxType() {
        return serviceTxType;
    }

    public void setServiceTxType(String serviceTxType) {
        this.serviceTxType = serviceTxType;
    }

    public String getTriggerCardOrder() {
        return triggerCardOrder;
    }

    public void setTriggerCardOrder(String triggerCardOrder) {
        this.triggerCardOrder = triggerCardOrder;
    }

    public String getCheckCardFlag() {
        return checkCardFlag;
    }

    public void setCheckCardFlag(String checkCardFlag) {
        this.checkCardFlag = checkCardFlag;
    }

    public String getOnlyTransaction() {
        return onlyTransaction;
    }

    public void setOnlyTransaction(String onlyTransaction) {
        this.onlyTransaction = onlyTransaction;
    }

    public String getEzcData() {
        return ezcData;
    }

    public void setEzcData(String ezcData) {
        this.ezcData = ezcData;
    }

    public String getIcashData() {
        return icashData;
    }

    public void setIcashData(String icashData) {
        this.icashData = icashData;
    }

    public String getIpassData() {
        return ipassData;
    }

    public void setIpassData(String ipassData) {
        this.ipassData = ipassData;
    }

    public String getHappyCashData() {
        return happyCashData;
    }

    public void setHappyCashData(String happyCashData) {
        this.happyCashData = happyCashData;
    }
}
