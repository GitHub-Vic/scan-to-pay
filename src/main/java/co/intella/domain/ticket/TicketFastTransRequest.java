package co.intella.domain.ticket;

import co.intella.model.Device;
import co.intella.model.PaymentAccount;

import java.util.List;

public class TicketFastTransRequest extends TicketBaseRequest {

    public TicketFastTransRequest(String testMode, Device device, PaymentAccount paymentAccount, List<PaymentAccount> paymentAccountList) {
        super(testMode, device, paymentAccount);
        this.setOnlyFindCard("0");
        this.setTicketServiceType("FastTrans");
        this.setOnlyTransaction("1");

        //1：悠遊卡、2：一卡通、4：HappyCash、8：愛金卡、15：4種卡片都可尋卡。(請以10進制方式相加，例只開啟 愛金卡及一卡通，8+2 = 10)
        this.setCheckCardFlag(String.valueOf(paymentAccountList.stream().mapToLong(pa -> {
            long a = 0;
            switch (String.valueOf(pa.getBank().getBankId())) {
                case "18":
                    a = 1;
                    break;
                case "21":
                    a = 2;
                    break;
                case "28":
                    a = 8;
                    break;
                case "37":
                    a = 4;
                    break;
            }
            return a;
        }).sum()));
    }
}
