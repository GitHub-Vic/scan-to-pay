package co.intella.domain.ticket;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TicketFastTransResponse extends TicketBaseResponse {

    @JsonProperty("FastTransData")
    @SerializedName("FastTransData")
    private List<JsonObject>  fastTransDataList;

    @JsonProperty("TicketData")
    @SerializedName("TicketData")
    private JsonObject ticketData;

    public List<JsonObject> getFastTransDataList() {
        return fastTransDataList;
    }

    public void setFastTransDataList(List<JsonObject> fastTransDataList) {
        this.fastTransDataList = fastTransDataList;
    }

    public JsonObject getTicketData() {
        return ticketData;
    }

    public void setTicketData(JsonObject ticketData) {
        this.ticketData = ticketData;
    }
}
