package co.intella.domain.ticket;

import co.intella.model.Device;
import co.intella.model.PaymentAccount;

public class TicketFindCardRequest extends TicketBaseRequest {

    public TicketFindCardRequest(String testMode, Device device, PaymentAccount paymentAccount) {
        super(testMode, device, paymentAccount);
        this.setOnlyFindCard("1");
        this.setTicketServiceType("FindCard");
        this.setOnlyTransaction("0");
    }
}
