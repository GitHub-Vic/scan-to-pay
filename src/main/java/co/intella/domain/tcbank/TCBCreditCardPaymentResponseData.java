package co.intella.domain.tcbank;

import co.intella.domain.integration.ResponseGeneralPlatform;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class TCBCreditCardPaymentResponseData extends ResponseGeneralPlatform{
    @SerializedName("StoreOrderNo")
    @JsonProperty(value = "OrderId")
    private String storeOrderNo;

    @SerializedName("DeviceInfo")
    @JsonProperty(value = "TerminalId")
    private String deviceInfo;

    @SerializedName("TotalFee")
    @JsonProperty(value = "TransAmt")
    private String totalFee;

    @SerializedName("Extended")
    @JsonProperty(value = "PrivateData")
    private String extended;

    @SerializedName("CardId")
    @JsonProperty(value = "PAN")
    private String cardId;

    @SerializedName("ExtenNo")
    @JsonProperty(value = "CVV2/4DBC/CVV2")
    private String extenNo;

    @SerializedName("ExpireDate")
    @JsonProperty(value = "ExpireDate")
    private String expireDate;

    @SerializedName("TransMode")
    @JsonProperty(value = "TransMode")
    private String transMode;

    @SerializedName("NotifyURL")
    @JsonProperty(value = "NotifyURL")
    private String notifyURL;

    @SerializedName("Installment")
    @JsonProperty(value = "Installment")
    private String installment;

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getExtended() {
        return extended;
    }

    public void setExtended(String extended) {
        this.extended = extended;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getExtenNo() {
        return extenNo;
    }

    public void setExtenNo(String extenNo) {
        this.extenNo = extenNo;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getTransMode() {
        return transMode;
    }

    public void setTransMode(String transMode) {
        this.transMode = transMode;
    }

    public String getNotifyURL() {
        return notifyURL;
    }

    public void setNotifyURL(String notifyURL) {
        this.notifyURL = notifyURL;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(String installment) {
        this.installment = installment;
    }
}
