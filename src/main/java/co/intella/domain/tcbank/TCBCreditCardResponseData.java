package co.intella.domain.tcbank;

/**
 * @author Miles
 */
public class TCBCreditCardResponseData {

    private String merchantId;

    private String terminalId;

    private String orderId;

    private String transMode;

    private String transDate;

    private String transTime;

    private String transAmt;

    private String approveCode;

    private String installmentType;

    private String redeemType;

    private String installment;

    private String fee;

    private String firstAmt;

    private String eachAmt;

    private String redeemUsed;

    private String redeemBalance;

    private String creaditAmt;

    private String responseCode;

    private String responseMessage;

    private String privateData;

    private String riskMark;

    private String foreign;

    private String secureStatus;

    private String html;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTransMode() {
        return transMode;
    }

    public void setTransMode(String transMode) {
        this.transMode = transMode;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getTransTime() {
        return transTime;
    }

    public void setTransTime(String transTime) {
        this.transTime = transTime;
    }

    public String getTransAmt() {
        return transAmt;
    }

    public void setTransAmt(String transAmt) {
        this.transAmt = transAmt;
    }

    public String getApproveCode() {
        return approveCode;
    }

    public void setApproveCode(String approveCode) {
        this.approveCode = approveCode;
    }

    public String getInstallmentType() {
        return installmentType;
    }

    public void setInstallmentType(String installmentType) {
        this.installmentType = installmentType;
    }

    public String getRedeemType() {
        return redeemType;
    }

    public void setRedeemType(String redeemType) {
        this.redeemType = redeemType;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(String installment) {
        this.installment = installment;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getFirstAmt() {
        return firstAmt;
    }

    public void setFirstAmt(String firstAmt) {
        this.firstAmt = firstAmt;
    }

    public String getEachAmt() {
        return eachAmt;
    }

    public void setEachAmt(String eachAmt) {
        this.eachAmt = eachAmt;
    }

    public String getRedeemUsed() {
        return redeemUsed;
    }

    public void setRedeemUsed(String redeemUsed) {
        this.redeemUsed = redeemUsed;
    }

    public String getRedeemBalance() {
        return redeemBalance;
    }

    public void setRedeemBalance(String redeemBalance) {
        this.redeemBalance = redeemBalance;
    }

    public String getCreaditAmt() {
        return creaditAmt;
    }

    public void setCreaditAmt(String creaditAmt) {
        this.creaditAmt = creaditAmt;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getPrivateData() {
        return privateData;
    }

    public void setPrivateData(String privateData) {
        this.privateData = privateData;
    }

    public String getRiskMark() {
        return riskMark;
    }

    public void setRiskMark(String riskMark) {
        this.riskMark = riskMark;
    }

    public String getForeign() {
        return foreign;
    }

    public void setForeign(String foreign) {
        this.foreign = foreign;
    }

    public String getSecureStatus() {
        return secureStatus;
    }

    public void setSecureStatus(String secureStatus) {
        this.secureStatus = secureStatus;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }
}
