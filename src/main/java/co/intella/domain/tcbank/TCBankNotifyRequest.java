package co.intella.domain.tcbank;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Miles
 */
public class TCBankNotifyRequest {

    @JsonProperty(value = "MerchantID", required = true)
    private String merchantId;

    @JsonProperty(value = "TerminalID", required = true)
    private String terminalId;

    @JsonProperty(value = "OrderID")
    private String orderId;

    @JsonProperty(value = "PAN")
    private String pan;

    @JsonProperty(value = "TransCode")
    private String transCode;

    @JsonProperty(value = "TransDate")
    private String transDate;

    @JsonProperty(value = "TransTime")
    private String transTime;

    @JsonProperty(value = "TransAmt")
    private String transAmt;

    @JsonProperty(value = "ApproveCode")
    private String approveCode;

    @JsonProperty(value = "ResponseCode")
    private String responseCode;

    @JsonProperty(value = "ResponseMsg")
    private String responseMsg;

    @JsonProperty(value = "InstallType")
    private String installType;

    @JsonProperty(value = "Install")
    private String install;

    @JsonProperty(value = "FirstAmt")
    private String firstAmt;

    @JsonProperty(value = "EachAmt")
    private String eachAmt;

    @JsonProperty(value = "Fee")
    private String fee;

    @JsonProperty(value = "RedeemType")
    private String redeemType;

    @JsonProperty(value = "RedeemUsed")
    private String redeemUsed;

    @JsonProperty(value = "RedeemBalance")
    private String redeemBalance;

    @JsonProperty(value = "CreditAmt")
    private String creditAmt;

    @JsonProperty(value = "RiskMark")
    private String riskMark;

    @JsonProperty(value = "FOREIGN")
    private String foreign;

    @JsonProperty(value = "SECURE_STATUS")
    private String secureStatue;


    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getTransCode() {
        return transCode;
    }

    public void setTransCode(String transCode) {
        this.transCode = transCode;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getTransTime() {
        return transTime;
    }

    public void setTransTime(String transTime) {
        this.transTime = transTime;
    }

    public String getTransAmt() {
        return transAmt;
    }

    public void setTransAmt(String transAmt) {
        this.transAmt = transAmt;
    }

    public String getApproveCode() {
        return approveCode;
    }

    public void setApproveCode(String approveCode) {
        this.approveCode = approveCode;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public String getInstallType() {
        return installType;
    }

    public void setInstallType(String installType) {
        this.installType = installType;
    }

    public String getInstall() {
        return install;
    }

    public void setInstall(String install) {
        this.install = install;
    }

    public String getFirstAmt() {
        return firstAmt;
    }

    public void setFirstAmt(String firstAmt) {
        this.firstAmt = firstAmt;
    }

    public String getEachAmt() {
        return eachAmt;
    }

    public void setEachAmt(String eachAmt) {
        this.eachAmt = eachAmt;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getRedeemType() {
        return redeemType;
    }

    public void setRedeemType(String redeemType) {
        this.redeemType = redeemType;
    }

    public String getRedeemUsed() {
        return redeemUsed;
    }

    public void setRedeemUsed(String redeemUsed) {
        this.redeemUsed = redeemUsed;
    }

    public String getRedeemBalance() {
        return redeemBalance;
    }

    public void setRedeemBalance(String redeemBalance) {
        this.redeemBalance = redeemBalance;
    }

    public String getCreditAmt() {
        return creditAmt;
    }

    public void setCreditAmt(String creditAmt) {
        this.creditAmt = creditAmt;
    }

    public String getRiskMark() {
        return riskMark;
    }

    public void setRiskMark(String riskMark) {
        this.riskMark = riskMark;
    }

    public String getForeign() {
        return foreign;
    }

    public void setForeign(String foreign) {
        this.foreign = foreign;
    }

    public String getSecureStatue() {
        return secureStatue;
    }

    public void setSecureStatue(String secureStatue) {
        this.secureStatue = secureStatue;
    }
}
