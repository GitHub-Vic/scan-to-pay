package co.intella.domain.tcbank;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class TCBCreditCardRequestData {

    private String merchantId;

    @SerializedName("DeviceInfo")
    private String terminalId;

    @SerializedName("StoreOrderNo")
    private String orderId;

    private String transCode;

    @SerializedName("CardId")
    private String pan;

    @SerializedName("ExpireDate")
    private String expireData;

    @SerializedName("TransMode")
    private String transMode;

    @SerializedName("Installment")
    private String install;

    @SerializedName("TotalFee")
    private String transAmt;

    private String extenNo;

    @SerializedName("Extended")
    private String privateData;

    @SerializedName("NotifyURL")
    private String notifyUrl;

    @SerializedName("Token")
    private String token;

    @SerializedName("TokenSeq")
    private Long tokenSeq;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTransCode() {
        return transCode;
    }

    public void setTransCode(String transCode) {
        this.transCode = transCode;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getExpireData() {
        return expireData;
    }

    public void setExpireData(String expireData) {
        this.expireData = expireData;
    }

    public String getTransMode() {
        return transMode;
    }

    public void setTransMode(String transMode) {
        this.transMode = transMode;
    }

    public String getInstall() {
        return install;
    }

    public void setInstall(String install) {
        this.install = install;
    }

    public String getTransAmt() {
        return transAmt;
    }

    public void setTransAmt(String transAmt) {
        this.transAmt = transAmt;
    }

    public String getExtenNo() {
        return extenNo;
    }

    public void setExtenNo(String extenNo) {
        this.extenNo = extenNo;
    }

    public String getPrivateData() {
        return privateData;
    }

    public void setPrivateData(String privateData) {
        this.privateData = privateData;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getTokenSeq() {
        return tokenSeq;
    }

    public void setTokenSeq(Long tokenSeq) {
        this.tokenSeq = tokenSeq;
    }
}
