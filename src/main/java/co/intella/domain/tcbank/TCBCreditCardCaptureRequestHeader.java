package co.intella.domain.tcbank;

import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class TCBCreditCardCaptureRequestHeader {

    @SerializedName("header")
    private String header;      // "H"

    @SerializedName("MchId")
    private String mchId;

    @SerializedName("CaptureTime")
    private String captureTime;  // yyyyMMdd

    @SerializedName("SequentialNo")
    private String sequentialNo;

    @SerializedName("TotalCount")
    private String totalCount;    // trade number

    @SerializedName("NumberSign")
    private String numberSign;  // + | -

    @SerializedName("TotalFee")
    private String totalFee;

    private String createTime;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getCaptureTime() {
        return captureTime;
    }

    public void setCaptureTime(String captureTime) {
        this.captureTime = captureTime;
    }

    public String getSequentialNo() {
        return sequentialNo;
    }

    public void setSequentialNo(String sequentialNo) {
        this.sequentialNo = sequentialNo;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getNumberSign() {
        return numberSign;
    }

    public void setNumberSign(String numberSign) {
        this.numberSign = numberSign;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
