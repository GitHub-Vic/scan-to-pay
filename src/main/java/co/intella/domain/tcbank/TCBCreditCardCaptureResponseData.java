package co.intella.domain.tcbank;

import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class TCBCreditCardCaptureResponseData {

    @SerializedName("MchId")
    private String mchId;

    @SerializedName("DeviceInfo")
    private String deviceInfo;

    @SerializedName("StoreOrderNo")
    private String storeOrderNo;

    @SerializedName("TotalFee")
    private String totalFee;

    @SerializedName("AuthCode")
    private String authCode;

    @SerializedName("CloseType")
    private String closeType;

    @SerializedName("EstablishedAt")
    private String establishedAt;  // 8

    @SerializedName("Extended")
    private String extended;

    @SerializedName("CardOwnerInfo")
    private String cardOwnerInfo;

    private String acceptedAt;   // 6

    private String statusCode;

    private String statusMsg;

    private String batchAndSeq;

    private String transMode;

    private int installment;

    private String instFirst;

    private String instEach;

    private String instFee;

    private String redeemUsed;

    private String balanceSign;

    private String redeemBalance;

    private String creditAmt;

    private String paidAt; // 8

    private String secureStatus;

    private String foreign;

    private String reserved;

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getCloseType() {
        return closeType;
    }

    public void setCloseType(String closeType) {
        this.closeType = closeType;
    }

    public String getEstablishedAt() {
        return establishedAt;
    }

    public void setEstablishedAt(String establishedAt) {
        this.establishedAt = establishedAt;
    }

    public String getExtended() {
        return extended;
    }

    public void setExtended(String extended) {
        this.extended = extended;
    }

    public String getCardOwnerInfo() {
        return cardOwnerInfo;
    }

    public void setCardOwnerInfo(String cardOwnerInfo) {
        this.cardOwnerInfo = cardOwnerInfo;
    }

    public String getAcceptedAt() {
        return acceptedAt;
    }

    public void setAcceptedAt(String acceptedAt) {
        this.acceptedAt = acceptedAt;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public String getBatchAndSeq() {
        return batchAndSeq;
    }

    public void setBatchAndSeq(String batchAndSeq) {
        this.batchAndSeq = batchAndSeq;
    }

    public String getTransMode() {
        return transMode;
    }

    public void setTransMode(String transMode) {
        this.transMode = transMode;
    }

    public int getInstallment() {
        return installment;
    }

    public void setInstallment(int installment) {
        this.installment = installment;
    }

    public String getInstFirst() {
        return instFirst;
    }

    public void setInstFirst(String instFirst) {
        this.instFirst = instFirst;
    }

    public String getInstEach() {
        return instEach;
    }

    public void setInstEach(String instEach) {
        this.instEach = instEach;
    }

    public String getInstFee() {
        return instFee;
    }

    public void setInstFee(String instFee) {
        this.instFee = instFee;
    }

    public String getRedeemUsed() {
        return redeemUsed;
    }

    public void setRedeemUsed(String redeemUsed) {
        this.redeemUsed = redeemUsed;
    }

    public String getBalanceSign() {
        return balanceSign;
    }

    public void setBalanceSign(String balanceSign) {
        this.balanceSign = balanceSign;
    }

    public String getRedeemBalance() {
        return redeemBalance;
    }

    public void setRedeemBalance(String redeemBalance) {
        this.redeemBalance = redeemBalance;
    }

    public String getCreditAmt() {
        return creditAmt;
    }

    public void setCreditAmt(String creditAmt) {
        this.creditAmt = creditAmt;
    }

    public String getPaidAt() {
        return paidAt;
    }

    public void setPaidAt(String paidAt) {
        this.paidAt = paidAt;
    }

    public String getSecureStatus() {
        return secureStatus;
    }

    public void setSecureStatus(String secureStatus) {
        this.secureStatus = secureStatus;
    }

    public String getForeign() {
        return foreign;
    }

    public void setForeign(String foreign) {
        this.foreign = foreign;
    }

    public String getReserved() {
        return reserved;
    }

    public void setReserved(String reserved) {
        this.reserved = reserved;
    }
}
