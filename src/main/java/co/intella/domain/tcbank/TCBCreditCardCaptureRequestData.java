package co.intella.domain.tcbank;

import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class TCBCreditCardCaptureRequestData {

    private String mchId;

    private String deviceInfo;

    private String StoreOrderNo;

    private long totalFee;

    private String authCode;

    private String closeType;

    private int tradeDate;  // 8

    private String extended;

    private String cardOwnerInfo;

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getStoreOrderNo() {
        return StoreOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        StoreOrderNo = storeOrderNo;
    }

    public long getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(long totalFee) {
        this.totalFee = totalFee;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getCloseType() {
        return closeType;
    }

    public void setCloseType(String closeType) {
        this.closeType = closeType;
    }

    public int getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(int tradeDate) {
        this.tradeDate = tradeDate;
    }

    public String getExtended() {
        return extended;
    }

    public void setExtended(String extended) {
        this.extended = extended;
    }

    public String getCardOwnerInfo() {
        return cardOwnerInfo;
    }

    public void setCardOwnerInfo(String cardOwnerInfo) {
        this.cardOwnerInfo = cardOwnerInfo;
    }
}
