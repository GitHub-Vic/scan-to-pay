package co.intella.domain.gama;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class GamaRefundResponseData {

    @JsonProperty("MerchantID")
    @SerializedName("MerchantID")
    private String merchantId;

    @JsonProperty("SubMerchantID")
    @SerializedName("SubMerchantID")
    private String subMerchantId;

    @JsonProperty("MerchantOrderID")
    @SerializedName("MerchantOrderID")
    private String merchantOrderId;


    @JsonProperty("ResultCode")
    @SerializedName("ResultCode")
    private String resultCode;

    @JsonProperty("ResultMessage")
    @SerializedName("ResultMessage")
    private String resultMessage;

    @JsonProperty("TransAmount")
    @SerializedName("TransAmount")
    private Integer transAmount;

    @JsonProperty("CurrencyCode")
    @SerializedName("CurrencyCode")
    private String currencyCode;

    @JsonProperty("MAC")
    @SerializedName("MAC")
    private String mac;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getSubMerchantId() {
        return subMerchantId;
    }

    public void setSubMerchantId(String subMerchantId) {
        this.subMerchantId = subMerchantId;
    }

    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    public void setMerchantOrderId(String merchantOrderId) {
        this.merchantOrderId = merchantOrderId;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public Integer getTransAmount() {
        return transAmount;
    }

    public void setTransAmount(Integer transAmount) {
        this.transAmount = transAmount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
}
