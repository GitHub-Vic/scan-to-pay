package co.intella.domain.gama;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GamaCreateOrderRequestData extends GamaBasic {

    @JsonProperty("MerchantID")
    @SerializedName("MerchantID")
    private String merchantId;

    @JsonProperty("SubMerchantID")
    @SerializedName("SubMerchantID")
    private String subMerchantId;

    @JsonProperty("MerchantOrderID")
    @SerializedName("MerchantOrderID")
    private String merchantOrderId;

    @JsonProperty("TransType")
    @SerializedName("TransType")
    private Integer transType;

    @JsonProperty("TradingContent")
    @SerializedName("TradingContent")
    private String tradingContent;

    @JsonProperty("TradingToken")
    @SerializedName("TradingToken")
    private String tradingToken;

    @JsonProperty("Memo")
    @SerializedName("Memo")
    private String memo;

    @JsonProperty("TransactionID")
    @SerializedName("TransactionID")
    private String transactionID;

    @JsonProperty("TransAmount")
    @SerializedName("TransAmount")
    private Integer transAmount;

    @JsonProperty("ExcludeAmount")
    @SerializedName("ExcludeAmount")
    private Integer excludeAmount;

    @JsonProperty("ExcludeItem")
    @SerializedName("ExcludeItem")
    private String excludeItem;

    @JsonProperty("CurrencyCode")
    @SerializedName("CurrencyCode")
    private String currencyCode;

    @JsonProperty("BusinessDate")
    @SerializedName("BusinessDate")
    private String businessDate;

    @JsonProperty("Remark1")
    @SerializedName("Remark1")
    private String remark1;

    @JsonProperty("Remark2")
    @SerializedName("Remark2")
    private String remark2;

    @JsonProperty("InboundAuthToken")
    @SerializedName("InboundAuthToken")
    private String inboundAuthToken;

    @JsonProperty("ExpiredDateTime")
    @SerializedName("ExpiredDateTime")
    private String expiredDateTime;

    @JsonProperty("MAC")
    @SerializedName("MAC")
    private String mac;

    @JsonProperty("PostBackUrl")
    @SerializedName("PostBackUrl")
    private String postBackUrl;
    
    @JsonProperty("PayWay")
    @SerializedName("PayWay")
    private int payWay;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getSubMerchantId() {
        return subMerchantId;
    }

    public void setSubMerchantId(String subMerchantId) {
        this.subMerchantId = subMerchantId;
    }

    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    public void setMerchantOrderId(String merchantOrderId) {
        this.merchantOrderId = merchantOrderId;
    }

    public Integer getTransType() {
        return transType;
    }

    public void setTransType(Integer transType) {
        this.transType = transType;
    }

    public String getTradingContent() {
        return tradingContent;
    }

    public void setTradingContent(String tradingContent) {
        this.tradingContent = tradingContent;
    }

    public String getTradingToken() {
        return tradingToken;
    }

    public void setTradingToken(String tradingToken) {
        this.tradingToken = tradingToken;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Integer getTransAmount() {
        return transAmount;
    }

    public void setTransAmount(Integer transAmount) {
        this.transAmount = transAmount;
    }

    public Integer getExcludeAmount() {
        return excludeAmount;
    }

    public void setExcludeAmount(Integer excludeAmount) {
        this.excludeAmount = excludeAmount;
    }

    public String getExcludeItem() {
        return excludeItem;
    }

    public void setExcludeItem(String excludeItem) {
        this.excludeItem = excludeItem;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getBusinessDate() {
        return businessDate;
    }

    public void setBusinessDate(String businessDate) {
        this.businessDate = businessDate;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    public String getInboundAuthToken() {
        return inboundAuthToken;
    }

    public void setInboundAuthToken(String inboundAuthToken) {
        this.inboundAuthToken = inboundAuthToken;
    }

    public String getExpiredDateTime() {
        return expiredDateTime;
    }

    public void setExpiredDateTime(String expiredDateTime) {
        this.expiredDateTime = expiredDateTime;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

	public String getPostBackUrl() {
		return postBackUrl;
	}

	public void setPostBackUrl(String postBackUrl) {
		this.postBackUrl = postBackUrl;
	}

	public int getPayWay() {
		return payWay;
	}

	public void setPayWay(int payWay) {
		this.payWay = payWay;
	}

}
