package co.intella.domain.gama;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class GamaResponseData {

    @JsonProperty("TransactionToken")
    @SerializedName("TransactionToken")
    private String transactionToken;

    @JsonProperty("TransactionID")
    @SerializedName("TransactionID")
    private String transactionId;

    @JsonProperty("MerchantID")
    @SerializedName("MerchantID")
    private String merchantId;

    @JsonProperty("SubMerchantID")
    @SerializedName("SubMerchantID")
    private String subMerchantId;

    @JsonProperty("MerchantOrderID")
    @SerializedName("MerchantOrderID")
    private String merchantOrderId;

    @JsonProperty("TransType")
    @SerializedName("TransType")
    private Integer transType;

    @JsonProperty("QRCodeData")
    @SerializedName("QRCodeData")
    private String qrCodeData;


    @JsonProperty("ResultCode")
    @SerializedName("ResultCode")
    private String resultCode;

    @JsonProperty("ResultMessage")
    @SerializedName("ResultMessage")
    private String resultMessage;

    @JsonProperty("TransAmount")
    @SerializedName("TransAmount")
    private Double transAmount;

    @JsonProperty("CurrencyCode")
    @SerializedName("CurrencyCode")
    private String currencyCode;

    @JsonProperty("ExpiredDateTime")
    @SerializedName("ExpiredDateTime")
    private String expiredDateTime;

    @JsonProperty("MAC")
    @SerializedName("MAC")
    private String mac;

    //if value

    @JsonProperty("PayerId")
    @SerializedName("PayerId")
    private String payerId;

    @JsonProperty("TransFinallyAmount")
    @SerializedName("TransFinallyAmount")
    private Double transFinallyAmount;

    @JsonProperty("TransFee")
    @SerializedName("TransFee")
    private Integer transFee;


    @JsonProperty("PerformanceBondDateTime")
    @SerializedName("PerformanceBondDateTime")
    private String performanceBondDateTime;

    @JsonProperty("CreatedDate")
    @SerializedName("CreatedDate")
    private String createdDate;

    @JsonProperty("TransDate")
    @SerializedName("TransDate")
    private String transDate;

    @JsonProperty("StatusCode")
    @SerializedName("StatusCode")
    private String statusCode;

    @JsonProperty("Relation_ID_Trans")
    @SerializedName("Relation_ID_Trans")
    private String relationIdTrans;

    @JsonProperty("RelationID")
    @SerializedName("RelationID")
    private String relationID;

    @JsonProperty("LoveCode")
    @SerializedName("LoveCode")
    private String loveCode;

    @JsonProperty("CarrierID")
    @SerializedName("CarrierID")
    private String carrierID;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getSubMerchantId() {
        return subMerchantId;
    }

    public void setSubMerchantId(String subMerchantId) {
        this.subMerchantId = subMerchantId;
    }

    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    public void setMerchantOrderId(String merchantOrderId) {
        this.merchantOrderId = merchantOrderId;
    }

    public Integer getTransType() {
        return transType;
    }

    public void setTransType(Integer transType) {
        this.transType = transType;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public Double getTransAmount() {
        return transAmount;
    }

    public void setTransAmount(Double transAmount) {
        this.transAmount = transAmount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getExpiredDateTime() {
        return expiredDateTime;
    }

    public void setExpiredDateTime(String expiredDateTime) {
        this.expiredDateTime = expiredDateTime;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getPayerId() {
        return payerId;
    }

    public void setPayerId(String payerId) {
        this.payerId = payerId;
    }

    public Double getTransFinallyAmount() {
        return transFinallyAmount;
    }

    public void setTransFinallyAmount(Double transFinallyAmount) {
        this.transFinallyAmount = transFinallyAmount;
    }

    public Integer getTransFee() {
        return transFee;
    }

    public void setTransFee(Integer transFee) {
        this.transFee = transFee;
    }

    public String getPerformanceBondDateTime() {
        return performanceBondDateTime;
    }

    public void setPerformanceBondDateTime(String performanceBondDateTime) {
        this.performanceBondDateTime = performanceBondDateTime;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getRelationIdTrans() {
        return relationIdTrans;
    }

    public void setRelationIdTrans(String relationIdTrans) {
        this.relationIdTrans = relationIdTrans;
    }

    public String getLoveCode() {
        return loveCode;
    }

    public void setLoveCode(String loveCode) {
        this.loveCode = loveCode;
    }

    public String getCarrierID() {
        return carrierID;
    }

    public void setCarrierID(String carrierID) {
        this.carrierID = carrierID;
    }

    public String getQrCodeData() {
        return qrCodeData;
    }

    public void setQrCodeData(String qrCodeData) {
        this.qrCodeData = qrCodeData;
    }

    public String getTransactionToken() {
        return transactionToken;
    }

    public void setTransactionToken(String transactionToken) {
        this.transactionToken = transactionToken;
    }

    public String getRelationID() {
        return relationID;
    }

    public void setRelationID(String relationID) {
        this.relationID = relationID;
    }
}
