package co.intella.domain.gama;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class GamaQrcodeData {

    @JsonProperty("Type")
    @SerializedName("Type")
    private Integer type;

    @JsonProperty("ReceiverName")
    @SerializedName("ReceiverName")
    private String receiverName;

    @JsonProperty("ReceiverLoginID")
    @SerializedName("ReceiverLoginID")
    private String receiverLoginID;

    @JsonProperty("TransactionToken")
    @SerializedName("TransactionToken")
    private String transactionToken;

    @JsonProperty("Source")
    @SerializedName("Source")
    private String source;

    @JsonProperty("TransType")
    @SerializedName("TransType")
    private String TransType;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverLoginID() {
        return receiverLoginID;
    }

    public void setReceiverLoginID(String receiverLoginID) {
        this.receiverLoginID = receiverLoginID;
    }

    public String getTransactionToken() {
        return transactionToken;
    }

    public void setTransactionToken(String transactionToken) {
        this.transactionToken = transactionToken;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTransType() {
        return TransType;
    }

    public void setTransType(String transType) {
        TransType = transType;
    }
}
