package co.intella.domain.gama;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GamaInAppData {
    @JsonProperty("ReturnUrl")
    @SerializedName("ReturnUrl")
    private String returnUrl;

    @JsonProperty("Type")
    @SerializedName("Type")
    private String type="3";

    @JsonProperty("ReceiverName")
    @SerializedName("ReceiverName")
    private String receiverName="intella";

    @JsonProperty("ReceiverLoginID")
    @SerializedName("ReceiverLoginID")
    private String receiverLoginID;

    @JsonProperty("TransactionToken")
    @SerializedName("TransactionToken")
    private String transactionToken;

    @JsonProperty("Source")
    @SerializedName("Source")
    private String source="Vender";

    @JsonProperty("TransType")
    @SerializedName("TransType")
    private String transType="10";

    @JsonProperty("ScenesType")
    @SerializedName("ScenesType")
    private String scenesType="1";

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverLoginID() {
        return receiverLoginID;
    }

    public void setReceiverLoginID(String receiverLoginID) {
        this.receiverLoginID = receiverLoginID;
    }

    public String getTransactionToken() {
        return transactionToken;
    }

    public void setTransactionToken(String transactionToken) {
        this.transactionToken = transactionToken;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getScenesType() {
        return scenesType;
    }

    public void setScenesType(String scenesType) {
        this.scenesType = scenesType;
    }
}
