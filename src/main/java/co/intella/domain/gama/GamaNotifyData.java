package co.intella.domain.gama;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import javax.persistence.criteria.CriteriaBuilder;

/**
 * @author Alex
 */

public class GamaNotifyData {

    @SerializedName("TransactionToken")
    private String transactionToken;

    @SerializedName("TransactionID")
    private String transactionId;

    @SerializedName("MerchantOrderID")
    private String merchantOrderId;

    @SerializedName("TransAmount")
    private int transAmount;

    @SerializedName("TransFinallyAmount")
    private String transFinallyAmount;

    @SerializedName("TransFee")
    private String transFee;

    @SerializedName("CurrencyCode")
    private String currencyCode;

    @SerializedName("TradingContent")
    private String tradingContent;

    @SerializedName("Memo")
    private String memo;

    @SerializedName("TransType")
    private String transType;

    @SerializedName("BusinessDate")
    private String businessDate;

    @SerializedName("Remark1")
    private String remark1;

    @SerializedName("Remark2")
    private String remark2;

    @SerializedName("ExpiredDateTime")
    private String expiredDateTime;

    @SerializedName("PerformanceBondDate")
    private String performanceBondDate;

    @SerializedName("TransDate")
    private String transDate;

    @SerializedName("StatusCode")
    private String statusCode;

    @SerializedName("MAC")
    private String mac;

    @SerializedName("LoveCode")
    private String loveCode;

    @SerializedName("CarrierID")
    private String carrierID;
    
    @SerializedName("CreditCardFourNumber")
    private String creditCardFourNumber;

    public String getTransactionToken() {
        return transactionToken;
    }

    public void setTransactionToken(String transactionToken) {
        this.transactionToken = transactionToken;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getMerchantOrderId() {
        return merchantOrderId;
    }

    public void setMerchantOrderId(String merchantOrderId) {
        this.merchantOrderId = merchantOrderId;
    }

    public int getTransAmount() {
        return transAmount;
    }

    public void setTransAmount(int transAmount) {
        this.transAmount = transAmount;
    }

    public String getTransFinallyAmount() {
        return transFinallyAmount;
    }

    public void setTransFinallyAmount(String transFinallyAmount) {
        this.transFinallyAmount = transFinallyAmount;
    }

    public String getTransFee() {
        return transFee;
    }

    public void setTransFee(String transFee) {
        this.transFee = transFee;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getTradingContent() {
        return tradingContent;
    }

    public void setTradingContent(String tradingContent) {
        this.tradingContent = tradingContent;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getBusinessDate() {
        return businessDate;
    }

    public void setBusinessDate(String businessDate) {
        this.businessDate = businessDate;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    public String getExpiredDateTime() {
        return expiredDateTime;
    }

    public void setExpiredDateTime(String expiredDateTime) {
        this.expiredDateTime = expiredDateTime;
    }

    public String getPerformanceBondDate() {
        return performanceBondDate;
    }

    public void setPerformanceBondDate(String performanceBondDate) {
        this.performanceBondDate = performanceBondDate;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getLoveCode() {
        return loveCode;
    }

    public void setLoveCode(String loveCode) {
        this.loveCode = loveCode;
    }

    public String getCarrierID() {
        return carrierID;
    }

    public void setCarrierID(String carrierID) {
        this.carrierID = carrierID;
    }

	public String getCreditCardFourNumber() {
		return creditCardFourNumber;
	}

	public void setCreditCardFourNumber(String creditCardFourNumber) {
		this.creditCardFourNumber = creditCardFourNumber;
	}
}
