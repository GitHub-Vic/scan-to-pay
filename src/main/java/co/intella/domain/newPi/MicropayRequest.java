package co.intella.domain.newPi;

import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import co.intella.utility.SystemInstance;
import com.google.gson.JsonObject;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MicropayRequest {

    public MicropayRequest() {
        super();
    }

    public MicropayRequest(TradeDetail tradeDetail, PaymentAccount paymentAccount, JsonObject dataObject) {
        this.bill_id = tradeDetail.getOrderId();
        this.seq_no = "0000000001";
        this.barcode = tradeDetail.getBarcode();
        this.amt = tradeDetail.getPayment();
        this.sale_amt = 0L;
        this.bill_amt = 0L;
        this.proxy_amt = 0L;
        this.nondiscount = "0";
        this.nondiscount_amt = 0L;
        this.channel_id = paymentAccount.getAccount();
        this.store_no = dataObject.has("StoreInfo") ? dataObject.get("StoreInfo").getAsString() : paymentAccount.getAccount();
        this.reg_id = dataObject.has("DeviceInfo") ? dataObject.get("DeviceInfo").getAsString() : "0000000001";
        this.items = dataObject.has("Body") ? dataObject.get("Body").getAsString() : tradeDetail.getDescription();
        this.create_time = LocalDateTime.now().format(DateTimeFormatter.ofPattern(SystemInstance.DATE_PATTERN));
    }

    private String bill_id;
    private String seq_no;
    private String barcode;
    private Long amt;
    private Long sale_amt;
    private Long bill_amt;
    private Long proxy_amt;
    private String nondiscount;
    private Long nondiscount_amt;
    private String channel_id;
    private String store_no;
    private String reg_id;
    private String items;
    private String create_time;

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }

    public String getSeq_no() {
        return seq_no;
    }

    public void setSeq_no(String seq_no) {
        this.seq_no = seq_no;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Long getAmt() {
        return amt;
    }

    public void setAmt(Long amt) {
        this.amt = amt;
    }

    public Long getSale_amt() {
        return sale_amt;
    }

    public void setSale_amt(Long sale_amt) {
        this.sale_amt = sale_amt;
    }

    public Long getBill_amt() {
        return bill_amt;
    }

    public void setBill_amt(Long bill_amt) {
        this.bill_amt = bill_amt;
    }

    public Long getProxy_amt() {
        return proxy_amt;
    }

    public void setProxy_amt(Long proxy_amt) {
        this.proxy_amt = proxy_amt;
    }

    public String getNondiscount() {
        return nondiscount;
    }

    public void setNondiscount(String nondiscount) {
        this.nondiscount = nondiscount;
    }

    public Long getNondiscount_amt() {
        return nondiscount_amt;
    }

    public void setNondiscount_amt(Long nondiscount_amt) {
        this.nondiscount_amt = nondiscount_amt;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    public String getStore_no() {
        return store_no;
    }

    public void setStore_no(String store_no) {
        this.store_no = store_no;
    }

    public String getReg_id() {
        return reg_id;
    }

    public void setReg_id(String reg_id) {
        this.reg_id = reg_id;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    @Override
    public String toString() {
        return "MicropayRequest{" +
                "bill_id='" + bill_id + '\'' +
                ", seq_no='" + seq_no + '\'' +
                ", barcode='" + barcode + '\'' +
                ", amt=" + amt +
                ", sale_amt=" + sale_amt +
                ", bill_amt=" + bill_amt +
                ", proxy_amt=" + proxy_amt +
                ", nondiscount='" + nondiscount + '\'' +
                ", nondiscount_amt=" + nondiscount_amt +
                ", channel_id='" + channel_id + '\'' +
                ", store_no='" + store_no + '\'' +
                ", reg_id='" + reg_id + '\'' +
                ", items='" + items + '\'' +
                ", create_time='" + create_time + '\'' +
                '}';
    }
}
