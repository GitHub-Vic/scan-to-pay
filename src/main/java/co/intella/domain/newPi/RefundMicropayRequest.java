package co.intella.domain.newPi;

import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import co.intella.utility.SystemInstance;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class RefundMicropayRequest {

    public RefundMicropayRequest() {
        super();
    }

    public RefundMicropayRequest(TradeDetail tradeDetail, PaymentAccount paymentAccount) {
        this.bill_id = tradeDetail.getOrderId();
        this.wallet_transaction_id = tradeDetail.getSystemOrderId();
        this.channel_id = paymentAccount.getAccount();
        this.refund_amount = tradeDetail.getPayment();
        this.create_time = LocalDateTime.now().format(DateTimeFormatter.ofPattern(SystemInstance.DATE_PATTERN));
    }

    private String bill_id;
    private String wallet_transaction_id;
    private String channel_id;
    private Long refund_amount;
    private String create_time;

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }

    public String getWallet_transaction_id() {
        return wallet_transaction_id;
    }

    public void setWallet_transaction_id(String wallet_transaction_id) {
        this.wallet_transaction_id = wallet_transaction_id;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    public Long getRefund_amount() {
        return refund_amount;
    }

    public void setRefund_amount(Long refund_amount) {
        this.refund_amount = refund_amount;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    @Override
    public String toString() {
        return "RefundMicropayRequest{" +
                "bill_id='" + bill_id + '\'' +
                ", wallet_transaction_id='" + wallet_transaction_id + '\'' +
                ", channel_id='" + channel_id + '\'' +
                ", refund_amount='" + refund_amount + '\'' +
                ", create_time='" + create_time + '\'' +
                '}';
    }
}
