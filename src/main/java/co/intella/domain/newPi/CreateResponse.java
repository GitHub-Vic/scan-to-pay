package co.intella.domain.newPi;

public class CreateResponse {

    private String return_code;
    private String return_message;
    private TransData trans_data;

    public static class TransData {
        private String url;
        private String ios_url;
        private String android_url;
        private String expire_time;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getIos_url() {
            return ios_url;
        }

        public void setIos_url(String ios_url) {
            this.ios_url = ios_url;
        }

        public String getAndroid_url() {
            return android_url;
        }

        public void setAndroid_url(String android_url) {
            this.android_url = android_url;
        }

        public String getExpire_time() {
            return expire_time;
        }

        public void setExpire_time(String expire_time) {
            this.expire_time = expire_time;
        }

        @Override
        public String toString() {
            return "TransData{" +
                    "url='" + url + '\'' +
                    ", ios_url='" + ios_url + '\'' +
                    ", android_url='" + android_url + '\'' +
                    ", expire_time='" + expire_time + '\'' +
                    '}';
        }
    }

    public String getReturn_code() {
        return return_code;
    }

    public void setReturn_code(String return_code) {
        this.return_code = return_code;
    }

    public String getReturn_message() {
        return return_message;
    }

    public void setReturn_message(String return_message) {
        this.return_message = return_message;
    }

    public TransData getTrans_data() {
        return trans_data;
    }

    public void setTrans_data(TransData trans_data) {
        this.trans_data = trans_data;
    }

    @Override
    public String toString() {
        return "CreateResponse{" +
                "return_code='" + return_code + '\'' +
                ", return_message='" + return_message + '\'' +
                ", trans_data=" + trans_data +
                '}';
    }
}
