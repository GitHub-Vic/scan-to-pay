package co.intella.domain.newPi;

public class RefundResponse {

    private String return_code;
    private String return_message;
    private RefundedData refunded_data;

    public static class RefundedData {
        private String bill_id;
        private String transaction_id;
        private String refund_bill_id;
        private String refund_id;
        private Integer refunded_amount;
        private Integer remained_amount;

        public String getBill_id() {
            return bill_id;
        }

        public void setBill_id(String bill_id) {
            this.bill_id = bill_id;
        }

        public String getTransaction_id() {
            return transaction_id;
        }

        public void setTransaction_id(String transaction_id) {
            this.transaction_id = transaction_id;
        }

        public String getRefund_bill_id() {
            return refund_bill_id;
        }

        public void setRefund_bill_id(String refund_bill_id) {
            this.refund_bill_id = refund_bill_id;
        }

        public String getRefund_id() {
            return refund_id;
        }

        public void setRefund_id(String refund_id) {
            this.refund_id = refund_id;
        }

        public Integer getRefunded_amount() {
            return refunded_amount;
        }

        public void setRefunded_amount(Integer refunded_amount) {
            this.refunded_amount = refunded_amount;
        }

        public Integer getRemained_amount() {
            return remained_amount;
        }

        public void setRemained_amount(Integer remained_amount) {
            this.remained_amount = remained_amount;
        }

        @Override
        public String toString() {
            return "RefundedData{" +
                    "bill_id='" + bill_id + '\'' +
                    ", transaction_id='" + transaction_id + '\'' +
                    ", refund_bill_id='" + refund_bill_id + '\'' +
                    ", refund_id='" + refund_id + '\'' +
                    ", refunded_amount=" + refunded_amount +
                    ", remained_amount=" + remained_amount +
                    '}';
        }
    }

    public String getReturn_code() {
        return return_code;
    }

    public void setReturn_code(String return_code) {
        this.return_code = return_code;
    }

    public String getReturn_message() {
        return return_message;
    }

    public void setReturn_message(String return_message) {
        this.return_message = return_message;
    }

    public RefundedData getRefunded_data() {
        return refunded_data;
    }

    public void setRefunded_data(RefundedData refunded_data) {
        this.refunded_data = refunded_data;
    }

    @Override
    public String toString() {
        return "RefundResponse{" +
                "return_code='" + return_code + '\'' +
                ", return_message='" + return_message + '\'' +
                ", refunded_data=" + refunded_data +
                '}';
    }
}
