package co.intella.domain.newPi;

public class AuthorizationResponse {

    private String return_code;
    private String return_message;
    private AuthData auth_data;

    public static class AuthData {

        private String token;
        private String expired_time;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getExpired_time() {
            return expired_time;
        }

        public void setExpired_time(String expired_time) {
            this.expired_time = expired_time;
        }

        @Override
        public String toString() {
            return "authData{" +
                    "token='" + token + '\'' +
                    ", expired_time='" + expired_time + '\'' +
                    '}';
        }
    }

    public String getReturn_code() {
        return return_code;
    }

    public void setReturn_code(String return_code) {
        this.return_code = return_code;
    }

    public String getReturn_message() {
        return return_message;
    }

    public void setReturn_message(String return_message) {
        this.return_message = return_message;
    }

    public AuthData getAuth_data() {
        return auth_data;
    }

    public void setAuth_data(AuthData auth_data) {
        this.auth_data = auth_data;
    }

    @Override
    public String toString() {
        return "AuthorizationResponse{" +
                "return_code='" + return_code + '\'' +
                ", return_message='" + return_message + '\'' +
                ", auth_data=" + auth_data +
                '}';
    }
}
