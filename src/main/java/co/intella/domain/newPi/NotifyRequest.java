package co.intella.domain.newPi;

public class NotifyRequest {

    private String bill_id;
    private String transaction_id;
    private String paid_time;
    private String secret_key;

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getPaid_time() {
        return paid_time;
    }

    public void setPaid_time(String paid_time) {
        this.paid_time = paid_time;
    }

    public String getSecret_key() {
        return secret_key;
    }

    public void setSecret_key(String secret_key) {
        this.secret_key = secret_key;
    }

    @Override
    public String toString() {
        return "NotifyRequest{" +
                "bill_id='" + bill_id + '\'' +
                ", transaction_id='" + transaction_id + '\'' +
                ", paid_time='" + paid_time + '\'' +
                ", secret_key='" + secret_key + '\'' +
                '}';
    }
}
