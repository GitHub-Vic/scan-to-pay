package co.intella.domain.newPi;

import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;

public class StatusRequest {
    public StatusRequest() {
        super();
    }

    public StatusRequest(PaymentAccount paymentAccount, TradeDetail tradeDetail) {

        this.bill_id = tradeDetail.getOrderId();
        this.token = paymentAccount.getAccountTerminalId();
        this.partner_id = paymentAccount.getAccount();
    }


    private String partner_id;
    private String token;
    private String bill_id;


    public String getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }

    @Override
    public String toString() {
        return "StatusRequest{" +
                "partner_id='" + partner_id + '\'' +
                ", token='" + token + '\'' +
                ", bill_id='" + bill_id + '\'' +
                '}';
    }
}
