package co.intella.domain.newPi;

import java.util.List;

public class StatusResponse {

    private String return_code;
    private String return_message;
    private OrderData order_data;

    public static class OrderData {
        private String bill_id;
        private String transaction_id;
        private String status;
        private String created_time;
        private String paid_time;
        private String store_no;
        private String store_branch_name;
        private Integer created_money;
        private Integer paid_money;
        private PaidInfo paid_info;
        private List<RefundedInfo> refunded_info;
        private RemainedInfo remained_info;
        private List<ItemLists> item_lists;
        private String memo;

        public static class PaidInfo {
            private String balance;
            private String credit_card;
            private String pipoint;
            private String other;

            public String getBalance() {
                return balance;
            }

            public void setBalance(String balance) {
                this.balance = balance;
            }

            public String getCredit_card() {
                return credit_card;
            }

            public void setCredit_card(String credit_card) {
                this.credit_card = credit_card;
            }

            public String getPipoint() {
                return pipoint;
            }

            public void setPipoint(String pipoint) {
                this.pipoint = pipoint;
            }

            public String getOther() {
                return other;
            }

            public void setOther(String other) {
                this.other = other;
            }

            @Override
            public String toString() {
                return "PaidInfo{" +
                        "balance='" + balance + '\'' +
                        ", credit_card='" + credit_card + '\'' +
                        ", pipoint='" + pipoint + '\'' +
                        ", other='" + other + '\'' +
                        '}';
            }
        }

        public static class RefundedInfo {
            private String refund_bill_id;
            private String refund_id;
            private String refunded_time;
            private String refunded_money;
            private String refunded_message;

            public String getRefund_bill_id() {
                return refund_bill_id;
            }

            public void setRefund_bill_id(String refund_bill_id) {
                this.refund_bill_id = refund_bill_id;
            }

            public String getRefund_id() {
                return refund_id;
            }

            public void setRefund_id(String refund_id) {
                this.refund_id = refund_id;
            }

            public String getRefunded_time() {
                return refunded_time;
            }

            public void setRefunded_time(String refunded_time) {
                this.refunded_time = refunded_time;
            }

            public String getRefunded_money() {
                return refunded_money;
            }

            public void setRefunded_money(String refunded_money) {
                this.refunded_money = refunded_money;
            }

            public String getRefunded_message() {
                return refunded_message;
            }

            public void setRefunded_message(String refunded_message) {
                this.refunded_message = refunded_message;
            }

            @Override
            public String toString() {
                return "RefundedInfo{" +
                        "refund_bill_id='" + refund_bill_id + '\'' +
                        ", refund_id='" + refund_id + '\'' +
                        ", refunded_time='" + refunded_time + '\'' +
                        ", refunded_money='" + refunded_money + '\'' +
                        ", refunded_message='" + refunded_message + '\'' +
                        '}';
            }
        }

        public static class RemainedInfo {
            private String balance;
            private String credit_card;
            private String pipoint;
            private String other;

            public String getBalance() {
                return balance;
            }

            public void setBalance(String balance) {
                this.balance = balance;
            }

            public String getCredit_card() {
                return credit_card;
            }

            public void setCredit_card(String credit_card) {
                this.credit_card = credit_card;
            }

            public String getPipoint() {
                return pipoint;
            }

            public void setPipoint(String pipoint) {
                this.pipoint = pipoint;
            }

            public String getOther() {
                return other;
            }

            public void setOther(String other) {
                this.other = other;
            }

            @Override
            public String toString() {
                return "RemainedInfo{" +
                        "balance='" + balance + '\'' +
                        ", credit_card='" + credit_card + '\'' +
                        ", pipoint='" + pipoint + '\'' +
                        ", other='" + other + '\'' +
                        '}';
            }
        }

        public static class ItemLists {
            private String name;
            private String price;
            private String qty;
            private String note;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getQty() {
                return qty;
            }

            public void setQty(String qty) {
                this.qty = qty;
            }

            public String getNote() {
                return note;
            }

            public void setNote(String note) {
                this.note = note;
            }

            @Override
            public String toString() {
                return "ItemLists{" +
                        "name='" + name + '\'' +
                        ", price='" + price + '\'' +
                        ", qty='" + qty + '\'' +
                        ", note='" + note + '\'' +
                        '}';
            }
        }

        public String getBill_id() {
            return bill_id;
        }

        public void setBill_id(String bill_id) {
            this.bill_id = bill_id;
        }

        public String getTransaction_id() {
            return transaction_id;
        }

        public void setTransaction_id(String transaction_id) {
            this.transaction_id = transaction_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreated_time() {
            return created_time;
        }

        public void setCreated_time(String created_time) {
            this.created_time = created_time;
        }

        public String getPaid_time() {
            return paid_time;
        }

        public void setPaid_time(String paid_time) {
            this.paid_time = paid_time;
        }

        public String getStore_no() {
            return store_no;
        }

        public void setStore_no(String store_no) {
            this.store_no = store_no;
        }

        public String getStore_branch_name() {
            return store_branch_name;
        }

        public void setStore_branch_name(String store_branch_name) {
            this.store_branch_name = store_branch_name;
        }

        public Integer getCreated_money() {
            return created_money;
        }

        public void setCreated_money(Integer created_money) {
            this.created_money = created_money;
        }

        public Integer getPaid_money() {
            return paid_money;
        }

        public void setPaid_money(Integer paid_money) {
            this.paid_money = paid_money;
        }

        public PaidInfo getPaid_info() {
            return paid_info;
        }

        public void setPaid_info(PaidInfo paid_info) {
            this.paid_info = paid_info;
        }

        public List<RefundedInfo> getRefunded_info() {
            return refunded_info;
        }

        public void setRefunded_info(List<RefundedInfo> refunded_info) {
            this.refunded_info = refunded_info;
        }

        public RemainedInfo getRemained_info() {
            return remained_info;
        }

        public void setRemained_info(RemainedInfo remained_info) {
            this.remained_info = remained_info;
        }

        public List<ItemLists> getItem_lists() {
            return item_lists;
        }

        public void setItem_lists(List<ItemLists> item_lists) {
            this.item_lists = item_lists;
        }

        public String getMemo() {
            return memo;
        }

        public void setMemo(String memo) {
            this.memo = memo;
        }

        @Override
        public String toString() {
            return "OrderData{" +
                    "bill_id='" + bill_id + '\'' +
                    ", transaction_id='" + transaction_id + '\'' +
                    ", status='" + status + '\'' +
                    ", created_time='" + created_time + '\'' +
                    ", paid_time='" + paid_time + '\'' +
                    ", store_no='" + store_no + '\'' +
                    ", store_branch_name='" + store_branch_name + '\'' +
                    ", created_money=" + created_money +
                    ", paid_money=" + paid_money +
                    ", paid_info=" + paid_info +
                    ", refunded_info=" + refunded_info +
                    ", remained_info=" + remained_info +
                    ", item_lists=" + item_lists +
                    ", memo='" + memo + '\'' +
                    '}';
        }
    }

    public String getReturn_code() {
        return return_code;
    }

    public void setReturn_code(String return_code) {
        this.return_code = return_code;
    }

    public String getReturn_message() {
        return return_message;
    }

    public void setReturn_message(String return_message) {
        this.return_message = return_message;
    }

    public OrderData getOrder_data() {
        return order_data;
    }

    public void setOrder_data(OrderData order_data) {
        this.order_data = order_data;
    }

    @Override
    public String toString() {
        return "StatusResponse{" +
                "return_code='" + return_code + '\'' +
                ", return_message='" + return_message + '\'' +
                ", order_data=" + order_data +
                '}';
    }
}
