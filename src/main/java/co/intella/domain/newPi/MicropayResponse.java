package co.intella.domain.newPi;

import com.google.gson.annotations.SerializedName;

public class MicropayResponse {

    private String version;
    private String status;
    private String error_code;
    private String error_desc;
    private String bill_id;
    private String psp_id;
    private String wallet_transaction_id;
    private String trans_status;
    private Integer amt;
    private Integer sale_amt;
    private Integer bill_amt;
    private Integer proxy_amt;
    private String carrier_id_2;
    private String channel_id;
    private String store_no;
    private String seq_no;
    private String reg_id;
    private String items;
    private String create_time;
    private String transaction_time;
    private String member_card_id;
    private String member_card_type;
    private String payment_type;
    private Integer balance_amount;
    @SerializedName("2nd_funding_amount")
    private Integer Twond_funding_amount;
    @SerializedName("2nd_funding_type")
    private Integer Twond_funding_type;
    @SerializedName("2nd_funding_source_code")
    private Integer Twond_funding_source_code;
    private String merchant_discount_code;
    private Integer merchant_discount_amount;
    private String psp_discount_code;
    private Integer psp_discount_amount;
    private Integer psp_bonuspoint;
    private Integer psp_bonuspoint_amount;
    private String barcode;
    private String clearing_by;
    private String coupon_id;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_desc() {
        return error_desc;
    }

    public void setError_desc(String error_desc) {
        this.error_desc = error_desc;
    }

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }

    public String getPsp_id() {
        return psp_id;
    }

    public void setPsp_id(String psp_id) {
        this.psp_id = psp_id;
    }

    public String getWallet_transaction_id() {
        return wallet_transaction_id;
    }

    public void setWallet_transaction_id(String wallet_transaction_id) {
        this.wallet_transaction_id = wallet_transaction_id;
    }

    public String getTrans_status() {
        return trans_status;
    }

    public void setTrans_status(String trans_status) {
        this.trans_status = trans_status;
    }

    public Integer getAmt() {
        return amt;
    }

    public void setAmt(Integer amt) {
        this.amt = amt;
    }

    public Integer getSale_amt() {
        return sale_amt;
    }

    public void setSale_amt(Integer sale_amt) {
        this.sale_amt = sale_amt;
    }

    public Integer getBill_amt() {
        return bill_amt;
    }

    public void setBill_amt(Integer bill_amt) {
        this.bill_amt = bill_amt;
    }

    public Integer getProxy_amt() {
        return proxy_amt;
    }

    public void setProxy_amt(Integer proxy_amt) {
        this.proxy_amt = proxy_amt;
    }

    public String getCarrier_id_2() {
        return carrier_id_2;
    }

    public void setCarrier_id_2(String carrier_id_2) {
        this.carrier_id_2 = carrier_id_2;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    public String getStore_no() {
        return store_no;
    }

    public void setStore_no(String store_no) {
        this.store_no = store_no;
    }

    public String getSeq_no() {
        return seq_no;
    }

    public void setSeq_no(String seq_no) {
        this.seq_no = seq_no;
    }

    public String getReg_id() {
        return reg_id;
    }

    public void setReg_id(String reg_id) {
        this.reg_id = reg_id;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getTransaction_time() {
        return transaction_time;
    }

    public void setTransaction_time(String transaction_time) {
        this.transaction_time = transaction_time;
    }

    public String getMember_card_id() {
        return member_card_id;
    }

    public void setMember_card_id(String member_card_id) {
        this.member_card_id = member_card_id;
    }

    public String getMember_card_type() {
        return member_card_type;
    }

    public void setMember_card_type(String member_card_type) {
        this.member_card_type = member_card_type;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public Integer getBalance_amount() {
        return balance_amount;
    }

    public void setBalance_amount(Integer balance_amount) {
        this.balance_amount = balance_amount;
    }

    public Integer getTwond_funding_amount() {
        return Twond_funding_amount;
    }

    public void setTwond_funding_amount(Integer twond_funding_amount) {
        Twond_funding_amount = twond_funding_amount;
    }

    public Integer getTwond_funding_type() {
        return Twond_funding_type;
    }

    public void setTwond_funding_type(Integer twond_funding_type) {
        Twond_funding_type = twond_funding_type;
    }

    public Integer getTwond_funding_source_code() {
        return Twond_funding_source_code;
    }

    public void setTwond_funding_source_code(Integer twond_funding_source_code) {
        Twond_funding_source_code = twond_funding_source_code;
    }

    public String getMerchant_discount_code() {
        return merchant_discount_code;
    }

    public void setMerchant_discount_code(String merchant_discount_code) {
        this.merchant_discount_code = merchant_discount_code;
    }

    public Integer getMerchant_discount_amount() {
        return merchant_discount_amount;
    }

    public void setMerchant_discount_amount(Integer merchant_discount_amount) {
        this.merchant_discount_amount = merchant_discount_amount;
    }

    public String getPsp_discount_code() {
        return psp_discount_code;
    }

    public void setPsp_discount_code(String psp_discount_code) {
        this.psp_discount_code = psp_discount_code;
    }

    public Integer getPsp_discount_amount() {
        return psp_discount_amount;
    }

    public void setPsp_discount_amount(Integer psp_discount_amount) {
        this.psp_discount_amount = psp_discount_amount;
    }

    public Integer getPsp_bonuspoint() {
        return psp_bonuspoint;
    }

    public void setPsp_bonuspoint(Integer psp_bonuspoint) {
        this.psp_bonuspoint = psp_bonuspoint;
    }

    public Integer getPsp_bonuspoint_amount() {
        return psp_bonuspoint_amount;
    }

    public void setPsp_bonuspoint_amount(Integer psp_bonuspoint_amount) {
        this.psp_bonuspoint_amount = psp_bonuspoint_amount;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getClearing_by() {
        return clearing_by;
    }

    public void setClearing_by(String clearing_by) {
        this.clearing_by = clearing_by;
    }

    public String getCoupon_id() {
        return coupon_id;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }

    @Override
    public String toString() {
        return "MicropayResponse{" +
                "version='" + version + '\'' +
                ", status='" + status + '\'' +
                ", error_code='" + error_code + '\'' +
                ", error_desc='" + error_desc + '\'' +
                ", bill_id='" + bill_id + '\'' +
                ", psp_id='" + psp_id + '\'' +
                ", wallet_transaction_id='" + wallet_transaction_id + '\'' +
                ", trans_status='" + trans_status + '\'' +
                ", amt=" + amt +
                ", sale_amt=" + sale_amt +
                ", bill_amt=" + bill_amt +
                ", proxy_amt=" + proxy_amt +
                ", carrier_id_2='" + carrier_id_2 + '\'' +
                ", channel_id='" + channel_id + '\'' +
                ", store_no='" + store_no + '\'' +
                ", seq_no='" + seq_no + '\'' +
                ", reg_id='" + reg_id + '\'' +
                ", items='" + items + '\'' +
                ", create_time='" + create_time + '\'' +
                ", transaction_time='" + transaction_time + '\'' +
                ", member_card_id='" + member_card_id + '\'' +
                ", member_card_type='" + member_card_type + '\'' +
                ", payment_type='" + payment_type + '\'' +
                ", balance_amount=" + balance_amount +
                ", Twond_funding_amount=" + Twond_funding_amount +
                ", Twond_funding_type=" + Twond_funding_type +
                ", Twond_funding_source_code=" + Twond_funding_source_code +
                ", merchant_discount_code='" + merchant_discount_code + '\'' +
                ", merchant_discount_amount=" + merchant_discount_amount +
                ", psp_discount_code='" + psp_discount_code + '\'' +
                ", psp_discount_amount=" + psp_discount_amount +
                ", psp_bonuspoint=" + psp_bonuspoint +
                ", psp_bonuspoint_amount=" + psp_bonuspoint_amount +
                ", barcode='" + barcode + '\'' +
                ", clearing_by='" + clearing_by + '\'' +
                ", coupon_id='" + coupon_id + '\'' +
                '}';
    }
}
