package co.intella.domain.newPi;

import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import co.intella.utility.EncryptUtil;

public class CreateRequest {

    public CreateRequest() {
        super();
    }

    public CreateRequest(PaymentAccount paymentAccount, TradeDetail tradeDetail, String apiUrl) {

        this.partner_id = paymentAccount.getAccount();
        this.token = paymentAccount.getAccountTerminalId();
        this.bill_id = tradeDetail.getOrderId();
        this.money = tradeDetail.getPayment();
        this.return_url = String.format("%sallpaypass/api/payment/order/result?orderId=%s", apiUrl, EncryptUtil.encryptSimple(tradeDetail.getOrderId()));
    }

    private String partner_id;
    private String token;
    private String bill_id;
    private String return_url;
    private Long money;

    public String getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }

    public Long getMoney() {
        return money;
    }

    public void setMoney(Long money) {
        this.money = money;
    }

    public String getReturn_url() {
        return return_url;
    }

    public void setReturn_url(String return_url) {
        this.return_url = return_url;
    }

    @Override
    public String toString() {
        return "CreateRequest{" +
                "partner_id='" + partner_id + '\'' +
                ", token='" + token + '\'' +
                ", bill_id='" + bill_id + '\'' +
                ", return_url='" + return_url + '\'' +
                ", money=" + money +
                '}';
    }
}
