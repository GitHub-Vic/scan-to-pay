package co.intella.domain.newPi;

import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;

public class RefundRequest {

    public RefundRequest() {
        super();
    }

    public RefundRequest(TradeDetail tradeDetail, PaymentAccount paymentAccount) {
        this.partner_id = paymentAccount.getAccount();
        this.bill_id = tradeDetail.getOrderId();
        this.token = paymentAccount.getAccountTerminalId();
        this.refund_bill_id = tradeDetail.getStoreRefundId();
        this.refund_money = tradeDetail.getPayment();
    }

    private String partner_id;
    private String token;
    private String bill_id;
    private String refund_bill_id;
    private Long refund_money;

    public String getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }

    public String getRefund_bill_id() {
        return refund_bill_id;
    }

    public void setRefund_bill_id(String refund_bill_id) {
        this.refund_bill_id = refund_bill_id;
    }

    public Long getRefund_money() {
        return refund_money;
    }

    public void setRefund_money(Long refund_money) {
        this.refund_money = refund_money;
    }

    @Override
    public String toString() {
        return "RefundRequest{" +
                "partner_id='" + partner_id + '\'' +
                ", token='" + token + '\'' +
                ", bill_id='" + bill_id + '\'' +
                ", refund_bill_id='" + refund_bill_id + '\'' +
                ", refund_money=" + refund_money +
                '}';
    }
}
