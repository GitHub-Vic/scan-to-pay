package co.intella.domain.newPi;

import co.intella.model.PaymentAccount;

public class AuthorizationRequest {

    public AuthorizationRequest() {
        super();
    }

    public AuthorizationRequest(PaymentAccount paymentAccount) {
        this.expired_in = 1440;
        this.partner_key = paymentAccount.getHashKey();
        this.partner_id = paymentAccount.getAccount();
    }


    private String partner_id;
    private String partner_key;
    private Integer expired_in;

    public String getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public String getPartner_key() {
        return partner_key;
    }

    public void setPartner_key(String partner_key) {
        this.partner_key = partner_key;
    }

    public Integer getExpired_in() {
        return expired_in;
    }

    public void setExpired_in(Integer expired_in) {
        this.expired_in = expired_in;
    }

    @Override
    public String toString() {
        return "AuthorizationRequest{" +
                "partner_id='" + partner_id + '\'' +
                ", partner_key='" + partner_key + '\'' +
                ", expired_in=" + expired_in +
                '}';
    }
}
