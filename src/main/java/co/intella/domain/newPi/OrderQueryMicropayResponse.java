package co.intella.domain.newPi;

public class OrderQueryMicropayResponse {

    private String version;
    private String status;
    private String error_code;
    private String error_desc;
    private String bill_id;
    private String psp_id;
    private String wallet_transaction_id;
    private String trans_status;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_desc() {
        return error_desc;
    }

    public void setError_desc(String error_desc) {
        this.error_desc = error_desc;
    }

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }

    public String getPsp_id() {
        return psp_id;
    }

    public void setPsp_id(String psp_id) {
        this.psp_id = psp_id;
    }

    public String getWallet_transaction_id() {
        return wallet_transaction_id;
    }

    public void setWallet_transaction_id(String wallet_transaction_id) {
        this.wallet_transaction_id = wallet_transaction_id;
    }

    public String getTrans_status() {
        return trans_status;
    }

    public void setTrans_status(String trans_status) {
        this.trans_status = trans_status;
    }

    @Override
    public String toString() {
        return "OrderQueryMicropayResponse{" +
                "version='" + version + '\'' +
                ", status='" + status + '\'' +
                ", error_code='" + error_code + '\'' +
                ", error_desc='" + error_desc + '\'' +
                ", bill_id='" + bill_id + '\'' +
                ", psp_id='" + psp_id + '\'' +
                ", wallet_transaction_id='" + wallet_transaction_id + '\'' +
                ", trans_status='" + trans_status + '\'' +
                '}';
    }
}
