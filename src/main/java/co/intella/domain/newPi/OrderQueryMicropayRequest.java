package co.intella.domain.newPi;

import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import org.apache.commons.lang3.StringUtils;

public class OrderQueryMicropayRequest {

    public OrderQueryMicropayRequest() {
        super();
    }

    public OrderQueryMicropayRequest(TradeDetail tradeDetail, PaymentAccount paymentAccount) {
        this.bill_id = tradeDetail.getOrderId();
        this.channel_id = paymentAccount.getAccount();
        this.wallet_transaction_id = StringUtils.defaultString(tradeDetail.getSystemOrderId(), "");
    }

    private String bill_id;
    private String wallet_transaction_id;
    private String channel_id;

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }

    public String getWallet_transaction_id() {
        return wallet_transaction_id;
    }

    public void setWallet_transaction_id(String wallet_transaction_id) {
        this.wallet_transaction_id = wallet_transaction_id;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    @Override
    public String toString() {
        return "OrderQueryMicropayRequest{" +
                "bill_id='" + bill_id + '\'' +
                ", wallet_transaction_id='" + wallet_transaction_id + '\'' +
                ", channel_id='" + channel_id + '\'' +
                '}';
    }
}
