package co.intella.domain.newPi;

public class NotifyResponse {

    public NotifyResponse() {
        super();
    }

    public NotifyResponse(NotifyRequest notifyRequest) {

        this.bill_id = notifyRequest.getBill_id();
        this.status = "success";
        //Pi Server 將只會發送 1 次通知，所以直接壓成success
    }

    private String status;
    private String bill_id;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }

    @Override
    public String toString() {
        return "NotifyResponse{" +
                "status='" + status + '\'' +
                ", bill_id='" + bill_id + '\'' +
                '}';
    }
}
