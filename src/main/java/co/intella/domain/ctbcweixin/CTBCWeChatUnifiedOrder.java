package co.intella.domain.ctbcweixin;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;


public class CTBCWeChatUnifiedOrder {

	@JsonProperty("MchID")
	@SerializedName("MchID")
	private String mchID;

	@JsonProperty("DeviceInfo")
	@SerializedName("DeviceInfo")
	private String deviceInfo;

	@JsonProperty("Body")
	@SerializedName("Body")
	private String body;

	@JsonProperty("Attach")
	@SerializedName("Attach")
	private String attach;

	@JsonProperty("OutTradeNo")
	@SerializedName("OutTradeNo")
	private String outTradeNo;

	@JsonProperty("TxAmt")
	@SerializedName("TxAmt")
	private String txAmt;

	@JsonProperty("Currency")
	@SerializedName("Currency")
	private String currency;

	@JsonProperty("TotalFee")
	@SerializedName("TotalFee")
	private String totalFee;

	@JsonProperty("TimeStart")
	@SerializedName("TimeStart")
	private String timeStart;

	@JsonProperty("TimeExpire")
	@SerializedName("TimeExpire")
	private String timeExpire;

	@JsonProperty("GoodsTag")
	@SerializedName("GoodsTag")
	private String goodsTag;

	@JsonProperty("ProductID")
	@SerializedName("ProductID")
	private String productID;

	@JsonProperty("MchNotifyURL")
	@SerializedName("MchNotifyURL")
	private String mchNotifyURL;

    @JsonProperty("MAC")
    @SerializedName("MAC")
    private String mac;
    
    public String getMchID() {
		return mchID;
	}

	public void setMchID(String mchID) {
		this.mchID = mchID;
	}

	public String getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getTxAmt() {
		return txAmt;
	}

	public void setTxAmt(String txAmt) {
		this.txAmt = txAmt;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(String totalFee) {
		this.totalFee = totalFee;
	}

	public String getTimeStart() {
		return timeStart;
	}

	public void setTimeStart(String timeStart) {
		this.timeStart = timeStart;
	}

	public String getTimeExpire() {
		return timeExpire;
	}

	public void setTimeExpire(String timeExpire) {
		this.timeExpire = timeExpire;
	}

	public String getGoodsTag() {
		return goodsTag;
	}

	public void setGoodsTag(String goodsTag) {
		this.goodsTag = goodsTag;
	}

	public String getProductID() {
		return productID;
	}

	public void setProductID(String productID) {
		this.productID = productID;
	}

	public String getMchNotifyURL() {
		return mchNotifyURL;
	}

	public void setMchNotifyURL(String mchNotifyURL) {
		this.mchNotifyURL = mchNotifyURL;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getTxn() {
		return txn;
	}

	public void setTxn(String txn) {
		this.txn = txn;
	}

	@JsonProperty("TXN")
    @SerializedName("TXN")
    private String txn;
}
