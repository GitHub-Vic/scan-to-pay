package co.intella.domain.notify;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class TransactionMessage {

    private String description;

    private String merchantId;

    private String orderId;

    private String systemOrderId;

    @SerializedName("zhifuType")
    private String paymentType;

    private String money;

    private String date;

    private boolean isOfficial;

    @SerializedName("officialContent")
    private String officialContent;

    private String detail;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isOfficial() {
        return isOfficial;
    }

    public void setOfficial(boolean official) {
        isOfficial = official;
    }

    public String getOfficialContent() {
        return officialContent;
    }

    public void setOfficialContent(String officialContent) {
        this.officialContent = officialContent;
    }

    public String getSystemOrderId() {
        return systemOrderId;
    }

    public void setSystemOrderId(String systemOrderId) {
        this.systemOrderId = systemOrderId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
