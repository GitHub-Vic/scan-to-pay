package co.intella.domain.notify;

/**
 * @author Alex
 */

public class ApnsMessage {

    private String title;

    private String msg;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
