package co.intella.domain.notify;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class AppNotification {

    @SerializedName("cmd")
    private String command;

    private String type;

    @SerializedName("msg")
    private String message;

    private String udid;

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUdid() {
        return udid;
    }

    public void setUdid(String udid) {
        this.udid = udid;
    }
}
