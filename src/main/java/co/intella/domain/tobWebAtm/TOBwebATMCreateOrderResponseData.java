package co.intella.domain.tobWebAtm;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "CardPayRs")
public class TOBwebATMCreateOrderResponseData {
    @XmlElement(name = "RC")
    private String rC;
    @XmlElement(name = "MSG")
    private String mSG;
    @XmlElement(name = "SendSeqNo")
    private String sendSeqNo;
    @XmlElement(name = "MID")
    private String mID ;
    @XmlElement(name = "TID")
    private String tID;
    @XmlElement(name = "TxnType")
    private String txnType;
    @XmlElement(name = "ONO")
    private String oNO ;
    @XmlElement(name = "AccIdTo")
    private String accIdTo;
    @XmlElement(name = "CurAmt")
    private String curAmt;
    @XmlElement(name = "TrnDt")
    private String trnDt;
    @XmlElement(name = "TrnTime")
    private String trnTime;
    @XmlElement(name = "BankIdFrom")
    private String bankIdFrom;
    @XmlElement(name = "AcctIdFrom")
    private String acctIdFrom;
    @XmlElement(name = "DueDt")
    private String dueDt;
    @XmlElement(name = "TxnSeqNo")
    private String txnSeqNo;
    @XmlElement(name = "PayTxnFee")
    private String payTxnFee;
    @XmlElement(name = "MAC")
    private String mAC ;
    @XmlTransient
    public String getrC() {
        return rC;
    }

    public void setrC(String rC) {
        this.rC = rC;
    }
    @XmlTransient
    public String getmSG() {
        return mSG;
    }

    public void setmSG(String mSG) {
        this.mSG = mSG;
    }
    @XmlTransient
    public String getSendSeqNo() {
        return sendSeqNo;
    }

    public void setSendSeqNo(String sendSeqNo) {
        this.sendSeqNo = sendSeqNo;
    }
    @XmlTransient
    public String getmID() {
        return mID;
    }

    public void setmID(String mID) {
        this.mID = mID;
    }
    @XmlTransient
    public String gettID() {
        return tID;
    }

    public void settID(String tID) {
        this.tID = tID;
    }
    @XmlTransient
    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }
    @XmlTransient
    public String getoNO() {
        return oNO;
    }

    public void setoNO(String oNO) {
        this.oNO = oNO;
    }
    @XmlTransient
    public String getAccIdTo() {
        return accIdTo;
    }

    public void setAccIdTo(String accIdTo) {
        this.accIdTo = accIdTo;
    }
    @XmlTransient
    public String getCurAmt() {
        return curAmt;
    }

    public void setCurAmt(String curAmt) {
        this.curAmt = curAmt;
    }
    @XmlTransient
    public String getTrnDt() {
        return trnDt;
    }

    public void setTrnDt(String trnDt) {
        this.trnDt = trnDt;
    }
    @XmlTransient
    public String getTrnTime() {
        return trnTime;
    }

    public void setTrnTime(String trnTime) {
        this.trnTime = trnTime;
    }
    @XmlTransient
    public String getBankIdFrom() {
        return bankIdFrom;
    }

    public void setBankIdFrom(String bankIdFrom) {
        this.bankIdFrom = bankIdFrom;
    }
    @XmlTransient
    public String getAcctIdFrom() {
        return acctIdFrom;
    }

    public void setAcctIdFrom(String acctIdFrom) {
        this.acctIdFrom = acctIdFrom;
    }
    @XmlTransient
    public String getDueDt() {
        return dueDt;
    }

    public void setDueDt(String dueDt) {
        this.dueDt = dueDt;
    }
    @XmlTransient
    public String getTxnSeqNo() {
        return txnSeqNo;
    }

    public void setTxnSeqNo(String txnSeqNo) {
        this.txnSeqNo = txnSeqNo;
    }
    @XmlTransient
    public String getPayTxnFee() {
        return payTxnFee;
    }

    public void setPayTxnFee(String payTxnFee) {
        this.payTxnFee = payTxnFee;
    }
    @XmlTransient
    public String getmAC() {
        return mAC;
    }

    public void setmAC(String mAC) {
        this.mAC = mAC;
    }

    @Override
    public String toString() {
        return "TOBwebATMCreateOrderResponseData{" +
                "rC='" + rC + '\'' +
                ", mSG='" + mSG + '\'' +
                ", sendSeqNo='" + sendSeqNo + '\'' +
                ", mID='" + mID + '\'' +
                ", tID='" + tID + '\'' +
                ", txnType='" + txnType + '\'' +
                ", oNO='" + oNO + '\'' +
                ", accIdTo='" + accIdTo + '\'' +
                ", curAmt='" + curAmt + '\'' +
                ", trnDt='" + trnDt + '\'' +
                ", trnTime='" + trnTime + '\'' +
                ", bankIdFrom='" + bankIdFrom + '\'' +
                ", acctIdFrom='" + acctIdFrom + '\'' +
                ", dueDt='" + dueDt + '\'' +
                ", txnSeqNo='" + txnSeqNo + '\'' +
                ", payTxnFee='" + payTxnFee + '\'' +
                ", mAC='" + mAC + '\'' +
                '}';
    }
}
