package co.intella.domain.tobWebAtm;

import com.fasterxml.jackson.annotation.JsonInclude;


public class TOBwebATMCreateOrderRequestData {

    private String sendSeqNo;
    private String mID ;
    private String tID ;
    private String txnType ;
    private String userData ;
    private String oNO ;
    private String accIdTo ;
    private Integer curAmt  ;
    private String mAC ;
    private String rsURL ;

    public String getSendSeqNo() {
        return sendSeqNo;
    }

    public void setSendSeqNo(String sendSeqNo) {
        this.sendSeqNo = sendSeqNo;
    }

    public String getmID() {
        return mID;
    }

    public void setmID(String mID) {
        this.mID = mID;
    }

    public String gettID() {
        return tID;
    }

    public void settID(String tID) {
        this.tID = tID;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getUserData() {
        return userData;
    }

    public void setUserData(String userData) {
        this.userData = userData;
    }

    public String getoNO() {
        return oNO;
    }

    public void setoNO(String oNO) {
        this.oNO = oNO;
    }

    public String getAccIdTo() {
        return accIdTo;
    }

    public void setAccIdTo(String accIdTo) {
        this.accIdTo = accIdTo;
    }

    public Integer getCurAmt() {
        return curAmt;
    }

    public void setCurAmt(Integer curAmt) {
        this.curAmt = curAmt;
    }

    public String getmAC() {
        return mAC;
    }

    public void setmAC(String mAC) {
        this.mAC = mAC;
    }

    public String getRsURL() {
        return rsURL;
    }

    public void setRsURL(String rsURL) {
        this.rsURL = rsURL;
    }

    @Override
    public String toString() {

        return  "<?xml version=\"1.0\" encoding=\"Big5\"?>"+
                "<CardPayRq>"+
                "<SendSeqNo>"+sendSeqNo+"</SendSeqNo>"+
                "<MID>"+mID+"</MID>"+
                "<TID>"+tID+"</TID>"+
                "<TxnType>"+txnType+"</TxnType>"+
                "<UserData>"+userData+"</UserData>"+
                "<ONO>"+oNO+"</ONO>"+
                "<AccIdTo>"+accIdTo+"</AccIdTo>"+
                "<CurAmt>"+curAmt+"</CurAmt>"+
                "<MAC>"+mAC+"</MAC>"+
                "<RsURL>"+rsURL+"</RsURL>"+
                "</CardPayRq>";



//        return "TOBwebATMCreateOrderRequestData{" +
//                "sendSeqNo='" + sendSeqNo + '\'' +
//                ", mID='" + mID + '\'' +
//                ", tID='" + tID + '\'' +
//                ", txnType='" + txnType + '\'' +
//                ", userData='" + userData + '\'' +
//                ", oNO='" + oNO + '\'' +
//                ", accIdTo='" + accIdTo + '\'' +
//                ", curAmt=" + curAmt +
//                ", mAC='" + mAC + '\'' +
//                ", rsURL='" + rsURL + '\'' +
//                '}';
    }
}

