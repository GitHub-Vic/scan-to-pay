package co.intella.domain.tobWebAtm;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "RsXMLData", namespace = "http://www.cedar.com.tw/bluestar/")
public class TOBwebATMNotifyData {
    @XmlElement(name = "TrnDt")
    private String trnDt;
    @XmlElement(name = "TrnTime")
    private String trnTime;
    @XmlElement(name = "RCPTId")
    private String rCPTId;
    @XmlElement(name = "CurAmt")
    private String curAmt;
    @XmlElement(name = "Code")
    private String code;
    @XmlElement(name = "UserData")
    private String userData;
    @XmlElement(name = "CName")
    private String cName;
    @XmlElement(name = "PName")
    private String pName;
    @XmlElement(name = "TrnType")
    private String trnType;
    @XmlElement(name = "SITDate")
    private String sITDate;
    @XmlElement(name = "CLLBR")
    private String cLLBR;
    @XmlTransient
    public String getTrnDt() {
        return trnDt;
    }

    public void setTrnDt(String trnDt) {
        this.trnDt = trnDt;
    }
    @XmlTransient
    public String getTrnTime() {
        return trnTime;
    }

    public void setTrnTime(String trnTime) {
        this.trnTime = trnTime;
    }
    @XmlTransient
    public String getrCPTId() {
        return rCPTId;
    }

    public void setrCPTId(String rCPTId) {
        this.rCPTId = rCPTId;
    }
    @XmlTransient
    public String getCurAmt() {
        return curAmt;
    }

    public void setCurAmt(String curAmt) {
        this.curAmt = curAmt;
    }
    @XmlTransient
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    @XmlTransient
    public String getUserData() {
        return userData;
    }

    public void setUserData(String userData) {
        this.userData = userData;
    }
    @XmlTransient
    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }
    @XmlTransient
    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }
    @XmlTransient
    public String getTrnType() {
        return trnType;
    }

    public void setTrnType(String trnType) {
        this.trnType = trnType;
    }
    @XmlTransient
    public String getsITDate() {
        return sITDate;
    }

    public void setsITDate(String sITDate) {
        this.sITDate = sITDate;
    }
    @XmlTransient
    public String getcLLBR() {
        return cLLBR;
    }

    public void setcLLBR(String cLLBR) {
        this.cLLBR = cLLBR;
    }

    @Override
    public String toString() {
        return "TOBwebATMNotifyData{" +
                "trnDt='" + trnDt + '\'' +
                ", trnTime='" + trnTime + '\'' +
                ", rCPTId='" + rCPTId + '\'' +
                ", curAmt='" + curAmt + '\'' +
                ", code='" + code + '\'' +
                ", userData='" + userData + '\'' +
                ", cName='" + cName + '\'' +
                ", pName='" + pName + '\'' +
                ", trnType='" + trnType + '\'' +
                ", sITDate='" + sITDate + '\'' +
                ", cLLBR='" + cLLBR + '\'' +
                '}';
    }
}
