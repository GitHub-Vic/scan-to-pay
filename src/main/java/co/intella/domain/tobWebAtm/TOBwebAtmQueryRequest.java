package co.intella.domain.tobWebAtm;

public class TOBwebAtmQueryRequest {
    private String sendSeqNo;
    private String mID;
    private String mAC;

    public TOBwebAtmQueryRequest() {
        this.sendSeqNo = sendSeqNo;
        this.mID = mID;
        this.mAC = mAC;
    }

    public TOBwebAtmQueryRequest(String sendSeqNo, String mID, String mAC) {
        this.sendSeqNo = sendSeqNo;
        this.mID = mID;
        this.mAC = mAC;
    }

    public String getSendSeqNo() {
        return sendSeqNo;
    }

    public void setSendSeqNo(String sendSeqNo) {
        this.sendSeqNo = sendSeqNo;
    }

    public String getmID() {
        return mID;
    }

    public void setmID(String mID) {
        this.mID = mID;
    }

    public String getmAC() {
        return mAC;
    }

    public void setmAC(String mAC) {
        this.mAC = mAC;
    }


    @Override
    public String toString() {
        return "<?xml version=\"1.0\" encoding=\"Big5\"?>" +
                "<CardPayInqRq>" +
                "<SendSeqNo>" + sendSeqNo + "</SendSeqNo>" +
                "<MID>" + mID + "</MID>" +
                "<MAC>" + mAC + "</MAC>" +
                "</CardPayInqRq>";
    }
}
