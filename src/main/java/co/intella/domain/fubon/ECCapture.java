package co.intella.domain.fubon;

import co.intella.constant.ServiceType;
import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import org.apache.commons.lang3.StringUtils;

import java.text.DecimalFormat;

public class ECCapture {

    private String txCode;

    private String permitCode;
    private String merchantID;
    private String terminalID;
    private String orderID;
    private String transCode;
    private String transDate;
    private String transTime;
    private String approveCode;
    private String transAmt;

    private String resultCode;
    private String message;

    public ECCapture() {

    }

    public ECCapture(PaymentAccount paymentAccount, TradeDetail tradeDetail, String methodStr) {

        this.setTxCode("ECCapture");
        this.setPermitCode(StringUtils.defaultIfBlank(paymentAccount.getHashKey(), "!QE#3456"));
        this.setMerchantID(paymentAccount.getAccount());
        this.setTerminalID(paymentAccount.getAccountTerminalId());
        this.setOrderID(tradeDetail.getOrderId());
        this.setTransCode(ServiceType.REFUND.equals(methodStr) ? "01" : "00");

        String[] platformPaidDates = tradeDetail.getPlatformPaidDate().split(" ");
        this.setTransDate(platformPaidDates[0]);
        this.setTransTime(platformPaidDates[1]);

        this.setApproveCode(tradeDetail.getTxParams());
        this.setTransAmt(new DecimalFormat("########.00").format(tradeDetail.getPayment()));    //會四捨五入
    }

    public String getTxCode() {
        return txCode;
    }

    public void setTxCode(String txCode) {
        this.txCode = txCode;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getTransCode() {
        return transCode;
    }

    public void setTransCode(String transCode) {
        this.transCode = transCode;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getTransTime() {
        return transTime;
    }

    public void setTransTime(String transTime) {
        this.transTime = transTime;
    }

    public String getApproveCode() {
        return approveCode;
    }

    public void setApproveCode(String approveCode) {
        this.approveCode = approveCode;
    }

    public String getTransAmt() {
        return transAmt;
    }

    public void setTransAmt(String transAmt) {
        this.transAmt = transAmt;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPermitCode() {
        return permitCode;
    }

    public void setPermitCode(String permitCode) {
        this.permitCode = permitCode;
    }
}
