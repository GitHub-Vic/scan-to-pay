package co.intella.domain.fubon;

import com.google.gson.annotations.SerializedName;

public class FuBonParamBean {
	private String messageType = "";
	private String merchantId = "";
	private String subMID = "";
	private String terminalId = "";
	private String acquirerId = "";
	private String orderId = "";
	private String sysOrderId = "";
	private String pan = "";
	private String cvv2 = "";
	private String expireDate = "";
	private String transCode = "";
	private String transDate = "";
	private String transTime = "";
	private String userData = "";
	private String userDefine = "";
	private String currencyCode = "";
	private double transAmt = 0.0D;
	private String email = "";
	private String socialId = "";
	private String billMessage = "";
	private int transMode = 0;
	private int installCount = 0;
	private String installType = "";
	private double firstAmt = 0.0D;
	private double eachAmt = 0.0D;
	private double fee = 0.0D;
	private String redemType = "";
	private int redemUsed = 0;
	private int redemBalance = 0;
	private double creditAmt = 0.0D;
	@SerializedName(value = "resCode", alternate = {"responseCode"})
	private String resCode = "";
	@SerializedName(value = "resMsg", alternate = {"responseMsg"})
	private String resMsg = "";
	private String approveCode = "";
	private String batchNo = "";
	private String reversalFlag = "";
	private String direction = "";
	private String rrn = "";
	private String transType = "";
	private String payPage = "";
	private String notifyURL = "";
	private String notifyParam = "";
	private String notifyType = "";
	private String cup_notifyURL = "";
	private String cup_notifyParam = "";
	private String mobilePayFlag = "N";
	private String mobilePayTx = "";
	private String mobilePayInfoA = "";
	private String mobilePayInfoB = "";
	private String mobilePayCrypto = "";
	private String mobilePayECI = "";
	private String mobilePayWallet = "";
	@SerializedName(value = "mPermitCode", alternate = {"permitCode"})
	private String mPermitCode = "";

	public String getTranstype() {
		return this.transType;
	}

	public String getTransType() {
		return this.transType;
	}

	public void setTransType(String paramString) {
		this.transType = paramString;
	}

	public void setRRN(String paramString) {
		this.rrn = paramString;
	}

	public String getRRN() {
		return this.rrn;
	}

	public void setDirection(String paramString) {
		this.direction = paramString;
	}

	public String getDirection() {
		return this.direction;
	}

	public void setReversalFlag(String paramString) {
		this.reversalFlag = paramString;
	}

	public String getReversalFlag() {
		return this.reversalFlag;
	}

	public void setResponseCode(String paramString) {
		this.resCode = paramString;
	}

	public String getResponseCode() {
		return this.resCode;
	}

	public void setResponseMsg(String paramString) {
		this.resMsg = paramString;
	}

	public String getResponseMsg() {
		return this.resMsg;
	}

	public void setApproveCode(String paramString) {
		this.approveCode = paramString;
	}

	public String getApproveCode() {
		return this.approveCode;
	}

	public void setBatchNo(String paramString) {
		this.batchNo = paramString;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setMessageType(String paramString) {
		this.messageType = paramString;
	}

	public void setMerchantID(String paramString) {
		this.merchantId = paramString;
	}

	public void setSubMID(String paramString) {
		this.subMID = paramString;
	}

	public void setTerminalID(String paramString) {
		this.terminalId = paramString;
	}

	public void setAcquirerID(String paramString) {
		this.acquirerId = paramString;
	}

	public void setOrderID(String paramString) {
		this.orderId = paramString;
	}

	public void setSysOrderID(String paramString) {
		this.sysOrderId = paramString;
	}

	public void setPAN(String paramString) {
		this.pan = paramString;
	}

	public void setCVV2(String paramString) {
		this.cvv2 = paramString;
	}

	public void setExpireDate(String paramString) {
		this.expireDate = paramString;
	}

	public void setTransCode(String paramString) {
		this.transCode = paramString;
	}

	public void setTransDate(String paramString) {
		this.transDate = paramString;
	}

	public void setTransTime(String paramString) {
		this.transTime = paramString;
	}

	public void setUserData(String paramString) {
		this.userData = paramString;
	}

	public void setUserDefine(String paramString) {
		this.userDefine = paramString;
	}

	public void setCurrencyCode(String paramString) {
		this.currencyCode = paramString;
	}

	public void setTransAmt(double paramDouble) {
		this.transAmt = paramDouble;
	}

	public void setEmail(String paramString) {
		this.email = paramString;
	}

	public void setSocialID(String paramString) {
		this.socialId = paramString;
	}

	public void setBillMessage(String paramString) {
		this.billMessage = paramString;
	}

	public void setTransMode(int paramInt) {
		this.transMode = paramInt;
	}

	public void setInstallCount(int paramInt) {
		this.installCount = paramInt;
	}

	public void setInstallType(String paramString) {
		this.installType = paramString;
	}

	public void setFirstAmt(double paramDouble) {
		this.firstAmt = paramDouble;
	}

	public void setEachAmt(double paramDouble) {
		this.eachAmt = paramDouble;
	}

	public void setFee(double paramDouble) {
		this.fee = paramDouble;
	}

	public void setRedemType(String paramString) {
		this.redemType = paramString;
	}

	public void setRedemUsed(int paramInt) {
		this.redemUsed = paramInt;
	}

	public void setRedemBalance(int paramInt) {
		this.redemBalance = paramInt;
	}

	public void setCreditAmt(double paramDouble) {
		this.creditAmt = paramDouble;
	}

	public void setPayPage(String paramString) {
		this.payPage = paramString;
	}

	public String getMessageType() {
		return this.messageType;
	}

	public String getMerchantID() {
		return this.merchantId;
	}

	public String getSubMID() {
		return this.subMID;
	}

	public String getTerminalID() {
		return this.terminalId;
	}

	public String getAcquirerID() {
		return this.acquirerId;
	}

	public String getOrderID() {
		return this.orderId;
	}

	public String getSysOrderID() {
		return this.sysOrderId;
	}

	public String getPAN() {
		return this.pan;
	}

	public String getCVV2() {
		return this.cvv2;
	}

	public String getExpireDate() {
		return this.expireDate;
	}

	public String getTransCode() {
		return this.transCode;
	}

	public String getTransDate() {
		return this.transDate;
	}

	public String getTransTime() {
		return this.transTime;
	}

	public String getUserData() {
		return this.userData;
	}

	public String getUserDefine() {
		return this.userDefine;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public double getTransAmt() {
		return this.transAmt;
	}

	public String getEmail() {
		return this.email;
	}

	public String getSocialID() {
		return this.socialId;
	}

	public String getBillMessage() {
		return this.billMessage;
	}

	public int getTransMode() {
		return this.transMode;
	}

	public int getInstallCount() {
		return this.installCount;
	}

	public String getInstallType() {
		return this.installType;
	}

	public double getFirstAmt() {
		return this.firstAmt;
	}

	public double getEachAmt() {
		return this.eachAmt;
	}

	public double getFee() {
		return this.fee;
	}

	public String getRedemType() {
		return this.redemType;
	}

	public int getRedemUsed() {
		return this.redemUsed;
	}

	public int getRedemBalance() {
		return this.redemBalance;
	}

	public double getCreditAmt() {
		return this.creditAmt;
	}

	public String getPayPage() {
		return this.payPage;
	}

	public String getNotifyURL() {
		return this.notifyURL;
	}

	public void setNotifyURL(String paramString) {
		this.notifyURL = paramString;
	}

	public String getNotifyParam() {
		return this.notifyParam;
	}

	public void setNotifyParam(String paramString) {
		this.notifyParam = paramString;
	}

	public String getNotifyType() {
		return this.notifyType;
	}

	public void setNotifyType(String paramString) {
		this.notifyType = paramString;
	}

	public String getCUPNotifyURL() {
		return this.cup_notifyURL;
	}

	public void setCUPNotifyURL(String paramString) {
		this.cup_notifyURL = paramString;
	}

	public String getCUPNotifyParam() {
		return this.cup_notifyParam;
	}

	public void setCUPNotifyParam(String paramString) {
		this.cup_notifyParam = paramString;
	}

	public String getMobilePayFlag() {
		return this.mobilePayFlag;
	}

	public void setMobilePayFlag(String paramString) {
		this.mobilePayFlag = paramString;
	}

	public String getMobilePayTx() {
		return this.mobilePayTx;
	}

	public void setMobilePayTx(String paramString) {
		this.mobilePayTx = paramString;
	}

	public String getMobilePayInfoA() {
		return this.mobilePayInfoA;
	}

	public void setMobilePayInfoA(String paramString) {
		this.mobilePayInfoA = paramString;
	}

	public String getMobilePayInfoB() {
		return this.mobilePayInfoB;
	}

	public void setMobilePayInfoB(String paramString) {
		this.mobilePayInfoB = paramString;
	}

	public String getMobilePayCrypto() {
		return this.mobilePayCrypto;
	}

	public void setMobilePayCrypto(String paramString) {
		this.mobilePayCrypto = paramString;
	}

	public String getMobilePayECI() {
		return this.mobilePayECI;
	}

	public void setMobilePayECI(String paramString) {
		this.mobilePayECI = paramString;
	}

	public String getMobilePayWallet() {
		return this.mobilePayWallet;
	}

	public void setMobilePayWallet(String paramString) {
		this.mobilePayWallet = paramString;
	}

	public String getPermitCode() {
		return this.mPermitCode;
	}

	public void setPermitCode(String paramString) {
		this.mPermitCode = paramString;
	}

	@Override
	public String toString() {
		return "FuBonParamBean [messageType=" + messageType + ", merchantId=" + merchantId + ", subMID=" + subMID
				+ ", terminalId=" + terminalId + ", acquirerId=" + acquirerId + ", orderId=" + orderId + ", sysOrderId="
				+ sysOrderId + ", pan=" + pan + ", cvv2=" + cvv2 + ", expireDate=" + expireDate + ", transCode="
				+ transCode + ", transDate=" + transDate + ", transTime=" + transTime + ", userData=" + userData
				+ ", userDefine=" + userDefine + ", currencyCode=" + currencyCode + ", transAmt=" + transAmt
				+ ", email=" + email + ", socialId=" + socialId + ", billMessage=" + billMessage + ", transMode="
				+ transMode + ", installCount=" + installCount + ", installType=" + installType + ", firstAmt="
				+ firstAmt + ", eachAmt=" + eachAmt + ", fee=" + fee + ", redemType=" + redemType + ", redemUsed="
				+ redemUsed + ", redemBalance=" + redemBalance + ", creditAmt=" + creditAmt + ", resCode=" + resCode
				+ ", resMsg=" + resMsg + ", approveCode=" + approveCode + ", batchNo=" + batchNo + ", reversalFlag="
				+ reversalFlag + ", direction=" + direction + ", rrn=" + rrn + ", transType=" + transType + ", payPage="
				+ payPage + ", notifyURL=" + notifyURL + ", notifyParam=" + notifyParam + ", notifyType=" + notifyType
				+ ", cup_notifyURL=" + cup_notifyURL + ", cup_notifyParam=" + cup_notifyParam + ", mobilePayFlag="
				+ mobilePayFlag + ", mobilePayTx=" + mobilePayTx + ", mobilePayInfoA=" + mobilePayInfoA
				+ ", mobilePayInfoB=" + mobilePayInfoB + ", mobilePayCrypto=" + mobilePayCrypto + ", mobilePayECI="
				+ mobilePayECI + ", mobilePayWallet=" + mobilePayWallet + ", mPermitCode=" + mPermitCode + "]";
	}
	
}
