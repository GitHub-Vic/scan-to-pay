package co.intella.domain.fubon;

import co.intella.model.PaymentAccount;
import co.intella.utility.MD5Util;
import com.google.gson.annotations.SerializedName;
import org.apache.tomcat.util.security.ConcurrentMessageDigest;
import org.apache.tomcat.util.security.MD5Encoder;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ChgPermit {
    @SerializedName("permitCode")
    private String permitCode;
    @SerializedName("permitCodeNew")
    private String permitCodeNew;
    @SerializedName("merchantID")
    private String merchantID;
    @SerializedName("resultCode")
    private String resultCode;
    @SerializedName("message")
    private String message;
    public ChgPermit() {

    }
    public ChgPermit(String testMode, PaymentAccount paymentAccount) {
        this.setPermitCode("0".equals(testMode) ? " " : paymentAccount.getAccount());
        this.setMerchantID(paymentAccount.getAccount());

//        String permitCodeStr = "intellaScan2pay" + paymentAccount.getAccount() ;
//        String permitCodeMD5 = MD5Util.getMD5(permitCodeStr).toLowerCase();
        this.setPermitCodeNew("!QE#3456");
    }

    public String getPermitCode() {
        return permitCode;
    }

    public void setPermitCode(String permitCode) {
        this.permitCode = permitCode;
    }

    public String getPermitCodeNew() {
        return permitCodeNew;
    }

    public void setPermitCodeNew(String permitCodeNew) {
        this.permitCodeNew = permitCodeNew;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
