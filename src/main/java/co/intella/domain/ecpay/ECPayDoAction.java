package co.intella.domain.ecpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class ECPayDoAction {

    @JsonProperty("MerchantID")
    @SerializedName("MerchantID")
    private String merchantId;

    @JsonProperty("MerchantTradeNo")
    @SerializedName("StoreOrderNo")
    private String merchantTradeNo;

    @JsonProperty("TradeNo")
    @SerializedName("SystemOrderId")
    private String tradeNo;

    @JsonProperty("Action")
    @SerializedName("Action")
    private String action;

    @JsonProperty("TotalAmount")
    @SerializedName("TotalFee")
    private String totalAmount;

    @JsonProperty("CheckMacValue")
    @SerializedName("CheckMacValue")
    private String checkMacValue;

    @JsonProperty("PlatformID")
    @SerializedName("PlatformID")
    private String platformID;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCheckMacValue() {
        return checkMacValue;
    }

    public void setCheckMacValue(String checkMacValue) {
        this.checkMacValue = checkMacValue;
    }

    public String getPlatformID() {
        return platformID;
    }

    public void setPlatformID(String platformID) {
        this.platformID = platformID;
    }
}
