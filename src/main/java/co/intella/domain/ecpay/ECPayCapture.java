package co.intella.domain.ecpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class ECPayCapture {

    @JsonProperty("MerchantID")
    @SerializedName("MerchantID")
    private String merchantId;

    @JsonProperty("MerchantTradeNo")
    @SerializedName("MerchantTradeNo")
    private String merchantTradeNo;

    @SerializedName("CheckMacValue")
    private String checkMacValue;

    @JsonProperty("CaptureAMT")
    @SerializedName("CaptureAMT")
    private String captureAMT;

    @JsonProperty("UserRefundAMT")
    @SerializedName("UserRefundAMT")
    private String userRefundAMT;

    @JsonProperty("PlatformID")
    @SerializedName("PlatformID")
    private String platformID;

    @JsonProperty("Remark")
    @SerializedName("Remark")
    private String remark;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getCheckMacValue() {
        return checkMacValue;
    }

    public void setCheckMacValue(String checkMacValue) {
        this.checkMacValue = checkMacValue;
    }

    public String getCaptureAMT() {
        return captureAMT;
    }

    public void setCaptureAMT(String captureAMT) {
        this.captureAMT = captureAMT;
    }

    public String getUserRefundAMT() {
        return userRefundAMT;
    }

    public void setUserRefundAMT(String userRefundAMT) {
        this.userRefundAMT = userRefundAMT;
    }

    public String getPlatformID() {
        return platformID;
    }

    public void setPlatformID(String platformID) {
        this.platformID = platformID;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
