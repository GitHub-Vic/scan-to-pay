package co.intella.domain.ecpay;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class ECPayOrderResponse {

    @SerializedName("MerchantID")
    private String merchantId;

    @SerializedName("MerchantTradeNo")
    private String merchantTradeNo;

    @SerializedName("SPToken")
    private String spToken;

    @SerializedName("gwsr")
    private String gwsr;

    @SerializedName("RtnCode")
    private int returnCode;

    @SerializedName("RtnMsg")
    private String returnMessage;

    @SerializedName("CheckMacValue")
    private String checkMacValue;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getSpToken() {
        return spToken;
    }

    public void setSpToken(String spToken) {
        this.spToken = spToken;
    }

    public String getGwsr() {
        return gwsr;
    }

    public void setGwsr(String gwsr) {
        this.gwsr = gwsr;
    }

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public String getCheckMacValue() {
        return checkMacValue;
    }

    public void setCheckMacValue(String checkMacValue) {
        this.checkMacValue = checkMacValue;
    }
}
