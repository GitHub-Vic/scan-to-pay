package co.intella.domain.ecpay;

import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class ECPayQueryResponse {

    @SerializedName("MerchantID")
    private String merchantId;

    @SerializedName("MerchantTradeNo")
    private String merchantTradeNo;

    @SerializedName("StoreID")
    private String storeID;

    @SerializedName("TradeNo")
    private String tradeNo;

    @SerializedName("TradeAmt")
    private String tradeAmt;

    @SerializedName("PaymentDate")
    private String paymentDate;

    @SerializedName("PaymentType")
    private String paymentType;

    @SerializedName("HandlingCharge")
    private String handlingCharge;

    @SerializedName("PaymentTypeChargeFee")
    private String paymentTypeChargeFee;

    @SerializedName("TradeDate")
    private String tradeDate;

    @SerializedName("TradeStatus")
    private String tradeStatus;

    @SerializedName("ItemName")
    private String itemName;

    @SerializedName("CustomField1")
    private String customField1;

    @SerializedName("CustomField2")
    private String customField2;

    @SerializedName("CustomField3")
    private String customField3;

    @SerializedName("CustomField4")
    private String customField4;

    @SerializedName("CheckMacValue")
    private String checkMacValue;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getStoreID() {
        return storeID;
    }

    public void setStoreID(String storeID) {
        this.storeID = storeID;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public String getTradeAmt() {
        return tradeAmt;
    }

    public void setTradeAmt(String tradeAmt) {
        this.tradeAmt = tradeAmt;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getHandlingCharge() {
        return handlingCharge;
    }

    public void setHandlingCharge(String handlingCharge) {
        this.handlingCharge = handlingCharge;
    }

    public String getPaymentTypeChargeFee() {
        return paymentTypeChargeFee;
    }

    public void setPaymentTypeChargeFee(String paymentTypeChargeFee) {
        this.paymentTypeChargeFee = paymentTypeChargeFee;
    }

    public String getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }

    public String getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    public String getCheckMacValue() {
        return checkMacValue;
    }

    public void setCheckMacValue(String checkMacValue) {
        this.checkMacValue = checkMacValue;
    }
}
