package co.intella.domain.ecpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class ECPayOrder {

    @JsonProperty("MerchantID")
    @SerializedName("MerchantID")
    private String merchantId;

    @JsonProperty("MerchantTradeNo")
    @SerializedName("StoreOrderNo")
    private String merchantTradeNo;

    @JsonProperty("StoreID")
    @SerializedName("StoreInfo")
    private String storeId;

    // yyyy/MM/dd HH:mm:ss
    @JsonProperty("MerchantTradeDate")
    @SerializedName("MerchantTradeDate")
    private String merchantTradeDate;

    @JsonProperty("PaymentType")
    @SerializedName("PaymentType")
    private String paymentType;

    @JsonProperty("TotalAmount")
    @SerializedName("TotalFee")
    private String totalAmount;

    @JsonProperty("TradeDesc")
    @SerializedName("Detail")
    private String tradeDesc;

    @JsonProperty("ItemName")
    @SerializedName("Body")
    private String itemName;

    @JsonProperty("ReturnURL")
    @SerializedName("ReturnURL")
    private String returnURL;

    @JsonProperty("ChoosePayment")
    @SerializedName("TransMode")
    private String choosePayment;

    @JsonProperty("CheckMacValue")
    @SerializedName("CheckMacValue")
    private String checkMacValue;

    @JsonProperty("NeedExtraPaidInfo")
    @SerializedName("NeedExtraPaidInfo")
    private String needExtraPaidInfo;

    @JsonProperty("PlatformID")
    @SerializedName("PlatformID")
    private String platformId;

    @JsonProperty("InvoiceMark")
    @SerializedName("InvoiceMark")
    private String invoiceMark;

    @JsonProperty("HoldTradeAMT")
    @SerializedName("HoldTradeAMT")
    private String holdTradeAMT;

    @JsonProperty("CustomField1")
    @SerializedName("CustomField1")
    private String customField1;

    @JsonProperty("CustomField2")
    @SerializedName("CustomField2")
    private String customField2;

    @JsonProperty("CustomField3")
    @SerializedName("CustomField3")
    private String customField3;

    @JsonProperty("CustomField4")
    @SerializedName("CustomField4")
    private String customField4;

    @JsonProperty("EncryptType")
    @SerializedName("EncryptType")
    private String encryptType;

    //ATM
    @JsonProperty("ExpireDate")
    @SerializedName("ExpireDate")
    private String expireDate;

    @JsonProperty("PaymentInfoURL")
    @SerializedName("PaymentInfoURL")
    private String paymentInfoURL;

    //CVS
    @JsonProperty("Desc_1")
    @SerializedName("Desc_1")
    private String desc1;

    @JsonProperty("Desc_2")
    @SerializedName("Desc_2")
    private String desc2;

    @JsonProperty("Desc_3")
    @SerializedName("Desc_3")
    private String desc3;

    @JsonProperty("Desc_4")
    @SerializedName("Desc_4")
    private String desc4;

    @JsonProperty("StoreExpireDate")
    @SerializedName("StoreExpireDate")
    private String storeExpireDate;

    //Credit
    @JsonProperty("BindingCard")
    @SerializedName("BindingCard")
    private String bindingCard;

    @JsonProperty("MerchantMemberID")
    @SerializedName("MerchantMemberID")
    private String merchantMemberId;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getMerchantTradeDate() {
        return merchantTradeDate;
    }

    public void setMerchantTradeDate(String merchantTradeDate) {
        this.merchantTradeDate = merchantTradeDate;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTradeDesc() {
        return tradeDesc;
    }

    public void setTradeDesc(String tradeDesc) {
        this.tradeDesc = tradeDesc;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getReturnURL() {
        return returnURL;
    }

    public void setReturnURL(String returnURL) {
        this.returnURL = returnURL;
    }

    public String getChoosePayment() {
        return choosePayment;
    }

    public void setChoosePayment(String choosePayment) {
        this.choosePayment = choosePayment;
    }

    public String getCheckMacValue() {
        return checkMacValue;
    }

    public void setCheckMacValue(String checkMacValue) {
        this.checkMacValue = checkMacValue;
    }

    public String getNeedExtraPaidInfo() {
        return needExtraPaidInfo;
    }

    public void setNeedExtraPaidInfo(String needExtraPaidInfo) {
        this.needExtraPaidInfo = needExtraPaidInfo;
    }

    public String getPlatformId() {
        return platformId;
    }

    public void setPlatformId(String platformId) {
        this.platformId = platformId;
    }

    public String getInvoiceMark() {
        return invoiceMark;
    }

    public void setInvoiceMark(String invoiceMark) {
        this.invoiceMark = invoiceMark;
    }

    public String getHoldTradeAMT() {
        return holdTradeAMT;
    }

    public void setHoldTradeAMT(String holdTradeAMT) {
        this.holdTradeAMT = holdTradeAMT;
    }

    public String getEncryptType() {
        return encryptType;
    }

    public void setEncryptType(String encryptType) {
        this.encryptType = encryptType;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getPaymentInfoURL() {
        return paymentInfoURL;
    }

    public void setPaymentInfoURL(String paymentInfoURL) {
        this.paymentInfoURL = paymentInfoURL;
    }

    public String getStoreExpireDate() {
        return storeExpireDate;
    }

    public void setStoreExpireDate(String storeExpireDate) {
        this.storeExpireDate = storeExpireDate;
    }

    public String getBindingCard() {
        return bindingCard;
    }

    public void setBindingCard(String bindingCard) {
        this.bindingCard = bindingCard;
    }

    public String getMerchantMemberId() {
        return merchantMemberId;
    }

    public void setMerchantMemberId(String merchantMemberId) {
        this.merchantMemberId = merchantMemberId;
    }

    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    public String getDesc1() {
        return desc1;
    }

    public void setDesc1(String desc1) {
        this.desc1 = desc1;
    }

    public String getDesc2() {
        return desc2;
    }

    public void setDesc2(String desc2) {
        this.desc2 = desc2;
    }

    public String getDesc3() {
        return desc3;
    }

    public void setDesc3(String desc3) {
        this.desc3 = desc3;
    }

    public String getDesc4() {
        return desc4;
    }

    public void setDesc4(String desc4) {
        this.desc4 = desc4;
    }
}