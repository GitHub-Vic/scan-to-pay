package co.intella.domain.ecpay;

import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class ECPayCaptureResponse {

    @SerializedName("MerchantID")
    private String merchantId;

    @SerializedName("MerchantTradeNo")
    private String merchantTradeNo;

    @SerializedName("TradeNo")
    private String tradeNo;

    @SerializedName("RtnCode")
    private int returnCode;

    @SerializedName("RtnMsg")
    private String returnMessage;

    @SerializedName("AllocationDate")
    private String allocationDate;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public String getAllocationDate() {
        return allocationDate;
    }

    public void setAllocationDate(String allocationDate) {
        this.allocationDate = allocationDate;
    }
}
