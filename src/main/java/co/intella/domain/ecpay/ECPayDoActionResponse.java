package co.intella.domain.ecpay;

import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class ECPayDoActionResponse {

    @SerializedName("MerchantID")
    private String merchantId;

    @SerializedName("MerchantTradeNo")
    private String merchantTradeNo;

    @SerializedName("TradeNo")
    private String tradeNo;

    @SerializedName("RtnCode")
    private String rtnCode;

    @SerializedName("RtnMsg")
    private String rtnMsg;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public String getRtnCode() {
        return rtnCode;
    }

    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }
}
