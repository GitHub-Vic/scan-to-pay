package co.intella.domain;

public class PaymentResponse {

	public PaymentResponseHeader header;
	public PaymentResponseData data;

	public PaymentResponse() {
		super();
		this.header = new PaymentResponseHeader();
		this.data = new PaymentResponseData();
	}

	public PaymentResponse(PaymentResponseHeader header, PaymentResponseData data) {
		super();
		this.header = header;
		this.data = data;
	}

	public PaymentResponseHeader getHeader() {
		return header;
	}

	public void setHeader(PaymentResponseHeader header) {
		this.header = header;
	}

	public PaymentResponseData getData() {
		return data;
	}

	public void setData(PaymentResponseData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "PaymentResponse [header=" + header + ", data=" + data + "]";
	}
	
}
