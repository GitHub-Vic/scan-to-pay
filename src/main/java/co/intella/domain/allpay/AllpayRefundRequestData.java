package co.intella.domain.allpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class AllpayRefundRequestData extends AllpayBasicRequestData {

    @SerializedName("StoreOrderNo")
    @JsonProperty("MerchantTradeNo")
    private String merchantTradeNo;

    @SerializedName("StoreInfo")
    @JsonProperty("StoreID")
    private String storeId;

    @SerializedName("DeviceInfo")
    @JsonProperty("PosID")
    private String posId;

    @SerializedName("AuthCode")
    @JsonProperty("Barcode")
    private String barcode;

    @SerializedName("AllpayTradeNo")
    @JsonProperty("AllpayTradeNo")
    private String allpayTradeNo;

    @SerializedName("RefundFee")
    @JsonProperty("RefundAmount")
    private String refundAmount;

    @JsonProperty("CheckMacValue")
    private String checkMacValue;


    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getPosId() {
        return posId;
    }

    public void setPosId(String posId) {
        this.posId = posId;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getAllpayTradeNo() {
        return allpayTradeNo;
    }

    public void setAllpayTradeNo(String allpayTradeNo) {
        this.allpayTradeNo = allpayTradeNo;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getCheckMacValue() {
        return checkMacValue;
    }

    public void setCheckMacValue(String checkMacValue) {
        this.checkMacValue = checkMacValue;
    }
}
