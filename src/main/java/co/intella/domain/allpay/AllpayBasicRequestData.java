package co.intella.domain.allpay;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Alex
 */

public class AllpayBasicRequestData {
    @JsonProperty("PlatformID")
    private String platformId;

    @JsonProperty("MerchantID")
    private String merchantId;

    public String getPlatformId() {
        return platformId;
    }

    public void setPlatformId(String platformId) {
        this.platformId = platformId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }
}
