package co.intella.domain.allpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class AllpayBasicResponseData {
    @SerializedName("RtnCode")
    @JsonProperty("RtnCode")
    private String rtnCode;

    @SerializedName("RtnMsg")
    @JsonProperty("RtnMsg")
    private String rtnMsg;

    @SerializedName("PlatformID")
    @JsonProperty("PlatformID")
    private String platformId;

    @SerializedName("MerchantID")
    @JsonProperty("MerchantID")
    private String merchantId;

    public String getRtnCode() {
        return rtnCode;
    }

    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }

    public String getPlatformId() {
        return platformId;
    }

    public void setPlatformId(String platformId) {
        this.platformId = platformId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }
}
