package co.intella.domain.allpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class AllpayOLPayResponse {

    @SerializedName("RtnCode")
    @JsonProperty("RtnCode")
    private String rtnCode;

    @SerializedName("RtnMsg")
    @JsonProperty("RtnMsg")
    private String rtnMsg;

    @SerializedName("MerchantID")
    @JsonProperty("MerchantID")
    private String merchantId;

    @SerializedName("APPID")
    @JsonProperty("APPID")
    private String appId;

    @SerializedName("MerchantTradeNo")
    @JsonProperty("MerchantTradeNo")
    private String merchantTradeNo;

    @SerializedName("StoreID")
    @JsonProperty("StoreID")
    private String storeId;

    @SerializedName("CheckMacValue")
    @JsonProperty("CheckMacValue")
    private String checkMacValue;

    public String getRtnCode() {
        return rtnCode;
    }

    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getCheckMacValue() {
        return checkMacValue;
    }

    public void setCheckMacValue(String checkMacValue) {
        this.checkMacValue = checkMacValue;
    }
}
