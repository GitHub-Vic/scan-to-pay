package co.intella.domain.allpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class AllpayOLCreditRefundResponse {
    @JsonProperty("RtnCode")
    @SerializedName("RtnCode")
    private String rtnCode;

    @JsonProperty("RtnMsg")
    @SerializedName("RtnMsg")
    private String rtnMsg;

    @JsonProperty("MerchantTradeNo")
    @SerializedName("MerchantTradeNo")
    private String merchantTradeNo;

    @JsonProperty("Merchant")
    @SerializedName("Merchant")
    private String merchantId;

    @JsonProperty("TradeNo")
    @SerializedName("TradeNo")
    private String tradeNo;

    @JsonProperty("RefundTradeNo")
    @SerializedName("RefundTradeNo")
    private String refundTradeNo;

    public String getRtnCode() {
        return rtnCode;
    }

    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public String getRefundTradeNo() {
        return refundTradeNo;
    }

    public void setRefundTradeNo(String refundTradeNo) {
        this.refundTradeNo = refundTradeNo;
    }
}
