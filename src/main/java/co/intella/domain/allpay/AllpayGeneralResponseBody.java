package co.intella.domain.allpay;

import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class AllpayGeneralResponseBody {

    @SerializedName("RtnCode")
    private String rtnCode;

    @SerializedName("RtnMsg")
    private String rtnMsg;

    @SerializedName("PlatformID")
    private String platformID;

    @SerializedName("MerchantID")
    private String merchantID;

    @SerializedName("Encryption")
    private String encryption;

    @SerializedName("Format")
    private String format;

    @SerializedName("Data")
    private String data;

    public String getRtnCode() {
        return rtnCode;
    }

    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }

    public String getPlatformID() {
        return platformID;
    }

    public void setPlatformID(String platformID) {
        this.platformID = platformID;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getEncryption() {
        return encryption;
    }

    public void setEncryption(String encryption) {
        this.encryption = encryption;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
