package co.intella.domain.allpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import org.springframework.beans.factory.annotation.Value;

import java.util.Iterator;

/**
 * @author Alex
 */

public class AllpayMicropayRequestData extends AllpayBasicRequestData{

//    @JsonProperty("PlatformID")
//    private String platformId;
//
//    @SerializedName("MchId")
//    @JsonProperty("MerchantID")
//    private String merchantId;

    @SerializedName("StoreOrderNo")
    @JsonProperty("MerchantTradeNo")
    private String merchantTradeNo;

    @SerializedName("StoreInfo")
    @JsonProperty("StoreID")
    private String storeId;

    @SerializedName("DeviceInfo")
    @JsonProperty("PosID")
    private String posId;

    @SerializedName("")
    @JsonProperty("MerchantTradeDate")
    private String merchantTradeDate;

    @JsonProperty("MerchantTradeTime")
    private String merchantTradeTime;

    @SerializedName("AuthCode")
    @JsonProperty("Barcode")
    private String barcode;

    @SerializedName("TotalFee")
    @JsonProperty("TradeAmount")
    private String tradeAmount;

    @SerializedName("Detail")
    @JsonProperty("TradeDesc")
    private String tradeDesc;

    @JsonProperty("CheckOutType")
    private String checkOutType;

    @JsonProperty("Remark")
    private String remark;

    @JsonProperty("UseRedeem")
    private String useRedeem;

    @JsonProperty("CheckMacValue")
    private String checkMacValue;

    @JsonProperty("UseRedeemAmount")
    private String useRedeemAmount;

//    public String getPlatformId() {
//        return platformId;
//    }
//
//    public void setPlatformId(String platformId) {
//        this.platformId = platformId;
//    }
//
//    public String getMerchantId() {
//        return merchantId;
//    }
//
//    public void setMerchantId(String merchantId) {
//        this.merchantId = merchantId;
//    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getPosId() {
        return posId;
    }

    public void setPosId(String posId) {
        this.posId = posId;
    }

    public String getMerchantTradeDate() {
        return merchantTradeDate;
    }

    public void setMerchantTradeDate(String merchantTradeDate) {
        this.merchantTradeDate = merchantTradeDate;
    }

    public String getMerchantTradeTime() {
        return merchantTradeTime;
    }

    public void setMerchantTradeTime(String merchantTradeTime) {
        this.merchantTradeTime = merchantTradeTime;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(String tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public String getTradeDesc() {
        return tradeDesc;
    }

    public void setTradeDesc(String tradeDesc) {
        this.tradeDesc = tradeDesc;
    }

    public String getCheckOutType() {
        return checkOutType;
    }

    public void setCheckOutType(String checkOutType) {
        this.checkOutType = checkOutType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getUseRedeem() {
        return useRedeem;
    }

    public void setUseRedeem(String useRedeem) {
        this.useRedeem = useRedeem;
    }

    public String getCheckMacValue() {
        return checkMacValue;
    }

    public void setCheckMacValue(String checkMacValue) {
        this.checkMacValue = checkMacValue;
    }

    public String getUseRedeemAmount() {
        return useRedeemAmount;
    }

    public void setUseRedeemAmount(String useRedeemAmount) {
        this.useRedeemAmount = useRedeemAmount;
    }
}
