package co.intella.domain.allpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class AllpayRefundResponseData extends AllpayBasicResponseData {



    @SerializedName("MerchantTradeNo")
    @JsonProperty("MerchantTradeNo")
    private String merchantTradeNo;

    @SerializedName("StoreID")
    @JsonProperty("StoreID")
    private String storeId;

    @SerializedName("PosID")
    @JsonProperty("PosID")
    private String posId;

    @SerializedName("Barcode")
    @JsonProperty("Barcode")
    private String barcode;

    @SerializedName("AllpayTradeNo")
    @JsonProperty("AllpayTradeNo")
    private String allpayTradeNo;

    @SerializedName("CheckMacValue")
    @JsonProperty("CheckMacValue")
    private String checkMacValue;

    @SerializedName("RefundDate")
    @JsonProperty("RefundDate")
    private String refundDate;


    @SerializedName("RefundTradeNo")
    @JsonProperty("RefundTradeNo")
    private String refundTradeNo;

    @SerializedName("RefundTime")
    @JsonProperty("RefundTime")
    private String refundTime;

    @SerializedName("RefundAmount")
    @JsonProperty("RefundAmount")
    private String refundAmount;



    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getPosId() {
        return posId;
    }

    public void setPosId(String posId) {
        this.posId = posId;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getAllpayTradeNo() {
        return allpayTradeNo;
    }

    public void setAllpayTradeNo(String allpayTradeNo) {
        this.allpayTradeNo = allpayTradeNo;
    }

    public String getCheckMacValue() {
        return checkMacValue;
    }

    public void setCheckMacValue(String checkMacValue) {
        this.checkMacValue = checkMacValue;
    }

    public String getRefundDate() {
        return refundDate;
    }

    public void setRefundDate(String refundDate) {
        this.refundDate = refundDate;
    }

    public String getRefundTradeNo() {
        return refundTradeNo;
    }

    public void setRefundTradeNo(String refundTradeNo) {
        this.refundTradeNo = refundTradeNo;
    }

    public String getRefundTime() {
        return refundTime;
    }

    public void setRefundTime(String refundTime) {
        this.refundTime = refundTime;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }
}
