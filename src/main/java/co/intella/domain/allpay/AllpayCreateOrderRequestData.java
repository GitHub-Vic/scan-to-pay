package co.intella.domain.allpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class AllpayCreateOrderRequestData extends AllpayBasicRequestData {

    @SerializedName("StoreOrderNo")
    @JsonProperty("MerchantTradeNo")
    private String merchantTradeNo;

    @JsonProperty("MerchantTradeTime")
    private String merchantTradeTime;


    @SerializedName("TotalFee")
    @JsonProperty("TradeAmount")
    private String tradeAmount;

    @SerializedName("Detail")
    @JsonProperty("TradeDesc")
    private String tradeDesc;

    @SerializedName("ItemName")
    @JsonProperty("ItemName")
    private String itemName;

    @SerializedName("ReturnURL")
    @JsonProperty("ReturnURL")
    private String returnURL;

    @JsonProperty("CustomField1")
    private String customField1;

    @JsonProperty("CustomField2")
    private String customField2;

    @JsonProperty("CustomField3")
    private String customField3;



    @JsonProperty("UseRedeem")
    private String useRedeem;

    @JsonProperty("TradeEffectiveTime")
    private String tradeEffectiveTime;

    @JsonProperty("CheckMacValue")
    private String checkMacValue;


//    public String getPlatformId() {
//        return platformId;
//    }
//
//    public void setPlatformId(String platformId) {
//        this.platformId = platformId;
//    }
//
//    public String getMerchantId() {
//        return merchantId;
//    }
//
//    public void setMerchantId(String merchantId) {
//        this.merchantId = merchantId;
//    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }




    public String getMerchantTradeTime() {
        return merchantTradeTime;
    }

    public void setMerchantTradeTime(String merchantTradeTime) {
        this.merchantTradeTime = merchantTradeTime;
    }


    public String getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(String tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public String getTradeDesc() {
        return tradeDesc;
    }

    public void setTradeDesc(String tradeDesc) {
        this.tradeDesc = tradeDesc;
    }


    public String getUseRedeem() {
        return useRedeem;
    }

    public void setUseRedeem(String useRedeem) {
        this.useRedeem = useRedeem;
    }

    public String getCheckMacValue() {
        return checkMacValue;
    }

    public void setCheckMacValue(String checkMacValue) {
        this.checkMacValue = checkMacValue;
    }


    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getReturnURL() {
        return returnURL;
    }

    public void setReturnURL(String returnURL) {
        this.returnURL = returnURL;
    }

    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    public String getTradeEffectiveTime() {
        return tradeEffectiveTime;
    }

    public void setTradeEffectiveTime(String tradeEffectiveTime) {
        this.tradeEffectiveTime = tradeEffectiveTime;
    }
}
