package co.intella.domain.allpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class AllpaySingleQueryRequestData extends AllpayBasicRequestData {
//
//    @JsonProperty("PlatformID")
//    private String platformId;
//
//    @JsonProperty("MerchantID")
//    private String merchantId;

    @SerializedName("StoreOrderNo")
    @JsonProperty("MerchantTradeNo")
    private String merchantTradeNo;

    @SerializedName("AuthCode")
    @JsonProperty("Barcode")
    private String barcode;

    @JsonProperty("CheckMacValue")
    private String checkMacValue;

//    public String getPlatformId() {
//        return platformId;
//    }
//
//    public void setPlatformId(String platformId) {
//        this.platformId = platformId;
//    }
//
//    public String getMerchantId() {
//        return merchantId;
//    }
//
//    public void setMerchantId(String merchantId) {
//        this.merchantId = merchantId;
//    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getCheckMacValue() {
        return checkMacValue;
    }

    public void setCheckMacValue(String checkMacValue) {
        this.checkMacValue = checkMacValue;
    }
}
