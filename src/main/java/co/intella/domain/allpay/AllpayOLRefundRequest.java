package co.intella.domain.allpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class AllpayOLRefundRequest extends AllpayBasicRequestData{
//    @SerializedName("mchId")
//    @JsonProperty("MerchantID")
//    private String merchantId;

    @SerializedName("StoreOrderNo")
    @JsonProperty("MerchantTradeNo")
    private String merchantTradeNo;

    @JsonProperty("TradeNo")
    private String tradeNo;

    @SerializedName("RefundFee")
    @JsonProperty("ChargeBackTotalAmount")
    private String chargeBackTotalAmount;

    @JsonProperty("CheckMacValue")
    private String checkMacValue;

    @JsonProperty("Remark")
    private String remark;

//    @JsonProperty("PlatformID")
//    private String platformId;



    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public String getChargeBackTotalAmount() {
        return chargeBackTotalAmount;
    }

    public void setChargeBackTotalAmount(String chargeBackTotalAmount) {
        this.chargeBackTotalAmount = chargeBackTotalAmount;
    }

    public String getCheckMacValue() {
        return checkMacValue;
    }

    public void setCheckMacValue(String checkMacValue) {
        this.checkMacValue = checkMacValue;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

}
