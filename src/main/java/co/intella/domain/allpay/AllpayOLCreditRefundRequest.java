package co.intella.domain.allpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class AllpayOLCreditRefundRequest  extends AllpayBasicRequestData{

    @SerializedName("StoreOrderNo")
    @JsonProperty("MerchantTradeNo")
    private String merchantTradeNo;

    @JsonProperty("TradeNo")
    private String tradeNo;

    @JsonProperty("Action")
    private String action;

    @SerializedName("RefundFee")
    @JsonProperty("TotalAmount")
    private String totalAmount;

    @JsonProperty("CheckMacValue")
    private String checkMacValue;

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCheckMacValue() {
        return checkMacValue;
    }

    public void setCheckMacValue(String checkMacValue) {
        this.checkMacValue = checkMacValue;
    }
}
