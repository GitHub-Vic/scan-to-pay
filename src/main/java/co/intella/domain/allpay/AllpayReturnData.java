package co.intella.domain.allpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class AllpayReturnData {

    @SerializedName("MerchantTradeNo")
    private String merchantTradeNo;

    @SerializedName("MerchantID")
    private String merchantID;


    @SerializedName("PaymentDate")
    private String paymentDate;


    @SerializedName("PaymentType")
    private String paymentType;


    @SerializedName("PaymentTypeChargeFee")
    private String paymentTypeChargeFee;


    @SerializedName("RtnCode")
    private String rtnCode;

    @SerializedName("RtnMsg")
    private String rtnMsg;

    @SerializedName("SimulatePaid")
    private String simulatePaid;

    @SerializedName("TradeAmt")
    private String tradeAmt;

    @SerializedName("TradeDate")
    private String tradeDate;

    @SerializedName("TradeNo")
    private String tradeNo;

    @SerializedName("CheckMacValue")
    private String checkMacValue;

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentTypeChargeFee() {
        return paymentTypeChargeFee;
    }

    public void setPaymentTypeChargeFee(String paymentTypeChargeFee) {
        this.paymentTypeChargeFee = paymentTypeChargeFee;
    }

    public String getRtnCode() {
        return rtnCode;
    }

    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }

    public String getSimulatePaid() {
        return simulatePaid;
    }

    public void setSimulatePaid(String simulatePaid) {
        this.simulatePaid = simulatePaid;
    }

    public String getTradeAmt() {
        return tradeAmt;
    }

    public void setTradeAmt(String tradeAmt) {
        this.tradeAmt = tradeAmt;
    }

    public String getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public String getCheckMacValue() {
        return checkMacValue;
    }

    public void setCheckMacValue(String checkMacValue) {
        this.checkMacValue = checkMacValue;
    }
}
