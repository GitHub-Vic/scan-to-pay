package co.intella.domain.allpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class AllpayMicropayResponseData extends AllpayBasicResponseData {



    @SerializedName("MerchantTradeNo")
    @JsonProperty("MerchantTradeNo")
    private String merchantTradeNo;

    @SerializedName("Barcode")
    @JsonProperty("Barcode")
    private String barcode;

    @SerializedName("CheckMacValue")
    @JsonProperty("CheckMacValue")
    private String checkMacValue;

    @SerializedName("StoreID")
    @JsonProperty("StoreID")
    private String storeID;

    @SerializedName("PosID")
    @JsonProperty("PosID")
    private String posId;

    @SerializedName("AllpayTradeNo")
    @JsonProperty("AllpayTradeNo")
    private String allpayTradeNo;

    @SerializedName("AllpayTradeDate")
    @JsonProperty("AllpayTradeDate")
    private String allpayTradeDate;

    @SerializedName("AllpayTradeTime")
    @JsonProperty("AllpayTradeTime")
    private String allpayTradeTime;

    @SerializedName("TradeAmount")
    @JsonProperty("TradeAmount")
    private String tradeAmount;

    @SerializedName("PayAmount")
    @JsonProperty("PayAmount")
    private String payAmount;

    @SerializedName("RedeemAmount")
    @JsonProperty("RedeemAmount")
    private String redeemAmount;

    @SerializedName("PaymentType")
    @JsonProperty("PaymentType")
    private String paymentType;



    @SerializedName("RefundTradeNo")
    @JsonProperty("RefundTradeNo")
    private String refundTradeNo;

    @SerializedName("RefundDate")
    @JsonProperty("RefundDate")
    private String refundDate;

    @SerializedName("RefundTime")
    @JsonProperty("RefundTime")
    private String refundTime;

    @SerializedName("RefundAmount")
    @JsonProperty("RefundAmount")
    private String refundAmount;

    @SerializedName("CarrierNum")
    @JsonProperty("CarrierNum")
    private String carrierNum;

    @SerializedName("MembershipCardNo")
    @JsonProperty("MembershipCardNo")
    private String membershipCardNo;



    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getCheckMacValue() {
        return checkMacValue;
    }

    public void setCheckMacValue(String checkMacValue) {
        this.checkMacValue = checkMacValue;
    }

    public String getStoreID() {
        return storeID;
    }

    public void setStoreID(String storeID) {
        this.storeID = storeID;
    }

    public String getPosId() {
        return posId;
    }

    public void setPosId(String posId) {
        this.posId = posId;
    }

    public String getAllpayTradeNo() {
        return allpayTradeNo;
    }

    public void setAllpayTradeNo(String allpayTradeNo) {
        this.allpayTradeNo = allpayTradeNo;
    }

    public String getAllpayTradeDate() {
        return allpayTradeDate;
    }

    public void setAllpayTradeDate(String allpayTradeDate) {
        this.allpayTradeDate = allpayTradeDate;
    }

    public String getAllpayTradeTime() {
        return allpayTradeTime;
    }

    public void setAllpayTradeTime(String allpayTradeTime) {
        this.allpayTradeTime = allpayTradeTime;
    }

    public String getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(String tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public String getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(String payAmount) {
        this.payAmount = payAmount;
    }

    public String getRedeemAmount() {
        return redeemAmount;
    }

    public void setRedeemAmount(String redeemAmount) {
        this.redeemAmount = redeemAmount;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getRefundTradeNo() {
        return refundTradeNo;
    }

    public void setRefundTradeNo(String refundTradeNo) {
        this.refundTradeNo = refundTradeNo;
    }

    public String getRefundDate() {
        return refundDate;
    }

    public void setRefundDate(String refundDate) {
        this.refundDate = refundDate;
    }

    public String getRefundTime() {
        return refundTime;
    }

    public void setRefundTime(String refundTime) {
        this.refundTime = refundTime;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getCarrierNum() {
        return carrierNum;
    }

    public void setCarrierNum(String carrierNum) {
        this.carrierNum = carrierNum;
    }

    public String getMembershipCardNo() {
        return membershipCardNo;
    }

    public void setMembershipCardNo(String membershipCardNo) {
        this.membershipCardNo = membershipCardNo;
    }
}
