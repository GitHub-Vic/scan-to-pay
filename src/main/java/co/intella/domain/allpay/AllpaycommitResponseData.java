package co.intella.domain.allpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class AllpaycommitResponseData extends AllpayBasicResponseData {

    @SerializedName("MerchantTradeNo")
    @JsonProperty("MerchantTradeNo")
    private String merchantTradeNo;

    @SerializedName("Barcode")
    @JsonProperty("Barcode")
    private String barcode;

    @SerializedName("CheckMacValue")
    @JsonProperty("CheckMacValue")
    private String checkMacValue;

    @SerializedName("StoreID")
    @JsonProperty("StoreID")
    private String storeID;

    @SerializedName("PosID")
    @JsonProperty("PosID")
    private String posId;

    @SerializedName("AllpayTradeNo")
    @JsonProperty("AllpayTradeNo")
    private String allpayTradeNo;

    @SerializedName("TradeType")
    @JsonProperty("TradeType")
    private String tradeType;

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getCheckMacValue() {
        return checkMacValue;
    }

    public void setCheckMacValue(String checkMacValue) {
        this.checkMacValue = checkMacValue;
    }

    public String getStoreID() {
        return storeID;
    }

    public void setStoreID(String storeID) {
        this.storeID = storeID;
    }

    public String getPosId() {
        return posId;
    }

    public void setPosId(String posId) {
        this.posId = posId;
    }

    public String getAllpayTradeNo() {
        return allpayTradeNo;
    }

    public void setAllpayTradeNo(String allpayTradeNo) {
        this.allpayTradeNo = allpayTradeNo;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }
}
