package co.intella.domain.allpay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class AllpayCommitRequestData extends AllpayBasicRequestData {
//    @SerializedName("PlatformID")
//    @JsonProperty("PlatformID")
//    private String platformId;
//
//    @SerializedName("MerchantID")
//    @JsonProperty("MerchantID")
//    private String merchantId;

    @SerializedName("MerchantTradeNo")
    @JsonProperty("MerchantTradeNo")
    private String merchantTradeNo;

    @SerializedName("StoreID")
    @JsonProperty("StoreID")
    private String storeId;

    @SerializedName("PosID")
    @JsonProperty("PosID")
    private String posId;

    @SerializedName("Barcode")
    @JsonProperty("Barcode")
    private String barcode;

    @SerializedName("AllpayTradeNo")
    @JsonProperty("AllpayTradeNo")
    private String allpayTradeNo;

    @SerializedName("TradeStatus")
    @JsonProperty("TradeType")
    private String tradeType;

    @JsonProperty("CheckMacValue")
    private String checkMacValue;

//    public String getPlatformId() {
//        return platformId;
//    }
//
//    public void setPlatformId(String platformId) {
//        this.platformId = platformId;
//    }
//
//    public String getMerchantId() {
//        return merchantId;
//    }
//
//    public void setMerchantId(String merchantId) {
//        this.merchantId = merchantId;
//    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getPosId() {
        return posId;
    }

    public void setPosId(String posId) {
        this.posId = posId;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getAllpayTradeNo() {
        return allpayTradeNo;
    }

    public void setAllpayTradeNo(String allpayTradeNo) {
        this.allpayTradeNo = allpayTradeNo;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getCheckMacValue() {
        return checkMacValue;
    }

    public void setCheckMacValue(String checkMacValue) {
        this.checkMacValue = checkMacValue;
    }
}
