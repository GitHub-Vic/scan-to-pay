package co.intella.domain.megaTwpay2;

import co.intella.model.PaymentAccount;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class MegaBaseModel {

    public MegaBaseModel() {
    }

    public MegaBaseModel(PaymentAccount paymentAccount) {
        this.storeId = paymentAccount.getAccount();
        this.endpointCode = paymentAccount.getAccountTerminalId();
    }

    @JsonProperty("storeId")
    @SerializedName("storeId")
    protected String storeId;

    @JsonProperty("endpointCode")
    @SerializedName("endpointCode")
    protected String endpointCode;

    @JsonProperty("sign")
    @SerializedName("sign")
    protected String sign;

    @JsonProperty("rtnCode")
    @SerializedName("rtnCode")
    protected String rtnCode;

    @JsonProperty("rtnMsg")
    @SerializedName("rtnMsg")
    protected String rtnMsg;

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getRtnCode() {
        return rtnCode;
    }

    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getEndpointCode() {
        return endpointCode;
    }

    public void setEndpointCode(String endpointCode) {
        this.endpointCode = endpointCode;
    }

}
