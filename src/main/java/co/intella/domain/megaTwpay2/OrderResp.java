package co.intella.domain.megaTwpay2;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class OrderResp extends MegaBaseModel {


    @JsonProperty("terminalId")
    @SerializedName("terminalId")
    private String terminalId;

    @JsonProperty("orderNumber")
    @SerializedName("orderNumber")
    private String orderNumber;

    @JsonProperty("txnDir")
    @SerializedName("txnDir")
    private String txnDir;

    @JsonProperty("txnDateTime")
    @SerializedName("txnDateTime")
    private String txnDateTime;

    @JsonProperty("txnAmt")
    @SerializedName("txnAmt")
    private String txnAmt;

    @JsonProperty("txnCurrency")
    @SerializedName("txnCurrency")
    private String txnCurrency;

    @JsonProperty("terminalDateTime")
    @SerializedName("terminalDateTime")
    private String terminalDateTime;

    @JsonProperty("txnSeqno")
    @SerializedName("txnSeqno")
    private String txnSeqno;

    @JsonProperty("txnAccNO")
    @SerializedName("txnAccNO")
    private String txnAccNO;

    @JsonProperty("payType")
    @SerializedName("payType")
    private String payType;

    @JsonProperty("txnFISCTac")
    @SerializedName("txnFISCTac")
    private String txnFISCTac;

    @JsonProperty("txnFinishDateTim")
    @SerializedName("txnFinishDateTim")
    private String txnFinishDateTim;

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getTxnDir() {
        return txnDir;
    }

    public void setTxnDir(String txnDir) {
        this.txnDir = txnDir;
    }

    public String getTxnDateTime() {
        return txnDateTime;
    }

    public void setTxnDateTime(String txnDateTime) {
        this.txnDateTime = txnDateTime;
    }

    public String getTxnAmt() {
        return txnAmt;
    }

    public void setTxnAmt(String txnAmt) {
        this.txnAmt = txnAmt;
    }

    public String getTxnCurrency() {
        return txnCurrency;
    }

    public void setTxnCurrency(String txnCurrency) {
        this.txnCurrency = txnCurrency;
    }

    public String getTerminalDateTime() {
        return terminalDateTime;
    }

    public void setTerminalDateTime(String terminalDateTime) {
        this.terminalDateTime = terminalDateTime;
    }

    public String getTxnSeqno() {
        return txnSeqno;
    }

    public void setTxnSeqno(String txnSeqno) {
        this.txnSeqno = txnSeqno;
    }

    public String getTxnAccNO() {
        return txnAccNO;
    }

    public void setTxnAccNO(String txnAccNO) {
        this.txnAccNO = txnAccNO;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getTxnFISCTac() {
        return txnFISCTac;
    }

    public void setTxnFISCTac(String txnFISCTac) {
        this.txnFISCTac = txnFISCTac;
    }

    public String getTxnFinishDateTim() {
        return txnFinishDateTim;
    }

    public void setTxnFinishDateTim(String txnFinishDateTim) {
        this.txnFinishDateTim = txnFinishDateTim;
    }
}
