package co.intella.domain.megaTwpay2;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class RefundResp extends MegaBaseModel {

    @JsonProperty("txnDir")
    @SerializedName("txnDir")
    private String txnDir;

    @JsonProperty("txnDateTime")
    @SerializedName("txnDateTime")
    private String txnDateTime;

    @JsonProperty("orgOrderNumber")
    @SerializedName("orgOrderNumber")
    private String orgOrderNumber;

    @JsonProperty("orgCurrency")
    @SerializedName("orgCurrency")
    private String orgCurrency;

    @JsonProperty("orgAmt")
    @SerializedName("orgAmt")
    private String orgAmt;

    @JsonProperty("cancelAmt")
    @SerializedName("cancelAmt")
    private String cancelAmt;

    @JsonProperty("orgTerminalId")
    @SerializedName("orgTerminalId")
    private String orgTerminalId;

    @JsonProperty("txnSeqno")
    @SerializedName("txnSeqno")
    private String txnSeqno;

    @JsonProperty("terminalId")
    @SerializedName("terminalId")
    private String terminalId;

    public String getTxnDir() {
        return txnDir;
    }

    public void setTxnDir(String txnDir) {
        this.txnDir = txnDir;
    }

    public String getTxnDateTime() {
        return txnDateTime;
    }

    public void setTxnDateTime(String txnDateTime) {
        this.txnDateTime = txnDateTime;
    }

    public String getOrgOrderNumber() {
        return orgOrderNumber;
    }

    public void setOrgOrderNumber(String orgOrderNumber) {
        this.orgOrderNumber = orgOrderNumber;
    }

    public String getOrgCurrency() {
        return orgCurrency;
    }

    public void setOrgCurrency(String orgCurrency) {
        this.orgCurrency = orgCurrency;
    }

    public String getOrgAmt() {
        return orgAmt;
    }

    public void setOrgAmt(String orgAmt) {
        this.orgAmt = orgAmt;
    }

    public String getCancelAmt() {
        return cancelAmt;
    }

    public void setCancelAmt(String cancelAmt) {
        this.cancelAmt = cancelAmt;
    }

    public String getOrgTerminalId() {
        return orgTerminalId;
    }

    public void setOrgTerminalId(String orgTerminalId) {
        this.orgTerminalId = orgTerminalId;
    }

    public String getTxnSeqno() {
        return txnSeqno;
    }

    public void setTxnSeqno(String txnSeqno) {
        this.txnSeqno = txnSeqno;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }
}
