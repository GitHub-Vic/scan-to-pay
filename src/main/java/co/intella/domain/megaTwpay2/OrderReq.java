package co.intella.domain.megaTwpay2;

import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import co.intella.utility.DateUtil;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.time.LocalDateTime;

public class OrderReq extends MegaBaseModel {

    public OrderReq() {

    }

    public OrderReq(PaymentAccount paymentAccount, TradeDetail tradeDetail) {
        super(paymentAccount);
        this.txnDir = "RQ";
        LocalDateTime now = LocalDateTime.now();
        this.txnDateTime = DateUtil.getDateTime(now, "yyyyMMddHHmmss");
        this.txnCurrency = "901";
        this.terminalDateTime = DateUtil.getDateTime(now, "yyyyMMddHHmmss");
        this.terminalId = paymentAccount.getAccount();
        this.txnAmt = tradeDetail.getPayment() + "00";
        this.orderNumber = tradeDetail.getOrderId();
        this.txnFISCTac = tradeDetail.getBarcode();
    }


    @JsonProperty("terminalId")
    @SerializedName("terminalId")
    private String terminalId;

    @JsonProperty("orderNumber")
    @SerializedName("orderNumber")
    private String orderNumber;

    @JsonProperty("txnDir")
    @SerializedName("txnDir")
    private String txnDir;

    @JsonProperty("txnDateTime")
    @SerializedName("txnDateTime")
    private String txnDateTime;

    @JsonProperty("txnAmt")
    @SerializedName("txnAmt")
    private String txnAmt;

    @JsonProperty("txnCurrency")
    @SerializedName("txnCurrency")
    private String txnCurrency;

    @JsonProperty("txnFISCTac")
    @SerializedName("txnFISCTac")
    private String txnFISCTac;

    @JsonProperty("terminalDateTime")
    @SerializedName("terminalDateTime")
    private String terminalDateTime;

    public String getTxnDateTime() {
        return txnDateTime;
    }

    public void setTxnDateTime(String txnDateTime) {
        this.txnDateTime = txnDateTime;
    }

    public String getTxnAmt() {
        return txnAmt;
    }

    public void setTxnAmt(String txnAmt) {
        this.txnAmt = txnAmt;
    }

    public String getTxnCurrency() {
        return txnCurrency;
    }

    public void setTxnCurrency(String txnCurrency) {
        this.txnCurrency = txnCurrency;
    }

    public String getTxnFISCTac() {
        return txnFISCTac;
    }

    public void setTxnFISCTac(String txnFISCTac) {
        this.txnFISCTac = txnFISCTac;
    }

    public String getTerminalDateTime() {
        return terminalDateTime;
    }

    public void setTerminalDateTime(String terminalDateTime) {
        this.terminalDateTime = terminalDateTime;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getTxnDir() {
        return txnDir;
    }

    public void setTxnDir(String txnDir) {
        this.txnDir = txnDir;
    }
}
