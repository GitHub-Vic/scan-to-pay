package co.intella.domain.megaTwpay2;

import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import co.intella.utility.DateUtil;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.time.LocalDateTime;

public class InqueryReq extends MegaBaseModel {
    public InqueryReq() {
    }

    public InqueryReq(PaymentAccount paymentAccount, TradeDetail tradeDetail) {
        super(paymentAccount);
        this.txnDir = "RQ";
        this.orderNumber = tradeDetail.getOrderId();
        LocalDateTime now = LocalDateTime.now();
        this.inqDateTime = DateUtil.getDateTime(now, "yyyyMMdd");

    }

    @JsonProperty("txnDir")
    @SerializedName("txnDir")
    private String txnDir;

    @JsonProperty("terminalId")
    @SerializedName("terminalId")
    private String terminalId;

    @JsonProperty("orderNumber")
    @SerializedName("orderNumber")
    private String orderNumber;

    @JsonProperty("inqDateTime")
    @SerializedName("inqDateTime")
    private String inqDateTime;

    @JsonProperty("txnType")
    @SerializedName("txnType")
    private String txnType;

    @JsonProperty("txnSeqno")
    @SerializedName("txnSeqno")
    private String txnSeqno;

    @JsonProperty("txnStatus")
    @SerializedName("txnStatus")
    private String txnStatus;

    @JsonProperty("payType")
    @SerializedName("payType")
    private String payType;

    @JsonProperty("acctDate")
    @SerializedName("acctDate")
    private String acctDate;

    @JsonProperty("txnAccNO")
    @SerializedName("txnAccNO")
    private String txnAccNO;

    @JsonProperty("crtDateTime")
    @SerializedName("crtDateTime")
    private String crtDateTime;

    @JsonProperty("txnDateTime")
    @SerializedName("txnDateTime")
    private String txnDateTime;

    @JsonProperty("txnAcctDate")
    @SerializedName("txnAcctDate")
    private String txnAcctDate;

    @JsonProperty("txnCurrency")
    @SerializedName("txnCurrency")
    private String txnCurrency;

    @JsonProperty("txnAmt")
    @SerializedName("txnAmt")
    private String txnAmt;

    @JsonProperty("carrierType")
    @SerializedName("carrierType")
    private String carrierType;

    @JsonProperty("carrierId1")
    @SerializedName("carrierId1")
    private String carrierId1;

    @JsonProperty("storeMemo")
    @SerializedName("storeMemo")
    private String storeMemo;

    public String getTxnDir() {
        return txnDir;
    }

    public void setTxnDir(String txnDir) {
        this.txnDir = txnDir;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getInqDateTime() {
        return inqDateTime;
    }

    public void setInqDateTime(String inqDateTime) {
        this.inqDateTime = inqDateTime;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getTxnSeqno() {
        return txnSeqno;
    }

    public void setTxnSeqno(String txnSeqno) {
        this.txnSeqno = txnSeqno;
    }

    public String getTxnStatus() {
        return txnStatus;
    }

    public void setTxnStatus(String txnStatus) {
        this.txnStatus = txnStatus;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getAcctDate() {
        return acctDate;
    }

    public void setAcctDate(String acctDate) {
        this.acctDate = acctDate;
    }

    public String getTxnAccNO() {
        return txnAccNO;
    }

    public void setTxnAccNO(String txnAccNO) {
        this.txnAccNO = txnAccNO;
    }

    public String getCrtDateTime() {
        return crtDateTime;
    }

    public void setCrtDateTime(String crtDateTime) {
        this.crtDateTime = crtDateTime;
    }

    public String getTxnDateTime() {
        return txnDateTime;
    }

    public void setTxnDateTime(String txnDateTime) {
        this.txnDateTime = txnDateTime;
    }

    public String getTxnAcctDate() {
        return txnAcctDate;
    }

    public void setTxnAcctDate(String txnAcctDate) {
        this.txnAcctDate = txnAcctDate;
    }

    public String getTxnCurrency() {
        return txnCurrency;
    }

    public void setTxnCurrency(String txnCurrency) {
        this.txnCurrency = txnCurrency;
    }

    public String getTxnAmt() {
        return txnAmt;
    }

    public void setTxnAmt(String txnAmt) {
        this.txnAmt = txnAmt;
    }

    public String getCarrierType() {
        return carrierType;
    }

    public void setCarrierType(String carrierType) {
        this.carrierType = carrierType;
    }

    public String getCarrierId1() {
        return carrierId1;
    }

    public void setCarrierId1(String carrierId1) {
        this.carrierId1 = carrierId1;
    }

    public String getStoreMemo() {
        return storeMemo;
    }

    public void setStoreMemo(String storeMemo) {
        this.storeMemo = storeMemo;
    }
}
