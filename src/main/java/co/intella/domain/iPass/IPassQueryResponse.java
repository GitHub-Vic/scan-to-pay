package co.intella.domain.iPass;

import com.google.gson.annotations.SerializedName;

public class IPassQueryResponse extends IPassGeneralResponse {

    @SerializedName("CardID")
    private String cardID;

    @SerializedName("CardSN")
    private String cardSN;

    @SerializedName("CardSNFull")
    private String cardSNFull;

    @SerializedName("Balance")
    private String balance;

    @SerializedName("BackUpBalance")
    private String backUpBalance;

    @SerializedName("CardType")
    private String cardType;

    @SerializedName("IdentityType")
    private String identityType;

    @SerializedName("IdentityExpiryDate")
    private String identityExpiryDate;

    @SerializedName("SpecialCardType")
    private String specialCardType;

    @SerializedName("SpecialIdentityProvider")
    private String specialIdentityProvider;

    @SerializedName("SpecialIdentityType")
    private String specialIdentityType;

    @SerializedName("SpecialIdentityExpireDate")
    private String specialIdentityExpireDate;

    @SerializedName("SpecialIdentityUsagePointLimit")
    private String specialIdentityUsagePointLimit;

    @SerializedName("UsageAccumulatedPointBeforeTxn")
    private String usageAccumulatedPointBeforeTxn;

    @SerializedName("SpecialIdentityResetDate")
    private String specialIdentityResetDate;

    @SerializedName("AutoloadFlag")
    private String autoloadFlag;

    @SerializedName("AutoloadUnit")
    private String autoloadUnit;

    @SerializedName("SpecialIdentityPointRatio")
    private String specialIdentityPointRatio;

    @SerializedName("MaxPointPerTrip")
    private String maxPointPerTrip;

    @SerializedName("UsePointLimit")
    private String usePointLimit;

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }

    public String getCardSN() {
        return cardSN;
    }

    public void setCardSN(String cardSN) {
        this.cardSN = cardSN;
    }

    public String getCardSNFull() {
        return cardSNFull;
    }

    public void setCardSNFull(String cardSNFull) {
        this.cardSNFull = cardSNFull;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getBackUpBalance() {
        return backUpBalance;
    }

    public void setBackUpBalance(String backUpBalance) {
        this.backUpBalance = backUpBalance;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getIdentityType() {
        return identityType;
    }

    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }

    public String getIdentityExpiryDate() {
        return identityExpiryDate;
    }

    public void setIdentityExpiryDate(String identityExpiryDate) {
        this.identityExpiryDate = identityExpiryDate;
    }

    public String getSpecialCardType() {
        return specialCardType;
    }

    public void setSpecialCardType(String specialCardType) {
        this.specialCardType = specialCardType;
    }

    public String getSpecialIdentityProvider() {
        return specialIdentityProvider;
    }

    public void setSpecialIdentityProvider(String specialIdentityProvider) {
        this.specialIdentityProvider = specialIdentityProvider;
    }

    public String getSpecialIdentityType() {
        return specialIdentityType;
    }

    public void setSpecialIdentityType(String specialIdentityType) {
        this.specialIdentityType = specialIdentityType;
    }

    public String getSpecialIdentityExpireDate() {
        return specialIdentityExpireDate;
    }

    public void setSpecialIdentityExpireDate(String specialIdentityExpireDate) {
        this.specialIdentityExpireDate = specialIdentityExpireDate;
    }

    public String getSpecialIdentityUsagePointLimit() {
        return specialIdentityUsagePointLimit;
    }

    public void setSpecialIdentityUsagePointLimit(String specialIdentityUsagePointLimit) {
        this.specialIdentityUsagePointLimit = specialIdentityUsagePointLimit;
    }

    public String getUsageAccumulatedPointBeforeTxn() {
        return usageAccumulatedPointBeforeTxn;
    }

    public void setUsageAccumulatedPointBeforeTxn(String usageAccumulatedPointBeforeTxn) {
        this.usageAccumulatedPointBeforeTxn = usageAccumulatedPointBeforeTxn;
    }

    public String getSpecialIdentityResetDate() {
        return specialIdentityResetDate;
    }

    public void setSpecialIdentityResetDate(String specialIdentityResetDate) {
        this.specialIdentityResetDate = specialIdentityResetDate;
    }

    public String getAutoloadFlag() {
        return autoloadFlag;
    }

    public void setAutoloadFlag(String autoloadFlag) {
        this.autoloadFlag = autoloadFlag;
    }

    public String getAutoloadUnit() {
        return autoloadUnit;
    }

    public void setAutoloadUnit(String autoloadUnit) {
        this.autoloadUnit = autoloadUnit;
    }

    public String getSpecialIdentityPointRatio() {
        return specialIdentityPointRatio;
    }

    public void setSpecialIdentityPointRatio(String specialIdentityPointRatio) {
        this.specialIdentityPointRatio = specialIdentityPointRatio;
    }

    public String getMaxPointPerTrip() {
        return maxPointPerTrip;
    }

    public void setMaxPointPerTrip(String maxPointPerTrip) {
        this.maxPointPerTrip = maxPointPerTrip;
    }

    public String getUsePointLimit() {
        return usePointLimit;
    }

    public void setUsePointLimit(String usePointLimit) {
        this.usePointLimit = usePointLimit;
    }
}
