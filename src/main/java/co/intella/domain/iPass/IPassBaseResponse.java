package co.intella.domain.iPass;

import co.intella.domain.integration.ResponseGeneralData;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * sol  ，新增於ipass 小額 及 停車場用
 */
public class IPassBaseResponse extends ResponseGeneralData {

    @JsonProperty("DataTime")
    @SerializedName("DataTime")
    private String dataTime;

    @JsonProperty("ShopName")
    @SerializedName("ShopName")
    private String shopName;

    @JsonProperty("NewAESKey")
    @SerializedName("NewAESKey")
    private String newAESKey;

    @JsonProperty("AutoloadFlag")
    @SerializedName("AutoloadFlag")
    private String autoloadFlag;

    @JsonProperty("AdviceCount")
    @SerializedName("AdviceCount")
    private String adviceCount;

    @JsonProperty("BackupHostAddress")
    @SerializedName("BackupHostAddress")
    private String backupHostAddress;

    @JsonProperty("CardStatus")
    @SerializedName("CardStatus")
    private String cardStatus;

    @JsonProperty("Time")
    @SerializedName("Time")
    private String time;

    @JsonProperty("MainHostPort")
    @SerializedName("MainHostPort")
    private String mainHostPort;

    @JsonProperty("PinCode")
    @SerializedName("PinCode")
    private String pinCode;

    @JsonProperty("OpenAutoLoadActiveEnable")
    @SerializedName("OpenAutoLoadActiveEnable")
    private String openAutoLoadActiveEnable;

    @JsonProperty("BackupHostPort")
    @SerializedName("BackupHostPort")
    private String backupHostPort;

    @JsonProperty("CloseAutoLoadActiveEnable")
    @SerializedName("CloseAutoLoadActiveEnable")
    private String closeAutoLoadActiveEnable;

    @JsonProperty("FuncOpenFlag")
    @SerializedName("FuncOpenFlag")
    private String funcOpenFlag;

    @JsonProperty("TxnsReturn")
    @SerializedName("TxnsReturn")
    private String txnsReturn;

    @JsonProperty("ProfileExpiry")
    @SerializedName("ProfileExpiry")
    private String profileExpiry;

    @JsonProperty("TxDataInFo")
    @SerializedName("TxDataInFo")
    private List<TxDataInFo> txDataInFoList;

    @JsonProperty("CardTxnSerialNumber")
    @SerializedName("CardTxnSerialNumber")
    private String cardTxnSerialNumber;

    @JsonProperty("ChangeCardActiveEnable")
    @SerializedName("ChangeCardActiveEnable")
    private String changeCardActiveEnable;

    @JsonProperty("PosOperatorID")
    @SerializedName("PosOperatorID")
    private String posOperatorID;

    @JsonProperty("BeforeTXNBalance")
    @SerializedName("BeforeTXNBalance")
    private String beforeTXNBalance;

    @JsonProperty("PosSerialNumber")
    @SerializedName("PosSerialNumber")
    private String posSerialNumber;

    @JsonProperty("SettlementData")
    @SerializedName("SettlementData")
    private List<SettlementData> settlementDataList;

    @JsonProperty("PurchaseDate")
    @SerializedName("PurchaseDate")
    private String purchaseDate;

    @JsonProperty("Balance")
    @SerializedName("Balance")
    private String balance;

    @JsonProperty("SamIDLen")
    @SerializedName("SamIDLen")
    private String samIDLen;

    @JsonProperty("TXNResult")
    @SerializedName("TXNResult")
    private String tXNResult;

    @JsonProperty("ShopID")
    @SerializedName("ShopID")
    private String shopID;

    @JsonProperty("ErrorCode")
    @SerializedName("ErrorCode")
    private String errorCode;

    @JsonProperty("ChangeCardActiveAmount")
    @SerializedName("ChangeCardActiveAmount")
    private String changeCardActiveAmount;

    @JsonProperty("SamID")
    @SerializedName("SamID")
    private String samID;

    @JsonProperty("EquipID")
    @SerializedName("EquipID")
    private String equipID;

    @JsonProperty("RecordDeductAllLen")
    @SerializedName("RecordDeductAllLen")
    private String recordDeductAllLen;

    @JsonProperty("CardSerialNo")
    @SerializedName("CardSerialNo")
    private String cardSerialNo;

    @JsonProperty("Date")
    @SerializedName("Date")
    private String date;

    @JsonProperty("PurchaseOneDayValue")
    @SerializedName("PurchaseOneDayValue")
    private String purchaseOneDayValue;

    @JsonProperty("MaxValueTxn")
    @SerializedName("MaxValueTxn")
    private String maxValueTxn;

    @JsonProperty("CardNumberForPrint")
    @SerializedName("CardNumberForPrint")
    private String cardNumberForPrint;

    @JsonProperty("CloseAutoLoadActiveAmount")
    @SerializedName("CloseAutoLoadActiveAmount")
    private String closeAutoLoadActiveAmount;

    @JsonProperty("AutoloadUnit")
    @SerializedName("AutoloadUnit")
    private String autoloadUnit;

    @JsonProperty("TxnCompanyID")
    @SerializedName("TxnCompanyID")
    private String txnCompanyID;

    @JsonProperty("McErrorCode")
    @SerializedName("McErrorCode")
    private String mcErrorCode;

    @JsonProperty("AreaCode")
    @SerializedName("AreaCode")
    private String areaCode;

    @JsonProperty("LockCardActiveAmount")
    @SerializedName("LockCardActiveAmount")
    private String lockCardActiveAmount;

    @JsonProperty("BankID")
    @SerializedName("BankID")
    private String bankID;

    @JsonProperty("IssuerCode")
    @SerializedName("IssuerCode")
    private String issuerCode;

    @JsonProperty("RegisterFlag")
    @SerializedName("RegisterFlag")
    private String registerFlag;

    @JsonProperty("FieldFlags")
    @SerializedName("FieldFlags")
    private String fieldFlags;

    @JsonProperty("TerminalID")
    @SerializedName("TerminalID")
    private String terminalID;

    @JsonProperty("ApiVer")
    @SerializedName("ApiVer")
    private String apiVer;

    @JsonProperty("Amount")
    @SerializedName("Amount")
    private String amount;

    @JsonProperty("CompanyName")
    @SerializedName("CompanyName")
    private String companyName;

    @JsonProperty("ProfileType")
    @SerializedName("ProfileType")
    private String profileType;

    @JsonProperty("DeviceID")
    @SerializedName("DeviceID")
    private String deviceID;

    @JsonProperty("AICMID")
    @SerializedName("AICMID")
    private String aICMID;

    @JsonProperty("MainHostAddress")
    @SerializedName("MainHostAddress")
    private String mainHostAddress;

    @JsonProperty("AICID")
    @SerializedName("AICID")
    private String aICID;

    @JsonProperty("TxnsType")
    @SerializedName("TxnsType")
    private String txnsType;

    @JsonProperty("Rrn")
    @SerializedName("Rrn")
    private String rrn;

    @JsonProperty("ChineseErrorMessage")
    @SerializedName("ChineseErrorMessage")
    private String chineseErrorMessage;

    @JsonProperty("CompanyID")
    @SerializedName("CompanyID")
    private String companyID;

    @JsonProperty("AutoLoadActiveEnable")
    @SerializedName("AutoLoadActiveEnable")
    private String autoLoadActiveEnable;

    @JsonProperty("ErrorMessage")
    @SerializedName("ErrorMessage")
    private String errorMessage;

    @JsonProperty("PosInvoiceNumber")
    @SerializedName("PosInvoiceNumber")
    private String posInvoiceNumber;

    @JsonProperty("MaxValue")
    @SerializedName("MaxValue")
    private String maxValue;

    @JsonProperty("LockCardActiveEnable")
    @SerializedName("LockCardActiveEnable")
    private String lockCardActiveEnable;

    @JsonProperty("CardType")
    @SerializedName("CardType")
    private String cardType;

    @JsonProperty("OpenAutoLoadActiveAmount")
    @SerializedName("OpenAutoLoadActiveAmount")
    private String openAutoLoadActiveAmount;

    @JsonProperty("EVRecoveryActiveAmount")
    @SerializedName("EVRecoveryActiveAmount")
    private String eVRecoveryActiveAmount;

    @JsonProperty("Air")
    @SerializedName("Air")
    private String air;

    @JsonProperty("AutoLoadActiveAmount")
    @SerializedName("AutoLoadActiveAmount")
    private String autoLoadActiveAmount;

    @JsonProperty("EVRecoveryActiveEnable")
    @SerializedName("EVRecoveryActiveEnable")
    private String eVRecoveryActiveEnable;

    @JsonProperty("CardDesc")
    @SerializedName("CardDesc")
    private String cardDesc;

    @JsonProperty("EZCardID")
    @SerializedName("EZCardID")
    private String ezCardId;

    @JsonProperty("OrderId")
    @SerializedName("OrderId")
    private String orderId;

    @JsonProperty("AutoTopUpAmount")
    @SerializedName("AutoTopUpAmount")
    private String autoTopUpAmount;

    public String getDataTime() {
        return dataTime;
    }

    public void setDataTime(String dataTime) {
        this.dataTime = dataTime;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getNewAESKey() {
        return newAESKey;
    }

    public void setNewAESKey(String newAESKey) {
        this.newAESKey = newAESKey;
    }

    public String getAutoloadFlag() {
        return autoloadFlag;
    }

    public void setAutoloadFlag(String autoloadFlag) {
        this.autoloadFlag = autoloadFlag;
    }

    public String getAdviceCount() {
        return adviceCount;
    }

    public void setAdviceCount(String adviceCount) {
        this.adviceCount = adviceCount;
    }

    public String getBackupHostAddress() {
        return backupHostAddress;
    }

    public void setBackupHostAddress(String backupHostAddress) {
        this.backupHostAddress = backupHostAddress;
    }

    public String getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(String cardStatus) {
        this.cardStatus = cardStatus;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMainHostPort() {
        return mainHostPort;
    }

    public void setMainHostPort(String mainHostPort) {
        this.mainHostPort = mainHostPort;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getOpenAutoLoadActiveEnable() {
        return openAutoLoadActiveEnable;
    }

    public void setOpenAutoLoadActiveEnable(String openAutoLoadActiveEnable) {
        this.openAutoLoadActiveEnable = openAutoLoadActiveEnable;
    }

    public String getBackupHostPort() {
        return backupHostPort;
    }

    public void setBackupHostPort(String backupHostPort) {
        this.backupHostPort = backupHostPort;
    }

    public String getCloseAutoLoadActiveEnable() {
        return closeAutoLoadActiveEnable;
    }

    public void setCloseAutoLoadActiveEnable(String closeAutoLoadActiveEnable) {
        this.closeAutoLoadActiveEnable = closeAutoLoadActiveEnable;
    }

    public String getFuncOpenFlag() {
        return funcOpenFlag;
    }

    public void setFuncOpenFlag(String funcOpenFlag) {
        this.funcOpenFlag = funcOpenFlag;
    }

    public String getTxnsReturn() {
        return txnsReturn;
    }

    public void setTxnsReturn(String txnsReturn) {
        this.txnsReturn = txnsReturn;
    }

    public String getProfileExpiry() {
        return profileExpiry;
    }

    public void setProfileExpiry(String profileExpiry) {
        this.profileExpiry = profileExpiry;
    }

    public List<TxDataInFo> getTxDataInFoList() {
        return txDataInFoList;
    }

    public void setTxDataInFoList(List<TxDataInFo> txDataInFoList) {
        this.txDataInFoList = txDataInFoList;
    }

    public String getCardTxnSerialNumber() {
        return cardTxnSerialNumber;
    }

    public void setCardTxnSerialNumber(String cardTxnSerialNumber) {
        this.cardTxnSerialNumber = cardTxnSerialNumber;
    }

    public String getChangeCardActiveEnable() {
        return changeCardActiveEnable;
    }

    public void setChangeCardActiveEnable(String changeCardActiveEnable) {
        this.changeCardActiveEnable = changeCardActiveEnable;
    }

    public String getPosOperatorID() {
        return posOperatorID;
    }

    public void setPosOperatorID(String posOperatorID) {
        this.posOperatorID = posOperatorID;
    }

    public String getBeforeTXNBalance() {
        return beforeTXNBalance;
    }

    public void setBeforeTXNBalance(String beforeTXNBalance) {
        this.beforeTXNBalance = beforeTXNBalance;
    }

    public String getPosSerialNumber() {
        return posSerialNumber;
    }

    public void setPosSerialNumber(String posSerialNumber) {
        this.posSerialNumber = posSerialNumber;
    }

    public List<SettlementData> getSettlementDataList() {
        return settlementDataList;
    }

    public void setSettlementDataList(List<SettlementData> settlementDataList) {
        this.settlementDataList = settlementDataList;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getSamIDLen() {
        return samIDLen;
    }

    public void setSamIDLen(String samIDLen) {
        this.samIDLen = samIDLen;
    }

    public String gettXNResult() {
        return tXNResult;
    }

    public void settXNResult(String tXNResult) {
        this.tXNResult = tXNResult;
    }

    public String getShopID() {
        return shopID;
    }

    public void setShopID(String shopID) {
        this.shopID = shopID;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getChangeCardActiveAmount() {
        return changeCardActiveAmount;
    }

    public void setChangeCardActiveAmount(String changeCardActiveAmount) {
        this.changeCardActiveAmount = changeCardActiveAmount;
    }

    public String getSamID() {
        return samID;
    }

    public void setSamID(String samID) {
        this.samID = samID;
    }

    public String getEquipID() {
        return equipID;
    }

    public void setEquipID(String equipID) {
        this.equipID = equipID;
    }

    public String getRecordDeductAllLen() {
        return recordDeductAllLen;
    }

    public void setRecordDeductAllLen(String recordDeductAllLen) {
        this.recordDeductAllLen = recordDeductAllLen;
    }

    public String getCardSerialNo() {
        return cardSerialNo;
    }

    public void setCardSerialNo(String cardSerialNo) {
        this.cardSerialNo = cardSerialNo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPurchaseOneDayValue() {
        return purchaseOneDayValue;
    }

    public void setPurchaseOneDayValue(String purchaseOneDayValue) {
        this.purchaseOneDayValue = purchaseOneDayValue;
    }

    public String getMaxValueTxn() {
        return maxValueTxn;
    }

    public void setMaxValueTxn(String maxValueTxn) {
        this.maxValueTxn = maxValueTxn;
    }

    public String getCardNumberForPrint() {
        return cardNumberForPrint;
    }

    public void setCardNumberForPrint(String cardNumberForPrint) {
        this.cardNumberForPrint = cardNumberForPrint;
    }

    public String getCloseAutoLoadActiveAmount() {
        return closeAutoLoadActiveAmount;
    }

    public void setCloseAutoLoadActiveAmount(String closeAutoLoadActiveAmount) {
        this.closeAutoLoadActiveAmount = closeAutoLoadActiveAmount;
    }

    public String getAutoloadUnit() {
        return autoloadUnit;
    }

    public void setAutoloadUnit(String autoloadUnit) {
        this.autoloadUnit = autoloadUnit;
    }

    public String getTxnCompanyID() {
        return txnCompanyID;
    }

    public void setTxnCompanyID(String txnCompanyID) {
        this.txnCompanyID = txnCompanyID;
    }

    public String getMcErrorCode() {
        return mcErrorCode;
    }

    public void setMcErrorCode(String mcErrorCode) {
        this.mcErrorCode = mcErrorCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getLockCardActiveAmount() {
        return lockCardActiveAmount;
    }

    public void setLockCardActiveAmount(String lockCardActiveAmount) {
        this.lockCardActiveAmount = lockCardActiveAmount;
    }

    public String getBankID() {
        return bankID;
    }

    public void setBankID(String bankID) {
        this.bankID = bankID;
    }

    public String getIssuerCode() {
        return issuerCode;
    }

    public void setIssuerCode(String issuerCode) {
        this.issuerCode = issuerCode;
    }

    public String getRegisterFlag() {
        return registerFlag;
    }

    public void setRegisterFlag(String registerFlag) {
        this.registerFlag = registerFlag;
    }

    public String getFieldFlags() {
        return fieldFlags;
    }

    public void setFieldFlags(String fieldFlags) {
        this.fieldFlags = fieldFlags;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getApiVer() {
        return apiVer;
    }

    public void setApiVer(String apiVer) {
        this.apiVer = apiVer;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getProfileType() {
        return profileType;
    }

    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getaICMID() {
        return aICMID;
    }

    public void setaICMID(String aICMID) {
        this.aICMID = aICMID;
    }

    public String getMainHostAddress() {
        return mainHostAddress;
    }

    public void setMainHostAddress(String mainHostAddress) {
        this.mainHostAddress = mainHostAddress;
    }

    public String getaICID() {
        return aICID;
    }

    public void setaICID(String aICID) {
        this.aICID = aICID;
    }

    public String getTxnsType() {
        return txnsType;
    }

    public void setTxnsType(String txnsType) {
        this.txnsType = txnsType;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getChineseErrorMessage() {
        return chineseErrorMessage;
    }

    public void setChineseErrorMessage(String chineseErrorMessage) {
        this.chineseErrorMessage = chineseErrorMessage;
    }

    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getAutoLoadActiveEnable() {
        return autoLoadActiveEnable;
    }

    public void setAutoLoadActiveEnable(String autoLoadActiveEnable) {
        this.autoLoadActiveEnable = autoLoadActiveEnable;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getPosInvoiceNumber() {
        return posInvoiceNumber;
    }

    public void setPosInvoiceNumber(String posInvoiceNumber) {
        this.posInvoiceNumber = posInvoiceNumber;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public String getLockCardActiveEnable() {
        return lockCardActiveEnable;
    }

    public void setLockCardActiveEnable(String lockCardActiveEnable) {
        this.lockCardActiveEnable = lockCardActiveEnable;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getOpenAutoLoadActiveAmount() {
        return openAutoLoadActiveAmount;
    }

    public void setOpenAutoLoadActiveAmount(String openAutoLoadActiveAmount) {
        this.openAutoLoadActiveAmount = openAutoLoadActiveAmount;
    }

    public String geteVRecoveryActiveAmount() {
        return eVRecoveryActiveAmount;
    }

    public void seteVRecoveryActiveAmount(String eVRecoveryActiveAmount) {
        this.eVRecoveryActiveAmount = eVRecoveryActiveAmount;
    }

    public String getAir() {
        return air;
    }

    public void setAir(String air) {
        this.air = air;
    }

    public String getAutoLoadActiveAmount() {
        return autoLoadActiveAmount;
    }

    public void setAutoLoadActiveAmount(String autoLoadActiveAmount) {
        this.autoLoadActiveAmount = autoLoadActiveAmount;
    }

    public String geteVRecoveryActiveEnable() {
        return eVRecoveryActiveEnable;
    }

    public void seteVRecoveryActiveEnable(String eVRecoveryActiveEnable) {
        this.eVRecoveryActiveEnable = eVRecoveryActiveEnable;
    }

    public String getCardDesc() {
        return cardDesc;
    }

    public void setCardDesc(String cardDesc) {
        this.cardDesc = cardDesc;
    }

    public String getEzCardId() {
        return ezCardId;
    }

    public void setEzCardId(String ezCardId) {
        this.ezCardId = ezCardId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getAutoTopUpAmount() {
        return autoTopUpAmount;
    }

    public void setAutoTopUpAmount(String autoTopUpAmount) {
        this.autoTopUpAmount = autoTopUpAmount;
    }
}
