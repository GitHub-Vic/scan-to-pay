package co.intella.domain.iPass;

import com.google.gson.annotations.SerializedName;

public class IPassTradeResponse extends IPassGeneralResponse {

    @SerializedName("Time")
    private String time;

    @SerializedName("Date")
    private String date;

    @SerializedName("CardSN")
    private String cardSN;

    @SerializedName("CardSNFull")
    private String cardSNFull;

    @SerializedName("BeforeBalance")
    private String beforeBalance;

    @SerializedName("Amount")
    private String amount;

    @SerializedName("AfterBalance")
    private String afterBalance;

    @SerializedName("AutoTopUpAmount")
    private String autoTopUpAmount;

    @SerializedName("CardType")
    private String cardType;

    @SerializedName("IdentityType")
    private String identityType;

    @SerializedName("IdentityExpiryDate")
    private String identityExpiryDate;

    @SerializedName("SpecialCardType")
    private String specialCardType;

    @SerializedName("SpecialIdentityProvider")
    private String specialIdentityProvider;

    @SerializedName("SpecialIdentityType")
    private String specialIdentityType;

    @SerializedName("SpecialIdentityExpireDate")
    private String specialIdentityExpireDate;

    @SerializedName("SpecialIdentityUsagePointLimit")
    private String specialIdentityUsagePointLimit;

    @SerializedName("UsageAccumulatedPointBeforeTxn")
    private String usageAccumulatedPointBeforeTxn;

    @SerializedName("SpecialIdentityResetDate")
    private String specialIdentityResetDate;

    @SerializedName("AutoloadFlag")
    private String autoloadFlag;

    @SerializedName("AutoloadUnit")
    private String autoloadUnit;

    @SerializedName("SpecialIdentityTxnPoint")
    private String specialIdentityTxnPoint;

    @SerializedName("CarFleetName")
    private String carFleetName;

    @SerializedName("LicensePlate")
    private String licensePlate;

    @SerializedName("DriverNumber")
    private String driverNumber;

    @SerializedName("OrderId")
    private String orderId;

    @SerializedName("PointBalance")
    private String pointBalance;

    @SerializedName("PaidAmount")
    private String paidAmount;

    @SerializedName("TerminalID")
    private String terminalID;

    @SerializedName("DeviceID")
    private String deviceID;

    @SerializedName("POSTradeSN")
    private String postradeSN;

    @SerializedName("Automatic")
    private String automatic;

    @SerializedName("cardId")
    private String cardId;

    @SerializedName("cardIdDscr")
    private String cardIdDscr;

    @SerializedName("Beforepoint")
    private String beforepoint;

    @SerializedName("DiscountAmount")
    private String discountAmount;

    @SerializedName("NeedBalance")
    private String needBalance;

    @SerializedName("Rrn")
    private String rrn;

    @SerializedName("Total")
    private String total;

    public String getNeedBalance() {
        return needBalance;
    }

    public void setNeedBalance(String needBalance) {
        this.needBalance = needBalance;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getBeforepoint() {
        return beforepoint;
    }

    public void setBeforepoint(String beforepoint) {
        this.beforepoint = beforepoint;
    }

    private IPassTradeRequest request;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCardSN() {
        return cardSN;
    }

    public void setCardSN(String cardSN) {
        this.cardSN = cardSN;
    }

    public String getCardSNFull() {
        return cardSNFull;
    }

    public void setCardSNFull(String cardSNFull) {
        this.cardSNFull = cardSNFull;
    }

    public String getBeforeBalance() {
        return beforeBalance;
    }

    public void setBeforeBalance(String beforeBalance) {
        this.beforeBalance = beforeBalance;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAfterBalance() {
        return afterBalance;
    }

    public void setAfterBalance(String afterBalance) {
        this.afterBalance = afterBalance;
    }

    public String getAutoTopUpAmount() {
        return autoTopUpAmount;
    }

    public void setAutoTopUpAmount(String autoTopUpAmount) {
        this.autoTopUpAmount = autoTopUpAmount;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getIdentityType() {
        return identityType;
    }

    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }

    public String getIdentityExpiryDate() {
        return identityExpiryDate;
    }

    public void setIdentityExpiryDate(String identityExpiryDate) {
        this.identityExpiryDate = identityExpiryDate;
    }

    public String getSpecialCardType() {
        return specialCardType;
    }

    public void setSpecialCardType(String specialCardType) {
        this.specialCardType = specialCardType;
    }

    public String getSpecialIdentityProvider() {
        return specialIdentityProvider;
    }

    public void setSpecialIdentityProvider(String specialIdentityProvider) {
        this.specialIdentityProvider = specialIdentityProvider;
    }

    public String getSpecialIdentityType() {
        return specialIdentityType;
    }

    public void setSpecialIdentityType(String specialIdentityType) {
        this.specialIdentityType = specialIdentityType;
    }

    public String getSpecialIdentityExpireDate() {
        return specialIdentityExpireDate;
    }

    public void setSpecialIdentityExpireDate(String specialIdentityExpireDate) {
        this.specialIdentityExpireDate = specialIdentityExpireDate;
    }

    public String getSpecialIdentityUsagePointLimit() {
        return specialIdentityUsagePointLimit;
    }

    public void setSpecialIdentityUsagePointLimit(String specialIdentityUsagePointLimit) {
        this.specialIdentityUsagePointLimit = specialIdentityUsagePointLimit;
    }

    public String getUsageAccumulatedPointBeforeTxn() {
        return usageAccumulatedPointBeforeTxn;
    }

    public void setUsageAccumulatedPointBeforeTxn(String usageAccumulatedPointBeforeTxn) {
        this.usageAccumulatedPointBeforeTxn = usageAccumulatedPointBeforeTxn;
    }

    public String getSpecialIdentityResetDate() {
        return specialIdentityResetDate;
    }

    public void setSpecialIdentityResetDate(String specialIdentityResetDate) {
        this.specialIdentityResetDate = specialIdentityResetDate;
    }

    public String getAutoloadFlag() {
        return autoloadFlag;
    }

    public void setAutoloadFlag(String autoloadFlag) {
        this.autoloadFlag = autoloadFlag;
    }

    public String getAutoloadUnit() {
        return autoloadUnit;
    }

    public void setAutoloadUnit(String autoloadUnit) {
        this.autoloadUnit = autoloadUnit;
    }

    public String getCarFleetName() {
        return carFleetName;
    }

    public void setCarFleetName(String carFleetName) {
        this.carFleetName = carFleetName;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getDriverNumber() {
        return driverNumber;
    }

    public void setDriverNumber(String driverNumber) {
        this.driverNumber = driverNumber;
    }

    public IPassTradeRequest getRequest() {
        return request;
    }

    public void setRequest(IPassTradeRequest request) {
        this.request = request;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getSpecialIdentityTxnPoint() {
        return specialIdentityTxnPoint;
    }

    public void setSpecialIdentityTxnPoint(String specialIdentityTxnPoint) {
        this.specialIdentityTxnPoint = specialIdentityTxnPoint;
    }

    public String getPointBalance() {
        return pointBalance;
    }

    public void setPointBalance(String pointBalance) {
        this.pointBalance = pointBalance;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getPostradeSN() {
        return postradeSN;
    }

    public void setPostradeSN(String postradeSN) {
        this.postradeSN = postradeSN;
    }

    public String getAutomatic() {
        return automatic;
    }

    public void setAutomatic(String automatic) {
        this.automatic = automatic;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardIdDscr() {
        return cardIdDscr;
    }

    public void setCardIdDscr(String cardIdDscr) {
        this.cardIdDscr = cardIdDscr;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
