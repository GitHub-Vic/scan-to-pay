package co.intella.domain.iPass;

import com.google.gson.annotations.SerializedName;

public class IPassErrorResponse extends IPassGeneralResponse {

    @SerializedName("ErrorMessage")
    private String errorMessage;

    @SerializedName("ServiceType")
    private String serviceType;

    @SerializedName("MerchantId")
    private String merchantId;

    @SerializedName("DeviceId")
    private String deviceId;

    @SerializedName("Total")
    private String total;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
