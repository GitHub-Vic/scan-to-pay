package co.intella.domain.iPass;

import com.google.gson.annotations.SerializedName;

public class IPassFindCardResponse extends IPassGeneralResponse {
    @SerializedName("CardID")
    private String cardID;

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }
}
