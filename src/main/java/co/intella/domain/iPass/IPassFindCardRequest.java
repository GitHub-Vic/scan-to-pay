package co.intella.domain.iPass;

public class IPassFindCardRequest extends IPassQueryRequest {
    public IPassFindCardRequest() {
        this.setServiceType("FindCard");
    }
}
