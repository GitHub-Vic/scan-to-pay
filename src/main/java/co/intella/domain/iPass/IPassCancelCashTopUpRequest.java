package co.intella.domain.iPass;

import co.intella.model.Device;
import co.intella.model.PaymentAccount;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class IPassCancelCashTopUpRequest extends IPassBaseRequest {

    @JsonProperty("AccountNum")
    @SerializedName("AccountNum")
    private String accountNum = "";

    @JsonProperty("AccountNumLen")
    @SerializedName("AccountNumLen")
    private String accountNumLen = "";


    @JsonProperty("PollingCard")
    @SerializedName("PollingCard")
    private String pollingCard = "";
    /**
     * 基礎初始化資料，並set  device 的 postradesn、lastusedate
     *
     * @param testMode
     * @param device
     * @param paymentAccount
     */
    public IPassCancelCashTopUpRequest(String testMode, Device device, PaymentAccount paymentAccount) {
        super(testMode, device, paymentAccount);
        this.setServiceType("CancelCashTopUp");
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public String getAccountNumLen() {
        return accountNumLen;
    }

    public void setAccountNumLen(String accountNumLen) {
        this.accountNumLen = accountNumLen;
    }

    public String getPollingCard() {
        return pollingCard;
    }

    public void setPollingCard(String pollingCard) {
        this.pollingCard = pollingCard;
    }
}
