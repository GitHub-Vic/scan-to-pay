package co.intella.domain.iPass;

import co.intella.model.Device;
import co.intella.model.Merchant;
import co.intella.model.PaymentAccount;

public class IPassTermAuthRequest extends IPassBaseRequest {

    /**
     * 基礎初始化資料，並set  device 的 postradesn、lastusedate
     *
     * @param testMode
     * @param device
     * @param paymentAccount
     */
    public IPassTermAuthRequest(String testMode, Device device, PaymentAccount paymentAccount, String termIP, Merchant merchant) {
        super(testMode, device, paymentAccount);
        this.setCompanyID(paymentAccount.getAppId());
        this.setShopID(paymentAccount.getAccountTerminalId());
        this.setServiceType("TermAuth");
        this.setJointBankID("00000000");
        this.setaICID("0000");
        this.setStrAICMID("00000000000000000000000000000000");
        this.setTermSN(device.getPosCode());
        this.setTermIP(termIP);
        this.setPinCode(merchant.getComId());

        if ("1".equals(testMode) && "super".equals(merchant.getAccountId())) {
            this.setPinCode("12345678");
        }
    }

}
