package co.intella.domain.iPass;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class SettlementData {

    @JsonProperty("BatchNumber1")
    @SerializedName("BatchNumber1")
    private String batchNumber1;

    @JsonProperty("TotalCounter1")
    @SerializedName("TotalCounter1")
    private String totalCounter1;

    @JsonProperty("TotalAmount1")
    @SerializedName("TotalAmount1")
    private String totalAmount1;

    @JsonProperty("QueryTicketCounter1")
    @SerializedName("QueryTicketCounter1")
    private String queryTicketCounter1;

    @JsonProperty("QueryTicketAmount1")
    @SerializedName("QueryTicketAmount1")
    private String queryTicketAmount1;

    @JsonProperty("PurchaseCounter1")
    @SerializedName("PurchaseCounter1")
    private String purchaseCounter1;

    @JsonProperty("PurchaseAmount1")
    @SerializedName("PurchaseAmount1")
    private String purchaseAmount1;

    @JsonProperty("CancelPurchaseCounter1")
    @SerializedName("CancelPurchaseCounter1")
    private String cancelPurchaseCounter1;

    @JsonProperty("CancelPurchaseAmount1")
    @SerializedName("CancelPurchaseAmount1")
    private String cancelPurchaseAmount1;

    @JsonProperty("CancelAddCounter1")
    @SerializedName("CancelAddCounter1")
    private String cancelAddCounter1;

    @JsonProperty("CancelAddAmount1")
    @SerializedName("CancelAddAmount1")
    private String cancelAddAmount1;

    @JsonProperty("AddValueAutoloadCounter1")
    @SerializedName("AddValueAutoloadCounter1")
    private String addValueAutoloadCounter1;

    @JsonProperty("AddValueAutoloadAmount1")
    @SerializedName("AddValueAutoloadAmount1")
    private String addValueAutoloadAmount1;

    @JsonProperty("AddValueCashCounter1")
    @SerializedName("AddValueCashCounter1")
    private String addValueCashCounter1;

    @JsonProperty("AddValueCashAmount1")
    @SerializedName("AddValueCashAmount1")
    private String addValueCashAmount1;

    @JsonProperty("AddValueRefundCounter1")
    @SerializedName("AddValueRefundCounter1")
    private String addValueRefundCounter1;

    @JsonProperty("AddValueRefundAmount1")
    @SerializedName("AddValueRefundAmount1")
    private String addValueRefundAmount1;

    @JsonProperty("AddValueReturnCounter1")
    @SerializedName("AddValueReturnCounter1")
    private String addValueReturnCounter1;

    @JsonProperty("AddValueReturnAmount1")
    @SerializedName("AddValueReturnAmount1")
    private String addValueReturnAmount1;

    @JsonProperty("EVRecoveryCounter1")
    @SerializedName("EVRecoveryCounter1")
    private String eVRecoveryCounter1;

    @JsonProperty("EVRecoveryAmount1")
    @SerializedName("EVRecoveryAmount1")
    private String eVRecoveryAmount1;

    @JsonProperty("AutoloadOffCounter1")
    @SerializedName("AutoloadOffCounter1")
    private String autoloadOffCounter1;

    @JsonProperty("AutoloadOffAmount1")
    @SerializedName("AutoloadOffAmount1")
    private String autoloadOffAmount1;

    @JsonProperty("AutoloadOnCounter1")
    @SerializedName("AutoloadOnCounter1")
    private String autoloadOnCounter1;

    @JsonProperty("AutoloadOnAmount1")
    @SerializedName("AutoloadOnAmount1")
    private String autoloadOnAmount1;

    @JsonProperty("LockCardCounter1")
    @SerializedName("LockCardCounter1")
    private String lockCardCounter1;

    @JsonProperty("LockCardAmount1")
    @SerializedName("LockCardAmount1")
    private String lockCardAmount1;

    @JsonProperty("ChangeCardCounter1")
    @SerializedName("ChangeCardCounter1")
    private String changeCardCounter1;

    @JsonProperty("ChangeCardAmount1")
    @SerializedName("ChangeCardAmount1")
    private String changeCardAmount1;

    @JsonProperty("AutoloadAddCounter1")
    @SerializedName("AutoloadAddCounter1")
    private String autoloadAddCounter1;

    @JsonProperty("AutoloadAddAmount1")
    @SerializedName("AutoloadAddAmount1")
    private String autoloadAddAmount1;


    @JsonProperty("BatchNumber2")
    @SerializedName("BatchNumber2")
    private String batchNumber2;

    @JsonProperty("TotalCounter2")
    @SerializedName("TotalCounter2")
    private String totalCounter2;

    @JsonProperty("TotalAmount2")
    @SerializedName("TotalAmount2")
    private String totalAmount2;

    @JsonProperty("QueryTicketCounter2")
    @SerializedName("QueryTicketCounter2")
    private String queryTicketCounter2;

    @JsonProperty("QueryTicketAmount2")
    @SerializedName("QueryTicketAmount2")
    private String queryTicketAmount2;

    @JsonProperty("PurchaseCounter2")
    @SerializedName("PurchaseCounter2")
    private String purchaseCounter2;

    @JsonProperty("PurchaseAmount2")
    @SerializedName("PurchaseAmount2")
    private String purchaseAmount2;

    @JsonProperty("CancelPurchaseCounter2")
    @SerializedName("CancelPurchaseCounter2")
    private String cancelPurchaseCounter2;

    @JsonProperty("CancelPurchaseAmount2")
    @SerializedName("CancelPurchaseAmount2")
    private String cancelPurchaseAmount2;

    @JsonProperty("CancelAddCounter2")
    @SerializedName("CancelAddCounter2")
    private String cancelAddCounter2;

    @JsonProperty("CancelAddAmount2")
    @SerializedName("CancelAddAmount2")
    private String cancelAddAmount2;

    @JsonProperty("AddValueAutoloadCounter2")
    @SerializedName("AddValueAutoloadCounter2")
    private String addValueAutoloadCounter2;

    @JsonProperty("AddValueAutoloadAmount2")
    @SerializedName("AddValueAutoloadAmount2")
    private String addValueAutoloadAmount2;

    @JsonProperty("AddValueCashCounter2")
    @SerializedName("AddValueCashCounter2")
    private String addValueCashCounter2;

    @JsonProperty("AddValueCashAmount2")
    @SerializedName("AddValueCashAmount2")
    private String addValueCashAmount2;

    @JsonProperty("AddValueRefundCounter2")
    @SerializedName("AddValueRefundCounter2")
    private String addValueRefundCounter2;

    @JsonProperty("AddValueRefundAmount2")
    @SerializedName("AddValueRefundAmount2")
    private String addValueRefundAmount2;

    @JsonProperty("AddValueReturnCounter2")
    @SerializedName("AddValueReturnCounter2")
    private String addValueReturnCounter2;

    @JsonProperty("AddValueReturnAmount2")
    @SerializedName("AddValueReturnAmount2")
    private String addValueReturnAmount2;

    @JsonProperty("EVRecoveryCounter2")
    @SerializedName("EVRecoveryCounter2")
    private String eVRecoveryCounter2;

    @JsonProperty("EVRecoveryAmount2")
    @SerializedName("EVRecoveryAmount2")
    private String eVRecoveryAmount2;

    @JsonProperty("AutoloadOffCounter2")
    @SerializedName("AutoloadOffCounter2")
    private String autoloadOffCounter2;

    @JsonProperty("AutoloadOffAmount2")
    @SerializedName("AutoloadOffAmount2")
    private String autoloadOffAmount2;

    @JsonProperty("AutoloadOnCounter2")
    @SerializedName("AutoloadOnCounter2")
    private String autoloadOnCounter2;

    @JsonProperty("AutoloadOnAmount2")
    @SerializedName("AutoloadOnAmount2")
    private String autoloadOnAmount2;

    @JsonProperty("LockCardCounter2")
    @SerializedName("LockCardCounter2")
    private String lockCardCounter2;

    @JsonProperty("LockCardAmount2")
    @SerializedName("LockCardAmount2")
    private String lockCardAmount2;

    @JsonProperty("ChangeCardCounter2")
    @SerializedName("ChangeCardCounter2")
    private String changeCardCounter2;

    @JsonProperty("ChangeCardAmount2")
    @SerializedName("ChangeCardAmount2")
    private String changeCardAmount2;

    @JsonProperty("AutoloadAddCounter2")
    @SerializedName("AutoloadAddCounter2")
    private String autoloadAddCounter2;

    @JsonProperty("AutoloadAddAmount2")
    @SerializedName("AutoloadAddAmount2")
    private String autoloadAddAmount2;

    public String getBatchNumber1() {
        return batchNumber1;
    }

    public void setBatchNumber1(String batchNumber1) {
        this.batchNumber1 = batchNumber1;
    }

    public String getTotalCounter1() {
        return totalCounter1;
    }

    public void setTotalCounter1(String totalCounter1) {
        this.totalCounter1 = totalCounter1;
    }

    public String getTotalAmount1() {
        return totalAmount1;
    }

    public void setTotalAmount1(String totalAmount1) {
        this.totalAmount1 = totalAmount1;
    }

    public String getQueryTicketCounter1() {
        return queryTicketCounter1;
    }

    public void setQueryTicketCounter1(String queryTicketCounter1) {
        this.queryTicketCounter1 = queryTicketCounter1;
    }

    public String getQueryTicketAmount1() {
        return queryTicketAmount1;
    }

    public void setQueryTicketAmount1(String queryTicketAmount1) {
        this.queryTicketAmount1 = queryTicketAmount1;
    }

    public String getPurchaseCounter1() {
        return purchaseCounter1;
    }

    public void setPurchaseCounter1(String purchaseCounter1) {
        this.purchaseCounter1 = purchaseCounter1;
    }

    public String getPurchaseAmount1() {
        return purchaseAmount1;
    }

    public void setPurchaseAmount1(String purchaseAmount1) {
        this.purchaseAmount1 = purchaseAmount1;
    }

    public String getCancelPurchaseCounter1() {
        return cancelPurchaseCounter1;
    }

    public void setCancelPurchaseCounter1(String cancelPurchaseCounter1) {
        this.cancelPurchaseCounter1 = cancelPurchaseCounter1;
    }

    public String getCancelPurchaseAmount1() {
        return cancelPurchaseAmount1;
    }

    public void setCancelPurchaseAmount1(String cancelPurchaseAmount1) {
        this.cancelPurchaseAmount1 = cancelPurchaseAmount1;
    }

    public String getCancelAddCounter1() {
        return cancelAddCounter1;
    }

    public void setCancelAddCounter1(String cancelAddCounter1) {
        this.cancelAddCounter1 = cancelAddCounter1;
    }

    public String getCancelAddAmount1() {
        return cancelAddAmount1;
    }

    public void setCancelAddAmount1(String cancelAddAmount1) {
        this.cancelAddAmount1 = cancelAddAmount1;
    }

    public String getAddValueAutoloadCounter1() {
        return addValueAutoloadCounter1;
    }

    public void setAddValueAutoloadCounter1(String addValueAutoloadCounter1) {
        this.addValueAutoloadCounter1 = addValueAutoloadCounter1;
    }

    public String getAddValueAutoloadAmount1() {
        return addValueAutoloadAmount1;
    }

    public void setAddValueAutoloadAmount1(String addValueAutoloadAmount1) {
        this.addValueAutoloadAmount1 = addValueAutoloadAmount1;
    }

    public String getAddValueCashCounter1() {
        return addValueCashCounter1;
    }

    public void setAddValueCashCounter1(String addValueCashCounter1) {
        this.addValueCashCounter1 = addValueCashCounter1;
    }

    public String getAddValueCashAmount1() {
        return addValueCashAmount1;
    }

    public void setAddValueCashAmount1(String addValueCashAmount1) {
        this.addValueCashAmount1 = addValueCashAmount1;
    }

    public String getAddValueRefundCounter1() {
        return addValueRefundCounter1;
    }

    public void setAddValueRefundCounter1(String addValueRefundCounter1) {
        this.addValueRefundCounter1 = addValueRefundCounter1;
    }

    public String getAddValueRefundAmount1() {
        return addValueRefundAmount1;
    }

    public void setAddValueRefundAmount1(String addValueRefundAmount1) {
        this.addValueRefundAmount1 = addValueRefundAmount1;
    }

    public String getAddValueReturnCounter1() {
        return addValueReturnCounter1;
    }

    public void setAddValueReturnCounter1(String addValueReturnCounter1) {
        this.addValueReturnCounter1 = addValueReturnCounter1;
    }

    public String getAddValueReturnAmount1() {
        return addValueReturnAmount1;
    }

    public void setAddValueReturnAmount1(String addValueReturnAmount1) {
        this.addValueReturnAmount1 = addValueReturnAmount1;
    }

    public String geteVRecoveryCounter1() {
        return eVRecoveryCounter1;
    }

    public void seteVRecoveryCounter1(String eVRecoveryCounter1) {
        this.eVRecoveryCounter1 = eVRecoveryCounter1;
    }

    public String geteVRecoveryAmount1() {
        return eVRecoveryAmount1;
    }

    public void seteVRecoveryAmount1(String eVRecoveryAmount1) {
        this.eVRecoveryAmount1 = eVRecoveryAmount1;
    }

    public String getAutoloadOffCounter1() {
        return autoloadOffCounter1;
    }

    public void setAutoloadOffCounter1(String autoloadOffCounter1) {
        this.autoloadOffCounter1 = autoloadOffCounter1;
    }

    public String getAutoloadOffAmount1() {
        return autoloadOffAmount1;
    }

    public void setAutoloadOffAmount1(String autoloadOffAmount1) {
        this.autoloadOffAmount1 = autoloadOffAmount1;
    }

    public String getAutoloadOnCounter1() {
        return autoloadOnCounter1;
    }

    public void setAutoloadOnCounter1(String autoloadOnCounter1) {
        this.autoloadOnCounter1 = autoloadOnCounter1;
    }

    public String getAutoloadOnAmount1() {
        return autoloadOnAmount1;
    }

    public void setAutoloadOnAmount1(String autoloadOnAmount1) {
        this.autoloadOnAmount1 = autoloadOnAmount1;
    }

    public String getLockCardCounter1() {
        return lockCardCounter1;
    }

    public void setLockCardCounter1(String lockCardCounter1) {
        this.lockCardCounter1 = lockCardCounter1;
    }

    public String getLockCardAmount1() {
        return lockCardAmount1;
    }

    public void setLockCardAmount1(String lockCardAmount1) {
        this.lockCardAmount1 = lockCardAmount1;
    }

    public String getChangeCardCounter1() {
        return changeCardCounter1;
    }

    public void setChangeCardCounter1(String changeCardCounter1) {
        this.changeCardCounter1 = changeCardCounter1;
    }

    public String getChangeCardAmount1() {
        return changeCardAmount1;
    }

    public void setChangeCardAmount1(String changeCardAmount1) {
        this.changeCardAmount1 = changeCardAmount1;
    }

    public String getAutoloadAddCounter1() {
        return autoloadAddCounter1;
    }

    public void setAutoloadAddCounter1(String autoloadAddCounter1) {
        this.autoloadAddCounter1 = autoloadAddCounter1;
    }

    public String getAutoloadAddAmount1() {
        return autoloadAddAmount1;
    }

    public void setAutoloadAddAmount1(String autoloadAddAmount1) {
        this.autoloadAddAmount1 = autoloadAddAmount1;
    }

    public String getBatchNumber2() {
        return batchNumber2;
    }

    public void setBatchNumber2(String batchNumber2) {
        this.batchNumber2 = batchNumber2;
    }

    public String getTotalCounter2() {
        return totalCounter2;
    }

    public void setTotalCounter2(String totalCounter2) {
        this.totalCounter2 = totalCounter2;
    }

    public String getTotalAmount2() {
        return totalAmount2;
    }

    public void setTotalAmount2(String totalAmount2) {
        this.totalAmount2 = totalAmount2;
    }

    public String getQueryTicketCounter2() {
        return queryTicketCounter2;
    }

    public void setQueryTicketCounter2(String queryTicketCounter2) {
        this.queryTicketCounter2 = queryTicketCounter2;
    }

    public String getQueryTicketAmount2() {
        return queryTicketAmount2;
    }

    public void setQueryTicketAmount2(String queryTicketAmount2) {
        this.queryTicketAmount2 = queryTicketAmount2;
    }

    public String getPurchaseCounter2() {
        return purchaseCounter2;
    }

    public void setPurchaseCounter2(String purchaseCounter2) {
        this.purchaseCounter2 = purchaseCounter2;
    }

    public String getPurchaseAmount2() {
        return purchaseAmount2;
    }

    public void setPurchaseAmount2(String purchaseAmount2) {
        this.purchaseAmount2 = purchaseAmount2;
    }

    public String getCancelPurchaseCounter2() {
        return cancelPurchaseCounter2;
    }

    public void setCancelPurchaseCounter2(String cancelPurchaseCounter2) {
        this.cancelPurchaseCounter2 = cancelPurchaseCounter2;
    }

    public String getCancelPurchaseAmount2() {
        return cancelPurchaseAmount2;
    }

    public void setCancelPurchaseAmount2(String cancelPurchaseAmount2) {
        this.cancelPurchaseAmount2 = cancelPurchaseAmount2;
    }

    public String getCancelAddCounter2() {
        return cancelAddCounter2;
    }

    public void setCancelAddCounter2(String cancelAddCounter2) {
        this.cancelAddCounter2 = cancelAddCounter2;
    }

    public String getCancelAddAmount2() {
        return cancelAddAmount2;
    }

    public void setCancelAddAmount2(String cancelAddAmount2) {
        this.cancelAddAmount2 = cancelAddAmount2;
    }

    public String getAddValueAutoloadCounter2() {
        return addValueAutoloadCounter2;
    }

    public void setAddValueAutoloadCounter2(String addValueAutoloadCounter2) {
        this.addValueAutoloadCounter2 = addValueAutoloadCounter2;
    }

    public String getAddValueAutoloadAmount2() {
        return addValueAutoloadAmount2;
    }

    public void setAddValueAutoloadAmount2(String addValueAutoloadAmount2) {
        this.addValueAutoloadAmount2 = addValueAutoloadAmount2;
    }

    public String getAddValueCashCounter2() {
        return addValueCashCounter2;
    }

    public void setAddValueCashCounter2(String addValueCashCounter2) {
        this.addValueCashCounter2 = addValueCashCounter2;
    }

    public String getAddValueCashAmount2() {
        return addValueCashAmount2;
    }

    public void setAddValueCashAmount2(String addValueCashAmount2) {
        this.addValueCashAmount2 = addValueCashAmount2;
    }

    public String getAddValueRefundCounter2() {
        return addValueRefundCounter2;
    }

    public void setAddValueRefundCounter2(String addValueRefundCounter2) {
        this.addValueRefundCounter2 = addValueRefundCounter2;
    }

    public String getAddValueRefundAmount2() {
        return addValueRefundAmount2;
    }

    public void setAddValueRefundAmount2(String addValueRefundAmount2) {
        this.addValueRefundAmount2 = addValueRefundAmount2;
    }

    public String getAddValueReturnCounter2() {
        return addValueReturnCounter2;
    }

    public void setAddValueReturnCounter2(String addValueReturnCounter2) {
        this.addValueReturnCounter2 = addValueReturnCounter2;
    }

    public String getAddValueReturnAmount2() {
        return addValueReturnAmount2;
    }

    public void setAddValueReturnAmount2(String addValueReturnAmount2) {
        this.addValueReturnAmount2 = addValueReturnAmount2;
    }

    public String geteVRecoveryCounter2() {
        return eVRecoveryCounter2;
    }

    public void seteVRecoveryCounter2(String eVRecoveryCounter2) {
        this.eVRecoveryCounter2 = eVRecoveryCounter2;
    }

    public String geteVRecoveryAmount2() {
        return eVRecoveryAmount2;
    }

    public void seteVRecoveryAmount2(String eVRecoveryAmount2) {
        this.eVRecoveryAmount2 = eVRecoveryAmount2;
    }

    public String getAutoloadOffCounter2() {
        return autoloadOffCounter2;
    }

    public void setAutoloadOffCounter2(String autoloadOffCounter2) {
        this.autoloadOffCounter2 = autoloadOffCounter2;
    }

    public String getAutoloadOffAmount2() {
        return autoloadOffAmount2;
    }

    public void setAutoloadOffAmount2(String autoloadOffAmount2) {
        this.autoloadOffAmount2 = autoloadOffAmount2;
    }

    public String getAutoloadOnCounter2() {
        return autoloadOnCounter2;
    }

    public void setAutoloadOnCounter2(String autoloadOnCounter2) {
        this.autoloadOnCounter2 = autoloadOnCounter2;
    }

    public String getAutoloadOnAmount2() {
        return autoloadOnAmount2;
    }

    public void setAutoloadOnAmount2(String autoloadOnAmount2) {
        this.autoloadOnAmount2 = autoloadOnAmount2;
    }

    public String getLockCardCounter2() {
        return lockCardCounter2;
    }

    public void setLockCardCounter2(String lockCardCounter2) {
        this.lockCardCounter2 = lockCardCounter2;
    }

    public String getLockCardAmount2() {
        return lockCardAmount2;
    }

    public void setLockCardAmount2(String lockCardAmount2) {
        this.lockCardAmount2 = lockCardAmount2;
    }

    public String getChangeCardCounter2() {
        return changeCardCounter2;
    }

    public void setChangeCardCounter2(String changeCardCounter2) {
        this.changeCardCounter2 = changeCardCounter2;
    }

    public String getChangeCardAmount2() {
        return changeCardAmount2;
    }

    public void setChangeCardAmount2(String changeCardAmount2) {
        this.changeCardAmount2 = changeCardAmount2;
    }

    public String getAutoloadAddCounter2() {
        return autoloadAddCounter2;
    }

    public void setAutoloadAddCounter2(String autoloadAddCounter2) {
        this.autoloadAddCounter2 = autoloadAddCounter2;
    }

    public String getAutoloadAddAmount2() {
        return autoloadAddAmount2;
    }

    public void setAutoloadAddAmount2(String autoloadAddAmount2) {
        this.autoloadAddAmount2 = autoloadAddAmount2;
    }
}
