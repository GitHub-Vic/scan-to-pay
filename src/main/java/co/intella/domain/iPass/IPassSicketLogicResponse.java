package co.intella.domain.iPass;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class IPassSicketLogicResponse extends IPassGeneralResponse {

    @JsonProperty("Code")
    @SerializedName("Code")
    private String code;

    @JsonProperty("Msg")
    @SerializedName("Msg")
    private String msg;

    @JsonProperty("Total")
    @SerializedName("Total")
    private String total;

    @JsonProperty("Cash")
    @SerializedName("Cash")
    private String cash;

    @JsonProperty("NeedBalance")
    @SerializedName("NeedBalance")
    private String needBalance;

    @JsonProperty("Point")
    @SerializedName("Point")
    private String point;

    @JsonProperty("StoreOrderNo")
    @SerializedName("StoreOrderNo")
    private String storeOrderNo;

    @JsonProperty("QueryCardInfoSultData")
    @SerializedName("QueryCardInfoSultData")
    private String queryCardInfoSultData;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCash() {
        return cash;
    }

    public void setCash(String cash) {
        this.cash = cash;
    }

    public String getMoney() {
        return needBalance;
    }

    public void setMoney(String money) {
        this.needBalance = money;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getNeedBalance() {
        return needBalance;
    }

    public void setNeedBalance(String needBalance) {
        this.needBalance = needBalance;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getQueryCardInfoSultData() {
        return queryCardInfoSultData;
    }

    public void setQueryCardInfoSultData(String queryCardInfoSultData) {
        this.queryCardInfoSultData = queryCardInfoSultData;
    }
}
