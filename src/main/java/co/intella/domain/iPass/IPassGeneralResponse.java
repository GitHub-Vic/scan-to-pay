package co.intella.domain.iPass;

import co.intella.domain.integration.ResponseGeneralData;
import com.google.gson.annotations.SerializedName;

public class IPassGeneralResponse extends ResponseGeneralData {

    @SerializedName("TXNResult")
    private String txnResult;

    @SerializedName("ErrorCode")
    private String errorCode;

    @SerializedName("ReaderID")
    private String readerID;

    @SerializedName("APVersion")
    private String apVersion;

    @SerializedName("SAMID")
    private String samID;

    @SerializedName("NewAESKey")
    private String newAESKey;

    public String getTxnResult() {
        return txnResult;
    }

    public void setTxnResult(String txnResult) {
        this.txnResult = txnResult;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getReaderID() {
        return readerID;
    }

    public void setReaderID(String readerID) {
        this.readerID = readerID;
    }

    public String getApVersion() {
        return apVersion;
    }

    public void setApVersion(String apVersion) {
        this.apVersion = apVersion;
    }

    public String getSamID() {
        return samID;
    }

    public void setSamID(String samID) {
        this.samID = samID;
    }

    public String getNewAESKey() {
        return newAESKey;
    }

    public void setNewAESKey(String newAESKey) {
        this.newAESKey = newAESKey;
    }
}
