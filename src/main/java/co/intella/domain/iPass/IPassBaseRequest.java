package co.intella.domain.iPass;

import co.intella.model.Device;
import co.intella.model.PaymentAccount;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * sol  ，新增於ipass 小額 及 停車場用
 */
public class IPassBaseRequest {

    @JsonProperty("ServiceType")
    @SerializedName("ServiceType")
    private String serviceType = "";

    @JsonProperty("TestMode")
    @SerializedName("TestMode")
    private String testMode = "";

    @JsonProperty("TerminalID")
    @SerializedName("TerminalID")
    private String terminalID = "";

    @JsonProperty("DeviceID")
    @SerializedName("DeviceID")
    private String deviceID = "";

    @JsonProperty("Time")
    @SerializedName("Time")
    private String time = "";

    @JsonProperty("Date")
    @SerializedName("Date")
    private String date = "";

    @JsonProperty("TtyApiPort")
    @SerializedName("TtyApiPort")
    private String ttyApiPort = "";

    @JsonProperty("TtyServicePort")
    @SerializedName("TtyServicePort")
    private String ttyServicePort = "";

    @JsonProperty("CancelAddRrn")
    @SerializedName("CancelAddRrn")
    private String cancelAddRrn = "";

    @JsonProperty("Amount")
    @SerializedName("Amount")
    private String amount = "";

    @JsonProperty("PosSerialNumber")
    @SerializedName("PosSerialNumber")
    private String posSerialNumber = "";

    @JsonProperty("PosInvoiceNumber")
    @SerializedName("PosInvoiceNumber")
    private String posInvoiceNumber = "";

    @JsonProperty("PosOperatorID")
    @SerializedName("PosOperatorID")
    private String posOperatorID = "";

    @JsonProperty("FieldFlags")
    @SerializedName("FieldFlags")
    private String fieldFlags = "";

    @JsonProperty("AESKey")
    @SerializedName("AESKey")
    private String aesKey = "";

    @JsonProperty("CompanyID")
    @SerializedName("CompanyID")
    private String companyID = "";

    @JsonProperty("ShopID")
    @SerializedName("ShopID")
    private String shopID = "";

    @JsonProperty("JointBankID")
    @SerializedName("JointBankID")
    private String jointBankID = "";

    @JsonProperty("AICID")
    @SerializedName("AICID")
    private String aICID = "";

    @JsonProperty("StrAICMID")
    @SerializedName("StrAICMID")
    private String strAICMID = "";

    @JsonProperty("PinCode")
    @SerializedName("PinCode")
    private String pinCode = "";

    @JsonProperty("TermSN")
    @SerializedName("TermSN")
    private String termSN = "";

    @JsonProperty("TermIP")
    @SerializedName("TermIP")
    private String termIP = "";

    @JsonProperty("UARTMode")
    @SerializedName("UARTMode")
    private String uARTMode = "";

    /**
     * 基礎初始化資料，並set  device 的 postradesn、lastusedate
     *
     * @param testMode
     * @param device
     * @param paymentAccount
     */
    public IPassBaseRequest(String testMode, Device device, PaymentAccount paymentAccount) {
        LocalDateTime now = LocalDateTime.now();
        String date = now.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        String time = now.format(DateTimeFormatter.ofPattern("HHmmss"));

        this.setTestMode(testMode);
        this.setTerminalID(paymentAccount.getAccount());
        this.setDeviceID(device.getOwnerDeviceId());
        this.setTime(time);
        this.setDate(date);
        this.setCancelAddRrn("0");              //若執行取消現金加值/取消扣值交易，須填入此項目；若其他交易則全填0
        this.setPosOperatorID("ABC123");        //預設
        this.setFieldFlags("FFFFFFFF");         //預設
        this.setuARTMode("0");                  //預設

        this.setAesKey(device.getAesKey());
        this.setPosInvoiceNumber(date);         //為當天的交易批號，可從1開始，隔天加1，20碼英文 or 數字

        if (StringUtils.isNotEmpty(device.getLastusedate()) && date.equals(device.getLastusedate())) {
            this.setPosSerialNumber(String.format("%06d", Integer.valueOf(device.getPostradesn()) + 1));
            device.setPostradesn(String.valueOf(Integer.valueOf(device.getPostradesn()) + 1));
        } else {
            this.setPosSerialNumber(String.format("%06d", 1));
            device.setLastusedate(date);
            device.setPostradesn("1");
        }
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getTestMode() {
        return testMode;
    }

    public void setTestMode(String testMode) {
        this.testMode = testMode;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTtyApiPort() {
        return ttyApiPort;
    }

    public void setTtyApiPort(String ttyApiPort) {
        this.ttyApiPort = ttyApiPort;
    }

    public String getTtyServicePort() {
        return ttyServicePort;
    }

    public void setTtyServicePort(String ttyServicePort) {
        this.ttyServicePort = ttyServicePort;
    }

    public String getCancelAddRrn() {
        return cancelAddRrn;
    }

    public void setCancelAddRrn(String cancelAddRrn) {
        this.cancelAddRrn = cancelAddRrn;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPosSerialNumber() {
        return posSerialNumber;
    }

    public void setPosSerialNumber(String posSerialNumber) {
        this.posSerialNumber = posSerialNumber;
    }

    public String getPosInvoiceNumber() {
        return posInvoiceNumber;
    }

    public void setPosInvoiceNumber(String posInvoiceNumber) {
        this.posInvoiceNumber = posInvoiceNumber;
    }

    public String getPosOperatorID() {
        return posOperatorID;
    }

    public void setPosOperatorID(String posOperatorID) {
        this.posOperatorID = posOperatorID;
    }

    public String getFieldFlags() {
        return fieldFlags;
    }

    public void setFieldFlags(String fieldFlags) {
        this.fieldFlags = fieldFlags;
    }

    public String getAesKey() {
        return aesKey;
    }

    public void setAesKey(String aesKey) {
        this.aesKey = aesKey;
    }

    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getShopID() {
        return shopID;
    }

    public void setShopID(String shopID) {
        this.shopID = shopID;
    }

    public String getJointBankID() {
        return jointBankID;
    }

    public void setJointBankID(String jointBankID) {
        this.jointBankID = jointBankID;
    }

    public String getaICID() {
        return aICID;
    }

    public void setaICID(String aICID) {
        this.aICID = aICID;
    }

    public String getStrAICMID() {
        return strAICMID;
    }

    public void setStrAICMID(String strAICMID) {
        this.strAICMID = strAICMID;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getTermSN() {
        return termSN;
    }

    public void setTermSN(String termSN) {
        this.termSN = termSN;
    }

    public String getTermIP() {
        return termIP;
    }

    public void setTermIP(String termIP) {
        this.termIP = termIP;
    }

    public String getuARTMode() {
        return uARTMode;
    }

    public void setuARTMode(String uARTMode) {
        this.uARTMode = uARTMode;
    }

    @Override
    public String toString() {
        return "IPassBaseRequest{" +
                "serviceType='" + serviceType + '\'' +
                ", testMode='" + testMode + '\'' +
                ", terminalID='" + terminalID + '\'' +
                ", deviceID='" + deviceID + '\'' +
                ", time='" + time + '\'' +
                ", date='" + date + '\'' +
                ", ttyApiPort='" + ttyApiPort + '\'' +
                ", ttyServicePort='" + ttyServicePort + '\'' +
                ", cancelAddRrn='" + cancelAddRrn + '\'' +
                ", amount='" + amount + '\'' +
                ", posSerialNumber='" + posSerialNumber + '\'' +
                ", posInvoiceNumber='" + posInvoiceNumber + '\'' +
                ", posOperatorID='" + posOperatorID + '\'' +
                ", fieldFlags='" + fieldFlags + '\'' +
                ", aesKey='" + aesKey + '\'' +
                ", companyID='" + companyID + '\'' +
                ", shopID='" + shopID + '\'' +
                ", jointBankID='" + jointBankID + '\'' +
                ", aICID='" + aICID + '\'' +
                ", strAICMID='" + strAICMID + '\'' +
                ", pinCode='" + pinCode + '\'' +
                ", termSN='" + termSN + '\'' +
                ", termIP='" + termIP + '\'' +
                ", uARTMode='" + uARTMode + '\'' +
                '}';
    }
}
