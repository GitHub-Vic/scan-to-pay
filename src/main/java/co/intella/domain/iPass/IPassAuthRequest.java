package co.intella.domain.iPass;

import com.google.gson.annotations.SerializedName;

public class IPassAuthRequest extends IPassGeneralRequest {

    @SerializedName("POSTradeSN")
    private String posTradeSN;

    @SerializedName("ShopNumber")
    private String shopNumber;

    @SerializedName("SAMSlotNumber")
    private String samSlotNumber;

    @SerializedName("ClientIP")
    private String clientIP;

    public IPassAuthRequest() {
        this.setServiceType("TermAuth");
    }

    public String getPosTradeSN() {
        return posTradeSN;
    }

    public void setPosTradeSN(String posTradeSN) {
        this.posTradeSN = posTradeSN;
    }

    public String getShopNumber() {
        return shopNumber;
    }

    public void setShopNumber(String shopNumber) {
        this.shopNumber = shopNumber;
    }

    public String getSamSlotNumber() {
        return samSlotNumber;
    }

    public void setSamSlotNumber(String samSlotNumber) {
        this.samSlotNumber = samSlotNumber;
    }

    public String getClientIP() {
        return clientIP;
    }

    public void setClientIP(String clientIP) {
        this.clientIP = clientIP;
    }
}
