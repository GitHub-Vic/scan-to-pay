package co.intella.domain.iPass;

import co.intella.model.Device;
import co.intella.model.PaymentAccount;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class IPassTradeRefundRequest extends  IPassBaseRequest {
    /**
     * 基礎初始化資料，並set  device 的 postradesn、lastusedate
     *
     * @param testMode
     * @param device
     * @param paymentAccount
     */
    public IPassTradeRefundRequest(String testMode, Device device, PaymentAccount paymentAccount) {
        super(testMode, device, paymentAccount);
        this.setServiceType("RefundTopUp");   //取消扣值交易
    }
    @JsonProperty("AccountNum")
    @SerializedName("AccountNum")
    private String accountNum = "";

    @JsonProperty("AccountNumLen")
    @SerializedName("AccountNumLen")
    private String accountNumLen = "";

    @JsonProperty("PollingCard")
    @SerializedName("PollingCard")
    private String pollingCard = "";

}
