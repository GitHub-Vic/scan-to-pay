package co.intella.domain.iPass;

import co.intella.model.Device;
import co.intella.model.PaymentAccount;

public class IPassOfflineInquiryCardRequest extends IPassBaseRequest {
    /**
     * 基礎初始化資料，並set  device 的 postradesn、lastusedate
     *
     * @param testMode
     * @param device
     * @param paymentAccount
     */
    public IPassOfflineInquiryCardRequest(String testMode, Device device, PaymentAccount paymentAccount) {
        super(testMode, device, paymentAccount);
        this.setServiceType("OfflineInquiryCard");
    }
}
