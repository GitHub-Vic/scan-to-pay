package co.intella.domain.iPass;

import com.google.gson.annotations.SerializedName;

public class IPassQueryRequest extends IPassGeneralRequest {

    @SerializedName("LicensePlate")
    private String licensePlate;

    public IPassQueryRequest() {
        this.setServiceType("QueryCardInfo");
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }
}
