package co.intella.domain.iPass;

import com.google.gson.annotations.SerializedName;

public class IPassTradeRequest extends IPassGeneralRequest {

    @SerializedName("POSTradeSN")
    private String posTradeSN;

    @SerializedName("Amount")
    private String amount;

    @SerializedName("SpecialIdentityTxnPoint")
    private String specialIdentityTxnPoint;

    @SerializedName("SpecialIdentityPointRatio")
    private String specialIdentityPointRatio;

    @SerializedName("SpecialIdentityDiscountAmount")
    private String specialIdentityDiscountAmount;

    @SerializedName("PaidAmount")
    private String paidAmount;

    @SerializedName("FareFlag")
    private String fareFlag;

    @SerializedName("TransactionType")
    private String transactionType;

    @SerializedName("SpecialIdentityResetFlag")
    private String specialIdentityResetFlag;

    @SerializedName("NeedCash")
    private String needCash;

    @SerializedName("LicensePlate")
    private String licensePlate;

    public IPassTradeRequest() {
        this.setServiceType("TradeSale");
    }

    public String getPosTradeSN() {
        return posTradeSN;
    }

    public void setPosTradeSN(String posTradeSN) {
        this.posTradeSN = posTradeSN;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSpecialIdentityTxnPoint() {
        return specialIdentityTxnPoint;
    }

    public void setSpecialIdentityTxnPoint(String specialIdentityTxnPoint) {
        this.specialIdentityTxnPoint = specialIdentityTxnPoint;
    }

    public String getSpecialIdentityPointRatio() {
        return specialIdentityPointRatio;
    }

    public void setSpecialIdentityPointRatio(String specialIdentityPointRatio) {
        this.specialIdentityPointRatio = specialIdentityPointRatio;
    }

    public String getSpecialIdentityDiscountAmount() {
        return specialIdentityDiscountAmount;
    }

    public void setSpecialIdentityDiscountAmount(String specialIdentityDiscountAmount) {
        this.specialIdentityDiscountAmount = specialIdentityDiscountAmount;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getFareFlag() {
        return fareFlag;
    }

    public void setFareFlag(String fareFlag) {
        this.fareFlag = fareFlag;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getSpecialIdentityResetFlag() {
        return specialIdentityResetFlag;
    }

    public void setSpecialIdentityResetFlag(String specialIdentityResetFlag) {
        this.specialIdentityResetFlag = specialIdentityResetFlag;
    }

    public String getNeedCash() {
        return needCash;
    }

    public void setNeedCash(String needCash) {
        this.needCash = needCash;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }
}
