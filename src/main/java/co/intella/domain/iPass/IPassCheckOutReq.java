package co.intella.domain.iPass;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class IPassCheckOutReq extends IPassGeneralRequest{

    @JsonProperty("Count")
    @SerializedName("Count")
    private String count;

    @JsonProperty("Data")
    @SerializedName("Data")
    private ArrayList data;

    @JsonProperty("FileSN")
    @SerializedName("FileSN")
    private String fileSN;

    public IPassCheckOutReq() {
        this.setServiceType("Checkout");
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public ArrayList getData() {
        return data;
    }

    public void setData(ArrayList data) {
        this.data = data;
    }

    public String getFileSN() {
        return fileSN;
    }

    public void setFileSN(String fileSN) {
        this.fileSN = fileSN;
    }
}
