package co.intella.domain.iPass;

import com.google.gson.annotations.SerializedName;

public class IPassGeneralRequest {

    @SerializedName("ServiceType")
    private String serviceType;

    @SerializedName("TestMode")
    private String testMode;

    @SerializedName("UARTMode")
    private String uartMode;

    @SerializedName("SystemID")
    private String systemID;

    @SerializedName("SPID")
    private String spID;

    @SerializedName("Time")
    private String time;

    @SerializedName("Date")
    private String date;

    @SerializedName("TerminalID")
    private String terminalID;

    @SerializedName("DeviceID")
    private String deviceID;

    @SerializedName("MultiElectronicTicket")
    private String multiElectronicTicket;

    @SerializedName("AESKey")
    private String aesKey;

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getTestMode() {
        return testMode;
    }

    public void setTestMode(String testMode) {
        this.testMode = testMode;
    }

    public String getUartMode() {
        return uartMode;
    }

    public void setUartMode(String uartMode) {
        this.uartMode = uartMode;
    }

    public String getSystemID() {
        return systemID;
    }

    public void setSystemID(String systemID) {
        this.systemID = systemID;
    }

    public String getSpID() {
        return spID;
    }

    public void setSpID(String spID) {
        this.spID = spID;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getMultiElectronicTicket() {
        return multiElectronicTicket;
    }

    public void setMultiElectronicTicket(String multiElectronicTicket) {
        this.multiElectronicTicket = multiElectronicTicket;
    }

    public String getAesKey() {
        return aesKey;
    }

    public void setAesKey(String aesKey) {
        this.aesKey = aesKey;
    }
}
