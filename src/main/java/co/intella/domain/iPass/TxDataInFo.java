package co.intella.domain.iPass;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class TxDataInFo {

    @JsonProperty("NewCashTopEquipID")
    @SerializedName("NewCashTopEquipID")
    private String newCashTopEquipID;

    @JsonProperty("NewCashTopTxnType")
    @SerializedName("NewCashTopTxnType")
    private String newCashTopTxnType;

    @JsonProperty("NewCashTopTxnTime")
    @SerializedName("NewCashTopTxnTime")
    private String newCashTopTxnTime;

    @JsonProperty("NewCashTopTxnAmount")
    @SerializedName("NewCashTopTxnAmount")
    private String newCashTopTxnAmount;

    @JsonProperty("NewCashTopSystemID")
    @SerializedName("NewCashTopSystemID")
    private String newCashTopSystemID;

    @JsonProperty("NewCashTopBalance")
    @SerializedName("NewCashTopBalance")
    private String newCashTopBalance;

    @JsonProperty("NewCashTopCtsnLsb")
    @SerializedName("NewCashTopCtsnLsb")
    private String newCashTopCtsnLsb;

    @JsonProperty("NewCashTopSpID")
    @SerializedName("NewCashTopSpID")
    private String newCashTopSpID;

    @JsonProperty("NewPaymentBalance")
    @SerializedName("NewPaymentBalance")
    private String newPaymentBalance;

    @JsonProperty("NewPaymentSystemID")
    @SerializedName("NewPaymentSystemID")
    private String newPaymentSystemID;

    @JsonProperty("NewPaymentTxnTime")
    @SerializedName("NewPaymentTxnTime")
    private String newPaymentTxnTime;

    @JsonProperty("NewPaymentSpID")
    @SerializedName("NewPaymentSpID")
    private String newPaymentSpID;

    @JsonProperty("NewPaymentTxnAmount")
    @SerializedName("NewPaymentTxnAmount")
    private String newPaymentTxnAmount;

    @JsonProperty("NewPaymentEquipID")
    @SerializedName("NewPaymentEquipID")
    private String newPaymentEquipID;

    @JsonProperty("NewPaymentCtsnLsb")
    @SerializedName("NewPaymentCtsnLsb")
    private String newPaymentCtsnLsb;

    @JsonProperty("NewPaymentTxnType")
    @SerializedName("NewPaymentTxnType")
    private String newPaymentTxnType;


    @JsonProperty("PaymentData1SpID")
    @SerializedName("PaymentData1SpID")
    private String paymentData1SpID;
    @JsonProperty("PaymentData1Balance")
    @SerializedName("PaymentData1Balance")
    private String PpaymentData1Balance;
    @JsonProperty("PaymentData1TxnAmount")
    @SerializedName("PaymentData1TxnAmount")
    private String paymentData1TxnAmount;
    @JsonProperty("PaymentData1SystemID")
    @SerializedName("PaymentData1SystemID")
    private String paymentData1SystemID;
    @JsonProperty("PaymentData1TxnType")
    @SerializedName("PaymentData1TxnType")
    private String paymentData1TxnType;
    @JsonProperty("PaymentData1TxnTime")
    @SerializedName("PaymentData1TxnTime")
    private String paymentData1TxnTime;
    @JsonProperty("PaymentData1EquipID")
    @SerializedName("PaymentData1EquipID")
    private String paymentData1EquipID;
    @JsonProperty("PaymentData1CtsnLsb")
    @SerializedName("PaymentData1CtsnLsb")
    private String paymentData1CtsnLsb;

    @JsonProperty("PaymentData2SpID")
    @SerializedName("PaymentData2SpID")
    private String PaymentData2SpID;
    @JsonProperty("PaymentData2Balance")
    @SerializedName("PaymentData2Balance")
    private String PPaymentData2Balance;
    @JsonProperty("PaymentData2TxnAmount")
    @SerializedName("PaymentData2TxnAmount")
    private String PaymentData2TxnAmount;
    @JsonProperty("PaymentData2SystemID")
    @SerializedName("PaymentData2SystemID")
    private String PaymentData2SystemID;
    @JsonProperty("PaymentData2TxnType")
    @SerializedName("PaymentData2TxnType")
    private String PaymentData2TxnType;
    @JsonProperty("PaymentData2TxnTime")
    @SerializedName("PaymentData2TxnTime")
    private String PaymentData2TxnTime;
    @JsonProperty("PaymentData2EquipID")
    @SerializedName("PaymentData2EquipID")
    private String PaymentData2EquipID;
    @JsonProperty("PaymentData2CtsnLsb")
    @SerializedName("PaymentData2CtsnLsb")
    private String PaymentData2CtsnLsb;

    @JsonProperty("PaymentData3SpID")
    @SerializedName("PaymentData3SpID")
    private String PaymentData3SpID;
    @JsonProperty("PaymentData3Balance")
    @SerializedName("PaymentData3Balance")
    private String PPaymentData3Balance;
    @JsonProperty("PaymentData3TxnAmount")
    @SerializedName("PaymentData3TxnAmount")
    private String PaymentData3TxnAmount;
    @JsonProperty("PaymentData3SystemID")
    @SerializedName("PaymentData3SystemID")
    private String PaymentData3SystemID;
    @JsonProperty("PaymentData3TxnType")
    @SerializedName("PaymentData3TxnType")
    private String PaymentData3TxnType;
    @JsonProperty("PaymentData3TxnTime")
    @SerializedName("PaymentData3TxnTime")
    private String PaymentData3TxnTime;
    @JsonProperty("PaymentData3EquipID")
    @SerializedName("PaymentData3EquipID")
    private String PaymentData3EquipID;
    @JsonProperty("PaymentData3CtsnLsb")
    @SerializedName("PaymentData3CtsnLsb")
    private String PaymentData3CtsnLsb;

    @JsonProperty("PaymentData4SpID")
    @SerializedName("PaymentData4SpID")
    private String PaymentData4SpID;
    @JsonProperty("PaymentData4Balance")
    @SerializedName("PaymentData4Balance")
    private String PPaymentData4Balance;
    @JsonProperty("PaymentData4TxnAmount")
    @SerializedName("PaymentData4TxnAmount")
    private String PaymentData4TxnAmount;
    @JsonProperty("PaymentData4SystemID")
    @SerializedName("PaymentData4SystemID")
    private String PaymentData4SystemID;
    @JsonProperty("PaymentData4TxnType")
    @SerializedName("PaymentData4TxnType")
    private String PaymentData4TxnType;
    @JsonProperty("PaymentData4TxnTime")
    @SerializedName("PaymentData4TxnTime")
    private String PaymentData4TxnTime;
    @JsonProperty("PaymentData4EquipID")
    @SerializedName("PaymentData4EquipID")
    private String PaymentData4EquipID;
    @JsonProperty("PaymentData4CtsnLsb")
    @SerializedName("PaymentData4CtsnLsb")
    private String PaymentData4CtsnLsb;

    @JsonProperty("PaymentData5SpID")
    @SerializedName("PaymentData5SpID")
    private String PaymentData5SpID;
    @JsonProperty("PaymentData5Balance")
    @SerializedName("PaymentData5Balance")
    private String PPaymentData5Balance;
    @JsonProperty("PaymentData5TxnAmount")
    @SerializedName("PaymentData5TxnAmount")
    private String PaymentData5TxnAmount;
    @JsonProperty("PaymentData5SystemID")
    @SerializedName("PaymentData5SystemID")
    private String PaymentData5SystemID;
    @JsonProperty("PaymentData5TxnType")
    @SerializedName("PaymentData5TxnType")
    private String PaymentData5TxnType;
    @JsonProperty("PaymentData5TxnTime")
    @SerializedName("PaymentData5TxnTime")
    private String PaymentData5TxnTime;
    @JsonProperty("PaymentData5EquipID")
    @SerializedName("PaymentData5EquipID")
    private String PaymentData5EquipID;
    @JsonProperty("PaymentData5CtsnLsb")
    @SerializedName("PaymentData5CtsnLsb")
    private String PaymentData5CtsnLsb;

    @JsonProperty("PaymentData6SpID")
    @SerializedName("PaymentData6SpID")
    private String PaymentData6SpID;
    @JsonProperty("PaymentData6Balance")
    @SerializedName("PaymentData6Balance")
    private String PPaymentData6Balance;
    @JsonProperty("PaymentData6TxnAmount")
    @SerializedName("PaymentData6TxnAmount")
    private String PaymentData6TxnAmount;
    @JsonProperty("PaymentData6SystemID")
    @SerializedName("PaymentData6SystemID")
    private String PaymentData6SystemID;
    @JsonProperty("PaymentData6TxnType")
    @SerializedName("PaymentData6TxnType")
    private String PaymentData6TxnType;
    @JsonProperty("PaymentData6TxnTime")
    @SerializedName("PaymentData6TxnTime")
    private String PaymentData6TxnTime;
    @JsonProperty("PaymentData6EquipID")
    @SerializedName("PaymentData6EquipID")
    private String PaymentData6EquipID;
    @JsonProperty("PaymentData6CtsnLsb")
    @SerializedName("PaymentData6CtsnLsb")
    private String PaymentData6CtsnLsb;


    public String getNewCashTopEquipID() {
        return newCashTopEquipID;
    }

    public void setNewCashTopEquipID(String newCashTopEquipID) {
        this.newCashTopEquipID = newCashTopEquipID;
    }

    public String getNewCashTopTxnType() {
        return newCashTopTxnType;
    }

    public void setNewCashTopTxnType(String newCashTopTxnType) {
        this.newCashTopTxnType = newCashTopTxnType;
    }

    public String getNewCashTopTxnTime() {
        return newCashTopTxnTime;
    }

    public void setNewCashTopTxnTime(String newCashTopTxnTime) {
        this.newCashTopTxnTime = newCashTopTxnTime;
    }

    public String getNewCashTopTxnAmount() {
        return newCashTopTxnAmount;
    }

    public void setNewCashTopTxnAmount(String newCashTopTxnAmount) {
        this.newCashTopTxnAmount = newCashTopTxnAmount;
    }

    public String getNewCashTopSystemID() {
        return newCashTopSystemID;
    }

    public void setNewCashTopSystemID(String newCashTopSystemID) {
        this.newCashTopSystemID = newCashTopSystemID;
    }

    public String getNewCashTopBalance() {
        return newCashTopBalance;
    }

    public void setNewCashTopBalance(String newCashTopBalance) {
        this.newCashTopBalance = newCashTopBalance;
    }

    public String getNewCashTopCtsnLsb() {
        return newCashTopCtsnLsb;
    }

    public void setNewCashTopCtsnLsb(String newCashTopCtsnLsb) {
        this.newCashTopCtsnLsb = newCashTopCtsnLsb;
    }

    public String getNewCashTopSpID() {
        return newCashTopSpID;
    }

    public void setNewCashTopSpID(String newCashTopSpID) {
        this.newCashTopSpID = newCashTopSpID;
    }

    public String getNewPaymentBalance() {
        return newPaymentBalance;
    }

    public void setNewPaymentBalance(String newPaymentBalance) {
        this.newPaymentBalance = newPaymentBalance;
    }

    public String getNewPaymentSystemID() {
        return newPaymentSystemID;
    }

    public void setNewPaymentSystemID(String newPaymentSystemID) {
        this.newPaymentSystemID = newPaymentSystemID;
    }

    public String getNewPaymentTxnTime() {
        return newPaymentTxnTime;
    }

    public void setNewPaymentTxnTime(String newPaymentTxnTime) {
        this.newPaymentTxnTime = newPaymentTxnTime;
    }

    public String getNewPaymentSpID() {
        return newPaymentSpID;
    }

    public void setNewPaymentSpID(String newPaymentSpID) {
        this.newPaymentSpID = newPaymentSpID;
    }

    public String getNewPaymentTxnAmount() {
        return newPaymentTxnAmount;
    }

    public void setNewPaymentTxnAmount(String newPaymentTxnAmount) {
        this.newPaymentTxnAmount = newPaymentTxnAmount;
    }

    public String getNewPaymentEquipID() {
        return newPaymentEquipID;
    }

    public void setNewPaymentEquipID(String newPaymentEquipID) {
        this.newPaymentEquipID = newPaymentEquipID;
    }

    public String getNewPaymentCtsnLsb() {
        return newPaymentCtsnLsb;
    }

    public void setNewPaymentCtsnLsb(String newPaymentCtsnLsb) {
        this.newPaymentCtsnLsb = newPaymentCtsnLsb;
    }

    public String getNewPaymentTxnType() {
        return newPaymentTxnType;
    }

    public void setNewPaymentTxnType(String newPaymentTxnType) {
        this.newPaymentTxnType = newPaymentTxnType;
    }

    public String getPaymentData1SpID() {
        return paymentData1SpID;
    }

    public void setPaymentData1SpID(String paymentData1SpID) {
        this.paymentData1SpID = paymentData1SpID;
    }

    public String getPpaymentData1Balance() {
        return PpaymentData1Balance;
    }

    public void setPpaymentData1Balance(String ppaymentData1Balance) {
        PpaymentData1Balance = ppaymentData1Balance;
    }

    public String getPaymentData1TxnAmount() {
        return paymentData1TxnAmount;
    }

    public void setPaymentData1TxnAmount(String paymentData1TxnAmount) {
        this.paymentData1TxnAmount = paymentData1TxnAmount;
    }

    public String getPaymentData1SystemID() {
        return paymentData1SystemID;
    }

    public void setPaymentData1SystemID(String paymentData1SystemID) {
        this.paymentData1SystemID = paymentData1SystemID;
    }

    public String getPaymentData1TxnType() {
        return paymentData1TxnType;
    }

    public void setPaymentData1TxnType(String paymentData1TxnType) {
        this.paymentData1TxnType = paymentData1TxnType;
    }

    public String getPaymentData1TxnTime() {
        return paymentData1TxnTime;
    }

    public void setPaymentData1TxnTime(String paymentData1TxnTime) {
        this.paymentData1TxnTime = paymentData1TxnTime;
    }

    public String getPaymentData1EquipID() {
        return paymentData1EquipID;
    }

    public void setPaymentData1EquipID(String paymentData1EquipID) {
        this.paymentData1EquipID = paymentData1EquipID;
    }

    public String getPaymentData1CtsnLsb() {
        return paymentData1CtsnLsb;
    }

    public void setPaymentData1CtsnLsb(String paymentData1CtsnLsb) {
        this.paymentData1CtsnLsb = paymentData1CtsnLsb;
    }

    public String getPaymentData2SpID() {
        return PaymentData2SpID;
    }

    public void setPaymentData2SpID(String paymentData2SpID) {
        PaymentData2SpID = paymentData2SpID;
    }

    public String getPPaymentData2Balance() {
        return PPaymentData2Balance;
    }

    public void setPPaymentData2Balance(String PPaymentData2Balance) {
        this.PPaymentData2Balance = PPaymentData2Balance;
    }

    public String getPaymentData2TxnAmount() {
        return PaymentData2TxnAmount;
    }

    public void setPaymentData2TxnAmount(String paymentData2TxnAmount) {
        PaymentData2TxnAmount = paymentData2TxnAmount;
    }

    public String getPaymentData2SystemID() {
        return PaymentData2SystemID;
    }

    public void setPaymentData2SystemID(String paymentData2SystemID) {
        PaymentData2SystemID = paymentData2SystemID;
    }

    public String getPaymentData2TxnType() {
        return PaymentData2TxnType;
    }

    public void setPaymentData2TxnType(String paymentData2TxnType) {
        PaymentData2TxnType = paymentData2TxnType;
    }

    public String getPaymentData2TxnTime() {
        return PaymentData2TxnTime;
    }

    public void setPaymentData2TxnTime(String paymentData2TxnTime) {
        PaymentData2TxnTime = paymentData2TxnTime;
    }

    public String getPaymentData2EquipID() {
        return PaymentData2EquipID;
    }

    public void setPaymentData2EquipID(String paymentData2EquipID) {
        PaymentData2EquipID = paymentData2EquipID;
    }

    public String getPaymentData2CtsnLsb() {
        return PaymentData2CtsnLsb;
    }

    public void setPaymentData2CtsnLsb(String paymentData2CtsnLsb) {
        PaymentData2CtsnLsb = paymentData2CtsnLsb;
    }

    public String getPaymentData3SpID() {
        return PaymentData3SpID;
    }

    public void setPaymentData3SpID(String paymentData3SpID) {
        PaymentData3SpID = paymentData3SpID;
    }

    public String getPPaymentData3Balance() {
        return PPaymentData3Balance;
    }

    public void setPPaymentData3Balance(String PPaymentData3Balance) {
        this.PPaymentData3Balance = PPaymentData3Balance;
    }

    public String getPaymentData3TxnAmount() {
        return PaymentData3TxnAmount;
    }

    public void setPaymentData3TxnAmount(String paymentData3TxnAmount) {
        PaymentData3TxnAmount = paymentData3TxnAmount;
    }

    public String getPaymentData3SystemID() {
        return PaymentData3SystemID;
    }

    public void setPaymentData3SystemID(String paymentData3SystemID) {
        PaymentData3SystemID = paymentData3SystemID;
    }

    public String getPaymentData3TxnType() {
        return PaymentData3TxnType;
    }

    public void setPaymentData3TxnType(String paymentData3TxnType) {
        PaymentData3TxnType = paymentData3TxnType;
    }

    public String getPaymentData3TxnTime() {
        return PaymentData3TxnTime;
    }

    public void setPaymentData3TxnTime(String paymentData3TxnTime) {
        PaymentData3TxnTime = paymentData3TxnTime;
    }

    public String getPaymentData3EquipID() {
        return PaymentData3EquipID;
    }

    public void setPaymentData3EquipID(String paymentData3EquipID) {
        PaymentData3EquipID = paymentData3EquipID;
    }

    public String getPaymentData3CtsnLsb() {
        return PaymentData3CtsnLsb;
    }

    public void setPaymentData3CtsnLsb(String paymentData3CtsnLsb) {
        PaymentData3CtsnLsb = paymentData3CtsnLsb;
    }

    public String getPaymentData4SpID() {
        return PaymentData4SpID;
    }

    public void setPaymentData4SpID(String paymentData4SpID) {
        PaymentData4SpID = paymentData4SpID;
    }

    public String getPPaymentData4Balance() {
        return PPaymentData4Balance;
    }

    public void setPPaymentData4Balance(String PPaymentData4Balance) {
        this.PPaymentData4Balance = PPaymentData4Balance;
    }

    public String getPaymentData4TxnAmount() {
        return PaymentData4TxnAmount;
    }

    public void setPaymentData4TxnAmount(String paymentData4TxnAmount) {
        PaymentData4TxnAmount = paymentData4TxnAmount;
    }

    public String getPaymentData4SystemID() {
        return PaymentData4SystemID;
    }

    public void setPaymentData4SystemID(String paymentData4SystemID) {
        PaymentData4SystemID = paymentData4SystemID;
    }

    public String getPaymentData4TxnType() {
        return PaymentData4TxnType;
    }

    public void setPaymentData4TxnType(String paymentData4TxnType) {
        PaymentData4TxnType = paymentData4TxnType;
    }

    public String getPaymentData4TxnTime() {
        return PaymentData4TxnTime;
    }

    public void setPaymentData4TxnTime(String paymentData4TxnTime) {
        PaymentData4TxnTime = paymentData4TxnTime;
    }

    public String getPaymentData4EquipID() {
        return PaymentData4EquipID;
    }

    public void setPaymentData4EquipID(String paymentData4EquipID) {
        PaymentData4EquipID = paymentData4EquipID;
    }

    public String getPaymentData4CtsnLsb() {
        return PaymentData4CtsnLsb;
    }

    public void setPaymentData4CtsnLsb(String paymentData4CtsnLsb) {
        PaymentData4CtsnLsb = paymentData4CtsnLsb;
    }

    public String getPaymentData5SpID() {
        return PaymentData5SpID;
    }

    public void setPaymentData5SpID(String paymentData5SpID) {
        PaymentData5SpID = paymentData5SpID;
    }

    public String getPPaymentData5Balance() {
        return PPaymentData5Balance;
    }

    public void setPPaymentData5Balance(String PPaymentData5Balance) {
        this.PPaymentData5Balance = PPaymentData5Balance;
    }

    public String getPaymentData5TxnAmount() {
        return PaymentData5TxnAmount;
    }

    public void setPaymentData5TxnAmount(String paymentData5TxnAmount) {
        PaymentData5TxnAmount = paymentData5TxnAmount;
    }

    public String getPaymentData5SystemID() {
        return PaymentData5SystemID;
    }

    public void setPaymentData5SystemID(String paymentData5SystemID) {
        PaymentData5SystemID = paymentData5SystemID;
    }

    public String getPaymentData5TxnType() {
        return PaymentData5TxnType;
    }

    public void setPaymentData5TxnType(String paymentData5TxnType) {
        PaymentData5TxnType = paymentData5TxnType;
    }

    public String getPaymentData5TxnTime() {
        return PaymentData5TxnTime;
    }

    public void setPaymentData5TxnTime(String paymentData5TxnTime) {
        PaymentData5TxnTime = paymentData5TxnTime;
    }

    public String getPaymentData5EquipID() {
        return PaymentData5EquipID;
    }

    public void setPaymentData5EquipID(String paymentData5EquipID) {
        PaymentData5EquipID = paymentData5EquipID;
    }

    public String getPaymentData5CtsnLsb() {
        return PaymentData5CtsnLsb;
    }

    public void setPaymentData5CtsnLsb(String paymentData5CtsnLsb) {
        PaymentData5CtsnLsb = paymentData5CtsnLsb;
    }

    public String getPaymentData6SpID() {
        return PaymentData6SpID;
    }

    public void setPaymentData6SpID(String paymentData6SpID) {
        PaymentData6SpID = paymentData6SpID;
    }

    public String getPPaymentData6Balance() {
        return PPaymentData6Balance;
    }

    public void setPPaymentData6Balance(String PPaymentData6Balance) {
        this.PPaymentData6Balance = PPaymentData6Balance;
    }

    public String getPaymentData6TxnAmount() {
        return PaymentData6TxnAmount;
    }

    public void setPaymentData6TxnAmount(String paymentData6TxnAmount) {
        PaymentData6TxnAmount = paymentData6TxnAmount;
    }

    public String getPaymentData6SystemID() {
        return PaymentData6SystemID;
    }

    public void setPaymentData6SystemID(String paymentData6SystemID) {
        PaymentData6SystemID = paymentData6SystemID;
    }

    public String getPaymentData6TxnType() {
        return PaymentData6TxnType;
    }

    public void setPaymentData6TxnType(String paymentData6TxnType) {
        PaymentData6TxnType = paymentData6TxnType;
    }

    public String getPaymentData6TxnTime() {
        return PaymentData6TxnTime;
    }

    public void setPaymentData6TxnTime(String paymentData6TxnTime) {
        PaymentData6TxnTime = paymentData6TxnTime;
    }

    public String getPaymentData6EquipID() {
        return PaymentData6EquipID;
    }

    public void setPaymentData6EquipID(String paymentData6EquipID) {
        PaymentData6EquipID = paymentData6EquipID;
    }

    public String getPaymentData6CtsnLsb() {
        return PaymentData6CtsnLsb;
    }

    public void setPaymentData6CtsnLsb(String paymentData6CtsnLsb) {
        PaymentData6CtsnLsb = paymentData6CtsnLsb;
    }
}
