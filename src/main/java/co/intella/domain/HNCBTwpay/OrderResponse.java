package co.intella.domain.HNCBTwpay;


public class OrderResponse {

    private String storeId;
    private String endpointCode;
    private String terminalId;
    private String orderNumber;
    private String txnDir;
    private String txnDateTime;
    private String txnAmt;
    private String txnCurrency;
    private String txnFISCTac;
    private String txnSeqno;
    private String txnAccNO;
    private String respCode;
    private String respDesc;
    private String sign;

    @Override
    public String toString() {
        return "OrderResponse{" +
                "storeId='" + storeId + '\'' +
                ", endpointCode='" + endpointCode + '\'' +
                ", terminalId='" + terminalId + '\'' +
                ", orderNumber='" + orderNumber + '\'' +
                ", txnDir='" + txnDir + '\'' +
                ", txnDateTime='" + txnDateTime + '\'' +
                ", txnAmt='" + txnAmt + '\'' +
                ", txnCurrency='" + txnCurrency + '\'' +
                ", txnFISCTac='" + txnFISCTac + '\'' +
                ", txnSeqno='" + txnSeqno + '\'' +
                ", txnAccNO='" + txnAccNO + '\'' +
                ", respCode='" + respCode + '\'' +
                ", respDesc='" + respDesc + '\'' +
                ", sign='" + sign + '\'' +
                '}';
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getEndpointCode() {
        return endpointCode;
    }

    public void setEndpointCode(String endpointCode) {
        this.endpointCode = endpointCode;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getTxnDir() {
        return txnDir;
    }

    public void setTxnDir(String txnDir) {
        this.txnDir = txnDir;
    }

    public String getTxnDateTime() {
        return txnDateTime;
    }

    public void setTxnDateTime(String txnDateTime) {
        this.txnDateTime = txnDateTime;
    }

    public String getTxnAmt() {
        return txnAmt;
    }

    public void setTxnAmt(String txnAmt) {
        this.txnAmt = txnAmt;
    }

    public String getTxnCurrency() {
        return txnCurrency;
    }

    public void setTxnCurrency(String txnCurrency) {
        this.txnCurrency = txnCurrency;
    }

    public String getTxnFISCTac() {
        return txnFISCTac;
    }

    public void setTxnFISCTac(String txnFISCTac) {
        this.txnFISCTac = txnFISCTac;
    }

    public String getTxnSeqno() {
        return txnSeqno;
    }

    public void setTxnSeqno(String txnSeqno) {
        this.txnSeqno = txnSeqno;
    }

    public String getTxnAccNO() {
        return txnAccNO;
    }

    public void setTxnAccNO(String txnAccNO) {
        this.txnAccNO = txnAccNO;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getRespDesc() {
        return respDesc;
    }

    public void setRespDesc(String respDesc) {
        this.respDesc = respDesc;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
