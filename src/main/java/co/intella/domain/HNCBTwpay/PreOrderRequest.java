package co.intella.domain.HNCBTwpay;

import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import co.intella.utility.SystemInstance;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;


public class PreOrderRequest {

    public PreOrderRequest(PaymentAccount paymentAccount, TradeDetail tradeDetail) {
        this.txnDir = "RQ";
        this.channelCode = "TWP";
        //金融卡收款特店編號
        this.storeId = paymentAccount.getAccount();
        //金融卡末端代碼
        this.endpointCode = paymentAccount.getAccountTerminalId();
        //信用卡收款特店編號
        this.creditStoreId = paymentAccount.getSystemID();
        //信用卡末端代碼
        this.creditEndpointCode = paymentAccount.getSpID();

        this.txnOrderNumber = tradeDetail.getSystemOrderId();
        this.txnAmt = Long.valueOf(tradeDetail.getPayment()).intValue() + "00";
        this.txnCurrency = "901";
        this.terminalId = String.format("%06d", new Random().nextInt(999999));
        //時效五分鐘
        this.expiryDate = LocalDateTime.now().plusMinutes(5).format(DateTimeFormatter.ofPattern(SystemInstance.DATE_PATTERN));

    }

    private String txnDir;
    private String channelCode;
    private String storeId;
    private String endpointCode;
    private String creditStoreId;
    private String creditEndpointCode;
    private String terminalId;
    private String txnOrderNumber;
    private String txnAmt;
    private String txnCurrency;
    private String expiryDate;
    private String noticeURL;
    private String sign;

    @Override
    public String toString() {
        return "PreOrderRequest{" +
                "txnDir='" + txnDir + '\'' +
                ", channelCode='" + channelCode + '\'' +
                ", storeId='" + storeId + '\'' +
                ", endpointCode='" + endpointCode + '\'' +
                ", creditStoreId='" + creditStoreId + '\'' +
                ", creditEndpointCode='" + creditEndpointCode + '\'' +
                ", terminalId='" + terminalId + '\'' +
                ", txnOrderNumber='" + txnOrderNumber + '\'' +
                ", txnAmt='" + txnAmt + '\'' +
                ", txnCurrency='" + txnCurrency + '\'' +
                ", expiryDate='" + expiryDate + '\'' +
                ", noticeURL='" + noticeURL + '\'' +
                ", sign='" + sign + '\'' +
                '}';
    }

    public String getTxnDir() {
        return txnDir;
    }

    public void setTxnDir(String txnDir) {
        this.txnDir = txnDir;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getEndpointCode() {
        return endpointCode;
    }

    public void setEndpointCode(String endpointCode) {
        this.endpointCode = endpointCode;
    }

    public String getCreditStoreId() {
        return creditStoreId;
    }

    public void setCreditStoreId(String creditStoreId) {
        this.creditStoreId = creditStoreId;
    }

    public String getCreditEndpointCode() {
        return creditEndpointCode;
    }

    public void setCreditEndpointCode(String creditEndpointCode) {
        this.creditEndpointCode = creditEndpointCode;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getTxnOrderNumber() {
        return txnOrderNumber;
    }

    public void setTxnOrderNumber(String txnOrderNumber) {
        this.txnOrderNumber = txnOrderNumber;
    }

    public String getTxnAmt() {
        return txnAmt;
    }

    public void setTxnAmt(String txnAmt) {
        this.txnAmt = txnAmt;
    }

    public String getTxnCurrency() {
        return txnCurrency;
    }

    public void setTxnCurrency(String txnCurrency) {
        this.txnCurrency = txnCurrency;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getNoticeURL() {
        return noticeURL;
    }

    public void setNoticeURL(String noticeURL) {
        this.noticeURL = noticeURL;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
