package co.intella.domain.HNCBTwpay;


public class InqueryResponse {

    private String txnDir;
    private String storeId;
    private String endpointCode;
    private String orderNumber;
    private String inqDateTime;
    private String txnType;
    private String txnSeqno;
    private String txnAccNO;
    private String crtDateTime;
    private String txnDateTime;
    private String txnAcctDate;
    private String txnCurrency;
    private String txnAmt;
    private String txnRefundAmt;
    private String txnRefundSeqno;
    private String respCode;
    private String respDesc;
    private String sign;

    public String getTxnDir() {
        return txnDir;
    }

    public void setTxnDir(String txnDir) {
        this.txnDir = txnDir;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getEndpointCode() {
        return endpointCode;
    }

    public void setEndpointCode(String endpointCode) {
        this.endpointCode = endpointCode;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getInqDateTime() {
        return inqDateTime;
    }

    public void setInqDateTime(String inqDateTime) {
        this.inqDateTime = inqDateTime;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getTxnSeqno() {
        return txnSeqno;
    }

    public void setTxnSeqno(String txnSeqno) {
        this.txnSeqno = txnSeqno;
    }

    public String getTxnAccNO() {
        return txnAccNO;
    }

    public void setTxnAccNO(String txnAccNO) {
        this.txnAccNO = txnAccNO;
    }

    public String getCrtDateTime() {
        return crtDateTime;
    }

    public void setCrtDateTime(String crtDateTime) {
        this.crtDateTime = crtDateTime;
    }

    public String getTxnDateTime() {
        return txnDateTime;
    }

    public void setTxnDateTime(String txnDateTime) {
        this.txnDateTime = txnDateTime;
    }

    public String getTxnAcctDate() {
        return txnAcctDate;
    }

    public void setTxnAcctDate(String txnAcctDate) {
        this.txnAcctDate = txnAcctDate;
    }

    public String getTxnCurrency() {
        return txnCurrency;
    }

    public void setTxnCurrency(String txnCurrency) {
        this.txnCurrency = txnCurrency;
    }

    public String getTxnAmt() {
        return txnAmt;
    }

    public void setTxnAmt(String txnAmt) {
        this.txnAmt = txnAmt;
    }

    public String getTxnRefundAmt() {
        return txnRefundAmt;
    }

    public void setTxnRefundAmt(String txnRefundAmt) {
        this.txnRefundAmt = txnRefundAmt;
    }

    public String getTxnRefundSeqno() {
        return txnRefundSeqno;
    }

    public void setTxnRefundSeqno(String txnRefundSeqno) {
        this.txnRefundSeqno = txnRefundSeqno;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getRespDesc() {
        return respDesc;
    }

    public void setRespDesc(String respDesc) {
        this.respDesc = respDesc;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    @Override
    public String toString() {
        return "InqueryResponse{" +
                "txnDir='" + txnDir + '\'' +
                ", storeId='" + storeId + '\'' +
                ", endpointCode='" + endpointCode + '\'' +
                ", orderNumber='" + orderNumber + '\'' +
                ", inqDateTime='" + inqDateTime + '\'' +
                ", txnType='" + txnType + '\'' +
                ", txnSeqno='" + txnSeqno + '\'' +
                ", txnAccNO='" + txnAccNO + '\'' +
                ", crtDateTime='" + crtDateTime + '\'' +
                ", txnDateTime='" + txnDateTime + '\'' +
                ", txnAcctDate='" + txnAcctDate + '\'' +
                ", txnCurrency='" + txnCurrency + '\'' +
                ", txnAmt='" + txnAmt + '\'' +
                ", txnRefundAmt='" + txnRefundAmt + '\'' +
                ", txnRefundSeqno='" + txnRefundSeqno + '\'' +
                ", respCode='" + respCode + '\'' +
                ", respDesc='" + respDesc + '\'' +
                ", sign='" + sign + '\'' +
                '}';
    }
}
