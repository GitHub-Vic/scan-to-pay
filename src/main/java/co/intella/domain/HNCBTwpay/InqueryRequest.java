package co.intella.domain.HNCBTwpay;


import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import co.intella.utility.SystemInstance;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class InqueryRequest {

    public InqueryRequest(TradeDetail tradeDetail, PaymentAccount paymentAccount) {
        this.txnDir = "RQ";
        this.storeId = paymentAccount.getAccount();
        this.endpointCode = paymentAccount.getAccountTerminalId();
        this.inqDateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern(SystemInstance.DATE_PATTERN));
        this.orderNumber = tradeDetail.getOrderId();
    }

    private String txnDir;
    private String storeId;
    private String endpointCode;
    private String orderNumber;
    private String inqDateTime;
    private String sign;

    public String getTxnDir() {
        return txnDir;
    }

    public void setTxnDir(String txnDir) {
        this.txnDir = txnDir;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getEndpointCode() {
        return endpointCode;
    }

    public void setEndpointCode(String endpointCode) {
        this.endpointCode = endpointCode;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getInqDateTime() {
        return inqDateTime;
    }

    public void setInqDateTime(String inqDateTime) {
        this.inqDateTime = inqDateTime;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    @Override
    public String toString() {
        return "InqueryRequest{" +
                "txnDir='" + txnDir + '\'' +
                ", storeId='" + storeId + '\'' +
                ", endpointCode='" + endpointCode + '\'' +
                ", orderNumber='" + orderNumber + '\'' +
                ", inqDateTime='" + inqDateTime + '\'' +
                ", sign='" + sign + '\'' +
                '}';
    }
}
