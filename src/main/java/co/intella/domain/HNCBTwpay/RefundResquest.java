package co.intella.domain.HNCBTwpay;


import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import co.intella.utility.SystemInstance;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;


public class RefundResquest {

    public RefundResquest(TradeDetail tradeDetail, PaymentAccount paymentAccount) {

        this.txnDir = "RQ";
        this.storeId = paymentAccount.getAccount();
        this.orgEndpointCode = paymentAccount.getAccountTerminalId();
        this.terminalId = String.format("%06d", new Random().nextInt(999999));
        this.txnDateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern(SystemInstance.DATE_PATTERN));
        this.orgOrderNumber = tradeDetail.getOrderId();
        this.orgCurrency = "901";
        this.orgAmt = Long.valueOf(tradeDetail.getPayment()).intValue() + "00";
        this.txnSeqno = tradeDetail.getSystemOrderId();
    }

    @Override
    public String toString() {
        return "RefundResquest{" +
                "txnDir='" + txnDir + '\'' +
                ", storeId='" + storeId + '\'' +
                ", orgEndpointCode='" + orgEndpointCode + '\'' +
                ", terminalId='" + terminalId + '\'' +
                ", txnDateTime='" + txnDateTime + '\'' +
                ", orgOrderNumber='" + orgOrderNumber + '\'' +
                ", orgCurrency='" + orgCurrency + '\'' +
                ", orgAmt='" + orgAmt + '\'' +
                ", txnSeqno='" + txnSeqno + '\'' +
                ", sign='" + sign + '\'' +
                '}';
    }

    public String getTxnDir() {
        return txnDir;
    }

    public void setTxnDir(String txnDir) {
        this.txnDir = txnDir;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getOrgEndpointCode() {
        return orgEndpointCode;
    }

    public void setOrgEndpointCode(String orgEndpointCode) {
        this.orgEndpointCode = orgEndpointCode;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getTxnDateTime() {
        return txnDateTime;
    }

    public void setTxnDateTime(String txnDateTime) {
        this.txnDateTime = txnDateTime;
    }

    public String getOrgOrderNumber() {
        return orgOrderNumber;
    }

    public void setOrgOrderNumber(String orgOrderNumber) {
        this.orgOrderNumber = orgOrderNumber;
    }

    public String getOrgCurrency() {
        return orgCurrency;
    }

    public void setOrgCurrency(String orgCurrency) {
        this.orgCurrency = orgCurrency;
    }

    public String getOrgAmt() {
        return orgAmt;
    }

    public void setOrgAmt(String orgAmt) {
        this.orgAmt = orgAmt;
    }

    public String getTxnSeqno() {
        return txnSeqno;
    }

    public void setTxnSeqno(String txnSeqno) {
        this.txnSeqno = txnSeqno;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    private String txnDir;
    private String storeId;
    private String orgEndpointCode;
    private String terminalId;
    private String txnDateTime;
    private String orgOrderNumber;
    private String orgCurrency;
    private String orgAmt;
    private String txnSeqno;
    private String sign;
}
