package co.intella.domain.HNCBTwpay;

public class PreOrderResponse {

    private String respCode;
    private String respDesc;
    private String txnDir;
    private String channelCode;
    private String storeId;
    private String endpointCode;
    private String creditStoreId;
    private String creditEndpointCode;
    private String terminalId;
    private String txnOrderNumber;
    private String txnAmt;
    private String txnCurrency;
    private String expiryDate;
    private String qrCodeURL;
    private String sign;

    @Override
    public String toString() {
        return "PreOrderResponse{" +
                "respCode='" + respCode + '\'' +
                ", respDesc='" + respDesc + '\'' +
                ", txnDir='" + txnDir + '\'' +
                ", channelCode='" + channelCode + '\'' +
                ", storeId='" + storeId + '\'' +
                ", endpointCode='" + endpointCode + '\'' +
                ", creditStoreId='" + creditStoreId + '\'' +
                ", creditEndpointCode='" + creditEndpointCode + '\'' +
                ", terminalId='" + terminalId + '\'' +
                ", txnOrderNumber='" + txnOrderNumber + '\'' +
                ", txnAmt='" + txnAmt + '\'' +
                ", txnCurrency='" + txnCurrency + '\'' +
                ", expiryDate='" + expiryDate + '\'' +
                ", qrCodeURL='" + qrCodeURL + '\'' +
                ", sign='" + sign + '\'' +
                '}';
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getRespDesc() {
        return respDesc;
    }

    public void setRespDesc(String respDesc) {
        this.respDesc = respDesc;
    }

    public String getTxnDir() {
        return txnDir;
    }

    public void setTxnDir(String txnDir) {
        this.txnDir = txnDir;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getEndpointCode() {
        return endpointCode;
    }

    public void setEndpointCode(String endpointCode) {
        this.endpointCode = endpointCode;
    }

    public String getCreditStoreId() {
        return creditStoreId;
    }

    public void setCreditStoreId(String creditStoreId) {
        this.creditStoreId = creditStoreId;
    }

    public String getCreditEndpointCode() {
        return creditEndpointCode;
    }

    public void setCreditEndpointCode(String creditEndpointCode) {
        this.creditEndpointCode = creditEndpointCode;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getTxnOrderNumber() {
        return txnOrderNumber;
    }

    public void setTxnOrderNumber(String txnOrderNumber) {
        this.txnOrderNumber = txnOrderNumber;
    }

    public String getTxnAmt() {
        return txnAmt;
    }

    public void setTxnAmt(String txnAmt) {
        this.txnAmt = txnAmt;
    }

    public String getTxnCurrency() {
        return txnCurrency;
    }

    public void setTxnCurrency(String txnCurrency) {
        this.txnCurrency = txnCurrency;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getQrCodeURL() {
        return qrCodeURL;
    }

    public void setQrCodeURL(String qrCodeURL) {
        this.qrCodeURL = qrCodeURL;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
