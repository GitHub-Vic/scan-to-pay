package co.intella.domain.HNCBTwpay;


import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import co.intella.utility.SystemInstance;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class OrderRequest {

    public OrderRequest(PaymentAccount paymentAccount, TradeDetail tradeDetail) {
        this.storeId = paymentAccount.getAccount();
        this.endpointCode = paymentAccount.getAccountTerminalId();
        this.terminalId = String.format("%06d", new Random().nextInt(999999));
        this.orderNumber = tradeDetail.getOrderId();
        this.txnDir = "RQ";
        this.txnDateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern(SystemInstance.DATE_PATTERN));
        this.txnAmt = Long.valueOf(tradeDetail.getPayment()).intValue() + "00";
        this.txnCurrency = "901";
        this.txnFISCTac = tradeDetail.getBarcode();
    }

    private String storeId;
    private String endpointCode;
    private String terminalId;
    private String orderNumber;
    private String txnDir;
    private String txnDateTime;
    private String txnAmt;
    private String txnCurrency;
    private String txnFISCTac;
    private String sign;

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getEndpointCode() {
        return endpointCode;
    }

    public void setEndpointCode(String endpointCode) {
        this.endpointCode = endpointCode;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getTxnDir() {
        return txnDir;
    }

    public void setTxnDir(String txnDir) {
        this.txnDir = txnDir;
    }

    public String getTxnDateTime() {
        return txnDateTime;
    }

    public void setTxnDateTime(String txnDateTime) {
        this.txnDateTime = txnDateTime;
    }

    public String getTxnAmt() {
        return txnAmt;
    }

    public void setTxnAmt(String txnAmt) {
        this.txnAmt = txnAmt;
    }

    public String getTxnCurrency() {
        return txnCurrency;
    }

    public void setTxnCurrency(String txnCurrency) {
        this.txnCurrency = txnCurrency;
    }

    public String getTxnFISCTac() {
        return txnFISCTac;
    }

    public void setTxnFISCTac(String txnFISCTac) {
        this.txnFISCTac = txnFISCTac;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    @Override
    public String toString() {
        return "OrderRequest{" +
                "storeId='" + storeId + '\'' +
                ", endpointCode='" + endpointCode + '\'' +
                ", terminalId='" + terminalId + '\'' +
                ", orderNumber='" + orderNumber + '\'' +
                ", txnDir='" + txnDir + '\'' +
                ", txnDateTime='" + txnDateTime + '\'' +
                ", txnAmt='" + txnAmt + '\'' +
                ", txnCurrency='" + txnCurrency + '\'' +
                ", txnFISCTac='" + txnFISCTac + '\'' +
                ", sign='" + sign + '\'' +
                '}';
    }
}
