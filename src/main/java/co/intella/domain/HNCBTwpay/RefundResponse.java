package co.intella.domain.HNCBTwpay;

public class RefundResponse {
    private String txnDir;
    private String storeId;
    private String orgEndpointCode;
    private String terminalId;
    private String txnDateTime;
    private String orgOrderNumber;
    private String orgCurrency;
    private String orgAmt;
    private String orgTerminalId;
    private String isOrderRefund;
    private String txnSeqno;
    private String respCode;
    private String respDesc;
    private String sign;

    @Override
    public String toString() {
        return "RefundResponse{" +
                "txnDir='" + txnDir + '\'' +
                ", storeId='" + storeId + '\'' +
                ", orgEndpointCode='" + orgEndpointCode + '\'' +
                ", terminalId='" + terminalId + '\'' +
                ", txnDateTime='" + txnDateTime + '\'' +
                ", orgOrderNumber='" + orgOrderNumber + '\'' +
                ", orgCurrency='" + orgCurrency + '\'' +
                ", orgAmt='" + orgAmt + '\'' +
                ", orgTerminalId='" + orgTerminalId + '\'' +
                ", isOrderRefund='" + isOrderRefund + '\'' +
                ", txnSeqno='" + txnSeqno + '\'' +
                ", respCode='" + respCode + '\'' +
                ", respDesc='" + respDesc + '\'' +
                ", sign='" + sign + '\'' +
                '}';
    }

    public String getTxnDir() {
        return txnDir;
    }

    public void setTxnDir(String txnDir) {
        this.txnDir = txnDir;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getOrgEndpointCode() {
        return orgEndpointCode;
    }

    public void setOrgEndpointCode(String orgEndpointCode) {
        this.orgEndpointCode = orgEndpointCode;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getTxnDateTime() {
        return txnDateTime;
    }

    public void setTxnDateTime(String txnDateTime) {
        this.txnDateTime = txnDateTime;
    }

    public String getOrgOrderNumber() {
        return orgOrderNumber;
    }

    public void setOrgOrderNumber(String orgOrderNumber) {
        this.orgOrderNumber = orgOrderNumber;
    }

    public String getOrgCurrency() {
        return orgCurrency;
    }

    public void setOrgCurrency(String orgCurrency) {
        this.orgCurrency = orgCurrency;
    }

    public String getOrgAmt() {
        return orgAmt;
    }

    public void setOrgAmt(String orgAmt) {
        this.orgAmt = orgAmt;
    }

    public String getOrgTerminalId() {
        return orgTerminalId;
    }

    public void setOrgTerminalId(String orgTerminalId) {
        this.orgTerminalId = orgTerminalId;
    }

    public String getIsOrderRefund() {
        return isOrderRefund;
    }

    public void setIsOrderRefund(String isOrderRefund) {
        this.isOrderRefund = isOrderRefund;
    }

    public String getTxnSeqno() {
        return txnSeqno;
    }

    public void setTxnSeqno(String txnSeqno) {
        this.txnSeqno = txnSeqno;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getRespDesc() {
        return respDesc;
    }

    public void setRespDesc(String respDesc) {
        this.respDesc = respDesc;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
