package co.intella.domain.line;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class LinePaymentUrl {

    @JsonProperty("web")
    @SerializedName("web")
    private String web;

    @JsonProperty("app")
    @SerializedName("app")
    private String app;

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }
}
