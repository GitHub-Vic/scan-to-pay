package co.intella.domain.line;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class LinePayInfoData {

    @JsonProperty("method")
    @SerializedName("method")
    private String method;

    @JsonProperty("amount")
    @SerializedName("amount")
    private Integer amount;

    @JsonProperty("creditCardNickname")
    @SerializedName("creditCardNickname")
    private String creditCardNickname;

    @JsonProperty("creditCardBrand")
    @SerializedName("creditCardBrand")
    private String creditCardBrand;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getCreditCardNickname() {
        return creditCardNickname;
    }

    public void setCreditCardNickname(String creditCardNickname) {
        this.creditCardNickname = creditCardNickname;
    }

    public String getCreditCardBrand() {
        return creditCardBrand;
    }

    public void setCreditCardBrand(String creditCardBrand) {
        this.creditCardBrand = creditCardBrand;
    }
}
