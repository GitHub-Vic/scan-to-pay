package co.intella.domain.line;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class LineConfirmRequestData {

    @JsonProperty("amount")
    @SerializedName("amount")
    private Integer amount;

    @JsonProperty("currency")
    @SerializedName("currency")
    private String currency;

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
