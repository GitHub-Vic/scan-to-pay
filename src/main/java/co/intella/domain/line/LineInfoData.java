package co.intella.domain.line;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Alex
 */

public class LineInfoData {

    @JsonProperty("transactionId")
    @SerializedName("transactionId")
    private Number transactionId;

    @JsonProperty("orderId")
    @SerializedName("orderId")
    private String orderId;

    @JsonProperty("paymentUrl")
    @SerializedName("paymentUrl")
    private LinePaymentUrl paymentUrl;

    @JsonProperty("paymentAccessToken")
    @SerializedName("paymentAccessToken")
    private String paymentAccessToken;

    @JsonProperty("authorizationExpireDate")
    @SerializedName("authorizationExpireDate")
    private String authorizationExpireDate;


    @JsonProperty("payInfo")
    @SerializedName("payInfo")
    private List<LinePayInfoData> payInfos;

    @JsonProperty("refundList")
    @SerializedName("refundList")
    private List<LineRefundListData> refundLists;

    @JsonProperty("regKey")
    @SerializedName("regKey")
    private String regKey;

    @JsonProperty("refundTransactionId")
    @SerializedName("refundTransactionId")
    private Number refundTransactionId;

    @JsonProperty("refundTransactionDate")
    @SerializedName("refundTransactionDate")
    private String refundTransactionDate;

    @JsonProperty("transactionDate")
    @SerializedName("transactionDate")
    private String transactionDate;

    private String needCheck;

    private Number balance;

    private String transactionType;

    private String productName;

    private String currency;

    public List<LineRefundListData> getRefundLists() {
        return refundLists;
    }

    public void setRefundLists(List<LineRefundListData> refundLists) {
        this.refundLists = refundLists;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Number getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Number transactionId) {
        this.transactionId = transactionId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public LinePaymentUrl getPaymentUrl() {
        return paymentUrl;
    }

    public void setPaymentUrl(LinePaymentUrl paymentUrl) {
        this.paymentUrl = paymentUrl;
    }

    public String getPaymentAccessToken() {
        return paymentAccessToken;
    }

    public void setPaymentAccessToken(String paymentAccessToken) {
        this.paymentAccessToken = paymentAccessToken;
    }

    public String getAuthorizationExpireDate() {
        return authorizationExpireDate;
    }

    public void setAuthorizationExpireDate(String authorizationExpireDate) {
        this.authorizationExpireDate = authorizationExpireDate;
    }



    public String getRegKey() {
        return regKey;
    }

    public void setRegKey(String regKey) {
        this.regKey = regKey;
    }

    public List<LinePayInfoData> getPayInfos() {
        return payInfos;
    }

    public void setPayInfos(List<LinePayInfoData> payInfos) {
        this.payInfos = payInfos;
    }

    public String getRefundTransactionDate() {
        return refundTransactionDate;
    }

    public void setRefundTransactionDate(String refundTransactionDate) {
        this.refundTransactionDate = refundTransactionDate;
    }

    public Number getRefundTransactionId() {
        return refundTransactionId;
    }

    public void setRefundTransactionId(Number refundTransactionId) {
        this.refundTransactionId = refundTransactionId;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getNeedCheck() {
        return needCheck;
    }

    public void setNeedCheck(String needCheck) {
        this.needCheck = needCheck;
    }

    public Number getBalance() {
        return balance;
    }

    public void setBalance(Number balance) {
        this.balance = balance;
    }
}
