package co.intella.domain.line;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LineReserveRequestData {

    @JsonProperty("productName")
    @SerializedName("Body")
    private String productName;

    @JsonProperty("productImageUrl")
    @SerializedName("productImageUrl")
    private String productImageUrl;

    @JsonProperty("amount")
    @SerializedName("TotalFee")
    private Integer amount;

    @JsonProperty("currency")
    @SerializedName("currency")
    private String currency;


//    @JsonProperty("MerchantID")
//    @SerializedName("MerchantID")
//    private String merchantId;

    @JsonProperty("mid")
    @SerializedName("mid")
    private String mid;

    @JsonProperty("oneTimeKey")
    @SerializedName("AuthCode")
    private String oneTimeKey;

    @JsonProperty("confirmUrl")
    @SerializedName("confirmUrl")
    private String confirmUrl;

    @JsonProperty("confirmUrlType")
    @SerializedName("confirmUrlType")
    private String confirmUrlType="CLIENT";

    @JsonProperty("checkConfirmUrlBrowser")
    @SerializedName("checkConfirmUrlBrowser")
    private Boolean checkConfirmUrlBrowser = false;

    @JsonProperty("cancelUrl")
    @SerializedName("cancelUrl")
    private String cancelUrl;

    @JsonProperty("packageName")
    @SerializedName("packageName")
    private String packageName;

    @JsonProperty("orderId")
    @SerializedName("StoreOrderNo")
    private String orderId;

    @JsonProperty("deliveryPlacePhone")
    @SerializedName("deliveryPlacePhone")
    private String deliveryPlacePhone;

    @JsonProperty("payType")
    @SerializedName("payType")
    private String payType;

    @JsonProperty("langCd")
    @SerializedName("langCd")
    private String langCd;

    @JsonProperty("capture")
    @SerializedName("capture")
    private Boolean capture=true;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductImageUrl() {
        return productImageUrl;
    }

    public void setProductImageUrl(String productImageUrl) {
        this.productImageUrl = productImageUrl;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

//    public String getMerchantId() {
//        return merchantId;
//    }

//    public void setMerchantId(String merchantId) {
//        this.merchantId = merchantId;
//    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getOneTimeKey() {
        return oneTimeKey;
    }

    public void setOneTimeKey(String oneTimeKey) {
        this.oneTimeKey = oneTimeKey;
    }

    public String getConfirmUrl() {
        return confirmUrl;
    }

    public void setConfirmUrl(String confirmUrl) {
        this.confirmUrl = confirmUrl;
    }

    public String getConfirmUrlType() {
        return confirmUrlType;
    }

    public void setConfirmUrlType(String confirmUrlType) {
        this.confirmUrlType = confirmUrlType;
    }

    public Boolean getCheckConfirmUrlBrowser() {
        return checkConfirmUrlBrowser;
    }

    public void setCheckConfirmUrlBrowser(Boolean checkConfirmUrlBrowser) {
        this.checkConfirmUrlBrowser = checkConfirmUrlBrowser;
    }

    public String getCancelUrl() {
        return cancelUrl;
    }

    public void setCancelUrl(String cancelUrl) {
        this.cancelUrl = cancelUrl;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDeliveryPlacePhone() {
        return deliveryPlacePhone;
    }

    public void setDeliveryPlacePhone(String deliveryPlacePhone) {
        this.deliveryPlacePhone = deliveryPlacePhone;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getLangCd() {
        return langCd;
    }

    public void setLangCd(String langCd) {
        this.langCd = langCd;
    }

    public Boolean getCapture() {
        return capture;
    }

    public void setCapture(Boolean capture) {
        this.capture = capture;
    }
}
