package co.intella.domain.line;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Alex
 */

public class LineQueryResponseData {

    @JsonProperty("returnCode")
    @SerializedName("returnCode")
    private String returnCode;

    @JsonProperty("returnMessage")
    @SerializedName("returnMessage")
    private String returnMessage;

    @JsonProperty("info")
    @SerializedName("info")
    private List<LineInfoData> info;

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public List<LineInfoData> getInfo() {
        return info;
    }

    public void setInfo(List<LineInfoData> info) {
        this.info = info;
    }
    
}
