package co.intella.domain.line;

/**
 * @author Alex
 */

public class LineRefundListData {

    private String refundTransactionId;

    private String transactionType;

    private String refundAmount;

    private String refundTransactionDate;

    public String getRefundTransactionId() {
        return refundTransactionId;
    }

    public void setRefundTransactionId(String refundTransactionId) {
        this.refundTransactionId = refundTransactionId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getRefundTransactionDate() {
        return refundTransactionDate;
    }

    public void setRefundTransactionDate(String refundTransactionDate) {
        this.refundTransactionDate = refundTransactionDate;
    }
}
