package co.intella.domain.line;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class LineResponseData {

    @JsonProperty("returnCode")
    @SerializedName("returnCode")
    private String returnCode;

    @JsonProperty("returnMessage")
    @SerializedName("returnMessage")
    private String returnMessage;

    @JsonProperty("info")
    @SerializedName("info")
    private LineInfoData info;

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public LineInfoData getInfo() {
        return info;
    }

    public void setInfo(LineInfoData info) {
        this.info = info;
    }
}
