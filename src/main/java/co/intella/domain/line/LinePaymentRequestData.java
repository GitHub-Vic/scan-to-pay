package co.intella.domain.line;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class LinePaymentRequestData {

    @JsonProperty("productName")
    @SerializedName("Body")
    private String productName;

    @JsonProperty("amount")
    @SerializedName("TotalFee")
    private Integer amount;

    @JsonProperty("currency")
    @SerializedName("currency")
    private String currency;

    @JsonProperty("oneTimeKey")
    @SerializedName("AuthCode")
    private String oneTimeKey;

    @JsonProperty("orderId")
    @SerializedName("StoreOrderNo")
    private String orderId;

    @JsonProperty("capture")
    @SerializedName("capture")
    private Boolean capture = true;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @SerializedName("DeviceInfo")
    private String device;

    public String getProductName() {
	return productName;
    }

    public void setProductName(String productName) {
	this.productName = productName;
    }

    public Integer getAmount() {
	return amount;
    }

    public void setAmount(Integer amount) {
	this.amount = amount;
    }

    public String getCurrency() {
	return currency;
    }

    public void setCurrency(String currency) {
	this.currency = currency;
    }

    public String getOneTimeKey() {
	return oneTimeKey;
    }

    public void setOneTimeKey(String oneTimeKey) {
	this.oneTimeKey = oneTimeKey;
    }

    public String getOrderId() {
	return orderId;
    }

    public void setOrderId(String orderId) {
	this.orderId = orderId;
    }

    public Boolean getCapture() {
	return capture;
    }

    public void setCapture(Boolean capture) {
	this.capture = capture;
    }

    public String getDevice() {
	return device;
    }

    public void setDevice(String device) {
	this.device = device;
    }

}
