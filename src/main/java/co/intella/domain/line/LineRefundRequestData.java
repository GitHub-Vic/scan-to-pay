package co.intella.domain.line;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */

public class LineRefundRequestData {

    @JsonProperty("refundAmount")
    @SerializedName("RefundFee")
    private Integer refundAmount;


    public Integer getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(Integer refundAmount) {
        this.refundAmount = refundAmount;
    }
}
