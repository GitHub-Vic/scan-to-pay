package co.intella.domain.friDay;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Alex
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FridayContentData {

    private String txOrderNo;

    private String channelTerminalId;

    private String storeNo;
    private String operatorNo;
    private Boolean orderRefund =true;

    private String refundItems;
    private String payments;


    public String getTxOrderNo() {
        return txOrderNo;
    }

    public void setTxOrderNo(String txOrderNo) {
        this.txOrderNo = txOrderNo;
    }

    public String getChannelTerminalId() {
        return channelTerminalId;
    }

    public void setChannelTerminalId(String channelTerminalId) {
        this.channelTerminalId = channelTerminalId;
    }

    public String getStoreNo() {
        return storeNo;
    }

    public void setStoreNo(String storeNo) {
        this.storeNo = storeNo;
    }

    public String getOperatorNo() {
        return operatorNo;
    }

    public void setOperatorNo(String operatorNo) {
        this.operatorNo = operatorNo;
    }


    public String getRefundItems() {
        return refundItems;
    }

    public void setRefundItems(String refundItems) {
        this.refundItems = refundItems;
    }

    public String getPayments() {
        return payments;
    }

    public void setPayments(String payments) {
        this.payments = payments;
    }

    public Boolean getOrderRefund() {
        return orderRefund;
    }

    public void setOrderRefund(Boolean orderRefund) {
        this.orderRefund = orderRefund;
    }
}
