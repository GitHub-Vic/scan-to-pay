package co.intella.domain.friDay;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Alex
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FridayCreateOrderResponseData {

    private String errorCode;

    private String storeId;

    private String storeName;

    private String orderId;

    private String amount;

    private String channelId;

    private String partnerId;

    private String productId;

    private String walletStoreId;

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getWalletStoreId() {
        return walletStoreId;
    }

    public void setWalletStoreId(String walletStoreId) {
        this.walletStoreId = walletStoreId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
