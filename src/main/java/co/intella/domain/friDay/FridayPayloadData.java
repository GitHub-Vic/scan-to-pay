package co.intella.domain.friDay;

/**
 * @author Alex
 */

public class FridayPayloadData {

    private String api;

    private FridayContentData content;

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public FridayContentData getContent() {
        return content;
    }

    public void setContent(FridayContentData content) {
        this.content = content;
    }
}
