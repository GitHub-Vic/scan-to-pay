package co.intella.domain.friDay;

import co.intella.domain.line.LinePayInfoData;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Alex
 */

public class FridayNotifyRequestData {

    public class FridayPayment{
        private String paymentMethod;
        private String amount;

        public String getPaymentMethod() {
            return paymentMethod;
        }

        public void setPaymentMethod(String paymentMethod) {
            this.paymentMethod = paymentMethod;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }
    }

    private String orderId;

    private String txOrderNo;

    private String orderDateTime;
    private String orderStatus;
    private String totalAmt;
    private List<FridayPayment> payments;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTxOrderNo() {
        return txOrderNo;
    }

    public void setTxOrderNo(String txOrderNo) {
        this.txOrderNo = txOrderNo;
    }

    public String getOrderDateTime() {
        return orderDateTime;
    }

    public void setOrderDateTime(String orderDateTime) {
        this.orderDateTime = orderDateTime;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(String totalAmt) {
        this.totalAmt = totalAmt;
    }

    public List<FridayPayment> getPayments() {
        return payments;
    }

    public void setPayments(List<FridayPayment> payments) {
        this.payments = payments;
    }
}

