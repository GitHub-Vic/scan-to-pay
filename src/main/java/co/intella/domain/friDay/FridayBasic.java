package co.intella.domain.friDay;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Alex
 */

public class FridayBasic {

    @JsonProperty("serviceID")
    private String serviceId;

    private String payloadData;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getPayloadData() {
        return payloadData;
    }

    public void setPayloadData(String payloadData) {
        this.payloadData = payloadData;
    }
}
