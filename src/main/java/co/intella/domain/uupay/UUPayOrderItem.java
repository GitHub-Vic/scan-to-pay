package co.intella.domain.uupay;

import co.intella.model.TradeDetail;

public class UUPayOrderItem {
    private String item;
    private Integer qty;
    private Integer unitPrice;
    private Integer subtotal;
    private String itemDesc;

    public UUPayOrderItem() {
        super();
    }

    public UUPayOrderItem(String body, String totalFee, TradeDetail tradeDetail) {
        super();
        this.item = tradeDetail.getDescription();
        this.qty = 1;
        this.unitPrice = Integer.valueOf(totalFee);
        this.subtotal = Integer.valueOf(totalFee);
        this.itemDesc = body;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Integer unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Integer getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Integer subtotal) {
        this.subtotal = subtotal;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }
}
