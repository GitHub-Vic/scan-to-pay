package co.intella.domain.uupay;

public class UUPayOLPayResponse extends UUPayGeneralResponse {
	private String sellerMemberUid;
	private String merchantOrderNo;
	private String orderNo;
	private String paymentNo;
	private String orderStatus;
	private String orderCreateDateTime;
	private String paymentMethod;
	private String curreny;
	private double orderAmount;
	private double point;
	private double paymentAmount;
	private String eventCode;
	private double rebateNotApplicableAmount;
	private String maskCreditCardNo;
	private String cobrandedCode;
	private String redirectPaymentUrl;
	public String getSellerMemberUid() {
		return sellerMemberUid;
	}
	public void setSellerMemberUid(String sellerMemberUid) {
		this.sellerMemberUid = sellerMemberUid;
	}
	public String getMerchantOrderNo() {
		return merchantOrderNo;
	}
	public void setMerchantOrderNo(String merchantOrderNo) {
		this.merchantOrderNo = merchantOrderNo;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getPaymentNo() {
		return paymentNo;
	}
	public void setPaymentNo(String paymentNo) {
		this.paymentNo = paymentNo;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getOrderCreateDateTime() {
		return orderCreateDateTime;
	}
	public void setOrderCreateDateTime(String orderCreateDateTime) {
		this.orderCreateDateTime = orderCreateDateTime;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getCurreny() {
		return curreny;
	}
	public void setCurreny(String curreny) {
		this.curreny = curreny;
	}
	public double getOrderAmount() {
		return orderAmount;
	}
	public void setOrderAmount(double orderAmount) {
		this.orderAmount = orderAmount;
	}
	public double getPoint() {
		return point;
	}
	public void setPoint(double point) {
		this.point = point;
	}
	public double getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getEventCode() {
		return eventCode;
	}
	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}
	public double getRebateNotApplicableAmount() {
		return rebateNotApplicableAmount;
	}
	public void setRebateNotApplicableAmount(double rebateNotApplicableAmount) {
		this.rebateNotApplicableAmount = rebateNotApplicableAmount;
	}
	public String getMaskCreditCardNo() {
		return maskCreditCardNo;
	}
	public void setMaskCreditCardNo(String maskCreditCardNo) {
		this.maskCreditCardNo = maskCreditCardNo;
	}
	public String getCobrandedCode() {
		return cobrandedCode;
	}
	public void setCobrandedCode(String cobrandedCode) {
		this.cobrandedCode = cobrandedCode;
	}
	public String getRedirectPaymentUrl() {
		return redirectPaymentUrl;
	}
	public void setRedirectPaymentUrl(String redirectPaymentUrl) {
		this.redirectPaymentUrl = redirectPaymentUrl;
	}
}
