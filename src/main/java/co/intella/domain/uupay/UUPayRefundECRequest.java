package co.intella.domain.uupay;

import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import org.apache.commons.lang3.StringUtils;

public class UUPayRefundECRequest extends UUPayGeneralRequest {


    public UUPayRefundECRequest() {
        super();
    }

    public UUPayRefundECRequest(TradeDetail tradeDetail, PaymentAccount paymentAccount, String testMode) {
        super();
        this.contractNo = paymentAccount.getAccountTerminalId();
        this.orderNo = tradeDetail.getSystemOrderId();
        this.refundAmount = tradeDetail.getPayment();
//        this.rebateNotApplicableAmount = 0L;
        this.executorId = "0".equals(testMode) ? StringUtils.defaultString(paymentAccount.getPlatformCheckCode(), "") : "intella.co";
    }

    private String contractNo;
    private String orderNo;
    private String executorId;
    private Long refundAmount;
//    private Long rebateNotApplicableAmount;

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Long getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(Long refundAmount) {
        this.refundAmount = refundAmount;
    }

//    public Long getRebateNotApplicableAmount() {
//        return rebateNotApplicableAmount;
//    }
//
//    public void setRebateNotApplicableAmount(Long rebateNotApplicableAmount) {
//        this.rebateNotApplicableAmount = rebateNotApplicableAmount;
//    }

    public String getExecutorId() {
        return executorId;
    }

    public void setExecutorId(String executorId) {
        this.executorId = executorId;
    }
}
