package co.intella.domain.uupay;

import co.intella.utility.EncryptUtil;

public class UUPayOLPayRequest extends UUPayGeneralRequest {
    private String callBackUrl;
    private String notifyUrl;
    private String imageUrl;
    private Object order;

    public UUPayOLPayRequest() {
        super();
    }

    public UUPayOLPayRequest(String API_URL, String orderId) {
        super();
        this.callBackUrl = API_URL + "allpaypass/api/payment/order/result?orderId=" + EncryptUtil.encryptSimple(orderId);
        this.notifyUrl = API_URL + "allpaypass/api/UUPay/notify";
    }

    public String getCallBackUrl() {
        return callBackUrl;
    }

    public void setCallBackUrl(String callBackUrl) {
        this.callBackUrl = callBackUrl;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Object getOrder() {
        return order;
    }

    public void setOrder(Object order) {
        this.order = order;
    }
}
