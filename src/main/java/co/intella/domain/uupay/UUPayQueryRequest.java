package co.intella.domain.uupay;

public class UUPayQueryRequest extends UUPayGeneralRequest {
	private String merchantOrderNo;
	private String contractNo;
	
	public UUPayQueryRequest() {
		super();
	}
	public UUPayQueryRequest(String merchantOrderNo, String contractNo) {
		super();
		this.merchantOrderNo = merchantOrderNo;
		this.contractNo = contractNo;
	}
	
	public String getMerchantOrderNo() {
		return merchantOrderNo;
	}
	public void setMerchantOrderNo(String merchantOrderNo) {
		this.merchantOrderNo = merchantOrderNo;
	}
	public String getContractNo() {
		return contractNo;
	}
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
}
