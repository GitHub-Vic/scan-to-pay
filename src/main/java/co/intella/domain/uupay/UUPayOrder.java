package co.intella.domain.uupay;

import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;

public class UUPayOrder {
    private String merchantOrderNo;
    private String contractNo;
    private String currency;
    private String orderDesc;
    private String orderTiltle;
    private String tradeType;
    private long orderAmount;
    private String eventCode;
    private long rebateNotApplicableAmount;
    private String paymentBarCode;
    private Object[] orderItems;

    public UUPayOrder() {
        super();
    }

    public UUPayOrder(TradeDetail tradeDetail, PaymentAccount paymentAccount) {
        super();
        this.merchantOrderNo = tradeDetail.getOrderId();
        this.contractNo = paymentAccount.getAccountTerminalId();
        this.currency = "TWD";
        this.tradeType = "IMMEDIATE";
        this.orderAmount = tradeDetail.getPayment();
        this.rebateNotApplicableAmount = 0;
    }

    public String getMerchantOrderNo() {
        return merchantOrderNo;
    }

    public void setMerchantOrderNo(String merchantOrderNo) {
        this.merchantOrderNo = merchantOrderNo;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(String orderDesc) {
        this.orderDesc = orderDesc;
    }

    public String getOrderTiltle() {
        return orderTiltle;
    }

    public void setOrderTiltle(String orderTiltle) {
        this.orderTiltle = orderTiltle;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public long getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(long orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public long getRebateNotApplicableAmount() {
        return rebateNotApplicableAmount;
    }

    public void setRebateNotApplicableAmount(long rebateNotApplicableAmount) {
        this.rebateNotApplicableAmount = rebateNotApplicableAmount;
    }

    public Object[] getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(Object[] orderItems) {
        this.orderItems = orderItems;
    }

    public String getPaymentBarCode() {
        return paymentBarCode;
    }

    public void setPaymentBarCode(String paymentBarCode) {
        this.paymentBarCode = paymentBarCode;
    }
}
