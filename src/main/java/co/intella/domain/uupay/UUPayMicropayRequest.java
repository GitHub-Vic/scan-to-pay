package co.intella.domain.uupay;

public class UUPayMicropayRequest extends UUPayGeneralRequest {

    public UUPayMicropayRequest() {
        super();
    }

    private Object order;

    public Object getOrder() {
        return order;
    }

    public void setOrder(Object order) {
        this.order = order;
    }
}
