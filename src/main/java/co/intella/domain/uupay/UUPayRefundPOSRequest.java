package co.intella.domain.uupay;

import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;

public class UUPayRefundPOSRequest extends UUPayGeneralRequest {


    public UUPayRefundPOSRequest() {
        super();
    }

    public UUPayRefundPOSRequest(TradeDetail tradeDetail, PaymentAccount paymentAccount) {
        super();
        this.contractNo = paymentAccount.getAccountTerminalId();
        this.orderNo = tradeDetail.getSystemOrderId();
        this.refundAmount = tradeDetail.getPayment();
        this.rebateNotApplicableAmount = 0l;
    }

    private String contractNo;
    private String orderNo;
    private Long refundAmount;
    private Long rebateNotApplicableAmount;

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Long getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(Long refundAmount) {
        this.refundAmount = refundAmount;
    }

    public Long getRebateNotApplicableAmount() {
        return rebateNotApplicableAmount;
    }

    public void setRebateNotApplicableAmount(Long rebateNotApplicableAmount) {
        this.rebateNotApplicableAmount = rebateNotApplicableAmount;
    }
}
