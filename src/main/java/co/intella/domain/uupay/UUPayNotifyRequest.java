package co.intella.domain.uupay;

public class UUPayNotifyRequest {
	private String sellerMemberUid;
	private String merchantOrderNo;
	private String orderNo;
	private String orderStatus;
	private String orderCreateDateTime;
	private String paymentMethod;
	private String currency;
	private double orderAmount;
	
	public String getSellerMemberUid() {
		return sellerMemberUid;
	}
	public void setSellerMemberUid(String sellerMemberUid) {
		this.sellerMemberUid = sellerMemberUid;
	}
	public String getMerchantOrderNo() {
		return merchantOrderNo;
	}
	public void setMerchantOrderNo(String merchantOrderNo) {
		this.merchantOrderNo = merchantOrderNo;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getOrderCreateDateTime() {
		return orderCreateDateTime;
	}
	public void setOrderCreateDateTime(String orderCreateDateTime) {
		this.orderCreateDateTime = orderCreateDateTime;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public double getOrderAmount() {
		return orderAmount;
	}
	public void setOrderAmount(double orderAmount) {
		this.orderAmount = orderAmount;
	}
}
