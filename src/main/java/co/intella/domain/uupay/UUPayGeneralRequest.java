package co.intella.domain.uupay;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class UUPayGeneralRequest {
    public UUPayGeneralRequest() {
        this.requestDateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
    }

    protected String requestDateTime;

    public String getRequestDateTime() {
        return requestDateTime;
    }

    public void setRequestDateTime(String requestDateTime) {
        this.requestDateTime = requestDateTime;
    }
}
