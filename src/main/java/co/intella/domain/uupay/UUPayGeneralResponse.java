package co.intella.domain.uupay;

import com.fasterxml.jackson.databind.JsonNode;

public class UUPayGeneralResponse {
    private String returnCode;
    private String returnMsg;
    private JsonNode data;

    public UUPayGeneralResponse() {
        super();
    }

    public UUPayGeneralResponse(String returnCode, String returnMsg) {
        super();
        this.returnCode = returnCode;
        this.returnMsg = returnMsg;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public JsonNode getData() {
        return data;
    }

    public void setData(JsonNode data) {
        this.data = data;
    }
}
