package co.intella.domain.tsgw;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Peilun
 */

public class GwMerchantApiPayResponse {

  @SerializedName("amount")
  @JsonProperty("amount")
  private String amount;

  @SerializedName("gw")
  @JsonProperty("gw")
  private String gw;

  @SerializedName("merchantid")
  @JsonProperty("merchantid")
  private String merchantid;

  @SerializedName("orderid")
  @JsonProperty("orderid")
  private String orderid;

  @SerializedName("ordermemo")
  @JsonProperty("ordermemo")
  private String ordermemo;

  @SerializedName("ordername")
  @JsonProperty("ordername")
  private String ordername;

  @SerializedName("storeid")
  @JsonProperty("storeid")
  private String storeid;

  @SerializedName("terminalid")
  @JsonProperty("terminalid")
  private String terminalid;

  @SerializedName("timestamp")
  @JsonProperty("timestamp")
  private String timestamp;

  @SerializedName("sign")
  @JsonProperty("sign")
  private String sign;

  @SerializedName("return_code")
  @JsonProperty("return_code")
  private String returncode;

  @SerializedName("return_message")
  @JsonProperty("return_message")
  private String returnmessage;

  public String getAmount() {
    return amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
  }

  public String getGw() {
    return gw;
  }

  public void setGw(String gw) {
    this.gw = gw;
  }

  public String getMerchantid() {
    return merchantid;
  }

  public void setMerchantid(String merchantid) {
    this.merchantid = merchantid;
  }

  public String getOrderid() {
    return orderid;
  }

  public void setOrderid(String orderid) {
    this.orderid = orderid;
  }

  public String getOrdermemo() {
    return ordermemo;
  }

  public void setOrdermemo(String ordermemo) {
    this.ordermemo = ordermemo;
  }

  public String getOrdername() {
    return ordername;
  }

  public void setOrdername(String ordername) {
    this.ordername = ordername;
  }

  public String getStoreid() {
    return storeid;
  }

  public void setStoreid(String storeid) {
    this.storeid = storeid;
  }

  public String getTerminalid() {
    return terminalid;
  }

  public void setTerminalid(String terminalid) {
    this.terminalid = terminalid;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public String getSign() {
    return sign;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }

  public String getReturncode() {
    return returncode;
  }

  public void setReturncode(String returncode) {
    this.returncode = returncode;
  }

  public String getReturnmessage() {
    return returnmessage;
  }

  public void setReturnmessage(String returnmessage) {
    this.returnmessage = returnmessage;
  }


}
