package co.intella.domain.tsgw;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class GwMerchantApiRefundResponse {

  @SerializedName("gw")
  @JsonProperty("gw")
  private String gw;

  @SerializedName("merchantid")
  @JsonProperty("merchantid")
  private String merchantId;

  @SerializedName("storeid")
  @JsonProperty("storeid")
  private String storeId;

  @SerializedName("terminalid")
  @JsonProperty("terminalid")
  private String terminalId;

  @SerializedName("orderid")
  @JsonProperty("orderid")
  private String orderId;

  @SerializedName("refundid")
  @JsonProperty("refundid")
  private String refundId;

  @SerializedName("payamount")
  @JsonProperty("payamount")
  private String payAmount;

  @SerializedName("refundamount")
  @JsonProperty("refundamount")
  private String refundAmount;

  @SerializedName("totalpayamount")
  @JsonProperty("totalpayamount")
  private String totalPayAmount;

  @SerializedName("totalrefundamount")
  @JsonProperty("totalrefundamount")
  private String totalRefundAmount;

  @SerializedName("return_code")
  @JsonProperty("return_code")
  private String returnCode;

  @SerializedName("return_message")
  @JsonProperty("return_message")
  private String returnMessage;

  @SerializedName("timestamp")
  @JsonProperty("timestamp")
  private String timeStamp;

  @SerializedName("sign")
  @JsonProperty("sign")
  private String sign;

  public String getGw() {
    return gw;
  }

  public void setGw(String gw) {
    this.gw = gw;
  }

  public String getMerchantId() {
    return merchantId;
  }

  public void setMerchantId(String merchantId) {
    this.merchantId = merchantId;
  }

  public String getStoreId() {
    return storeId;
  }

  public void setStoreId(String storeId) {
    this.storeId = storeId;
  }

  public String getTerminalId() {
    return terminalId;
  }

  public void setTerminalId(String terminalId) {
    this.terminalId = terminalId;
  }

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public String getRefundId() {
    return refundId;
  }

  public void setRefundId(String refundId) {
    this.refundId = refundId;
  }

  public String getPayAmount() {
    return payAmount;
  }

  public void setPayAmount(String payAmount) {
    this.payAmount = payAmount;
  }

  public String getRefundAmount() {
    return refundAmount;
  }

  public void setRefundAmount(String refundAmount) {
    this.refundAmount = refundAmount;
  }

  public String getTotalPayAmount() {
    return totalPayAmount;
  }

  public void setTotalPayAmount(String totalPayAmount) {
    this.totalPayAmount = totalPayAmount;
  }

  public String getTotalRefundAmount() {
    return totalRefundAmount;
  }

  public void setTotalRefundAmount(String totalRefundAmount) {
    this.totalRefundAmount = totalRefundAmount;
  }

  public String getReturnCode() {
    return returnCode;
  }

  public void setReturnCode(String returnCode) {
    this.returnCode = returnCode;
  }

  public String getReturnMessage() {
    return returnMessage;
  }

  public void setReturnMessage(String returnMessage) {
    this.returnMessage = returnMessage;
  }

  public String getTimeStamp() {
    return timeStamp;
  }

  public void setTimeStamp(String timeStamp) {
    this.timeStamp = timeStamp;
  }

  public String getSign() {
    return sign;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }
}
