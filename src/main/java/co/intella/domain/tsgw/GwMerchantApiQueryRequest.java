package co.intella.domain.tsgw;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class GwMerchantApiQueryRequest {

  @SerializedName("gw")
  @JsonProperty("gw")
  private String gw;

  @SerializedName("merchantid")
  @JsonProperty("merchantid")
  private String merchantId;

  @SerializedName("StoreOrderNo")
  @JsonProperty("id")
  private String id;


  @SerializedName("storeid")
  @JsonProperty("storeid")
  private String storeId;

  @SerializedName("DeviceInfo")
  @JsonProperty("terminalid")
  private String terminalId;

  @SerializedName("timestamp")
  @JsonProperty("timestamp")
  private String timeStamp;

  @SerializedName("type")
  @JsonProperty("type")
  private String type;

  @SerializedName("sign")
  @JsonProperty("sign")
  private String sign;

  public String getGw() {
    return gw;
  }

  public void setGw(String gw) {
    this.gw = gw;
  }

  public String getMerchantId() {
    return merchantId;
  }

  public void setMerchantId(String merchantId) {
    this.merchantId = merchantId;
  }


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getStoreId() {
    return storeId;
  }

  public void setStoreId(String storeId) {
    this.storeId = storeId;
  }

  public String getTerminalId() {
    return terminalId;
  }

  public void setTerminalId(String terminalId) {
    this.terminalId = terminalId;
  }

  public String getTimeStamp() {
    return timeStamp;
  }

  public void setTimeStamp(String timeStamp) {
    this.timeStamp = timeStamp;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getSign() {
    return sign;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }
}
