package co.intella.domain.tsgw;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class GwMerchantApiQueryResponse {

  @SerializedName("gw")
  @JsonProperty("gw")
  private String gw;

  @SerializedName("merchantid")
  @JsonProperty("merchantid")
  private String merchantId;

  @SerializedName("storeid")
  @JsonProperty("storeid")
  private String storeId;

  @SerializedName("terminalid")
  @JsonProperty("terminalid")
  private String terminalId;

  @SerializedName("paytype")
  @JsonProperty("paytype")
  private String payType;

  @SerializedName("orderid")
  @JsonProperty("orderid")
  private String orderId;

  @JsonInclude(Include.NON_NULL)
  @SerializedName("refundid")
  @JsonProperty("refundid")
  private String refundId;

  @SerializedName("amount")
  @JsonProperty("amount")
  private String amount;

  @JsonInclude(Include.NON_NULL)
  @SerializedName("buyeremail")
  @JsonProperty("buyeremail")
  private String buyerEmail;

  @SerializedName("status")
  @JsonProperty("status")
  private String status;

  @SerializedName("transtime")
  @JsonProperty("transtime")
  private String transTime;

  @SerializedName("sign")
  @JsonProperty("sign")
  private String sign;

  @SerializedName("return_code")
  @JsonProperty("return_code")
  private String returnCode;

  @JsonInclude(Include.NON_NULL)
  @SerializedName("return_message")
  @JsonProperty("return_message")
  private String returnMessage;

  @SerializedName("timestamp")
  @JsonProperty("timestamp")
  private String timeStamp;

  @JsonInclude(Include.NON_NULL)
  @SerializedName("appropriationtime")
  @JsonProperty("appropriationtime")
  private String appropriationTime;

  public String getGw() {
    return gw;
  }

  public void setGw(String gw) {
    this.gw = gw;
  }

  public String getMerchantId() {
    return merchantId;
  }

  public void setMerchantId(String merchantId) {
    this.merchantId = merchantId;
  }

  public String getStoreId() {
    return storeId;
  }

  public void setStoreId(String storeId) {
    this.storeId = storeId;
  }

  public String getTerminalId() {
    return terminalId;
  }

  public void setTerminalId(String terminalId) {
    this.terminalId = terminalId;
  }

  public String getPayType() {
    return payType;
  }

  public void setPayType(String payType) {
    this.payType = payType;
  }

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public String getRefundId() {
    return refundId;
  }

  public void setRefundId(String refundId) {
    this.refundId = refundId;
  }

  public String getAmount() {
    return amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
  }

  public String getBuyerEmail() {
    return buyerEmail;
  }

  public void setBuyerEmail(String buyerEmail) {
    this.buyerEmail = buyerEmail;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getTransTime() {
    return transTime;
  }

  public void setTransTime(String transTime) {
    this.transTime = transTime;
  }

  public String getSign() {
    return sign;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }

  public String getReturnCode() {
    return returnCode;
  }

  public void setReturnCode(String returnCode) {
    this.returnCode = returnCode;
  }

  public String getReturnMessage() {
    return returnMessage;
  }

  public void setReturnMessage(String returnMessage) {
    this.returnMessage = returnMessage;
  }

  public String getTimeStamp() {
    return timeStamp;
  }

  public void setTimeStamp(String timeStamp) {
    this.timeStamp = timeStamp;
  }

  public String getAppropriationTime() {
    return appropriationTime;
  }

  public void setAppropriationTime(String appropriationTime) {
    this.appropriationTime = appropriationTime;
  }
}
