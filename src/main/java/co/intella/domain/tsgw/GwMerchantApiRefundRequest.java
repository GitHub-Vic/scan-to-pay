package co.intella.domain.tsgw;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class GwMerchantApiRefundRequest {

  @SerializedName("RefundFee")
  @JsonProperty("amount")
  private String amount;

  @SerializedName("gw")
  @JsonProperty("gw")
  private String gw;

  @SerializedName("merchantid")
  @JsonProperty("merchantid")
  private String merchantId;

  @SerializedName("StoreOrderNo")
  @JsonProperty("orderid")
  private String orderId;

  @SerializedName("StoreRefundNo")
  @JsonProperty("refundid")
  private String refundId;

  @SerializedName("RefundMsg")
  @JsonProperty("refundreason")
  private String refundReason;

  @SerializedName("storeid")
  @JsonProperty("storeid")
  private String storeId;

  @SerializedName("DeviceInfo")
  @JsonProperty("terminalid")
  private String terminalId;

  @SerializedName("timestamp")
  @JsonProperty("timestamp")
  private String timeStamp;

  @SerializedName("sign")
  @JsonProperty("sign")
  private String sign;

  public String getAmount() {
    return amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
  }

  public String getGw() {
    return gw;
  }

  public void setGw(String gw) {
    this.gw = gw;
  }

  public String getMerchantId() {
    return merchantId;
  }

  public void setMerchantId(String merchantId) {
    this.merchantId = merchantId;
  }

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public String getRefundId() {
    return refundId;
  }

  public void setRefundId(String refundId) {
    this.refundId = refundId;
  }

  public String getRefundReason() {
    return refundReason;
  }

  public void setRefundReason(String refundReason) {
    this.refundReason = refundReason;
  }

  public String getStoreId() {
    return storeId;
  }

  public void setStoreId(String storeId) {
    this.storeId = storeId;
  }

  public String getTerminalId() {
    return terminalId;
  }

  public void setTerminalId(String terminalId) {
    this.terminalId = terminalId;
  }

  public String getTimeStamp() {
    return timeStamp;
  }

  public void setTimeStamp(String timeStamp) {
    this.timeStamp = timeStamp;
  }

  public String getSign() {
    return sign;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }
}
