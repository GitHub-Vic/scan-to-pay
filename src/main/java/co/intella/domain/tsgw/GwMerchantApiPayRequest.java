package co.intella.domain.tsgw;

import com.google.gson.annotations.SerializedName;

/**
 * @author Peilun
 */

public class GwMerchantApiPayRequest {

    @SerializedName("TotalFee")
    private String amount;

    @SerializedName("AuthCode")
    private String barcode;

    @SerializedName("gw")
    private String gw;

    @SerializedName("merchantid")
    private String merchantid;

    @SerializedName("StoreOrderNo")
    private String orderid;

    @SerializedName("Detail")
    private String ordermemo;

    @SerializedName("Body")
    private String ordername;

    @SerializedName("storeid")
    private String storeid;

    @SerializedName("DeviceInfo")
    private String terminalid;

    @SerializedName("timestamp")
    private String timestamp;

    @SerializedName("sign")
    private String sign;

    public String getAmount() {
	return amount;
    }

    public void setAmount(String amount) {
	this.amount = amount;
    }

    public String getBarcode() {
	return barcode;
    }

    public void setBarcode(String barcode) {
	this.barcode = barcode;
    }

    public String getGw() {
	return gw;
    }

    public void setGw(String gw) {
	this.gw = gw;
    }

    public String getMerchantid() {
	return merchantid;
    }

    public void setMerchantid(String merchantid) {
	this.merchantid = merchantid;
    }

    public String getOrderid() {
	return orderid;
    }

    public void setOrderid(String orderid) {
	this.orderid = orderid;
    }

    public String getOrdermemo() {
	return ordermemo;
    }

    public void setOrdermemo(String ordermemo) {
	this.ordermemo = ordermemo;
    }

    public String getOrdername() {
	return ordername;
    }

    public void setOrdername(String ordername) {
	this.ordername = ordername;
    }

    public String getStoreid() {
	return storeid;
    }

    public void setStoreid(String storeid) {
	this.storeid = storeid;
    }

    public String getTerminalid() {
	return terminalid;
    }

    public void setTerminalid(String terminalid) {
	this.terminalid = terminalid;
    }

    public String getTimestamp() {
	return timestamp;
    }

    public void setTimestamp(String timestamp) {
	this.timestamp = timestamp;
    }

    public String getSign() {
	return sign;
    }

    public void setSign(String sign) {
	this.sign = sign;
    }

}
