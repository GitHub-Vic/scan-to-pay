package co.intella.domain.crm;

/**
 * @author Andy Lin
 */
public class CrmPermissionData {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
