package co.intella.domain.crm;

import co.intella.model.MerchantType;
import co.intella.model.Recipient;
import co.intella.model.Sale;

import javax.persistence.*;
import java.util.Set;

/**
 * @author Andy Lin
 */
public class CrmMerchantData {

    private String name;

    private String email;

    private MerchantType type;

    private Long creator;

    private String pin;

    private String accountId;

    private String comId;

    private String phone;

    private String signDate;

    private String status;

    private String paycodeToken;

    private String activateToken;

    private String refundPin;

    private Set<Recipient> recipients;

    private long saleId;

    private String saleAccountId;

    private String companyName;

    private String companyAddress;

    private String companyPhone;

    private String companyContactName;

    private String companyContactMobile;

    private String companyContactEmail;

    private String principalNationality;

    private String principalName;

    private String principalIdentity;

    private String registryLicenseType;

    private String address;

    private String description;

    private String salesCategory;

    private String salesType;

    private String url;

    private String financialInstitution;

    private String bankName;

    private String bankBranch;

    private String bankAccountName;

    private String bankAccountId;

    private String fax;

    private String bankContactInfo;

    private String bankContactPhone;

    private String bankContactMobile;

    private String bankContactEmail;

    private String invoiceEmail;

    private String registrationForm;

    private String declarationOfSalesAndBusinessTaxByABusinessEntity;

    private String extension;

    private String taxId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public MerchantType getType() {
        return type;
    }

    public void setType(MerchantType type) {
        this.type = type;
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getComId() {
        return comId;
    }

    public void setComId(String comId) {
        this.comId = comId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSignDate() {
        return signDate;
    }

    public void setSignDate(String signDate) {
        this.signDate = signDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPaycodeToken() {
        return paycodeToken;
    }

    public void setPaycodeToken(String paycodeToken) {
        this.paycodeToken = paycodeToken;
    }

    public String getActivateToken() {
        return activateToken;
    }

    public void setActivateToken(String activateToken) {
        this.activateToken = activateToken;
    }

    public String getRefundPin() {
        return refundPin;
    }

    public void setRefundPin(String refundPin) {
        this.refundPin = refundPin;
    }

    public Set<Recipient> getRecipients() {
        return recipients;
    }

    public void setRecipients(Set<Recipient> recipients) {
        this.recipients = recipients;
    }

    public long getSaleId() {
        return saleId;
    }

    public void setSaleId(long saleId) {
        this.saleId = saleId;
    }

    public String getSaleAccountId() {
        return saleAccountId;
    }

    public void setSaleAccountId(String saleAccountId) {
        this.saleAccountId = saleAccountId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getCompanyContactName() {
        return companyContactName;
    }

    public void setCompanyContactName(String companyContactName) {
        this.companyContactName = companyContactName;
    }

    public String getCompanyContactMobile() {
        return companyContactMobile;
    }

    public void setCompanyContactMobile(String companyContactMobile) {
        this.companyContactMobile = companyContactMobile;
    }

    public String getCompanyContactEmail() {
        return companyContactEmail;
    }

    public void setCompanyContactEmail(String companyContactEmail) {
        this.companyContactEmail = companyContactEmail;
    }

    public String getPrincipalNationality() {
        return principalNationality;
    }

    public void setPrincipalNationality(String principalNationality) {
        this.principalNationality = principalNationality;
    }

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    public String getPrincipalIdentity() {
        return principalIdentity;
    }

    public void setPrincipalIdentity(String principalIdentity) {
        this.principalIdentity = principalIdentity;
    }

    public String getRegistryLicenseType() {
        return registryLicenseType;
    }

    public void setRegistryLicenseType(String registryLicenseType) {
        this.registryLicenseType = registryLicenseType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSalesCategory() {
        return salesCategory;
    }

    public void setSalesCategory(String salesCategory) {
        this.salesCategory = salesCategory;
    }

    public String getSalesType() {
        return salesType;
    }

    public void setSalesType(String salesType) {
        this.salesType = salesType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFinancialInstitution() {
        return financialInstitution;
    }

    public void setFinancialInstitution(String financialInstitution) {
        this.financialInstitution = financialInstitution;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(String bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getBankContactInfo() {
        return bankContactInfo;
    }

    public void setBankContactInfo(String bankContactInfo) {
        this.bankContactInfo = bankContactInfo;
    }

    public String getBankContactPhone() {
        return bankContactPhone;
    }

    public void setBankContactPhone(String bankContactPhone) {
        this.bankContactPhone = bankContactPhone;
    }

    public String getBankContactMobile() {
        return bankContactMobile;
    }

    public void setBankContactMobile(String bankContactMobile) {
        this.bankContactMobile = bankContactMobile;
    }

    public String getBankContactEmail() {
        return bankContactEmail;
    }

    public void setBankContactEmail(String bankContactEmail) {
        this.bankContactEmail = bankContactEmail;
    }

    public String getInvoiceEmail() {
        return invoiceEmail;
    }

    public void setInvoiceEmail(String invoiceEmail) {
        this.invoiceEmail = invoiceEmail;
    }

    public String getRegistrationForm() {
        return registrationForm;
    }

    public void setRegistrationForm(String registrationForm) {
        this.registrationForm = registrationForm;
    }

    public String getDeclarationOfSalesAndBusinessTaxByABusinessEntity() {
        return declarationOfSalesAndBusinessTaxByABusinessEntity;
    }

    public void setDeclarationOfSalesAndBusinessTaxByABusinessEntity(String declarationOfSalesAndBusinessTaxByABusinessEntity) {
        this.declarationOfSalesAndBusinessTaxByABusinessEntity = declarationOfSalesAndBusinessTaxByABusinessEntity;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }
}
