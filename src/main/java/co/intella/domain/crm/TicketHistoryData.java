package co.intella.domain.crm;

import org.joda.time.DateTime;

/**
 * @author Alex
 */

public class TicketHistoryData {

    private long id;

    private String actor;

    private long ticketId;

    private String action;

    private DateTime systime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public long getTicketId() {
        return ticketId;
    }

    public void setTicketId(long ticketId) {
        this.ticketId = ticketId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public DateTime getSystime() {
        return systime;
    }

    public void setSystime(DateTime systime) {
        this.systime = systime;
    }
}
