package co.intella.domain;

import javax.crypto.SecretKey;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class DecryptRequest {
	private JsonObject header;
    private JsonPrimitive data;
    private SecretKey secretKey;
	public JsonObject getHeader() {
		return header;
	}
	public void setHeader(JsonObject header) {
		this.header = header;
	}
	public JsonPrimitive getData() {
		return data;
	}
	public void setData(JsonPrimitive data) {
		this.data = data;
	}
	public SecretKey getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(SecretKey secretKey) {
		this.secretKey = secretKey;
	}
    
}
