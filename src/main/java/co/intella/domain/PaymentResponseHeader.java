package co.intella.domain;

public class PaymentResponseHeader extends PaymentHeader {

	public String statusCode;
	public String statusDesc;
	public String responseTime;

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public String getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(String responseTime) {
		this.responseTime = responseTime;
	}

	@Override
	public String toString() {
		return "PaymentResponseHeader [statusCode=" + statusCode + ", statusDesc=" + statusDesc + ", tradeKey="
				+ tradeKey + ", responseTime=" + responseTime + "]";
	}
	
}
