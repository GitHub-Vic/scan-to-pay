package co.intella.domain.yuantaoffline;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class YuanTaOfflineRequest {

    @SerializedName("Action")
    @JsonProperty("Action")
    private String action;

    @SerializedName("BankPKeyVersion")
    @JsonProperty("BankPKeyVersion")
    private String bankPKeyVersion;

    @SerializedName("SecureData")
    @JsonProperty("SecureData")
    private String secureData;

    @SerializedName("APIToken")
    @JsonProperty("APIToken")
    private String apiToken;

    @SerializedName("HashDigest")
    @JsonProperty("HashDigest")
    private String hashDigest;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getBankPKeyVersion() {
        return bankPKeyVersion;
    }

    public void setBankPKeyVersion(String bankPKeyVersion) {
        this.bankPKeyVersion = bankPKeyVersion;
    }

    public String getSecureData() {
        return secureData;
    }

    public void setSecureData(String secureData) {
        this.secureData = secureData;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public String getHashDigest() {
        return hashDigest;
    }

    public void setHashDigest(String hashDigest) {
        this.hashDigest = hashDigest;
    }

}
