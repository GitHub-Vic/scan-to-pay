package co.intella.domain.yuantaoffline;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class YuanTaOfflinePrecreateNotifyRequest {

    @SerializedName("Action")
    @JsonProperty("Action")
    private String action;

    @SerializedName("Notify_time")
    @JsonProperty("Notify_time")
    private String NotifyTime;

    @SerializedName("Notify_id")
    @JsonProperty("Notify_id")
    private String NotifyId;

    @SerializedName("OrderNo")
    @JsonProperty("OrderNo")
    private String orderNo;

    @SerializedName("Status")
    @JsonProperty("Status")
    private String status;

    @SerializedName("HashDigest")
    @JsonProperty("HashDigest")
    private String hashDigest;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getNotifyTime() {
        return NotifyTime;
    }

    public void setNotifyTime(String notifyTime) {
        NotifyTime = notifyTime;
    }

    public String getNotifyId() {
        return NotifyId;
    }

    public void setNotifyId(String notifyId) {
        NotifyId = notifyId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHashDigest() {
        return hashDigest;
    }

    public void setHashDigest(String hashDigest) {
        this.hashDigest = hashDigest;
    }
}
