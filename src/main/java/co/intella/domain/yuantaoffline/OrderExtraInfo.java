package co.intella.domain.yuantaoffline;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class OrderExtraInfo {

    @SerializedName("dept_name")
    @JsonProperty("dept_name")
    private String deptName;

    @SerializedName("invoce_no")
    @JsonProperty("invoce_no")
    private String invoceNo;

    @SerializedName("operator")
    @JsonProperty("operator")
    private String operator;

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getInvoceNo() {
        return invoceNo;
    }

    public void setInvoceNo(String invoceNo) {
        this.invoceNo = invoceNo;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }
}
