package co.intella.domain.yuantaoffline;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class YuanTaOfflineCancelResponseDataObject extends YuanTaOfflineResponseDataObject{

    @SerializedName("BankOrderNo")
    @JsonProperty("BankOrderNo")
    private String bankOrderNo;

    @SerializedName("BankCancelOrderNo")
    @JsonProperty("BankCancelOrderNo")
    private String bankCancelOrderNo;

    @SerializedName("BankCancelOrderDT")
    @JsonProperty("BankCancelOrderDT")
    private String bankCancelOrderDT;

    @SerializedName("WalletOrderNo")
    @JsonProperty("WalletOrderNo")
    private String walletOrderNo;

    @SerializedName("WalletCancelOrderNo")
    @JsonProperty("WalletCancelOrderNo")
    private String walletCancelOrderNo;

    @SerializedName("WalletCancelOrderDT")
    @JsonProperty("WalletCancelOrderDT")
    private String walletCancelOrderDT;

    public String getBankOrderNo() {
        return bankOrderNo;
    }

    public void setBankOrderNo(String bankOrderNo) {
        this.bankOrderNo = bankOrderNo;
    }

    public String getBankCancelOrderNo() {
        return bankCancelOrderNo;
    }

    public void setBankCancelOrderNo(String bankCancelOrderNo) {
        this.bankCancelOrderNo = bankCancelOrderNo;
    }

    public String getBankCancelOrderDT() {
        return bankCancelOrderDT;
    }

    public void setBankCancelOrderDT(String bankCancelOrderDT) {
        this.bankCancelOrderDT = bankCancelOrderDT;
    }

    public String getWalletOrderNo() {
        return walletOrderNo;
    }

    public void setWalletOrderNo(String walletOrderNo) {
        this.walletOrderNo = walletOrderNo;
    }

    public String getWalletCancelOrderNo() {
        return walletCancelOrderNo;
    }

    public void setWalletCancelOrderNo(String walletCancelOrderNo) {
        this.walletCancelOrderNo = walletCancelOrderNo;
    }

    public String getWalletCancelOrderDT() {
        return walletCancelOrderDT;
    }

    public void setWalletCancelOrderDT(String walletCancelOrderDT) {
        this.walletCancelOrderDT = walletCancelOrderDT;
    }
}
