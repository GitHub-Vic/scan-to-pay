package co.intella.domain.yuantaoffline;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class YuanTaOfflineResponse {

    @SerializedName("Action")
    @JsonProperty("Action")
    private String action;

    @SerializedName("Status")
    @JsonProperty("Status")
    private String status;

    @SerializedName("ErrorCode")
    @JsonProperty("ErrorCode")
    private String errorCode;

    @SerializedName("ErrorDesc")
    @JsonProperty("ErrorDesc")
    private String errorDesc;

    @SerializedName("SecureData")
    @JsonProperty("SecureData")
    private String secureData;

    @SerializedName("HashDigest")
    @JsonProperty("HashDigest")
    private String hashDigest;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    public String getSecureData() {
        return secureData;
    }

    public void setSecureData(String secureData) {
        this.secureData = secureData;
    }

    public String getHashDigest() {
        return hashDigest;
    }

    public void setHashDigest(String hashDigest) {
        this.hashDigest = hashDigest;
    }
}
