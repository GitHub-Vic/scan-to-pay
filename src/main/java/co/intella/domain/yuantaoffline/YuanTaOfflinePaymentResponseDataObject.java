package co.intella.domain.yuantaoffline;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class YuanTaOfflinePaymentResponseDataObject extends YuanTaOfflineResponseDataObject{

    @SerializedName("BuyerID")
    @JsonProperty("BuyerID")
    private String buyerID;

    @SerializedName("BankOrderNo")
    @JsonProperty("BankOrderNo")
    private String bankOrderNo;

    @SerializedName("BankOrderDT")
    @JsonProperty("BankOrderDT")
    private String bankOrderDT;

    @SerializedName("WalletOrderNo")
    @JsonProperty("WalletOrderNo")
    private String walletOrderNo;

    @SerializedName("WalletOrderDT")
    @JsonProperty("WalletOrderDT")
    private String walletOrderDT;

    @SerializedName("OrderCurrency")
    @JsonProperty("OrderCurrency")
    private String orderCurrency;

    @SerializedName("OrderAmount")
    @JsonProperty("OrderAmount")
    private String orderAmount;

    @SerializedName("RealOrderAmount")
    @JsonProperty("RealOrderAmount")
    private String realOrderAmount;

    @SerializedName("BankCurrency")
    @JsonProperty("BankCurrency")
    private String bankCurrency;

    @SerializedName("Store2BankExRate")
    @JsonProperty("Store2BankExRate")
    private String store2BankExRate;

    @SerializedName("WalletCurrency")
    @JsonProperty("WalletCurrency")
    private String walletCurrency;

    @SerializedName("RealWalletAmount")
    @JsonProperty("RealWalletAmount")
    private String realWalletAmount;

    @SerializedName("ReceiptDesc")
    @JsonProperty("ReceiptDesc")
    private String receiptDesc;

    @SerializedName("Memo")
    @JsonProperty("Memo")
    private String memo;

    @SerializedName("BuyerWalletUserID")
    @JsonProperty("BuyerWalletUserID")
    private String buyerWalletUserID;

    @SerializedName("BuyerWalletLoginID")
    @JsonProperty("BuyerWalletLoginID")
    private String buyerWalletLoginID;

    @SerializedName("BuyerWalletBalance")
    @JsonProperty("BuyerWalletBalance")
    private String buyerWalletBalance;

    @SerializedName("OrderExtraInfo")
    @JsonProperty("OrderExtraInfo")
    private OrderExtraInfo orderExtraInfo;

    @SerializedName("Coupons")
    @JsonProperty("Coupons")
    private String coupons;

    public String getBuyerID() {
        return buyerID;
    }

    public void setBuyerID(String buyerID) {
        this.buyerID = buyerID;
    }

    public String getBankOrderNo() {
        return bankOrderNo;
    }

    public void setBankOrderNo(String bankOrderNo) {
        this.bankOrderNo = bankOrderNo;
    }

    public String getBankOrderDT() {
        return bankOrderDT;
    }

    public void setBankOrderDT(String bankOrderDT) {
        this.bankOrderDT = bankOrderDT;
    }

    public String getWalletOrderNo() {
        return walletOrderNo;
    }

    public void setWalletOrderNo(String walletOrderNo) {
        this.walletOrderNo = walletOrderNo;
    }

    public String getWalletOrderDT() {
        return walletOrderDT;
    }

    public void setWalletOrderDT(String walletOrderDT) {
        this.walletOrderDT = walletOrderDT;
    }

    public String getOrderCurrency() {
        return orderCurrency;
    }

    public void setOrderCurrency(String orderCurrency) {
        this.orderCurrency = orderCurrency;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getRealOrderAmount() {
        return realOrderAmount;
    }

    public void setRealOrderAmount(String realOrderAmount) {
        this.realOrderAmount = realOrderAmount;
    }

    public String getBankCurrency() {
        return bankCurrency;
    }

    public void setBankCurrency(String bankCurrency) {
        this.bankCurrency = bankCurrency;
    }

    public String getStore2BankExRate() {
        return store2BankExRate;
    }

    public void setStore2BankExRate(String store2BankExRate) {
        this.store2BankExRate = store2BankExRate;
    }

    public String getWalletCurrency() {
        return walletCurrency;
    }

    public void setWalletCurrency(String walletCurrency) {
        this.walletCurrency = walletCurrency;
    }

    public String getRealWalletAmount() {
        return realWalletAmount;
    }

    public void setRealWalletAmount(String realWalletAmount) {
        this.realWalletAmount = realWalletAmount;
    }

    public String getReceiptDesc() {
        return receiptDesc;
    }

    public void setReceiptDesc(String receiptDesc) {
        this.receiptDesc = receiptDesc;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getBuyerWalletUserID() {
        return buyerWalletUserID;
    }

    public void setBuyerWalletUserID(String buyerWalletUserID) {
        this.buyerWalletUserID = buyerWalletUserID;
    }

    public String getBuyerWalletLoginID() {
        return buyerWalletLoginID;
    }

    public void setBuyerWalletLoginID(String buyerWalletLoginID) {
        this.buyerWalletLoginID = buyerWalletLoginID;
    }

    public String getBuyerWalletBalance() {
        return buyerWalletBalance;
    }

    public void setBuyerWalletBalance(String buyerWalletBalance) {
        this.buyerWalletBalance = buyerWalletBalance;
    }

    public OrderExtraInfo getOrderExtraInfo() {
        return orderExtraInfo;
    }

    public void setOrderExtraInfo(OrderExtraInfo orderExtraInfo) {
        this.orderExtraInfo = orderExtraInfo;
    }

    public String getCoupons() {
        return coupons;
    }

    public void setCoupons(String coupons) {
        this.coupons = coupons;
    }
}
