package co.intella.domain.yuantaoffline;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class YuanTaOfflineQueryActionResponseDataObject extends YuanTaOfflineResponseDataObject{

    @SerializedName("IsCancelActionEnable")
    @JsonProperty("IsCancelActionEnable")
    private String isCancelActionEnable;

    @SerializedName("IsRefundActionEnable")
    @JsonProperty("IsRefundActionEnable")
    private String isRefundActionEnable;

    @SerializedName("RefundMax")
    @JsonProperty("RefundMax")
    private String refundMax;

    @SerializedName("RefundMin")
    @JsonProperty("RefundMin")
    private String refundMin;

    public String getIsCancelActionEnable() {
        return isCancelActionEnable;
    }

    public void setIsCancelActionEnable(String isCancelActionEnable) {
        this.isCancelActionEnable = isCancelActionEnable;
    }

    public String getIsRefundActionEnable() {
        return isRefundActionEnable;
    }

    public void setIsRefundActionEnable(String isRefundActionEnable) {
        this.isRefundActionEnable = isRefundActionEnable;
    }

    public String getRefundMax() {
        return refundMax;
    }

    public void setRefundMax(String refundMax) {
        this.refundMax = refundMax;
    }

    public String getRefundMin() {
        return refundMin;
    }

    public void setRefundMin(String refundMin) {
        this.refundMin = refundMin;
    }
}
