package co.intella.domain.yuantaoffline;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class YuanTaOfflineQueryResponseDataObject extends YuanTaOfflineResponseDataObject{

    @SerializedName("BankOrderNo")
    @JsonProperty("BankOrderNo")
    private String bankOrderNo;

    @SerializedName("BankOrderDT")
    @JsonProperty("BankOrderDT")
    private String bankOrderDT;

    @SerializedName("WalletOrderNo")
    @JsonProperty("WalletOrderNo")
    private String walletOrderNo;

    @SerializedName("WalletOrderDT")
    @JsonProperty("WalletOrderDT")
    private String walletOrderDT;

    @SerializedName("OrderCurrency")
    @JsonProperty("OrderCurrency")
    private String orderCurrency;

    @SerializedName("OrderAmount")
    @JsonProperty("OrderAmount")
    private String orderAmount;

    @SerializedName("RealOrderAmount")
    @JsonProperty("RealOrderAmount")
    private String realOrderAmount;

    @SerializedName("BankCurrency")
    @JsonProperty("BankCurrency")
    private String bankCurrency;

    @SerializedName("Store2BankExRate")
    @JsonProperty("Store2BankExRate")
    private String store2BankExRate;

    @SerializedName("WalletCurrency")
    @JsonProperty("WalletCurrency")
    private String walletCurrency;

    @SerializedName("RealWalletAmount")
    @JsonProperty("RealWalletAmount")
    private String realWalletAmount;

    @SerializedName("RefundOrderAmount")
    @JsonProperty("RefundOrderAmount")
    private String refundOrderAmount;

    @SerializedName("BuyerID")
    @JsonProperty("BuyerID")
    private String buyerID;

    @SerializedName("BuyerTransferType")
    @JsonProperty("BuyerTransferType")
    private String buyerTransferType;

    @SerializedName("BuyerWallet")
    @JsonProperty("BuyerWallet")
    private String buyerWallet;

    @SerializedName("BuyerPaymentType")
    @JsonProperty("BuyerPaymentType")
    private String buyerPaymentType;

    @SerializedName("OrderStatus")
    @JsonProperty("OrderStatus")
    private String orderStatus;

    @SerializedName("OrderTitle")
    @JsonProperty("OrderTitle")
    private String orderTitle;

    @SerializedName("OrderDesc")
    @JsonProperty("OrderDesc")
    private String orderDesc;

    @SerializedName("StoreApproprationStatus")
    @JsonProperty("StoreApproprationStatus")
    private String storeApproprationStatus;

    @SerializedName("StoreApproprationDT")
    @JsonProperty("StoreApproprationDT")
    private String storeApproprationDT;

    @SerializedName("StoreApproprationNo")
    @JsonProperty("StoreApproprationNo")
    private String storeApproprationNo;

    @SerializedName("OrderFaileCode")
    @JsonProperty("OrderFaileCode")
    private String orderFaileCode;

    @SerializedName("OrderFaileDesc")
    @JsonProperty("OrderFaileDesc")
    private String orderFaileDesc;

    @SerializedName("BuyerWalletUserID")
    @JsonProperty("BuyerWalletUserID")
    private String buyerWalletUserID;

    @SerializedName("BuyerWalletLoginID")
    @JsonProperty("BuyerWalletLoginID")
    private String buyerWalletLoginID;

    @SerializedName("BuyerWalletBalance")
    @JsonProperty("BuyerWalletBalance")
    private String buyerWalletBalance;

    public String getBankOrderNo() {
        return bankOrderNo;
    }

    public void setBankOrderNo(String bankOrderNo) {
        this.bankOrderNo = bankOrderNo;
    }

    public String getBankOrderDT() {
        return bankOrderDT;
    }

    public void setBankOrderDT(String bankOrderDT) {
        this.bankOrderDT = bankOrderDT;
    }

    public String getWalletOrderNo() {
        return walletOrderNo;
    }

    public void setWalletOrderNo(String walletOrderNo) {
        this.walletOrderNo = walletOrderNo;
    }

    public String getWalletOrderDT() {
        return walletOrderDT;
    }

    public void setWalletOrderDT(String walletOrderDT) {
        this.walletOrderDT = walletOrderDT;
    }

    public String getOrderCurrency() {
        return orderCurrency;
    }

    public void setOrderCurrency(String orderCurrency) {
        this.orderCurrency = orderCurrency;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getRealOrderAmount() {
        return realOrderAmount;
    }

    public void setRealOrderAmount(String realOrderAmount) {
        this.realOrderAmount = realOrderAmount;
    }

    public String getBankCurrency() {
        return bankCurrency;
    }

    public void setBankCurrency(String bankCurrency) {
        this.bankCurrency = bankCurrency;
    }

    public String getStore2BankExRate() {
        return store2BankExRate;
    }

    public void setStore2BankExRate(String store2BankExRate) {
        this.store2BankExRate = store2BankExRate;
    }

    public String getWalletCurrency() {
        return walletCurrency;
    }

    public void setWalletCurrency(String walletCurrency) {
        this.walletCurrency = walletCurrency;
    }

    public String getRealWalletAmount() {
        return realWalletAmount;
    }

    public void setRealWalletAmount(String realWalletAmount) {
        this.realWalletAmount = realWalletAmount;
    }

    public String getRefundOrderAmount() {
        return refundOrderAmount;
    }

    public void setRefundOrderAmount(String refundOrderAmount) {
        this.refundOrderAmount = refundOrderAmount;
    }

    public String getBuyerID() {
        return buyerID;
    }

    public void setBuyerID(String buyerID) {
        this.buyerID = buyerID;
    }

    public String getBuyerTransferType() {
        return buyerTransferType;
    }

    public void setBuyerTransferType(String buyerTransferType) {
        this.buyerTransferType = buyerTransferType;
    }

    public String getBuyerWallet() {
        return buyerWallet;
    }

    public void setBuyerWallet(String buyerWallet) {
        this.buyerWallet = buyerWallet;
    }

    public String getBuyerPaymentType() {
        return buyerPaymentType;
    }

    public void setBuyerPaymentType(String buyerPaymentType) {
        this.buyerPaymentType = buyerPaymentType;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderTitle() {
        return orderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }

    public String getOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(String orderDesc) {
        this.orderDesc = orderDesc;
    }

    public String getStoreApproprationStatus() {
        return storeApproprationStatus;
    }

    public void setStoreApproprationStatus(String storeApproprationStatus) {
        this.storeApproprationStatus = storeApproprationStatus;
    }

    public String getStoreApproprationDT() {
        return storeApproprationDT;
    }

    public void setStoreApproprationDT(String storeApproprationDT) {
        this.storeApproprationDT = storeApproprationDT;
    }

    public String getStoreApproprationNo() {
        return storeApproprationNo;
    }

    public void setStoreApproprationNo(String storeApproprationNo) {
        this.storeApproprationNo = storeApproprationNo;
    }

    public String getOrderFaileCode() {
        return orderFaileCode;
    }

    public void setOrderFaileCode(String orderFaileCode) {
        this.orderFaileCode = orderFaileCode;
    }

    public String getOrderFaileDesc() {
        return orderFaileDesc;
    }

    public void setOrderFaileDesc(String orderFaileDesc) {
        this.orderFaileDesc = orderFaileDesc;
    }

    public String getBuyerWalletUserID() {
        return buyerWalletUserID;
    }

    public void setBuyerWalletUserID(String buyerWalletUserID) {
        this.buyerWalletUserID = buyerWalletUserID;
    }

    public String getBuyerWalletLoginID() {
        return buyerWalletLoginID;
    }

    public void setBuyerWalletLoginID(String buyerWalletLoginID) {
        this.buyerWalletLoginID = buyerWalletLoginID;
    }

    public String getBuyerWalletBalance() {
        return buyerWalletBalance;
    }

    public void setBuyerWalletBalance(String buyerWalletBalance) {
        this.buyerWalletBalance = buyerWalletBalance;
    }
}
