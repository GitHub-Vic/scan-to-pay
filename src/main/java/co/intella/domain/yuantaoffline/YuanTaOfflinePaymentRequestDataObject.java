package co.intella.domain.yuantaoffline;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class YuanTaOfflinePaymentRequestDataObject extends YuanTaOfflineRequestDataObject{

    @SerializedName("BuyerID")
    @JsonProperty("BuyerID")
    private String buyerID;

    @SerializedName("OrderCurrency")
    @JsonProperty("OrderCurrency")
    private String orderCurrency;

    @SerializedName("OrderAmount")
    @JsonProperty("OrderAmount")
    private String orderAmount;

    @SerializedName("OrderTitle")
    @JsonProperty("OrderTitle")
    private String orderTitle;

    @SerializedName("OrderDesc")
    @JsonProperty("OrderDesc")
    private String orderDesc;

    @SerializedName("ProductQuantity")
    @JsonProperty("ProductQuantity")
    private String productQuantity;

    @SerializedName("OrderExtraInfo")
    @JsonProperty("OrderExtraInfo")
    private OrderExtraInfo orderExtraInfo;

    @SerializedName("OrderLongitude")
    @JsonProperty("OrderLongitude")
    private String orderLongitude;

    @SerializedName("OrderLatitude")
    @JsonProperty("OrderLatitude")
    private String orderLatitude;

    @SerializedName("ProductInfo")
    @JsonProperty("ProductInfo")
    private String productInfo;

    @SerializedName("BuyerWallet")
    @JsonProperty("BuyerWallet")
    private String buyerWallet;

    @SerializedName("BuyerPaymentType")
    @JsonProperty("BuyerPaymentType")
    private String buyerPaymentType;

    @SerializedName("BuyerTransferType")
    @JsonProperty("BuyerTransferType")
    private String buyerTransferType;

    @SerializedName("Coupons")
    @JsonProperty("Coupons")
    private String coupons;

    public String getBuyerID() {
        return buyerID;
    }

    public void setBuyerID(String buyerID) {
        this.buyerID = buyerID;
    }

    public String getOrderCurrency() {
        return orderCurrency;
    }

    public void setOrderCurrency(String orderCurrency) {
        this.orderCurrency = orderCurrency;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getOrderTitle() {
        return orderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }

    public String getOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(String orderDesc) {
        this.orderDesc = orderDesc;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    public OrderExtraInfo getOrderExtraInfo() {
        return orderExtraInfo;
    }

    public void setOrderExtraInfo(OrderExtraInfo orderExtraInfo) {
        this.orderExtraInfo = orderExtraInfo;
    }

    public String getOrderLongitude() {
        return orderLongitude;
    }

    public void setOrderLongitude(String orderLongitude) {
        this.orderLongitude = orderLongitude;
    }

    public String getOrderLatitude() {
        return orderLatitude;
    }

    public void setOrderLatitude(String orderLatitude) {
        this.orderLatitude = orderLatitude;
    }

    public String getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(String productInfo) {
        this.productInfo = productInfo;
    }

    public String getBuyerWallet() {
        return buyerWallet;
    }

    public void setBuyerWallet(String buyerWallet) {
        this.buyerWallet = buyerWallet;
    }

    public String getBuyerPaymentType() {
        return buyerPaymentType;
    }

    public void setBuyerPaymentType(String buyerPaymentType) {
        this.buyerPaymentType = buyerPaymentType;
    }

    public String getBuyerTransferType() {
        return buyerTransferType;
    }

    public void setBuyerTransferType(String buyerTransferType) {
        this.buyerTransferType = buyerTransferType;
    }

    public String getCoupons() {
        return coupons;
    }

    public void setCoupons(String coupons) {
        this.coupons = coupons;
    }
}
