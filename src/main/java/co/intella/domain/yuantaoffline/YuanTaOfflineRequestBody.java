package co.intella.domain.yuantaoffline;

public class YuanTaOfflineRequestBody {
    public static final String PAYMENT = "payment";

    public static final String PRECREATE = "precreate";

    public static final String PRECREATENOTIFY = "precreateNotify";

    public static final String QUERY_FOREX = "queryforex";

    public static final String CANCEL = "cancel";

    public static final String QUERY = "query";

    public static final String QUERY_ACTION = "queryaction";

    public static final String REFUND = "refund";

    public static final String TIMEOUT = "30";

}
