package co.intella.domain.yuantaoffline;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class YuanTaOfflineResponseDataObject {

    @SerializedName("OrderDT")
    @JsonProperty("OrderDT")
    private String orderDT;

    @SerializedName("OrderNo")
    @JsonProperty("OrderNo")
    private String orderNo;

    public String getOrderDT() {
        return orderDT;
    }

    public void setOrderDT(String orderDT) {
        this.orderDT = orderDT;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
