package co.intella.domain.yuantaoffline;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class YuanTaOfflineQueryForexRequestDataObject extends YuanTaOfflineRequestDataObject{

    @SerializedName("StoreID")
    @JsonProperty("StoreID")
    private String storeID;

    @SerializedName("TermID")
    @JsonProperty("TermID")
    private String termID;

    @SerializedName("Timeout")
    @JsonProperty("Timeout")
    private String timeout;

    @SerializedName("Amount")
    @JsonProperty("Amount")
    private String amount;

    @SerializedName("Currency")
    @JsonProperty("Currency")
    private String currency;

    public String getStoreID() {
        return storeID;
    }

    public void setStoreID(String storeID) {
        this.storeID = storeID;
    }

    public String getTermID() {
        return termID;
    }

    public void setTermID(String termID) {
        this.termID = termID;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
