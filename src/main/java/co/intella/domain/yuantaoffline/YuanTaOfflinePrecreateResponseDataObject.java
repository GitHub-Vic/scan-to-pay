package co.intella.domain.yuantaoffline;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class YuanTaOfflinePrecreateResponseDataObject extends YuanTaOfflineResponseDataObject{

    @SerializedName("BankOrderNo")
    @JsonProperty("BankOrderNo")
    private String bankOrderNo;

    @SerializedName("BuyerWallet")
    @JsonProperty("BuyerWallet")
    private String buyerWallet;

    @SerializedName("BankReceiveDT")
    @JsonProperty("BankReceiveDT")
    private String bankReceiveDT;

    @SerializedName("Qrcode")
    @JsonProperty("Qrcode")
    private String qrcode;

    @SerializedName("BigPicUrl")
    @JsonProperty("BigPicUrl")
    private String bigPicUrl;

    @SerializedName("PicUrl")
    @JsonProperty("PicUrl")
    private String picUrl;

    @SerializedName("SmallPicUrl")
    @JsonProperty("SmallPicUrl")
    private String smallPicUrl;

    @SerializedName("OrderCurrency")
    @JsonProperty("OrderCurrency")
    private String orderCurrency;

    @SerializedName("OrderAmount")
    @JsonProperty("OrderAmount")
    private Number orderAmount;

    @SerializedName("RealOrderAmount")
    @JsonProperty("RealOrderAmount")
    private String realOrderAmount;

    @SerializedName("BankCurrency")
    @JsonProperty("BankCurrency")
    private String bankCurrency;

    @SerializedName("Store2BankExRate")
    @JsonProperty("Store2BankExRate")
    private String store2BankExRate;

    @SerializedName("WalletCurrency")
    @JsonProperty("WalletCurrency")
    private String walletCurrency;

    @SerializedName("OrderExtraInfo")
    @JsonProperty("OrderExtraInfo")
    private OrderExtraInfo orderExtraInfo;

    public String getBankOrderNo() {
        return bankOrderNo;
    }

    public void setBankOrderNo(String bankOrderNo) {
        this.bankOrderNo = bankOrderNo;
    }

    public String getBuyerWallet() {
        return buyerWallet;
    }

    public void setBuyerWallet(String buyerWallet) {
        this.buyerWallet = buyerWallet;
    }

    public String getBankReceiveDT() {
        return bankReceiveDT;
    }

    public void setBankReceiveDT(String bankReceiveDT) {
        this.bankReceiveDT = bankReceiveDT;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getBigPicUrl() {
        return bigPicUrl;
    }

    public void setBigPicUrl(String bigPicUrl) {
        this.bigPicUrl = bigPicUrl;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getSmallPicUrl() {
        return smallPicUrl;
    }

    public void setSmallPicUrl(String smallPicUrl) {
        this.smallPicUrl = smallPicUrl;
    }

    public String getOrderCurrency() {
        return orderCurrency;
    }

    public void setOrderCurrency(String orderCurrency) {
        this.orderCurrency = orderCurrency;
    }

    public Number getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(Number orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getRealOrderAmount() {
        return realOrderAmount;
    }

    public void setRealOrderAmount(String realOrderAmount) {
        this.realOrderAmount = realOrderAmount;
    }

    public String getBankCurrency() {
        return bankCurrency;
    }

    public void setBankCurrency(String bankCurrency) {
        this.bankCurrency = bankCurrency;
    }

    public String getStore2BankExRate() {
        return store2BankExRate;
    }

    public void setStore2BankExRate(String store2BankExRate) {
        this.store2BankExRate = store2BankExRate;
    }

    public String getWalletCurrency() {
        return walletCurrency;
    }

    public void setWalletCurrency(String walletCurrency) {
        this.walletCurrency = walletCurrency;
    }

    public OrderExtraInfo getOrderExtraInfo() {
        return orderExtraInfo;
    }

    public void setOrderExtraInfo(OrderExtraInfo orderExtraInfo) {
        this.orderExtraInfo = orderExtraInfo;
    }
}
