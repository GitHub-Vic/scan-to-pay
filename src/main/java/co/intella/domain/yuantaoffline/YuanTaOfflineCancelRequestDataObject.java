package co.intella.domain.yuantaoffline;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class YuanTaOfflineCancelRequestDataObject extends YuanTaOfflineRequestDataObject{

    @SerializedName("CancelOrderDT")
    @JsonProperty("CancelOrderDT")
    private String cancelOrderDT;

    public String getCancelOrderDT() {
        return cancelOrderDT;
    }

    public void setCancelOrderDT(String cancelOrderDT) {
        this.cancelOrderDT = cancelOrderDT;
    }
}
