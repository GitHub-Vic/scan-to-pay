package co.intella.domain.yuantaoffline;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class YuanTaOfflineRequestDataObject {

    @SerializedName("StoreID")
    @JsonProperty("StoreID")
    private String storeID;

    @SerializedName("TermID")
    @JsonProperty("TermID")
    private String termID;

    @SerializedName("Timeout")
    @JsonProperty("Timeout")
    private String timeout;

    @SerializedName("OrderDT")
    @JsonProperty("OrderDT")
    private String orderDT;

    @SerializedName("OrderNo")
    @JsonProperty("OrderNo")
    private String orderNo;

    public String getStoreID() {
        return storeID;
    }

    public void setStoreID(String storeID) {
        this.storeID = storeID;
    }

    public String getTermID() {
        return termID;
    }

    public void setTermID(String termID) {
        this.termID = termID;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public String getOrderDT() {
        return orderDT;
    }

    public void setOrderDT(String orderDT) {
        this.orderDT = orderDT;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
