package co.intella.domain.yuantaoffline;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class YuanTaOfflineRefundRequestDataObject extends YuanTaOfflineRequestDataObject{

    @SerializedName("RefundOrderNo")
    @JsonProperty("RefundOrderNo")
    private String refundOrderNo;

    @SerializedName("RefundOrderDT")
    @JsonProperty("RefundOrderDT")
    private String refundOrderDT;

    @SerializedName("RefundOrderAmount")
    @JsonProperty("RefundOrderAmount")
    private String refundOrderAmount;

    @SerializedName("RefundReason")
    @JsonProperty("RefundReason")
    private String refundReason;

    public String getRefundOrderNo() {
        return refundOrderNo;
    }

    public void setRefundOrderNo(String refundOrderNo) {
        this.refundOrderNo = refundOrderNo;
    }

    public String getRefundOrderDT() {
        return refundOrderDT;
    }

    public void setRefundOrderDT(String refundOrderDT) {
        this.refundOrderDT = refundOrderDT;
    }

    public String getRefundOrderAmount() {
        return refundOrderAmount;
    }

    public void setRefundOrderAmount(String refundOrderAmount) {
        this.refundOrderAmount = refundOrderAmount;
    }

    public String getRefundReason() {
        return refundReason;
    }

    public void setRefundReason(String refundReason) {
        this.refundReason = refundReason;
    }
}
