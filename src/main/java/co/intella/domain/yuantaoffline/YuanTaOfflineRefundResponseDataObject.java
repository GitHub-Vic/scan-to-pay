package co.intella.domain.yuantaoffline;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class YuanTaOfflineRefundResponseDataObject extends YuanTaOfflineResponseDataObject{

    @SerializedName("BankRefundOrderNo")
    @JsonProperty("BankRefundOrderNo")
    private String bankRefundOrderNo;

    @SerializedName("BankRefundOrderDT")
    @JsonProperty("BankRefundOrderDT")
    private String bankRefundOrderDT;

    @SerializedName("WalletOrderNo")
    @JsonProperty("WalletOrderNo")
    private String walletOrderNo;

    @SerializedName("WalletRefundOrderDT")
    @JsonProperty("WalletRefundOrderDT")
    private String walletRefundOrderDT;

    @SerializedName("RefundOrderCurrency")
    @JsonProperty("RefundOrderCurrency")
    private String refundOrderCurrency;

    @SerializedName("RefundOrderAmount")
    @JsonProperty("RefundOrderAmount")
    private String refundOrderAmount;

    @SerializedName("RealRefundOrderAmount")
    @JsonProperty("RealRefundOrderAmount")
    private String realRefundOrderAmount;

    @SerializedName("BankCurrency")
    @JsonProperty("BankCurrency")
    private String bankCurrency;

    @SerializedName("Store2BankRefundExRate")
    @JsonProperty("Store2BankRefundExRate")
    private String store2BankRefundExRate;

    @SerializedName("RealBankRefundAmount")
    @JsonProperty("RealBankRefundAmount")
    private String realBankRefundAmount;

    @SerializedName("WalletCurrency")
    @JsonProperty("WalletCurrency")
    private String walletCurrency;

    @SerializedName("WalletRefundExAmount")
    @JsonProperty("WalletRefundExAmount")
    private String walletRefundExAmount;

    @SerializedName("RealWalletRefundAmount")
    @JsonProperty("RealWalletRefundAmount")
    private String realWalletRefundAmount;

    @SerializedName("BuyerWalletUserID")
    @JsonProperty("BuyerWalletUserID")
    private String buyerWalletUserID;

    @SerializedName("BuyerWalletLoginID")
    @JsonProperty("BuyerWalletLoginID")
    private String buyerWalletLoginID;

    @SerializedName("BuyerWalletBalance")
    @JsonProperty("BuyerWalletBalance")
    private String buyerWalletBalance;

    public String getBankRefundOrderNo() {
        return bankRefundOrderNo;
    }

    public void setBankRefundOrderNo(String bankRefundOrderNo) {
        this.bankRefundOrderNo = bankRefundOrderNo;
    }

    public String getBankRefundOrderDT() {
        return bankRefundOrderDT;
    }

    public void setBankRefundOrderDT(String bankRefundOrderDT) {
        this.bankRefundOrderDT = bankRefundOrderDT;
    }

    public String getWalletOrderNo() {
        return walletOrderNo;
    }

    public void setWalletOrderNo(String walletOrderNo) {
        this.walletOrderNo = walletOrderNo;
    }

    public String getWalletRefundOrderDT() {
        return walletRefundOrderDT;
    }

    public void setWalletRefundOrderDT(String walletRefundOrderDT) {
        this.walletRefundOrderDT = walletRefundOrderDT;
    }

    public String getRefundOrderCurrency() {
        return refundOrderCurrency;
    }

    public void setRefundOrderCurrency(String refundOrderCurrency) {
        this.refundOrderCurrency = refundOrderCurrency;
    }

    public String getRefundOrderAmount() {
        return refundOrderAmount;
    }

    public void setRefundOrderAmount(String refundOrderAmount) {
        this.refundOrderAmount = refundOrderAmount;
    }

    public String getRealRefundOrderAmount() {
        return realRefundOrderAmount;
    }

    public void setRealRefundOrderAmount(String realRefundOrderAmount) {
        this.realRefundOrderAmount = realRefundOrderAmount;
    }

    public String getBankCurrency() {
        return bankCurrency;
    }

    public void setBankCurrency(String bankCurrency) {
        this.bankCurrency = bankCurrency;
    }

    public String getStore2BankRefundExRate() {
        return store2BankRefundExRate;
    }

    public void setStore2BankRefundExRate(String store2BankRefundExRate) {
        this.store2BankRefundExRate = store2BankRefundExRate;
    }

    public String getRealBankRefundAmount() {
        return realBankRefundAmount;
    }

    public void setRealBankRefundAmount(String realBankRefundAmount) {
        this.realBankRefundAmount = realBankRefundAmount;
    }

    public String getWalletCurrency() {
        return walletCurrency;
    }

    public void setWalletCurrency(String walletCurrency) {
        this.walletCurrency = walletCurrency;
    }

    public String getWalletRefundExAmount() {
        return walletRefundExAmount;
    }

    public void setWalletRefundExAmount(String walletRefundExAmount) {
        this.walletRefundExAmount = walletRefundExAmount;
    }

    public String getRealWalletRefundAmount() {
        return realWalletRefundAmount;
    }

    public void setRealWalletRefundAmount(String realWalletRefundAmount) {
        this.realWalletRefundAmount = realWalletRefundAmount;
    }

    public String getBuyerWalletUserID() {
        return buyerWalletUserID;
    }

    public void setBuyerWalletUserID(String buyerWalletUserID) {
        this.buyerWalletUserID = buyerWalletUserID;
    }

    public String getBuyerWalletLoginID() {
        return buyerWalletLoginID;
    }

    public void setBuyerWalletLoginID(String buyerWalletLoginID) {
        this.buyerWalletLoginID = buyerWalletLoginID;
    }

    public String getBuyerWalletBalance() {
        return buyerWalletBalance;
    }

    public void setBuyerWalletBalance(String buyerWalletBalance) {
        this.buyerWalletBalance = buyerWalletBalance;
    }
}
