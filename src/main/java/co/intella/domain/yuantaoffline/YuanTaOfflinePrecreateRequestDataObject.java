package co.intella.domain.yuantaoffline;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class YuanTaOfflinePrecreateRequestDataObject extends YuanTaOfflineRequestDataObject{

    @SerializedName("GoodsShowUrl")
    @JsonProperty("GoodsShowUrl")
    private String goodsShowUrl;

    @SerializedName("TradeLimit")
    @JsonProperty("TradeLimit")
    private String tradeLimit;

    @SerializedName("OrderCurrency")
    @JsonProperty("OrderCurrency")
    private String orderCurrency;

    @SerializedName("OrderAmount")
    @JsonProperty("OrderAmount")
    private String orderAmount;

    @SerializedName("OrderTitle")
    @JsonProperty("OrderTitle")
    private String orderTitle;

    @SerializedName("OrderDesc")
    @JsonProperty("OrderDesc")
    private String orderDesc;

    @SerializedName("ProductQuantity")
    @JsonProperty("ProductQuantity")
    private String productQuantity;

    @SerializedName("OrderExtraInfo")
    @JsonProperty("OrderExtraInfo")
    private OrderExtraInfo orderExtraInfo;

    @SerializedName("ProductInfo")
    @JsonProperty("ProductInfo")
    private String productInfo;

    @SerializedName("notify_url")
    @JsonProperty("notify_url")
    private String notifyUrl;

    public String getGoodsShowUrl() {
        return goodsShowUrl;
    }

    public void setGoodsShowUrl(String goodsShowUrl) {
        this.goodsShowUrl = goodsShowUrl;
    }

    public String getTradeLimit() {
        return tradeLimit;
    }

    public void setTradeLimit(String tradeLimit) {
        this.tradeLimit = tradeLimit;
    }

    public String getOrderCurrency() {
        return orderCurrency;
    }

    public void setOrderCurrency(String orderCurrency) {
        this.orderCurrency = orderCurrency;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getOrderTitle() {
        return orderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }

    public String getOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(String orderDesc) {
        this.orderDesc = orderDesc;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    public OrderExtraInfo getOrderExtraInfo() {
        return orderExtraInfo;
    }

    public void setOrderExtraInfo(OrderExtraInfo orderExtraInfo) {
        this.orderExtraInfo = orderExtraInfo;
    }

    public String getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(String productInfo) {
        this.productInfo = productInfo;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }
}
