package co.intella.domain.yuantaoffline;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class YuanTaOfflineQueryForexResponseDataObject extends YuanTaOfflineResponseDataObject{

    @SerializedName("ExchangeRate")
    @JsonProperty("ExchangeRate")
    private String exchangeRate;

    @SerializedName("ExchangeAmount")
    @JsonProperty("ExchangeAmount")
    private String exchangeAmount;

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getExchangeAmount() {
        return exchangeAmount;
    }

    public void setExchangeAmount(String exchangeAmount) {
        this.exchangeAmount = exchangeAmount;
    }
}
