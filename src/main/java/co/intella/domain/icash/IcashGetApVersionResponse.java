package co.intella.domain.icash;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class IcashGetApVersionResponse extends IcashBaseResponse {

    @JsonProperty("POSApiVer")
    @SerializedName("POSApiVer")
    private String posApiVer;

    @JsonProperty("ICashVer")
    @SerializedName("ICashVer")
    private String icashVer;

    public String getPosApiVer() {
        return posApiVer;
    }

    public void setPosApiVer(String posApiVer) {
        this.posApiVer = posApiVer;
    }

    public String getIcashVer() {
        return icashVer;
    }

    public void setIcashVer(String icashVer) {
        this.icashVer = icashVer;
    }
}
