package co.intella.domain.icash;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IcashTransactionResponse extends IcashBaseResponse {

    @JsonProperty("ICTX2Data")
    @SerializedName("ICTX2Data")
    private List<ICTX2Data> iCTX2Data;

    public List<ICTX2Data> getiCTX2Data() {
        return iCTX2Data;
    }

    public void setiCTX2Data(List<ICTX2Data> iCTX2Data) {
        this.iCTX2Data = iCTX2Data;
    }

    public static class ICTX2Data {
        @JsonProperty("TxData")
        @SerializedName("TxData")
        private List<JsonObject> txData;

        public List<JsonObject> getTxData() {
            return txData;
        }

        public void setTxData(List<JsonObject> txData) {
            this.txData = txData;
        }

    }
}
