package co.intella.domain.icash;

import co.intella.model.Device;
import co.intella.model.PaymentAccount;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class IcashSettlementData {

    public IcashSettlementData(Device device, PaymentAccount paymentAccount) {

        this.setTerminalID(paymentAccount.getAccount());
        this.setDeviceID(device.getOwnerDeviceId());
        this.setStoreNO(paymentAccount.getAppId());
        this.setTaxIDNumber(paymentAccount.getSpID());
        this.setMarketCode(paymentAccount.getPlatformCheckCode());
        this.setPosCode(device.getPosCode());
//        this.setTxDate(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")));
        this.setTxDate("");
        this.setUser(paymentAccount.getSftpUser());
        this.setPassword(paymentAccount.getSftpPasswd());
    }

    @JsonProperty("TerminalID")
    @SerializedName("TerminalID")
    private String terminalID = "";

    @JsonProperty("DeviceID")
    @SerializedName("DeviceID")
    private String deviceID = "";


    @JsonProperty("TaxIDNumber")
    @SerializedName("TaxIDNumber")
    private String taxIDNumber = "";

    @JsonProperty("MarketNumber")
    @SerializedName("MarketNumber")
    private String marketNumber = "";


    @JsonProperty("MarketCode")
    @SerializedName("MarketCode")
    private String marketCode = "";

    @JsonProperty("StoreNO")
    @SerializedName("StoreNO")
    private String storeNO = "";

    @JsonProperty("POSCode")
    @SerializedName("POSCode")
    private String posCode = "";

    @JsonProperty("TxDate")
    @SerializedName("TxDate")
    private String txDate = "";

    @JsonProperty("TxData")
    @SerializedName("TxData")
    private String txData = "[{\"Log01\":\"\"}]";

    @JsonProperty("User")
    @SerializedName("User")
    private String user = "";

    @JsonProperty("Password")
    @SerializedName("Password")
    private String password = "";

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getTaxIDNumber() {
        return taxIDNumber;
    }

    public void setTaxIDNumber(String taxIDNumber) {
        this.taxIDNumber = taxIDNumber;
    }

    public String getMarketNumber() {
        return marketNumber;
    }

    public void setMarketNumber(String marketNumber) {
        this.marketNumber = marketNumber;
    }

    public String getMarketCode() {
        return marketCode;
    }

    public void setMarketCode(String marketCode) {
        this.marketCode = marketCode;
    }

    public String getStoreNO() {
        return storeNO;
    }

    public void setStoreNO(String storeNO) {
        this.storeNO = storeNO;
    }

    public String getPosCode() {
        return posCode;
    }

    public void setPosCode(String posCode) {
        this.posCode = posCode;
    }

    public String getTxDate() {
        return txDate;
    }

    public void setTxDate(String txDate) {
        this.txDate = txDate;
    }

    public String getTxData() {
        return txData;
    }

    public void setTxData(String txData) {
        this.txData = txData;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
