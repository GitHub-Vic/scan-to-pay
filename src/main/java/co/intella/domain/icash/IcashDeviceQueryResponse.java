package co.intella.domain.icash;

import co.intella.domain.contratStore.DeviceVo;
import co.intella.domain.easycard.EzCardBasicResponse;
import co.intella.domain.integration.ResponseGeneralData;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IcashDeviceQueryResponse extends IcashBaseResponse {

    @SerializedName("DeviceList")
    private List<DeviceVo> deviceList;

    @JsonProperty("OrderId")
    @SerializedName("OrderId")
    private String orderId;

    public List<DeviceVo> getDeviceList() {
        return deviceList;
    }

    public void setDeviceList(List<DeviceVo> deviceList) {
        this.deviceList = deviceList;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
