package co.intella.domain.icash;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IcashQueryResponse extends IcashTermAuthResponse {

    @SerializedName("OrderList")
    private List<IcashTradeDetailVo> list;

    public List<IcashTradeDetailVo> getList() {
        return list;
    }

    public void setList(List<IcashTradeDetailVo> list) {
        this.list = list;
    }
}

