package co.intella.domain.icash;

import co.intella.model.Device;
import co.intella.model.PaymentAccount;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * 購貨(整合聯名卡自動加值)	on-line
 */
public class IcashTradeSaleRequest extends IcashBaseRequest {

    @JsonProperty("Amount")
    @SerializedName("Amount")
    private String amount;

    /**
     * 基礎初始化資料，並set  device 的 postradesn、lastusedate
     *
     * @param testMode
     * @param device
     * @param paymentAccount
     */
    public IcashTradeSaleRequest(String testMode, Device device, PaymentAccount paymentAccount) {
        super(testMode, device, paymentAccount);
        this.setServiceType("TradeSale");
        this.setCommandMode("3");
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

}
