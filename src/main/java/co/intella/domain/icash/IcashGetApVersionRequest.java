package co.intella.domain.icash;

import co.intella.model.Device;
import co.intella.model.PaymentAccount;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class IcashGetApVersionRequest extends IcashBaseRequest {

    /**
     * 基礎初始化資料，並set  device 的 postradesn、lastusedate
     * @param testMode
     * @param device
     * @param paymentAccount
     */
    public IcashGetApVersionRequest(String testMode, Device device, PaymentAccount paymentAccount) {
        super(testMode, device, paymentAccount);
        this.setServiceType("GetApVersion");
        this.setCommandMode("8");
    }
}
