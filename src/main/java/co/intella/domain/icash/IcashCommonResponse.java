package co.intella.domain.icash;

import co.intella.domain.integration.ResponseGeneralData;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class IcashCommonResponse extends ResponseGeneralData {

    @JsonProperty("TXNResult")
    @SerializedName("TXNResult")
    private String txnResult;

    @JsonProperty("ErrorCode")
    @SerializedName("ErrorCode")
    private String errorCode;

    public String getTxnResult() {
        return txnResult;
    }

    public void setTxnResult(String txnResult) {
        this.txnResult = txnResult;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
