package co.intella.domain.icash;

import co.intella.domain.integration.ResponseGeneralData;
import com.google.gson.annotations.SerializedName;

public class IcashBatchCheckoutResponse extends ResponseGeneralData {

    @SerializedName("TerminalTXNNumber")
    private String terminalTXNNumber;

    @SerializedName("HostSerialNumber")
    private String hostSerialNumber;

    @SerializedName("TerminalID")
    private String terminalId;

    @SerializedName("SettlementStatus")
    private String settlementStatus;

    @SerializedName("TxReserveAmount")
    private long txReserveAmount;

    @SerializedName("TxTradeAmount")
    private long txTradeAmount;

    @SerializedName("TxRefundAmount")
    private long txRefundAmount;

    @SerializedName("TxCancelAmount")
    private long txCancelAmount;

    @SerializedName("TxReserveCount")
    private long txReserveCount;

    @SerializedName("TxTradeCount")
    private long txTradeCount;

    @SerializedName("TxRefundCount")
    private long txRefundCount;

    @SerializedName("TxCancelCount")
    private long txCancelCount;

    public String getTerminalTXNNumber() {
        return terminalTXNNumber;
    }

    public void setTerminalTXNNumber(String terminalTXNNumber) {
        this.terminalTXNNumber = terminalTXNNumber;
    }

    public String getHostSerialNumber() {
        return hostSerialNumber;
    }

    public void setHostSerialNumber(String hostSerialNumber) {
        this.hostSerialNumber = hostSerialNumber;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getSettlementStatus() {
        return settlementStatus;
    }

    public void setSettlementStatus(String settlementStatus) {
        this.settlementStatus = settlementStatus;
    }

    public long getTxReserveAmount() {
        return txReserveAmount;
    }

    public void setTxReserveAmount(long txReserveAmount) {
        this.txReserveAmount = txReserveAmount;
    }

    public long getTxTradeAmount() {
        return txTradeAmount;
    }

    public void setTxTradeAmount(long txTradeAmount) {
        this.txTradeAmount = txTradeAmount;
    }

    public long getTxRefundAmount() {
        return txRefundAmount;
    }

    public void setTxRefundAmount(long txRefundAmount) {
        this.txRefundAmount = txRefundAmount;
    }

    public long getTxReserveCount() {
        return txReserveCount;
    }

    public void setTxReserveCount(long txReserveCount) {
        this.txReserveCount = txReserveCount;
    }

    public long getTxTradeCount() {
        return txTradeCount;
    }

    public void setTxTradeCount(long txTradeCount) {
        this.txTradeCount = txTradeCount;
    }

    public long getTxRefundCount() {
        return txRefundCount;
    }

    public void setTxRefundCount(long txRefundCount) {
        this.txRefundCount = txRefundCount;
    }

    public long getTxCancelAmount() {
        return txCancelAmount;
    }

    public void setTxCancelAmount(long txCancelAmount) {
        this.txCancelAmount = txCancelAmount;
    }

    public long getTxCancelCount() {
        return txCancelCount;
    }

    public void setTxCancelCount(long txCancelCount) {
        this.txCancelCount = txCancelCount;
    }
}
