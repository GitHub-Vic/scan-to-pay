package co.intella.domain.icash;

import co.intella.domain.contratStore.EzcTradeDetailVo;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IcashOrderQueryResponse  extends IcashBaseResponse{

    @SerializedName("OrderList")
    private List<EzcTradeDetailVo> list;

    public List<EzcTradeDetailVo> getList() {
        return list;
    }

    public void setList(List<EzcTradeDetailVo> list) {
        this.list = list;
    }
}
