package co.intella.domain.icash;

import co.intella.model.Device;
import co.intella.model.PaymentAccount;

/**
 * 端末授權	啟用時要連線授權(已包含申裝開通功能)
 */
public class IcashTermAuthRequest extends IcashBaseRequest {
    /**
     * 基礎初始化資料，並set  device 的 postradesn、lastusedate
     * @param testMode
     * @param device
     * @param paymentAccount
     */
    public IcashTermAuthRequest(String testMode, Device device, PaymentAccount paymentAccount) {
        super(testMode, device, paymentAccount);
        this.setServiceType("TermAuth");
        this.setCommandMode("1");
    }
}
