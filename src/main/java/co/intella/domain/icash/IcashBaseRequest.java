package co.intella.domain.icash;

import co.intella.model.Device;
import co.intella.model.PaymentAccount;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class IcashBaseRequest {

    @JsonProperty("TestMode")
    @SerializedName("TestMode")
    private String testMode;

    @JsonProperty("CommandMode")
    @SerializedName("CommandMode")
    private String commandMode;

    @JsonProperty("ServiceType")
    @SerializedName("ServiceType")
    private String serviceType;

    @JsonProperty("TerminalID")
    @SerializedName("TerminalID")
    private String terminalID;

    @JsonProperty("DeviceID")
    @SerializedName("DeviceID")
    private String deviceID;

    @JsonProperty("MarketCode")
    @SerializedName("MarketCode")
    private String marketCode;

    @JsonProperty("StoreNO")
    @SerializedName("StoreNO")
    private String storeNO;

    @JsonProperty("POSCode")
    @SerializedName("POSCode")
    private String posCode;

    @JsonProperty("POSTradeSN")
    @SerializedName("POSTradeSN")
    private String posTradeSN;

    @JsonProperty("CashierCode")
    @SerializedName("CashierCode")
    private String cashierCode;

    @JsonProperty("Time")
    @SerializedName("Time")
    private String time;

    @JsonProperty("Date")
    @SerializedName("Date")
    private String date;

    @JsonProperty("AESKey")
    @SerializedName("AESKey")
    private String aesKey;

    @JsonProperty("TxData")
    @SerializedName("TxData")
    private String txData = "";

    /**
     * 基礎初始化資料，並set  device 的 postradesn、lastusedate
     * setCashierCode  有額外做事！
     *
     * @param testMode
     * @param device
     * @param paymentAccount
     */
    public IcashBaseRequest(String testMode, Device device, PaymentAccount paymentAccount) {
        LocalDateTime now = LocalDateTime.now();
        String date = now.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        String time = now.format(DateTimeFormatter.ofPattern("HHmmss"));

        this.setTestMode(testMode);
        this.setTerminalID(paymentAccount.getAccount());
        this.setDeviceID(device.getOwnerDeviceId());
        this.setStoreNO(paymentAccount.getAppId());

        this.setPosCode(device.getPosCode());
        this.setMarketCode(paymentAccount.getPlatformCheckCode());
        this.setCashierCode("0001");        //預設
        this.setTime(time);
        this.setDate(date);
        this.setAesKey(device.getAesKey());

        if (StringUtils.isNotEmpty(device.getLastusedate()) && date.equals(device.getLastusedate())) {
            this.setPosTradeSN(String.format("%06d", Integer.valueOf(device.getPostradesn()) + 1));
            device.setPostradesn(String.valueOf(Integer.valueOf(device.getPostradesn()) + 1));
        } else {
            this.setPosTradeSN(String.format("%06d", 1));
            device.setLastusedate(date);
            device.setPostradesn("1");
        }
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getMarketCode() {
        return marketCode;
    }

    public void setMarketCode(String marketCode) {
        this.marketCode = marketCode;
    }

    public String getStoreNO() {
        return storeNO;
    }

    public void setStoreNO(String storeNO) {
        this.storeNO = storeNO;
    }

    public String getPosCode() {
        return posCode;
    }

    public void setPosCode(String posCode) {
        this.posCode = posCode;
    }

    public String getPosTradeSN() {
        return posTradeSN;
    }

    public void setPosTradeSN(String posTradeSN) {
        this.posTradeSN = posTradeSN;
    }

    public String getCashierCode() {
        return cashierCode;
    }

    /**
     * 因只接受四碼純數字
     * 我有額外做事    Sol
     * @param cashierCode
     */
    public void setCashierCode(String cashierCode) {
        String cashier = cashierCode.replaceAll("(\\D*)?(\\d*)(\\D*)?", "$2");
        cashier = cashier.substring(0, Integer.min(4, cashier.length()));
        this.cashierCode = String.format("%04d", Long.valueOf(cashier));
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAesKey() {
        return aesKey;
    }

    public void setAesKey(String aesKey) {
        this.aesKey = aesKey;
    }

    public String getTestMode() {
        return testMode;
    }

    public void setTestMode(String testMode) {
        this.testMode = testMode;
    }

    public String getCommandMode() {
        return commandMode;
    }

    public void setCommandMode(String commandMode) {
        this.commandMode = commandMode;
    }

    public String getTxData() {
        return txData;
    }

    public void setTxData(String txData) {
        this.txData = txData;
    }
}