package co.intella.domain.icash;

import co.intella.domain.integration.ResponseGeneralData;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IcashBaseResponse extends ResponseGeneralData {

    @JsonProperty("TXNResult")
    @SerializedName("TXNResult")
    private String txnResult;

    @JsonProperty("ErrorCode")
    @SerializedName("ErrorCode")
    private String errorCode;

    @JsonProperty("McErrorCode")
    @SerializedName("McErrorCode")
    private String mcErrorCode;

    @JsonProperty("ErrorMessage")
    @SerializedName("ErrorMessage")
    private String errorMessage;

    @JsonProperty("ChineseErrorMessage")
    @SerializedName("ChineseErrorMessage")
    private String chineseErrorMessage;

    @JsonProperty("NewAESKey")
    @SerializedName("NewAESKey")
    private String newAesKey;

    @JsonProperty("Time")
    @SerializedName("Time")
    private String time;

    @JsonProperty("TerminalID")
    @SerializedName("TerminalID")
    private String terminalID;

    @JsonProperty("DeviceID")
    @SerializedName("DeviceID")
    private String deviceID;

    @JsonProperty("Date")
    @SerializedName("Date")
    private String date;

    @JsonProperty("AutoTopUpAmount")
    @SerializedName("AutoTopUpAmount")
    private String autoTopUpAmount;

    public String getTxnResult() {
        return txnResult;
    }

    public void setTxnResult(String txnResult) {
        this.txnResult = txnResult;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getNewAesKey() {
        return newAesKey;
    }

    public void setNewAesKey(String newAesKey) {
        this.newAesKey = newAesKey;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getAutoTopUpAmount() {
        return autoTopUpAmount;
    }

    public void setAutoTopUpAmount(String autoTopUpAmount) {
        this.autoTopUpAmount = autoTopUpAmount;
    }

    public String getChineseErrorMessage() {
        return chineseErrorMessage;
    }

    public void setChineseErrorMessage(String chineseErrorMessage) {
        this.chineseErrorMessage = chineseErrorMessage;
    }

    public String getMcErrorCode() {
        return mcErrorCode;
    }

    public void setMcErrorCode(String mcErrorCode) {
        this.mcErrorCode = mcErrorCode;
    }
}
