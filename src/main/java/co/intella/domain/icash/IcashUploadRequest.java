package co.intella.domain.icash;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class IcashUploadRequest {

    @JsonProperty("Time")
    @SerializedName("Time")
    private String time;

    @JsonProperty("Date")
    @SerializedName("Date")
    private String date;

    @JsonProperty("TaxIDNumber")
    @SerializedName("TaxIDNumber")
    private String taxIDNumber;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTaxIDNumber() {
        return taxIDNumber;
    }

    public void setTaxIDNumber(String taxIDNumber) {
        this.taxIDNumber = taxIDNumber;
    }
}
