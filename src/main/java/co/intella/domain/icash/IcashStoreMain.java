package co.intella.domain.icash;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.StringUtils;

/**
 * 門市主檔用，因有要求，
 * 故在set都強制一律轉BIG5 再轉Unicode
 */
public class IcashStoreMain {


    @JsonProperty("TaxIDNumber")
    @SerializedName("TaxIDNumber")
    private String taxIDNumber = "";

    @JsonProperty("StoreMatchCode")
    @SerializedName("StoreMatchCode")
    private String storeMatchCode = "";

    @JsonProperty("StoreFullName")
    @SerializedName("StoreFullName")
    private String storeFullName = "";

    @JsonProperty("StoreAbbreviation")
    @SerializedName("StoreAbbreviation")
    private String storeAbbreviation = "";

    @JsonProperty("StartDate")
    @SerializedName("StartDate")
    private String startDate = "";

    @JsonProperty("EndDate")
    @SerializedName("EndDate")
    private String endDate = "";

    @JsonProperty("OpenDate")
    @SerializedName("OpenDate")
    private String openDate = "";

    @JsonProperty("SalesDate")
    @SerializedName("SalesDate")
    private String salesDate = "";

    @JsonProperty("CloseDate")
    @SerializedName("CloseDate")
    private String closeDate = "";

    @JsonProperty("PostalCode")
    @SerializedName("PostalCode")
    private String postalCode = "";

    @JsonProperty("Address")
    @SerializedName("Address")
    private String address = "";

    @JsonProperty("PhoneAreaNum")
    @SerializedName("PhoneAreaNum")
    private String phoneAreaNum = "";

    @JsonProperty("PhoneNum")
    @SerializedName("PhoneNum")
    private String phoneNum = "";

    @JsonProperty("User")
    @SerializedName("User")
    private String user = "";

    @JsonProperty("Password")
    @SerializedName("Password")
    private String password= "";

    private String toBig5Unicode(String str) {
        String result = str;
        try {
            if (StringUtils.isNotEmpty(str)) {
//                result = new String(str.getBytes("UTF-8"), "BIG5");
                result = this.StrToUnicode2(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private String StrToUnicode2(String str) throws Exception {
        StringBuffer outHexStrBuf = new StringBuffer();
        for (char c : str.toCharArray()) {
            outHexStrBuf.append("\\u");
            String hexStr = Integer.toHexString(c);
            for (int i = 0; i < (4 - hexStr.length()); i++) outHexStrBuf.append("0");
            outHexStrBuf.append(hexStr);
        }
        return outHexStrBuf.toString();
    }

    public String getTaxIDNumber() {
        return taxIDNumber;
    }

    public void setTaxIDNumber(String taxIDNumber) {
        this.taxIDNumber = taxIDNumber;
    }

    public String getStoreMatchCode() {
        return storeMatchCode;
    }

    public void setStoreMatchCode(String storeMatchCode) {
        this.storeMatchCode = storeMatchCode;
    }

    public String getStoreFullName() {
        return storeFullName;
    }

    public void setStoreFullName(String storeFullName) {
        this.storeFullName = toBig5Unicode(storeFullName);
    }

    public String getStoreAbbreviation() {
        return storeAbbreviation;
    }

    public void setStoreAbbreviation(String storeAbbreviation) {
        this.storeAbbreviation = toBig5Unicode(storeAbbreviation);
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public String getSalesDate() {
        return salesDate;
    }

    public void setSalesDate(String salesDate) {
        this.salesDate = salesDate;
    }

    public String getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = toBig5Unicode(address);
    }

    public String getPhoneAreaNum() {
        return phoneAreaNum;
    }

    public void setPhoneAreaNum(String phoneAreaNum) {
        this.phoneAreaNum = phoneAreaNum;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
