package co.intella.domain.icash;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class IcashNonTransactionRequest {

    @JsonProperty("TestMode")
    @SerializedName("TestMode")
    private String testMode = "";

    @JsonProperty("ServiceType")
    @SerializedName("ServiceType")
    private String serviceType = "";

    @JsonProperty("Time")
    @SerializedName("Time")
    private String time = "";

    @JsonProperty("Date")
    @SerializedName("Date")
    private String date = "";

    @JsonProperty("TaxIDNumber")
    @SerializedName("TaxIDNumber")
    private String taxIDNumber = "";

    @JsonProperty("AutoUpload")
    @SerializedName("AutoUpload")
    private String autoUpload = "";

    @JsonProperty("SettlementType")
    @SerializedName("SettlementType")
    private String settlementType = "";

    @JsonProperty("TxLogType")
    @SerializedName("TxLogType")
    private String txLogType = "";

    @JsonProperty("Count")
    @SerializedName("Count")
    private String count = "";

    @JsonProperty("FileSN")
    @SerializedName("FileSN")
    private String fileSN = "";

    @JsonProperty("Data")
    @SerializedName("Data")
    private String data = "[{}]";

    public IcashNonTransactionRequest(String testMode) {
        LocalDateTime now = LocalDateTime.now();
        this.setDate(now.format(DateTimeFormatter.ofPattern("yyyyMMdd")));
        this.setTime(now.format(DateTimeFormatter.ofPattern("HHmmss")));
        this.setTestMode(testMode);
        this.setAutoUpload("1");
        this.setSettlementType("2");
        this.setTxLogType("0");
    }

    public String getTestMode() {
        return testMode;
    }

    public void setTestMode(String testMode) {
        this.testMode = testMode;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTaxIDNumber() {
        return taxIDNumber;
    }

    public void setTaxIDNumber(String taxIDNumber) {
        this.taxIDNumber = taxIDNumber;
    }

    public String getAutoUpload() {
        return autoUpload;
    }

    public void setAutoUpload(String autoUpload) {
        this.autoUpload = autoUpload;
    }

    public String getSettlementType() {
        return settlementType;
    }

    public void setSettlementType(String settlementType) {
        this.settlementType = settlementType;
    }

    public String getTxLogType() {
        return txLogType;
    }

    public void setTxLogType(String txLogType) {
        this.txLogType = txLogType;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getFileSN() {
        return fileSN;
    }

    public void setFileSN(String fileSN) {
        this.fileSN = fileSN;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

}
