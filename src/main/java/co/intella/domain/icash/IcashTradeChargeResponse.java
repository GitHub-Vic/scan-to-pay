package co.intella.domain.icash;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class IcashTradeChargeResponse extends IcashTransactionResponse {

    @JsonProperty("CardNumberForPrint")
    @SerializedName("CardNumberForPrint")
    private String cardNumberForPrint;

    @JsonProperty("OrderId")
    @SerializedName("OrderId")
    private String orderId;

    @JsonProperty("Amount")
    @SerializedName("Amount")
    private String amount;

    @JsonProperty("AutoTopUpResult")
    @SerializedName("AutoTopUpResult")
    private String autoTopUpResult;

    @JsonProperty("BeforeTXNBalance")
    @SerializedName("BeforeTXNBalance")
    private String beforeTXNBalance;

    @JsonProperty("Balance")
    @SerializedName("Balance")
    private String balance;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAutoTopUpResult() {
        return autoTopUpResult;
    }

    public void setAutoTopUpResult(String autoTopUpResult) {
        this.autoTopUpResult = autoTopUpResult;
    }

    public String getBeforeTXNBalance() {
        return beforeTXNBalance;
    }

    public void setBeforeTXNBalance(String beforeTXNBalance) {
        this.beforeTXNBalance = beforeTXNBalance;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
}
