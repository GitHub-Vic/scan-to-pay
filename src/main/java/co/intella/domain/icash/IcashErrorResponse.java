package co.intella.domain.icash;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class IcashErrorResponse extends IcashTermAuthResponse{

    @JsonProperty("ErrorMessage")
    @SerializedName("ErrorMessage")
    private String ErrorMessage;

    @JsonProperty("Method")
    @SerializedName("Method")
    private String Method;

    @JsonProperty("ServiceType")
    @SerializedName("ServiceType")
    private String ServiceType;

    @JsonProperty("ServiceType")
    @SerializedName("MerchantId")
    private String MerchantId;

    @JsonProperty("ServiceType")
    @SerializedName("DeviceId")
    private String DeviceId;

    public String getErrorMessage() {
        return ErrorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.ErrorMessage = errorMessage;
    }

    public String getMethod() {
        return Method;
    }

    public void setMethod(String method) {
        this.Method = method;
    }

    public String getServiceType() {
        return ServiceType;
    }

    public void setServiceType(String serviceType) {
        this.ServiceType = serviceType;
    }

    public String getMerchantId() {
        return MerchantId;
    }

    public void setMerchantId(String merchantId) {
        this.MerchantId = merchantId;
    }

    public String getDeviceId() {
        return DeviceId;
    }

    public void setDeviceId(String deviceId) {
        this.DeviceId = deviceId;
    }
}
