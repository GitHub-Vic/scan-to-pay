package co.intella.domain.icash;


import co.intella.model.Device;
import co.intella.model.PaymentAccount;

/**
 * 讀取卡片資訊	off-line
 */
public class IcashQueryCardInfoRequest extends IcashBaseRequest {
    /**
     * 基礎初始化資料，並set  device 的 postradesn、lastusedate
     * @param testMode
     * @param device
     * @param paymentAccount
     */
    public IcashQueryCardInfoRequest(String testMode, Device device, PaymentAccount paymentAccount) {
        super(testMode, device, paymentAccount);
        this.setServiceType("QueryCardInfo");
        this.setCommandMode("2");
    }
}
