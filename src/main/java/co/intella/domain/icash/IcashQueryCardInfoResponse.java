package co.intella.domain.icash;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class IcashQueryCardInfoResponse extends IcashBaseResponse {

    @JsonProperty("CardNumberForPrint")
    @SerializedName("CardNumberForPrint")
    private String cardNumberForPrint;

    @JsonProperty("Balance")
    @SerializedName("Balance")
    private String balance;

    @JsonProperty("CardType")
    @SerializedName("CardType")
    private String cardType;

    @JsonProperty("ExpiredDate")
    @SerializedName("ExpiredDate")
    private String expiredDate;

    @JsonProperty("CardStatus")
    @SerializedName("CardStatus")
    private String cardStatus;

    @JsonProperty("IpointFlag")
    @SerializedName("IpointFlag")
    private String ipointFlag;

    @JsonProperty("LastTxlog")
    @SerializedName("LastTxlog")
    private String lastTxlog;

    @JsonProperty("EZCardID")
    @SerializedName("EZCardID")
    private String ezCardID;

    public String getCardNumberForPrint() {
        return cardNumberForPrint;
    }

    public void setCardNumberForPrint(String cardNumberForPrint) {
        this.cardNumberForPrint = cardNumberForPrint;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(String expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(String cardStatus) {
        this.cardStatus = cardStatus;
    }

    public String getIpointFlag() {
        return ipointFlag;
    }

    public void setIpointFlag(String ipointFlag) {
        this.ipointFlag = ipointFlag;
    }

    public String getLastTxlog() {
        return lastTxlog;
    }

    public void setLastTxlog(String lastTxlog) {
        this.lastTxlog = lastTxlog;
    }

    public String getEzCardID() {
        return ezCardID;
    }

    public void setEzCardID(String ezCardID) {
        this.ezCardID = ezCardID;
    }
}
