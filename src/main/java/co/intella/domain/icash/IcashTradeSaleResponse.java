package co.intella.domain.icash;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class IcashTradeSaleResponse extends IcashTransactionResponse {

    @JsonProperty("CardNumberForPrint")
    @SerializedName("CardNumberForPrint")
    private String cardNumberForPrint;

    @JsonProperty("OrderId")
    @SerializedName("OrderId")
    private String orderId;

    @JsonProperty("Amount")
    @SerializedName("Amount")
    private String amount;

    @JsonProperty("AutoTopUpResult")
    @SerializedName("AutoTopUpResult")
    private String autoTopUpResult;

    @JsonProperty("Balance")
    @SerializedName("Balance")
    private String balance;

    @JsonProperty("BeforeTXNBalance")
    @SerializedName("BeforeTXNBalance")
    private String beforeTXNBalance;

    @JsonProperty("CardDesc")
    @SerializedName("CardDesc")
    private String cardDesc;

    @JsonProperty("EZCardID")
    @SerializedName("EZCardID")
    private String ezCardId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAutoTopUpResult() {
        return autoTopUpResult;
    }

    public void setAutoTopUpResult(String autoTopUpResult) {
        this.autoTopUpResult = autoTopUpResult;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getBeforeTXNBalance() {
        return beforeTXNBalance;
    }

    public void setBeforeTXNBalance(String beforeTXNBalance) {
        this.beforeTXNBalance = beforeTXNBalance;
    }

    public String getCardDesc() {
        return cardDesc;
    }

    public void setCardDesc(String cardDesc) {
        this.cardDesc = cardDesc;
    }

    public String getEzCardId() {
        return ezCardId;
    }

    public void setEzCardId(String ezCardId) {
        this.ezCardId = ezCardId;
    }

    public String getCardNumberForPrint() {
        return cardNumberForPrint;
    }

    public void setCardNumberForPrint(String cardNumberForPrint) {
        this.cardNumberForPrint = cardNumberForPrint;
    }
}
