package co.intella.domain.icash;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Map;

public class IcashBuildStoreFileRequest {

    @JsonProperty("TaxIDNumber")
    @SerializedName("TaxIDNumber")
    private String taxIDNumber;

    @JsonProperty("Time")
    @SerializedName("Time")
    private String time;

    @JsonProperty("Date")
    @SerializedName("Date")
    private String date;

    @JsonProperty("Count")
    @SerializedName("Count")
    private String count;

    @JsonProperty("StoreData")
    @SerializedName("StoreData")
    private ArrayList storeData;

    public String getTaxIDNumber() {
        return taxIDNumber;
    }

    public void setTaxIDNumber(String taxIDNumber) {
        this.taxIDNumber = taxIDNumber;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public ArrayList getStoreData() {
        return storeData;
    }

    public void setStoreData(ArrayList storeData) {
        this.storeData = storeData;
    }
}
