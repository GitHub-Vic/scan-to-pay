package co.intella.domain;

public class PaymentHeader {

	public String method;
	public String serviceType;
	public String mchId;
	public String tradeKey;
	public String createTime;

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getMchId() {
		return mchId;
	}

	public void setMchId(String mchId) {
		this.mchId = mchId;
	}

	public String getTradeKey() {
		return tradeKey;
	}

	public void setTradeKey(String tradeKey) {
		this.tradeKey = tradeKey;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

}
