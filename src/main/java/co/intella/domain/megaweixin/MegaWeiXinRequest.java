package co.intella.domain.megaweixin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class MegaWeiXinRequest {

	@SerializedName("Header")
	@JsonProperty("Header")
	private MegaWeiXinRequestHeader header;

	@SerializedName("Data")
	@JsonProperty("Data")
	private MegaWeiXinRequestData data;
	
	public MegaWeiXinRequest() {
		super();
	}

	public MegaWeiXinRequest(MegaWeiXinRequestHeader header, MegaWeiXinRequestData data) {
		super();
		this.header = header;
		this.data = data;
	}

	public MegaWeiXinRequestHeader getHeader() {
		return header;
	}

	public void setHeader(MegaWeiXinRequestHeader header) {
		this.header = header;
	}

	public MegaWeiXinRequestData getData() {
		return data;
	}

	public void setData(MegaWeiXinRequestData data) {
		this.data = data;
	}
	
}
