package co.intella.domain.megaweixin;

public class WeiXinMicroPayReq {
	
	private String store_order_no;
	private String device_info;
	private String desc;
	private String purpose_type;
	private int order_amount;
	private String currency;
	private String spbill_create_ip;
	private String auth_code;
	private String detail;
	private String attach;
	private String goods_tag;
	private String user_id;
	
	public WeiXinMicroPayReq() {
		super();
	}
	
	public WeiXinMicroPayReq(String store_order_no, String device_info, String desc, String purpose_type,
			int order_amount, String currency, String spbill_create_ip, String auth_code, String detail) {
		super();
		this.store_order_no = store_order_no;
		this.device_info = device_info;
		this.desc = desc;
		this.purpose_type = purpose_type;
		this.order_amount = order_amount;
		this.currency = "TWD";
		this.spbill_create_ip = spbill_create_ip;
		this.auth_code = auth_code;
		this.detail = detail;
	}
	
	public String getStore_order_no() {
		return store_order_no;
	}
	public void setStore_order_no(String store_order_no) {
		this.store_order_no = store_order_no;
	}
	public String getDevice_info() {
		return device_info;
	}
	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getPurpose_type() {
		return purpose_type;
	}
	public void setPurpose_type(String purpose_type) {
		this.purpose_type = purpose_type;
	}
	public int getOrder_amount() {
		return order_amount;
	}
	public void setOrder_amount(int order_amount) {
		this.order_amount = order_amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getSpbill_create_ip() {
		return spbill_create_ip;
	}
	public void setSpbill_create_ip(String spbill_create_ip) {
		this.spbill_create_ip = spbill_create_ip;
	}
	public String getAuth_code() {
		return auth_code;
	}
	public void setAuth_code(String auth_code) {
		this.auth_code = auth_code;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getAttach() {
		return attach;
	}
	public void setAttach(String attach) {
		this.attach = attach;
	}
	public String getGoods_tag() {
		return goods_tag;
	}
	public void setGoods_tag(String goods_tag) {
		this.goods_tag = goods_tag;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

}
