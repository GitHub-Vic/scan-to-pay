package co.intella.domain.megaweixin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class JsapiHeaderRequestData {
	@SerializedName("TradeMode")
	@JsonProperty("TradeMode")
	private String tradeMode;
	
	@SerializedName("TradeTime")
	@JsonProperty("TradeTime")
	private String tradeTime;
	
	@SerializedName("BarcodeSeq")
	@JsonProperty("BarcodeSeq")
	private String barcodeSeq;
	
	@SerializedName("TradeCurrency")
	@JsonProperty("TradeCurrency")
	private String tradeCurrency;
	
	@SerializedName("TradeAmount")
	@JsonProperty("TradeAmount")
	private String tradeAmount;
	
	@SerializedName("ProductDesc")
	@JsonProperty("ProductDesc")
	private String productDesc;
	
	@SerializedName("DataValue")
	@JsonProperty("DataValue")
	private String dataValue;

	public JsapiHeaderRequestData() {
		super();
	}
	
	public JsapiHeaderRequestData(WeiXinUnifiedorderJsapiReq weiXinUnifiedorderJsapiReq, String productDesc) {
		super();
		this.tradeMode = "FTF";
		this.tradeTime = weiXinUnifiedorderJsapiReq.getTradeTime();
		this.barcodeSeq = weiXinUnifiedorderJsapiReq.getBarcodeSeq();
		this.tradeCurrency = weiXinUnifiedorderJsapiReq.getTradeCurr();
		this.tradeAmount = weiXinUnifiedorderJsapiReq.getTradeAmount();
//		this.productDesc = productDesc;
	}

	public String getTradeMode() {
		return tradeMode;
	}

	public void setTradeMode(String tradeMode) {
		this.tradeMode = tradeMode;
	}

	public String getTradeTime() {
		return tradeTime;
	}

	public void setTradeTime(String tradeTime) {
		this.tradeTime = tradeTime;
	}

	public String getBarcodeSeq() {
		return barcodeSeq;
	}

	public void setBarcodeSeq(String barcodeSeq) {
		this.barcodeSeq = barcodeSeq;
	}

	public String getTradeCurrency() {
		return tradeCurrency;
	}

	public void setTradeCurrency(String tradeCurrency) {
		this.tradeCurrency = tradeCurrency;
	}

	public String getTradeAmount() {
		return tradeAmount;
	}

	public void setTradeAmount(String tradeAmount) {
		this.tradeAmount = tradeAmount;
	}

	public String getProductDesc() {
		return productDesc;
	}

	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}

	public String getDataValue() {
		return dataValue;
	}

	public void setDataValue(String dataValue) {
		this.dataValue = dataValue;
	}
}
