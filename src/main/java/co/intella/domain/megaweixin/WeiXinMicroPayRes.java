package co.intella.domain.megaweixin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class WeiXinMicroPayRes {
	
	  @SerializedName("SYS_ORDER_NO")
	  @JsonProperty("SYS_ORDER_NO")
	  private String sys_order_no;

	public String getSys_order_no() {
		return sys_order_no;
	}

	public void setSys_order_no(String sys_order_no) {
		this.sys_order_no = sys_order_no;
	}
	 
}
