package co.intella.domain.megaweixin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class WeiXinUnifiedorderJsapiReq {
	
	@SerializedName("StoreOrderNo")
	@JsonProperty("StoreOrderNo")
	private String storeOrderNo;
	
	@SerializedName("BarcodeSeq")
	@JsonProperty("BarcodeSeq")
	private String barcodeSeq;
	
	@SerializedName("TradeTime")
	@JsonProperty("TradeTime")
	private String tradeTime;
	
	@SerializedName("TradeCurrency")
	@JsonProperty("TradeCurrency")
	private String tradeCurrency;
	
	@SerializedName("TradeAmount")
	@JsonProperty("TradeAmount")
	private String tradeAmount;
	
	@SerializedName("PurposeType")
	@JsonProperty("PurposeType")
	private String purposeType;
	
	@SerializedName("BuyrDesc")
	@JsonProperty("BuyrDesc")
	private String buyrDesc;
	
	@SerializedName("BusinessId")
	@JsonProperty("BusinessId")
	private String businessId;
	
	@SerializedName("UserId")
	@JsonProperty("UserId")
	private String userId;
	
	private String detail;
	
	public WeiXinUnifiedorderJsapiReq() {
		super();
	}

	public WeiXinUnifiedorderJsapiReq(String storeOrderNo, String barcodeSeq, String tradeTime, String tradeAmount, String purposeType) {
		super();
		this.storeOrderNo = storeOrderNo;
		this.barcodeSeq = barcodeSeq;
		this.tradeTime = tradeTime;
		this.tradeCurrency = "TWD";
		this.tradeAmount = tradeAmount;
//		this.purposeType = purposeType;
	}

	public String getBarcodeSeq() {
		return barcodeSeq;
	}

	public void setBarcodeSeq(String barcodeSeq) {
		this.barcodeSeq = barcodeSeq;
	}

	public String getBusinessId() {
		return businessId;
	}

	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getStoreOrderNo() {
		return storeOrderNo;
	}

	public void setStoreOrderNo(String storeOrderNo) {
		this.storeOrderNo = storeOrderNo;
	}

	public String getTradeTime() {
		return tradeTime;
	}

	public void setTradeTime(String tradeTime) {
		this.tradeTime = tradeTime;
	}

	public String getTradeCurr() {
		return tradeCurrency;
	}

	public void setTradeCurrency(String tradeCurrency) {
		this.tradeCurrency = tradeCurrency;
	}

	public String getTradeAmount() {
		return tradeAmount;
	}

	public void setTradeAmount(String tradeAmount) {
		this.tradeAmount = tradeAmount;
	}

	public String getPurposeType() {
		return purposeType;
	}

	public void setPurposeType(String purposeType) {
		this.purposeType = purposeType;
	}

	public String getBuyrDesc() {
		return buyrDesc;
	}

	public void setBuyrDesc(String buyrDesc) {
		this.buyrDesc = buyrDesc;
	}
}
