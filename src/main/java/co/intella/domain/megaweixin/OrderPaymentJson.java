package co.intella.domain.megaweixin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class OrderPaymentJson {
	@SerializedName("STORE_ID")
	@JsonProperty("STORE_ID")
	private String store_id;
	
	@SerializedName("STORE_ORDER_NO")
	@JsonProperty("STORE_ORDER_NO")
	private String store_order_no;
	
	@SerializedName("SYS_ORDER_ID")
	@JsonProperty("SYS_ORDER_ID")
	private String sys_order_id;
	
	@SerializedName("ORDER_TIME")
	@JsonProperty("ORDER_TIME")
	private String order_time;
	
	@SerializedName("ORDER_CURRENCY")
	@JsonProperty("ORDER_CURRENCY")
	private String order_currency;
	
	@SerializedName("ORDER_AMOUNT")
	@JsonProperty("ORDER_AMOUNT")
	private String order_amount;
	
	@SerializedName("ORDER_DESC")
	@JsonProperty("ORDER_DESC")
	private String order_desc;

	public String getStore_id() {
		return store_id;
	}

	public void setStore_id(String store_id) {
		this.store_id = store_id;
	}

	public String getStore_order_no() {
		return store_order_no;
	}

	public void setStore_order_no(String store_order_no) {
		this.store_order_no = store_order_no;
	}

	public String getSys_order_id() {
		return sys_order_id;
	}

	public void setSys_order_id(String sys_order_id) {
		this.sys_order_id = sys_order_id;
	}

	public String getOrder_time() {
		return order_time;
	}

	public void setOrder_time(String order_time) {
		this.order_time = order_time;
	}

	public String getOrder_currency() {
		return order_currency;
	}

	public void setOrder_currency(String order_currency) {
		this.order_currency = order_currency;
	}

	public String getOrder_amount() {
		return order_amount;
	}

	public void setOrder_amount(String order_amount) {
		this.order_amount = order_amount;
	}

	public String getOrder_desc() {
		return order_desc;
	}

	public void setOrder_desc(String order_desc) {
		this.order_desc = order_desc;
	}
}
