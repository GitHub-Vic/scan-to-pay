package co.intella.domain.megaweixin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class MegaWeiXinRequestData {

	@SerializedName("APIStorePw")
	@JsonProperty("APIStorePw")
	private String apiStorePw;

	@SerializedName("TradeSign")
	@JsonProperty("TradeSign")
	private String tradeSign;

	@SerializedName("PaymentType")
	@JsonProperty("PaymentType")
	private String paymentType;

	@SerializedName("DataValue")
	@JsonProperty("DataValue")
	private String dataValue;

	public String getApiStorePw() {
		return apiStorePw;
	}

	public void setApiStorePw(String apiStorePw) {
		this.apiStorePw = apiStorePw;
	}

	public String getTradeSign() {
		return tradeSign;
	}

	public void setTradeSign(String tradeSign) {
		this.tradeSign = tradeSign;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getDataValue() {
		return dataValue;
	}

	public void setDataValue(String dataValue) {
		this.dataValue = dataValue;
	}
	
}
