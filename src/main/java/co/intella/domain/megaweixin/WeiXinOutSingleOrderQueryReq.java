package co.intella.domain.megaweixin;

public class WeiXinOutSingleOrderQueryReq {
	
	private String device_info;
	private String store_order_no;
	private String sys_order_no;
	private String user_id;
	
    public WeiXinOutSingleOrderQueryReq() {
		super();
	}
	
    public WeiXinOutSingleOrderQueryReq(String device_info, String store_order_no, String sys_order_no) {
		super();
		this.device_info = device_info;
		this.store_order_no = store_order_no;
		this.sys_order_no = sys_order_no;
	}
    
	public String getDevice_info() {
		return device_info;
	}
	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}
	public String getStore_order_no() {
		return store_order_no;
	}
	public void setStore_order_no(String store_order_no) {
		this.store_order_no = store_order_no;
	}
	public String getSys_order_no() {
		return sys_order_no;
	}
	public void setSys_order_no(String sys_order_no) {
		this.sys_order_no = sys_order_no;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	
}
