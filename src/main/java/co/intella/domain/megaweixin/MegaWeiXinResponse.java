package co.intella.domain.megaweixin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class MegaWeiXinResponse {

  @SerializedName("Header")
  @JsonProperty("Header")
  private MegaWeiXinResponseHeader header;

  @SerializedName("Data")
  @JsonProperty("Data")
  private MegaWeiXinResponseData data;

  public MegaWeiXinResponseHeader getHeader() {
    return header;
  }

  public void setHeader(MegaWeiXinResponseHeader header) {
    this.header = header;
  }

  public MegaWeiXinResponseData getData() {
    return data;
  }

  public void setData(MegaWeiXinResponseData data) {
    this.data = data;
  }
}
