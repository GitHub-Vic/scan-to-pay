package co.intella.domain.megaweixin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class WeiXinRefundPayRes {
	
	  @SerializedName("SYS_REFUND_NO")
	  @JsonProperty("SYS_REFUND_NO")
	  private String sys_refund_no;

	public String getSys_refund_no() {
		return sys_refund_no;
	}

	public void setSys_refund_no(String sys_refund_no) {
		this.sys_refund_no = sys_refund_no;
	}
	  
}
