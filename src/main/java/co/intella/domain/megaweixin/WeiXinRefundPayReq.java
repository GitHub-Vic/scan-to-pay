package co.intella.domain.megaweixin;

public class WeiXinRefundPayReq {

	private String store_refund_no;
	private String store_order_no;
	private String sys_order_no;
	private String device_info;
	private int order_amount;
	private String currency;
	private int refund_amount;
	private String user_id;
	
	public WeiXinRefundPayReq() {
		super();
	}

	public WeiXinRefundPayReq(String store_refund_no, String store_order_no, String sys_order_no, String device_info,
			int order_amount, String currency, int refund_amount) {
		super();
		this.store_refund_no = store_refund_no;
		this.store_order_no = store_order_no;
		this.sys_order_no = sys_order_no;
		this.device_info = device_info;
		this.order_amount = order_amount;
		this.currency = "TWD";
		this.refund_amount = refund_amount;
	}
	
	public String getStore_refund_no() {
		return store_refund_no;
	}
	public void setStore_refund_no(String store_refund_no) {
		this.store_refund_no = store_refund_no;
	}
	public String getStore_order_no() {
		return store_order_no;
	}
	public void setStore_order_no(String store_order_no) {
		this.store_order_no = store_order_no;
	}
	public String getSys_order_no() {
		return sys_order_no;
	}
	public void setSys_order_no(String sys_order_no) {
		this.sys_order_no = sys_order_no;
	}
	public String getDevice_info() {
		return device_info;
	}
	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}
	public int getOrder_amount() {
		return order_amount;
	}
	public void setOrder_amount(int order_amount) {
		this.order_amount = order_amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public int getRefund_amount() {
		return refund_amount;
	}
	public void setRefund_amount(int refund_amount) {
		this.refund_amount = refund_amount;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	
}
