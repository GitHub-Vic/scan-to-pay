package co.intella.domain.megaweixin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class WeiXinOutSingleOrderQueryRes {

	  @SerializedName("STORE_ID")
	  @JsonProperty("STORE_ID")
	  private String store_id;
	  
	  @SerializedName("SYS_ORDER_NO")
	  @JsonProperty("SYS_ORDER_NO")
	  private String sys_order_no;
	  
	  @SerializedName("STORE_ORDER_NO")
	  @JsonProperty("STORE_ORDER_NO")
	  private String store_order_no;
	  
	  @SerializedName("WEIXIN_TRANSACTION_ID")
	  @JsonProperty("WEIXIN_TRANSACTION_ID")
	  private String weixin_transaction_id;
	  
	  @SerializedName("SYS_STATUS")
	  @JsonProperty("SYS_STATUS")
	  private String sys_status;
	  
	  @SerializedName("WEIXIN_TRADE_STATE")
	  @JsonProperty("WEIXIN_TRADE_STATE")
	  private String weixin_trade_state;
	  
	  @SerializedName("WEIXIN_TRADE_DESC")
	  @JsonProperty("WEIXIN_TRADE_DESC")
	  private String weixin_trade_desc;

	  @SerializedName("AMOUNT")
	  @JsonProperty("AMOUNT")
	  private String amount;
	  
	  @SerializedName("ORDER_CURRENCY")
	  @JsonProperty("ORDER_CURRENCY")
	  private String order_currency;
	  
	  @SerializedName("CUSTOMER_PAYMENT_LOCAL_CURRENCY")
	  @JsonProperty("CUSTOMER_PAYMENT_LOCAL_CURRENCY")
	  private String customer_payment_local_currency;
	  
	  @SerializedName("CUSTOMER_PAYMENT_LOCAL_AMOUNT")
	  @JsonProperty("CUSTOMER_PAYMENT_LOCAL_AMOUNT")
	  private String customer_payment_local_amount;
	  
	  @SerializedName("PURPOSE_TYPE")
	  @JsonProperty("PURPOSE_TYPE")
	  private String purpose_type;
	  
	  @SerializedName("STORE_FEE_RATE")
	  @JsonProperty("STORE_FEE_RATE")
	  private String store_fee_rate;
	  
	  @SerializedName("IS_REFUND")
	  @JsonProperty("IS_REFUND")
	  private String is_refund;
	  
	  @SerializedName("REFUNDED_AMOUNT")
	  @JsonProperty("REFUNDED_AMOUNT")
	  private String refund_amount;
	  
	  @SerializedName("DEVICE_INFO")
	  @JsonProperty("DEVICE_INFO")
	  private String device_info;
	  
	  @SerializedName("PRODUCT_DESC")
	  @JsonProperty("PRODUCT_DESC")
	  private String product_desc;
	  
	  @SerializedName("DETAIL")
	  @JsonProperty("DETAIL")
	  private String detail;
	  
	  @SerializedName("TRANSACTION_SIGN")
	  @JsonProperty("TRANSACTION_SIGN")
	  private String transaction_sign;
	  
	  @SerializedName("SERVICE_TYPE")
	  @JsonProperty("SERVICE_TYPE")
	  private String service_type;
	  
	  @SerializedName("USER_ID")
	  @JsonProperty("USER_ID")
	  private String user_id;
	  
	  @SerializedName("CREATE_TIME")
	  @JsonProperty("CREATE_TIME")
	  private String create_time;
	  
	  @SerializedName("CREATE_BY")
	  @JsonProperty("CREATE_BY")
	  private String create_by;

	public String getStore_id() {
		return store_id;
	}

	public void setStore_id(String store_id) {
		this.store_id = store_id;
	}

	public String getSys_order_no() {
		return sys_order_no;
	}

	public void setSys_order_no(String sys_order_no) {
		this.sys_order_no = sys_order_no;
	}

	public String getStore_order_no() {
		return store_order_no;
	}

	public void setStore_order_no(String store_order_no) {
		this.store_order_no = store_order_no;
	}

	public String getWeixin_transaction_id() {
		return weixin_transaction_id;
	}

	public void setWeixin_transaction_id(String weixin_transaction_id) {
		this.weixin_transaction_id = weixin_transaction_id;
	}

	public String getSys_status() {
		return sys_status;
	}

	public void setSys_status(String sys_status) {
		this.sys_status = sys_status;
	}

	public String getWeixin_trade_state() {
		return weixin_trade_state;
	}

	public void setWeixin_trade_state(String weixin_trade_state) {
		this.weixin_trade_state = weixin_trade_state;
	}

	public String getWeixin_trade_desc() {
		return weixin_trade_desc;
	}

	public void setWeixin_trade_desc(String weixin_trade_desc) {
		this.weixin_trade_desc = weixin_trade_desc;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getOrder_currency() {
		return order_currency;
	}

	public void setOrder_currency(String order_currency) {
		this.order_currency = order_currency;
	}

	public String getCustomer_payment_local_currency() {
		return customer_payment_local_currency;
	}

	public void setCustomer_payment_local_currency(String customer_payment_local_currency) {
		this.customer_payment_local_currency = customer_payment_local_currency;
	}

	public String getCustomer_payment_local_amount() {
		return customer_payment_local_amount;
	}

	public void setCustomer_payment_local_amount(String customer_payment_local_amount) {
		this.customer_payment_local_amount = customer_payment_local_amount;
	}

	public String getPurpose_type() {
		return purpose_type;
	}

	public void setPurpose_type(String purpose_type) {
		this.purpose_type = purpose_type;
	}

	public String getStore_fee_rate() {
		return store_fee_rate;
	}

	public void setStore_fee_rate(String store_fee_rate) {
		this.store_fee_rate = store_fee_rate;
	}

	public String getIs_refund() {
		return is_refund;
	}

	public void setIs_refund(String is_refund) {
		this.is_refund = is_refund;
	}

	public String getRefund_amount() {
		return refund_amount;
	}

	public void setRefund_amount(String refund_amount) {
		this.refund_amount = refund_amount;
	}

	public String getDevice_info() {
		return device_info;
	}

	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}

	public String getProduct_desc() {
		return product_desc;
	}

	public void setProduct_desc(String product_desc) {
		this.product_desc = product_desc;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getTransaction_sign() {
		return transaction_sign;
	}

	public void setTransaction_sign(String transaction_sign) {
		this.transaction_sign = transaction_sign;
	}

	public String getService_type() {
		return service_type;
	}

	public void setService_type(String service_type) {
		this.service_type = service_type;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getCreate_time() {
		return create_time;
	}

	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}

	public String getCreate_by() {
		return create_by;
	}

	public void setCreate_by(String create_by) {
		this.create_by = create_by;
	}

}
