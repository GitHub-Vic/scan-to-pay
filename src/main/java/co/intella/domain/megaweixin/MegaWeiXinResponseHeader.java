package co.intella.domain.megaweixin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class MegaWeiXinResponseHeader {

	@SerializedName("StatusCode")
	@JsonProperty("StatusCode")
	private String statusCode;

	@SerializedName("StatusDesc")
	@JsonProperty("StatusDesc")
	private String statusDesc;

	@SerializedName("ResponseTime")
	@JsonProperty("ResponseTime")
	private String responseTime;

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public String getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(String responseTime) {
		this.responseTime = responseTime;
	}

}
