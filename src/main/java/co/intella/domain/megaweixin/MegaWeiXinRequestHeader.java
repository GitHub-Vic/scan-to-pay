package co.intella.domain.megaweixin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class MegaWeiXinRequestHeader {

	public MegaWeiXinRequestHeader(String version, String apiStoreId, String terminalIp, String serviceName, String hashValue) {
		super();
		this.version = version;
		this.apiStoreId = apiStoreId;
		this.terminalIp = terminalIp;
		this.serviceName = serviceName;
		this.hashValue = hashValue;
	}

	@SerializedName("Version")
	@JsonProperty("Version")
	private String version;

	@SerializedName("ApiStoreId")
	@JsonProperty("ApiStoreId")
	private String apiStoreId;

	@SerializedName("TerminalIp")
	@JsonProperty("TerminalIp")
	private String terminalIp;

	@SerializedName("ServiceName")
	@JsonProperty("ServiceName")
	private String serviceName;

	@SerializedName("HashValue")
	@JsonProperty("HashValue")
	private String hashValue;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getApiStoreId() {
		return apiStoreId;
	}

	public void setApiStoreId(String apiStoreId) {
		this.apiStoreId = apiStoreId;
	}

	public String getTerminalIp() {
		return terminalIp;
	}

	public void setTerminalIp(String terminalIp) {
		this.terminalIp = terminalIp;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getHashValue() {
		return hashValue;
	}

	public void setHashValue(String hashValue) {
		this.hashValue = hashValue;
	}
	
}
