package co.intella.domain.megaweixin;

public class GoodsDetail {
	
	private String goods_name;
	private int quantity;
	
	public GoodsDetail(String goods_name, int quantity) {
		super();
		this.goods_name = goods_name;
		this.quantity = quantity;
	}
	
	public String getGoods_name() {
		return goods_name;
	}
	public void setGoods_name(String goods_name) {
		this.goods_name = goods_name;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
}
