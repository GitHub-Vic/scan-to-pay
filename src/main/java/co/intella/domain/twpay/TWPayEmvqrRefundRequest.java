package co.intella.domain.twpay;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "QrpEmvqrRefundReq", namespace = "http://www.focas.fisc.com.tw/QRP/emvqrRefund")
@XmlAccessorType(XmlAccessType.FIELD)
public class TWPayEmvqrRefundRequest {

	private String mti;
	private String cardNumber;
	private String processingCode;
	private String amt;
	private String systemDateTime;
	private String traceNumber;
	private String localTime;
	private String localDate;
	private String posEntryMode;
	private String acqBank;
	private String srrn;
	private String authCode;
	private String terminalId;
	private String merchantId;
	private String merchantName;
	private String otherInfo;
	private String txnCurrencyCode;
	private String orgTxnData;
	private String hostId;
	private String mcc;

	public String getMti() {
		return mti;
	}

	public void setMti(String mti) {
		this.mti = mti;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getProcessingCode() {
		return processingCode;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getSystemDateTime() {
		return systemDateTime;
	}

	public void setSystemDateTime(String systemDateTime) {
		this.systemDateTime = systemDateTime;
	}

	public String getTraceNumber() {
		return traceNumber;
	}

	public void setTraceNumber(String traceNumber) {
		this.traceNumber = traceNumber;
	}

	public String getLocalTime() {
		return localTime;
	}

	public void setLocalTime(String localTime) {
		this.localTime = localTime;
	}

	public String getLocalDate() {
		return localDate;
	}

	public void setLocalDate(String localDate) {
		this.localDate = localDate;
	}

	public String getPosEntryMode() {
		return posEntryMode;
	}

	public void setPosEntryMode(String posEntryMode) {
		this.posEntryMode = posEntryMode;
	}

	public String getAcqBank() {
		return acqBank;
	}

	public void setAcqBank(String acqBank) {
		this.acqBank = acqBank;
	}

	public String getSrrn() {
		return srrn;
	}

	public void setSrrn(String srrn) {
		this.srrn = srrn;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getOtherInfo() {
		return otherInfo;
	}

	public void setOtherInfo(String otherInfo) {
		this.otherInfo = otherInfo;
	}

	public String getTxnCurrencyCode() {
		return txnCurrencyCode;
	}

	public void setTxnCurrencyCode(String txnCurrencyCode) {
		this.txnCurrencyCode = txnCurrencyCode;
	}

	public String getOrgTxnData() {
		return orgTxnData;
	}

	public void setOrgTxnData(String orgTxnData) {
		this.orgTxnData = orgTxnData;
	}

	public String getHostId() {
		return hostId;
	}

	public void setHostId(String hostId) {
		this.hostId = hostId;
	}

	public String getMcc() {
		return mcc;
	}

	public void setMcc(String mcc) {
		this.mcc = mcc;
	}
}
