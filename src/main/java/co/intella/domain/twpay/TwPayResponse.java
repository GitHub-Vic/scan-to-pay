package co.intella.domain.twpay;

public class TwPayResponse {
	
    private String txnStatus;
    private String hostId;
    private String respCode;
    private String notifyExpiryDate;
    private String txnType;
    private String srrn;
    private String txnDateLocal;
    private String txnTimeLocal;
    private String qrPaymentInfo;
    private String txnToken;
    private String redirectUrl;
    private String verifyCode;
    private String uriSchema;
    private String paymentTool;
    private String orderNbr;
    private String transferorBank;
    private String cardNbrOrAccountNbr;
    private String txnExcDateLocal;
    private String txnExcTimeLocal;
    
	public String getTxnStatus() {
		return txnStatus;
	}
	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
	}
	public String getHostId() {
		return hostId;
	}
	public void setHostId(String hostId) {
		this.hostId = hostId;
	}
	public String getRespCode() {
		return respCode;
	}
	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}
	public String getTxnType() {
		return txnType;
	}
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	public String getSrrn() {
		return srrn;
	}
	public void setSrrn(String srrn) {
		this.srrn = srrn;
	}
	public String getTxnDateLocal() {
		return txnDateLocal;
	}
	public void setTxnDateLocal(String txnDateLocal) {
		this.txnDateLocal = txnDateLocal;
	}
	public String getTxnTimeLocal() {
		return txnTimeLocal;
	}
	public void setTxnTimeLocal(String txnTimeLocal) {
		this.txnTimeLocal = txnTimeLocal;
	}
	public String getTxnToken() {
		return txnToken;
	}
	public void setTxnToken(String txnToken) {
		this.txnToken = txnToken;
	}
	public String getRedirectUrl() {
		return redirectUrl;
	}
	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
	public String getVerifyCode() {
		return verifyCode;
	}
	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}
	public String getUriSchema() {
		return uriSchema;
	}
	public void setUriSchema(String uriSchema) {
		this.uriSchema = uriSchema;
	}
	public String getNotifyExpiryDate() {
		return notifyExpiryDate;
	}
	public void setNotifyExpiryDate(String notifyExpiryDate) {
		this.notifyExpiryDate = notifyExpiryDate;
	}
	public String getQrPaymentInfo() {
		return qrPaymentInfo;
	}
	public void setQrPaymentInfo(String qrPaymentInfo) {
		this.qrPaymentInfo = qrPaymentInfo;
	}
	public String getPaymentTool() {
		return paymentTool;
	}
	public void setPaymentTool(String paymentTool) {
		this.paymentTool = paymentTool;
	}
	public String getOrderNbr() {
		return orderNbr;
	}
	public void setOrderNbr(String orderNbr) {
		this.orderNbr = orderNbr;
	}
	public String getTransferorBank() {
		return transferorBank;
	}
	public void setTransferorBank(String transferorBank) {
		this.transferorBank = transferorBank;
	}
	public String getCardNbrOrAccountNbr() {
		return cardNbrOrAccountNbr;
	}
	public void setCardNbrOrAccountNbr(String cardNbrOrAccountNbr) {
		this.cardNbrOrAccountNbr = cardNbrOrAccountNbr;
	}
	public String getTxnExcDateLocal() {
		return txnExcDateLocal;
	}
	public void setTxnExcDateLocal(String txnExcDateLocal) {
		this.txnExcDateLocal = txnExcDateLocal;
	}
	public String getTxnExcTimeLocal() {
		return txnExcTimeLocal;
	}
	public void setTxnExcTimeLocal(String txnExcTimeLocal) {
		this.txnExcTimeLocal = txnExcTimeLocal;
	}

}
