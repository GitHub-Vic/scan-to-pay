package co.intella.domain.twpay;


import javax.xml.bind.annotation.*;

@XmlRootElement(name = "QrpWalletRefundReq", namespace = "http://www.focas.fisc.com.tw/QRP/walletRefund")
@XmlAccessorType(XmlAccessType.FIELD)
public class WalletRefundRequest  {

    private String mti;
    private String cardNumber;
    private String processingCode;
    private String amt;
    private String systemDateTime;
    private String traceNumber;
    private String localTime;
    private String localDate;
    private String mcc;
    private String countryCode;
    private String posEntryMode;
    private String posConditionCode;
    private String acqBank;
    private String terminalId;
    private String merchantId;
    private String merchantName;
    private String orderNumber;
    private String txnCurrencyCode;
    private OrgTxnData orgTxnData;
    private String hostId;

    public String getMti() {
        return mti;
    }

    public void setMti(String mti) {
        this.mti = mti;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getProcessingCode() {
        return processingCode;
    }

    public void setProcessingCode(String processingCode) {
        this.processingCode = processingCode;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getSystemDateTime() {
        return systemDateTime;
    }

    public void setSystemDateTime(String systemDateTime) {
        this.systemDateTime = systemDateTime;
    }

    public String getTraceNumber() {
        return traceNumber;
    }

    public void setTraceNumber(String traceNumber) {
        this.traceNumber = traceNumber;
    }

    public String getLocalTime() {
        return localTime;
    }

    public void setLocalTime(String localTime) {
        this.localTime = localTime;
    }

    public String getLocalDate() {
        return localDate;
    }

    public void setLocalDate(String localDate) {
        this.localDate = localDate;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPosEntryMode() {
        return posEntryMode;
    }

    public void setPosEntryMode(String posEntryMode) {
        this.posEntryMode = posEntryMode;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }

    public void setPosConditionCode(String posConditionCode) {
        this.posConditionCode = posConditionCode;
    }

    public String getAcqBank() {
        return acqBank;
    }

    public void setAcqBank(String acqBank) {
        this.acqBank = acqBank;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getTxnCurrencyCode() {
        return txnCurrencyCode;
    }

    public void setTxnCurrencyCode(String txnCurrencyCode) {
        this.txnCurrencyCode = txnCurrencyCode;
    }

    public OrgTxnData getOrgTxnData() {
        return orgTxnData;
    }

    public void setOrgTxnData(OrgTxnData orgTxnData) {
        this.orgTxnData = orgTxnData;
    }

    public String getHostId() {
        return hostId;
    }

    public void setHostId(String hostId) {
        this.hostId = hostId;
    }
}

