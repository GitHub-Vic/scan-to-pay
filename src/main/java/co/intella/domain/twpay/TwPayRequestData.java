package co.intella.domain.twpay;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.google.gson.annotations.SerializedName;

/**
 * @author Alex
 */
@JsonInclude(Include.NON_NULL)
public class TwPayRequestData {

    private String mti;

    private String cardNumber;

    private String processingCode;

    @SerializedName("TotalFee")
    private String amt;

    private String traceNumber;

    private String localTime;
    private String localDate;

    private String countryCode;

    private String posEntryMode;

    private String posConditionCode;

    private String acqBank;

    private String srrn;

    private String responseCode;

    private String terminalId;

    private String merchantId;

    @SerializedName("StoreOrderNo")
    private String orderNumber;
    
    private String orderNbr;
    
    private String otherInfo;

    private String txnCurrencyCode;

    private String chipData;

    private String verifyCode;

    private String hostId;

    @SerializedName("AuthCode")
    private String authCode;

    private String systemDateTime;
    
    private String txnDateLocal;
    
    private String txnTimeLocal;
    
    private String txnType;
    
    private String deviceType; 
    
    private QRPayment qrPayment;
    
    private String txnStatus;
    
    private String redirectUrl;

    private String qrPaymentInfo;

    public String getMti() {
        return mti;
    }

    public void setMti(String mti) {
        this.mti = mti;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getProcessingCode() {
        return processingCode;
    }

    public void setProcessingCode(String processingCode) {
        this.processingCode = processingCode;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getTraceNumber() {
        return traceNumber;
    }

    public void setTraceNumber(String traceNumber) {
        this.traceNumber = traceNumber;
    }

    public String getLocalTime() {
        return localTime;
    }

    public void setLocalTime(String localTime) {
        this.localTime = localTime;
    }

    public String getLocalDate() {
        return localDate;
    }

    public void setLocalDate(String localDate) {
        this.localDate = localDate;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPosEntryMode() {
        return posEntryMode;
    }

    public void setPosEntryMode(String posEntryMode) {
        this.posEntryMode = posEntryMode;
    }

    public String getPosConditionCode() {
        return posConditionCode;
    }

    public void setPosConditionCode(String posConditionCode) {
        this.posConditionCode = posConditionCode;
    }

    public String getAcqBank() {
        return acqBank;
    }

    public void setAcqBank(String acqBank) {
        this.acqBank = acqBank;
    }

    public String getSrrn() {
        return srrn;
    }

    public void setSrrn(String srrn) {
        this.srrn = srrn;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
    }

    public String getTxnCurrencyCode() {
        return txnCurrencyCode;
    }

    public void setTxnCurrencyCode(String txnCurrencyCode) {
        this.txnCurrencyCode = txnCurrencyCode;
    }

    public String getChipData() {
        return chipData;
    }

    public void setChipData(String chipData) {
        this.chipData = chipData;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public String getHostId() {
        return hostId;
    }

    public void setHostId(String hostId) {
        this.hostId = hostId;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getSystemDateTime() {
        return systemDateTime;
    }

    public void setSystemDateTime(String systemDateTime) {
        this.systemDateTime = systemDateTime;
    }

	public String getTxnDateLocal() {
		return txnDateLocal;
	}

	public void setTxnDateLocal(String txnDateLocal) {
		this.txnDateLocal = txnDateLocal;
	}

	public String getTxnTimeLocal() {
		return txnTimeLocal;
	}

	public void setTxnTimeLocal(String txnTimeLocal) {
		this.txnTimeLocal = txnTimeLocal;
	}

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public QRPayment getQrPayment() {
		return qrPayment;
	}

	public void setQrPayment(QRPayment qrPayment) {
		this.qrPayment = qrPayment;
	}

	public String getTxnStatus() {
		return txnStatus;
	}

	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
	}

	public String getOrderNbr() {
		return orderNbr;
	}

	public void setOrderNbr(String orderNbr) {
		this.orderNbr = orderNbr;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

    public String getQrPaymentInfo() {
        return qrPaymentInfo;
    }

    public void setQrPaymentInfo(String qrPaymentInfo) {
        this.qrPaymentInfo = qrPaymentInfo;
    }

    @Override
    public String toString() {
        return "TwPayRequestData{" +
                "mti='" + mti + '\'' +
                ", cardNumber='" + cardNumber + '\'' +
                ", processingCode='" + processingCode + '\'' +
                ", amt='" + amt + '\'' +
                ", traceNumber='" + traceNumber + '\'' +
                ", localTime='" + localTime + '\'' +
                ", localDate='" + localDate + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", posEntryMode='" + posEntryMode + '\'' +
                ", posConditionCode='" + posConditionCode + '\'' +
                ", acqBank='" + acqBank + '\'' +
                ", srrn='" + srrn + '\'' +
                ", responseCode='" + responseCode + '\'' +
                ", terminalId='" + terminalId + '\'' +
                ", merchantId='" + merchantId + '\'' +
                ", orderNumber='" + orderNumber + '\'' +
                ", orderNbr='" + orderNbr + '\'' +
                ", otherInfo='" + otherInfo + '\'' +
                ", txnCurrencyCode='" + txnCurrencyCode + '\'' +
                ", chipData='" + chipData + '\'' +
                ", verifyCode='" + verifyCode + '\'' +
                ", hostId='" + hostId + '\'' +
                ", authCode='" + authCode + '\'' +
                ", systemDateTime='" + systemDateTime + '\'' +
                ", txnDateLocal='" + txnDateLocal + '\'' +
                ", txnTimeLocal='" + txnTimeLocal + '\'' +
                ", txnType='" + txnType + '\'' +
                ", deviceType='" + deviceType + '\'' +
                ", qrPayment=" + qrPayment +
                ", txnStatus='" + txnStatus + '\'' +
                ", redirectUrl='" + redirectUrl + '\'' +
                ", qrPaymentInfo='" + qrPaymentInfo + '\'' +
                '}';
    }
}
