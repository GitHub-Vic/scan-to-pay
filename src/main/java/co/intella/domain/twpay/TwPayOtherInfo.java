package co.intella.domain.twpay;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Alex
 */

public class TwPayOtherInfo {
    @JsonProperty("tag01")
    private String tag01;

    @JsonProperty("tag02")
    private String tag02;

    @JsonProperty("tag03")
    private String tag03;

    @JsonProperty("tag04")
    private String tag04;

    @JsonProperty("tag05")
    private String tag05;

    @JsonProperty("tag06")
    private String tag06;

    @JsonProperty("tag07")
    private String tag07;

    @JsonProperty("tag08")
    private String tag08;

    @JsonProperty("tag09")
    private String tag09;

    @JsonProperty("tag10")
    private String tag10;

    @JsonProperty("tag11")
    private String tag11;

    @JsonProperty("tag12")
    private String tag12;

    @JsonProperty("tag13")
    private String tag13;

    @JsonProperty("tag14")
    private String tag14;

    @JsonProperty("tag15")
    private String tag15;

    @JsonProperty("tag16")
    private String tag16;

    @JsonProperty("tag17")
    private String tag17;

    private String tag18;
    private String tag19;
    private String tag20;
    private String tag21;
    private String tag22;
    private String tag23;
    private String tag30;
    private String tag31;
    private String tag97;
    private String tag98;
    private String tag99;


    @JsonProperty("tag80")
    private String tag80;

    @JsonProperty("tag81")
    private String tag81;

    public String getTag01() {
        return tag01;
    }

    public void setTag01(String tag01) {
        this.tag01 = tag01;
    }

    public String getTag02() {
        return tag02;
    }

    public void setTag02(String tag02) {
        this.tag02 = tag02;
    }

    public String getTag03() {
        return tag03;
    }

    public void setTag03(String tag03) {
        this.tag03 = tag03;
    }

    public String getTag04() {
        return tag04;
    }

    public void setTag04(String tag04) {
        this.tag04 = tag04;
    }

    public String getTag05() {
        return tag05;
    }

    public void setTag05(String tag05) {
        this.tag05 = tag05;
    }

    public String getTag06() {
        return tag06;
    }

    public void setTag06(String tag06) {
        this.tag06 = tag06;
    }

    public String getTag07() {
        return tag07;
    }

    public void setTag07(String tag07) {
        this.tag07 = tag07;
    }

    public String getTag08() {
        return tag08;
    }

    public void setTag08(String tag08) {
        this.tag08 = tag08;
    }

    public String getTag09() {
        return tag09;
    }

    public void setTag09(String tag09) {
        this.tag09 = tag09;
    }

    public String getTag10() {
        return tag10;
    }

    public void setTag10(String tag10) {
        this.tag10 = tag10;
    }

    public String getTag11() {
        return tag11;
    }

    public void setTag11(String tag11) {
        this.tag11 = tag11;
    }

    public String getTag12() {
        return tag12;
    }

    public void setTag12(String tag12) {
        this.tag12 = tag12;
    }

    public String getTag13() {
        return tag13;
    }

    public void setTag13(String tag13) {
        this.tag13 = tag13;
    }

    public String getTag14() {
        return tag14;
    }

    public void setTag14(String tag14) {
        this.tag14 = tag14;
    }

    public String getTag15() {
        return tag15;
    }

    public void setTag15(String tag15) {
        this.tag15 = tag15;
    }

    public String getTag16() {
        return tag16;
    }

    public void setTag16(String tag16) {
        this.tag16 = tag16;
    }

    public String getTag17() {
        return tag17;
    }

    public void setTag17(String tag17) {
        this.tag17 = tag17;
    }

    public String getTag18() {
        return tag18;
    }

    public void setTag18(String tag18) {
        this.tag18 = tag18;
    }

    public String getTag19() {
        return tag19;
    }

    public void setTag19(String tag19) {
        this.tag19 = tag19;
    }

    public String getTag20() {
        return tag20;
    }

    public void setTag20(String tag20) {
        this.tag20 = tag20;
    }

    public String getTag21() {
        return tag21;
    }

    public void setTag21(String tag21) {
        this.tag21 = tag21;
    }

    public String getTag22() {
        return tag22;
    }

    public void setTag22(String tag22) {
        this.tag22 = tag22;
    }

    public String getTag23() {
        return tag23;
    }

    public void setTag23(String tag23) {
        this.tag23 = tag23;
    }

    public String getTag30() {
        return tag30;
    }

    public void setTag30(String tag30) {
        this.tag30 = tag30;
    }

    public String getTag31() {
        return tag31;
    }

    public void setTag31(String tag31) {
        this.tag31 = tag31;
    }

    public String getTag97() {
        return tag97;
    }

    public void setTag97(String tag97) {
        this.tag97 = tag97;
    }

    public String getTag98() {
        return tag98;
    }

    public void setTag98(String tag98) {
        this.tag98 = tag98;
    }

    public String getTag99() {
        return tag99;
    }

    public void setTag99(String tag99) {
        this.tag99 = tag99;
    }

    public String getTag80() {
        return tag80;
    }

    public void setTag80(String tag80) {
        this.tag80 = tag80;
    }

    public String getTag81() {
        return tag81;
    }

    public void setTag81(String tag81) {
        this.tag81 = tag81;
    }
}
