package co.intella.domain.twpay;

public class OrgTxnData {
	
	private String mti;
	private String processingCode;
	private String amt;
	private String systemDateTime;
	private String srrn;
	private String localTime;
	private String localDate;
	private String traceNumber;

	public String getMti() {
		return mti;
	}
	public void setMti(String mti) {
		this.mti = mti;
	}
	public String getProcessingCode() {
		return processingCode;
	}
	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String amt) {
		this.amt = amt;
	}
	public String getSystemDateTime() {
		return systemDateTime;
	}
	public void setSystemDateTime(String systemDateTime) {
		this.systemDateTime = systemDateTime;
	}
	public String getSrrn() {
		return srrn;
	}
	public void setSrrn(String srrn) {
		this.srrn = srrn;
	}
	public String getLocalTime() {
		return localTime;
	}
	public void setLocalTime(String localTime) {
		this.localTime = localTime;
	}
	public String getLocalDate() {
		return localDate;
	}
	public void setLocalDate(String localDate) {
		this.localDate = localDate;
	}
	public String getTraceNumber() {
		return traceNumber;
	}
	public void setTraceNumber(String traceNumber) {
		this.traceNumber = traceNumber;
	}
    
}
