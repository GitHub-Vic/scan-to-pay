package co.intella.domain.twpay;

public class WebToAppNotify {
    private String acqBank;
    private String terminalId;
    private String merchantId;
    private String orderNumber;
    private String cardPlan;
    private String responseCode;
    private String verifyCode;

    public String getAcqBank() {
        return acqBank;
    }

    public void setAcqBank(String acqBank) {
        this.acqBank = acqBank;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getCardPlan() {
        return cardPlan;
    }

    public void setCardPlan(String cardPlan) {
        this.cardPlan = cardPlan;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    @Override
    public String toString() {
        return "WebToAppNotify{" +
                "acqBank='" + acqBank + '\'' +
                ", terminalId='" + terminalId + '\'' +
                ", merchantId='" + merchantId + '\'' +
                ", orderNumber='" + orderNumber + '\'' +
                ", cardPlan='" + cardPlan + '\'' +
                ", responseCode='" + responseCode + '\'' +
                ", verifyCode='" + verifyCode + '\'' +
                '}';
    }
}
