package co.intella.domain.twpay;

public class BarCodeData {

    private String time;
    private String date;
    private String bankCode;
    private byte[] cardRemerk;
    private String cardNum;
    private String TSN;
    private String keep;
    private byte[] TAC;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public String getTSN() {
        return TSN;
    }

    public void setTSN(String TSN) {
        this.TSN = TSN;
    }

    public byte[] getCardRemerk() {
        return cardRemerk;
    }

    public void setCardRemerk(byte[] cardRemerk) {
        this.cardRemerk = cardRemerk;
    }

    public byte[] getTAC() {
        return TAC;
    }

    public void setTAC(byte[] TAC) {
        this.TAC = TAC;
    }

    public String getKeep() {
        return keep;
    }

    public void setKeep(String keep) {
        this.keep = keep;
    }
}
