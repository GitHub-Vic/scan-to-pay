package co.intella.domain.twpay;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class QRPayment {
	
	private String txnAmt;
	private String orderNbr;
	private String secureCode;
	private String txnCurrencyCode;
	private String acqInfo;
	private String qrExpirydate;
	private String interactionInfo1;
	private String interactionInfo2;
	private String interactionInfo3;
	private String payerInfo;
	private String txnType;
	private String name;
	private String acquirerCode; // 收單行代碼
	private String merchantId;
	private String terminalId;
	
	public String getTxnAmt() {
		return txnAmt;
	}
	public void setTxnAmt(String txnAmt) {
		this.txnAmt = txnAmt;
	}
	public String getOrderNbr() {
		return orderNbr;
	}
	public void setOrderNbr(String orderNbr) {
		this.orderNbr = orderNbr;
	}
	public String getSecureCode() {
		return secureCode;
	}
	public void setSecureCode(String secureCode) {
		this.secureCode = secureCode;
	}
	public String getTxnCurrencyCode() {
		return txnCurrencyCode;
	}
	public void setTxnCurrencyCode(String txnCurrencyCode) {
		this.txnCurrencyCode = txnCurrencyCode;
	}
	public String getAcqInfo() {
		return acqInfo;
	}
	public void setAcqInfo(String acqInfo) {
		this.acqInfo = acqInfo;
	}
	public String getQrExpirydate() {
		return qrExpirydate;
	}
	public void setQrExpirydate(String qrExpirydate) {
		this.qrExpirydate = qrExpirydate;
	}
	public String getInteractionInfo1() {
		return interactionInfo1;
	}
	public void setInteractionInfo1(String interactionInfo1) {
		this.interactionInfo1 = interactionInfo1;
	}
	public String getInteractionInfo2() {
		return interactionInfo2;
	}
	public void setInteractionInfo2(String interactionInfo2) {
		this.interactionInfo2 = interactionInfo2;
	}
	public String getInteractionInfo3() {
		return interactionInfo3;
	}
	public void setInteractionInfo3(String interactionInfo3) {
		this.interactionInfo3 = interactionInfo3;
	}
	public String getPayerInfo() {
		return payerInfo;
	}
	public void setPayerInfo(String payerInfo) {
		this.payerInfo = payerInfo;
	}
	public String getTxnType() {
		return txnType;
	}
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAcquirerCode() {
		return acquirerCode;
	}
	public void setAcquirerCode(String acquirerCode) {
		this.acquirerCode = acquirerCode;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getTerminalId() {
		return terminalId;
	}
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}
	
}
