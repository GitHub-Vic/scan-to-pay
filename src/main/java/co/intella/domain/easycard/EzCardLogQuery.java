package co.intella.domain.easycard;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class EzCardLogQuery extends EzCardBasicRequest {

    @SerializedName("QueryTime")
    private String queryTime;

    @SerializedName("QueryDate")
    private String queryDate;

    public EzCardLogQuery() {
        this.setServiceType("DumpSingleICERLog");
    }

    public String getQueryTime() {
        return queryTime;
    }

    public void setQueryTime(String queryTime) {
        this.queryTime = queryTime;
    }

    public String getQueryDate() {
        return queryDate;
    }

    public void setQueryDate(String queryDate) {
        this.queryDate = queryDate;
    }
}
