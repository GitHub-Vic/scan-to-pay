package co.intella.domain.easycard;

/**
 * @author Miles
 */
public class EzCardReserve extends EzCardPayment {

    public EzCardReserve() {
        this.setServiceType("CashTopUp");
    }
}
