package co.intella.domain.easycard;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class EzCardBasicRequest {

    @SerializedName("ServiceType")
    private String serviceType;

    @SerializedName("TerminalID")
    private String terminalId;

    @SerializedName("DeviceID")
    private String deviceId;

    @SerializedName("TerminalTXNNumber")
    private String terminalTXNNumber;

    @SerializedName("HostSerialNumber")
    private String hostSerialNumber;

    @SerializedName("Time")
    private String time;

    @SerializedName("Date")
    private String date;

    @SerializedName("AESKey")
    private String aesKey;

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getTerminalTXNNumber() {
        return terminalTXNNumber;
    }

    public void setTerminalTXNNumber(String terminalTXNNumber) {
        this.terminalTXNNumber = terminalTXNNumber;
    }

    public String getHostSerialNumber() {
        return hostSerialNumber;
    }

    public void setHostSerialNumber(String hostSerialNumber) {
        this.hostSerialNumber = hostSerialNumber;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAesKey() {
        return aesKey;
    }

    public void setAesKey(String aesKey) {
        this.aesKey = aesKey;
    }
}
