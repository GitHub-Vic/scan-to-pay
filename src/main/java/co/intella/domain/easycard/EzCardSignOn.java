package co.intella.domain.easycard;

/**
 * @author Miles
 */
public class EzCardSignOn extends EzCardBasicRequest {

    public EzCardSignOn() {
        this.setServiceType("SignON");
    }

}
