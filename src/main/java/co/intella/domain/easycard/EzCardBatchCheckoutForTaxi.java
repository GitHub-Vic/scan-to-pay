package co.intella.domain.easycard;

import com.google.gson.annotations.SerializedName;

public class EzCardBatchCheckoutForTaxi extends EzCardBatchCheckout {

    @SerializedName("TotalPoint")
    private String totalPoint;

    @SerializedName("TotalPointCount")
    private String totalPointCount;


    public EzCardBatchCheckoutForTaxi() {
        this.setServiceType("Settlement");
    }

    public String getTotalPoint() {
        return totalPoint;
    }

    public void setTotalPoint(String totalPoint) {
        this.totalPoint = totalPoint;
    }

    public String getTotalPointCount() {
        return totalPointCount;
    }

    public void setTotalPointCount(String totalPointCount) {
        this.totalPointCount = totalPointCount;
    }

	@Override
	public String toString() {
		return "EzCardBatchCheckoutForTaxi [totalPoint=" + totalPoint + ", totalPointCount=" + totalPointCount + "]";
	}
    
    
}
