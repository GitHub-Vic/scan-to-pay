package co.intella.domain.easycard;

/**
 * @author Miles
 */
public class EzCardRefund extends EzCardPayment {

    public EzCardRefund() {
        this.setServiceType("Refund");
    }

    private String refundKey;

    public String getRefundKey() {
        return refundKey;
    }

    public void setRefundKey(String refundKey) {
        this.refundKey = refundKey;
    }
}
