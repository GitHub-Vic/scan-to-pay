package co.intella.domain.easycard;

import co.intella.domain.easycard.raw.EzCardTransaction;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Miles
 */
public class EzCardTradeQueryResponse extends EzCardBasicResponse {

    @SerializedName("TerminalTXNNumber")
    private String terminalTXNNumber;

    @SerializedName("HostSerialNumber")
    private String hostSerialNumber;

    @SerializedName("DeviceID")
    private String deviceId;

    @SerializedName("NewDeviceID")
    private String newDeviceId;

    @SerializedName("CardTXNData")
    private List<EzCardTransaction> cardTXNData;

    public String getTerminalTXNNumber() {
        return terminalTXNNumber;
    }

    public void setTerminalTXNNumber(String terminalTXNNumber) {
        this.terminalTXNNumber = terminalTXNNumber;
    }

    public String getHostSerialNumber() {
        return hostSerialNumber;
    }

    public void setHostSerialNumber(String hostSerialNumber) {
        this.hostSerialNumber = hostSerialNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getNewDeviceId() {
        return newDeviceId;
    }

    public void setNewDeviceId(String newDeviceId) {
        this.newDeviceId = newDeviceId;
    }

    public List<EzCardTransaction> getCardTXNData() {
        return cardTXNData;
    }

    public void setCardTXNData(List<EzCardTransaction> cardTXNData) {
        this.cardTXNData = cardTXNData;
    }
}
