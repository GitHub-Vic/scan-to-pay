package co.intella.domain.easycard;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class EzCardErrorResponse extends EzCardBasicResponse {

    @SerializedName("Method")
    private String method;

    @SerializedName("ServiceType")
    private String serviceType;

    @SerializedName("MerchantId")
    private String merchantId;

    @SerializedName("DeviceId")
    private String deviceId;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
