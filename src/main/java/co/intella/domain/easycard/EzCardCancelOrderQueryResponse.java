package co.intella.domain.easycard;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class EzCardCancelOrderQueryResponse extends EzCardPaymentResponse {

    @SerializedName("ActionType")
    private String actionType;

    @SerializedName("TxnDeviceId")
    private String txnDeviceId;

    @SerializedName("TxnOrderId")
    private String tradeOrderId;

    @SerializedName("CreateDate")
    private String createDate;

    @SerializedName("UserId")
    private String userId;

    @SerializedName("OrderStatus")
    private String orderStatus;

    @SerializedName("RefundStatus")
    private String refundStatus;

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getTxnDeviceId() {
        return txnDeviceId;
    }

    public void setTxnDeviceId(String txnDeviceId) {
        this.txnDeviceId = txnDeviceId;
    }

    public String getTradeOrderId() {
        return tradeOrderId;
    }

    public void setTradeOrderId(String tradeOrderId) {
        this.tradeOrderId = tradeOrderId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(String refundStatus) {
        this.refundStatus = refundStatus;
    }
}
