package co.intella.domain.easycard;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class EzCardCancel extends EzCardPayment {

    @SerializedName("OriginalDongleDeviceID")
    private String originalDongleDeviceID;

    @SerializedName("OriginalRRN")
    private String originalRRN;

    @SerializedName("OriginalTXNDate")
    private String originalTXNDate;

    private String refundKey;

    public EzCardCancel() {
        this.setServiceType("Cancel");
    }

    public String getOriginalDongleDeviceID() {
        return originalDongleDeviceID;
    }

    public void setOriginalDongleDeviceID(String originalDongleDeviceID) {
        this.originalDongleDeviceID = originalDongleDeviceID;
    }

    public String getOriginalRRN() {
        return originalRRN;
    }

    public void setOriginalRRN(String originalRRN) {
        this.originalRRN = originalRRN;
    }

    public String getOriginalTXNDate() {
        return originalTXNDate;
    }

    public void setOriginalTXNDate(String originalTXNDate) {
        this.originalTXNDate = originalTXNDate;
    }

    public String getRefundKey() {
        return refundKey;
    }

    public void setRefundKey(String refundKey) {
        this.refundKey = refundKey;
    }
}
