package co.intella.domain.easycard;

import co.intella.domain.contratStore.EzcTradeDetailVo;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Miles
 */
public class EzCardOrderQueryResponse extends EzCardBasicResponse {

    @SerializedName("OrderList")
    private List<EzcTradeDetailVo> list;

    public List<EzcTradeDetailVo> getList() {
        return list;
    }

    public void setList(List<EzcTradeDetailVo> list) {
        this.list = list;
    }
}
