package co.intella.domain.easycard;

import co.intella.model.Device;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Miles
 */
public class EzCardSignOnResponse extends EzCardBasicResponse {

    @SerializedName("DeviceID")
    private String deviceId;

    @SerializedName("NewDeviceID")
    private String newDeviceId;

    @SerializedName("HostSerialNumber")
    private String hostSerialNumber;

    private List<Device> deviceList;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getNewDeviceId() {
        return newDeviceId;
    }

    public void setNewDeviceId(String newDeviceId) {
        this.newDeviceId = newDeviceId;
    }

    public List<Device> getDeviceList() {
        return deviceList;
    }

    public void setDeviceList(List<Device> deviceList) {
        this.deviceList = deviceList;
    }

    public String getHostSerialNumber() {
        return hostSerialNumber;
    }

    public void setHostSerialNumber(String hostSerialNumber) {
        this.hostSerialNumber = hostSerialNumber;
    }
}
