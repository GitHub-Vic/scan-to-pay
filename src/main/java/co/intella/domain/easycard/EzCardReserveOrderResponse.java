package co.intella.domain.easycard;

import co.intella.domain.contratStore.EzcReserveDetailVo;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Miles
 */
public class EzCardReserveOrderResponse extends EzCardBasicResponse {

    @SerializedName("List")
    private List<EzcReserveDetailVo> list;

    public List<EzcReserveDetailVo> getList() {
        return list;
    }

    public void setList(List<EzcReserveDetailVo> list) {
        this.list = list;
    }
}
