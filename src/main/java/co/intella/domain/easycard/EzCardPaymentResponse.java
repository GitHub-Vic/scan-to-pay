package co.intella.domain.easycard;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class EzCardPaymentResponse extends EzCardBasicResponse {

    @SerializedName("EZCardID")
    private String ezCardId;

    @SerializedName("EZCardType")
    private String ezCardType;

    @SerializedName("PersonalProfile")
    private String personalProfile;

    @SerializedName("CardNumberForPrint")
    private String cardNumberForPrint;

    @SerializedName("Amount")
    private String amount;

    @SerializedName("Balance")
    private String balance;

    @SerializedName("BeforeTXNBalance")
    private String beforeTXNBalance;

    @SerializedName("TerminalTXNNumber")
    private String terminalTXNNumber;

    @SerializedName("HostSerialNumber")
    private String hostSerialNumber;

    @SerializedName("Time")
    private String time;

    @SerializedName("Date")
    private String date;

    @SerializedName("ExpiryDate")
    private String expiryDate;

    @SerializedName("CPUPurseVersion")
    private String cpuPurseVersion;

    @SerializedName("BankCode")
    private String bankCode;

    @SerializedName("AreaCode")
    private String areaCode;

    @SerializedName("AutoTopUpAmount")
    private String autoTopUpAmount;

    @SerializedName("AutoTopUpResult")
    private String autoTopUpResult;

    @SerializedName("CardDesc")
    private String cardDesc;

    private EzCardPayment request;

    public String getEzCardId() {
        return ezCardId;
    }

    public void setEzCardId(String ezCardId) {
        this.ezCardId = ezCardId;
    }

    public String getEzCardType() {
        return ezCardType;
    }

    public void setEzCardType(String ezCardType) {
        this.ezCardType = ezCardType;
    }

    public String getPersonalProfile() {
        return personalProfile;
    }

    public void setPersonalProfile(String personalProfile) {
        this.personalProfile = personalProfile;
    }

    public String getCardNumberForPrint() {
        return cardNumberForPrint;
    }

    public void setCardNumberForPrint(String cardNumberForPrint) {
        this.cardNumberForPrint = cardNumberForPrint;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getBeforeTXNBalance() {
        return beforeTXNBalance;
    }

    public void setBeforeTXNBalance(String beforeTXNBalance) {
        this.beforeTXNBalance = beforeTXNBalance;
    }

    public String getTerminalTXNNumber() {
        return terminalTXNNumber;
    }

    public void setTerminalTXNNumber(String terminalTXNNumber) {
        this.terminalTXNNumber = terminalTXNNumber;
    }

    public String getHostSerialNumber() {
        return hostSerialNumber;
    }

    public void setHostSerialNumber(String hostSerialNumber) {
        this.hostSerialNumber = hostSerialNumber;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getCpuPurseVersion() {
        return cpuPurseVersion;
    }

    public void setCpuPurseVersion(String cpuPurseVersion) {
        this.cpuPurseVersion = cpuPurseVersion;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAutoTopUpAmount() {
        return autoTopUpAmount;
    }

    public void setAutoTopUpAmount(String autoTopUpAmount) {
        this.autoTopUpAmount = autoTopUpAmount;
    }

    public EzCardPayment getRequest() {
        return request;
    }

    public void setRequest(EzCardPayment request) {
        this.request = request;
    }

    public String getAutoTopUpResult() {
        return autoTopUpResult;
    }

    public void setAutoTopUpResult(String autoTopUpResult) {
        this.autoTopUpResult = autoTopUpResult;
    }

    public String getCardDesc() {
        return cardDesc;
    }

    public void setCardDesc(String cardDesc) {
        this.cardDesc = cardDesc;
    }
}
