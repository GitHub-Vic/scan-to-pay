package co.intella.domain.easycard;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class EzCardPayment extends EzCardBasicRequest {

    @SerializedName("Amount")
    private String amount;

    @SerializedName("Retry")
    private String retry;

    @SerializedName("BatchNumber")
    private String batchNumber;

    @SerializedName("SameCard")
    private String sameCard;

    public EzCardPayment() {
        this.setServiceType("Payment");
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRetry() {
        return retry;
    }

    public void setRetry(String retry) {
        this.retry = retry;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getSameCard() {
        return sameCard;
    }

    public void setSameCard(String sameCard) {
        this.sameCard = sameCard;
    }
}
