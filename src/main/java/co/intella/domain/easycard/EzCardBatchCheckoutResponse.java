package co.intella.domain.easycard;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class EzCardBatchCheckoutResponse extends EzCardBasicResponse {

    @SerializedName("TerminalTXNNumber")
    private String terminalTXNNumber;

    @SerializedName("HostSerialNumber")
    private String hostSerialNumber;

    @SerializedName("TerminalID")
    private String terminalId;

    @SerializedName("SettlementStatus")
    private String settlementStatus;

    @SerializedName("TxReserveAmount")
    private long txReserveAmount;

    @SerializedName("TxTradeAmount")
    private long txTradeAmount;

    @SerializedName("TxRefundAmount")
    private long txRefundAmount;

    @SerializedName("TxCancelAmount")
    private long txCancelAmount;

    @SerializedName("TxReserveCount")
    private long txReserveCount;

    @SerializedName("TxTradeCount")
    private long txTradeCount;

    @SerializedName("TxRefundCount")
    private long txRefundCount;

    @SerializedName("TxCancelCount")
    private long txCancelCount;

    @SerializedName("TxPointAmount")
    private long txPointAmount;

    @SerializedName("TxPointCount")
    private long txPointCount;

    @SerializedName("TxCountTripForDriver")
    private long txCountTripForDriver;

    @SerializedName("Discount")
    private long discount;

    @SerializedName("CarFleetName")
    private String carFleetName;

    @SerializedName("LicensePlate")
    private String licensePlate;

    @SerializedName("DriverNumber")
    private String driverNumber;

    @SerializedName("BatchNumber")
    private String batchNumber;

    @SerializedName("BatchNo")
    private String batchNo;

    public String getTerminalTXNNumber() {
        return terminalTXNNumber;
    }

    public void setTerminalTXNNumber(String terminalTXNNumber) {
        this.terminalTXNNumber = terminalTXNNumber;
    }

    public String getHostSerialNumber() {
        return hostSerialNumber;
    }

    public void setHostSerialNumber(String hostSerialNumber) {
        this.hostSerialNumber = hostSerialNumber;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getSettlementStatus() {
        return settlementStatus;
    }

    public void setSettlementStatus(String settlementStatus) {
        this.settlementStatus = settlementStatus;
    }

    public long getTxReserveAmount() {
        return txReserveAmount;
    }

    public void setTxReserveAmount(long txReserveAmount) {
        this.txReserveAmount = txReserveAmount;
    }

    public long getTxTradeAmount() {
        return txTradeAmount;
    }

    public void setTxTradeAmount(long txTradeAmount) {
        this.txTradeAmount = txTradeAmount;
    }

    public long getTxRefundAmount() {
        return txRefundAmount;
    }

    public void setTxRefundAmount(long txRefundAmount) {
        this.txRefundAmount = txRefundAmount;
    }

    public long getTxReserveCount() {
        return txReserveCount;
    }

    public void setTxReserveCount(long txReserveCount) {
        this.txReserveCount = txReserveCount;
    }

    public long getTxTradeCount() {
        return txTradeCount;
    }

    public void setTxTradeCount(long txTradeCount) {
        this.txTradeCount = txTradeCount;
    }

    public long getTxRefundCount() {
        return txRefundCount;
    }

    public void setTxRefundCount(long txRefundCount) {
        this.txRefundCount = txRefundCount;
    }

    public long getTxCancelAmount() {
        return txCancelAmount;
    }

    public void setTxCancelAmount(long txCancelAmount) {
        this.txCancelAmount = txCancelAmount;
    }

    public long getTxCancelCount() {
        return txCancelCount;
    }

    public void setTxCancelCount(long txCancelCount) {
        this.txCancelCount = txCancelCount;
    }

    public long getTxPointAmount() {
        return txPointAmount;
    }

    public void setTxPointAmount(long txPointAmount) {
        this.txPointAmount = txPointAmount;
    }

    public long getTxPointCount() {
        return txPointCount;
    }

    public void setTxPointCount(long txPointCount) {
        this.txPointCount = txPointCount;
    }

    public long getTxCountTripForDriver() {
        return txCountTripForDriver;
    }

    public void setTxCountTripForDriver(long txCountTripForDriver) {
        this.txCountTripForDriver = txCountTripForDriver;
    }

    public long getDiscount() {
        return discount;
    }

    public void setDiscount(long discount) {
        this.discount = discount;
    }

    public String getCarFleetName() {
        return carFleetName;
    }

    public void setCarFleetName(String carFleetName) {
        this.carFleetName = carFleetName;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getDriverNumber() {
        return driverNumber;
    }

    public void setDriverNumber(String driverNumber) {
        this.driverNumber = driverNumber;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    @Override
    public String toString() {
        return "EzCardBatchCheckoutResponse{" +
                "terminalTXNNumber='" + terminalTXNNumber + '\'' +
                ", hostSerialNumber='" + hostSerialNumber + '\'' +
                ", terminalId='" + terminalId + '\'' +
                ", settlementStatus='" + settlementStatus + '\'' +
                ", txReserveAmount=" + txReserveAmount +
                ", txTradeAmount=" + txTradeAmount +
                ", txRefundAmount=" + txRefundAmount +
                ", txCancelAmount=" + txCancelAmount +
                ", txReserveCount=" + txReserveCount +
                ", txTradeCount=" + txTradeCount +
                ", txRefundCount=" + txRefundCount +
                ", txCancelCount=" + txCancelCount +
                ", txPointAmount=" + txPointAmount +
                ", txPointCount=" + txPointCount +
                ", txCountTripForDriver=" + txCountTripForDriver +
                ", discount=" + discount +
                ", carFleetName='" + carFleetName + '\'' +
                ", licensePlate='" + licensePlate + '\'' +
                ", driverNumber='" + driverNumber + '\'' +
                ", batchNumber='" + batchNumber + '\'' +
                ", batchNo='" + batchNo + '\'' +
                '}';
    }
}

