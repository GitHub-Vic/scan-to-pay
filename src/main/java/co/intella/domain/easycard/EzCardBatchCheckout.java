package co.intella.domain.easycard;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class EzCardBatchCheckout extends EzCardBasicRequest {

    @SerializedName("BatchNumber")
    private String batchNumber;

    @SerializedName("TotalTXNCount")
    private String totalTXNCount;

    @SerializedName("TotalAmount")
    private String totalAmount;

    public EzCardBatchCheckout() {
        this.setServiceType("Settlement");
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getTotalTXNCount() {
        return totalTXNCount;
    }

    public void setTotalTXNCount(String totalTXNCount) {
        this.totalTXNCount = totalTXNCount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }
}
