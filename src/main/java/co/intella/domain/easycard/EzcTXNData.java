package co.intella.domain.easycard;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class EzcTXNData {

    @SerializedName("TXNType")
    private String txnType;

    @SerializedName("Date")
    private String date;

    @SerializedName("Time")
    private String time;

    @SerializedName("CardNumber")
    private String cardNumber;

    @SerializedName("TerminalTXNNumber")
    private String terminalTXNNumber;

    @SerializedName("Amount")
    private String amount;

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getTerminalTXNNumber() {
        return terminalTXNNumber;
    }

    public void setTerminalTXNNumber(String terminalTXNNumber) {
        this.terminalTXNNumber = terminalTXNNumber;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
