package co.intella.domain.easycard;

import co.intella.model.Device;
import co.intella.model.PaymentAccount;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Miles
 */
public class EzCardIdQuery extends EzCardBasicRequest {

    public EzCardIdQuery() {
        this.setServiceType("EZCardNumberQuery");
    }

    public EzCardIdQuery(Device device, PaymentAccount paymentAccount) {
        LocalDateTime now = LocalDateTime.now();
        String date = now.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        String time = now.format(DateTimeFormatter.ofPattern("HHmmss"));

        this.setServiceType("EZCardNumberQuery");
        this.setTerminalId(paymentAccount.getAccount());
        this.setDeviceId(device.getOwnerDeviceId());

        this.setTerminalTXNNumber(time);
        this.setHostSerialNumber(LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"))); // 因之前有嚴重延遲，所以改為送出前當下
        this.setTime(time);
        this.setDate(date);
        this.setAesKey(device.getAesKey());
    }
}
