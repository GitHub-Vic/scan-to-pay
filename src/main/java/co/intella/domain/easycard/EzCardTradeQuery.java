package co.intella.domain.easycard;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class EzCardTradeQuery extends EzCardBasicRequest {

    @SerializedName("BeforeOrAfter")
    private String beforeOrAfter;

    public EzCardTradeQuery() {
        this.setServiceType("EZCardTXNInQuery");
    }

    public String getBeforeOrAfter() {
        return beforeOrAfter;
    }

    public void setBeforeOrAfter(String beforeOrAfter) {
        this.beforeOrAfter = beforeOrAfter;
    }
}
