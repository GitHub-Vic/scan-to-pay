package co.intella.domain.easycard;

/**
 * @author Miles
 */
public class EzCardAutoReserve extends EzCardPayment {

    public EzCardAutoReserve() {
        this.setServiceType("AutoTopUp");
    }
}
