package co.intella.domain.easycard;

import co.intella.domain.contratStore.DeviceVo;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Miles
 */
public class EzCardDeviceQueryResponse extends EzCardBasicResponse {

    @SerializedName("DeviceList")
    private List<DeviceVo> deviceList;

    public List<DeviceVo> getDeviceList() {
        return deviceList;
    }

    public void setDeviceList(List<DeviceVo> deviceList) {
        this.deviceList = deviceList;
    }

}
