package co.intella.domain.easycard;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Miles
 */
public class EzcSettlementReceive extends EzCardBasicRequest {

    public EzcSettlementReceive() {
        this.setServiceType("PrintTXNList");
    }

    @SerializedName("PrintID")
    private String printId;

    @SerializedName("DeviceNumber")
    private String deviceNumber;

    @SerializedName("BatchNumber")
    private String batchNumber;

    @SerializedName("TXNCount")
    private List<EzcTXNCount> txnCount;

    @SerializedName("TXNList")
    private List<EzcTXNData> txnList;

    public String getPrintId() {
        return printId;
    }

    public void setPrintId(String printId) {
        this.printId = printId;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public List<EzcTXNCount> getTxnCount() {
        return txnCount;
    }

    public void setTxnCount(List<EzcTXNCount> txnCount) {
        this.txnCount = txnCount;
    }

    public List<EzcTXNData> getTxnList() {
        return txnList;
    }

    public void setTxnList(List<EzcTXNData> txnList) {
        this.txnList = txnList;
    }
}
