package co.intella.domain.easycard;

import co.intella.domain.integration.ResponseGeneralData;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class EzCardBasicResponse extends ResponseGeneralData {

    @SerializedName("TXNResult")
    @JsonProperty("TXNResult")
    private String txnResult;

    @SerializedName("Retry")
    @JsonProperty("Retry")
    private String retry;

    @SerializedName("ErrorCode")
    @JsonProperty("ErrorCode")
    private String errorCode;

    @SerializedName("ErrorMessage")
    @JsonProperty("ErrorMessage")
    private String errorMessage;

    @SerializedName("OrderId")
    @JsonProperty("OrderId")
    private String orderId;

    @SerializedName("DongleDeviceID")
    @JsonProperty("DongleDeviceID")
    private String dongleDeviceID;

    @SerializedName("RRNumber")
    @JsonProperty("RRNumber")
    private String rrNumber;

    @SerializedName("TXNType")
    @JsonProperty("TXNType")
    private String txnType;

    @SerializedName("NewAESKey")
    @JsonProperty("NewAESKey")
    private String newAESKey;

    @SerializedName("EZCardPurseID")
    @JsonProperty("EZCardPurseID")
    private String ezCardPurseID;

    @SerializedName("DeviceNumber")
    @JsonProperty("DeviceNumber")
    private String deviceNumber;

    public String getTxnResult() {
        return txnResult;
    }

    public void setTxnResult(String txnResult) {
        this.txnResult = txnResult;
    }

    public String getRetry() {
        return retry;
    }

    public void setRetry(String retry) {
        this.retry = retry;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDongleDeviceID() {
        return dongleDeviceID;
    }

    public void setDongleDeviceID(String dongleDeviceID) {
        this.dongleDeviceID = dongleDeviceID;
    }

    public String getRrNumber() {
        return rrNumber;
    }

    public void setRrNumber(String rrNumber) {
        this.rrNumber = rrNumber;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getNewAESKey() {
        return newAESKey;
    }

    public void setNewAESKey(String newAESKey) {
        this.newAESKey = newAESKey;
    }

    public String getEzCardPurseID() {
        return ezCardPurseID;
    }

    public void setEzCardPurseID(String ezCardPurseID) {
        this.ezCardPurseID = ezCardPurseID;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }
}
