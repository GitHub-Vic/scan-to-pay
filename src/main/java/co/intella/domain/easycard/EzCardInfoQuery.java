package co.intella.domain.easycard;

/**
 * @author Miles
 */
public class EzCardInfoQuery extends EzCardBasicRequest {

    public EzCardInfoQuery() {
        this.setServiceType("EZCardDataInQuery");
    }

}
