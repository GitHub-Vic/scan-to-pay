package co.intella.domain.easycard;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class EzCardReceipt extends EzCardBasicRequest {

    public EzCardReceipt() {
        this.setServiceType("PrintReceipt");
        this.setDeductPoint("0");
        this.setPointBalance("0");
        this.setTransferDiscount("0");
    }

    @SerializedName("PrintID")
    private String printId;

    @SerializedName("DeviceNumber")
    private String deviceNumber;

    @SerializedName("BatchNumber")
    private String batchNumber;

    @SerializedName("CPUPurseVersion")
    private String cpuPurseVersion;

    @SerializedName("RRNumber")
    private String rrNumber;

    @SerializedName("EZCardID")
    private String ezCardId;

    @SerializedName("EZCardPurseID")
    private String ezCardPurseID;

    @SerializedName("BeforeTXNBalance")
    private String beforeTXNBalance;

    @SerializedName("AutoTopUpAmount")
    private String autoTopUpAmount;

    @SerializedName("Amount")
    private String amount;

    @SerializedName("TXNType")
    private String txnType;

    @SerializedName("Balance")
    private String balance;

    @SerializedName("TransferDiscount")
    private String transferDiscount;

    @SerializedName("PointBalance")
    private String pointBalance;

    @SerializedName("DeductPoint")
    private String deductPoint;

    public String getPrintId() {
        return printId;
    }

    public void setPrintId(String printId) {
        this.printId = printId;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getCpuPurseVersion() {
        return cpuPurseVersion;
    }

    public void setCpuPurseVersion(String cpuPurseVersion) {
        this.cpuPurseVersion = cpuPurseVersion;
    }

    public String getRrNumber() {
        return rrNumber;
    }

    public void setRrNumber(String rrNumber) {
        this.rrNumber = rrNumber;
    }

    public String getEzCardId() {
        return ezCardId;
    }

    public void setEzCardId(String ezCardId) {
        this.ezCardId = ezCardId;
    }

    public String getEzCardPurseID() {
        return ezCardPurseID;
    }

    public void setEzCardPurseID(String ezCardPurseID) {
        this.ezCardPurseID = ezCardPurseID;
    }

    public String getBeforeTXNBalance() {
        return beforeTXNBalance;
    }

    public void setBeforeTXNBalance(String beforeTXNBalance) {
        this.beforeTXNBalance = beforeTXNBalance;
    }

    public String getAutoTopUpAmount() {
        return autoTopUpAmount;
    }

    public void setAutoTopUpAmount(String autoTopUpAmount) {
        this.autoTopUpAmount = autoTopUpAmount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getTransferDiscount() {
        return transferDiscount;
    }

    public void setTransferDiscount(String transferDiscount) {
        this.transferDiscount = transferDiscount;
    }

    public String getPointBalance() {
        return pointBalance;
    }

    public void setPointBalance(String pointBalance) {
        this.pointBalance = pointBalance;
    }

    public String getDeductPoint() {
        return deductPoint;
    }

    public void setDeductPoint(String deductPoint) {
        this.deductPoint = deductPoint;
    }
}
