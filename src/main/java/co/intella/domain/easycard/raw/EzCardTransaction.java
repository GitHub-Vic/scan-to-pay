package co.intella.domain.easycard.raw;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class EzCardTransaction {

    @SerializedName("Time")
    private String time;

    @SerializedName("Date")
    private String date;

    @SerializedName("TSQN")
    private String tsqn;

    @SerializedName("SubType")
    private String subType;

    @SerializedName("TXNAMT")
    private String txnAmt;

    @SerializedName("EV")
    private String ev;

    @SerializedName("Sequence")
    private String sequence;

    @SerializedName("LocationID")
    private String locationID;

    @SerializedName("ServiceProviderID")
    private String serviceProviderID;

    public String getTsqn() {
        return tsqn;
    }

    public void setTsqn(String tsqn) {
        this.tsqn = tsqn;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getTxnAmt() {
        return txnAmt;
    }

    public void setTxnAmt(String txnAmt) {
        this.txnAmt = txnAmt;
    }

    public String getEv() {
        return ev;
    }

    public void setEv(String ev) {
        this.ev = ev;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getLocationID() {
        return locationID;
    }

    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

    public String getServiceProviderID() {
        return serviceProviderID;
    }

    public void setServiceProviderID(String serviceProviderID) {
        this.serviceProviderID = serviceProviderID;
    }
}
