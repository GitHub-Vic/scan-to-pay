package co.intella.domain.easycard;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class EzcTXNCount {

    @SerializedName("PaymentCount")
    private String paymentCount;

    @SerializedName("PaymentTotal")
    private String paymentTotal;

    @SerializedName("RefundCount")
    private String refundCount;

    @SerializedName("RefundTotal")
    private String refundTotal;

    @SerializedName("CancelReserveCount")
    private String cancelReserveCount;

    @SerializedName("CancelReserveTotal")
    private String cancelReserveTotal;

    @SerializedName("AutoTopUpCount")
    private String autoTopUpCount;

    @SerializedName("AutoTopUpTotal")
    private String autoTopUpTotal;

    @SerializedName("CashTopUpCount")
    private String cashTopUpCount;

    @SerializedName("CashTopUpTotal")
    private String cashTopUpTotal;

    public String getPaymentCount() {
        return paymentCount;
    }

    public void setPaymentCount(String paymentCount) {
        this.paymentCount = paymentCount;
    }

    public String getPaymentTotal() {
        return paymentTotal;
    }

    public void setPaymentTotal(String paymentTotal) {
        this.paymentTotal = paymentTotal;
    }

    public String getRefundCount() {
        return refundCount;
    }

    public void setRefundCount(String refundCount) {
        this.refundCount = refundCount;
    }

    public String getRefundTotal() {
        return refundTotal;
    }

    public void setRefundTotal(String refundTotal) {
        this.refundTotal = refundTotal;
    }

    public String getAutoTopUpCount() {
        return autoTopUpCount;
    }

    public void setAutoTopUpCount(String autoTopUpCount) {
        this.autoTopUpCount = autoTopUpCount;
    }

    public String getAutoTopUpTotal() {
        return autoTopUpTotal;
    }

    public void setAutoTopUpTotal(String autoTopUpTotal) {
        this.autoTopUpTotal = autoTopUpTotal;
    }

    public String getCancelReserveCount() {
        return cancelReserveCount;
    }

    public void setCancelReserveCount(String cancelReserveCount) {
        this.cancelReserveCount = cancelReserveCount;
    }

    public String getCancelReserveTotal() {
        return cancelReserveTotal;
    }

    public void setCancelReserveTotal(String cancelReserveTotal) {
        this.cancelReserveTotal = cancelReserveTotal;
    }

    public String getCashTopUpCount() {
        return cashTopUpCount;
    }

    public void setCashTopUpCount(String cashTopUpCount) {
        this.cashTopUpCount = cashTopUpCount;
    }

    public String getCashTopUpTotal() {
        return cashTopUpTotal;
    }

    public void setCashTopUpTotal(String cashTopUpTotal) {
        this.cashTopUpTotal = cashTopUpTotal;
    }
}
