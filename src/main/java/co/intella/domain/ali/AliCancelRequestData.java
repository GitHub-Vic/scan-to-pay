package co.intella.domain.ali;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Miles Wu
 */
public class AliCancelRequestData extends AliBasicRequestData{


    @SerializedName("OrderDT")
    @JsonProperty(value = "OrderDT")
    private String orderDate;

    @SerializedName("StoreOrderNo")
    @JsonProperty(value = "OrderNo", required = true)
    private String orderNumber;

    @JsonProperty(value = "CancelOrderDT")
    private String cancelOrderDate;

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getCancelOrderDate() {
        return cancelOrderDate;
    }

    public void setCancelOrderDate(String cancelOrderDate) {
        this.cancelOrderDate = cancelOrderDate;
    }
}
