package co.intella.domain.ali;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Miles Wu
 */
public class AliQueryActionRequestData extends AliBasicRequestData {

    @SerializedName("OrderDT")
    @JsonProperty(value = "OrderDT")
    private String orderDate;

    @SerializedName("StoreOrderNo")
    @JsonProperty(value = "OrderNo", required = true)
    private String orderNo;

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
