package co.intella.domain.ali;

/**
 * @author Miles Wu
 */
public class AliRequestBody {

    public static final String PAYMENT = "payment";

    public static final String CURRENCY = "queryforex";

    public static final String CANCEL = "cancel";

    public static final String QUERY = "query";

    public static final String QUERY_ACTION = "queryaction";

    public static final String REFUND = "refund";

    private static final String BANK_PRIVATE_KEY_VERSION = "002";

    public AliRequestBody() {
        this.bankPKeyVersion = BANK_PRIVATE_KEY_VERSION;
    }

    private String action;

    private String bankPKeyVersion;

    private String secureData;

    private String apiToken;

    private String hashDigest;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getBankPKeyVersion() {
        return bankPKeyVersion;
    }

    public void setBankPKeyVersion(String bankPKeyVersion) {
        this.bankPKeyVersion = bankPKeyVersion;
    }

    public String getSecureData() {
        return secureData;
    }

    public void setSecureData(String secureData) {
        this.secureData = secureData;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public String getHashDigest() {
        return hashDigest;
    }

    public void setHashDigest(String hashDigest) {
        this.hashDigest = hashDigest;
    }
}
