package co.intella.domain.ali;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles Wu
 */
public class AliResponseBody {

    @SerializedName("Action")
    private String action;

    @SerializedName("Status")
    private String status;

    @SerializedName("ErrorCode")
    private String errorCode;

    @SerializedName("ErrorDesc")
    private String errorDesc;

    @SerializedName("HashDigest")
    private String hashDigest;

    @SerializedName("SecureData")
    private String secureData;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    public String getHashDigest() {
        return hashDigest;
    }

    public void setHashDigest(String hashDigest) {
        this.hashDigest = hashDigest;
    }

    public String getSecureData() {
        return secureData;
    }

    public void setSecureData(String secureData) {
        this.secureData = secureData;
    }
}
