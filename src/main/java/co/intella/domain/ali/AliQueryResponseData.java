package co.intella.domain.ali;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class AliQueryResponseData {

    @SerializedName("OrderNo")
    private String orderNo;

    @SerializedName("OrderDT")
    private String orderDate;

    @SerializedName("BankOrderNo")
    private String bankOrderNo;

    @SerializedName("BankOrderDT")
    private String bankOrderDate;

    @SerializedName("WalletOrderNo")
    private String walletOrderNo;

    @SerializedName("WalletOrderDT")
    private String walletOrderDate;

    @SerializedName("OrderCurrency")
    private String orderCurrency;

    @SerializedName("OrderAmount")
    private double orderAmount;

    @SerializedName("RealOrderAmount")
    private String realOrderAmount;

    @SerializedName("BankCurrency")
    private String bankCurrency;

    @SerializedName("Store2BankExRate")
    private double store2BankRate;

    @SerializedName("WalletCurrency")
    private String walletCurrency;

    @SerializedName("RealWalletAmount")
    private double realWalletAmount;

    @SerializedName("RefundOrderAmount")
    private double refundOrderAmount;

    @SerializedName("BuyerID")
    private String buyerId;

    @SerializedName("BuyerTransferType")
    private int buyerTrasferType;

    @SerializedName("BuyerWallet")
    private String buyerWallet;

    @SerializedName("BuyerPaymentType")
    private int buyerPaymentType;

    @SerializedName("OrderStatus")
    private int orderStatus;

    @SerializedName("OrderTitle")
    private String orderTitle;

    @SerializedName("OrderDesc")
    private String orderDesc;

    @SerializedName("StoreApproprationStatus")
    private int storeAppropriationStatus;

    @SerializedName("StoreApproprationDate")
    private String storeAppropriationDate;

    @SerializedName("StoreApproprationNo")
    private String storeAppropriationNo;

    @SerializedName("OrderFaileCode")
    private int orderFailCode;

    @SerializedName("OrderFaileDesc")
    private String orderFailDesc;

    @SerializedName("BuyerWalletUserID")
    private String buyerWalletUserId;

    @SerializedName("BuyerWalletLoginID")
    private String buyerWalletLoginId;

    @SerializedName("BuyerWalletBalance")
    private double buyerWalletBalance;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getBankOrderNo() {
        return bankOrderNo;
    }

    public void setBankOrderNo(String bankOrderNo) {
        this.bankOrderNo = bankOrderNo;
    }

    public String getBankOrderDate() {
        return bankOrderDate;
    }

    public void setBankOrderDate(String bankOrderDate) {
        this.bankOrderDate = bankOrderDate;
    }

    public String getWalletOrderNo() {
        return walletOrderNo;
    }

    public void setWalletOrderNo(String walletOrderNo) {
        this.walletOrderNo = walletOrderNo;
    }

    public String getWalletOrderDate() {
        return walletOrderDate;
    }

    public void setWalletOrderDate(String walletOrderDate) {
        this.walletOrderDate = walletOrderDate;
    }

    public String getOrderCurrency() {
        return orderCurrency;
    }

    public void setOrderCurrency(String orderCurrency) {
        this.orderCurrency = orderCurrency;
    }

    public double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(double orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getRealOrderAmount() {
        return realOrderAmount;
    }

    public void setRealOrderAmount(String realOrderAmount) {
        this.realOrderAmount = realOrderAmount;
    }

    public String getBankCurrency() {
        return bankCurrency;
    }

    public void setBankCurrency(String bankCurrency) {
        this.bankCurrency = bankCurrency;
    }

    public double getStore2BankRate() {
        return store2BankRate;
    }

    public void setStore2BankRate(double store2BankRate) {
        this.store2BankRate = store2BankRate;
    }

    public String getWalletCurrency() {
        return walletCurrency;
    }

    public void setWalletCurrency(String walletCurrency) {
        this.walletCurrency = walletCurrency;
    }

    public double getRealWalletAmount() {
        return realWalletAmount;
    }

    public void setRealWalletAmount(double realWalletAmount) {
        this.realWalletAmount = realWalletAmount;
    }

    public double getRefundOrderAmount() {
        return refundOrderAmount;
    }

    public void setRefundOrderAmount(double refundOrderAmount) {
        this.refundOrderAmount = refundOrderAmount;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public int getBuyerTrasferType() {
        return buyerTrasferType;
    }

    public void setBuyerTrasferType(int buyerTrasferType) {
        this.buyerTrasferType = buyerTrasferType;
    }

    public String getBuyerWallet() {
        return buyerWallet;
    }

    public void setBuyerWallet(String buyerWallet) {
        this.buyerWallet = buyerWallet;
    }

    public int getBuyerPaymentType() {
        return buyerPaymentType;
    }

    public void setBuyerPaymentType(int buyerPaymentType) {
        this.buyerPaymentType = buyerPaymentType;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderTitle() {
        return orderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }

    public String getOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(String orderDesc) {
        this.orderDesc = orderDesc;
    }

    public int getStoreAppropriationStatus() {
        return storeAppropriationStatus;
    }

    public void setStoreAppropriationStatus(int storeAppropriationStatus) {
        this.storeAppropriationStatus = storeAppropriationStatus;
    }

    public String getStoreAppropriationDate() {
        return storeAppropriationDate;
    }

    public void setStoreAppropriationDate(String storeAppropriationDate) {
        this.storeAppropriationDate = storeAppropriationDate;
    }

    public String getStoreAppropriationNo() {
        return storeAppropriationNo;
    }

    public void setStoreAppropriationNo(String storeAppropriationNo) {
        this.storeAppropriationNo = storeAppropriationNo;
    }

    public int getOrderFailCode() {
        return orderFailCode;
    }

    public void setOrderFailCode(int orderFailCode) {
        this.orderFailCode = orderFailCode;
    }

    public String getOrderFailDesc() {
        return orderFailDesc;
    }

    public void setOrderFailDesc(String orderFailDesc) {
        this.orderFailDesc = orderFailDesc;
    }

    public String getBuyerWalletUserId() {
        return buyerWalletUserId;
    }

    public void setBuyerWalletUserId(String buyerWalletUserId) {
        this.buyerWalletUserId = buyerWalletUserId;
    }

    public String getBuyerWalletLoginId() {
        return buyerWalletLoginId;
    }

    public void setBuyerWalletLoginId(String buyerWalletLoginId) {
        this.buyerWalletLoginId = buyerWalletLoginId;
    }

    public double getBuyerWalletBalance() {
        return buyerWalletBalance;
    }

    public void setBuyerWalletBalance(double buyerWalletBalance) {
        this.buyerWalletBalance = buyerWalletBalance;
    }
}
