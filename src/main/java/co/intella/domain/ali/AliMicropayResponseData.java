package co.intella.domain.ali;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class AliMicropayResponseData {

    @SerializedName("OrderNo")
    private String orderNo;

    @SerializedName("OrderDT")
    private String orderDate;

    @SerializedName("BuyerWallet")
    private String buyerWallet;

    @SerializedName("BuyerID")
    private String buyerId;

    @SerializedName("BankOrderNo")
    private String bankOrderNo;

    @SerializedName("BankOrderDT")
    private String bankOrderDate;

    @SerializedName("WalletOrderNo")
    private String walletOrderNo;

    @SerializedName("WalletOrderDT")
    private String walletOrderDate;

    @SerializedName("OrderCurrency")
    private String orderCurrency;

    @SerializedName("OrderAmount")
    private double orderAmount;

    @SerializedName("RealOrderAmount")
    private double realOrderAmount;

    @SerializedName("BankCurrency")
    private String bankCurrency;

    @SerializedName("Store2BankExRate")
    private String store2BankExRate;

    @SerializedName("WalletCurrency")
    private String walletCurrency;

    @SerializedName("RealWalletAmount")
    private double realWalletAmount;

    @SerializedName("ReceiptDesc")
    private String receuptDesc;

    @SerializedName("Memo")
    private String memo;

    @SerializedName("BuyerWalletUserID")
    private String buyerWalletUserId;

    @SerializedName("BuyerWalletLoginID")
    private String buyerWalletLoginId;

    @SerializedName("BuyerWalletBalance")
    private double buyerWalletBalance;

    @SerializedName("OrderExtraInfo")
    private String orderExtraInfo;

    @SerializedName("Coupons")
    private String coupons;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getBuyerWallet() {
        return buyerWallet;
    }

    public void setBuyerWallet(String buyerWallet) {
        this.buyerWallet = buyerWallet;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getBankOrderNo() {
        return bankOrderNo;
    }

    public void setBankOrderNo(String bankOrderNo) {
        this.bankOrderNo = bankOrderNo;
    }

    public String getBankOrderDate() {
        return bankOrderDate;
    }

    public void setBankOrderDate(String bankOrderDate) {
        this.bankOrderDate = bankOrderDate;
    }

    public String getWalletOrderNo() {
        return walletOrderNo;
    }

    public void setWalletOrderNo(String walletOrderNo) {
        this.walletOrderNo = walletOrderNo;
    }

    public String getWalletOrderDate() {
        return walletOrderDate;
    }

    public void setWalletOrderDate(String walletOrderDate) {
        this.walletOrderDate = walletOrderDate;
    }

    public String getOrderCurrency() {
        return orderCurrency;
    }

    public void setOrderCurrency(String orderCurrency) {
        this.orderCurrency = orderCurrency;
    }

    public double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(double orderAmount) {
        this.orderAmount = orderAmount;
    }

    public double getRealOrderAmount() {
        return realOrderAmount;
    }

    public void setRealOrderAmount(double realOrderAmount) {
        this.realOrderAmount = realOrderAmount;
    }

    public String getBankCurrency() {
        return bankCurrency;
    }

    public void setBankCurrency(String bankCurrency) {
        this.bankCurrency = bankCurrency;
    }

    public String getStore2BankExRate() {
        return store2BankExRate;
    }

    public void setStore2BankExRate(String store2BankExRate) {
        this.store2BankExRate = store2BankExRate;
    }

    public String getWalletCurrency() {
        return walletCurrency;
    }

    public void setWalletCurrency(String walletCurrency) {
        this.walletCurrency = walletCurrency;
    }

    public double getRealWalletAmount() {
        return realWalletAmount;
    }

    public void setRealWalletAmount(double realWalletAmount) {
        this.realWalletAmount = realWalletAmount;
    }

    public String getReceuptDesc() {
        return receuptDesc;
    }

    public void setReceuptDesc(String receuptDesc) {
        this.receuptDesc = receuptDesc;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getBuyerWalletUserId() {
        return buyerWalletUserId;
    }

    public void setBuyerWalletUserId(String buyerWalletUserId) {
        this.buyerWalletUserId = buyerWalletUserId;
    }

    public String getBuyerWalletLoginId() {
        return buyerWalletLoginId;
    }

    public void setBuyerWalletLoginId(String buyerWalletLoginId) {
        this.buyerWalletLoginId = buyerWalletLoginId;
    }

    public double getBuyerWalletBalance() {
        return buyerWalletBalance;
    }

    public void setBuyerWalletBalance(double buyerWalletBalance) {
        this.buyerWalletBalance = buyerWalletBalance;
    }

    public String getOrderExtraInfo() {
        return orderExtraInfo;
    }

    public void setOrderExtraInfo(String orderExtraInfo) {
        this.orderExtraInfo = orderExtraInfo;
    }

    public String getCoupons() {
        return coupons;
    }

    public void setCoupons(String coupons) {
        this.coupons = coupons;
    }
}
