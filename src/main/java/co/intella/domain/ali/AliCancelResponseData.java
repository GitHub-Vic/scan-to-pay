package co.intella.domain.ali;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class AliCancelResponseData {

    @SerializedName("OrderNo")
    private String orderNo;

    @SerializedName("OrderDT")
    private String orderDate;

    @SerializedName("BankOrderNo")
    private String bankOrderNo;

    @SerializedName("BankCancelOrderNo")
    private String bankCancelOrderNo;

    @SerializedName("BankCancelOrderDT")
    private String bankCancelOrderDate;

    @SerializedName("WalletOrderNo")
    private String walletOrderNo;

    @SerializedName("WalletCancelOrderNo")
    private String walletCancelOrderNo;

    @SerializedName("WalletCancelOrderDT")
    private String walletCancelOrderDate;

}
