package co.intella.domain.ali;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Alex
 */

public class AliReturnData {

    @JsonProperty("STOREID")
    private String stordId;

    @JsonProperty("ORDERID")
    private String orderId;

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("CCY")
    private String ccy;

    @JsonProperty("AMT")
    private String amt;

    @JsonProperty("SIGN")
    private String sign;

    @JsonProperty("SIGN_CHARSET")
    private String singCharset;

    public String getStordId() {
        return stordId;
    }

    public void setStordId(String stordId) {
        this.stordId = stordId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getSingCharset() {
        return singCharset;
    }

    public void setSingCharset(String singCharset) {
        this.singCharset = singCharset;
    }
}
