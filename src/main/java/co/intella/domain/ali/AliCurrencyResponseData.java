package co.intella.domain.ali;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles Wu
 */
public class AliCurrencyResponseData {

    @SerializedName("ExchangeRate")
    private double exchangeRate;

    @SerializedName("ExchangeAmount")
    private double exchangeAmount;

    public double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public double getExchangeAmount() {
        return exchangeAmount;
    }

    public void setExchangeAmount(double exchangeAmount) {
        this.exchangeAmount = exchangeAmount;
    }
}
