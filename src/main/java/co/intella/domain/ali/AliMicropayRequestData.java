package co.intella.domain.ali;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Miles Wu
 */
public class AliMicropayRequestData extends AliBasicRequestData{

    @SerializedName("AuthCode")
    @JsonProperty(value = "BuyerID", required = true)
    private String buyerId;

    @SerializedName("StoreOrderNo")
    @JsonProperty(value = "OrderNo", required = true)
    private String orderNumber;

    @SerializedName("TotalFee")
    @JsonProperty(value = "OrderAmount", required = true)
    private double orderAmount;

    @SerializedName("FeeType")
    @JsonProperty(value = "OrderCurrency", required = true)
    private String orderCurrency;

    @JsonProperty(value = "OrderDT", required = true)
    private String orderDate;

    @SerializedName("Body")
    @JsonProperty(value = "OrderTitle", required = true)
    private String orderTitle;

    @SerializedName("Detail")
    @JsonProperty(value = "OrderDesc")
    private String orderDescription;

    @JsonProperty(value = "ProductQuantity")
    private int productQuantity;

    @JsonProperty(value = "OrderExtraInfo")
    private String orderExtraInfo;

    @JsonProperty(value = "OrderLongitude")
    private double orderLongitude;

    @JsonProperty(value = "OrderLatitude")
    private double  orderLatitude;

    @JsonProperty(value = "ProductInfo")
    private String productInfo;

    @JsonProperty(value = "BuyerWallet")
    private String buyerWallet;

    @JsonProperty(value = "BuyerPaymentType")
    private int buyerPaymentType;

    @JsonProperty(value = "BuyerTransferType")
    private int buyerTransferType;

    @JsonProperty(value = "Coupons")
    private String coupons;

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(double orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getOrderCurrency() {
        return orderCurrency;
    }

    public void setOrderCurrency(String orderCurrency) {
        this.orderCurrency = orderCurrency;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderTitle() {
        return orderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }

    public String getOrderDescription() {
        return orderDescription;
    }

    public void setOrderDescription(String orderDescription) {
        this.orderDescription = orderDescription;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getOrderExtraInfo() {
        return orderExtraInfo;
    }

    public void setOrderExtraInfo(String orderExtraInfo) {
        this.orderExtraInfo = orderExtraInfo;
    }

    public double getOrderLongitude() {
        return orderLongitude;
    }

    public void setOrderLongitude(double orderLongitude) {
        this.orderLongitude = orderLongitude;
    }

    public double getOrderLatitude() {
        return orderLatitude;
    }

    public void setOrderLatitude(double orderLatitude) {
        this.orderLatitude = orderLatitude;
    }

    public String getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(String productInfo) {
        this.productInfo = productInfo;
    }

    public String getBuyerWallet() {
        return buyerWallet;
    }

    public void setBuyerWallet(String buyerWallet) {
        this.buyerWallet = buyerWallet;
    }

    public int getBuyerPaymentType() {
        return buyerPaymentType;
    }

    public void setBuyerPaymentType(int buyerPaymentType) {
        this.buyerPaymentType = buyerPaymentType;
    }

    public int getBuyerTransferType() {
        return buyerTransferType;
    }

    public void setBuyerTransferType(int buyerTransferType) {
        this.buyerTransferType = buyerTransferType;
    }

    public String getCoupons() {
        return coupons;
    }

    public void setCoupons(String coupons) {
        this.coupons = coupons;
    }
}
