package co.intella.domain.ali;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Miles Wu
 */
public class AliBasicRequestData {

    @JsonProperty(value = "StoreID", required = true)
    private String storeId;

    @SerializedName("DeviceInfo")
    @JsonProperty(value = "TermID", required = true)
    private String termId;

    @JsonProperty(value = "Timeout", required = true)
    private long timeout;

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getTermId() {
        return termId;
    }

    public void setTermId(String termId) {
        this.termId = termId;
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }
}
