package co.intella.domain.ali;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class AliOnlinepayRequestData {

    @SerializedName("AliServiceType")
    @JsonProperty("DEALID")
    private String aliServiceType;

    @SerializedName("AliMethod")
    @JsonProperty("CHANNELID")
    private String aliMethod;  // ali-pay : 02

    @SerializedName("MchId")
    @JsonProperty("STOREID")
    private String mchId;

    @SerializedName("StoreOrderNo")
    @JsonProperty("ORDERID")
    private String storeOrderNo;

    @SerializedName("FeeType")
    @JsonProperty("CCY")
    private String feeType;

    @SerializedName("TotalFee")
    @JsonProperty("AMT")
    private String totalFee;

    @SerializedName("Body")
    @JsonProperty("SUBJECT")
    private String body;

    @SerializedName("Detail")
    @JsonProperty("BODY")
    private String detail;

    @SerializedName("PayInterface")
    @JsonProperty("PAYINTERFACE")
    private String payInterface;  // 1: web-site, 2: mobile

    @SerializedName("TimeExpire")
    @JsonProperty("TIMEOUT")
    private String timeExpire; // 5m or 5h,...etc

    @SerializedName("BackURL")
    @JsonProperty("BACKURL")
    private String backurl;

    @SerializedName("Sign")
    @JsonProperty("SIGN")
    private String sign;

    @SerializedName("SignCharset")
    @JsonProperty("SIGN_CHARSET")
    private String signCharset;

    public String getAliServiceType() {
        return aliServiceType;
    }

    public void setAliServiceType(String aliServiceType) {
        this.aliServiceType = aliServiceType;
    }

    public String getAliMethod() {
        return aliMethod;
    }

    public void setAliMethod(String aliMethod) {
        this.aliMethod = aliMethod;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getPayInterface() {
        return payInterface;
    }

    public void setPayInterface(String payInterface) {
        this.payInterface = payInterface;
    }

    public String getTimeExpire() {
        return timeExpire;
    }

    public void setTimeExpire(String timeExpire) {
        this.timeExpire = timeExpire;
    }

    public String getBackurl() {
        return backurl;
    }

    public void setBackurl(String backurl) {
        this.backurl = backurl;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getSignCharset() {
        return signCharset;
    }

    public void setSignCharset(String signCharset) {
        this.signCharset = signCharset;
    }
}
