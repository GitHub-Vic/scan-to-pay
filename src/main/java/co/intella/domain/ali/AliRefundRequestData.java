package co.intella.domain.ali;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Miles Wu
 */
public class AliRefundRequestData extends AliBasicRequestData {

    @JsonProperty(value = "OrderDT")
    private String orderDate;

    @JsonProperty(value = "OrderNo", required = true)
    @SerializedName("StoreOrderNo")
    private String orderNum;

    @JsonProperty(value = "OutRefundNo", required = true)
    @SerializedName("StoreRefundNo")
    private String refundOrderNum;

    @JsonProperty(value = "RefundOrderDT")
    private String refundOrderDate;

    @JsonProperty(value = "RefundOrderAmount", required = true)
    @SerializedName("RefundFee")
    private double refundOrderAmount;

    @JsonProperty(value = "RefundReason")
    private String refundReason;

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getRefundOrderNum() {
        return refundOrderNum;
    }

    public void setRefundOrderNum(String refundOrderNum) {
        this.refundOrderNum = refundOrderNum;
    }

    public String getRefundOrderDate() {
        return refundOrderDate;
    }

    public void setRefundOrderDate(String refundOrderDate) {
        this.refundOrderDate = refundOrderDate;
    }

    public double getRefundOrderAmount() {
        return refundOrderAmount;
    }

    public void setRefundOrderAmount(double refundOrderAmount) {
        this.refundOrderAmount = refundOrderAmount;
    }

    public String getRefundReason() {
        return refundReason;
    }

    public void setRefundReason(String refundReason) {
        this.refundReason = refundReason;
    }
}
