package co.intella.domain.ali;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Miles Wu
 */
public class AliCurrencyRequestData extends AliBasicRequestData{

    @SerializedName("Amount")
    @JsonProperty(value = "Amount", required = true)
    private double amount;

    @SerializedName("Currency")
    @JsonProperty(value = "Currency")
    private String currency;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
