package co.intella.domain.ali;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class AliQueryActionResponseData {

    @SerializedName("OrderNo")
    private String orderNo;

    @SerializedName("OrderDT")
    private String orderDate;

    @SerializedName("IsCancelActionEnable")
    private String isCancelActionEnable;

    @SerializedName("IsRefundActionEnable")
    private String isRefundActionEnable;

    @SerializedName("RefundMax")
    private double refundMax;

    @SerializedName("RefundMin")
    private double refundMin;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getIsCancelActionEnable() {
        return isCancelActionEnable;
    }

    public void setIsCancelActionEnable(String isCancelActionEnable) {
        this.isCancelActionEnable = isCancelActionEnable;
    }

    public String getIsRefundActionEnable() {
        return isRefundActionEnable;
    }

    public void setIsRefundActionEnable(String isRefundActionEnable) {
        this.isRefundActionEnable = isRefundActionEnable;
    }

    public double getRefundMax() {
        return refundMax;
    }

    public void setRefundMax(double refundMax) {
        this.refundMax = refundMax;
    }

    public double getRefundMin() {
        return refundMin;
    }

    public void setRefundMin(double refundMin) {
        this.refundMin = refundMin;
    }
}
