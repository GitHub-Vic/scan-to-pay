package co.intella.domain.ali;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class AliOnlinepayResponseData {

    @SerializedName("STOREID")
    @JsonProperty("STOREID")
    private String mchId;

    @SerializedName("ORDERID")
    @JsonProperty("ORDERID")
    private String storeOrderNo;

    @SerializedName("STATUS")
    @JsonProperty("STATUS")
    private String status;

    @SerializedName("CCY")
    @JsonProperty("CCY")
    private String feeType;

    @SerializedName("AMT")
    @JsonProperty("AMT")
    private String totalFee;

    @SerializedName("SIGN")
    @JsonProperty("SIGN")
    private String sign;

    @SerializedName("SIGN_CHARSET")
    @JsonProperty("SIGN_CHARSET")
    private String signCharset;

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getSignCharset() {
        return signCharset;
    }

    public void setSignCharset(String signCharset) {
        this.signCharset = signCharset;
    }
}
