package co.intella.domain.ali;

import com.google.gson.annotations.SerializedName;

/**
 * @author Miles
 */
public class AliRefundResponseData {

    @SerializedName("OrderNo")
    private String orderNo;

    @SerializedName("OrderDT")
    private String orderDate;

    @SerializedName("BankRefundOrderNo")
    private String bankRefundOrderNo;

    @SerializedName("BankRefundOrderDT")
    private String bankRefundOrderDate;

    @SerializedName("WalletOrderNo")
    private String walletRefundOrderNo;

    @SerializedName("WalletRefundOrderDT")
    private String walletRefundOrderDate;

    @SerializedName("RefundOrderCurrency")
    private String refundOrderCurrency;

    @SerializedName("RefundOrderAmount")
    private double refundOrderAmount;

    @SerializedName("RealRefundOrderAmount")
    private double realRefundOrderAmount;

    @SerializedName("BankCurrency")
    private String bankCurrency;

    @SerializedName("Store2BankRefundExRate")
    private double store2BankRefundExRate;

    @SerializedName("RealBankRefundAmount")
    private double realBankRefundOrderAmount;

    @SerializedName("WalletCurrency")
    private String walletCurrency;

    @SerializedName("WalletRefundExAmount")
    private double walletRefundExAmount;

    @SerializedName("RealWalletRefundAmount")
    private double realWalletRefundAmount;

    @SerializedName("BuyerWalletUserID")
    private String buyerWalletUserId;

    @SerializedName("BuyerWalletLoginID")
    private String buyerWalletLoginId;

    @SerializedName("BuyerWalletBalance")
    private String buyerWalletBalance;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getBankRefundOrderNo() {
        return bankRefundOrderNo;
    }

    public void setBankRefundOrderNo(String bankRefundOrderNo) {
        this.bankRefundOrderNo = bankRefundOrderNo;
    }

    public String getBankRefundOrderDate() {
        return bankRefundOrderDate;
    }

    public void setBankRefundOrderDate(String bankRefundOrderDate) {
        this.bankRefundOrderDate = bankRefundOrderDate;
    }

    public String getWalletRefundOrderNo() {
        return walletRefundOrderNo;
    }

    public void setWalletRefundOrderNo(String walletRefundOrderNo) {
        this.walletRefundOrderNo = walletRefundOrderNo;
    }

    public String getWalletRefundOrderDate() {
        return walletRefundOrderDate;
    }

    public void setWalletRefundOrderDate(String walletRefundOrderDate) {
        this.walletRefundOrderDate = walletRefundOrderDate;
    }

    public String getRefundOrderCurrency() {
        return refundOrderCurrency;
    }

    public void setRefundOrderCurrency(String refundOrderCurrency) {
        this.refundOrderCurrency = refundOrderCurrency;
    }

    public double getRefundOrderAmount() {
        return refundOrderAmount;
    }

    public void setRefundOrderAmount(double refundOrderAmount) {
        this.refundOrderAmount = refundOrderAmount;
    }

    public double getRealRefundOrderAmount() {
        return realRefundOrderAmount;
    }

    public void setRealRefundOrderAmount(double realRefundOrderAmount) {
        this.realRefundOrderAmount = realRefundOrderAmount;
    }

    public String getBankCurrency() {
        return bankCurrency;
    }

    public void setBankCurrency(String bankCurrency) {
        this.bankCurrency = bankCurrency;
    }

    public double getStore2BankRefundExRate() {
        return store2BankRefundExRate;
    }

    public void setStore2BankRefundExRate(double store2BankRefundExRate) {
        this.store2BankRefundExRate = store2BankRefundExRate;
    }

    public double getRealBankRefundOrderAmount() {
        return realBankRefundOrderAmount;
    }

    public void setRealBankRefundOrderAmount(double realBankRefundOrderAmount) {
        this.realBankRefundOrderAmount = realBankRefundOrderAmount;
    }

    public String getWalletCurrency() {
        return walletCurrency;
    }

    public void setWalletCurrency(String walletCurrency) {
        this.walletCurrency = walletCurrency;
    }

    public double getWalletRefundExAmount() {
        return walletRefundExAmount;
    }

    public void setWalletRefundExAmount(double walletRefundExAmount) {
        this.walletRefundExAmount = walletRefundExAmount;
    }

    public double getRealWalletRefundAmount() {
        return realWalletRefundAmount;
    }

    public void setRealWalletRefundAmount(double realWalletRefundAmount) {
        this.realWalletRefundAmount = realWalletRefundAmount;
    }

    public String getBuyerWalletUserId() {
        return buyerWalletUserId;
    }

    public void setBuyerWalletUserId(String buyerWalletUserId) {
        this.buyerWalletUserId = buyerWalletUserId;
    }

    public String getBuyerWalletLoginId() {
        return buyerWalletLoginId;
    }

    public void setBuyerWalletLoginId(String buyerWalletLoginId) {
        this.buyerWalletLoginId = buyerWalletLoginId;
    }

    public String getBuyerWalletBalance() {
        return buyerWalletBalance;
    }

    public void setBuyerWalletBalance(String buyerWalletBalance) {
        this.buyerWalletBalance = buyerWalletBalance;
    }
}
