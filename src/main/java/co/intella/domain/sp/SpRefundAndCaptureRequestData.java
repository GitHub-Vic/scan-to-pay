package co.intella.domain.sp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class SpRefundAndCaptureRequestData extends SpBasicRequestData{

    @SerializedName("RespondType")
    @JsonProperty(value = "RespondType", required = true)
    private String respondType;

    @SerializedName(value = "CreateTime")
    @JsonProperty(value = "TimeStamp", required = true)
    private String createTime;

    @SerializedName(value = "ver")
    @JsonProperty(value = "Version", required = true)
    private String ver;

    @SerializedName(value = "StoreOrderNo")
    @JsonProperty(value = "MerchantOrderNo")
    private String storeOrderNo;

    @SerializedName(value = "TotalFee")
    @JsonProperty(value = "AMT", required = true)
    private String totalFee;

    @SerializedName(value = "IndexType")
    @JsonProperty(value = "IndexType", required = true)
    private String indexType;

    @SerializedName(value = "SysOrderNo")
    @JsonProperty(value = "TradeNo")
    private String sysOrderNo;

    @SerializedName(value = "CloseType")
    @JsonProperty(value = "CloseType", required = true)
    private String closeType;

    public String getRespondType() {
        return respondType;
    }

    public void setRespondType(String respondType) {
        this.respondType = respondType;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getIndexType() {
        return indexType;
    }

    public void setIndexType(String indexType) {
        this.indexType = indexType;
    }

    public String getSysOrderNo() {
        return sysOrderNo;
    }

    public void setSysOrderNo(String sysOrderNo) {
        this.sysOrderNo = sysOrderNo;
    }

    public String getCloseType() {
        return closeType;
    }

    public void setCloseType(String closeType) {
        this.closeType = closeType;
    }
}
