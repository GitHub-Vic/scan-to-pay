package co.intella.domain.sp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class SpSingleQueryRequestData extends SpBasicRequestData{

    @SerializedName("mchId")
    @JsonProperty(value = "MerchantID", required = true)
    private String mchId;

    @SerializedName("RespondType")
    @JsonProperty(value = "RespondType", required = true)
    private String respondType;

    @SerializedName(value = "CreateTime")
    @JsonProperty(value = "TimeStamp", required = true)
    private String createTime;

    @SerializedName(value = "ver")
    @JsonProperty(value = "Version", required = true)
    private String ver;

    @SerializedName(value = "CheckValue")
    @JsonProperty(value = "CheckValue", required = true)
    private String checkValue;

    @SerializedName(value = "StoreOrderNo")
    @JsonProperty(value = "MerchantOrderNo")
    private String storeOrderNo;

    @SerializedName(value = "TotalFee")
    @JsonProperty(value = "Amt", required = true)
    private String totalFee;

    public String getRespondType() {
        return respondType;
    }

    public void setRespondType(String respondType) {
        this.respondType = respondType;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public String getCheckValue() {
        return checkValue;
    }

    public void setCheckValue(String checkValue) {
        this.checkValue = checkValue;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }
}
