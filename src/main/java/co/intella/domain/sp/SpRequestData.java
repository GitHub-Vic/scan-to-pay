package co.intella.domain.sp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class SpRequestData extends SpBasicRequestData{

    @SerializedName(value = "MerchantID_")
    @JsonProperty(value = "MerchantID_")
    private String mchId;

    @SerializedName(value = "PostData_")
    @JsonProperty(value = "PostData_")
    private String postData;

    @SerializedName(value = "Pos_")
    @JsonProperty(value = "Pos_")
    private String pos;  // response format : "String" or "Json"

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getPostData() {
        return postData;
    }

    public void setPostData(String postData) {
        this.postData = postData;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }
}
