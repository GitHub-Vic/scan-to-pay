package co.intella.domain.sp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class SpPaymentRequestData extends SpBasicRequestData{

    @SerializedName(value = "CreateTime")
    @JsonProperty(value = "TimeStamp", required = true)
    private String createTime;

    @SerializedName(value = "ver")
    @JsonProperty(value = "Version", required = true)
    private String ver;

    @SerializedName(value = "StoreOrderNo")
    @JsonProperty(value = "MerchantOrderNo", required = true)
    private String storeOrderNo;

    @SerializedName(value = "TotalFee")
    @JsonProperty(value = "Amt", required = true)
    private String totalFee;

    @SerializedName(value = "Body")
    @JsonProperty(value = "ProdDesc", required = true)
    private String body;

    @SerializedName(value = "Email")
    @JsonProperty(value = "PayerEmail", required = true)
    private String email;

    @SerializedName(value = "CreditPayWay")
    @JsonProperty(value = "Inst")
    private String creditPayWay;

    @SerializedName(value = "CardId")
    @JsonProperty(value = "CardNo")
    private String cardId;

    @SerializedName(value = "ExpireDate")
    @JsonProperty(value = "Exp")
    private String expireDate;

    @SerializedName(value = "ExtenNo")
    @JsonProperty(value = "CVC")
    private String extenNo;

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreditPayWay() {
        return creditPayWay;
    }

    public void setCreditPayWay(String creditPayWay) {
        this.creditPayWay = creditPayWay;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getExtenNo() {
        return extenNo;
    }

    public void setExtenNo(String extenNo) {
        this.extenNo = extenNo;
    }
}
