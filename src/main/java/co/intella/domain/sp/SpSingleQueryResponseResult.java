package co.intella.domain.sp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class SpSingleQueryResponseResult {

    @SerializedName("MerchantID")
    @JsonProperty(value = "MerchantID", required = true)
    private String mchId;

    @SerializedName(value = "Amt")
    @JsonProperty(value = "Amt", required = true)
    private Integer totalFee;

    @SerializedName(value = "TradeNo")
    @JsonProperty(value = "TradeNo", required = true)
    private String sysOrderNo;

    @SerializedName(value = "MerchantOrderNo")
    @JsonProperty(value = "MerchantOrderNo", required = true)
    private String storeOrderNo;

    @SerializedName(value = "TradeStatus")
    @JsonProperty(value = "TradeStatus", required = true)
    private String tradeStatus;

    @SerializedName(value = "PaymentType")
    @JsonProperty(value = "PaymentType", required = true)
    private String paymentType;

    @SerializedName(value = "CreateTime")
    @JsonProperty(value = "CreateTime", required = true)
    private String createTime;

    @SerializedName(value = "PayTime")
    @JsonProperty(value = "PayTime", required = true)
    private String payTime;

    @SerializedName(value = "CheckCode")
    @JsonProperty(value = "CheckCode", required = true)
    private String checkCode;

    @SerializedName(value = "FundTime")
    @JsonProperty(value = "FundTime", required = true)
    private String fundTime;

    @SerializedName(value = "RespondCode")
    @JsonProperty(value = "RespondCode", required = true)
    private String respondCode;

    @SerializedName(value = "Auth")
    @JsonProperty(value = "Auth", required = true)
    private String authCode;

    @SerializedName(value = "ECI")
    @JsonProperty(value = "ECI", required = true)
    private String eCI;

    @SerializedName(value = "CloseAmt")
    @JsonProperty(value = "CloseAmt", required = true)
    private String closeAmt;

    @SerializedName(value = "CloseStatus")
    @JsonProperty(value = "CloseStatus", required = true)
    private String closeStatus;

    @SerializedName(value = "BackBalance")
    @JsonProperty(value = "BackBalance", required = true)
    private String backBalance;

    @SerializedName(value = "BackStatus")
    @JsonProperty(value = "BackStatus", required = true)
    private String backStatus;

    @SerializedName(value = "RespondMsg")
    @JsonProperty(value = "RespondMsg", required = true)
    private String respondMsg;

    @SerializedName(value = "Inst")
    @JsonProperty(value = "Inst", required = true)
    private String inst;

    @SerializedName(value = "InstFirst")
    @JsonProperty(value = "InstFirst", required = true)
    private String instFirst;

    @SerializedName(value = "InstEach")
    @JsonProperty(value = "InstEach", required = true)
    private String instEach;

    @SerializedName(value = "Bonus")
    @JsonProperty(value = "Bonus", required = true)
    private String bonus;

    @SerializedName(value = "RedAmt")
    @JsonProperty(value = "RedAmt", required = true)
    private String redAmt;

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public Integer getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(Integer totalFee) {
        this.totalFee = totalFee;
    }

    public String getSysOrderNo() {
        return sysOrderNo;
    }

    public void setSysOrderNo(String sysOrderNo) {
        this.sysOrderNo = sysOrderNo;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    public String getCheckCode() {
        return checkCode;
    }

    public void setCheckCode(String checkCode) {
        this.checkCode = checkCode;
    }

    public String getFundTime() {
        return fundTime;
    }

    public void setFundTime(String fundTime) {
        this.fundTime = fundTime;
    }

    public String getRespondCode() {
        return respondCode;
    }

    public void setRespondCode(String respondCode) {
        this.respondCode = respondCode;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String geteCI() {
        return eCI;
    }

    public void seteCI(String eCI) {
        this.eCI = eCI;
    }

    public String getCloseAmt() {
        return closeAmt;
    }

    public void setCloseAmt(String closeAmt) {
        this.closeAmt = closeAmt;
    }

    public String getCloseStatus() {
        return closeStatus;
    }

    public void setCloseStatus(String closeStatus) {
        this.closeStatus = closeStatus;
    }

    public String getBackBalance() {
        return backBalance;
    }

    public void setBackBalance(String backBalance) {
        this.backBalance = backBalance;
    }

    public String getBackStatus() {
        return backStatus;
    }

    public void setBackStatus(String backStatus) {
        this.backStatus = backStatus;
    }

    public String getRespondMsg() {
        return respondMsg;
    }

    public void setRespondMsg(String respondMsg) {
        this.respondMsg = respondMsg;
    }

    public String getInst() {
        return inst;
    }

    public void setInst(String inst) {
        this.inst = inst;
    }

    public String getInstFirst() {
        return instFirst;
    }

    public void setInstFirst(String instFirst) {
        this.instFirst = instFirst;
    }

    public String getInstEach() {
        return instEach;
    }

    public void setInstEach(String instEach) {
        this.instEach = instEach;
    }

    public String getBonus() {
        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }

    public String getRedAmt() {
        return redAmt;
    }

    public void setRedAmt(String redAmt) {
        this.redAmt = redAmt;
    }
}
