package co.intella.domain.sp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class SpSingleQueryResponseData {
    @SerializedName(value = "Status")
    @JsonProperty(value = "Status")
    private String status;

    @SerializedName(value = "Message")
    @JsonProperty(value = "Message")
    private String message;

    @SerializedName(value = "Result")
    @JsonProperty(value = "Result")
    private SpSingleQueryResponseResult result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SpSingleQueryResponseResult getResult() {
        return result;
    }

    public void setResult(SpSingleQueryResponseResult result) {
        this.result = result;
    }
}
