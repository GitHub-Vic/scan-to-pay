package co.intella.domain.sp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class SpRefundAndCaptureResponseResult {
    @SerializedName("MerchantID")
    @JsonProperty(value = "MerchantID", required = true)
    private String mchId;

    @SerializedName(value = "Amt")
    @JsonProperty(value = "Amt", required = true)
    private Integer totalFee;

    @SerializedName(value = "TradeNo")
    @JsonProperty(value = "TradeNo", required = true)
    private String sysOrderNo;

    @SerializedName(value = "MerchantOrderNo")
    @JsonProperty(value = "MerchantOrderNo", required = true)
    private String storeOrderNo;

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public Integer getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(Integer totalFee) {
        this.totalFee = totalFee;
    }

    public String getSysOrderNo() {
        return sysOrderNo;
    }

    public void setSysOrderNo(String sysOrderNo) {
        this.sysOrderNo = sysOrderNo;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }
}
