package co.intella.domain.sp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author Andy Lin
 */
public class SpPaymentResponseResult {
    @SerializedName("MerchantID")
    @JsonProperty(value = "MerchantID", required = true)
    private String mchId;

    @SerializedName(value = "Amt")
    @JsonProperty(value = "Amt", required = true)
    private Integer totalFee;

    @SerializedName(value = "TradeNo")
    @JsonProperty(value = "TradeNo", required = true)
    private String sysOrderNo;

    @SerializedName(value = "MerchantOrderNo")
    @JsonProperty(value = "MerchantOrderNo", required = true)
    private String storeOrderNo;

    @SerializedName(value = "RespondCode")
    @JsonProperty(value = "RespondCode", required = true)
    private String respondCode;

    @SerializedName(value = "Auth")
    @JsonProperty(value = "Auth", required = true)
    private String approveCode;

    @SerializedName(value = "AuthDate")
    @JsonProperty(value = "AuthDate", required = true)
    private String authDate;

    @SerializedName(value = "AuthTime")
    @JsonProperty(value = "AuthTime", required = true)
    private String authTime;

    @SerializedName(value = "Card6No")
    @JsonProperty(value = "Card6No")
    private String card6No;

    @SerializedName(value = "Card4No")
    @JsonProperty(value = "Card4No")
    private String card4No;

    @SerializedName(value = "Exp")
    @JsonProperty(value = "Exp")
    private String expireDate;

    @SerializedName(value = "Inst")
    @JsonProperty(value = "Inst")
    private int inst;

    @SerializedName(value = "InstFirst")
    @JsonProperty(value = "InstFirst")
    private int instFirst;

    @SerializedName(value = "InstEach")
    @JsonProperty(value = "InstEach")
    private int instEach;

    @SerializedName(value = "ECI")
    @JsonProperty(value = "ECI")
    private String eci;

    @SerializedName(value = "IP")
    @JsonProperty(value = "IP")
    private String ip;

    @SerializedName(value = "EscrowBank")
    @JsonProperty(value = "EscrowBank")
    private String escrowBank;

    @SerializedName(value = "CheckCode")
    @JsonProperty(value = "CheckCode")
    private String checkCode;

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public Integer getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(Integer totalFee) {
        this.totalFee = totalFee;
    }

    public String getSysOrderNo() {
        return sysOrderNo;
    }

    public void setSysOrderNo(String sysOrderNo) {
        this.sysOrderNo = sysOrderNo;
    }

    public String getStoreOrderNo() {
        return storeOrderNo;
    }

    public void setStoreOrderNo(String storeOrderNo) {
        this.storeOrderNo = storeOrderNo;
    }

    public String getRespondCode() {
        return respondCode;
    }

    public void setRespondCode(String respondCode) {
        this.respondCode = respondCode;
    }

    public String getApproveCode() {
        return approveCode;
    }

    public void setApproveCode(String approveCode) {
        this.approveCode = approveCode;
    }

    public String getAuthDate() {
        return authDate;
    }

    public void setAuthDate(String authDate) {
        this.authDate = authDate;
    }

    public String getAuthTime() {
        return authTime;
    }

    public void setAuthTime(String authTime) {
        this.authTime = authTime;
    }

    public String getCard6No() {
        return card6No;
    }

    public void setCard6No(String card6No) {
        this.card6No = card6No;
    }

    public String getCard4No() {
        return card4No;
    }

    public void setCard4No(String card4No) {
        this.card4No = card4No;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public int getInst() {
        return inst;
    }

    public void setInst(int inst) {
        this.inst = inst;
    }

    public int getInstFirst() {
        return instFirst;
    }

    public void setInstFirst(int instFirst) {
        this.instFirst = instFirst;
    }

    public int getInstEach() {
        return instEach;
    }

    public void setInstEach(int instEach) {
        this.instEach = instEach;
    }

    public String getEci() {
        return eci;
    }

    public void setEci(String eci) {
        this.eci = eci;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getEscrowBank() {
        return escrowBank;
    }

    public void setEscrowBank(String escrowBank) {
        this.escrowBank = escrowBank;
    }

    public String getCheckCode() {
        return checkCode;
    }

    public void setCheckCode(String checkCode) {
        this.checkCode = checkCode;
    }
}
