package co.intella.domain.iCashPay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class IcpRequestEncData {
    @JsonProperty("PlatformID")
    @SerializedName("PlatformID")
    private  String platformID;
    @JsonProperty("MerchantID")
    @SerializedName("MerchantID")
    private  String merchantID;
    @JsonProperty("WalletID")
    @SerializedName("WalletID")
    private  String walletID;
    @JsonProperty("MerchantTradeNo")
    @SerializedName("MerchantTradeNo")
    private  String merchantTradeNo;
    @JsonProperty("StoreID")
    @SerializedName("StoreID")
    private  String storeID;
    @JsonProperty("TradeTool")
    @SerializedName("TradeTool")
    private  String tradeTool;
    @JsonProperty("MerchantTradeDate")
    @SerializedName("MerchantTradeDate")
    private  String merchantTradeDate;
    @JsonProperty("Amount")
    @SerializedName("Amount")
    private  String amount;
    @JsonProperty("ItemNo")
    @SerializedName("ItemNo")
    private  String itemNo;
    @JsonProperty("ItemName")
    @SerializedName("ItemName")
    private  String itemName;
    @JsonProperty("Quantity")
    @SerializedName("Quantity")
    private  String quantity;
    @JsonProperty("ReturnURL")
    @SerializedName("ReturnURL")
    private  String returnURL;
    @JsonProperty("Remark")
    @SerializedName("Remark")
    private  String remark;
    @JsonProperty("CustomField1")
    @SerializedName("CustomField1")
    private  String customField1;
    @JsonProperty("CustomField2")
    @SerializedName("CustomField2")
    private  String customField2;
    @JsonProperty("CustomField3")
    @SerializedName("CustomField3")
    private  String customField3;
    @JsonProperty("StoreName")
    @SerializedName("StoreName")
    private  String storeName;
    @JsonProperty("CarrierType")
    @SerializedName("CarrierType")
    private  String carrierType;
    @JsonProperty("ItemAmt")
    @SerializedName("ItemAmt")
    private  String itemAmt;
    @JsonProperty("UtilityAmt")
    @SerializedName("UtilityAmt")
    private  String utilityAmt;
    @JsonProperty("CommAmt")
    @SerializedName("CommAmt")
    private  String commAmt;
    @JsonProperty("ExceptAmt1")
    @SerializedName("ExceptAmt1")
    private  String exceptAmt1;
    @JsonProperty("ExceptAmt2")
    @SerializedName("ExceptAmt2")
    private  String exceptAmt2;
    @JsonProperty("RedeemFlag")
    @SerializedName("RedeemFlag")
    private  String redeemFlag;
    @JsonProperty("BonusAmt")
    @SerializedName("BonusAmt")
    private  String bonusAmt;
    @JsonProperty("DebitPoint")
    @SerializedName("DebitPoint")
    private  String debitPoint;
    @JsonProperty("NonRedeemAmt")
    @SerializedName("NonRedeemAmt")
    private  String nonRedeemAmt;
    @JsonProperty("NonPointAmt")
    @SerializedName("NonPointAmt")
    private  String nonPointAmt;
    @JsonProperty("Invo_no")
    @SerializedName("Invo_no")
    private  String invo_no;
    @JsonProperty("DiscountUnit")
    @SerializedName("DiscountUnit")
    private  String discountUnit;
    @JsonProperty("DiscountType")
    @SerializedName("DiscountType")
    private  String discountType;
    @JsonProperty("DiscountAmt")
    @SerializedName("DiscountAmt")
    private  String discountAmt;
    @JsonProperty("DiscountFee")
    @SerializedName("DiscountFee")
    private  String discountFee;
    @JsonProperty("FeeAmt")
    @SerializedName("FeeAmt")
    private  String feeAmt;
    @JsonProperty("BillNo")
    @SerializedName("BillNo")
    private  String billNo;
    @JsonProperty("BillItem")
    @SerializedName("BillItem")
    private  String billItem;
    @JsonProperty("BillAmt")
    @SerializedName("BillAmt")
    private  String billAmt;
    @JsonProperty("BillFee")
    @SerializedName("BillFee")
    private  String billFee;
    @JsonProperty("BillBarcode")
    @SerializedName("BillBarcode")
    private  String billBarcode;

    public IcpRequestEncData() {
        this.utilityAmt = "0";//代收交易金額
        this.commAmt = "0";//代售交易金額
        this.exceptAmt1 = "0";//排他一交易金額(菸品)
        this.exceptAmt2 = "0";//排他二交易金額(預留)
        this.redeemFlag = "0";//點抵金開關 0/1
        this.bonusAmt = "0";//紅利折抵金額，若無點數折抵金額則為0
        this.debitPoint = "0";//紅利折抵點數，若無點數折抵金額則為0
        this.nonRedeemAmt = "0";//不可折抵金額
        this.nonPointAmt = "0";//不可贈點金額
        this.discountUnit = "1";//折扣單位
        this.discountType = "S";//折扣筆數
        this.discountAmt = "0";//折扣金(本金)
        this.discountFee = "0";//折扣金額(手續費)
        this.billNo = "";//帳單編號
        this.billItem = "";//繳費項目
        this.billAmt = "0";//繳費金額
        this.billFee = "0";//手續費(外加)
    }

    public String getPlatformID() {
        return platformID;
    }

    public void setPlatformID(String platformID) {
        this.platformID = platformID;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getWalletID() {
        return walletID;
    }

    public void setWalletID(String walletID) {
        this.walletID = walletID;
    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getStoreID() {
        return storeID;
    }

    public void setStoreID(String storeID) {
        this.storeID = storeID;
    }

    public String getTradeTool() {
        return tradeTool;
    }

    public void setTradeTool(String tradeTool) {
        this.tradeTool = tradeTool;
    }

    public String getMerchantTradeDate() {
        return merchantTradeDate;
    }

    public void setMerchantTradeDate(String merchantTradeDate) {
        this.merchantTradeDate = merchantTradeDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getItemNo() {
        return itemNo;
    }

    public void setItemNo(String itemNo) {
        this.itemNo = itemNo;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getReturnURL() {
        return returnURL;
    }

    public void setReturnURL(String returnURL) {
        this.returnURL = returnURL;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getCarrierType() {
        return carrierType;
    }

    public void setCarrierType(String carrierType) {
        this.carrierType = carrierType;
    }

    public String getItemAmt() {
        return itemAmt;
    }

    public void setItemAmt(String itemAmt) {
        this.itemAmt = itemAmt;
    }

    public String getUtilityAmt() {
        return utilityAmt;
    }

    public void setUtilityAmt(String utilityAmt) {
        this.utilityAmt = utilityAmt;
    }

    public String getCommAmt() {
        return commAmt;
    }

    public void setCommAmt(String commAmt) {
        this.commAmt = commAmt;
    }

    public String getExceptAmt1() {
        return exceptAmt1;
    }

    public void setExceptAmt1(String exceptAmt1) {
        this.exceptAmt1 = exceptAmt1;
    }

    public String getExceptAmt2() {
        return exceptAmt2;
    }

    public void setExceptAmt2(String exceptAmt2) {
        this.exceptAmt2 = exceptAmt2;
    }

    public String getRedeemFlag() {
        return redeemFlag;
    }

    public void setRedeemFlag(String redeemFlag) {
        this.redeemFlag = redeemFlag;
    }

    public String getBonusAmt() {
        return bonusAmt;
    }

    public void setBonusAmt(String bonusAmt) {
        this.bonusAmt = bonusAmt;
    }

    public String getDebitPoint() {
        return debitPoint;
    }

    public void setDebitPoint(String debitPoint) {
        this.debitPoint = debitPoint;
    }

    public String getNonRedeemAmt() {
        return nonRedeemAmt;
    }

    public void setNonRedeemAmt(String nonRedeemAmt) {
        this.nonRedeemAmt = nonRedeemAmt;
    }

    public String getNonPointAmt() {
        return nonPointAmt;
    }

    public void setNonPointAmt(String nonPointAmt) {
        this.nonPointAmt = nonPointAmt;
    }

    public String getInvo_no() {
        return invo_no;
    }

    public void setInvo_no(String invo_no) {
        this.invo_no = invo_no;
    }

    public String getDiscountUnit() {
        return discountUnit;
    }

    public void setDiscountUnit(String discountUnit) {
        this.discountUnit = discountUnit;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(String discountAmt) {
        this.discountAmt = discountAmt;
    }

    public String getDiscountFee() {
        return discountFee;
    }

    public void setDiscountFee(String discountFee) {
        this.discountFee = discountFee;
    }

    public String getFeeAmt() {
        return feeAmt;
    }

    public void setFeeAmt(String feeAmt) {
        this.feeAmt = feeAmt;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getBillItem() {
        return billItem;
    }

    public void setBillItem(String billItem) {
        this.billItem = billItem;
    }

    public String getBillAmt() {
        return billAmt;
    }

    public void setBillAmt(String billAmt) {
        this.billAmt = billAmt;
    }

    public String getBillFee() {
        return billFee;
    }

    public void setBillFee(String billFee) {
        this.billFee = billFee;
    }

    public String getBillBarcode() {
        return billBarcode;
    }

    public void setBillBarcode(String billBarcode) {
        this.billBarcode = billBarcode;
    }
}
