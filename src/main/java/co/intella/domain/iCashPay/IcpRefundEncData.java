package co.intella.domain.iCashPay;

import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class IcpRefundEncData {
    @JsonProperty("PlatformID")
    @SerializedName("PlatformID")
    private String platformID;
    @JsonProperty("MerchantID")
    @SerializedName("MerchantID")
    private String merchantID;
    @JsonProperty("WalletID")
    @SerializedName("WalletID")
    private String walletID;
    @JsonProperty("TransactionID")
    @SerializedName("TransactionID")
    private String transactionID;
    @JsonProperty("Amount")
    @SerializedName("Amount")
    private String amount;
    @JsonProperty("StoreID")
    @SerializedName("StoreID")
    private String storeID;
    @JsonProperty("StoreName")
    @SerializedName("StoreName")
    private String storeName;
    @JsonProperty("MerchantTradeNo")
    @SerializedName("MerchantTradeNo")
    private String merchantTradeNo;
    @JsonProperty("OMerchantTradeNo")
    @SerializedName("OMerchantTradeNo")
    private String oMerchantTradeNo;
    @JsonProperty("MerchantTradeDate")
    @SerializedName("MerchantTradeDate")
    private String merchantTradeDate;
    @JsonProperty("BonusAmt")
    @SerializedName("BonusAmt")
    private String bonusAmt;
    @JsonProperty("DebitPoint")
    @SerializedName("DebitPoint")
    private String debitPoint;
    @JsonProperty("DiscountUnit")
    @SerializedName("DiscountUnit")
    private String discountUnit;
    @JsonProperty("DiscountType")
    @SerializedName("DiscountType")
    private String discountType;
    @JsonProperty("DiscountAmt")
    @SerializedName("DiscountAmt")
    private String discountAmt;
    @JsonProperty("DiscountFee")
    @SerializedName("DiscountFee")
    private String discountFee;
    @JsonProperty("FeeAmt")
    @SerializedName("FeeAmt")
    private String feeAmt;
    @JsonProperty("BillNo")
    @SerializedName("BillNo")
    private String billNo;
    @JsonProperty("BillItem")
    @SerializedName("BillItem")
    private String billItem;
    @JsonProperty("BillAmt")
    @SerializedName("BillAmt")
    private String billAmt;
    @JsonProperty("BillFee")
    @SerializedName("BillFee")
    private String billFee;
    @JsonProperty("BillBarcode")
    @SerializedName("BillBarcode")
    private String billBarcode;
    @JsonProperty("DiscountList")
    @SerializedName("DiscountList")
    private String[] discountList;
    @JsonProperty("BillList")
    @SerializedName("BillList")
    private String[] billList;

    public IcpRefundEncData(PaymentAccount paymentAccount, TradeDetail tradeDetail) {
    this.platformID = paymentAccount.getAccount();
    this.merchantID = paymentAccount.getAccount();
    this.transactionID = tradeDetail.getSystemOrderId();
    this.amount = String.valueOf(tradeDetail.getPayment());
    this.merchantTradeNo = tradeDetail.getStoreRefundId();
    this.oMerchantTradeNo = tradeDetail.getOrderId();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    this.merchantTradeDate = sdf.format(new Date());
    this.bonusAmt = "0";
    this.debitPoint = "0";
//    this.discountUnit = "1";
//    this.discountType = "S";
//    this.discountAmt = "0";
//    this.discountFee = "0";
//    this.billNo = "";
//    this.billItem = "";
//    this.billAmt = "";
//    this.billFee = "0";
    this.discountList = new String[0];
    this.billList = new String[0];


    }

    public String getPlatformID() {
        return platformID;
    }

    public void setPlatformID(String platformID) {
        this.platformID = platformID;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getWalletID() {
        return walletID;
    }

    public void setWalletID(String walletID) {
        this.walletID = walletID;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStoreID() {
        return storeID;
    }

    public void setStoreID(String storeID) {
        this.storeID = storeID;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getoMerchantTradeNo() {
        return oMerchantTradeNo;
    }

    public void setoMerchantTradeNo(String oMerchantTradeNo) {
        this.oMerchantTradeNo = oMerchantTradeNo;
    }

    public String getMerchantTradeDate() {
        return merchantTradeDate;
    }

    public void setMerchantTradeDate(String merchantTradeDate) {
        this.merchantTradeDate = merchantTradeDate;
    }

    public String getBonusAmt() {
        return bonusAmt;
    }

    public void setBonusAmt(String bonusAmt) {
        this.bonusAmt = bonusAmt;
    }

    public String getDebitPoint() {
        return debitPoint;
    }

    public void setDebitPoint(String debitPoint) {
        this.debitPoint = debitPoint;
    }

    public String getDiscountUnit() {
        return discountUnit;
    }

    public void setDiscountUnit(String discountUnit) {
        this.discountUnit = discountUnit;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(String discountAmt) {
        this.discountAmt = discountAmt;
    }

    public String getDiscountFee() {
        return discountFee;
    }

    public void setDiscountFee(String discountFee) {
        this.discountFee = discountFee;
    }

    public String getFeeAmt() {
        return feeAmt;
    }

    public void setFeeAmt(String feeAmt) {
        this.feeAmt = feeAmt;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getBillItem() {
        return billItem;
    }

    public void setBillItem(String billItem) {
        this.billItem = billItem;
    }

    public String getBillAmt() {
        return billAmt;
    }

    public void setBillAmt(String billAmt) {
        this.billAmt = billAmt;
    }

    public String getBillFee() {
        return billFee;
    }

    public void setBillFee(String billFee) {
        this.billFee = billFee;
    }

    public String getBillBarcode() {
        return billBarcode;
    }

    public void setBillBarcode(String billBarcode) {
        this.billBarcode = billBarcode;
    }

    public String[] getDiscountList() {
        return discountList;
    }

    public void setDiscountList(String[] discountList) {
        this.discountList = discountList;
    }

    public String[] getBillList() {
        return billList;
    }

    public void setBillList(String[] billList) {
        this.billList = billList;
    }
}
