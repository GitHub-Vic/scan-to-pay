package co.intella.domain.iCashPay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class IcpQueryResponseEncData {
    @JsonProperty("PlatformID")
    @SerializedName("PlatformID")
    private String platformID;
    @JsonProperty("MerchantID")
    @SerializedName("MerchantID")
    private String merchantID;
    @JsonProperty("TransactionID")
    @SerializedName("TransactionID")
    private String transactionID;
    @JsonProperty("IcashAccount")
    @SerializedName("IcashAccount")
    private String icashAccount;
    @JsonProperty("TotalAmount")
    @SerializedName("TotalAmount")
    private String totalAmount;
    @JsonProperty("IcpAmount")
    @SerializedName("IcpAmount")
    private String icpAmount;
    @JsonProperty("PaymentDate")
    @SerializedName("PaymentDate")
    private String paymentDate;
    @JsonProperty("PaymentType")
    @SerializedName("PaymentType")
    private String paymentType;
    @JsonProperty("IcashAccountPayID")
    @SerializedName("IcashAccountPayID")
    private String icashAccountPayID;
    @JsonProperty("BankNo")
    @SerializedName("BankNo")
    private String bankNo;
    @JsonProperty("BankName")
    @SerializedName("BankName")
    private String bankName;
    @JsonProperty("TradeType")
    @SerializedName("TradeType")
    private String tradeType;
    @JsonProperty("TradeStatus")
    @SerializedName("TradeStatus")
    private String tradeStatus;
    @JsonProperty("AllocateStatus")
    @SerializedName("AllocateStatus")
    private String allocateStatus;
    @JsonProperty("BonusAmt")
    @SerializedName("BonusAmt")
    private String bonusAmt;
    @JsonProperty("DebitPoint")
    @SerializedName("DebitPoint")
    private String debitPoint;
    @JsonProperty("MaskedPan")
    @SerializedName("MaskedPan")
    private String maskedPan;
    @JsonProperty("VToken")
    @SerializedName("VToken")
    private String vToken;

    public String getPlatformID() {
        return platformID;
    }

    public void setPlatformID(String platformID) {
        this.platformID = platformID;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getIcashAccount() {
        return icashAccount;
    }

    public void setIcashAccount(String icashAccount) {
        this.icashAccount = icashAccount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getIcpAmount() {
        return icpAmount;
    }

    public void setIcpAmount(String icpAmount) {
        this.icpAmount = icpAmount;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getIcashAccountPayID() {
        return icashAccountPayID;
    }

    public void setIcashAccountPayID(String icashAccountPayID) {
        this.icashAccountPayID = icashAccountPayID;
    }

    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    public String getAllocateStatus() {
        return allocateStatus;
    }

    public void setAllocateStatus(String allocateStatus) {
        this.allocateStatus = allocateStatus;
    }

    public String getBonusAmt() {
        return bonusAmt;
    }

    public void setBonusAmt(String bonusAmt) {
        this.bonusAmt = bonusAmt;
    }

    public String getDebitPoint() {
        return debitPoint;
    }

    public void setDebitPoint(String debitPoint) {
        this.debitPoint = debitPoint;
    }

    public String getMaskedPan() {
        return maskedPan;
    }

    public void setMaskedPan(String maskedPan) {
        this.maskedPan = maskedPan;
    }

    public String getvToken() {
        return vToken;
    }

    public void setvToken(String vToken) {
        this.vToken = vToken;
    }
}
