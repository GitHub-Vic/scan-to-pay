package co.intella.domain.iCashPay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class IcpResponseEncData {
    @JsonProperty("PlatformID")
    @SerializedName("PlatformID")
    private  String platformID;
    @JsonProperty("MerchantID")
    @SerializedName("MerchantID")
    private  String merchantID;
    @JsonProperty("APPID")
    @SerializedName("APPID")
    private  String aPPID;
    @JsonProperty("MerchantTradeNo")
    @SerializedName("MerchantTradeNo")
    private  String merchantTradeNo;
    @JsonProperty("StoreID")
    @SerializedName("StoreID")
    private  String storeID;
    @JsonProperty("ExpiredTime")
    @SerializedName("ExpiredTime")
    private  String expiredTime;
    @JsonProperty("TradeToken")
    @SerializedName("TradeToken")
    private  String tradeToken;

    public String getPlatformID() {
        return platformID;
    }

    public void setPlatformID(String platformID) {
        this.platformID = platformID;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getaPPID() {
        return aPPID;
    }

    public void setaPPID(String aPPID) {
        this.aPPID = aPPID;
    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getStoreID() {
        return storeID;
    }

    public void setStoreID(String storeID) {
        this.storeID = storeID;
    }

    public String getExpiredTime() {
        return expiredTime;
    }

    public void setExpiredTime(String expiredTime) {
        this.expiredTime = expiredTime;
    }

    public String getTradeToken() {
        return tradeToken;
    }

    public void setTradeToken(String tradeToken) {
        this.tradeToken = tradeToken;
    }
}
