package co.intella.domain.iCashPay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class IcpNotifyEncData {
    @JsonProperty("MerchantID")
    @SerializedName("MerchantID")
    private String merchantID;
    @JsonProperty("MerchantTradeNo")
    @SerializedName("MerchantTradeNo")
    private String merchantTradeNo;
    @JsonProperty("StoreID")
    @SerializedName("StoreID")
    private String storeID;
    @JsonProperty("TransactionID")
    @SerializedName("TransactionID")
    private String transactionID;
    @JsonProperty("TradeAmount")
    @SerializedName("TradeAmount")
    private long tradeAmount;
    @JsonProperty("PaymentDate")
    @SerializedName("PaymentDate")
    private String paymentDate;
    @JsonProperty("PaymentType")
    @SerializedName("PaymentType")
    private String paymentType;
    @JsonProperty("TradeDate")
    @SerializedName("TradeDate")
    private String tradeDate;
    @JsonProperty("CarrierType")
    @SerializedName("CarrierType")
    private String carrierType;
    @JsonProperty("CarrierNum")
    @SerializedName("CarrierNum")
    private String carrierNum;
    @JsonProperty("RtnCode")
    @SerializedName("RtnCode")
    private int rtnCode;
    @JsonProperty("RtnMsg")
    @SerializedName("RtnMsg")
    private String rtnMsg;

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getStoreID() {
        return storeID;
    }

    public void setStoreID(String storeID) {
        this.storeID = storeID;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public long getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(long tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }

    public String getCarrierType() {
        return carrierType;
    }

    public void setCarrierType(String carrierType) {
        this.carrierType = carrierType;
    }

    public String getCarrierNum() {
        return carrierNum;
    }

    public void setCarrierNum(String carrierNum) {
        this.carrierNum = carrierNum;
    }

    public int getRtnCode() {
        return rtnCode;
    }

    public void setRtnCode(int rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }
}
