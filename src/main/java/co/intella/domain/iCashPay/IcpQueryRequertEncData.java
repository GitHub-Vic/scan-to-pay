package co.intella.domain.iCashPay;

import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class IcpQueryRequertEncData {
    @JsonProperty("PlatformID")
    @SerializedName("PlatformID")
    private String platformID;
    @JsonProperty("MerchantID")
    @SerializedName("MerchantID")
    private String merchantID;
    @JsonProperty("WalletID")
    @SerializedName("WalletID")
    private String walletID;
    @JsonProperty("MerchantTradeNo")
    @SerializedName("MerchantTradeNo")
    private String merchantTradeNo;

    public IcpQueryRequertEncData(PaymentAccount paymentAccount, TradeDetail tradeDetail) {
    this.merchantID = paymentAccount.getAccount();
    this.platformID = paymentAccount.getAccount();
    this.merchantTradeNo = tradeDetail.getOrderId();
    }

    public String getPlatformID() {
        return platformID;
    }

    public void setPlatformID(String platformID) {
        this.platformID = platformID;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getWalletID() {
        return walletID;
    }

    public void setWalletID(String walletID) {
        this.walletID = walletID;
    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }
}
