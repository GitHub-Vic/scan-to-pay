package co.intella.domain.iCashPay;

import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class IcpMicRequestEncData {

    @JsonProperty("PlatformID")
    @SerializedName("PlatformID")
    private  String platformID;
    @JsonProperty("MerchantID")
    @SerializedName("MerchantID")
    private  String merchantID;
    @JsonProperty("WalletID")
    @SerializedName("WalletID")
    private  String walletID;
    @JsonProperty("MerchantTradeNo")
    @SerializedName("MerchantTradeNo")
    private  String merchantTradeNo;
    @JsonProperty("StoreID")
    @SerializedName("StoreID")
    private  String storeID;
    @JsonProperty("StoreName")
    @SerializedName("StoreName")
    private  String storeName;
    @JsonProperty("MerchantTID")
    @SerializedName("MerchantTID")
    private  String merchantTID;
    @JsonProperty("PosRefNo")
    @SerializedName("PosRefNo")
    private  String posRefNo;
    @JsonProperty("CarrierType")
    @SerializedName("CarrierType")
    private  String carrierType;
    @JsonProperty("MerchantTradeDate")
    @SerializedName("MerchantTradeDate")
    private  String merchantTradeDate;
    @JsonProperty("ItemAmt")
    @SerializedName("ItemAmt")
    private  String itemAmt;
    @JsonProperty("UtilityAmt")
    @SerializedName("UtilityAmt")
    private  String utilityAmt;
    @JsonProperty("CommAmt")
    @SerializedName("CommAmt")
    private  String commAmt;
    @JsonProperty("ExceptAmt1")
    @SerializedName("ExceptAmt1")
    private  String exceptAmt1;
    @JsonProperty("ExceptAmt2")
    @SerializedName("ExceptAmt2")
    private  String exceptAmt2;
    @JsonProperty("RedeemFlag")
    @SerializedName("RedeemFlag")
    private  String redeemFlag;
    @JsonProperty("BonusAmt")
    @SerializedName("BonusAmt")
    private  String bonusAmt;
    @JsonProperty("DebitPoint")
    @SerializedName("DebitPoint")
    private  String debitPoint;
    @JsonProperty("NonRedeemAmt")
    @SerializedName("NonRedeemAmt")
    private  String nonRedeemAmt;
    @JsonProperty("NonPointAmt")
    @SerializedName("NonPointAmt")
    private  String nonPointAmt;
//    @JsonProperty("ItemName")
//    @SerializedName("ItemName")
//    private  String itemName;
//    @JsonProperty("Quantity")
//    @SerializedName("Quantity")
//    private  String quantity;
//    @JsonProperty("Remark")
//    @SerializedName("Remark")
//    private  String remark;
    @JsonProperty("TradeDesc")
    @SerializedName("TradeDesc")
    private  String tradeDesc;
    @JsonProperty("ItemName")
    @SerializedName("ItemName")
    private  String itemName;
    @JsonProperty("ItemType")
    @SerializedName("ItemType")
    private  String itemType;
    @JsonProperty("ItemAmount")
    @SerializedName("ItemAmount")
    private  String itemAmount;
    @JsonProperty("ItemQty")
    @SerializedName("ItemQty")
    private  String itemQty;
    @JsonProperty("InvoiceNo")
    @SerializedName("InvoiceNo")
    private  String invoiceNo;
    @JsonProperty("Description")
    @SerializedName("Description")
    private  String description;
    @JsonProperty("CustomField1")
    @SerializedName("CustomField1")
    private  String customField1;
    @JsonProperty("CustomField2")
    @SerializedName("CustomField2")
    private  String customField2;
    @JsonProperty("CustomField3")
    @SerializedName("CustomField3")
    private  String customField3;
    @JsonProperty("Barcode")
    @SerializedName("Barcode")
    private  String barcode;
    @JsonProperty("Amount")
    @SerializedName("Amount")
    private  String amount;

    public IcpMicRequestEncData(PaymentAccount paymentAccount, TradeDetail tradeDetail) {
        this.platformID = paymentAccount.getAccount();
        this.merchantID = paymentAccount.getAccount();
        this.merchantTradeNo = tradeDetail.getOrderId();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        this.merchantTradeDate = sdf.format(new Date());
        this.itemAmt = String.valueOf(tradeDetail.getPayment()*100);//交易總金額包含 2 位小數點，所以要*100
        this.utilityAmt = "0";
        this.commAmt = "0";
        this.exceptAmt1 = "0";
        this.exceptAmt2 = "0";
        this.redeemFlag = "0";
        this.bonusAmt = "0";
        this.debitPoint = "0";
        this.nonRedeemAmt ="0";
        this.nonPointAmt = "0";
        //this.tradeDesc = tradeDetail.getDetail();
        this.itemName = tradeDetail.getDescription();
        this.itemAmount = String.valueOf(tradeDetail.getPayment());
        this.itemQty = "1";
        this.barcode = tradeDetail.getBarcode();
        this.amount = String.valueOf(tradeDetail.getPayment()*100);//交易總金額包含 2 位小數點，所以要*100


    }

    public String getPlatformID() {
        return platformID;
    }

    public void setPlatformID(String platformID) {
        this.platformID = platformID;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getWalletID() {
        return walletID;
    }

    public void setWalletID(String walletID) {
        this.walletID = walletID;
    }

    public String getMerchantTradeNo() {
        return merchantTradeNo;
    }

    public void setMerchantTradeNo(String merchantTradeNo) {
        this.merchantTradeNo = merchantTradeNo;
    }

    public String getStoreID() {
        return storeID;
    }

    public void setStoreID(String storeID) {
        this.storeID = storeID;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getMerchantTID() {
        return merchantTID;
    }

    public void setMerchantTID(String merchantTID) {
        this.merchantTID = merchantTID;
    }

    public String getPosRefNo() {
        return posRefNo;
    }

    public void setPosRefNo(String posRefNo) {
        this.posRefNo = posRefNo;
    }

    public String getCarrierType() {
        return carrierType;
    }

    public void setCarrierType(String carrierType) {
        this.carrierType = carrierType;
    }

    public String getMerchantTradeDate() {
        return merchantTradeDate;
    }

    public void setMerchantTradeDate(String merchantTradeDate) {
        this.merchantTradeDate = merchantTradeDate;
    }

    public String getItemAmt() {
        return itemAmt;
    }

    public void setItemAmt(String itemAmt) {
        this.itemAmt = itemAmt;
    }

    public String getUtilityAmt() {
        return utilityAmt;
    }

    public void setUtilityAmt(String utilityAmt) {
        this.utilityAmt = utilityAmt;
    }

    public String getCommAmt() {
        return commAmt;
    }

    public void setCommAmt(String commAmt) {
        this.commAmt = commAmt;
    }

    public String getExceptAmt1() {
        return exceptAmt1;
    }

    public void setExceptAmt1(String exceptAmt1) {
        this.exceptAmt1 = exceptAmt1;
    }

    public String getExceptAmt2() {
        return exceptAmt2;
    }

    public void setExceptAmt2(String exceptAmt2) {
        this.exceptAmt2 = exceptAmt2;
    }

    public String getRedeemFlag() {
        return redeemFlag;
    }

    public void setRedeemFlag(String redeemFlag) {
        this.redeemFlag = redeemFlag;
    }

    public String getBonusAmt() {
        return bonusAmt;
    }

    public void setBonusAmt(String bonusAmt) {
        this.bonusAmt = bonusAmt;
    }

    public String getDebitPoint() {
        return debitPoint;
    }

    public void setDebitPoint(String debitPoint) {
        this.debitPoint = debitPoint;
    }

    public String getNonRedeemAmt() {
        return nonRedeemAmt;
    }

    public void setNonRedeemAmt(String nonRedeemAmt) {
        this.nonRedeemAmt = nonRedeemAmt;
    }

    public String getNonPointAmt() {
        return nonPointAmt;
    }

    public void setNonPointAmt(String nonPointAmt) {
        this.nonPointAmt = nonPointAmt;
    }

    public String getTradeDesc() {
        return tradeDesc;
    }

    public void setTradeDesc(String tradeDesc) {
        this.tradeDesc = tradeDesc;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(String itemAmount) {
        this.itemAmount = itemAmount;
    }

    public String getItemQty() {
        return itemQty;
    }

    public void setItemQty(String itemQty) {
        this.itemQty = itemQty;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
