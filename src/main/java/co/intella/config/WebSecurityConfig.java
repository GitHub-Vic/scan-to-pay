package co.intella.config;

import co.intella.auth.AuthenticationEntryPoint;
import co.intella.auth.CustomAuthenticationSuccessHandler;
import co.intella.auth.IntellaAuthProvider;
import co.intella.utility.SystemInstance;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.annotation.Resource;
import java.util.Arrays;

/**
 * @author Andy Lin
 */
@EnableWebSecurity
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Resource
    private AuthenticationEntryPoint authenticationEntryPoint;

    @Resource
    private CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and()
                .csrf().disable()
                .exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint)
            .and()
                .authorizeRequests()
                .antMatchers("/api/crm/sales/**", "/api/crm/role/**", "/api/crm/merchant/**")
                    .hasAnyRole("CRM_USER")
                .antMatchers("/api/merchant/**", "/api/trade/**", "/api/qrcode/**", "/api/device/**", "/api/paymentaccount/**", "/api/capture/**", "/api/captureOrder/**")
                    .hasAnyRole("APP_USER")
            .and()
                .formLogin()
                .successHandler(customAuthenticationSuccessHandler)
            .and()
                .logout();
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowCredentials(true);
        configuration.setAllowedOrigins(Arrays.asList("*"));
        configuration.setAllowedMethods(Arrays.asList("*"));
        configuration.setAllowedHeaders(Arrays.asList("*"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Bean
    public CustomAuthenticationSuccessHandler mySuccessHandler(){
        return new CustomAuthenticationSuccessHandler();
    }
}
