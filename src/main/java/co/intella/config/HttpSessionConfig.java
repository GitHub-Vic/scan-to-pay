package co.intella.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * @author Miles
 */
@EnableRedisHttpSession
@Configuration
public class HttpSessionConfig {
}
