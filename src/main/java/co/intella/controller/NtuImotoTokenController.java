package co.intella.controller;


import co.intella.service.ImotoTokenService;
import co.intella.utility.JsonUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/imotoToken")
public class NtuImotoTokenController {

    @Resource
    private ImotoTokenService imotoTokenService;

    @GetMapping(value = "test")
    public String test() throws Exception {

//        Map<String, String> requestData = new HashMap<String, String>();
//        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
//        ClassLoader classLoader = getClass().getClassLoader();
//        File file = new File(classLoader.getResource("key/pub.der").getFile());
//        PublicKey publicKey = KeyReader.loadPublicKeyFromDER(file);

        Map<String, String> resultMap = new LinkedHashMap<>();
//
//        requestData.put("Method", "00000");
//        requestData.put("ServiceType", "OLPay");
//        requestData.put("MchId", "iMoto4301");
//        requestData.put("TradeKey", "9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");
//        requestData.put("DeviceInfo", "skb0001");
//        requestData.put("Permanent", "Y");
//        requestData.put("Detail", "NTU iMoto");
//        requestData.put("CreateTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
//        NtuImotoToken ntuImotoToken;
//
//        for (int i = 6; i <= 805; ++i) {
//            String num = String.format("%04d", i);
//            requestData.put("Body", "Token " + num);
//            String responseEntity = HttpRequestUtil.post("https://a.intella.co/allpaypass/api/general", requestData, publicKey, secretKey);
//            String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, responseEntity);
//            JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);
//            JsonObject jsonData = json.getAsJsonObject("Data");
//            String url = jsonData.get("urlToken").getAsString();
//            String shortId = url.substring(url.lastIndexOf("/") + 1);
//            ntuImotoToken = new NtuImotoToken();
//            ntuImotoToken.setTokenOutercode(num);
//            ntuImotoToken.setShortId(shortId);
//            ntuImotoTokenService.insert(ntuImotoToken);
//            resultMap.put(num, url + ".imoto");
//        }

        return JsonUtil.toJsonStr(resultMap);
    }
}
