package co.intella.controller;

import co.intella.domain.line.LineResponseData;
import co.intella.domain.weixin.*;
import co.intella.service.MerchantService;
import co.intella.service.WeixinService;
import co.intella.utility.JsapiParamMediator;
import co.intella.utility.GeneralResponseMessageUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 *  @author Alex Chu
 */
@Controller
@RequestMapping("/api/weixin")
public class WeixinController {

    private final Logger LOGGER = LoggerFactory.getLogger(WeixinController.class);

    private static final String URL_DECODER = "UTF-8";

    @Resource
    private WeixinService weixinService;

    @Resource
    private MerchantService merchantService;

    @RequestMapping(method = RequestMethod.POST, value = "/ftf")
    public ResponseEntity<String> ftf(@RequestBody WeixinFtfRequestData input) {
        try {
            String result = weixinService.faceToFace(input);
            return new ResponseEntity<String>(result, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION][WX]" + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage(e.toString()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/prepareQrcode")
    public ResponseEntity<String> prepareQrcode(@RequestBody JsapiParamMediator input) {
        merchantService.registerJsapiMediator(input);
        return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/jsapi")
    public ResponseEntity<String> jsapi(@RequestBody WeixinJsapiRequestData input) {

        try {
            String result = weixinService.callJsApi(input);
            return new ResponseEntity<String>(result, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION][WX]" + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage(e.toString()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @RequestMapping(method= RequestMethod.GET, value="/getjsapi", params = {"p", "t", "r", "n", "p0", "p1", "p2", "p3", "s", "st"})
    public ModelAndView getJsAPI(
            @RequestParam("p") String partnerId
            , @RequestParam("t") String timeStamp
            , @RequestParam("r") String randomNum
            , @RequestParam("n") String noticeAddress
            , @RequestParam("p0") String extension
            , @RequestParam("p1") String uuid
            , @RequestParam("p2") String orderNo
            , @RequestParam("p3") String cost
            , @RequestParam("s") String signature
            , @RequestParam("st") String sigMethod)  {

        // todo  create an order to save request information

        LOGGER.info("Do Get JsPAI.");
        WeixinRequestParameter payparam = new WeixinRequestParameter();

        try {
            LOGGER.info("Set Pay params.");
            payparam.setP(URLDecoder.decode(partnerId,URL_DECODER));
            payparam.setT(URLDecoder.decode(timeStamp,URL_DECODER));
            payparam.setR(URLDecoder.decode(randomNum,URL_DECODER));
            payparam.setN(URLDecoder.decode(noticeAddress,URL_DECODER));
            payparam.setP0(URLDecoder.decode(extension,URL_DECODER));
            payparam.setP1(URLDecoder.decode(uuid,URL_DECODER));
            payparam.setP2(URLDecoder.decode(orderNo,URL_DECODER));
            payparam.setP3(URLDecoder.decode(cost,URL_DECODER));
            payparam.setS(URLDecoder.decode(signature,URL_DECODER));
            payparam.setSt(URLDecoder.decode(sigMethod,URL_DECODER));

            String redirectUrl = weixinService.payment(payparam);
            return new ModelAndView("redirect:" + redirectUrl);

        } catch (Exception e){
            LOGGER.error("[EXCEPTION][WX]" + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            return new ModelAndView("redirect:" + "???");
        }
    }

    @RequestMapping( method= RequestMethod.POST, value="/micropay")
    public ResponseEntity<String> micropay(@RequestBody WeixinMicropayRequestData input) {

        try {
            // todo: poll for choosing the corresponding  method(bank)
            String result = weixinService.micropay(input);
            return new ResponseEntity<String>(result , HttpStatus.OK);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("[EXCEPTION][WX]" + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("encoding error"), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e){
            LOGGER.error("[EXCEPTION][WX]" + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("error : " + e.toString()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/orderquery")
    public ResponseEntity<String> orderQuery(@RequestBody WeixinOrderQueryRequestData input) {
        try {
            String result = weixinService.orderQuery(input);
            return new ResponseEntity<String>(result, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION][WX]" + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("error : " + e.toString()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/singlequery")
    public ResponseEntity<String> singleQuery(@RequestBody WeixinSingleQueryRequestData input) {
        try {
            String result = weixinService.singleQuery(input);

            return new ResponseEntity<String>(result, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION][WX]" + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("error : " + e.toString()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @RequestMapping(method = RequestMethod.POST, value = "/refund")
    public ResponseEntity<String> refund(@RequestBody WeixinRefundRequestData input) {

        try {
            String result = weixinService.refund(input);

            return new ResponseEntity<String>(result, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION][WX]" + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("error : " + e.toString()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/ratequery")
    public ResponseEntity<String> rateQuery(@RequestBody WeixinCurrencyRequestData input) {
        try {
            String result = weixinService.rateQuery(input);

            return new ResponseEntity<String>(result, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION][WX]" + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("error : " + e.toString()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @RequestMapping("/paycode")
    public ModelAndView calculatePayCode(
            @RequestParam("p") String merchantId,
            @RequestParam("t") String orderId) throws JsonProcessingException, NoSuchAlgorithmException {

        try {
            String payCode = weixinService.getPayCode(merchantId, orderId);
            ModelAndView model = new ModelAndView();
            model.addObject("code", payCode);
            model.setViewName("paycode");
            return model;

        } catch (Exception e) {
            LOGGER.error("[EXCEPTION][WX]" + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            ModelAndView model = new ModelAndView();
            model.addObject("code", "Something wrong.");
            model.setViewName("paycode");
            return model;
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/result")
    public ResponseEntity<String> test2444(@RequestBody LineResponseData input) {
        return null;
    }

}
