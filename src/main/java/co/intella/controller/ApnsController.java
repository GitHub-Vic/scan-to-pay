package co.intella.controller;

import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;

import co.intella.model.Merchant;
import co.intella.model.Recipient;
import co.intella.service.MerchantService;
import co.intella.service.RecipientService;
import co.intella.service.TokenService;

/**
 * @author Alex
 */
@Controller
@RequestMapping("/api/apns")
public class ApnsController {
    private final Logger LOGGER = LoggerFactory.getLogger(ApnsController.class);

    @Resource
    private MerchantService merchantService;

    @Resource
    private RecipientService recipientService;
    
    @Resource
    private TokenService tokenService;

    @RequestMapping(method= RequestMethod.POST, value="/register" )
    public ResponseEntity<String> register(@RequestBody Map<String, String> request) throws Exception {

        LOGGER.info("[REQUEST] api/register/apns" + new Gson().toJson(request));
        String mchId = request.get("mchId");
        String udid = request.get("udid");
        String token = request.get("token");
        String appId = request.get("appId");
        String userId = request.get("userId");
//        String userTokenId = request.get("userTokenId");
        Merchant merchant = null;
        
        if (!StringUtils.isEmpty(mchId)) {
        	merchant = merchantService.getOne(mchId);
        }
        
        if (merchant==null && StringUtils.isEmpty(userId)) {
        	return new ResponseEntity<String>("Fail", HttpStatus.OK);
        }
        
        Recipient recipient = null;
        if (StringUtils.isEmpty(appId)) {
        	recipient = recipientService.getOneAppIdNull(udid);
        } else {
        	recipient = recipientService.getOne(udid, appId);
        }
        if (recipient==null){
            recipient = new Recipient();
            recipient.setCrDate(new Date());
        }
        recipient.setDateStamp(new Date());
        recipient.setSerialId(udid);
        recipient.setApnsToken(token);
        recipient.setMerchant(merchant);
        recipient.setAppId(appId);
        recipient.setUserId(userId);
        recipientService.save(recipient);

        return new ResponseEntity<String>("OK", HttpStatus.OK);
    }

    @RequestMapping(method= RequestMethod.POST, value="/unregister" )
    public ResponseEntity<String> unregister(@RequestBody Map<String, String> request) throws Exception {
        LOGGER.info("[REQUEST] api/unregister/apns" + new Gson().toJson(request));
        String udid = request.get("udid");
        String appId = request.get("appId");
        if (StringUtils.isEmpty(appId)) {
        	recipientService.deleteNullAppId(udid);
        } else {
        	recipientService.delete(udid, appId);
        }
        return new ResponseEntity<String>("OK", HttpStatus.OK);
    }
}
