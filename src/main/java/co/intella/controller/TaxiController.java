package co.intella.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import co.intella.service.BankService;
import co.intella.service.MerchantService;
import co.intella.service.PaymentAccountService;

@RestController
@RequestMapping("/api/taxi")
public class TaxiController {
	
    private final Logger LOGGER = LoggerFactory.getLogger(TaxiController.class);

    @Resource
    private MerchantService merchantService;

    @Resource
    private BankService bankService;

    @Resource
    private PaymentAccountService paymentAccountService;
    
    @Autowired
    private MessageSource messageSource;

     private Locale locale = new Locale("zh_TW");


    @GetMapping("/checkQrcodePayment/{accountId}" )
    @ResponseBody
    public Map<String, Object> checkQrcodePayment(@PathVariable(value = "accountId") String accountId) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	try {
    		result =  paymentAccountService.checkQrcodePayment(accountId);
    	} catch (Exception e) {
    		LOGGER.error("[EXCEPTION] checkQrcodePayment : " + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
    		result.put("msgCode", "9999");
    		result.put("msg", messageSource.getMessage("co.intella.exception.msg", null, locale));
    	}
    	return result;
    }

    @GetMapping("/checkTicketPayment/{accountId}")
    @ResponseBody
    public Map<String, Object> checkTicketPayment(@PathVariable(value = "accountId") String accountId) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	try {
    		result =  paymentAccountService.checkTicketPayment(accountId);
    	} catch (Exception e) {
    		LOGGER.error("[EXCEPTION] checkTicket : " + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
    		result.put("msgCode", "9999");
    		result.put("msg", messageSource.getMessage("co.intella.exception.msg", null, locale));
    	}
    	return result;
    }
}
