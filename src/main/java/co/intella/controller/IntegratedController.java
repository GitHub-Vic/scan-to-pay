package co.intella.controller;

import co.intella.constant.Payment;
import co.intella.crypto.AesCryptoUtil;
import co.intella.crypto.KeyReader;
import co.intella.domain.contratStore.CustomizePageRequest;
import co.intella.domain.integration.*;
import co.intella.domain.pi.*;
import co.intella.domain.weixin.WeixinCurrencyRequestData;
import co.intella.domain.weixin.WeixinMicropayRequestData;
import co.intella.domain.weixin.WeixinRefundRequestData;
import co.intella.domain.weixin.WeixinSingleQueryRequestData;
import co.intella.exception.*;
import co.intella.model.*;
import co.intella.net.Constant;
import co.intella.net.HttpRequestUtil;
import co.intella.service.*;
import co.intella.service.impl.TsGWServiceImpl;
import co.intella.utility.GeneralResponseMessageUtil;
import co.intella.utility.JsonUtil;
import co.intella.utility.MD5Util;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.gson.*;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;

import javax.annotation.Resource;
import javax.crypto.SecretKey;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.security.InvalidKeyException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static co.intella.crypto.RsaCryptoUtil.decrypt;

/**
 * Scan2pay Api
 *
 * @author Miles
 */
@Controller
@RequestMapping("/api/general")
public class IntegratedController {

    private final Logger LOGGER = LoggerFactory.getLogger(IntegratedController.class);

    private SecureRandom random = new SecureRandom();

    @Value("${api.url}")
    private String API_URL;

    @Value("${our.cdbc.weixin.api.url}")
    private String CTBC_URL;

//    private String CTBC_URL = "http://34.80.101.124:30392";

    @Resource
    private OrderService orderService;

    @Resource
    private AliService aliService;

    @Resource
    private PiInAppService piInAppService;

    @Resource
    private WeixinService weixinService;

    @Resource
    private SpService spService;

    @Resource
    private TcBankService tcBankService;

    @Resource
    private TsBankService tsBankService;

    @Resource
    private ECPayService ecPayService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private AllpayService allpayService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private JkosService jkosService;

    @Resource
    private AipeiService aipeiService;

    @Resource
    private GamaPayService gamaPayService;

    @Resource
    private LineService lineService;

    @Autowired
    private Environment env;

    @Autowired
    private MessageSource messageSource;

    @Resource
    private CheckInputService checkInputService;

    @Resource
    private EzCardService ezCardService;

    @Resource
    private PiService piService;

    @Resource
    private TwPayService twPayService;

    @Resource
    private FridayService fridayService;

    @Resource
    private Pay2GoService pay2GoService;

    @Resource
    private RedisService redisService;

    @Resource
    private LuckyPayService luckyPayService;

    @Resource
    private ScsbService scsbService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private YuanTaOfflineService yuanTaOfflineService;

    @Resource
    private AlipayShangHaiService alipayShangHaiService;

    @Resource
    private PaymentService fuBonService;

    @Resource
    private FuBonBankService fuBonBankService;

    @Resource
    private TsGWService tsGWService;

    @Resource
    private IPassService iPassService;

    @Resource
    private MegaTWPayService megaTWPayService;

    @Resource
    private LookupCodeService lookupCodeService;

    @Resource
    private TicketLogicService ticketLogicService;

    @Resource
    private MegaWeiXinService megaWeixinService;

    @Resource
    private HNTwpayService hnTwpayService;

    @Resource
    private RefundDetailService refundDetailService;

    @Resource
    private TsmcService tsmcService;

    @Resource
    private UUPayService uuPayService;

    @Resource
    private TOBwebAtmService toBwebAtmService;

    @Resource
    private IcashPayService icashPayService;

    @Resource
    private TCBTwpayService tcbTwpayService;

    @Resource
    private DiscountService discountService;

    @Resource
    private DiscountTradeService discountTradeService;

    @Resource
    private TradeDetailMultiService tradeDetailMultiService;

    @Resource
    private MegaTWPay2Service megaTWPay2Service;

    @Resource
    private CashService cashService;

    @Resource
    private NewPiService newPiService;

    private Locale locale = new Locale("zh_TW");

    // @CrossOrigin(origins = "*")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> generalRequest(@RequestBody IntegratedRequest requests, HttpServletRequest request) {

        String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
        String requestBody = requests.getRequestBody();
        String encryptApiKey = requests.getApiKey();

        String body = "";
        String clientIP = request.getHeader("X-FORWARDED-FOR");
        SecretKey secretKey = null;
        try {
            LOGGER.info("[REQUEST][GENERAL]IntegratedRequest =>" + JsonUtil.toJsonStr(requests));
            secretKey = decryptSecretKey(encryptApiKey);
            body = AesCryptoUtil.decrypt(secretKey, Constant.IV, Base64.decode(requestBody));
            LOGGER.info("[REQUEST][GENERAL][" + sessionId + "][" + clientIP + "]" + body);

            JsonObject jsonObject = new JsonParser().parse(body).getAsJsonObject();
            JsonObject header = jsonObject.getAsJsonObject("Header");
            JsonPrimitive data = jsonObject.getAsJsonPrimitive("Data");

            LOGGER.info("[REQUEST][GENERAL][" + sessionId + "][" + clientIP + "][HEADER]" + header);

            RequestHeader requestHeader = new Gson().fromJson(header.toString(), RequestHeader.class);

            LOGGER.info("[RequestHeader getMerchantId][" + requestHeader.getMerchantId() + "]");

            JsonObject requestData3 = new Gson().fromJson(data.getAsString(), JsonObject.class);

            if (!lookupCodeService.isTsmcAccount(requestHeader.getMerchantId())) {
                checkInputService.maskCradId(sessionId, clientIP, requestData3);
            }

            checkInputService.doRequest(requestHeader, requestData3);

            if (requestData3.has("Body")) {
                String bodyRandom = requestData3.get("Body").getAsString() + "-"
                        + String.format("%04d", new Random().nextInt(9999));
                if ("Payment".equals(requestHeader.getServiceType())) {
                    bodyRandom += requestData3.has("CardId")
                            ? "-" + requestData3.get("CardId").getAsString().substring(12)
                            : "";
                }
                requestData3.remove("Body");
                requestData3.addProperty("Body", bodyRandom);
                data = new JsonPrimitive(new Gson().toJson(requestData3));
            } else {
                String bodyRandom = "-" + String.format("%04d", new Random().nextInt(9999));
                if ("Payment".equals(requestHeader.getServiceType())) {
                    bodyRandom += requestData3.has("CardId")
                            ? "-" + requestData3.get("CardId").getAsString().substring(12)
                            : "";
                }
                requestData3.remove("Body");
                requestData3.addProperty("Body", bodyRandom);
                data = new JsonPrimitive(new Gson().toJson(requestData3));
            }

            if ("Payment".equals(requestHeader.getServiceType()) && "20000".equals(requestHeader.getMethod())) {
                List<PaymentAccount> paymentAccounts = paymentAccountService.listAllByAccount(requestHeader.getMerchantId());
                List<PaymentAccount> collect = paymentAccounts.stream()
                        .filter(PaymentAccount::getIsDefault)
                        .filter(paymentAccount -> Payment.CREDIT_CARD_BANKID.contains(paymentAccount.getBank().getBankId())).collect(Collectors.toList());

                Comparator<PaymentAccount> comparator = Comparator.comparing(e -> e.getBank().getBankId());
                collect.sort(comparator.reversed());

                requestHeader.setMethod("2" + (CollectionUtils.isNotEmpty(collect) ? collect.get(0).getBank().getBankId() : "00") + "00");
            }

            String generalResponseData = "";
            String integrateMchId = requestHeader.getMerchantId();

            if ("OLPay".equals(requestHeader.getServiceType())) {
                QrcodeParameter qrcodeParameter = new Gson().fromJson(data.getAsString(), QrcodeParameter.class);
                qrcodeParameter.setMchId(requestHeader.getMerchantId());
                if (!"00000".equals(requestHeader.getMethod())) {
                    qrcodeParameter.setMethod(requestHeader.getMethod());
                }
                JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
                if (requestData.has("Permanent") && requestData.get("Permanent").getAsString().equals("Y")) {
                    qrcodeParameter.setTimeExpire(null);
                    if (requestData.has("TotalFee")) {
                        qrcodeParameter.setCodeType("2");
                        //TODO  固定金額 的訂單長度不能超過4碼
                        qrcodeParameter.setStoreOrderNo(MD5Util.getMD5(qrcodeParameter.getStoreOrderNo()).substring(0, 4).toUpperCase());
                    } else {
                        qrcodeParameter.setCodeType("3");
                    }
                } else {
                    qrcodeParameter.setCodeType("1");
                    if (qrcodeParameter.getTimeExpire() == null) {
                        qrcodeParameter
                                .setTimeExpire(DateTime.now().plusMinutes(5).toString(SystemInstance.DATE_PATTERN));
                    }
                }

                qrcodeParameter = qrcodeService.createQrcode(qrcodeParameter);

                String urlToken = qrcodeParameter.getShortId();

                IntegratedOLPayResponseData integratedOLPayResponseData = new IntegratedOLPayResponseData();
                integratedOLPayResponseData.setUrlToken(API_URL + urlToken);
                ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
                responseGeneralBody.setData(integratedOLPayResponseData);

                ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
                responseGeneralHeader.setStatusCode(SystemInstance.STATUS_SUCCESS);
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
                responseGeneralHeader.setServiceType("OLPay");
                responseGeneralHeader.setMchId(requestHeader.getMerchantId());
                responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

                responseGeneralBody.setHeader(responseGeneralHeader);

                generalResponseData = new Gson().toJson(responseGeneralBody);

                byte[] encryptBytes = AesCryptoUtil.encrypt(secretKey, Constant.IV, generalResponseData);
                IntegratedResponse integratedResponse = new IntegratedResponse();
                integratedResponse.setResult(Base64.encode(encryptBytes).trim().replaceAll("\r?\n", ""));

                String encryptResponse = new Gson().toJson(integratedResponse);

                LOGGER.info("[RESPONSE][" + sessionId + "][" + DateTime.now().toString() + "] "
                        + encryptResponse);
                // setRedis
                String callBackUrl = "CallBackUrl";
                if (requestData.has(callBackUrl) && requestData.get(callBackUrl).getAsString() != null) {
                    redisService.setRedis(qrcodeParameter.getStoreOrderNo(), requestData.get(callBackUrl).getAsString(),
                            600);
                }
                return new ResponseEntity<String>(encryptResponse, HttpStatus.OK);
            } else if (SystemInstance.TYPE_MICROPAY.equals(requestHeader.getServiceType())) {
                if (!dispatchMicropayPlatform(data, sessionId, requestHeader)) {
                    return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "7120",
                            messageSource.getMessage("response.7120", null, locale));
                }

                // 20200325 edit sol: 被掃這邊判斷完支付後，做現折運算
                if (requestData3.has("TotalFee") && requestData3.has(SystemInstance.FIELD_NAME_STORE_ORDER_NO)) {
                    DiscountTrade discountTrade = discountService.checkoutTotalFree(requestHeader.getMerchantId(),
                            Long.valueOf(requestData3.get("TotalFee").getAsString()), requestHeader.getMethod());
                    TradeDetail tradeDetail = tradeDetailService
                            .getOne(requestData3.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString());
                    if (Objects.nonNull(discountTrade) && Objects.isNull(tradeDetail)) {
                        discountTrade
                                .setOrderId(requestData3.get(SystemInstance.FIELD_NAME_STORE_ORDER_NO).getAsString());
                        discountTrade = discountTradeService.save(discountTrade);
                        LOGGER.info("discountTrade save  :" + JsonUtil.toJsonStr(discountTrade));
                        if (Objects.nonNull(discountTrade)) {
                            requestData3.addProperty("TotalFee",
                                    String.valueOf(discountTrade.getTradeAmount().intValue()));
                            data = new JsonPrimitive(new Gson().toJson(requestData3));
                        }
                    }
                }
            } else if ("Refund".equals(requestHeader.getServiceType())
                    || "Cancel".equals(requestHeader.getServiceType())
                    || "EZCRefund".equals(requestHeader.getServiceType())) {
                Merchant merchant = merchantService.getOne(requestHeader.getMerchantId());

                JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
                if (!merchant.getAccountId().equals("mwd")) {
                    if (!requestData.has("RefundKey")) {
                        LOGGER.error("[EXCEPTION][Refund][Pin Error][" + sessionId + "]");
                        return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "7168",
                                messageSource.getMessage("response.7168", null, locale));
                    }
                    if (!merchant.getRefundPin().equals(requestData.get("RefundKey").getAsString())) {
                        LOGGER.error("[EXCEPTION][Refund][Pin Error][" + sessionId + "]");
                        return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "7168",
                                messageSource.getMessage("response.7168", null, locale));
                    }
                }
                if (requestHeader.getMethod().equals("00000")) {
                    TradeDetail tradeDetail = tradeDetailService.getOne(requestData.get("StoreOrderNo").getAsString());
                    if (tradeDetail != null) {
                        requestHeader.setMethod(tradeDetail.getMethod());
                        if (tradeDetail.getMethod().startsWith("2")) {
                            requestHeader.setServiceType("Cancel");
                        }
                    } else {
                        throw new OrderNotFoundException("Order not found");
                    }
                }
            }

            // dispatch credit.
            if ("20840".equals(requestHeader.getMethod())) {
                LOGGER.debug("[dispatch credit][Tsbank][" + sessionId + "]");
                requestHeader.setMethod("20800");
            }

            if ("10300".equals(requestHeader.getMethod())) {
                generalResponseData = getPiResponse(requestHeader, data, integrateMchId);
                LOGGER.debug("[RESPONSE][10300][" + sessionId + "]" + generalResponseData);
            } else if ("10220".equals(requestHeader.getMethod())) {
                // notice. we can not use this mode because we do not have store in real world.
                generalResponseData = aliService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][10220][" + sessionId + "]" + generalResponseData);
            } else if ("10110".equals(requestHeader.getMethod())) {
                generalResponseData = getWeixinResponse(requestHeader, data);
                LOGGER.debug("[RESPONSE][10110][" + sessionId + "]" + generalResponseData);
            } else if ("14010".equals(requestHeader.getMethod())) {
                generalResponseData = megaWeixinService.doRequest(requestHeader, data, integrateMchId, clientIP);
                LOGGER.debug("[RESPONSE][14010][" + sessionId + "]" + generalResponseData);
            } else if ("12710".equals(requestHeader.getMethod())) {
                generalResponseData = getCTBCWeixinResponse(requestHeader, data);
                LOGGER.debug("[RESPONSE][12710][" + sessionId + "]" + generalResponseData);
            } else if ("10500".equals(requestHeader.getMethod())) {
                generalResponseData = allpayService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][10500][" + sessionId + "]" + generalResponseData);
            } else if ("20400".equals(requestHeader.getMethod())) {
                generalResponseData = tcBankService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][20400][" + sessionId + "]" + generalResponseData);
            } else if ("20600".equals(requestHeader.getMethod())) {
                generalResponseData = spService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][20600][" + sessionId + "]" + generalResponseData);
            } else if ("22600".equals(requestHeader.getMethod()) || "23900".equals(requestHeader.getMethod())) {
                generalResponseData = scsbService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][22600][" + sessionId + "]" + generalResponseData);
            } else if ("60300".equals(requestHeader.getMethod())) {
                generalResponseData = getInAppResponse(requestHeader, data, integrateMchId);
                LOGGER.debug("[RESPONSE][60300][" + sessionId + "]" + generalResponseData);
            } else if ("60500".equals(requestHeader.getMethod())) {
                generalResponseData = allpayService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][60500][" + sessionId + "]" + generalResponseData);
            } else if ("20800".equals(requestHeader.getMethod())) {
                generalResponseData = tsBankService.doRequest(requestHeader, data, integrateMchId, clientIP);
                LOGGER.debug("[RESPONSE][20800][" + sessionId + "]" + generalResponseData);
            } else if ("21100".equals(requestHeader.getMethod())) {
                generalResponseData = ecPayService.doRequest(requestHeader, data, integrateMchId);
                LOGGER.debug("[RESPONSE][21100][" + sessionId + "]" + generalResponseData);
            } else if ("10900".equals(requestHeader.getMethod()) || "13600".equals(requestHeader.getMethod())) {
                generalResponseData = jkosService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][10900][" + sessionId + "]" + generalResponseData);
            } else if ("11300".equals(requestHeader.getMethod())) {
                generalResponseData = gamaPayService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][11300][" + sessionId + "]" + generalResponseData);
            } else if ("13000".equals(requestHeader.getMethod())) {
                generalResponseData = luckyPayService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][13000][" + sessionId + "]" + generalResponseData);
            }
//            else if ("31800".equals(requestHeader.getMethod())) {
//
//                Merchant merchant = merchantService.getOne(requestHeader.getMerchantId());
//
//                if (merchant == null) {
//                    throw new NoSuchMerchantException("Merchant error");
//                }
//
//                JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
//                generalResponseData = ezCardService.ezRequest(requestHeader, requestData, merchant);
//
//            } else if ("32100".equals(requestHeader.getMethod())) {
//
//                System.out.println(requestHeader.getMerchantId());
//                Merchant merchant = merchantService.getOne(requestHeader.getMerchantId());
//
//                if (merchant == null) {
//                    throw new NoSuchMerchantException("Merchant error");
//                }
//
//                JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
//                generalResponseData = iPassService.ezRequest(requestHeader, requestData, merchant);
//
//            }
            else if ("14900".equals(requestHeader.getMethod())) {
                generalResponseData = tsmcService.doRequest(requestHeader,
                        new Gson().fromJson(data.getAsString(), JsonObject.class));
                LOGGER.debug("[RESPONSE][00000][14500][FUBON][" + sessionId + "]" + generalResponseData);
            } else if (lookupCodeService.isTicketType(requestHeader.getMethod().substring(1, 3))) {
                if (lookupCodeService.isTsmcAccount(requestHeader.getMerchantId())
                        && "Payment".equals(requestHeader.getServiceType())) {
                    // 如果是台積電的票卡交易，先發起會員卡機，再去票證交易 20200301
//                    Merchant merchant = merchantService.getOne(requestHeader.getMerchantId());
//                    if (Objects.isNull(merchant)) {
//                        throw new NoSuchMerchantException("Merchant error");
//                    }
//                    tsmcService.doMqttTSMC(merchant, new Gson().fromJson(data.getAsString(), JsonObject.class));
                    generalResponseData = tsmcService.doRequest(requestHeader,
                            new Gson().fromJson(data.getAsString(), JsonObject.class));
                } else {
                    // 20190704 edit:Sol 票證相關前面插入新的票證邏輯 做一些事情再轉交該service做事
                    generalResponseData = ticketLogicService.doRequest(requestHeader,
                            new Gson().fromJson(data.getAsString(), JsonObject.class));
                }
                LOGGER.debug("[RESPONSE][ticketLogicService][" + sessionId + "]" + generalResponseData);
            } else if ("61300".equals(requestHeader.getMethod())) {
                generalResponseData = gamaPayService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][61300][" + sessionId + "]" + generalResponseData);
            } else if ("11400".equals(requestHeader.getMethod())) {
                generalResponseData = aipeiService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][11400][" + sessionId + "]" + generalResponseData);
            } else if ("11500".equals(requestHeader.getMethod())) {
                generalResponseData = lineService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][11500][" + sessionId + "]" + generalResponseData);
            } else if ("61500".equals(requestHeader.getMethod())) {
                generalResponseData = lineService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][61500][" + sessionId + "]" + generalResponseData);
            } else if ("14200".equals(requestHeader.getMethod()) || "14300".equals(requestHeader.getMethod())
                    || "14400".equals(requestHeader.getMethod()) || "12000".equals(requestHeader.getMethod())) {
                generalResponseData = twPayService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][" + requestHeader.getMethod() + "][" + sessionId + "]" + generalResponseData);
            } else if ("15000".equals(requestHeader.getMethod())) {
                generalResponseData = tcbTwpayService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][" + requestHeader.getMethod() + "][" + sessionId + "]" + generalResponseData);
            } else if ("15200".equals(requestHeader.getMethod())) {
                generalResponseData = megaTWPay2Service.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][" + requestHeader.getMethod() + "][" + sessionId + "]" + generalResponseData);
            } else if ("12300".equals(requestHeader.getMethod())) {
                generalResponseData = fridayService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][12300][" + sessionId + "]" + generalResponseData);
            } else if ("12400".equals(requestHeader.getMethod())) {
                generalResponseData = pay2GoService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][12400][" + sessionId + "]" + generalResponseData);
            } else if ("11100".equals(requestHeader.getMethod())) {
                generalResponseData = yuanTaOfflineService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][11100][" + sessionId + "]" + generalResponseData);
            } else if ("12520".equals(requestHeader.getMethod())) {
                generalResponseData = alipayShangHaiService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][12520][" + sessionId + "]" + generalResponseData);
            } else if ("13500".equals(requestHeader.getMethod())) {
                generalResponseData = megaTWPayService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][13500][" + sessionId + "]" + generalResponseData);
            } else if ("23240".equals(requestHeader.getMethod())) {
                generalResponseData = fuBonService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][00000][23240][FUBON][" + sessionId + "]" + generalResponseData);
            } else if ("13320".equals(requestHeader.getMethod())) {
                generalResponseData = tsGWService.doRequest(requestHeader, data, TsGWServiceImpl.ALIPAY_O);
                LOGGER.debug("[RESPONSE][13320][" + sessionId + "]" + generalResponseData);
            } else if ("13410".equals(requestHeader.getMethod())) {
                generalResponseData = tsGWService.doRequest(requestHeader, data, TsGWServiceImpl.WEIXIN_O);
                LOGGER.debug("[RESPONSE][13410][" + sessionId + "]" + generalResponseData);
            } else if ("23800".equals(requestHeader.getMethod())) {
                generalResponseData = fuBonBankService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][00000][23800][FUBON][" + sessionId + "]" + generalResponseData);
            } else if ("14500".equals(requestHeader.getMethod())) {
                generalResponseData = hnTwpayService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][00000][14500][FUBON][" + sessionId + "]" + generalResponseData);
            } else if ("15300".equals(requestHeader.getMethod())) {
                generalResponseData = cashService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][00000][15300][CASH][" + sessionId + "]" + generalResponseData);
            } else if ("13100".equals(requestHeader.getMethod())) {
                generalResponseData = uuPayService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][00000][13100][UUPAY][" + sessionId + "]" + generalResponseData);
            } else if ("15800".equals(requestHeader.getMethod())) {
                generalResponseData = icashPayService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][00000][15800][icashpay][" + sessionId + "]" + generalResponseData);
            } else if ("15900".equals(requestHeader.getMethod())) {
                generalResponseData = newPiService.doRequest(requestHeader, data);
                LOGGER.debug("[RESPONSE][00000][15900][newPi][" + sessionId + "]" + generalResponseData);
            } else if ("00000".equals(requestHeader.getMethod())) {
                if ("OrderQuery".equals(requestHeader.getServiceType())) {
                    JsonObject requestJson = new Gson().fromJson(data.getAsString(), JsonObject.class);
                    IntegratedOrderQueryRequestData requestData = new IntegratedOrderQueryRequestData();
                    requestData.setStartDate(requestJson.get("StartDate").getAsString());
                    requestData.setEndDate(requestJson.get("EndDate").getAsString());
                    requestData.setOrderStatus(
                            requestJson.has("OrderStatus") ? requestJson.get("OrderStatus").getAsString() : null);

                    List<TradeDetail> list = tradeDetailService.list(requestData, requestHeader.getMerchantId());
                    generalResponseData = getGeneralOrderQueryResponse(list, requestHeader.getMerchantId());
                    LOGGER.info("[RESPONSE][00000][" + sessionId + "]" + generalResponseData);

                } else if ("SingleOrderQuery".equals(requestHeader.getServiceType())) {

                    JsonObject requestJson = new Gson().fromJson(data.getAsString(), JsonObject.class);
                    TradeDetail tradeDetail = tradeDetailService.getOne(requestHeader.getMerchantId(),
                            requestJson.get("StoreOrderNo").getAsString());

                    if (Objects.isNull(tradeDetail)) {
                        throw new OrderNotFoundException(SystemInstance.ORDER_NOT_FOUND);
                    }
                    LocalDateTime orderDateTime = LocalDateTime.parse(tradeDetail.getCreateDate(),
                            DateTimeFormatter.ofPattern(SystemInstance.DATE_PATTERN));
                    LocalDateTime localDateTime = LocalDateTime.now();
                    requestHeader.setMethod(tradeDetail.getMethod());
                    // 調整如果已經成功，或還在當天，以DB狀態為主
                    if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())
                            || Duration.between(orderDateTime, localDateTime).toDays() >= 1) {
                        if ("10110".equals(tradeDetail.getMethod())) {
                            requestHeader.setMethod(tradeDetail.getMethod());
                            generalResponseData = this.getWeixinResponse(requestHeader, data);
                            LOGGER.debug("[RESPONSE][00000][10110][" + sessionId + "]" + generalResponseData);

                        } else if ("12710".equals(tradeDetail.getMethod())) {
                            generalResponseData = this.getCTBCWeixinResponse(requestHeader, data);
                            LOGGER.debug("[RESPONSE][00000][12710][" + sessionId + "]" + generalResponseData);

                        } else if ("14010".equals(tradeDetail.getMethod())) {
                            generalResponseData = megaWeixinService.doRequest(requestHeader, data, integrateMchId,
                                    clientIP);
                            LOGGER.debug("[RESPONSE][00000][14010][" + sessionId + "]" + generalResponseData);

                        } else if ("21100".equals(tradeDetail.getMethod())) {
                            generalResponseData = this.getGeneralResponseString(tradeDetail);
                            LOGGER.debug("[RESPONSE][00000][21100][" + sessionId + "]" + generalResponseData);

                        } else if ("10220".equals(tradeDetail.getMethod())) {

                            // NOTICE : there is no single order query or order query in YUANTA
                            generalResponseData = this.getGeneralResponseString(tradeDetail);
                            LOGGER.debug("[RESPONSE][00000][10200][" + sessionId + "]" + generalResponseData);
                        } else if ("10300".equals(tradeDetail.getMethod())) {
                            requestHeader.setMethod("10300");
                            generalResponseData = this.getPiResponse(requestHeader, data, integrateMchId);
                            LOGGER.debug("[RESPONSE][00000][10300][" + sessionId + "]" + generalResponseData);
                        } else if ("10500".equals(tradeDetail.getMethod())) {
                            // OLPAY does not have single order query api
                            if ("OLPay".equals(tradeDetail.getServiceType())) {
                                IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();
                                ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
                                responseGeneralHeader.setMchId(tradeDetail.getAccountId());
                                responseGeneralHeader.setMethod("00000");
                                responseGeneralHeader.setServiceType("SingleOrderQuery");
                                responseGeneralHeader.setStatusCode(SystemInstance.STATUS_SUCCESS);
                                responseGeneralHeader
                                        .setStatusDesc(messageSource.getMessage("response.0000", null, locale));

                                integratedSingleOrderQueryResponseData.setStoreOrderNo(tradeDetail.getOrderId());
                                integratedSingleOrderQueryResponseData
                                        .setSysOrderNo(StringUtils.defaultIfEmpty(tradeDetail.getSystemOrderId(), ""));
                                integratedSingleOrderQueryResponseData.setFeeType("TWD");
                                integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());

                                integratedSingleOrderQueryResponseData
                                        .setOrderStatus(convertSingleOrderQueryStatus(tradeDetail));
                                integratedSingleOrderQueryResponseData.setPaidAt(tradeDetail.getCreateDate());
                                integratedSingleOrderQueryResponseData.setRefundedMsg(tradeDetail.getRefundStatus());
                                RefundDetail refundDetail = refundDetailService
                                        .getOneByOrderIdAndMethod(tradeDetail.getOrderId(), tradeDetail.getMethod());
                                if (Objects.nonNull(refundDetail)) {
                                    integratedSingleOrderQueryResponseData.setRefundedAt(refundDetail.getCreateTime());
                                }

                                ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
                                responseGeneralBody.setHeader(responseGeneralHeader);
                                responseGeneralBody.setData(integratedSingleOrderQueryResponseData);

                                generalResponseData = new Gson().toJson(responseGeneralBody);
                                LOGGER.debug(
                                        "[RESPONSE][00000][10500][OLPAY][" + sessionId + "]" + generalResponseData);
                            } else {
                                requestHeader.setMethod(tradeDetail.getMethod());
                                generalResponseData = allpayService.doRequest(requestHeader, data);
                                LOGGER.debug(
                                        "[RESPONSE][00000][10500][OTHER][" + sessionId + "]" + generalResponseData);
                            }
                        } else if ("20840".equals(tradeDetail.getMethod())) {
                            generalResponseData = this.getGeneralResponseString(tradeDetail);
                            LOGGER.debug("[RESPONSE][00000][20840][" + sessionId + "]" + generalResponseData);
                        } else if ("11300".equals(tradeDetail.getMethod())) {
                            requestHeader.setMethod(tradeDetail.getMethod());
                            // 20191111 edit:sol 因為橘子只能正像查詢(只能查付款狀態)，目前先調整這樣
                            if (StringUtils.isEmpty(tradeDetail.getRefundStatus())) {
                                generalResponseData = gamaPayService.doRequest(requestHeader, data);
                            } else {
                                generalResponseData = this.getGeneralResponseString(tradeDetail);
                            }
                            LOGGER.debug("[RESPONSE][00000][11300][OTHER][" + sessionId + "]" + generalResponseData);
                        } else if ("11400".equals(tradeDetail.getMethod())) {
                            requestHeader.setMethod(tradeDetail.getMethod());
                            generalResponseData = aipeiService.doRequest(requestHeader, data);
                            LOGGER.debug("[RESPONSE][00000][11400][OTHER][" + sessionId + "]" + generalResponseData);
                        } else if ("11500".equals(tradeDetail.getMethod()) || "61500".equals(tradeDetail.getMethod())) {
                            requestHeader.setMethod(tradeDetail.getMethod());
                            generalResponseData = lineService.doRequest(requestHeader, data);
                            LOGGER.debug("[RESPONSE][00000][11500][OTHER][" + sessionId + "]" + generalResponseData);
                        } else if ("20800".equals(tradeDetail.getMethod())) {
                            requestHeader.setMethod(tradeDetail.getMethod());
                            generalResponseData = tsBankService.doRequest(requestHeader, data, integrateMchId,
                                    clientIP);
                            LOGGER.debug("[RESPONSE][00000][20800][OTHER][" + sessionId + "]" + generalResponseData);
                        } else if ("60300".equals(tradeDetail.getMethod())) {
                            requestHeader.setMethod(tradeDetail.getMethod());
                            generalResponseData = this.getInAppResponse(requestHeader, data, integrateMchId);
                            LOGGER.debug("[RESPONSE][00000][60300][OTHER][" + sessionId + "]" + generalResponseData);
                        } else if ("10900".equals(tradeDetail.getMethod()) || "13600".equals(tradeDetail.getMethod())) {
                            // NOTICE : there is no single order query or order query in jkos
                            if (SystemInstance.TYPE_OLPAY.equals(tradeDetail.getServiceType())) {
                                //正掃有查單
                                jkosService.doRequest(requestHeader, data);
                            }
                            tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());
                            generalResponseData = this.getGeneralResponseString(tradeDetail);
                            LOGGER.debug("[RESPONSE][00000][10900][" + sessionId + "]" + generalResponseData);
                        } else if ("13000".equals(requestHeader.getMethod())) {
                            generalResponseData = luckyPayService.doRequest(requestHeader, data);
                            LOGGER.debug("[RESPONSE][00000][13000][" + sessionId + "]" + generalResponseData);
                        } else if ("11100".equals(tradeDetail.getMethod())) {
                            requestHeader.setMethod(tradeDetail.getMethod());
                            generalResponseData = yuanTaOfflineService.doRequest(requestHeader, data);
                            LOGGER.debug("[RESPONSE][00000][11100][OTHER][" + sessionId + "]" + generalResponseData);
                        } else if ("12520".equals(tradeDetail.getMethod())) {
                            requestHeader.setMethod(tradeDetail.getMethod());
                            generalResponseData = alipayShangHaiService.doRequest(requestHeader, data);
                            LOGGER.debug("[RESPONSE][00000][12520][OTHER][" + sessionId + "]" + generalResponseData);
                        } else if ("13500".equals(tradeDetail.getMethod())) {
                            requestHeader.setMethod(tradeDetail.getMethod());
                            generalResponseData = megaTWPayService.doRequest(requestHeader, data);
                            LOGGER.debug("[RESPONSE][00000][13500][OTHER][" + sessionId + "]" + generalResponseData);
                        } else if ("13320".equals(tradeDetail.getMethod())) {
                            generalResponseData = tsGWService.doRequest(requestHeader, data, TsGWServiceImpl.ALIPAY_O);
                            LOGGER.debug("[RESPONSE][00000][13320][" + sessionId + "]" + generalResponseData);
                        } else if ("13410".equals(tradeDetail.getMethod())) {
                            generalResponseData = tsGWService.doRequest(requestHeader, data, TsGWServiceImpl.WEIXIN_O);
                            LOGGER.debug("[RESPONSE][00000][13410][" + sessionId + "]" + generalResponseData);
                        } else if ("13100".equals(tradeDetail.getMethod())) {
                            uuPayService.doRequest(requestHeader, data);
                            tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());
                            generalResponseData = this.getGeneralResponseString(tradeDetail);
                            LOGGER.debug("[RESPONSE][00000][13100][" + sessionId + "]" + generalResponseData);
                        } else if ("15700".equals(tradeDetail.getMethod())) {
                            toBwebAtmService.doRequest(requestHeader, data);
                            tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());
                            generalResponseData = this.getGeneralResponseString(tradeDetail);
                            LOGGER.debug("[RESPONSE][00000][15700][" + sessionId + "]" + generalResponseData);
                        } else if ("15800".equals(tradeDetail.getMethod())) {
                            icashPayService.doRequest(requestHeader, data);
                            tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());
                            generalResponseData = this.getGeneralResponseString(tradeDetail);
                            LOGGER.debug("[RESPONSE][00000][15800][" + sessionId + "]" + generalResponseData);
                        } else if ("15200".equals(tradeDetail.getMethod())) {
                            megaTWPay2Service.doRequest(requestHeader, data);
                            tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());
                            generalResponseData = this.getGeneralResponseString(tradeDetail);
                            LOGGER.debug("[RESPONSE][00000][15200][" + sessionId + "]" + generalResponseData);
                        } else if ("14500".equals(tradeDetail.getMethod())) {
                            hnTwpayService.doRequest(requestHeader, data);
                            tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());
                            generalResponseData = this.getGeneralResponseString(tradeDetail);
                            LOGGER.debug("[RESPONSE][00000][14500][" + sessionId + "]" + generalResponseData);
                        } else if ("15900".equals(tradeDetail.getMethod())) {
                            newPiService.doRequest(requestHeader, data);
                            tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());
                            generalResponseData = this.getGeneralResponseString(tradeDetail);
                            LOGGER.debug("[RESPONSE][00000][15900][" + sessionId + "]" + generalResponseData);
                        } else if ("14200".equals(tradeDetail.getMethod()) || "14300".equals(tradeDetail.getMethod())
                                || "14400".equals(tradeDetail.getMethod()) || "12000".equals(tradeDetail.getMethod())) {
                            requestHeader.setMethod(tradeDetail.getMethod());
                            // 財金回調的才有TxParams，才可以查單
                            if (StringUtils.isNotEmpty(tradeDetail.getTxParams())) {
                                generalResponseData = twPayService.doRequest(requestHeader, data);
                            } else if (SystemInstance.TRADE_FAIL.equals(tradeDetail.getStatus())) {
                                // 這區塊完全針對 台灣PAY提早回調，會更新錯誤訂單，故這邊十秒內忽視 TRADE_FAIL，防止查單出現錯誤狀態
                                if (Duration.between(orderDateTime, localDateTime).toMillis() > 10 * 1000) {
                                    tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
                                    generalResponseData = this.getGeneralResponseString(tradeDetail);
                                }
                            }
                            LOGGER.debug("[RESPONSE][00000][" + tradeDetail.getMethod() + "][OTHER][" + sessionId + "]"
                                    + generalResponseData);
                        }
                    }
                    if (StringUtils.isEmpty(generalResponseData)) {
                        // 若之前都沒有在這邊設定查單API 則以DB為主
                        generalResponseData = this.getGeneralResponseString(tradeDetailService.getOne(tradeDetail.getOrderId()));
                    }

                } else if ("PageQuery".equals(requestHeader.getServiceType())) {
                    JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);

                    String accountId = requestHeader.getMerchantId();
                    String index = requestData.get("Index").getAsString();
                    String pageSize = requestData.get("PageSize").getAsString();
                    String startData = requestData.get("StartDate").getAsString();
                    String endDate = requestData.get("EndDate").getAsString();

                    CustomizePageRequest pageRequest = new CustomizePageRequest();
                    pageRequest.setAccountId(accountId);
                    pageRequest.setIndex(Integer.valueOf(index));
                    pageRequest.setPageSize(Integer.valueOf(pageSize));
                    pageRequest.setStartDate(startData);
                    pageRequest.setEndDate(endDate);

                    List<TradeDetail> list = tradeDetailService.pageList(pageRequest);
                    generalResponseData = getGeneralPagingQueryResponse(list, requestHeader.getMerchantId());
                    LOGGER.debug("[RESPONSE][00000][PageQuery][" + sessionId + "]" + generalResponseData);
                } else if ("OrderQuery1.1".equals(requestHeader.getServiceType())) {
                    return tradeDetailMultiService.doService(body, secretKey, requestHeader);
                }
            } else {
                generalResponseData = "Request Not Supported.";
                LOGGER.error("[RESPONSE][--][" + sessionId + "]" + generalResponseData);
                return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "8002",
                        messageSource.getMessage("response.8002", null, locale));
            }

            LOGGER.info("[RESPONSE][ENCRYPT][" + sessionId + "] [generalResponseData]" + generalResponseData);
            byte[] encryptBytes = AesCryptoUtil.encrypt(secretKey, Constant.IV, generalResponseData);
            IntegratedResponse integratedResponse = new IntegratedResponse();
            integratedResponse.setResult(Base64.encode(encryptBytes).trim().replaceAll("\r?\n", ""));

            String encryptResponse = new Gson().toJson(integratedResponse);
            LOGGER.info("[RESPONSE][ENCRYPT][" + sessionId + "][" + DateTime.now().toString() + "]" + encryptResponse);
            return new ResponseEntity<String>(encryptResponse, HttpStatus.OK);

        } catch (OrderNotFoundException e) {
            LOGGER.error("[EXCEPTION][7002][" + sessionId + "][REQUEST] ");
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "7002",
                    messageSource.getMessage("response.7002", null, locale));
        } catch (OrderAlreadyExistedException e) {
            LOGGER.error("[EXCEPTION][7000][" + sessionId + "][REQUEST] ");
            LOGGER.error("[EXCEPTION][7000][" + sessionId + "]" + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "7000",
                    messageSource.getMessage("response.7000", null, locale));
        } catch (OrderAlreadyCanceledException e) {
            LOGGER.error("[EXCEPTION][7133][" + sessionId + "][REQUEST] ");
            LOGGER.error("[EXCEPTION][7133][" + sessionId + "]" + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "7001",
                    messageSource.getMessage("response.7001", null, locale));
        } catch (OrderAlreadyRefundedException e) {
            LOGGER.error("[EXCEPTION][7137][" + sessionId + "][REQUEST] ");
            LOGGER.error("[EXCEPTION][7137][" + sessionId + "]" + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "7112",
                    messageSource.getMessage("response.7112", null, locale));
        } catch (JsonSyntaxException e) {
            LOGGER.error("[EXCEPTION][8016][" + sessionId + "][REQUEST] ");
            LOGGER.error("[EXCEPTION][8016][" + sessionId + "]" + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "8016",
                    messageSource.getMessage("response.8016", null, locale));
        } catch (InvalidDataFormatException e) {
            LOGGER.error("[EXCEPTION][8002][" + sessionId + "][REQUEST] ");
            LOGGER.error("[EXCEPTION][8002][" + sessionId + "]" + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "8002",
                    messageSource.getMessage("response.8002", null, locale));
        } catch (NoSuchMerchantException e) {
            LOGGER.error("[EXCEPTION][7132][" + sessionId + "][REQUEST] ");
            LOGGER.error("[EXCEPTION][7132][" + sessionId + "]" + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "7132",
                    messageSource.getMessage("response.7132", null, locale));
        } catch (CreateTimeErrorException e) {
            LOGGER.error("[EXCEPTION][7304][" + sessionId + "][REQUEST] ");
            LOGGER.error("[EXCEPTION][7304][" + sessionId + "]" + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "7304",
                    messageSource.getMessage("response.7304", null, locale));
        } catch (DeviceNotFoundException e) {
            LOGGER.error("[EXCEPTION][7106][" + sessionId + "][REQUEST] ");
            LOGGER.error("[EXCEPTION][7106][" + sessionId + "]" + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "7106",
                    messageSource.getMessage("response.7106", null, locale));
        } catch (ServiceTypeNotSupportException e) {
            LOGGER.error("[EXCEPTION][8002][" + sessionId + "][REQUEST] ");
            LOGGER.error("[EXCEPTION][8002][" + sessionId + "]" + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "8002",
                    messageSource.getMessage("response.8002", null, locale));
        } catch (RefundKeyException e) {
            LOGGER.error("[EXCEPTION][7168][" + sessionId + "][REQUEST] ");
//            LOGGER.error("[EXCEPTION][7168][" + sessionId + "]" + e.getMessage() + ", \n" + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "7168",
                    messageSource.getMessage("response.7168", null, locale));
        } catch (DecryptException e) {
            LOGGER.error("[EXCEPTION][8003][" + sessionId + "][REQUEST] ");
            LOGGER.error("[EXCEPTION][8003][" + sessionId + "]" + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "8003",
                    messageSource.getMessage("response.8003", null, locale));
        } catch (AuthCodeErrorException e) {
            LOGGER.error("[EXCEPTION][7120][" + sessionId + "][REQUEST] ");
            LOGGER.error("[EXCEPTION][7120][" + sessionId + "]" + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "7120",
                    messageSource.getMessage("response.7120", null, locale));
        } catch (TradeKeyErrorException e) {
            LOGGER.error("[EXCEPTION][7105][TradeKeyError][" + sessionId + "][REQUEST] ");
//            LOGGER.error("[EXCEPTION][7105][" + sessionId + "]" + e.getMessage() + ", \n" + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));

            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "7105",
                    messageSource.getMessage("response.7105", null, locale));
        } catch (InvalidKeyException e) {
            LOGGER.error("[EXCEPTION][9994][PublicKeyError][" + sessionId + "][REQUEST] ");
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "9994",
                    messageSource.getMessage("response.9994", null, locale));
        } catch (TimeExpireException e) {
            LOGGER.error("[EXCEPTION][7124][TimeExpireException][" + sessionId + "][REQUEST] ");
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "7124",
                    messageSource.getMessage("response.7124", null, locale));
        } catch (InvokeAPIException e) {
            LOGGER.error("[EXCEPTION][8017][InvokeAPIException][" + sessionId + "][REQUEST] ");
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "8017",
                    messageSource.getMessage("response.8017", null, locale));
        } catch (TicketReadCardException e) {
            LOGGER.error("[EXCEPTION][7350][InvokeAPIException][" + sessionId + "][REQUEST] ");
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "7350",
                    messageSource.getMessage("response.7350", null, locale));
        } catch (OtherAPIException e) {
            LOGGER.error("[EXCEPTION][9995][OtherAPIException][" + sessionId + "][REQUEST] ");
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "9995", e.getMessage());
        } catch (AlreadyRetryException e) {
            LOGGER.error("[EXCEPTION][9997][AlreadyRetryException][" + sessionId + "][REQUEST] ");
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "9997", e.getMessage());
        } catch (OrderNoIsBlankException e) {
            LOGGER.error("[EXCEPTION][7102][OrderNoIsBlankException][" + sessionId + "][REQUEST] ");
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "7102", e.getMessage());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION][8002][unknown][" + sessionId + "][REQUEST] ");
            LOGGER.error("[EXCEPTION][8002][unknown][" + sessionId + "]" + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return GeneralResponseMessageUtil.getResponseOfException(secretKey, body, "8002",
                    messageSource.getMessage("response.8002", null, locale));
        }
    }

    private boolean dispatchMicropayPlatform(JsonPrimitive data, String sessionId, RequestHeader requestHeader)
            throws ServiceTypeNotSupportException {
        JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
        String authCodePrefix = requestData.get("AuthCode").getAsString().substring(0, 2);
        String authCode = requestData.get("AuthCode").getAsString();
        String gamapayReg = "[6][0-9]{17}";
        String linepayReg = "[3][1-9][0-9]{16}";
        String aipeiReg = "[8][8][0-9]{16}";
        String allpayReg = "[A][P].*";
        String weixinReg = "[1][0-5].*";
        String PiReg = "[P][I].*";
        String uuPayReg = "^99\\d{18}$";
        String icashpayReg = "[I][C].*";
        String alipayShangHaiReg = "^([2][5-9]|[3][0])" + "\\d{14,22}$";
        String yuanTaOfflineReg = "^([2][5-9]|[3][0])" + "\\d{14,22}$";
        if (authCode.matches(linepayReg)) {
            LOGGER.debug("[Micropay][Dispatch][Line][" + sessionId + "]");
            requestHeader.setMethod("11500");
        } else if ("22".equals(authCodePrefix)) {
            LOGGER.debug("[Micropay][Dispatch][Jkos][" + sessionId + "]");
            requestHeader.setMethod("10900");
        } else if (authCode.matches(allpayReg)) {
            LOGGER.debug("[Micropay][Dispatch][Allpay][" + sessionId + "]");
            requestHeader.setMethod("10500");
        } else if (authCode.matches(weixinReg)) {

            PaymentAccount tsbWeixin = paymentAccountService.getOne(requestHeader.getMerchantId(),
                    Long.valueOf(env.getProperty("bank.id.10110")));
            PaymentAccount ctbcWeixin = paymentAccountService.getOne(requestHeader.getMerchantId(),
                    Long.valueOf(env.getProperty("bank.id.12710")));
            PaymentAccount tsWeixin = paymentAccountService.getOne(requestHeader.getMerchantId(),
                    Long.valueOf(env.getProperty("bank.id.13410")));
            PaymentAccount megaWeixin = paymentAccountService.getOne(requestHeader.getMerchantId(),
                    Long.valueOf(env.getProperty("bank.id.14010")));

            if (null != ctbcWeixin && null != tsbWeixin && null != tsWeixin) {
                throw new ServiceTypeNotSupportException("The Merchant got 2 weixin accounts!");
            } else if (null != tsbWeixin) {
                requestHeader.setMethod("10110");
            } else if (null != ctbcWeixin) {
                requestHeader.setMethod("12710");
            } else if (null != tsWeixin) {
                requestHeader.setMethod("13410");
            } else if (null != megaWeixin) {
                requestHeader.setMethod("14010");
            }
            LOGGER.debug("[Micropay][Dispatch][Weixin][" + sessionId + "]");

        } else if ("25".equals(authCodePrefix) || "26".equals(authCodePrefix) || "27".equals(authCodePrefix) || "28".equals(authCodePrefix) || "29".equals(authCodePrefix) || "30".equals(authCodePrefix)) {
            LOGGER.debug("[Micropay][Dispatch][Alipay][" + sessionId + "]");
            PaymentAccount alipay = paymentAccountService.getOne(requestHeader.getMerchantId(), 02);
            PaymentAccount alipayShangHai = paymentAccountService.getOne(requestHeader.getMerchantId(), 25);
            PaymentAccount yuanTaOffline = paymentAccountService.getOne(requestHeader.getMerchantId(), 11);
            PaymentAccount tsAlipay = paymentAccountService.getOne(requestHeader.getMerchantId(), 33);

            if (alipay != null) {
                requestHeader.setMethod("10220");
            } else if (alipayShangHai != null) {
                LOGGER.debug("[Micropay][Dispatch][AipeiShangHai][" + sessionId + "]");
                requestHeader.setMethod("12520");
            } else if (yuanTaOffline != null) {
                LOGGER.debug("[Micropay][Dispatch][YuanTaOffline][" + sessionId + "]");
                requestHeader.setMethod("11100");
            } else if (tsAlipay != null) {
                LOGGER.debug("[Micropay][Dispatch][TSAlipay][" + sessionId + "]");
                requestHeader.setMethod("13320");
            }
            LOGGER.info(requestHeader.getMethod());
        } else if (authCode.matches(PiReg)) {
            LOGGER.debug("[Micropay][Dispatch][Pi][" + sessionId + "]");
            PaymentAccount pi = paymentAccountService.getOne(requestHeader.getMerchantId(), 3);
            PaymentAccount newPi = paymentAccountService.getOne(requestHeader.getMerchantId(), 59);
            if (pi != null) {
                LOGGER.debug("[Micropay][Dispatch][pi][" + sessionId + "]");
                requestHeader.setMethod("10300");
            } else if (newPi != null) {
                LOGGER.debug("[Micropay][Dispatch][newPi][" + sessionId + "]");
                requestHeader.setMethod("15900");
            }
        } else if (authCode.matches(gamapayReg)) {
            LOGGER.debug("[Micropay][Dispatch][Gamapay][" + sessionId + "]");
            requestHeader.setMethod("11300");
        } else if (authCode.matches(aipeiReg)) {
            LOGGER.debug("[Micropay][Dispatch][Aipei][" + sessionId + "]");
            requestHeader.setMethod("11400");
        } else if (authCode.matches(uuPayReg)) {
            LOGGER.debug("[Micropay][Dispatch][uuPayReg][" + sessionId + "]");
            requestHeader.setMethod("13100");
        } else if (authCode.matches(icashpayReg)) {
            LOGGER.debug("[Micropay][Dispatch][icashpay][" + sessionId + "]");
            requestHeader.setMethod("15800");
        } else if (requestData.get("AuthCode").getAsString().length() > 20) {
            PaymentAccount twPay = paymentAccountService.getOne(requestHeader.getMerchantId(), 20);
            PaymentAccount tobTwPay = paymentAccountService.getOne(requestHeader.getMerchantId(), 42);
            PaymentAccount fbTwPay = paymentAccountService.getOne(requestHeader.getMerchantId(), 43);
            PaymentAccount MegaTwPay = paymentAccountService.getOne(requestHeader.getMerchantId(), 35);
            PaymentAccount lbTwPay = paymentAccountService.getOne(requestHeader.getMerchantId(), 44);
            PaymentAccount tcbTwPay = paymentAccountService.getOne(requestHeader.getMerchantId(), 50);
            PaymentAccount MegaTwPay2 = paymentAccountService.getOne(requestHeader.getMerchantId(), 52);
            PaymentAccount hnTwPay2 = paymentAccountService.getOne(requestHeader.getMerchantId(), 45);
            if (tcbTwPay != null) {
                LOGGER.debug("[Micropay][Dispatch][TCBTwpay][" + sessionId + "]");
                requestHeader.setMethod("15000");
            } else if (twPay != null) {
                LOGGER.debug("[Micropay][Dispatch][TWPay][" + sessionId + "]");
                requestHeader.setMethod("12000");
            } else if (tobTwPay != null) {
                LOGGER.debug("[Micropay][Dispatch][TOBTwPay][" + sessionId + "]");
                requestHeader.setMethod("14200");
            } else if (fbTwPay != null) {
                LOGGER.debug("[Micropay][Dispatch][FBTwPay][" + sessionId + "]");
                requestHeader.setMethod("14300");
            } else if (MegaTwPay2 != null) {
                LOGGER.debug("[Micropay][Dispatch][MegaTwPay2][" + sessionId + "]");
                requestHeader.setMethod("15200");
            } else if (MegaTwPay != null) {
                LOGGER.debug("[Micropay][Dispatch][MegaTWPay][" + sessionId + "]");
                requestHeader.setMethod("13500");
            } else if (lbTwPay != null) {
                LOGGER.debug("[Micropay][Dispatch][LBTWPay][" + sessionId + "]");
                requestHeader.setMethod("14400");
            } else if (hnTwPay2 != null) {
                LOGGER.debug("[Micropay][Dispatch][HNTWPay][" + sessionId + "]");
                requestHeader.setMethod("14500");
            }
            LOGGER.info(requestHeader.getMethod());
        } else if (requestData.get("AuthCode").getAsString().length() == 12) {
            // todo makesure the test code rule
            LOGGER.debug("[Micropay][Dispatch][LineTest][" + sessionId + "]");
            requestHeader.setMethod("11500");
        } else {
            return false;
        }

        return true;
    }

    private String getGeneralResponseString(TradeDetail tradeDetail) {
        IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = this.getIntegratedSingleOrderQueryResponseData(tradeDetail);

        ResponseGeneralHeader responseGeneralHeader = getResponseGeneralHeader(tradeDetail);

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        responseGeneralBody.setHeader(responseGeneralHeader);
        responseGeneralBody.setData(integratedSingleOrderQueryResponseData);

        return new Gson().toJson(responseGeneralBody);
    }

    private ResponseGeneralHeader getResponseGeneralHeader(TradeDetail tradeDetail) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setMchId(tradeDetail.getAccountId());
        responseGeneralHeader.setMethod(tradeDetail.getMethod());
        responseGeneralHeader.setServiceType("SingleOrderQuery");
        responseGeneralHeader.setStatusCode(SystemInstance.STATUS_SUCCESS);
        responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        return responseGeneralHeader;
    }

    private IntegratedSingleOrderQueryResponseData getIntegratedSingleOrderQueryResponseData(TradeDetail tradeDetail) {
        IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();
        integratedSingleOrderQueryResponseData.setSysOrderNo(StringUtils.defaultIfEmpty(tradeDetail.getSystemOrderId(), ""));
        integratedSingleOrderQueryResponseData.setStoreOrderNo(tradeDetail.getOrderId());

        integratedSingleOrderQueryResponseData.setOrderStatus(this.convertSingleOrderQueryStatus(tradeDetail));
        integratedSingleOrderQueryResponseData.setPaidAt(tradeDetail.getCreateDate());
        integratedSingleOrderQueryResponseData.setRefundedMsg(tradeDetail.getRefundStatus());

        RefundDetail refundDetail = refundDetailService.getOneByOrderIdAndMethod(tradeDetail.getOrderId(), tradeDetail.getMethod());
        if (Objects.nonNull(refundDetail)) {
            integratedSingleOrderQueryResponseData.setRefundedAt(refundDetail.getCreateTime());
        }
        if (TicketLogicService.ticketMethodList.stream().anyMatch(s -> s.equals(tradeDetail.getMethod())) || "14900".equals(tradeDetail.getMethod())) {
            integratedSingleOrderQueryResponseData.setCardNum(tradeDetail.getUserId());
        }

        integratedSingleOrderQueryResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
        integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());
        return integratedSingleOrderQueryResponseData;
    }

    private String convertSingleOrderQueryStatus(TradeDetail tradeDetail) {
        String status = "3";
        if (SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus()) && StringUtils.isEmpty(tradeDetail.getRefundStatus())) {
            status = "1";
        } else if (SystemInstance.TRADE_WAITING.equals(tradeDetail.getStatus())
                || SystemInstance.TRADE_CREATING_ORDER.equals(tradeDetail.getStatus())) {
            status = "0";
        } else if (SystemInstance.TRADE_FAIL.equals(tradeDetail.getStatus())) {
            status = "2";
        } else if (SystemInstance.TRADE_INTERRUPTED.equals(tradeDetail.getStatus())) {
            status = "4";
        }

        return status;
    }

    private String getGeneralPagingQueryResponse(List<TradeDetail> tradeDetailList, String merchantId) throws JsonProcessingException {

        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setStatusCode(SystemInstance.STATUS_SUCCESS);
        responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        responseGeneralHeader.setMethod("00000");
        responseGeneralHeader.setServiceType("PageQuery");
        responseGeneralHeader.setMchId(merchantId);

        ObjectMapper mapper = new ObjectMapper();
        IntegratedPageQueryResponseData data = new IntegratedPageQueryResponseData();
        data.setList(mapper.writeValueAsString(tradeDetailList));

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        responseGeneralBody.setHeader(responseGeneralHeader);
        responseGeneralBody.setData(data);

        return new Gson().toJson(responseGeneralBody);
    }

    private String getGeneralOrderQueryResponse(List<TradeDetail> tradeDetailList, String merchantId) {
        List<IntegratedOrder> integratedOrderList = new ArrayList<IntegratedOrder>();

        for (TradeDetail tradeDetail : tradeDetailList) {
            IntegratedOrder integratedOrder = new IntegratedOrder();
            integratedOrder.setSysOrderNo(StringUtils.defaultIfEmpty(tradeDetail.getSystemOrderId(), ""));
            integratedOrder.setStoreOrderNo(tradeDetail.getOrderId());
            integratedOrder.setCashier(tradeDetail.getCashier());
            integratedOrder.setDetail(tradeDetail.getDetail());
            integratedOrder.setFeeType("TWD");
            integratedOrder.setTotalFee(tradeDetail.getPayment());
            integratedOrder.setStatus(tradeDetail.getStatus());
            integratedOrder.setRefundStatus(tradeDetail.getRefundStatus());
            integratedOrder.setDescription(tradeDetail.getDescription());
            integratedOrder.setMethod(tradeDetail.getMethod());
            integratedOrderList.add(integratedOrder);
        }

        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setStatusCode(SystemInstance.STATUS_SUCCESS);
        responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        responseGeneralHeader.setMethod("00000");
        responseGeneralHeader.setServiceType("OrderQuery");
        responseGeneralHeader.setMchId(merchantId);
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        IntegratedOrderQueryResponseData integratedOrderQueryResponseData = new IntegratedOrderQueryResponseData();
        integratedOrderQueryResponseData.setList(integratedOrderList);

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        responseGeneralBody.setHeader(responseGeneralHeader);
        responseGeneralBody.setData(integratedOrderQueryResponseData);

        return new Gson().toJson(responseGeneralBody);
    }

    private String getPiResponse(RequestHeader requestHeader, JsonPrimitive data, String integrateMchId) throws Exception {

        long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
        PaymentAccount paymentAccount = paymentAccountService.getOne(requestHeader.getMerchantId(), bankId);
        final String channlId = StringUtils.defaultIfBlank(paymentAccount.getPlatformCheckCode(), "INTELLA");
        if ("Refund".equals(requestHeader.getServiceType())) {

            PiRefundRequestData requestData = new Gson().fromJson(data.getAsString(), PiRefundRequestData.class);

            if (Strings.isNullOrEmpty(requestHeader.getCreateTime())
                    || Strings.isNullOrEmpty(requestHeader.getMerchantId())
                    || requestHeader.getCreateTime().length() != 14 || Strings.isNullOrEmpty(requestData.getRefundFee())
//                    || Strings.isNullOrEmpty(requestData.getRefundedMsg())
                    || (Strings.isNullOrEmpty(requestData.getStoreOrderNo())
                    && Strings.isNullOrEmpty(requestData.getSysOrderNo()))) {
                throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
            }

            requestData.setMchId(channlId);
            requestData.setCreateTime(requestHeader.getCreateTime());
            return processPiRequest(requestHeader, data, integrateMchId, requestData);

        } else if (SystemInstance.TYPE_MICROPAY.equals(requestHeader.getServiceType())) {

            PiMicropayRequestData requestData = new Gson().fromJson(data.getAsString(), PiMicropayRequestData.class);

            if (Strings.isNullOrEmpty(requestHeader.getCreateTime())
                    || Strings.isNullOrEmpty(requestHeader.getMerchantId())
                    || requestHeader.getCreateTime().length() != 14
                    || Strings.isNullOrEmpty(requestData.getStoreOrderNo())
                    || Strings.isNullOrEmpty(requestData.getBody()) || Strings.isNullOrEmpty(requestData.getTotalFee())
                    || Strings.isNullOrEmpty(requestData.getAuthCode())) {
                throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
            }

            requestData.setMchId(channlId);
            requestData.setCreateTime(requestHeader.getCreateTime());
            requestData.setStoreInfo(paymentAccount.getAccount());
            requestData.setAttach("0000000001");
            // pi device id max length : 2
            requestData.setDeviceInfo("1");
            return processPiRequest(requestHeader, data, integrateMchId, requestData);

        } else if ("SingleOrderQuery".equals(requestHeader.getServiceType())) {
            PiSingleOrderQueryRequestData requestData = new Gson().fromJson(data.getAsString(),
                    PiSingleOrderQueryRequestData.class);

            if (Strings.isNullOrEmpty(requestHeader.getMerchantId())
                    || (Strings.isNullOrEmpty(requestData.getStoreOrderNo())
                    && Strings.isNullOrEmpty(requestData.getSysOrderNo()))) {
                throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
            }

//            requestData.setMchId(paymentAccount.getAccount());
            requestData.setMchId(channlId);
            return processPiRequest(requestHeader, data, integrateMchId, requestData);

        } else if ("OrderQuery".equals(requestHeader.getServiceType())) {

//            PiOrderQueryRequestData piOrderQueryRequestData = new Gson().fromJson(data.getAsString(), PiOrderQueryRequestData.class);
//            piOrderQueryRequestData.setMchId(paymentAccount.getAccount());
//            return piService.doRequest(piOrderQueryRequestData, requestHeader);

        } else if ("Cancel".equals((requestHeader.getServiceType()))) {

            PiCancelRequestData requestData = new Gson().fromJson(data.getAsString(), PiCancelRequestData.class);

            if (requestHeader.getMerchantId().equals("") || requestData.getStoreOrderNo().equals("")) {
                throw new InvalidDataFormatException(SystemInstance.EXCEPTION_MESSAGE_DATA_FORMAT);
            }

            requestData.setMchId(paymentAccount.getAccount());
            return processPiRequest(requestHeader, data, integrateMchId, requestData);
        }

        return "Service Type Not Support.";
    }

    private String processPiRequest(RequestHeader requestHeader, JsonPrimitive data, String integrateMchId,
                                    PiBasicRequestData requestData) throws Exception {
        String generalResponseData;
        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);
        generalResponseData = piService.doRequest(requestData, requestHeader, tradeDetail, integrateMchId);
        tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, generalResponseData, data);
        return generalResponseData;
    }

    private String getInAppResponse(RequestHeader requestHeader, JsonPrimitive data, String integrateMchId)
            throws Exception {

        if ("InApp".equals(requestHeader.getServiceType())) {

            tradeProcedureService.setTradeDetailRequest(requestHeader, data);

            String generalResponseData;
            IntegratedInAppResponseData integratedInAppResponseData = new IntegratedInAppResponseData();

            ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
            responseGeneralHeader.setStatusCode(SystemInstance.STATUS_SUCCESS);
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));

            responseGeneralHeader.setServiceType(requestHeader.getServiceType());
            responseGeneralHeader.setMethod(requestHeader.getMethod());
            responseGeneralHeader.setMchId(integrateMchId);
            responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
            integratedInAppResponseData.setPartnerId(requestHeader.getMerchantId());

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedInAppResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);
            LOGGER.info("integrate response ready :" + generalResponseData);

            return generalResponseData;

        } else if ("SingleOrderQuery".equals(requestHeader.getServiceType())) {

            // todo update trade detail

            PiServerOrderQueryRequest requestData = new Gson().fromJson(data.getAsString(),
                    PiServerOrderQueryRequest.class);

            TradeDetail tradeDetail = tradeDetailService.getOne(requestHeader.getMerchantId(),
                    requestData.getPartnerOrderId());

            if (tradeDetail == null) {
                throw new OrderNotFoundException(SystemInstance.ORDER_NOT_FOUND);
            }

            PaymentAccount paymentAccount = paymentAccountService.getOne(requestHeader.getMerchantId(), 7);
            requestData.setPartnerId(paymentAccount.getAccount());
            requestData.setPartnerKey(tradeDetail.getMchKey());

            String generalResponseData = piInAppService.doRequest(requestData, requestHeader);

            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, generalResponseData, data);

            // todo convert generalResponseData to be object and set value into
            // IntegratedSingleOrderQueryResponseData
//            // todo success, fail, refund status.

            LOGGER.debug("integrate response ready :" + generalResponseData);

            return generalResponseData;

        } else if ("Refund".equals(requestHeader.getServiceType())) {

            PiServerRefundRequest requestData = new Gson().fromJson(data.getAsString(), PiServerRefundRequest.class);

            TradeDetail tradeDetail = tradeDetailService.getOne(requestHeader.getMerchantId(),
                    requestData.getPartnerOrderId());

            if (tradeDetail == null) {
                throw new OrderNotFoundException(SystemInstance.ORDER_NOT_FOUND);
            }

            requestData.setPartnerMoney(String.valueOf(tradeDetail.getPayment()));
            requestData.setPartnerKey(tradeDetail.getMchKey());

            PaymentAccount paymentAccount = paymentAccountService.getOne(requestHeader.getMerchantId(), 7);
            requestData.setPartnerId(paymentAccount.getAccount());

            tradeProcedureService.setTradeDetailRequest(requestHeader, data);
            String generalResponseData = piInAppService.doRequest(requestData, requestHeader);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, generalResponseData, data);

            LOGGER.debug("[RESPONSE] integrate response ready :" + generalResponseData);

            return generalResponseData;
        } else {
            LOGGER.error("Service type not support.");
            return "NOT SUPPORT.";
        }
    }

    private String getWeixinResponse(RequestHeader requestHeader, JsonPrimitive data) throws Exception {

        String generalResponseData = SystemInstance.EMPTY_STRING;
        String integrateMchId = requestHeader.getMerchantId();

        if (SystemInstance.TYPE_MICROPAY.equals(requestHeader.getServiceType())) {
            LOGGER.info("enter getWeixinResponse :" + requestHeader);
            tradeProcedureService.setTradeDetailRequest(requestHeader, data);

            WeixinMicropayRequestData requestData = new Gson().fromJson(data.getAsString(),
                    WeixinMicropayRequestData.class);
            requestData.setVer("101");
            requestData.setMethod("10");
            requestData.setMchId(requestHeader.getMerchantId());
            requestData.setTradeType("MICROPAY");

            String result = weixinService.micropay(requestData);
            LOGGER.info("[WEIXIN][Response] :" + result);

            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);

            JsonObject json = new Gson().fromJson(result, JsonObject.class);
            ResponseGeneralHeader responseGeneralHeader = getWeixinResponseHeader(result, requestHeader);

            IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();

            // fixme
            JsonObject dataJson = new Gson().fromJson(json.get("Data").toString(), JsonObject.class);
            JsonObject dataValueJson;
            if (dataJson.get("DataValue").isJsonNull()) {
                integratedMicropayResponseData.setSysOrderNo(null);
            } else {
                dataValueJson = new Gson().fromJson(dataJson.get("DataValue").getAsString(), JsonObject.class);
                integratedMicropayResponseData.setSysOrderNo(dataValueJson.get("SYS_ORDER_NO").getAsString());
            }

            integratedMicropayResponseData.setStoreOrderNo(requestData.getOutTradeNo());
            integratedMicropayResponseData.setTotalFee(requestData.getTotalFee());
            integratedMicropayResponseData.setAuthCode(requestData.getAuthCode());
//            integratedMicropayResponseData.setPlatformRsp(json.toString());

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedMicropayResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);

        } else if ("Currency".equals(requestHeader.getServiceType())) {
            WeixinCurrencyRequestData requestData = new Gson().fromJson(data.getAsString(),
                    WeixinCurrencyRequestData.class);
            long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
            PaymentAccount paymentAccount = paymentAccountService.getOne(requestHeader.getMerchantId(), bankId);

            requestData.setVer("101");
            requestData.setMethod("10");
            requestData.setTradeType("EXCHANGECURRENCY");
            requestData.setMchId(paymentAccount.getAccount());

            String result = weixinService.rateQuery(requestData);

            JsonObject json = new Gson().fromJson(result, JsonObject.class);

            IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();

            JsonObject dataJson = new Gson().fromJson(json.get("Data").toString(), JsonObject.class);

            try {
                JsonObject DataValueJson = new Gson().fromJson(dataJson.get("DataValue").toString(), JsonObject.class);
                integratedRefundResponseData.setSysRefundNo(DataValueJson.get("SYS_REFUND_NO").toString());
            } catch (Exception e) {
                integratedRefundResponseData.setSysOrderNo(null);
            }

            integratedRefundResponseData.setStoreRefundNo(requestData.getCurrency());

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setData(integratedRefundResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);

        } else if ("Refund".equals(requestHeader.getServiceType())) {
            TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

            WeixinRefundRequestData requestData = new Gson().fromJson(data.getAsString(),
                    WeixinRefundRequestData.class);
            requestData.setVer("101");
            requestData.setMethod("10");
            requestData.setMchId(requestHeader.getMerchantId());
            requestData.setTradeType("REFUND");
            requestData.setOutTradeNo(requestData.getOutTradeNo());
            requestData.setNonceStr(SystemInstance.EMPTY_STRING);
            requestData.setTransactionId(tradeDetail.getSystemOrderId());
            requestData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
            LOGGER.info("enter Weixin refund:" + requestHeader);
            String result = weixinService.refund(requestData);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);

            ResponseGeneralHeader responseGeneralHeader = getWeixinResponseHeader(result, requestHeader);
            JsonObject json = new Gson().fromJson(result, JsonObject.class);

            IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();

            JsonObject dataJson = new Gson().fromJson(json.get("Data").toString(), JsonObject.class);

            try {
                JsonObject DataValueJson = new Gson().fromJson(dataJson.get("DataValue").toString(), JsonObject.class);
                integratedRefundResponseData.setSysRefundNo(DataValueJson.get("SYS_REFUND_NO").toString());
            } catch (Exception e) {
                integratedRefundResponseData.setSysOrderNo(null);
            }
            integratedRefundResponseData.setStoreRefundNo(requestData.getOutRefundNo());
            integratedRefundResponseData.setStoreOrderNo(requestData.getOutTradeNo());
            integratedRefundResponseData.setSysOrderNo(requestData.getTransactionId());
            integratedRefundResponseData.setRefundedAt(responseGeneralHeader.getResponseTime());

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedRefundResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);

        } else if ("SingleOrderQuery".equals(requestHeader.getServiceType())) {
            LOGGER.info("begin weixin single query");
            long bankId = Long.valueOf(env.getProperty("bank.id." + requestHeader.getMethod()));
            PaymentAccount paymentAccount = paymentAccountService.getOne(requestHeader.getMerchantId(), bankId);

            WeixinSingleQueryRequestData requestData = new Gson().fromJson(data.getAsString(),
                    WeixinSingleQueryRequestData.class);

            requestData.setVer("101");
            requestData.setMethod("10");
            requestData.setTradeType("ORDERQUERY");
            requestData.setMchId(paymentAccount.getAccount());

            tradeProcedureService.setTradeDetailRequest(requestHeader, data);

            String result = weixinService.singleQuery(requestData);
            LOGGER.debug("[weixin][firstResult]" + result);
            result = weixinService.updateOrder(requestData);
            LOGGER.debug("[weixin][updateResult]" + result);

            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);

            ResponseGeneralHeader responseGeneralHeader = getWeixinResponseHeader(result, requestHeader);
            JsonObject json = new Gson().fromJson(result, JsonObject.class);

            IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();

            JsonObject dataJson = new Gson().fromJson(json.get("Data").toString(), JsonObject.class);
            JsonObject dataValueJson = new JsonObject();

            if (dataJson.has("DataValue") && dataJson.get("DataValue").isJsonPrimitive()) {
                dataValueJson = JsonUtil.convertJsonPrimitiveToJsonObject(dataJson.getAsJsonPrimitive("DataValue"));
            }

            // integratedSingleOrderQueryResponseData.setStoreOrderNo(dataValueJson.has("SYS_REFUND_NO")
            // ? dataValueJson.get("SYS_REFUND_NO").toString() : null);
            integratedSingleOrderQueryResponseData.setStoreOrderNo(requestData.getOutTradeNo());
            integratedSingleOrderQueryResponseData.setSysOrderNo(
                    dataValueJson.has("WEIXIN_TRANSACTION_ID") ? dataValueJson.get("WEIXIN_TRANSACTION_ID").toString()
                            : "");
            if (dataValueJson.has("SYS_STATUS")) {
                TradeDetail tradeDetail = tradeDetailService.getOne(requestData.getOutTradeNo());
                if (dataValueJson.get("SYS_STATUS").getAsString().equals("4")) {
                    RefundDetail refundDetail = refundDetailService.getOneByOrderIdAndMethod(tradeDetail.getOrderId(),
                            tradeDetail.getMethod());
                    if (Objects.nonNull(refundDetail)) {
                        integratedSingleOrderQueryResponseData.setRefundedAt(refundDetail.getCreateTime());
                    }
                }

                integratedSingleOrderQueryResponseData.setOrderStatus(
                        dataValueJson.has("SYS_STATUS") ? this.convertSingleOrderQueryStatus(tradeDetail) : "0");
            }
            integratedSingleOrderQueryResponseData.setOrderStatusDesc("TODO");

            TradeDetail tradeDetail = tradeDetailService.getOne(requestData.getOutTradeNo());
            if (Objects.nonNull(tradeDetail)) {
                integratedSingleOrderQueryResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
                integratedSingleOrderQueryResponseData.setPaidAt(tradeDetail.getCreateDate());
                integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());
                integratedSingleOrderQueryResponseData.setRefundedMsg(tradeDetail.getRefundStatus());
            }

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedSingleOrderQueryResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);
        }
        return generalResponseData;
    }

    private String getCTBCWeixinResponse(RequestHeader requestHeader, JsonPrimitive data) throws Exception {

        String generalResponseData = SystemInstance.EMPTY_STRING;
        String integrateMchId = requestHeader.getMerchantId();

        JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
        TradeDetail tradeDetail = tradeProcedureService.setTradeDetailRequest(requestHeader, data);

        PaymentAccount ctbcWeixinAccount = paymentAccountService.getOne(integrateMchId,
                Long.valueOf(env.getProperty("bank.id.12710")));
        if (SystemInstance.TYPE_MICROPAY.equals(requestHeader.getServiceType())) {

            LOGGER.info("[getMerchantId] :" + integrateMchId);
            LOGGER.info("[bank] :" + Long.valueOf(env.getProperty("bank.id.12710")));

            Map<String, String> map = new HashMap<String, String>();
            map.put("mchId", Long.toString(ctbcWeixinAccount.getMerchant().getMerchantSeqId()));
            map.put("deviceInfo", requestData.get("DeviceInfo").getAsString());
            map.put("authCode", requestData.get("AuthCode").getAsString());

            map.put("totalFee", requestData.get("TotalFee").getAsString());
            if (requestData.has("Cashier")) {
                map.put("cashier", requestData.get("Cashier").getAsString());
            } else {
                map.put("cashier", "00000000");
            }
            map.put("storeOrderNo", requestData.get("StoreOrderNo").getAsString());
            map.put("s2pOrderNo", requestData.get("StoreOrderNo").getAsString());
            map.put("systemOrderTime", requestHeader.getCreateTime());
            if (requestData.has("bonusType") && "none".equals(requestData.get("bonusType").getAsString().trim())) {
                map.put("bonusType", requestData.get("none").getAsString());
            }
            if (requestData.has("Body")) {
                map.put("productDesc", requestData.get("Body").getAsString());
            } else {
                map.put("productDesc", "The merchant did not input product description.");
            }
            if (requestData.has("Detail")) {
                map.put("productDetail", requestData.get("Detail").getAsString());
            } else {
                map.put("productDetail", "The merchant did not input product detail.");
            }

//       	   map.put("rsaKey", "-----BEGIN RSA PRIVATE KEY-----\nMIIEogIBAAKCAQEAmGfItbOY6rKHDvsyXqzwC+/GjPdNdaccJuZbyzQlZ/6/ASeZ\nd9n/8WF/zQdxnERRo9Xm4sZ8adE0UNoFnXZQ9qfvnqtQA+TTnwrS28VqmS13gtu9\nYhqr34cY3ImKe8k7msCae6uwE4XNaMvi3cNZ6PrsJZeK/foGGy142NHcyqpHQUon\nWiHAHQTvQc/shwDxwvRrK0fidAK4bUNkJEtoxBg/tLa9nkQprt7B+9J0BIdjiLXz\nEFM0yhnhymst7O/tLH9nQZiqlIayyJqepcJz6gXJvWCQIVWHcShFJfXl1OfnqKkm\npnBj7UqKVKrXZsVTpc/BQlERwf8CYBeqL9ICgwIDAQABAoIBAEq9Ip2QvJ3j/P2o\nzFPHhVSlOn5ozLC0ycZ902g8Y1ezguHDFKzE2JH8GVtnmhxjGJI61KZDZkHSWVCJ\nCpXkruG3C3iJVI0ES49EFJG4J4WE2YXv0tjocX8F1nhYbvQzhCpo55lcYdXBGTGa\nQuAz68SoQORSg9P3J8VuMERL7nXn95pA1LLcYYm8B5Nq2dq+YJrsBopGaIfc2m8H\nF4ottIQFQ8iHDk5tGmcEHuyV9vhW0ozQ+hifJ9agle6ZZ2madFJUTMkchTm4gCCj\nSWf+wNlC5rXPLT5d/u69mG7jg7scNK67M0dXJGtRme5YaMWwDiyAfUMRxe4y/2XN\nY+wdy+ECgYEAxzi9iK5R+H3JHueHvPJKXyDr7mNOOqPgg+LHd7dJH5t/iIJ18lfD\n5Zu65oeNNKlnBQ+sQ9+Sgz2421rVmkeYTLAq8BpPPGSlDOYEmCugcFPZmqpKMM+Z\n27gwfX2kRW9jceeUl59PLZw4oo/pIz4yD/0UXgwIg5dhm8ZIx8pJyVECgYEAw9dR\n1eXQpx1B9/oatY/qiUmCNgOe3AYo7PkIHC0d0xj8caj2dlYkToZAejcEd+p1Hyy1\ni+grg4zGHhd128GvKtCE7xnlAgfpyjLBJk34rst4mKBay3H7q57/GUZsrA+WUe8v\nWq7Sa5wIO72BY+HmTgBxEn6DbXf9xwH1GsOCmZMCgYAnu9JZg2MH65aEWmtBLRXW\nOUu1BQtiOLMbusxLvTuRWUMrZ9G8XvLMRa4WMmzgVvUcFk8mTl37XQEUht5ZYfQn\nP2+g6a5p/izJEMtTyke6f47EATjA2Xb3UpohPUDZNGkiG60QPI7fKuxEx+0YwVvc\nwn4gb8f27udh2T9E0by7sQKBgGsD6jAcGNx43GeJP7cfIWZLyN+DMvfAVfTIUGo1\nLSsBaFEjpyobAFP4gJtIHST59W5HnB0LjVuhPVGMOFcRi4q1zy5o0/88G8Dl4y3v\nRYkenpOobHTnoEKz62oAVbnf+qBjN1kyVsF+zfD3KRFPi+3rx933TBIxk3d/1lP9\nqgwJAoGAdJ+sLNwUoaLkt14Ln2Yh8Mx94J+rNh5BmkAFKay2pFRqiAG9OtxrEJBc\nvFePx0LuEUGSM9Xa0PfVY+YeApWBV2v0zlekxuv42vthTr2YDWCwJnhfoozzlxrS\niufXUSvyKpUN2zJLeBUbC/e1BYiP1M128iX23O9aclH646Gd9AM=\n-----END RSA PRIVATE KEY-----");
            map.put("rsaKey", ctbcWeixinAccount.getHashIV());
            map.put("thirdPartyAccount", ctbcWeixinAccount.getAppId());
            String weixinRequest = new Gson().toJson(map);

            LOGGER.info("[TRANSACTION] CTBC weixin callJsApiNoReturn start. :" + weixinRequest);
            String result = HttpRequestUtil.post(CTBC_URL + "/ctbc/api/wechat/passively/pay", weixinRequest);
            LOGGER.info("[TRANSACTION] CTBC weixin callJsApiNoReturn end.: " + result);
//           response:{"activelyQRCodeURL":null,"status":"I0000","stateDesc":"作業成功","amount":null,"orderNo":null,"bankNo":null,"refundOrderNo":null,"thirdPartyOrderNo":null,"resRawData":"{\"Request\":{\"Header\":{\"ServiceName\":\"Pay\",\"Version\":\"1.0\",\"ApiUserId\":\"FoGuangShan_uat\"},\"Data\":{\"PayType\":\"Token\",\"CardToken\":\"134657097309268381\",\"TxAmt\":{\"Ccy\":\"TWD\",\"Amt\":1},\"StoreId\":\"31\",\"CorpTxSeq\":\"2018103123310031\",\"OrderNo\":\"2018103123310031\",\"CorpTxTime\":\"2018/11/0623:59:59\",\"BonusType\":\"ByWallet\",\"PosNo\":\"00000000\",\"SalesTitle\":\"-1046\",\"SalesDetail\":\"Themerchantdidnotinputproductdetail.\"}},\"Response\":{\"Header\":{\"ServiceResult\":\"Y\",\"StatusCode\":\"I0000\",\"StatusDesc\":\"作業成功\",\"ResponseTime\":\"2018/11/0601:36:44\",\"AdditionalInfo\":\"\"},\"Data\":{\"TxSeq\":\"TX20181106000113860\",\"WalletId\":\"WeChat\",\"WalletTxTime\":\"2018/11/0601:36:44\",\"WalletTxSeq\":\"NFIUAT20181106005252\",\"DebitAmt\":\"1\",\"ChannelStatusCode\":\"SUCCESS\",\"ChannelStatusDesc\":\"\",\"PaymentChannel\":\"CreditCard\",\"BalanceAmt\":0,\"TxAmt\":{\"Ccy\":\"TWD\",\"Amt\":1},\"ChargeMode\":\"Auto\",\"PayMediaInfo\":{\"ShaMediaId\":\"\",\"MaskedMediaId\":\"\",\"PayMediaId\":\"\",\"ShaMediaType\":\"\"},\"SettleUnit\":\"CTBC\"}}}"}

            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);

            JsonObject resjson = new Gson().fromJson(result, JsonObject.class);
            ResponseGeneralHeader responseGeneralHeader = getWeixinResponseHeader(result, requestHeader);

            IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();

            if (resjson.get("thirdPartyOrderNo").isJsonNull()) {
                integratedMicropayResponseData.setSysOrderNo(null);
            } else {
                integratedMicropayResponseData.setSysOrderNo(resjson.get("thirdPartyOrderNo").getAsString());
            }

            integratedMicropayResponseData.setStoreOrderNo(requestData.get("StoreOrderNo").getAsString());
//           {"activelyQRCodeURL":null,"status":"0000","stateDesc":"paid","amount":null,"orderNo":"2018110823310101","refundOrderNo":null,"thirdPartyOrderNo":"TX20181108000113920","userPayTime":null,"userRefundTime":null,"orderTxTime":null,"resRawData":null}
            if (resjson.has("amount") && !resjson.get("amount").isJsonNull()
                    && !resjson.get("amount").getAsString().isEmpty()) {
                integratedMicropayResponseData.setTotalFee(resjson.get("amount").getAsString());
            } else {
                integratedMicropayResponseData.setTotalFee("");
            }

            integratedMicropayResponseData.setAuthCode(requestData.get("AuthCode").getAsString());

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedMicropayResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);

        } else if ("Refund".equals(requestHeader.getServiceType())) {

            if (!Long.toString(tradeDetail.getPayment()).equals(requestData.get("RefundFee").getAsString())) {
                throw new ServiceTypeNotSupportException("we don't provide partial refund");
            }
            Map<String, String> map = new HashMap<String, String>();
//        	{"refundOrderNo":"TX20181105000113840","s2pOrderNo":"2018103123310029","mchId":"31","totalFee":"1","rsaKey":"-----BEGIN RSA PRIVATE KEY-----\nMIIEogIBAAKCAQEAmGfItbOY6rKHDvsyXqzwC+/GjPdNdaccJuZbyzQlZ/6/ASeZ\nd9n/8WF/zQdxnERRo9Xm4sZ8adE0UNoFnXZQ9qfvnqtQA+TTnwrS28VqmS13gtu9\nYhqr34cY3ImKe8k7msCae6uwE4XNaMvi3cNZ6PrsJZeK/foGGy142NHcyqpHQUon\nWiHAHQTvQc/shwDxwvRrK0fidAK4bUNkJEtoxBg/tLa9nkQprt7B+9J0BIdjiLXz\nEFM0yhnhymst7O/tLH9nQZiqlIayyJqepcJz6gXJvWCQIVWHcShFJfXl1OfnqKkm\npnBj7UqKVKrXZsVTpc/BQlERwf8CYBeqL9ICgwIDAQABAoIBAEq9Ip2QvJ3j/P2o\nzFPHhVSlOn5ozLC0ycZ902g8Y1ezguHDFKzE2JH8GVtnmhxjGJI61KZDZkHSWVCJ\nCpXkruG3C3iJVI0ES49EFJG4J4WE2YXv0tjocX8F1nhYbvQzhCpo55lcYdXBGTGa\nQuAz68SoQORSg9P3J8VuMERL7nXn95pA1LLcYYm8B5Nq2dq+YJrsBopGaIfc2m8H\nF4ottIQFQ8iHDk5tGmcEHuyV9vhW0ozQ+hifJ9agle6ZZ2madFJUTMkchTm4gCCj\nSWf+wNlC5rXPLT5d/u69mG7jg7scNK67M0dXJGtRme5YaMWwDiyAfUMRxe4y/2XN\nY+wdy+ECgYEAxzi9iK5R+H3JHueHvPJKXyDr7mNOOqPgg+LHd7dJH5t/iIJ18lfD\n5Zu65oeNNKlnBQ+sQ9+Sgz2421rVmkeYTLAq8BpPPGSlDOYEmCugcFPZmqpKMM+Z\n27gwfX2kRW9jceeUl59PLZw4oo/pIz4yD/0UXgwIg5dhm8ZIx8pJyVECgYEAw9dR\n1eXQpx1B9/oatY/qiUmCNgOe3AYo7PkIHC0d0xj8caj2dlYkToZAejcEd+p1Hyy1\ni+grg4zGHhd128GvKtCE7xnlAgfpyjLBJk34rst4mKBay3H7q57/GUZsrA+WUe8v\nWq7Sa5wIO72BY+HmTgBxEn6DbXf9xwH1GsOCmZMCgYAnu9JZg2MH65aEWmtBLRXW\nOUu1BQtiOLMbusxLvTuRWUMrZ9G8XvLMRa4WMmzgVvUcFk8mTl37XQEUht5ZYfQn\nP2+g6a5p/izJEMtTyke6f47EATjA2Xb3UpohPUDZNGkiG60QPI7fKuxEx+0YwVvc\nwn4gb8f27udh2T9E0by7sQKBgGsD6jAcGNx43GeJP7cfIWZLyN+DMvfAVfTIUGo1\nLSsBaFEjpyobAFP4gJtIHST59W5HnB0LjVuhPVGMOFcRi4q1zy5o0/88G8Dl4y3v\nRYkenpOobHTnoEKz62oAVbnf+qBjN1kyVsF+zfD3KRFPi+3rx933TBIxk3d/1lP9\nqgwJAoGAdJ+sLNwUoaLkt14Ln2Yh8Mx94J+rNh5BmkAFKay2pFRqiAG9OtxrEJBc\nvFePx0LuEUGSM9Xa0PfVY+YeApWBV2v0zlekxuv42vthTr2YDWCwJnhfoozzlxrS\niufXUSvyKpUN2zJLeBUbC/e1BYiP1M128iX23O9aclH646Gd9AM=\n-----END RSA PRIVATE KEY-----",
//        	"thirdPartyAccount":"FoGuangShan_uat","cashier":"00000000"}
            map.put("refundOrderNo", tradeDetail.getSystemOrderId());
            map.put("s2pOrderNo", tradeDetail.getOrderId());
            map.put("totalFee", Long.toString(tradeDetail.getPayment()));
//        	map.put("rsaKey", "-----BEGIN RSA PRIVATE KEY-----\nMIIEogIBAAKCAQEAmGfItbOY6rKHDvsyXqzwC+/GjPdNdaccJuZbyzQlZ/6/ASeZ\nd9n/8WF/zQdxnERRo9Xm4sZ8adE0UNoFnXZQ9qfvnqtQA+TTnwrS28VqmS13gtu9\nYhqr34cY3ImKe8k7msCae6uwE4XNaMvi3cNZ6PrsJZeK/foGGy142NHcyqpHQUon\nWiHAHQTvQc/shwDxwvRrK0fidAK4bUNkJEtoxBg/tLa9nkQprt7B+9J0BIdjiLXz\nEFM0yhnhymst7O/tLH9nQZiqlIayyJqepcJz6gXJvWCQIVWHcShFJfXl1OfnqKkm\npnBj7UqKVKrXZsVTpc/BQlERwf8CYBeqL9ICgwIDAQABAoIBAEq9Ip2QvJ3j/P2o\nzFPHhVSlOn5ozLC0ycZ902g8Y1ezguHDFKzE2JH8GVtnmhxjGJI61KZDZkHSWVCJ\nCpXkruG3C3iJVI0ES49EFJG4J4WE2YXv0tjocX8F1nhYbvQzhCpo55lcYdXBGTGa\nQuAz68SoQORSg9P3J8VuMERL7nXn95pA1LLcYYm8B5Nq2dq+YJrsBopGaIfc2m8H\nF4ottIQFQ8iHDk5tGmcEHuyV9vhW0ozQ+hifJ9agle6ZZ2madFJUTMkchTm4gCCj\nSWf+wNlC5rXPLT5d/u69mG7jg7scNK67M0dXJGtRme5YaMWwDiyAfUMRxe4y/2XN\nY+wdy+ECgYEAxzi9iK5R+H3JHueHvPJKXyDr7mNOOqPgg+LHd7dJH5t/iIJ18lfD\n5Zu65oeNNKlnBQ+sQ9+Sgz2421rVmkeYTLAq8BpPPGSlDOYEmCugcFPZmqpKMM+Z\n27gwfX2kRW9jceeUl59PLZw4oo/pIz4yD/0UXgwIg5dhm8ZIx8pJyVECgYEAw9dR\n1eXQpx1B9/oatY/qiUmCNgOe3AYo7PkIHC0d0xj8caj2dlYkToZAejcEd+p1Hyy1\ni+grg4zGHhd128GvKtCE7xnlAgfpyjLBJk34rst4mKBay3H7q57/GUZsrA+WUe8v\nWq7Sa5wIO72BY+HmTgBxEn6DbXf9xwH1GsOCmZMCgYAnu9JZg2MH65aEWmtBLRXW\nOUu1BQtiOLMbusxLvTuRWUMrZ9G8XvLMRa4WMmzgVvUcFk8mTl37XQEUht5ZYfQn\nP2+g6a5p/izJEMtTyke6f47EATjA2Xb3UpohPUDZNGkiG60QPI7fKuxEx+0YwVvc\nwn4gb8f27udh2T9E0by7sQKBgGsD6jAcGNx43GeJP7cfIWZLyN+DMvfAVfTIUGo1\nLSsBaFEjpyobAFP4gJtIHST59W5HnB0LjVuhPVGMOFcRi4q1zy5o0/88G8Dl4y3v\nRYkenpOobHTnoEKz62oAVbnf+qBjN1kyVsF+zfD3KRFPi+3rx933TBIxk3d/1lP9\nqgwJAoGAdJ+sLNwUoaLkt14Ln2Yh8Mx94J+rNh5BmkAFKay2pFRqiAG9OtxrEJBc\nvFePx0LuEUGSM9Xa0PfVY+YeApWBV2v0zlekxuv42vthTr2YDWCwJnhfoozzlxrS\niufXUSvyKpUN2zJLeBUbC/e1BYiP1M128iX23O9aclH646Gd9AM=\n-----END RSA PRIVATE KEY-----");
            map.put("rsaKey", ctbcWeixinAccount.getHashIV());
            map.put("thirdPartyAccount", ctbcWeixinAccount.getAppId());
            map.put("mchId", Long.toString(ctbcWeixinAccount.getMerchant().getMerchantSeqId()));
            if (requestData.has("Cashier")) {
                map.put("cashier", requestData.get("Cashier").getAsString());
            } else {
                map.put("cashier", "00000000");
            }

            String weixinRequest = new Gson().toJson(map);
            LOGGER.info("[TRANSACTION] CTBC weixin refund start. :" + weixinRequest);
            String result = HttpRequestUtil.post(CTBC_URL + "/ctbc/api/wechat/passively/refund", weixinRequest);
            LOGGER.info("[TRANSACTION] CTBC weixin refund end.: " + result);
            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);

            ResponseGeneralHeader responseGeneralHeader = getWeixinResponseHeader(result, requestHeader);
            JsonObject dataJson = new Gson().fromJson(result, JsonObject.class);

            IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();

            try {

                integratedRefundResponseData.setSysRefundNo(dataJson.get("refundOrderNo").getAsString());
            } catch (Exception e) {
                integratedRefundResponseData.setSysOrderNo(null);
            }

            integratedRefundResponseData.setStoreOrderNo(dataJson.get("orderNo").getAsString());
            integratedRefundResponseData.setStoreRefundNo(requestData.get("StoreRefundNo").getAsString());
            integratedRefundResponseData.setRefundedAt(responseGeneralHeader.getResponseTime());

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedRefundResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);
//        	

            LOGGER.info("[Refund] CTBC weixin callJsApiNoReturn start. :");

        } else if ("SingleOrderQuery".equals(requestHeader.getServiceType())) {

            Map<String, String> map = new HashMap<String, String>();

            map.put("s2pOrderNo", requestData.get("StoreOrderNo").getAsString());
//            map.put("rsaKey", "-----BEGIN RSA PRIVATE KEY-----\nMIIEogIBAAKCAQEAmGfItbOY6rKHDvsyXqzwC+/GjPdNdaccJuZbyzQlZ/6/ASeZ\nd9n/8WF/zQdxnERRo9Xm4sZ8adE0UNoFnXZQ9qfvnqtQA+TTnwrS28VqmS13gtu9\nYhqr34cY3ImKe8k7msCae6uwE4XNaMvi3cNZ6PrsJZeK/foGGy142NHcyqpHQUon\nWiHAHQTvQc/shwDxwvRrK0fidAK4bUNkJEtoxBg/tLa9nkQprt7B+9J0BIdjiLXz\nEFM0yhnhymst7O/tLH9nQZiqlIayyJqepcJz6gXJvWCQIVWHcShFJfXl1OfnqKkm\npnBj7UqKVKrXZsVTpc/BQlERwf8CYBeqL9ICgwIDAQABAoIBAEq9Ip2QvJ3j/P2o\nzFPHhVSlOn5ozLC0ycZ902g8Y1ezguHDFKzE2JH8GVtnmhxjGJI61KZDZkHSWVCJ\nCpXkruG3C3iJVI0ES49EFJG4J4WE2YXv0tjocX8F1nhYbvQzhCpo55lcYdXBGTGa\nQuAz68SoQORSg9P3J8VuMERL7nXn95pA1LLcYYm8B5Nq2dq+YJrsBopGaIfc2m8H\nF4ottIQFQ8iHDk5tGmcEHuyV9vhW0ozQ+hifJ9agle6ZZ2madFJUTMkchTm4gCCj\nSWf+wNlC5rXPLT5d/u69mG7jg7scNK67M0dXJGtRme5YaMWwDiyAfUMRxe4y/2XN\nY+wdy+ECgYEAxzi9iK5R+H3JHueHvPJKXyDr7mNOOqPgg+LHd7dJH5t/iIJ18lfD\n5Zu65oeNNKlnBQ+sQ9+Sgz2421rVmkeYTLAq8BpPPGSlDOYEmCugcFPZmqpKMM+Z\n27gwfX2kRW9jceeUl59PLZw4oo/pIz4yD/0UXgwIg5dhm8ZIx8pJyVECgYEAw9dR\n1eXQpx1B9/oatY/qiUmCNgOe3AYo7PkIHC0d0xj8caj2dlYkToZAejcEd+p1Hyy1\ni+grg4zGHhd128GvKtCE7xnlAgfpyjLBJk34rst4mKBay3H7q57/GUZsrA+WUe8v\nWq7Sa5wIO72BY+HmTgBxEn6DbXf9xwH1GsOCmZMCgYAnu9JZg2MH65aEWmtBLRXW\nOUu1BQtiOLMbusxLvTuRWUMrZ9G8XvLMRa4WMmzgVvUcFk8mTl37XQEUht5ZYfQn\nP2+g6a5p/izJEMtTyke6f47EATjA2Xb3UpohPUDZNGkiG60QPI7fKuxEx+0YwVvc\nwn4gb8f27udh2T9E0by7sQKBgGsD6jAcGNx43GeJP7cfIWZLyN+DMvfAVfTIUGo1\nLSsBaFEjpyobAFP4gJtIHST59W5HnB0LjVuhPVGMOFcRi4q1zy5o0/88G8Dl4y3v\nRYkenpOobHTnoEKz62oAVbnf+qBjN1kyVsF+zfD3KRFPi+3rx933TBIxk3d/1lP9\nqgwJAoGAdJ+sLNwUoaLkt14Ln2Yh8Mx94J+rNh5BmkAFKay2pFRqiAG9OtxrEJBc\nvFePx0LuEUGSM9Xa0PfVY+YeApWBV2v0zlekxuv42vthTr2YDWCwJnhfoozzlxrS\niufXUSvyKpUN2zJLeBUbC/e1BYiP1M128iX23O9aclH646Gd9AM=\n-----END RSA PRIVATE KEY-----");
            map.put("rsaKey", ctbcWeixinAccount.getHashIV());
            map.put("thirdPartyAccount", ctbcWeixinAccount.getAppId());

            String weixinRequest = new Gson().toJson(map);

            tradeProcedureService.setTradeDetailRequest(requestHeader, data);

            LOGGER.info("[TRANSACTION] CTBC weixin query start. :" + weixinRequest);
            String result = HttpRequestUtil.post(CTBC_URL + "/ctbc/api/wechat/passively/queryPay", weixinRequest);
            LOGGER.info("[TRANSACTION] CTBC weixin query end.: " + result);

            tradeProcedureService.setTradeDetailResponse(integrateMchId, requestHeader, result, data);
            TradeDetail _tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());

            ResponseGeneralHeader responseGeneralHeader = getWeixinResponseHeader(result, requestHeader);
            JsonObject dataJson = new Gson().fromJson(result, JsonObject.class);

            IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();
            integratedSingleOrderQueryResponseData.setStoreOrderNo(requestData.get("StoreOrderNo").getAsString());
            integratedSingleOrderQueryResponseData.setSysOrderNo(dataJson.get("thirdPartyOrderNo").getAsString());
            integratedSingleOrderQueryResponseData.setDetail(_tradeDetail.getDetail());
            integratedSingleOrderQueryResponseData.setStoreInfo(_tradeDetail.getStoreInfo());
            integratedSingleOrderQueryResponseData.setFeeType("TWD");
            integratedSingleOrderQueryResponseData.setTotalFee(Long.toString(_tradeDetail.getPayment()));

            integratedSingleOrderQueryResponseData.setBody(_tradeDetail.getDescription());
            integratedSingleOrderQueryResponseData.setPaidAt(_tradeDetail.getCreateDate());
            integratedSingleOrderQueryResponseData.setRefundedMsg(_tradeDetail.getRefundStatus());

            RefundDetail refundDetail = refundDetailService.getOneByOrderIdAndMethod(_tradeDetail.getOrderId(),
                    _tradeDetail.getMethod());
            if (Objects.nonNull(refundDetail)) {
                integratedSingleOrderQueryResponseData.setRefundedAt(refundDetail.getCreateTime());
            }

            if ("0000".equals(dataJson.get("status").getAsString())) { // success
                integratedSingleOrderQueryResponseData.setOrderStatus("1");
            } else if ("9997".equals(dataJson.get("status").getAsString())) { // waiting
                integratedSingleOrderQueryResponseData.setOrderStatus("0");
            } else if ("9998".equals(dataJson.get("status").getAsString())) { // fail
                integratedSingleOrderQueryResponseData.setOrderStatus("2");
            } else if ("7112".equals(dataJson.get("status").getAsString())) { // refund
                integratedSingleOrderQueryResponseData.setOrderStatus("3");
            }

//            if (dataValueJson.has("SYS_STATUS") ) {
//                if (dataValueJson.get("SYS_STATUS").getAsString().equals("4")) {
//                    integratedSingleOrderQueryResponseData.setRefundedAt(responseGeneralHeader.getResponseTime());
//                })
//
//                integratedSingleOrderQueryResponseData.setOrderStatus(dataValueJson.has("SYS_STATUS") ? dataValueJson.get("SYS_STATUS").getAsString() : "0");
//            }
//            integratedSingleOrderQueryResponseData.setOrderStatusDesc("TODO");

            ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
            responseGeneralBody.setHeader(responseGeneralHeader);
            responseGeneralBody.setData(integratedSingleOrderQueryResponseData);

            generalResponseData = new Gson().toJson(responseGeneralBody);

        }

        return generalResponseData;
    }

    private ResponseGeneralHeader getWeixinResponseHeader(String result, RequestHeader requestHeader) {
        JsonObject json = new Gson().fromJson(result, JsonObject.class);
        ResponseGeneralHeader responseGeneralHeader = null;
        if (json.has("Header")) {
            responseGeneralHeader = new Gson().fromJson(json.get("Header").toString(), ResponseGeneralHeader.class);
        } else {
            responseGeneralHeader = new ResponseGeneralHeader();
            responseGeneralHeader.setStatusCode(json.get("status").getAsString());
            responseGeneralHeader.setStatusDesc(json.get("stateDesc").getAsString());
        }
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setMethod(requestHeader.getMethod());
        responseGeneralHeader.setMchId(requestHeader.getMerchantId());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        return responseGeneralHeader;
    }

    private SecretKey decryptSecretKey(String encryptApiKey) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(env.getProperty("intella.private.key")).getFile());

        PrivateKey privateKey = KeyReader.loadPrivateKeyFromDER(file);
        byte[] byteKeys = Base64.decode(encryptApiKey);
        String aesKey = decrypt(byteKeys, privateKey);
        return AesCryptoUtil.convertSecretKey(Base64.decode(aesKey));
    }

}
