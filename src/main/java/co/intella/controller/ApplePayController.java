package co.intella.controller;

import co.intella.constant.Bank;
import co.intella.domain.ApplePayTokenData;
import co.intella.domain.PaymentRequest;
import co.intella.domain.PaymentResponse;
import co.intella.domain.tsbank.TSBApplePayTokenData;
import co.intella.domain.tsbank.TSBCreditCardRequestDataPara;
import co.intella.domain.tsbank.TSBOtherResponseData;
import co.intella.model.RequestHeader;
import co.intella.model.*;
import co.intella.net.HttpRequestUtil;
import co.intella.service.*;
import co.intella.utility.JsonUtil;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Map;

/**
 * This api used by apple pay transaction
 *
 * @author Miles Wu
 */

@Controller
@RequestMapping("/applepay")
public class ApplePayController {

    private final Logger LOGGER = LoggerFactory.getLogger(ApplePayController.class);

    private final String MERCHANT = "test";

    @Resource
    private TsBankService tsBankService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private ApplePayService applePayService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private OrderService orderService;

    @Resource
    private PaymentService fuBonService;

    @Value("${api.url}")
    private String API_URL;

    @Autowired
    RestTemplate restTemplate;

    @Value("${applePaySessionCertFilePath}")
    private String applePaySessionCertFilePath;

    @Value("${applePaySessionCertMerchantId}")
    private String applePaySessionCertMerchantId;

    @Value("${applePaySessionCertMerchantDomain}")
    private String applePaySessionCertMerchantDomain;

    @RequestMapping(method = RequestMethod.POST, value = "/{qrcode}/{merchant}")
    @Deprecated
    public ResponseEntity<String> decryptTokenAndPay(@RequestBody Map<String, Object> request,
                                                     @PathVariable(value = "qrcode") String qrcode, @PathVariable(value = "merchant") String merchant)
            throws IOException {

        LOGGER.info("[REQUEST] applepay/decrypt/token/" + qrcode);

        String token = applePayService.getToken(new Gson().toJson(request), merchant);

        LOGGER.info("[TRANSACTION] get token " + token);
        try {
            // todo call service instead of api
            String result = HttpRequestUtil.post(API_URL + "allpaypass/applepay/trade/" + qrcode, token);
            LOGGER.info("[RESPONSE] apple pay, decrypt and pay. " + result);
            return new ResponseEntity<String>(result, HttpStatus.OK);
        } catch (Exception e) {
            // todo send email to tail dev there is something wrong.
            return new ResponseEntity<String>("something wrong" + e.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/qrcode/{qrcode}/merchant/{merchant}")
    public ResponseEntity<String> doApplePayPayment(@RequestBody Map<String, Object> request,
                                                    @PathVariable(value = "qrcode") String qrcode, @PathVariable(value = "merchant") String merchant)
            throws IOException {

        PaymentAccount paymentAccount = paymentAccountService.getOne(merchant, Bank.APPLE_PAY.getId());
        String method = String.format("2%s40", paymentAccount.getBankCode());

        LOGGER.info(String.format("[REQUEST] qrcode/%s/merchant/%s/method/%s", qrcode, merchant, method));
        ApplePayTokenData applePayTokenData = applePayService.decryptToken(new Gson().toJson(request), merchant);

        if (applePayTokenData == null)
            return new ResponseEntity<String>("decrypt apple pay token fail", HttpStatus.BAD_REQUEST);

        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.getHeader().setMethod(method);
        paymentRequest.getHeader().setMchId(merchant);

        try {

            // TODO maybe refactor latter for every acquirer bank
            PaymentResponse paymentResponse = null;
            if ("20840".equals(method)) {
                paymentResponse = doTSBApplePayPayment(new Gson().toJson(applePayTokenData), qrcode);
            } else {
                paymentResponse = fuBonService.doApplePayPayment(applePayTokenData, paymentRequest, qrcode);
            }
            LOGGER.info("[RESPONSE] apple pay, decrypt and pay. " + paymentResponse);
            return new ResponseEntity<String>(new Gson().toJson(paymentResponse), HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<String>("something wrong" + e.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/order/save")
    @ResponseBody
    public ResponseEntity<Void> saveOrder(@RequestBody String saveOrderRequest) {
        LOGGER.info("[APPLE][REQUEST] " + saveOrderRequest);
        return ResponseEntity.ok().build();
    }

    @RequestMapping("/success")
    public ModelAndView success() {
        LOGGER.info("[APPLE][REQUEST] show applepay success page.");
        ModelAndView model = new ModelAndView();
        model.setViewName("apple_success");
        return model;
    }

    private PaymentResponse doTSBApplePayPayment(@RequestBody String tokenData, @PathVariable String qrcodeId) {

        LOGGER.info("[APPLE][REQUEST] Token received from tokenService : " + tokenData + ", qrcode=" + qrcodeId);

        TSBApplePayTokenData tsbApplePayTokenData = new Gson().fromJson(tokenData, TSBApplePayTokenData.class);
        tsbApplePayTokenData.setMerchantId(null);

        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(qrcodeId);

        co.intella.model.RequestHeader requestHeader = new RequestHeader();
        requestHeader.setServiceType("ApplePay");
        requestHeader.setMerchantId(qrcodeParameter.getMchId());
        requestHeader.setCreateTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        requestHeader.setMethod("20840");

        TSBCreditCardRequestDataPara tsbCreditCardRequestDataPara = new TSBCreditCardRequestDataPara();

        tsbCreditCardRequestDataPara.setPayTokenData(tsbApplePayTokenData);

        if (qrcodeParameter.getCodeType().equals("3") || qrcodeParameter.getCodeType().equals("2")
                || qrcodeParameter.getCodeType().equals("5") || qrcodeParameter.getCodeType().equals("6") || qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            tsbCreditCardRequestDataPara.setOrderNo(orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter));
        } else if (qrcodeParameter.getCodeType().equals("1")) {
            tsbCreditCardRequestDataPara.setOrderNo(qrcodeParameter.getStoreOrderNo());
        }

        String bodyRandom = qrcodeService.getRandomBody(qrcodeParameter);// body + "-" + String.format("%04d", new
        // Random().nextInt(9999));

        tsbCreditCardRequestDataPara.setOrderDesc(bodyRandom);
        tsbCreditCardRequestDataPara.setCaptFlag("0");

        JsonPrimitive jsonPrimitive = new JsonPrimitive(new Gson().toJson(tsbCreditCardRequestDataPara));

        String result = SystemInstance.EMPTY_STRING;
        TradeDetail tradeDetail = setApplepayTradeDetail(requestHeader, tsbCreditCardRequestDataPara, qrcodeParameter);

        PaymentResponse paymentResponse = null;
        if (tradeDetail == null) {
            LOGGER.error("[ERROR][APPLE][OrderIdExist]");
            paymentResponse = new PaymentResponse();
            paymentResponse.getHeader().setStatusCode("7000");
            paymentResponse.getHeader().setStatusDesc("訂單編號已重複");
            return paymentResponse;
        }

        try {
            result = tsBankService.doRequest(requestHeader, jsonPrimitive, qrcodeParameter.getMchId(),
                    SystemInstance.EMPTY_STRING);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("[ERROR][APPLE] " + e.toString());
        }

        LOGGER.info("[APPLE][RESPONSE] to token service" + result);
        TSBOtherResponseData responseData = new Gson().fromJson(result, TSBOtherResponseData.class);
        updateApplepayTradeDetail(tradeDetail, responseData);

        responseData.getParams().setOrderNo(tsbCreditCardRequestDataPara.getOrderNo());
        LOGGER.info("[doTSBApplePayPayment]   responseData" + JsonUtil.toJsonStr(responseData));
        paymentResponse = tsBankService.mapToPaymentResponse(responseData);
        if (SystemInstance.TRADE_SUCCESS.equals(paymentResponse.getHeader().getStatusCode())) {
            // todo do this request async...
            appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
        }

        return paymentResponse;
    }

    private void updateApplepayTradeDetail(TradeDetail tradeDetail, TSBOtherResponseData responseData) {
        LOGGER.info("[updateApplepayTradeDetail]");
        tradeDetail.setMethod("20840");
        if (responseData == null) {
            LOGGER.info("[responseData is null]");
            return;
        }
        if (responseData.getParams().getRetCode().equals("00")) {
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            // todo comfirm systemOrderId
            tradeDetail.setSystemOrderId(responseData.getParams().getRrn());
        } else {
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
        }
        tradeDetailService.save(tradeDetail);
        LOGGER.info("[updateApplepayTradeDetail][SAVED]");
    }

    private TradeDetail setApplepayTradeDetail(RequestHeader requestHeader,
                                               TSBCreditCardRequestDataPara applerequestData, QrcodeParameter qrcodeParameter) {

        LOGGER.info("[setApplepayTradeDetail]");
        TradeDetail tradeDetail = tradeDetailService.getOne(applerequestData.getOrderNo());

        if (tradeDetail != null) {
            return null;
        } else {
            tradeDetail = new TradeDetail();
        }

        Merchant merchant = merchantService.getOne(requestHeader.getMerchantId());

        tradeDetail.setMethod("20840"); // 台新 apple pay
        tradeDetail.setServiceType(requestHeader.getServiceType());
        tradeDetail.setAccountId(requestHeader.getMerchantId());
        tradeDetail.setCreateDate(requestHeader.getCreateTime());

        tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
        tradeDetail.setOrderId(applerequestData.getOrderNo());
        tradeDetail.setDeviceRandomId(deviceService.getOne(merchant, "APP-001"));
        tradeDetail.setPaymentAccount(paymentAccountService.getOne(requestHeader.getMerchantId(), 12));
        Integer amount = applerequestData.getPayTokenData().getTransactionAmount() / 100;
        tradeDetail.setPayment(amount);
        tradeDetail.setDescription(applerequestData.getOrderDesc());

        if (qrcodeParameter.getOnSaleTotalFee() == amount) {
            tradeDetail.setOnSale(true);
        } else {
            tradeDetail.setOnSale(false);
        }
        tradeDetail.setOriginalPrice(qrcodeParameter.getTotalFee());
        tradeDetail.setQrcodeToken(qrcodeParameter.getShortId());
        tradeDetail = tradeDetailService.save(tradeDetail);
        tradeProcedureService.captureOrderSave(tradeDetail);
        return tradeDetail;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/getApplePaySession")
    public String getApplePaySession(@RequestBody String request, HttpServletRequest httpServletRequest) throws IOException, JSONException, CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, NoSuchProviderException {
        LOGGER.info("[REQUEST] api/applePay/getApplePaySession :" + request);
        JsonObject jsonObject = new Gson().fromJson(request, JsonObject.class);
        String url = jsonObject.get("url").getAsString();
        String merchant = MERCHANT;
        String applePaycert = FileUtils.readFileToString(new File(applePaySessionCertFilePath), StandardCharsets.UTF_8);
        JSONObject obj = new JSONObject();
        obj.put("merchantIdentifier", applePaySessionCertMerchantId);
        LOGGER.info("httpServletRequest.getServerName() =>" + httpServletRequest.getServerName());
        obj.put("domainName", httpServletRequest.getServerName());
        obj.put("displayName", "merchant id for test");

        //從憑證產生TrustManager
        String password = "changeit";  //預設密碼為changeit
        //讀取憑證
//        File file = new File(applePaySessionCertFilePath);
//        //通常會將憑證都加到%Java_Home%/lib/security/cacerts/中
//        //File file = new File(System.getProperty("java.home") + "/lib/security/cacerts");
        InputStream in = new FileInputStream(applePaySessionCertFilePath);
        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        keyStore.load(in, password.toCharArray());
        in.close();
        LOGGER.info("applePaySessionCert :" + keyStore.getCertificate("testcert"));
        //建立TrustManager
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance("SunX509", "SunJSSE");
        trustManagerFactory.init(keyStore);

        SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
        sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

        HttpsURLConnection httpsUrlConnection = (HttpsURLConnection) (new URL("https://apple-pay-gateway-pr-pod3.apple.com/paymentservices/startSession/")).openConnection();

        httpsUrlConnection.setRequestMethod("POST");
        httpsUrlConnection.setDoInput(true);
        httpsUrlConnection.setDoOutput(true);
        httpsUrlConnection.setConnectTimeout(3000);
        httpsUrlConnection.setReadTimeout(3000);
        httpsUrlConnection.setRequestProperty("Content-Type", "application/json; utf-8");
        httpsUrlConnection.setRequestProperty("Accept", "application/json");

        try (OutputStream os = httpsUrlConnection.getOutputStream()) {
            byte[] input = obj.toString().getBytes("utf-8");
            os.write(input, 0, input.length);
        }
        httpsUrlConnection.connect();
        StringBuffer response = new StringBuffer();
        String responseLine = null;
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(httpsUrlConnection.getInputStream(), "utf-8"))) {
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println(response.toString());
        }
        return responseLine;
    }
}
