package co.intella.controller;


import co.intella.domain.ntuImoto.IMotoValuation;
import co.intella.service.NtuImotoService;
import co.intella.service.QrType7Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Controller
@RequestMapping("/api/imoto")
public class IMotoController {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Resource
    private NtuImotoService ntuImotoService;
    
    @Resource
    private QrType7Service qrType7Service;

    @PostMapping(value = "/ntuValuation", produces = MediaType.APPLICATION_FORM_URLENCODED_VALUE, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String valuation(@ModelAttribute IMotoValuation iMotoValuation) {
        return ntuImotoService.doValuation(iMotoValuation);
    }

    @GetMapping(value = "/checkoutImotoAmount/{shortId}")
    @ResponseBody
    public Long checkoutImotoAmount(@PathVariable(value = "shortId") String shortId) {
    	long imotoAmount=ntuImotoService.checkoutImotoAmount(shortId);
//    	if(-1==imotoAmount) {
//    		imotoAmount=qrType7Service.getDemoInfoAmount(shortId);
//    	}
        return imotoAmount;
    }

}
