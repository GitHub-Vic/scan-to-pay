package co.intella.controller;


import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


import org.springframework.web.bind.annotation.RequestBody;
import co.intella.model.QrType7;
import co.intella.repository.QrType7Repository;
import co.intella.repository.QrcodeParameterRepository;
import co.intella.service.QrType7Service;
import co.intella.utility.JsonUtil;

@RequestMapping("api/qrcodeType7")
@RestController
public class QrcodePaymentController {
	@Resource
	private QrType7Repository qrType7Repository;

	@Resource
	private QrcodeParameterRepository qrcodeParameterRepository;
	
	@Resource
	private QrType7Service qrType7Service;

	private final Logger LOGGER = LoggerFactory.getLogger(QrcodePaymentController.class);

	@PostMapping
	public Map<String, Object> qrcodeType7(@RequestBody QrType7 qrType7) throws InterruptedException {
		LOGGER.info("QrType7 request=" + JsonUtil.toJsonStr(qrType7));
		Map<String, Object> response=qrType7Service.saveQrType7(qrType7);
		LOGGER.info("QrType7 response=" + JsonUtil.toJsonStr(response));
		return response;

	}

}
