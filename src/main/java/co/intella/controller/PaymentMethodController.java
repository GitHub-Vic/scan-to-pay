package co.intella.controller;

import co.intella.model.*;
import co.intella.repository.QrType7Repository;
import co.intella.repository.impl.QrType7RepositoryImpl;
import co.intella.repository.impl.QrType8RepositoryImpl;
import co.intella.service.*;
import co.intella.utility.EncryptUtil;
import co.intella.utility.OnlinepayUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Objects;

/**
 * This api used by payment pages
 *
 * @author Vita
 */
@Controller
@RequestMapping("/api/payment")
public class PaymentMethodController {
    private final Logger LOGGER = LoggerFactory.getLogger(PaymentMethodController.class);

    @Value("${api.url}")
    private String API_URL;

    @Autowired
    private Environment env;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private Scan2PayPropertyService propertyService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private OrderService orderService;

    @Resource
    private MegaWeiXinService megaWeiXinService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private RedirectService redirectService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private TradeDetailOtherInfoService tradeDetailOtherInfoService;

    @Autowired
    private QrType7Repository qrType7Repository;

    @Autowired
    private QrType7RepositoryImpl qrType7RepositoryImpl;
    @Autowired
    private QrType8RepositoryImpl qrType8RepositoryImpl;

    @Resource
    private DiscountService discountService;

    @Resource
    private DiscountTradeOLPayService discountTradeOLPayService;

    @Resource
    private OnlinepayUtil OnlinepayUtil;

    @RequestMapping("/method")
    public ModelAndView method() {
        ModelAndView model = new ModelAndView();
        model.setViewName("paymentMethod");
        return model;
    }

    @RequestMapping("/ezcard/{token}")
    public ModelAndView ezcard(@PathVariable String token) {
        LOGGER.info("[REQUEST]/api/payment/ezcard/" + token);

        QrcodeParameter qrcodeParameter;
        String totalFee = "0";
        qrcodeParameter = qrcodeService.getOneByShortId(token);
        String viewName = "webagent";
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("3")) {
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            viewName = "customized";
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, ""));
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(token));
        } else {
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, ""));
        }

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", "ezcard");
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("token", token);
        modelMap.addAttribute("amount", totalFee);
//        modelMap.addAttribute("merchantName",qrcodeParameter.getMchId());
        modelMap.addAttribute("merchant", qrcodeParameter.getMchId());
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        modelMap.addAttribute("isCustom", isCustom);
        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName(viewName);
        return model;
    }

    @RequestMapping("/scsb/{token}")
    public ModelAndView getScsb(@PathVariable String token) {
        return getDefaultWebAgent(token, "scsb", "22600");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/scsb/{totalFee}/{token}")
    public ModelAndView scsbCheckout(@PathVariable String totalFee, @PathVariable String token) {
        return getDefaultCheckout(totalFee, token, "scsb", "22600");
    }

    @RequestMapping("/scsb3D/{token}")
    public ModelAndView getScsb3D(@PathVariable String token) {
        return getDefaultWebAgent(token, "scsb3D", "23900");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/scsb3D/{totalFee}/{token}")
    public ModelAndView scsb3DCheckout(@PathVariable String totalFee, @PathVariable String token) {
        return getDefaultCheckout(totalFee, token, "scsb3D", "23900");
    }


    @RequestMapping("/shanghai/{token}")
    public ModelAndView shangHaiBank(@PathVariable String token) {
        return getDefaultWebAgent(token, "shanghai", "22600");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/shanghai/{totalFee}/{token}")
    public ModelAndView shanghaiCheckout(@PathVariable String totalFee, @PathVariable String token) {
        return getDefaultCheckout(totalFee, token, "shanghai", "22600");
    }

    @RequestMapping("/tsbank/{token}")
    public ModelAndView tsBank(@PathVariable String token) {
        LOGGER.info("[REQUEST] /api/payment/tsbank/" + token);

        QrcodeParameter qrcodeParameter;
        String totalFee = "0";
        qrcodeParameter = qrcodeService.getOneByShortId(token);
        String viewName = "webagent";
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("3")) {
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            viewName = "customized";
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "20800"));
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(token));
        } else {
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "20800"));
        }

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", "ts");
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("token", token);
        modelMap.addAttribute("amount", totalFee);
//        modelMap.addAttribute("merchantName",qrcodeParameter.getMchId());
        modelMap.addAttribute("merchant", qrcodeParameter.getMchId());
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        modelMap.addAttribute("isCustom", isCustom);
        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName(viewName);
        return model;
    }


    @RequestMapping(method = RequestMethod.GET, value = "/ts/{totalFee}/{token}")
    public ModelAndView tsCheckout(@PathVariable String totalFee, @PathVariable String token) {
        LOGGER.info("[REQUEST] /api/payment/tscheckout/" + totalFee + "/" + token);

        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(token);

        Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());

        String storeOrderNoNew;
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("1")) {
            storeOrderNoNew = qrcodeParameter.getStoreOrderNo();
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            storeOrderNoNew = orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            storeOrderNoNew = orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(token));
            storeOrderNoNew = orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
        } else {
            storeOrderNoNew = orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
        }

        if (qrcodeParameter.getOnSaleTotalFee() == 0) {
            DiscountTrade discountTrade = discountService.checkoutTotalFree(qrcodeParameter.getMchId(),
                    Double.valueOf(qrcodeParameter.getTotalFee()).longValue(), "20800");
            if (Objects.nonNull(discountTrade)) {
                DiscountTradeOLPay discountTradeOLPay = new DiscountTradeOLPay();
                BeanUtils.copyProperties(discountTrade, discountTradeOLPay, "discountTradeSeq");
                discountTradeOLPay.setShortId(qrcodeParameter.getShortId());
                discountTradeOLPayService.save(discountTradeOLPay);
                totalFee = String.valueOf(discountTrade.getTradeAmount());
            }
        }
        String body = qrcodeService.getRandomBody(qrcodeParameter);


        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", "ts");
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("orderId", storeOrderNoNew);
        modelMap.addAttribute("amount", totalFee);
        modelMap.addAttribute("merchantId", qrcodeParameter.getMchId());
        modelMap.addAttribute("merchantName", merchant.getName());
        modelMap.addAttribute("body", body);
        modelMap.addAttribute("token", token);
        modelMap.addAttribute("isCustom", isCustom);
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName("tsBankCheckOut");

        return model;
    }

    //ziv
    @RequestMapping("/fubonbank/{token}")
    public ModelAndView funbonBank(@PathVariable String token) {
        LOGGER.info("[REQUEST] /api/payment/fubonbank/" + token);
        return getDefaultWebAgent(token, "fubonbank", "23800");
    }


    @RequestMapping(method = RequestMethod.GET, value = "/fubonbank/{totalFee}/{token}")
    public ModelAndView fubonCheckout(@PathVariable String totalFee, @PathVariable String token) {
        return getDefaultCheckout(totalFee, token, "fubonbank", "23800");
    }

    @RequestMapping("/EZCPay/{token}")
    public ModelAndView ezcPay(@PathVariable String token) {
        LOGGER.info("[REQUEST] /api/payment/EZCPay/" + token);
        return getDefaultWebAgent(token, "EZCPay", "");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/EZCPay/{totalFee}/{token}")
    public ModelAndView ezcPayCheckout(@PathVariable String totalFee, @PathVariable String token) {
        LOGGER.info("[REQUEST] /api/payment/EZCPay/" + totalFee + "/" + token);
        return getDefaultCheckout(totalFee, token, "EZCPay", "");
    }

    @RequestMapping("/linepay/{token}")
    public ModelAndView linePay(@PathVariable String token) {
        LOGGER.info("[REQUEST] /api/payment/linepay/" + token);

        QrcodeParameter qrcodeParameter;
        String totalFee = "0";
        qrcodeParameter = qrcodeService.getOneByShortId(token);

        String viewName = "webagent";
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("3")) {
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            viewName = "customized";
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "11500"));
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(token));
        } else {
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "11500"));
        }

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", "linepay");
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("token", token);
        modelMap.addAttribute("amount", totalFee);
//        modelMap.addAttribute("merchantName",qrcodeParameter.getMchId());
        modelMap.addAttribute("merchant", qrcodeParameter.getMchId());
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        modelMap.addAttribute("isCustom", isCustom);
        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName(viewName);
        return model;
    }

    @RequestMapping("/gamapay/{token}")
    public ModelAndView gamaPay(@PathVariable String token) {
        LOGGER.info("[REQUEST] /api/payment/gamapay/" + token);

        QrcodeParameter qrcodeParameter;
        String totalFee = "0";
        qrcodeParameter = qrcodeService.getOneByShortId(token);

        String viewName = "webagent";
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("3")) {
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            viewName = "customized";
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "11300"));
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(token));
        } else {
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "11300"));
        }

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", "gamapay");
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("token", token);
        modelMap.addAttribute("amount", totalFee);
//        modelMap.addAttribute("merchantName",qrcodeParameter.getMchId());
        modelMap.addAttribute("merchant", qrcodeParameter.getMchId());
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        modelMap.addAttribute("isCustom", isCustom);
        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName(viewName);
        return model;
    }

    @RequestMapping("/TOBwebATM/{token}")
    public ModelAndView tOBwebATM(@PathVariable String token) {
        LOGGER.info("[REQUEST] /api/payment/TOBwebATM/" + token);

        QrcodeParameter qrcodeParameter;
        String totalFee = "0";
        qrcodeParameter = qrcodeService.getOneByShortId(token);

        String viewName = "webagent";
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("3")) {
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            viewName = "customized";
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "15700"));
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(token));
        } else {
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "15700"));
        }

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", "TOBwebATM");
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("token", token);
        modelMap.addAttribute("amount", totalFee);
//        modelMap.addAttribute("merchantName",qrcodeParameter.getMchId());
        modelMap.addAttribute("merchant", qrcodeParameter.getMchId());
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        modelMap.addAttribute("isCustom", isCustom);
        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName(viewName);
        return model;
    }

    @RequestMapping("/IcashPay/{token}")
    public ModelAndView icashPay(@PathVariable String token) {
        LOGGER.info("[REQUEST] /api/payment/IcashPay/" + token);

        QrcodeParameter qrcodeParameter;
        String totalFee = "0";
        qrcodeParameter = qrcodeService.getOneByShortId(token);

        String viewName = "webagent";
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("3")) {
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            viewName = "customized";
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "15800"));
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(token));
        } else {
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "15800"));
        }

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", "IcashPay");
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("token", token);
        modelMap.addAttribute("amount", totalFee);
//        modelMap.addAttribute("merchantName",qrcodeParameter.getMchId());
        modelMap.addAttribute("merchant", qrcodeParameter.getMchId());
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        modelMap.addAttribute("isCustom", isCustom);
        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName(viewName);
        return model;
    }

    @RequestMapping("/twpay/{token}")
    public ModelAndView twPay(@PathVariable String token) {
        LOGGER.info("[REQUEST] /api/payment/twpay/" + token);

        QrcodeParameter qrcodeParameter;
        String totalFee = "0";
        qrcodeParameter = qrcodeService.getOneByShortId(token);

        String viewName = "webagent";
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("3")) {
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            viewName = "customized";
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "12000"));
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(token));
        } else {
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "12000"));
        }

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", "twpay");
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("token", token);
        modelMap.addAttribute("amount", totalFee);
        modelMap.addAttribute("merchant", qrcodeParameter.getMchId());
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        modelMap.addAttribute("isCustom", isCustom);
        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName(viewName);
        return model;
    }

    @RequestMapping("/FISCTwpay/{token}/{method}")
    public ModelAndView fiscTwpay(@PathVariable String token, @PathVariable String method) {
        LOGGER.info("[REQUEST] /api/payment/FISCTwpay/" + token);
        method = "123".equals(method) ? "" : method;
        QrcodeParameter qrcodeParameter;
        String totalFee = "0";
        qrcodeParameter = qrcodeService.getOneByShortId(token);

        String viewName = "webagent";
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("3")) {
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            viewName = "customized";
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, method));
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(token));
        } else {
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, method));
        }

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", "FISCTwpay");
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("token", token);
        modelMap.addAttribute("amount", totalFee);
        modelMap.addAttribute("merchant", qrcodeParameter.getMchId());
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        modelMap.addAttribute("isCustom", isCustom);
        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName(viewName);
        return model;
    }

    @RequestMapping("/apple/{token}")
    public ModelAndView applePay(HttpSession session, @PathVariable String token) {
        LOGGER.info("[REQUEST] /api/payment/apple/" + token + " with " + session);

        QrcodeParameter qrcodeParameter;
        String totalFee = "0";
        qrcodeParameter = qrcodeService.getOneByShortId(token);
        String viewName = "webagent";
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("3")) {
            // todo what???
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            viewName = "customized";
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, ""));
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(token));
        } else {
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, ""));
        }

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", "apple");
        modelMap.addAttribute("amount", totalFee);
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("token", token);
        modelMap.addAttribute("merchant", qrcodeParameter.getMchId());
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        modelMap.addAttribute("isCustom", isCustom);
        try {
            modelMap.addAttribute("applePayHost", propertyService.get("server.domain"));
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] set apple pay host failed." + e.getMessage() + ", \n" + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
        }

        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName(viewName);
        return model;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/ec/{qrcode}")
    public ModelAndView ecPay(@PathVariable String qrcode) {
        LOGGER.info("[REQUEST] /api/payment/ec/" + qrcode);

        QrcodeParameter qrcodeParameter;
        String totalFee = "0";
        qrcodeParameter = qrcodeService.getOneByShortId(qrcode);
        String viewName = "webagent";
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("3")) {
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            viewName = "customized";
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "11000"));
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(qrcode));
        } else {
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "11000"));
        }

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", "ec");
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("token", qrcode);
        modelMap.addAttribute("amount", totalFee);
        modelMap.addAttribute("merchant", qrcodeParameter.getMchId());
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        modelMap.addAttribute("isCustom", isCustom);
        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName(viewName);
        return model;
    }

    @RequestMapping("/ecresult/{result}")
    public ModelAndView ecresult(@PathVariable String result) throws UnsupportedEncodingException {
        LOGGER.info("[REQUEST EC] " + result);
        JsonObject jsonObject = new Gson().fromJson(result, JsonObject.class);
        Merchant merchant = merchantService.getOne(jsonObject.get("MerchantID").getAsString());
        ModelAndView model = new ModelAndView();
        if ("0000".equals(jsonObject.get("RtnCode").getAsString())) {
            model.addObject("MerchantID", merchant.getName());
            model.addObject("MerchantTradeNo", jsonObject.get("MerchantTradeNo").getAsString());
            model.addObject("TradeAmt", jsonObject.get("TradeAmt").getAsInt());
            model.addObject("PaymentDate", jsonObject.get("PaymentDate").getAsString());
            model.addObject("RtnCode", jsonObject.get("RtnCode").getAsString());
            model.addObject("itemName", jsonObject.get("itemName").getAsString());
            model.addObject("token", jsonObject.get("token").getAsString());
        } else {
            model.addObject("MerchantID", jsonObject.get("MerchantID").getAsString());
            model.addObject("TradeAmt", jsonObject.get("TradeAmt").getAsInt());
            model.addObject("PaymentDate", jsonObject.get("PaymentDate").getAsString());
            model.addObject("RtnCode", jsonObject.get("RtnCode").getAsString());
            model.addObject("itemName", jsonObject.get("itemName").getAsString());
            model.addObject("token", jsonObject.get("token").getAsString());
        }
        model.addObject("titleBarColor", "ecColor");
        model.addObject("domain", API_URL);
        model.setViewName("paymentResult");
        model = redirectService.confirm(model);
        return model;
    }

    @RequestMapping("/Pay2Go/{token}")
    public ModelAndView pay2Go(@PathVariable String token) {
        LOGGER.info("[REQUEST] /api/payment/Pay2Go/" + token);

        QrcodeParameter qrcodeParameter;
        String totalFee = "0";
        qrcodeParameter = qrcodeService.getOneByShortId(token);

        String viewName = "webagent";
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("3")) {
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            viewName = "customized";
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "12400"));
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(token));
        } else {
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "12400"));
        }

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", "Pay2Go");
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("token", token);
        modelMap.addAttribute("amount", totalFee);
//        modelMap.addAttribute("merchantName",qrcodeParameter.getMchId());
        modelMap.addAttribute("merchant", qrcodeParameter.getMchId());
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        modelMap.addAttribute("isCustom", isCustom);
        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName(viewName);
        return model;
    }


    @RequestMapping("/dgpay/{token}")
    public ModelAndView dgpay(@PathVariable String token) {
        LOGGER.info("[REQUEST] /api/payment/dgpay/" + token);

        QrcodeParameter qrcodeParameter;
        String totalFee = "0";
        qrcodeParameter = qrcodeService.getOneByShortId(token);

        String viewName = "webagent";
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("3")) {
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            viewName = "customized";
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, ""));
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(token));
        } else {
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, ""));
        }

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", "dgpay");
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("token", token);
        modelMap.addAttribute("amount", totalFee);
        modelMap.addAttribute("merchant", qrcodeParameter.getMchId());
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        modelMap.addAttribute("isCustom", isCustom);
        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName(viewName);
        return model;
    }

    @RequestMapping("/alipayShangHai/{token}")
    public ModelAndView alipayShangHai(@PathVariable String token) {
        LOGGER.info("[REQUEST] /api/payment/alipayShangHai/" + token);

        QrcodeParameter qrcodeParameter;
        String totalFee = "0";
        qrcodeParameter = qrcodeService.getOneByShortId(token);

        String viewName = "webagent";
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("3")) {
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            viewName = "customized";
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "12900"));
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(token));
        } else {
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "12900"));
        }

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", "alipayShangHai");
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("token", token);
        modelMap.addAttribute("amount", totalFee);
        modelMap.addAttribute("merchant", qrcodeParameter.getMchId());
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        modelMap.addAttribute("isCustom", isCustom);
        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName(viewName);
        return model;
    }

    @RequestMapping("/megaweixin/{token}")
    public ModelAndView getMegaWeixin(@PathVariable String token) {
        return getDefaultWebAgent(token, "megaweixin", "14000");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/megaweixin/{totalFee}/{token}")
    public ModelAndView ncccCheckout(@PathVariable String totalFee, @PathVariable String token, HttpServletResponse response) {
        return getMegaWeixinCheckout(totalFee, token, response);
    }

    @RequestMapping("/appleresult/{result}")
    public ModelAndView tradeAppleResult(@PathVariable String result) {
        try {
            LOGGER.info("[REQUEST APPLE] " + result);
            JsonObject object = new Gson().fromJson(result, JsonObject.class);
            Merchant merchant = merchantService.getOne(object.get("merchant").getAsString());
            ModelAndView model = new ModelAndView();
            if (object.get("returnCode").getAsString().equals("00")) {
                model.addObject("TradeAmt", object.get("amount").getAsInt());
                model.addObject("PaymentDate", object.get("date").getAsString().replace("-", ""));
                model.addObject("MerchantID", merchant.getName());
                model.addObject("MerchantTradeNo", object.get("rrn").getAsString());
                model.addObject("RtnCode", object.get("returnCode").getAsString());
                model.addObject("token", object.get("token").getAsString());
                model.addObject("itemName", tradeDetailService.getOneBySystemOrderId(object.get("rrn").getAsString()).getDescription());
            } else {
                model.addObject("RtnCode", object.get("returnCode").getAsString());
                model.addObject("token", object.get("token").getAsString());
            }
            model.addObject("titleBarColor", "applePayColor");
            model.addObject("domain", API_URL);
            model.setViewName("paymentResult");
            model = redirectService.confirm(model);
            return model;
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] set apple pay host failed." + e.getMessage() + ", \n" + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return null;
        }
    }

    @RequestMapping("/yuanTaOffline/{token}")
    public ModelAndView yuanTaOffline(@PathVariable String token) {
        LOGGER.info("[REQUEST] /api/payment/yuantaoffline/" + token);

        QrcodeParameter qrcodeParameter;
        String totalFee = "0";
        qrcodeParameter = qrcodeService.getOneByShortId(token);

        String viewName = "webagent";
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("3")) {
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            viewName = "customized";
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "11100"));
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(token));
        } else {
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "11100"));
        }

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", "yuanTaOffline");
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("token", token);
        modelMap.addAttribute("amount", totalFee);
        modelMap.addAttribute("merchant", qrcodeParameter.getMchId());
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        modelMap.addAttribute("isCustom", isCustom);
        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName(viewName);
        return model;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/ec/{spToken}/{amount}/{order}/{merchantID}/{itemName}/{qrcode}/{merchant}")
    public ModelAndView paymentTest(@PathVariable String spToken, @PathVariable String amount, @PathVariable String order,
                                    @PathVariable String merchantID, @PathVariable String itemName, @PathVariable String qrcode,
                                    @PathVariable String merchant) throws JsonProcessingException {
        LOGGER.info("[REQUEST] api/ecpay/credittrade/" + spToken);
        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("domain", env.getProperty("api.url"));
        modelMap.addAttribute("payURL", env.getProperty("host.request.ecpay.dotransaction"));
        modelMap.addAttribute("merchantID", merchantID);
        modelMap.addAttribute("merchant", merchant);
        modelMap.addAttribute("spToken", spToken);
        modelMap.addAttribute("amount", amount);
        modelMap.addAttribute("orderId", order);
        modelMap.addAttribute("paymentType", "CREDIT");
        modelMap.addAttribute("paymentName", unicodeToChinese("\\u4fe1\\u7528\\u5361"));
        modelMap.addAttribute("paymentMethod", "ec");
        modelMap.addAttribute("itemName", itemName);
        modelMap.addAttribute("token", qrcode);

        try {
            ModelAndView model = new ModelAndView();
            model.setViewName("webagent");
            model.addAllObjects(modelMap);
            return model;

        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] set apple pay host failed." + e.getMessage() + ", \n" + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return null;
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/order/result")
    public ModelAndView findOrderResult(@RequestParam(value = "orderId") String orderId) {

        orderId = EncryptUtil.decryptSimple(orderId);
        LOGGER.info("[/order/result] orderId: " + orderId);
        TradeDetail tradeDetail = tradeDetailService.getOne(orderId);
        ModelAndView model = new ModelAndView();
        model.setViewName("paymentResult");
        model.addObject("domain", API_URL);
//        TradeDetailOtherInfo tradeDetailOtherInfo = tradeDetailOtherInfoService.getOneByUNHEXUuid(tradeDetail.getTradeDetailRandomId().toString());
//        Merchant merchant = merchantService.getOne(tradeDetail.getAccountId());

//        long delay = this.checkCallBackDelay(tradeDetailOtherInfo, merchant);
//        if (delay == 0 && StringUtils.isNotBlank(tradeDetailOtherInfo.getCallBackUrl())) {
//            return new ModelAndView("redirect:" + tradeDetailOtherInfo.getCallBackUrl());
//        } else {
        if (tradeDetail != null) {
            Merchant merchantResult = merchantService.getOne(tradeDetail.getAccountId());
//            if (Objects.nonNull(merchantResult) && StringUtils.isNotEmpty(merchantResult.getButtonText()) && StringUtils.isNotEmpty(merchantResult.getButtonURL())) {
//                model.addObject("MerchantID", merchantResult.getName());
//                model.addObject("buttonText", merchantResult.getButtonText());
//                model.addObject("buttonURL", String.format(merchantResult.getButtonURL(), tradeDetail.getOrderId()));
//            }
//                if (delay > 0) {
//                    model.addObject("callBackUrl", tradeDetailOtherInfo.getCallBackUrl());
//                    model.addObject("callBackDelay", delay);
//                }

            model.addObject("MerchantTradeNo", tradeDetail.getOrderId());
            model.addObject("TradeAmt", tradeDetail.getPayment());
            model.addObject("PaymentDate", tradeDetail.getCreateDate());
            model.addObject("itemName", tradeDetail.getDescription());

            if (SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                model.addObject("RtnCode", "00");
            } else {
                model.addObject("RtnCode", "9999");
            }
            model = redirectService.confirm(model);
            return model;
        } else {
            LOGGER.info("[/order/result] orderId: " + orderId + " not found.");
        }
//        }

        return null;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/megaorder/result")
    public ModelAndView findMegaOLPayOrderResult(@RequestParam(value = "SysOrderId") String sysOrderId, @CookieValue(value = "MegaWeixinOLpayOrderId", defaultValue = "") String megaWeixinOLpayOrderId, HttpServletResponse response) {
        String orderId = "";
        if (StringUtils.isBlank(sysOrderId) && StringUtils.isNotBlank(megaWeixinOLpayOrderId)) {
            orderId = megaWeixinOLpayOrderId;
        } else if (StringUtils.isNotBlank(sysOrderId)) {
            TradeDetail tradeDetail = tradeDetailService.getOneBySystemOrderId(sysOrderId);
            orderId = tradeDetail.getOrderId();
        }
        ModelAndView model = findOrderResult(EncryptUtil.encryptSimple(orderId));
        deleteCookie(response);
        return model;
    }

    @RequestMapping("/UUPay/{token}")
    public ModelAndView uuPay(@PathVariable String token) {
        LOGGER.info("[REQUEST] /api/payment/UUPay/" + token);

        QrcodeParameter qrcodeParameter;
        String totalFee = "0";
        qrcodeParameter = qrcodeService.getOneByShortId(token);

        String viewName = "webagent";
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("3")) {
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            viewName = "customized";
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "13100"));
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(token));
        } else {
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "13100"));
        }

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", "UUPay");
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("token", token);
        modelMap.addAttribute("amount", totalFee);
//        modelMap.addAttribute("merchantName",qrcodeParameter.getMchId());
        modelMap.addAttribute("merchant", qrcodeParameter.getMchId());
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        modelMap.addAttribute("isCustom", isCustom);
        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName(viewName);
        return model;
    }

    @RequestMapping("/JKos/{token}")
    public ModelAndView JKos(@PathVariable String token) {
        LOGGER.info("[REQUEST] /api/payment/JKos/" + token);

        QrcodeParameter qrcodeParameter;
        String totalFee = "0";
        qrcodeParameter = qrcodeService.getOneByShortId(token);

        String viewName = "webagent";
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("3")) {
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            viewName = "customized";
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "13600"));
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(token));
        } else {
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "13600"));
        }

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", "JKos");
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("token", token);
        modelMap.addAttribute("amount", totalFee);
//        modelMap.addAttribute("merchantName",qrcodeParameter.getMchId());
        modelMap.addAttribute("merchant", qrcodeParameter.getMchId());
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        modelMap.addAttribute("isCustom", isCustom);
        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName(viewName);
        return model;
    }

    @RequestMapping("/HNCBTwpay/{token}")
    public ModelAndView HNCBTwpay(@PathVariable String token) {
        LOGGER.info("[REQUEST] /api/payment/HNCBTwpay/" + token);

        QrcodeParameter qrcodeParameter;
        String totalFee = "0";
        qrcodeParameter = qrcodeService.getOneByShortId(token);

        String viewName = "webagent";
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("3")) {
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            viewName = "customized";
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "14500"));
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(token));
        } else {
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "14500"));
        }

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", "HNCBTwpay");
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("token", token);
        modelMap.addAttribute("amount", totalFee);
//        modelMap.addAttribute("merchantName",qrcodeParameter.getMchId());
        modelMap.addAttribute("merchant", qrcodeParameter.getMchId());
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        modelMap.addAttribute("isCustom", isCustom);
        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName(viewName);
        return model;
    }

    @RequestMapping("/newPI/{token}")
    public ModelAndView newPI(@PathVariable String token) {
        LOGGER.info("[REQUEST] /api/payment/newPI/" + token);

        QrcodeParameter qrcodeParameter;
        String totalFee = "0";
        qrcodeParameter = qrcodeService.getOneByShortId(token);

        String viewName = "webagent";
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("3")) {
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            viewName = "customized";
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "15900"));
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(token));
        } else {
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "15900"));
        }

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", "newPI");
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("token", token);
        modelMap.addAttribute("amount", totalFee);
//        modelMap.addAttribute("merchantName",qrcodeParameter.getMchId());
        modelMap.addAttribute("merchant", qrcodeParameter.getMchId());
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        modelMap.addAttribute("isCustom", isCustom);
        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName(viewName);
        return model;
    }

    // todo replicated code
    private String unicodeToChinese(String string) {
        String str = string.replace("\\u", ",");
        String[] s2 = str.split(",");
        String s1 = "";
        for (int i = 1; i < s2.length; i++) {
            s1 = s1 + (char) Integer.parseInt(s2[i], 16);
        }
        return s1;
    }

    private ModelAndView getDefaultWebAgent(String token, String paymentMethodName, String method) {
        LOGGER.info(String.format("[REQUEST] /api/payment/%s/", paymentMethodName, token));

        QrcodeParameter qrcodeParameter;
        String totalFee = "0";
        qrcodeParameter = qrcodeService.getOneByShortId(token);
        String viewName = "webagent";
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("3")) {
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            viewName = "customized";
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, method));
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(token));
        } else {
            totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, method));
        }

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", paymentMethodName);
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("token", token);
        modelMap.addAttribute("amount", totalFee);
//        modelMap.addAttribute("merchantName",qrcodeParameter.getMchId());
        modelMap.addAttribute("merchant", qrcodeParameter.getMchId());
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        modelMap.addAttribute("isCustom", isCustom);
        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName(viewName);
        return model;
    }

    private ModelAndView getDefaultCheckout(String totalFee, String token, String checkOutName, String method) {
        LOGGER.info(String.format("[REQUEST] /api/payment/%s/checkout/%s/%s", checkOutName, totalFee, token));

        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(token);

        Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());

        String storeOrderNoNew;
        String isCustom = "0";
        if (qrcodeParameter.getCodeType().equals("1")) {
            storeOrderNoNew = qrcodeParameter.getStoreOrderNo();
        } else if (qrcodeParameter.getCodeType().equals("5")) {
            storeOrderNoNew = orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
        } else if (qrcodeParameter.getCodeType().equals("6")) {
            storeOrderNoNew = orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
            isCustom = "1";
        } else if (qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
            totalFee = String.valueOf(getDemoInfoAmount(token));
            storeOrderNoNew = orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
        } else {
            storeOrderNoNew = orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
        }

        if (qrcodeParameter.getOnSaleTotalFee() == 0 && StringUtils.isNotBlank(method)) {
            DiscountTrade discountTrade = discountService.checkoutTotalFree(qrcodeParameter.getMchId(),
                    Double.valueOf(qrcodeParameter.getTotalFee()).longValue(), method);
            if (Objects.nonNull(discountTrade)) {
                DiscountTradeOLPay discountTradeOLPay = new DiscountTradeOLPay();
                BeanUtils.copyProperties(discountTrade, discountTradeOLPay, "discountTradeSeq");
                discountTradeOLPay.setShortId(qrcodeParameter.getShortId());
                discountTradeOLPayService.save(discountTradeOLPay);
                totalFee = String.valueOf(discountTrade.getTradeAmount());
            }
        }


        String body = qrcodeService.getRandomBody(qrcodeParameter);

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("paymentMethod", checkOutName);
        modelMap.addAttribute("domain", API_URL);
        modelMap.addAttribute("orderId", storeOrderNoNew);
        modelMap.addAttribute("amount", totalFee);
        modelMap.addAttribute("merchantId", qrcodeParameter.getMchId());
        modelMap.addAttribute("merchantName", merchant.getName());
        modelMap.addAttribute("body", body);
        modelMap.addAttribute("token", token);
        modelMap.addAttribute("isCustom", isCustom);
        modelMap.addAttribute("customizedTemplate", qrcodeParameter.getCustomizedTemplate());
        ModelAndView model = new ModelAndView();
        model.addAllObjects(modelMap);
        model.setViewName("tsBankCheckOut");

        return model;
    }

    public ModelAndView getMegaWeixinCheckout(String totalFee, String token, HttpServletResponse response) {
        LOGGER.info(String.format("[REQUEST] /api/payment/megaweixin/checkout/%s/%s", totalFee, token));

        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(token);

        if (qrcodeParameter.getOnSaleTotalFee() == 0) {
            DiscountTrade discountTrade = discountService.checkoutTotalFree(qrcodeParameter.getMchId(),
                    Double.valueOf(qrcodeParameter.getTotalFee()).longValue(), "14000");
            if (Objects.nonNull(discountTrade)) {
                DiscountTradeOLPay discountTradeOLPay = new DiscountTradeOLPay();
                BeanUtils.copyProperties(discountTrade, discountTradeOLPay, "discountTradeSeq");
                discountTradeOLPay.setShortId(qrcodeParameter.getShortId());
                discountTradeOLPayService.save(discountTradeOLPay);
                totalFee = String.valueOf(discountTrade.getTradeAmount());
            }
        }

        Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());

        return megaWeiXinService.doOLPay(qrcodeParameter, merchant, totalFee, response);
    }

    public boolean deleteCookie(HttpServletResponse response) {

        boolean result = true;
        try {
            Cookie cookie = new Cookie("MegaWeixinOLpayOrderId", null);
            cookie.setHttpOnly(true);
            cookie.setMaxAge(0);
            cookie.setPath("/allpaypass/");
            response.addCookie(cookie);
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    //    private long checkCallBackDelay(TradeDetailOtherInfo tradeDetailOtherInfo, Merchant merchant) {
//        long delay = -1;
//        if (Objects.nonNull(tradeDetailOtherInfo)) {
//            // 有特別的Detail
//            Map<String, String> tradeDetailOtherInfoMap = JsonUtil.parseJson(tradeDetailOtherInfo.getRequestDetail(), Map.class);
//            if (StringUtils.isNotEmpty(merchant.getCallBackDelay())) {
//                delay = Long.valueOf(merchant.getCallBackDelay());
//            } else if (tradeDetailOtherInfoMap.containsKey("Delay")) {
//                delay = Long.valueOf(tradeDetailOtherInfoMap.get("Delay"));
//            } else {
//                delay = 3;
//            }
//            delay = delay > 10 ? 3 : delay;     //超過十秒 強制為3秒
//        }
//        return delay;
//    }
    private int getDemoInfoAmount(String shortId) {
        QrType7 qrType7 = qrType7RepositoryImpl.searchTheNewest(shortId);
        QrType8 qrType8 = qrType8RepositoryImpl.searchTheNewest(shortId);
        if (Objects.nonNull(qrType7)) {
            return qrType7.getAmount();
        } else if (Objects.nonNull(qrType8)) {
            return qrType8.getAmount();
        } else {
            return 0;
        }
    }
}