package co.intella.controller.donate;

import co.intella.crypto.RsaCryptoUtil;
import co.intella.domain.integration.IntegratedResponse;
import co.intella.model.RequestHeader;

import java.io.File;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.Resource;
import javax.crypto.SecretKey;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.reflect.TypeToken;
import com.sun.org.apache.xml.internal.security.utils.Base64;

import co.intella.crypto.AesCryptoUtil;
import co.intella.crypto.KeyReader;
import co.intella.net.Constant;
import co.intella.service.MerchantService;
import javassist.expr.NewArray;


/**
 * 
 * ClassName: MerchantController <br/>  
 * Description: 避免登入驗證阻擋，ONLY FOR 佛光山  scan2pay-donate-server<br/>  
 * Copyright: Copyright (c) 2019 <br/>  
 * Company: intella.co <br/>  
 * @author mustang  
 * @version 1.0 
 * @Date 2019/07/30
 */
@Controller
@RequestMapping("/api/donate/merchant")
public class DonateMerchantController {
	private final Logger LOGGER = LoggerFactory.getLogger(DonateMerchantController.class);
	
	private final String CHAR_SET = "utf-8";
	
    @Resource
    private MerchantService merchantService;
    
    @Autowired
    private MessageSource messageSource;
    
     private Locale locale = new Locale("zh_TW");
    
    @Autowired
    private Environment env;
    
	/**
     * 
     * checkPin:use accountId, from scan2pay-data get Merchant, compare pin
     * 
     * @author mustang 
     * @param {"accountId":"特店代號1","pin": "特店密碼"}
     * @return {"msgCode":"0000","msg":"","name"="特店名稱"}
     * @return {"msgCode":"0001","msg":"請先輸入特店的帳號和密碼","name"=""}
     * @return {"msgCode":"0002","msg":"帳號密碼錯誤!!" ,"name"=""}
     * @return {"msgCode":"9999","msg":"發生例外錯誤，請聯繫系統管理員","name"=""}
     * @date Create Time: 2019/07/30
     */
    @RequestMapping(method = RequestMethod.POST, value = "/CheckPin")
    public ResponseEntity<String> checkPin (@RequestBody Map<String, String> requestMap){
    	try {
	    	String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
	    	String requestBody = requestMap.get("Request");
	        String encryptApiKey = requestMap.get("ApiKey");
	    	SecretKey secretKey = decryptSecretKey(encryptApiKey);

			String body = AesCryptoUtil.decrypt(secretKey, Constant.IV, Base64.decode(requestBody));
//			LOGGER.info("[REQUEST][CheckPin][" + sessionId + "][BODY]" + body); //test
			
			JsonObject jsonObject = new JsonParser().parse(body).getAsJsonObject();
//            JsonObject header = jsonObject.getAsJsonObject("Header");
//			RequestHeader requestHeader = new Gson().fromJson(header.toString(), RequestHeader.class);
            JsonPrimitive data = jsonObject.getAsJsonPrimitive("Data");
            JsonObject requestData = new Gson().fromJson(data.getAsString(), JsonObject.class);
            
            Map<String, String> generalResponse = new HashMap<String, String>();
            if (!requestData.has("accountId") || !requestData.has("pin")) {
            	generalResponse.put("msgCode", "0001");
				generalResponse.put("msg", messageSource.getMessage("api.merchant.checkPin.msg.0001", null, locale)); // 請先輸入特店的帳號和密碼
				generalResponse.put("name", "");
				return new ResponseEntity<String>(new Gson().toJson(generalResponse), HttpStatus.BAD_REQUEST);
            }
            String accountId = requestData.get("accountId").getAsString();
            String pin = requestData.get("pin").getAsString();
			if (StringUtils.isEmpty(accountId) || StringUtils.isEmpty(pin)) {
				generalResponse.put("msgCode", "0001");
				generalResponse.put("msg", messageSource.getMessage("api.merchant.checkPin.msg.0001", null, locale)); // 請先輸入特店的帳號和密碼
				generalResponse.put("name", "");
				return new ResponseEntity<String>(new Gson().toJson(generalResponse), HttpStatus.BAD_REQUEST);
			}
			
    		generalResponse = merchantService.checkPin(accountId, pin);				
	        return new ResponseEntity<String>(new Gson().toJson(generalResponse), HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
			Map<String, String> generalResponse = new HashMap<String, String>();
			generalResponse.put("msgCode", "9999");
			generalResponse.put("msg", messageSource.getMessage("api.merchant.checkPin.msg.9999", null, locale)); // 發生例外錯誤，請聯繫系統管理員
			generalResponse.put("name", "");
			return new ResponseEntity<String>(new Gson().toJson(generalResponse), HttpStatus.INTERNAL_SERVER_ERROR);
		}		
    }
    
    private SecretKey decryptSecretKey(String encryptApiKey) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(env.getProperty("intella.private.key")).getFile());
        PrivateKey privateKey = KeyReader.loadPrivateKeyFromDER(file);
        
        byte[] rsaEncrypt = Base64.decode(encryptApiKey);
        String aesBase64Key = RsaCryptoUtil.decrypt(rsaEncrypt, privateKey);
        byte[] aesEncrypt = Base64.decode(aesBase64Key);
        SecretKey secretKey = AesCryptoUtil.convertSecretKey(aesEncrypt);

        return secretKey;
    }
}
