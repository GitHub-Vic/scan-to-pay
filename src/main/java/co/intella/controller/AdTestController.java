package co.intella.controller;

import co.intella.crypto.AesCryptoUtil;
import co.intella.crypto.KeyReader;
import co.intella.domain.aipei.AipeiPaymentResponseData;
import co.intella.domain.aipei.AipeiPaymentResponseParaData;
import co.intella.domain.allpay.AllpayAio;
import co.intella.domain.contratStore.MerchantIncome;
import co.intella.domain.test.TestCreditCardRequest;
import co.intella.model.*;
import co.intella.net.Constant;
import co.intella.net.HttpRequestUtil;
import co.intella.service.*;
import co.intella.utility.GeneralUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import com.turo.pushy.apns.ApnsClient;
import com.turo.pushy.apns.ApnsClientBuilder;
import com.turo.pushy.apns.PushNotificationResponse;
import com.turo.pushy.apns.util.ApnsPayloadBuilder;
import com.turo.pushy.apns.util.SimpleApnsPushNotification;
import com.turo.pushy.apns.util.TokenUtil;
import io.netty.util.concurrent.Future;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.context.Context;

import javax.annotation.Resource;
import javax.crypto.SecretKey;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URL;
import java.security.PublicKey;
import java.security.Signature;
import java.util.*;
import java.util.concurrent.ExecutionException;

import static org.springframework.http.HttpHeaders.USER_AGENT;

/**
 * @author Miles
 */
@Controller
@RequestMapping("/api/test")
public class AdTestController {

    private final Logger LOGGER = LoggerFactory.getLogger(AdTestController.class);

    @Resource
    private TicketLogicService ticketLogicService;

    @Autowired
    private Environment env;

    @Resource
    private TestService testService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private BankService bankService;

    @Resource
    private AllpayService allpayService;

    @Resource
    private OrderService orderService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private EzCardService ezCardService;

    @Resource
    private MailService mailService;

    @Resource
    private NotifyService notifyService;

    @RequestMapping(method = RequestMethod.POST, value = "/aesTest")
    public ResponseEntity<String> aesTest(@RequestBody Map<String, String> requestBody) throws Exception {

        LOGGER.info("Request" + new Gson().toJson(requestBody));

        Map<String, String> response = new HashMap<String, String>();

        if (ezCardService.bootDevice(requestBody.get("DevID"), requestBody.get("TerID"))) {
            response.put("AESKey", "72737475767778797a31323334353637");
        } else {
            response.put("AESKey", "");
        }
//        SecretKey secretKey = AesCryptoUtil.convertSecretKey(Base64.decode(""));

//        AesCryptoUtil.decrypt(secretKey, Constant.IV, Base64.decode(requestBody));

//        Merchant merchant = merchantService.getOneByLoginId("super");

//        LOGGER.info(new ObjectMapper().writeValueAsString(merchant));


//        String responBody = Base64.encode(AesCryptoUtil.encrypt(secretKey,Constant.IV,res));

        return new ResponseEntity<String>(new Gson().toJson(response), HttpStatus.OK);

    }


    @RequestMapping(method = RequestMethod.GET, value = "/loginId")
    public void loginId() throws Exception {

        Merchant merchant = merchantService.getOneByLoginId("super");

        LOGGER.info(new ObjectMapper().writeValueAsString(merchant));

    }


    @RequestMapping(method = RequestMethod.GET, value = "/aes")
    public ModelAndView aes() throws Exception {

        ModelAndView model = new ModelAndView();
        model.setViewName("test/aesTest");
        return model;

    }

    @RequestMapping(method = RequestMethod.GET, value = "/invalidQrcode")
    public ModelAndView invalidQrcode() throws Exception {

        ModelAndView model = new ModelAndView();
        model.setViewName("invalidQrcode");
        return model;

    }

    @RequestMapping(method = RequestMethod.GET, value = "/orderTest/{merchantId}")
    public ResponseEntity<String> getorderId(@PathVariable String merchantId) throws Exception {

        String orderid = orderService.createOrderId(merchantId);

        return new ResponseEntity<String>(orderid, HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.POST, value = "/aipeitest")
    public ModelAndView tet2222(@RequestBody AipeiPaymentResponseData input) {
        LOGGER.info("fefw");
        AipeiPaymentResponseParaData data = new Gson().fromJson(input.getData(), AipeiPaymentResponseParaData.class);

        return null;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/notifyMerchant/{merchantId}")
    public ResponseEntity<String> notifyMerchant2(@PathVariable String merchantId) throws Exception {
        notifyService.notifyMerchantTest(merchantId);
        return new ResponseEntity<String>("OK", HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.POST, value = "/notifyMerchant")
    public ModelAndView verify(@RequestBody String input) throws Exception {
        LOGGER.info(input);

        LinkedHashMap map = new Gson().fromJson(input, LinkedHashMap.class);
        String sign = map.get("Sign").toString();

        LOGGER.info("sing = " + sign);

        map.remove("Sign");

        ClassLoader classLoader = getClass().getClassLoader();
        Signature signature = Signature.getInstance("SHA256withRSA");
        File file = new File(classLoader.getResource("key/stage-pub.der").getFile());
        PublicKey publicKey = KeyReader.loadPublicKeyFromDER(file);
        signature.initVerify(publicKey);
        signature.update(new Gson().toJson(map).getBytes());
        boolean bool = signature.verify(Base64.decode(sign.getBytes()));

        LOGGER.info(bool ? "YES" : "NO");

        return null;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/allpayAio")
    public ModelAndView allpayAio(@org.springframework.web.bind.annotation.RequestHeader(value = "User-Agent") String agent) {

        PaymentAccount paymentAccount = paymentAccountService.getOne("super", 5);
        AllpayAio allpayAio = new AllpayAio();
        allpayAio.setMerchantId("2000132");
        allpayAio.setMerchantTradeNo("sfw" + DateTime.now().toString(SystemInstance.DATE_PATTERN));
        allpayAio.setStoreId("sttore001");
        allpayAio.setMerchantTradeDate("2017/07/27 17:09:00");
        allpayAio.setPaymentType("aio"); //aio
        allpayAio.setTotalAmount("30");
        allpayAio.setTradeDesc("test111");
        allpayAio.setItemName("sausage");
        allpayAio.setReturnURL("https://s.intella.co/allpaypass/api/notify/ecpay");
        allpayAio.setChoosePayment("ALL");
        allpayAio.setNeedExtraPaidInfo("N");
        allpayAio.setPlatformId(SystemInstance.EMPTY_STRING);
        allpayAio.setInvoiceMark("N");
        allpayAio.setHoldTradeAMT("0");

        allpayAio.setCustomField1(SystemInstance.EMPTY_STRING);
        allpayAio.setCustomField2(SystemInstance.EMPTY_STRING);
        allpayAio.setCustomField3(SystemInstance.EMPTY_STRING);
        allpayAio.setCustomField4(SystemInstance.EMPTY_STRING);

        //atm
        allpayAio.setExpireDate("1");
        allpayAio.setPaymentInfoURL("https://s.intella.co/allpaypass/api/notify/ecpay");

        //cvs
        allpayAio.setStoreExpireDate("7");
        allpayAio.setDesc1(SystemInstance.EMPTY_STRING);
        allpayAio.setDesc2(SystemInstance.EMPTY_STRING);
        allpayAio.setDesc3(SystemInstance.EMPTY_STRING);
        allpayAio.setDesc4(SystemInstance.EMPTY_STRING);

        //credit
        allpayAio.setBindingCard("0");
        allpayAio.setBindingCard(SystemInstance.EMPTY_STRING);

        allpayAio.setEncryptType("1");

        try {
            String ecPayOrderResponse = allpayService.AioAll(allpayAio, paymentAccount);
            ModelAndView model = new ModelAndView();
            model.setViewName("allpayAioPage");
            model.addObject("merchantId", "2000132");
            model.addObject("sotreOrderId", allpayAio.getMerchantTradeNo());
            model.addObject("createTime", allpayAio.getMerchantTradeDate());
            model.addObject("amount", allpayAio.getTotalAmount());
            model.addObject("itemName", allpayAio.getItemName());
            model.addObject("desc", allpayAio.getTradeDesc());
            model.addObject("notifyUrl", allpayAio.getReturnURL());
            model.addObject("returnUrl", "https://s.intella.co/allpaypass/api/test/2");
            model.addObject("checkMacValue", ecPayOrderResponse);
            //model.addObject("html",ecPayOrderResponse);

            return model;

        } catch (Exception e) {
            e.printStackTrace();
            return null;

        }

    }

    @RequestMapping(value = "/docker")
    public ResponseEntity<String> testDockerSourceIp() throws Exception {
        String url = "http://a1.intella.co/intella";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // optional default is GET
        con.setRequestMethod("GET");
        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);
        int responseCode = con.getResponseCode();
        return new ResponseEntity<String>(String.valueOf(responseCode), HttpStatus.OK);
    }

    @RequestMapping(value = "/ip")
    public ResponseEntity<String> testingip(HttpServletRequest request) throws Exception {

        String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();

        LOGGER.info("Session:[" + sessionId + "]");

        System.out.println("Your Host addr: " + InetAddress.getLocalHost().getHostAddress());  // often returns "127.0.0.1"
        Enumeration<NetworkInterface> n = NetworkInterface.getNetworkInterfaces();
        String str = "";
        for (; n.hasMoreElements(); ) {
            NetworkInterface e = n.nextElement();

            Enumeration<InetAddress> a = e.getInetAddresses();
            for (; a.hasMoreElements(); ) {
                InetAddress addr = a.nextElement();
                str = str + addr.getHostAddress();
            }
        }

        str = str + "\n\r\t</br>";

        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = (String) headerNames.nextElement();
            str = str + headerName + " = " + request.getHeader(headerName) + "\n\r\t</br>";
        }

        return new ResponseEntity<String>(str, HttpStatus.OK);
    }

    @RequestMapping("/test/save")
    public ResponseEntity<String> fefwf(@RequestBody String ffff) {
        TestUUID testUUID = new TestUUID();

        testUUID.setCom(ffff);
        testService.save(testUUID);
        testUUID.setCom(ffff + "213");
        testService.save(testUUID);
        return new ResponseEntity<String>("Hello Intella", HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/1")
    public ResponseEntity<String> testing1(@org.springframework.web.bind.annotation.RequestHeader(value = "User-Agent") String agent) {
        LOGGER.info("aget : " + agent);
        MailInformation mailInformation = new MailInformation();
        mailInformation.setMailTo(new String[]{"alex@intella.co"});
        mailInformation.setSubject("ezc settlement failed");

        Context ctx = new Context();
        ctx.setVariable("username", "a");
        ctx.setVariable("batchNo", "b");
        ctx.setVariable("ezc response", "c");

        try {
            mailService.send(mailInformation, ctx);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<String>("Hello Intella", HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/2")
    public ModelAndView testing2(HttpSession session) {
        LOGGER.info("session : " + session.toString());
        return new ModelAndView("redirect:" + "https://www.intellapay.org/weixin/api/ali/test1");
    }

    @RequestMapping("/3")
    public ModelAndView testing3(HttpSession session) {
        LOGGER.info("session : " + session.toString());
        ModelAndView model = new ModelAndView();
        model.addObject("fuck", "1231123");
        model.setViewName("advertisement");
        return model;
    }

    @RequestMapping("/w")
    public ModelAndView circle(HttpSession session) {
        LOGGER.info("session : " + session.toString());
        ModelAndView model = new ModelAndView();
        model.setViewName("resetPassword");
        return model;
    }

    //move to online controller
    @RequestMapping("/{token}")
    public ModelAndView testing4(@PathVariable(value = "token") String token) {
        ModelAndView model = new ModelAndView();
        model.setViewName("webagent");
        return model;
    }

    @RequestMapping("/slogan/{mechant}")
    public ResponseEntity<String> slogan(@PathVariable(value = "token") String token, HttpSession session) {


        LOGGER.info("session : ");
        ModelAndView model = new ModelAndView();
        session.setAttribute("sessiontest", token);
        model.setViewName("keyboard");
        return new ResponseEntity<String>("Hello Intella", HttpStatus.OK);
    }

    @RequestMapping("/allpaytest")
    public ModelAndView payTest(HttpSession session) {
        LOGGER.info("session : " + session.toString());
        ModelAndView model = new ModelAndView();
        model.setViewName("allpayCheckOut");
        model.addObject("randomId", "0D2AE0EB95F44B059A884964985F1B0F");
        model.addObject("orderNo", "0D2AE0EB95F44B059A884964985F1B0F");
        model.addObject("totalFee", "1");
        model.addObject("merchant", "super");
        model.addObject("domain", "https://a.intella.co/");
        model.addObject("allpayDomain", env.getProperty("host.request.allpay"));
        return model;
    }

    @RequestMapping("/method")
    public ModelAndView method(HttpSession session) {
        LOGGER.info("session : " + session.toString());
        ModelAndView model = new ModelAndView();
        model.setViewName("paymentMethod");
        return model;
    }

    @RequestMapping("/allpaysuccesstest")
    public ModelAndView paysuccessTest(HttpSession session) {
        LOGGER.info("session : " + session.toString());
        ModelAndView model = new ModelAndView();
        model.setViewName("allpayCheckOutSuccess");
        return model;
    }

    @RequestMapping("/simulation")
    public ModelAndView simulationTest(@org.springframework.web.bind.annotation.RequestHeader(value = "User-Agent") String agent, HttpSession session) {
        LOGGER.info("session : " + session.toString());
        LOGGER.info("agent : " + agent);

        ModelAndView model = new ModelAndView();
        model.setViewName("tradingSimulation");

        return model;
    }

    @RequestMapping("/fb")
    public ModelAndView fb() {
        ModelAndView model = new ModelAndView();
        model.setViewName("fb");
        return model;
    }

    @RequestMapping("/tsbTest")
    public ModelAndView tsBankTestPage() {
        ModelAndView model = new ModelAndView();
        model.setViewName("test/tsBankTestPage");
        model.addObject("mch", "super");

        DateTime now = DateTime.now();
        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyyMMddHHmmss");
        model.addObject("time", now.toString());
        model.addObject("oid", "tsApiTest" + generateSerialNumber());
        model.addObject("detail", "sausage");
        model.addObject("amount", "1");
        return model;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/apnsAll")
    public ResponseEntity<String> notifyWithAPNSAll() {


        appNotificationService.notifyApp(tradeDetailService.getOne("1tmvlc35k40000000082"), merchantService.getOne("super"));


        return new ResponseEntity<String>("success ", HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.POST, value = "/apns")
    public ResponseEntity<String> notifyWithAPNS(@RequestBody Map<String, String> input) {

        LOGGER.info("[REQUEST] api/test/apns" + new Gson().toJson(input));
        String message = input.get("message");
        String token = input.get("token");

        try {

            LOGGER.info("create client");
            final ApnsClient apnsClient = new ApnsClientBuilder()
                    .setClientCredentials(new File("/opt/key/apns1234.p12"), "1234")
                    .setApnsServer("api.development.push.apple.com").build();

            LOGGER.info("pushNotification start...");
            final SimpleApnsPushNotification pushNotification;

            {
                final ApnsPayloadBuilder payloadBuilder = new ApnsPayloadBuilder();
                payloadBuilder.setAlertBody(message);

                final String payload = payloadBuilder.buildWithDefaultMaximumLength();
                final String sanitizeTokenString = TokenUtil.sanitizeTokenString(token);

                pushNotification = new SimpleApnsPushNotification(sanitizeTokenString, "org.reactjs.native.example.pushNotifyRn", payload);
            }

            final Future<PushNotificationResponse<SimpleApnsPushNotification>> sendNotificationFuture = apnsClient.sendNotification(pushNotification);

            final PushNotificationResponse<SimpleApnsPushNotification> pushNotificationResponse = sendNotificationFuture.get();

            LOGGER.info("pushNotification end...");

            if (pushNotificationResponse.isAccepted()) {
                LOGGER.info("Push notification accepted by APNs gateway." + new Gson().toJson(input));
                return new ResponseEntity<String>("success", HttpStatus.OK);
            } else {
                LOGGER.error("Notification rejected by the APNs gateway:" + pushNotificationResponse.getRejectionReason());
                if (pushNotificationResponse.getTokenInvalidationTimestamp() != null) {
                    LOGGER.error("\t and the token is invalid as of " + pushNotificationResponse.getTokenInvalidationTimestamp());
                }
                return new ResponseEntity<String>("failed :" + pushNotificationResponse.toString(), HttpStatus.OK);
            }


        } catch (IOException e) {
            LOGGER.info("IOException." + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>("IOException", HttpStatus.OK);
        } catch (InterruptedException e) {
            LOGGER.info("InterruptedException." + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>("InterruptedException", HttpStatus.OK);
        } catch (ExecutionException e) {
            LOGGER.info("ExecutionException." + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>("ExecutionException", HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/tsbAjaxTest")
    public ResponseEntity<String> tsBankAjaxRequest(@RequestBody TestCreditCardRequest input) {
        try {
            LOGGER.info("[REQUEST] api/test/tsbAjaxTest" + new Gson().toJson(input));

            Map<String, String> requestData = new HashMap<String, String>();
            requestData.put("MchId", input.getMch());
            requestData.put("Method", "20800");
            requestData.put("ServiceType", "Payment");
            requestData.put("StoreOrderNo", input.getOrderId());
            requestData.put("CreateTime", "20170330120000");
            requestData.put("TimeExpire", "20170330123456789");
            requestData.put("DeviceInfo", "T0000000");
            requestData.put("Body", "");
            requestData.put("TotalFee", input.getAmount());
            requestData.put("FeeType", "TWD");
            requestData.put("Detail", input.getDetail());
            requestData.put("StoreInfo", "intella-taipei");
            requestData.put("Cashier", "miles");
            requestData.put("BarcodeMode", "barcodeMode");
            requestData.put("Extended", "");
            requestData.put("CardId", input.getCardId());
            requestData.put("ExtenNo", input.getExtendNo());
            requestData.put("ExpireDate", input.getExpireDate());
            requestData.put("TransMode", "0");
            //requestData.put("use_redeem", "1");

            SecretKey secretKey = AesCryptoUtil.generateSecreteKey();

            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("key/pub.der").getFile());
            PublicKey publicKey = KeyReader.loadPublicKeyFromDER(file);

            String responseEntity = HttpRequestUtil.httpsPost("https://a.intella.co/allpaypass/api/general", requestData, publicKey, secretKey);

            String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, responseEntity);


            JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);

            String respJson = getResponseJson(json.get("Data").getAsJsonObject().get("platformRsp").getAsString());

            return new ResponseEntity<String>(respJson, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("https://intella.co", HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/tsbAjaxTest2")
    public ResponseEntity<String> tsBankAjaxRequest2(@RequestBody TestCreditCardRequest input) {
        try {
            LOGGER.info("[REQUEST] api/test/tsbAjaxTest2" + new Gson().toJson(input));

            Map<String, String> requestData = new HashMap<String, String>();
            requestData.put("MchId", "super");
            requestData.put("Method", "20800");
            requestData.put("ServiceType", input.getCardId());
            requestData.put("StoreOrderNo", input.getOrderId());
            requestData.put("StoreRefundNo", "R" + input.getOrderId());
            requestData.put("RefundKey", "0000");


            requestData.put("CreateTime", "20170330120000");

            SecretKey secretKey = AesCryptoUtil.generateSecreteKey();

            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("key/pub.der").getFile());
            PublicKey publicKey = KeyReader.loadPublicKeyFromDER(file);

            String responseEntity = HttpRequestUtil.httpsPost("https://a.intella.co/allpaypass/api/general", requestData, publicKey, secretKey);

            String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, responseEntity);


            //JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);

            //String respJson = getResponseJson(json.get("Data").getAsJsonObject().get("platformRsp").getAsString());

            return new ResponseEntity<String>(decryptResponse, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("https://intella.co", HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/createMerchant")
    public ResponseEntity<String> createMerchant(@RequestBody MerchantIncome input) {

        String no;
        for (int i = 1; i <= 50; i++) {
            if (i < 10) no = "int0" + String.valueOf(i);
            else no = "int" + String.valueOf(i);


            Merchant newAccount = new Merchant();

            newAccount.setName(no);
            newAccount.setEmail("miles@intella.co");
            newAccount.setCreator(Long.getLong("0"));
            newAccount.setPin(GeneralUtil.getHash("0000"));
            newAccount.setAccountId(no);
            newAccount.setComId("123456");
            newAccount.setPhone("001");
            newAccount.setSignDate(DateTime.now().toString("yyyyMMddHHmmss"));
            newAccount.setStatus("active");
            newAccount.setPaycodeToken("XXXX");
            String activateToken = UUID.randomUUID().toString().substring(0, 15);
            newAccount.setActivateToken(activateToken);
            newAccount.setRefundPin("0000");
            newAccount = merchantService.create(newAccount);


            savePayment(newAccount, "103543903790001", 1, "Weixin");
            savePayment(newAccount, "54390379101", 2, "Ali online");
            savePayment(newAccount, "MWD", 3, "mwd");
            savePayment(newAccount, "0100829354", 4, "TC bank");
            savePayment(newAccount, "1812526", 5, "Allpay");
            savePayment(newAccount, "000812770060201", 8, "TS bank");
            savePayment(newAccount, "3027720", 10, "EC Pay");
            savePayment(newAccount, "42343041201", 11, "Ali offline");

            savaDevice(newAccount, "2000132999", "ECpay-001");
            savaDevice(newAccount, "23412342", "Allpay-001");
            savaDevice(newAccount, "T0000000", "TS-001");
            savaDevice(newAccount, "T0000000", "Ali");
//            savaDevice(newAccount, "P1", "super-001");
            savaDevice(newAccount, "skb0001", "super-001");


        }
        return new ResponseEntity<String>("Hello Intella", HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.POST, value = "/batchAddPaymentAccount")
    public ResponseEntity<String> batchAddPaymentAccount(@RequestBody MerchantIncome input) {

        List<Merchant> a = merchantService.listAll();

        for (Merchant m : a
        ) {
            if (m.getMerchantSeqId() >= 31) {
                savePayment(m, "999812666555047", 12, "ApplePay Test");

            }
        }


        return new ResponseEntity<String>("Hello Intella", HttpStatus.OK);

    }

    private void savaDevice(Merchant newAccount, String deviceId, String type) {
        Device device = new Device();

        device.setName(null);
        device.setDescription(null);
        device.setOwner(newAccount);
        device.setOwnerDeviceId(deviceId);
        device.setType(type);


        deviceService.save(device);
    }

    private void savePayment(Merchant newAccount, String account, int i, String desc) {
        PaymentAccount paymentAccount = new PaymentAccount();

        paymentAccount.setDescription(desc);
        paymentAccount.setAccount(account);
        paymentAccount.setBank(bankService.getOne(i));
        paymentAccount.setMerchant(newAccount);
        paymentAccount.setDefault(true);

        PaymentAccount entity = paymentAccountService.save(paymentAccount);
    }


    private String getResponseJson(String message) {
        Map<String, String> response = new HashMap<String, String>();
        response.put("url", message);
        return new Gson().toJson(response);
    }

    private String generateSerialNumber() {

        char seeds[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        char randStr[] = new char[10];
        for (int i = 0; i < randStr.length; i++) {
            randStr[i] = seeds[(int) Math.round(Math.random() * (seeds.length - 1))];
        }

        return new String(randStr);
    }


    @RequestMapping(method= RequestMethod.GET, value="/testNotifyApp/{merchantId}/{orderId}")
    public ResponseEntity<String> testNotifyApp(@PathVariable String merchantId, @PathVariable String orderId) {

        appNotificationService.notifyApp(merchantId,orderId);
        return new ResponseEntity<String>(String.valueOf("123"), HttpStatus.OK);
    }
}
