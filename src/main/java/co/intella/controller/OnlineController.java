package co.intella.controller;

import co.intella.constant.BankNameEnum;
import co.intella.domain.ali.AliRequestBody;
import co.intella.domain.ecpay.ECPayOrder;
import co.intella.domain.ecpay.ECPayOrderResponse;
import co.intella.domain.gama.GamaCreateOrderResponseData;
import co.intella.domain.line.LineReserveRequestData;
import co.intella.domain.line.LineResponseData;
import co.intella.domain.twpay.TwPayResponse;
import co.intella.domain.weixin.WeixinJsapiRequestData;
import co.intella.exception.OrderAlreadyExistedException;
import co.intella.model.*;
import co.intella.repository.OrderIdByAccountIdRepository;
import co.intella.repository.impl.QrType7RepositoryImpl;
import co.intella.repository.impl.QrType8RepositoryImpl;
import co.intella.service.*;
import co.intella.utility.HttpUtil;
import co.intella.utility.JsonUtil;
import co.intella.utility.OnlinepayUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * Scan2pay Api
 *
 * @author Alex
 */
@Controller
@RequestMapping("/api/general")
public class OnlineController {

    private final Logger LOGGER = LoggerFactory.getLogger(OnlineController.class);

    @Value("${api.url}")
    private String API_URL;

    @Value("${spring.redis.key.timeout}")
    private int REDIS_TIMEOUT;

    @Value("${web.pos.host.domain}")
    private String WEB_POST_HOST_DOMAIN;

    @Value("${web.pos.edgeUrl.domain}")
    private String EDGE_URL;

    @Value("${hotst.mgt.server}")
    private String MGT_SERVER_URL;

    @Value("${payment.gateway.api.origin}")
    private String PAYMENT_GETWAP_API_URL;

    @Resource
    private ECPayService ecPayService;

    @Resource
    private AliService aliService;

    @Resource
    private WeixinService weixinService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private OrderService orderService;

    @Autowired
    private Environment env;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private EdgeService edgeService;

    private Locale locale = new Locale("zh_TW");

    @Resource
    private QueryJobService jobService;

    @Resource
    private Scan2PayPropertyService propertyService;

    @Resource
    private LineService lineService;

    @Resource
    private TradeProcedureService tradeProcedureService;
    @Resource
    private AlipayShangHaiService alipayShangHaiService;

    @Resource
    TwPayService twPayService;

    @Resource
    private GamaPayService gamaPayService;

    @Resource
    private YuanTaOfflineService yuanTaOfflineService;

    @Resource
    private RedisService redisService;

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private MultiQrcodeService multiQrcodeService;

    private static final String MULTI_ORDER_PERMISSION = "M04F01";

    @Resource
    private ImotoTokenService imotoTokenService;

    @Resource
    private ImotoTokenLogService imotoTokenLogService;

    @Autowired
    private OrderIdByAccountIdRepository orderIdByAccountIdRepository;

    @Autowired
    private QrType7RepositoryImpl qrType7RepositoryImpl;
    @Autowired
    private QrType8RepositoryImpl qrType8RepositoryImpl;

    @Resource
    private LookupCodeService lookupCodeService;

    @Resource
    private DiscountService discountService;

    @Resource
    private BankService bankService;

    @Resource
    private PaymentMethodController paymentMethodController;

    @Resource
    private PaymentMethodService paymentMethodService;

    @Resource
    private QrType7Service qrType7Service;

    @Resource
    private NewPiService newPiService;

    @Resource
    private OnlinepayUtil OnlinepayUtil;

    @Resource
    private DiscountTradeOLPayService discountTradeOLPayService;

    // customer decides amount
    @RequestMapping(method = RequestMethod.GET, value = "/onlinepay/{urlToken}/{amount}")
    public ResponseEntity<String> onlineWithoutFee(
            @org.springframework.web.bind.annotation.RequestHeader(value = "User-Agent") String agent,
            @PathVariable String urlToken, @PathVariable String amount) throws Exception {

        LOGGER.info("[REQUEST] api/general/onlinepay/" + urlToken + "/" + amount);
        LOGGER.info("[REQUEST] api/general/onlinepay/       User-Agent => " + agent);

        String platformURL;
        String storeOrderNoNew;
        QrcodeParameter qrcodeParameter;

        qrcodeParameter = qrcodeService.getOneByShortId(urlToken);

        if (qrcodeParameter == null) {
            return new ResponseEntity<String>(messageSource.getMessage("response.7158", null, locale), HttpStatus.OK);
        }
        if (qrcodeService.checkQrcodeExpired(qrcodeParameter)) {
            return new ResponseEntity<String>(messageSource.getMessage("response.7158", null, locale), HttpStatus.OK);
        }

        TradeDetail tradeDetail;
        String integrateMchId;
        if (qrcodeParameter.getCodeType().equals("3") ||
//        		qrcodeParameter.getCodeType().equals("2") ||
                qrcodeParameter.getCodeType().equals("5") || qrcodeParameter.getCodeType().equals("6") || qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) { // adds
            // codeType:5
            // & 6

//            storeOrderNoNew =String.format("%02d", new Random().nextInt(99))+ qrcodeParameter.getStoreOrderNo() + DateTime.now().toString(SystemInstance.DATE_YYPATTERN);

            storeOrderNoNew = orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
//            qrcodeParameter.setTimeExpire(DateTime.now().plusMinutes(5).toString(SystemInstance.DATE_PATTERN));
        } else {
            storeOrderNoNew = qrcodeParameter.getStoreOrderNo();
        }

        integrateMchId = qrcodeParameter.getMchId();
        tradeDetail = tradeDetailService.getOne(integrateMchId, storeOrderNoNew);

        // add last 4 code
        String body = qrcodeService.getRandomBody(qrcodeParameter);
        qrcodeParameter.setBody(body);

        if (tradeDetail != null) {
            boolean flag = tradeDetail.getStatus().equals("Waiting")
                    || tradeDetail.getStatus().equals(SystemInstance.TRADE_SUCCESS);
            if (flag) {
                return new ResponseEntity<String>(tradeDetail.getOnlinePayUrl(), HttpStatus.OK);
            }

        }

        LOGGER.info("before platform dispatch");
        try {

            if (agent.contains(env.getProperty("weixinAgent"))
                    && paymentAccountService.getOne(integrateMchId, 1) != null) {
                LOGGER.info("[REQUEST][ONLINE][WX]");

                platformURL = weixinOnline(qrcodeParameter, storeOrderNoNew, amount, integrateMchId, urlToken);

            } else if (agent.contains(env.getProperty("aliAgent"))
                    && paymentAccountService.getOne(integrateMchId, 2) != null) {
                LOGGER.info("[REQUEST][ONLINE][ALI]");

                platformURL = aliOnline(qrcodeParameter, storeOrderNoNew, amount, integrateMchId, urlToken);
            } else if (agent.contains(env.getProperty("aliAgent"))
                    && paymentAccountService.getOne(integrateMchId, 11) != null) {
                LOGGER.info("[REQUEST][ONLINE][ALISHANGHAI]");
                long bankId = Long.valueOf(env.getProperty("bank.id.11100"));
                PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);

                platformURL = yuanTaOfflineService.createOrder(tradeDetail, qrcodeParameter, amount, storeOrderNoNew,
                        paymentAccount);
            } else if (agent.contains(env.getProperty("aliAgent"))
                    && paymentAccountService.getOne(integrateMchId, 25) != null) {
                LOGGER.info("[REQUEST][ONLINE][ALISHANGHAI]");
                long bankId = Long.valueOf(env.getProperty("bank.id.12520"));
                PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);

                platformURL = alipayShangHaiService.createOrder(tradeDetail, qrcodeParameter, Integer.valueOf(amount),
                        storeOrderNoNew, paymentAccount);
            } else if (agent.contains(env.getProperty("allpayAgent"))
                    && paymentAccountService.getOne(integrateMchId, 5) != null) {
                LOGGER.info("[REQUEST][ONLINE][ALL PAY]");

                long bankId = Long.valueOf(env.getProperty("bank.id.10500"));
                PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);

                WeixinJsapiRequestData weixinJsapiRequestData = new WeixinJsapiRequestData();

                weixinJsapiRequestData.setOutTradeNo(storeOrderNoNew);
                weixinJsapiRequestData.setTotalFee(amount);
                weixinJsapiRequestData.setDeviceInfo(qrcodeParameter.getDeviceInfo());
                weixinJsapiRequestData.setStoreId(qrcodeParameter.getStoreInfo());
                weixinJsapiRequestData.setBody(qrcodeParameter.getBody());

                tradeDetail = setOnlineTradeDetailRequest(weixinJsapiRequestData, integrateMchId, paymentAccount,
                        qrcodeParameter);

                platformURL = API_URL + "allpaypass/api/allpay/checkout/"
                        + tradeDetail.getTradeDetailRandomId().toString();

            } else if (agent.contains(env.getProperty("lineAgent"))
                    && paymentAccountService.getOne(integrateMchId, 15) != null) {

                LOGGER.info("line redirect url ");
                long bankId = Long.valueOf(env.getProperty("bank.id.11500"));
                PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);
                platformURL = getLineUrl(qrcodeParameter, storeOrderNoNew, amount, integrateMchId, urlToken,
                        paymentAccount);

            } else if (agent.contains(env.getProperty("twmpAgent"))
                    && paymentAccountService.getOne(integrateMchId, 20) != null) {

                LOGGER.info("twpay redirect url ");
                String response = twPayService.getPaymentUrl(amount, urlToken, agent);
                TwPayResponse twPayResponse = new Gson().fromJson(response, TwPayResponse.class);
                platformURL = twPayResponse.getUriSchema();

            } else if (agent.contains(env.getProperty("gamaAgent"))
                    && paymentAccountService.getOne(integrateMchId, 13) != null) {

                LOGGER.info("gamapay redirect url");
                String response = gamaPayService.getPaymentUrl(amount, urlToken, agent);
                GamaCreateOrderResponseData gamaCreateOrderResponseData = new Gson().fromJson(response,
                        GamaCreateOrderResponseData.class);
                platformURL = gamaCreateOrderResponseData.getgPPUrl();// 正在寫

            } else if (agent.contains(env.getProperty("piAgent"))
                    && paymentAccountService.getOne(integrateMchId, 59) != null) {

                LOGGER.info("new  Pi redirect url");
                platformURL = newPiService.getPaymentUrl(amount, urlToken, agent);

            } else {
                LOGGER.info("[REQUEST][ONLINE][EC]");

                long bankId = Long.valueOf(env.getProperty("bank.id.21100"));

                PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);
                ECPayOrder ecPayOrder = new ECPayOrder();
                ecPayOrder.setMerchantId(paymentAccount.getAccount());
                ecPayOrder.setMerchantTradeNo(storeOrderNoNew);
                ecPayOrder.setStoreId("");
                ecPayOrder.setMerchantTradeDate(DateTime.now().toString("yyyy/MM/dd hh:mm:ss"));
                ecPayOrder.setPaymentType("aio"); // aio
                ecPayOrder.setTotalAmount(amount);
                ecPayOrder.setTradeDesc(qrcodeParameter.getDetail() == null ? "Intella" : qrcodeParameter.getDetail());
                ecPayOrder.setItemName(qrcodeParameter.getBody());
                ecPayOrder.setReturnURL(env.getProperty("api.url") + "allpaypass/api/notify/ecpay");
                ecPayOrder.setChoosePayment("ALL");
                ecPayOrder.setNeedExtraPaidInfo("N");
//            ecPayOrder.setNeedExtraPaidInfo("Y");
                ecPayOrder.setPlatformId("");
                ecPayOrder.setInvoiceMark("N");
                ecPayOrder.setHoldTradeAMT("0");
                ecPayOrder.setCustomField1("");
                ecPayOrder.setCustomField2("");
                ecPayOrder.setCustomField3("");
                ecPayOrder.setCustomField4("");
                ecPayOrder.setEncryptType("1");

                ECPayOrderResponse ecPayOrderResponse = ecPayService.createOrder(ecPayOrder);
                LOGGER.info("[RESPONSE][ONLINE][EC]" + new Gson().toJson(ecPayOrderResponse));

                Merchant merchant = merchantService.getOne(integrateMchId);

                tradeDetail = tradeDetailService.getOne(integrateMchId, storeOrderNoNew);
                if (tradeDetail != null) {
                    LOGGER.info("[SQL] \"" + storeOrderNoNew + "\" Order has been Existed.");
                } else {
                    tradeDetail = new TradeDetail();
                    tradeDetail.setAccountId(integrateMchId);
                    tradeDetail.setMethod("21100");
                    tradeDetail.setPaymentAccount(paymentAccount);
                    tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
                    tradeDetail.setStatus("Creating order");
                    tradeDetail.setOrderId(storeOrderNoNew);
                    tradeDetail.setDeviceRandomId(deviceService.getOne(merchant, qrcodeParameter.getDeviceInfo()));
                    tradeDetail.setPayment(Long.parseLong(amount));
                    tradeDetail.setServiceType("Payment");
                    tradeDetail.setQrcodeToken(urlToken);
                    tradeDetail.setTradeToken(ecPayOrderResponse.getSpToken());
                    tradeDetail.setDescription(qrcodeParameter.getBody());
                    if (qrcodeParameter.getOnSaleTotalFee() == Long.parseLong(amount)) {
                        tradeDetail.setOnSale(true);
                    } else {
                        tradeDetail.setOnSale(false);
                    }
                    tradeDetail.setOriginalPrice(qrcodeParameter.getTotalFee());

                    LOGGER.info("save online trade detail : " + new ObjectMapper().writeValueAsString(tradeDetail));

                    tradeDetail = tradeDetailService.save(tradeDetail);
                    tradeProcedureService.captureOrderSave(tradeDetail);
                }
                String description;
                if (tradeDetail.getDescription() == null) {
                    description = "emptyItem";
                } else {
                    description = tradeDetail.getDescription();
                }

                platformURL = API_URL + "allpaypass/api/payment/ec/" + tradeDetail.getTradeToken() + "/" + amount + "/"
                        + tradeDetail.getOrderId() + "/" + paymentAccount.getAccount() + "/" + description + "/"
                        + tradeDetail.getQrcodeToken() + "/" + merchant.getAccountId();
                LOGGER.info("Direct to " + platformURL);
            }

            return new ResponseEntity<String>(platformURL, HttpStatus.OK);
        } catch (OrderAlreadyExistedException e) {
            LOGGER.info("[Online][TradeExist]");
            platformURL = API_URL + "allpaypass/api/test/invalidQrcode";
            return new ResponseEntity<String>(platformURL, HttpStatus.OK);
        }

    }

    // private boolean checkQrcodeExpired(QrcodeParameter qrcodeParameter) {
//        if (qrcodeParameter.getCodeType().equals("4")){
//            if (DateTime.parse(qrcodeParameter.getCreateDate(), DateTimeFormat.forPattern(SystemInstance.DATE_PATTERN)).isBeforeNow()){
//                return true;
//            }
//        }
//        return false;
//    }

    private ModelAndView checkUrl(String url) {
        String[] s = url.split("\\.");
        String shortId = s[0];
        String end = s[1];
        QrcodeParameter qrcodeParameter = qrcodeService.getOriginOneByShortId(shortId);
        if (qrcodeParameter != null) {// TODO 驗證QRCODE
            LOGGER.info("[REQUEST] get qrcodeParameter" + qrcodeParameter.getMchId());
            Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId()); // for codeType "5" 用accountId查
            if (merchant == null) {// TODO 驗證 merchant
                LOGGER.info("[Qroce][accountNotActived]");
                ModelAndView modelAndView = new ModelAndView();
                modelAndView.setViewName("accountNotActived");
                return modelAndView;
            }
        } else {
            LOGGER.info("[Qroce][isNull]");
        }
        ModelAndView modelAndView = new ModelAndView();

        switch (end) {
            case "qr":
                // TODO
                modelAndView = this.showQrCode(qrcodeParameter, modelAndView);
                break;
            case "imoto":
                // TODO
                modelAndView = this.showImoto(qrcodeParameter, modelAndView, shortId);
                break;
            case "info":
                // TODO
                modelAndView = this.showInfo(qrcodeParameter, modelAndView, shortId);
                break;
            default:
                modelAndView = null;
                break;
        }
        return modelAndView;
    }

    // TODO qr(); 設定新頁面
    private ModelAndView showQrCode(QrcodeParameter qrcodeParameter, ModelAndView modelAndView) {
        modelAndView.setViewName("showQRcode");
        Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());
        String modifiedUrl = API_URL + qrcodeParameter.getShortId();
        modelAndView.addObject("url", modifiedUrl);
        modelAndView.addObject("parameterBody", qrcodeParameter.getBody());
        modelAndView.addObject("mchName", merchant.getCompanyName());
        modelAndView.addObject("detail", qrcodeParameter.getDetail());
        modelAndView.addObject("totalFee", qrcodeParameter.getTotalFee());
        modelAndView.addObject("codeType", qrcodeParameter.getCodeType());
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/onlinepay/{urlToken:.+}")
    public ModelAndView onlinePay(
            @org.springframework.web.bind.annotation.RequestHeader(value = "User-Agent") String agent,
            @PathVariable String urlToken, HttpServletRequest request, HttpServletResponse response) throws Exception {

        LOGGER.info("[REQUEST] /onlinepay/" + urlToken);
        LOGGER.info("[REQUEST] /onlinepay/   User-Agent => " + agent);

        String platformURL;
        String storeOrderNoNew;
        QrcodeParameter qrcodeParameter;
        Merchant merchant;

        if (urlToken.matches("\\w*\\.\\w*")) {
            if (urlToken.startsWith("intella.")) {//如果是intella.QR 轉成小寫
                urlToken = urlToken.toLowerCase();
            }
            ModelAndView qrcodePage = this.checkUrl(urlToken);
            if (nonNull(qrcodePage)) {
                return qrcodePage;
            }
        }
        Bank bank = null;
        qrcodeParameter = qrcodeService.getOriginOneByShortId(urlToken);

        if (lookupCodeService.isFGSAccount(qrcodeParameter.getMchId())) {
            // J 要求佛光山都不能因為agent，而有預設支付
            agent ="";
        }


        if (qrcodeParameter != null) {
            LOGGER.info("[REQUEST] get qrcodeParameter" + qrcodeParameter.getMchId());
            merchant = merchantService.getOne(qrcodeParameter.getMchId()); // for codeType "5"
            if (merchant == null) {
                LOGGER.info("[Qroce][accountNotActived]");
                ModelAndView modelAndView = new ModelAndView();
                modelAndView.setViewName("accountNotActived");
                return modelAndView;
            } else if ("7".equals(qrcodeParameter.getCodeType()) || "8".equals(qrcodeParameter.getCodeType())) {
                LOGGER.info("[REQUEST] get qrcodeParameter type7");

                Map<String, String> reqMap = new HashMap<>();
                reqMap.put("shortId", qrcodeParameter.getShortId());
                Map<String, String> resultMap = JsonUtil.parseJson(HttpUtil.doPostJson(MGT_SERVER_URL + "dmz/edgeToken/qrType7", JsonUtil.toJsonStr(reqMap)), Map.class);
                EncryptType encryptType = JsonUtil.parseJson(JsonUtil.toJsonStr(resultMap.get("data")), EncryptType.class);

                ModelAndView model = new ModelAndView();
                //TODO  要去打MGT 拿age資料
                if ("7".equals(qrcodeParameter.getCodeType())) {
                    model.setViewName("demoInfo");
                } else {
                    model.setViewName("qrType8");
                }
                model.addObject("shortId", urlToken);
                model.addObject("edgeToken", encryptType.getToken());
                model.addObject("edgeKey", encryptType.getKey());
                model.addObject("edgeIv", encryptType.getIv());
                model.addObject("edgeType", encryptType.getEncryptType());
                model.addObject("hostDomain", API_URL);
                model.addObject("paymentGetwayApi", PAYMENT_GETWAP_API_URL);
                model.addObject("mchId", qrcodeParameter.getMchId());
                model.addObject("originBody", qrcodeParameter.getBody());
                LOGGER.info("return to demoInfo");
                return model;
            }
        } else {
            LOGGER.info("[Qroce][isNull]");
            ModelAndView model = new ModelAndView();
            model.setViewName("invalidQrcode");
            return model;
        }
        if (qrcodeParameter.getStatus() != 1) {
            qrcodeParameter = null;
        }

        TradeDetail tradeDetail;
        String integrateMchId;
        String totalFee = "0";
        String isMultiQRCode = "0";
        List<MultiQrcode> multiQRCodeList = null;

        if (qrcodeParameter != null) {
            if (qrcodeService.checkQrcodeExpired(qrcodeParameter)) {
                ModelAndView modelAndView = new ModelAndView();
                modelAndView.setViewName("overdueQrcode");
                return modelAndView;
            }
            String method = qrcodeParameter.getMethod();
            LOGGER.info("method=" + method);
            if ((!Strings.isNullOrEmpty(method)) && (!"00000".equals(method))) {
                Integer bankId = 0;
                bankId = paymentMethodService.getOne(method) == null ? 0 : paymentMethodService.getOne(method).getPaymentAccountBankId();
                bank = bankService.getOne(bankId);
                agent = "";
            }

            if (bank != null) {
                try {
                    String methodName = BankNameEnum.valueOf(bank.getName()).getBankName();
                    Method m = paymentMethodController.getClass().getMethod(methodName, String.class);
                    ModelAndView result = (ModelAndView) (m.invoke(paymentMethodController, urlToken));
                    return result;
                } catch (Exception e) {
                    LOGGER.info(bank.getName() + " can't match emum");//如果對不到方法的繼續往下跑
                }
            }

            LOGGER.info("[qrcodeParameter] codeType:" + qrcodeParameter.getCodeType());
            merchant = merchantService.getOne(qrcodeParameter.getMchId()); // for codeType "5"
            if (merchant == null) {
                ModelAndView modelAndView = new ModelAndView();
                modelAndView.setViewName("accountNotActived");
                return modelAndView;
            }

            if (null != merchant.getMgtPermission() && merchant.getMgtPermission().contains(MULTI_ORDER_PERMISSION)) {
                LOGGER.info("IS MULTI_ORDER_PERMISSION");
                multiQRCodeList = multiQrcodeService.findMutiQRCode(qrcodeParameter.getShortId());
                if (multiQRCodeList != null && !multiQRCodeList.isEmpty()) {
                    isMultiQRCode = "1";
                }
            }

            if (qrcodeParameter.getCodeType().equals("2")) {
                LOGGER.info("[Qrocde][Type2]");
                Map<String, String> resultMap = orderService.createOrderIdByMchId(String.valueOf(merchant.getMerchantSeqId()));
                storeOrderNoNew = resultMap.get("orderNo") + StringUtils.defaultIfBlank(qrcodeParameter.getStoreInfo(), "");
//                storeOrderNoNew = String.format("%02d", new Random().nextInt(99)) + qrcodeParameter.getStoreOrderNo()
//                        + DateTime.now().toString(SystemInstance.DATE_PATTERN);

//                qrcodeParameter.setTimeExpire(DateTime.now().plusMinutes(5).toString(SystemInstance.DATE_PATTERN));
                totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, ""));

            } else if (qrcodeParameter.getCodeType().equals("3") || qrcodeParameter.getCodeType().equals("6") || qrcodeParameter.getCodeType().equals("7") || qrcodeParameter.getCodeType().equals("8")) {
                ImotoToken ntuImotoToken = imotoTokenService.getOneForEffectiveByShortId(qrcodeParameter.getShortId());
                if (nonNull(ntuImotoToken)) {
                    ImotoTokenLog imotoTokenLog = imotoTokenLogService.getLastLogByOutercodeAndTokenSeq(ntuImotoToken.getTokenOutercode(), ntuImotoToken.getImotoTokenSeq());
                    if (nonNull(imotoTokenLog)) {
                        totalFee = String.valueOf(imotoTokenLog.getAmount());
                    } else {
                        try {
                            response.sendRedirect(String.format("%s%s.imoto", API_URL, qrcodeParameter.getShortId()));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                LOGGER.info("[Qrocde][Type" + qrcodeParameter.getCodeType() + "]");
                String customizedTemplate = "6".equals(qrcodeParameter.getCodeType())
                        ? qrcodeParameter.getCustomizedTemplate()
                        : "";

                if (agent.contains(env.getProperty("weixinAgent"))) {
                    long bankId = Long.valueOf(env.getProperty("bank.id.10110"));
                    PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);
                    if (paymentAccount == null) {
                        return noServiceReturn(urlToken);
                    }
                    return redirectToWebagent(totalFee, urlToken, customizedTemplate, false, "10110");
                } else if (agent.contains(env.getProperty("aliAgent"))) {
                    long bankId = Long.valueOf(env.getProperty("bank.id.10220"));
                    PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);
                    if (paymentAccount == null) {
                        return noServiceReturn(urlToken);
                    }
                    return redirectToWebagent(totalFee, urlToken, customizedTemplate, false, "10220");
                } else if (agent.contains(env.getProperty("allpayAgent"))) {
                    long bankId = Long.valueOf(env.getProperty("bank.id.10500"));
                    PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);
                    if (paymentAccount == null) {
                        return noServiceReturn(urlToken);
                    }
                    return redirectToWebagent(totalFee, urlToken, customizedTemplate, false, "10500");
                } else if (agent.contains(env.getProperty("piAgent"))) {
                    long bankId = Long.valueOf(env.getProperty("bank.id.15900"));
                    PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);
                    if (paymentAccount == null) {
                        return noServiceReturn(urlToken);
                    }
                    return redirectToWebagent(totalFee, urlToken, customizedTemplate, false, "15900");
                } else if (agent.contains(env.getProperty("lineAgent"))) {
                    LOGGER.info("[line][type6]");
                    long bankId = Long.valueOf(env.getProperty("bank.id.11500"));
                    PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);
                    if (paymentAccount == null) {
                        return noServiceReturn(urlToken);
                    }
                    return redirectToWebagent(totalFee, urlToken, customizedTemplate, false, "11500");
                } else if (agent != null && agent.contains(env.getProperty("twmpAgent"))) {
                    PaymentAccount twPayPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 20);
                    PaymentAccount tobTwPayPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 42);
                    PaymentAccount fbTwPayPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 43);
                    PaymentAccount lbTwPayPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 44);
                    PaymentAccount tsbTwPayPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 50);
                    PaymentAccount megaTwPayNewPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 56);
                    if (null != twPayPaymentAccount) {
                        return redirectTo("bank.id.12000", qrcodeParameter.getMchId(), urlToken, totalFee, customizedTemplate, false, "12000");
                    } else if (null != tobTwPayPaymentAccount) {
                        return redirectTo("bank.id.14200", qrcodeParameter.getMchId(), urlToken, totalFee, customizedTemplate, false, "14200");
                    } else if (null != fbTwPayPaymentAccount) {
                        return redirectTo("bank.id.14300", qrcodeParameter.getMchId(), urlToken, totalFee, customizedTemplate, false, "14300");
                    } else if (null != lbTwPayPaymentAccount) {
                        return redirectTo("bank.id.14400", qrcodeParameter.getMchId(), urlToken, totalFee, customizedTemplate, false, "14400");
                    } else if (null != tsbTwPayPaymentAccount) {
                        return redirectTo("bank.id.15000", qrcodeParameter.getMchId(), urlToken, totalFee, customizedTemplate, false, "15000");
                    } else if (null != megaTwPayNewPaymentAccount) {
                        return redirectTo("bank.id.15600", qrcodeParameter.getMchId(), urlToken, totalFee, customizedTemplate, false, "15600");
                    }
                } else if (agent != null && agent.contains(env.getProperty("gamaAgent"))) {
                    return redirectTo("bank.id.11300", qrcodeParameter.getMchId(), urlToken, totalFee,
                            customizedTemplate, false, "11300");
                }
                List<String> accountName = new ArrayList<String>();
                String accountId = merchant.getAccountId();
                String accountArray = redisService.getRedis(accountId);
                if (accountArray != null) {
                    accountName = Arrays.asList(accountArray.split(" - "));
                } else {
                    List<PaymentAccount> list = paymentAccountService.listByQrcode(urlToken);
                    LOGGER.info("[Qrocde][Type3][Unknown agent]");
                    for (PaymentAccount aList : list) {
                        if (aList.getSeqBy() != null) {
                            accountName.add(aList.getBank().getName() + "," + aList.getSeqBy());
                        } else {
                            accountName.add(aList.getBank().getName() + ",99");
                        }
                    }
                    try {
                        String accountConvert = String.join(" - ", accountName);
                        redisService.setRedis(accountId, accountConvert, REDIS_TIMEOUT);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (bank != null) {// 防止method給的是paymentAccount所沒有的	
                    String accountList = JsonUtil.toJsonStr(accountName);
                    if (!accountList.contains(bank.getName())) {
                        bank = null;
                    }
                }

                LOGGER.info("[payment account] " + accountName);

                ModelAndView model = new ModelAndView();
                model.setViewName("paymentMethod");
                model.addObject("bankName", bank == null ? "" : bank.getName());
                model.addObject("token", urlToken);
                model.addObject("domain", API_URL);
                model.addObject("accountName", accountName);
                model.addObject("applePayHost", propertyService.get("server.domain"));
                model.addObject("merchant", qrcodeParameter.getMchId());
                model.addObject("customizedTemplate", customizedTemplate);
                model.addObject("isCustom", "6".equals(qrcodeParameter.getCodeType()) ? "1" : "0"); // for codeType:6
                model.addObject("isMultiQRCode", isMultiQRCode);
                this.setModelViewDetail(model, merchant, qrcodeParameter, response);
                model.addObject("paymentBanner", StringUtils.isNotBlank(merchant.getPaymentBanner()) ? merchant.getPaymentBanner() : "");
                model.addObject("multiQrcodeList", multiQRCodeList != null ? new Gson().toJson(multiQRCodeList) : "");

                return model;
            } else if ("5".equals(qrcodeParameter.getCodeType())) { // for codeType "5"
                LOGGER.info("[Qrocde][Type5]");
                totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, ""));
                String customizedTemplate = qrcodeParameter.getCustomizedTemplate();

                if (agent.contains(env.getProperty("weixinAgent"))) {
                    long bankId = Long.valueOf(env.getProperty("bank.id.10110"));
                    PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);
                    if (paymentAccount == null) {
                        return noServiceReturn(urlToken);
                    }
                    return redirectToWebagent(totalFee, urlToken, customizedTemplate, true, "10110");
                } else if (agent.contains(env.getProperty("aliAgent"))) {
                    long bankId = Long.valueOf(env.getProperty("bank.id.10220"));
                    PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);
                    if (paymentAccount == null) {
                        return noServiceReturn(urlToken);
                    }
                    return redirectToWebagent(totalFee, urlToken, customizedTemplate, true, "10220");
                } else if (agent.contains(env.getProperty("allpayAgent"))) {
                    long bankId = Long.valueOf(env.getProperty("bank.id.10500"));
                    PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);
                    if (paymentAccount == null) {
                        return noServiceReturn(urlToken);
                    }
                    return redirectToWebagent(totalFee, urlToken, customizedTemplate, true, "10500");
                } else if (agent.contains(env.getProperty("lineAgent"))) {
                    long bankId = Long.valueOf(env.getProperty("bank.id.11500"));
                    PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);
                    if (paymentAccount == null) {
                        return noServiceReturn(urlToken);
                    }
                    return redirectToWebagent(totalFee, urlToken, customizedTemplate, true, "11500");
                } else if (agent.contains(env.getProperty("piAgent"))) {
                    long bankId = Long.valueOf(env.getProperty("bank.id.15900"));
                    PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);
                    if (paymentAccount == null) {
                        return noServiceReturn(urlToken);
                    }
                    return redirectToWebagent(totalFee, urlToken, customizedTemplate, true, "15900");
                } else if (agent != null && agent.contains(env.getProperty("twmpAgent"))) {
                    PaymentAccount twPayPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 20);
                    PaymentAccount tobTwPayPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 42);
                    PaymentAccount fbTwPayPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 43);
                    PaymentAccount lbTwPayPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 44);
                    PaymentAccount tsbTwPayPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 50);
                    PaymentAccount megaTwPayNewPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 56);
                    if (null != twPayPaymentAccount) {
                        return redirectTo("bank.id.12000", qrcodeParameter.getMchId(), urlToken, totalFee, customizedTemplate, false, "12000");
                    } else if (null != tobTwPayPaymentAccount) {
                        return redirectTo("bank.id.14200", qrcodeParameter.getMchId(), urlToken, totalFee, customizedTemplate, false, "14200");
                    } else if (null != fbTwPayPaymentAccount) {
                        return redirectTo("bank.id.14300", qrcodeParameter.getMchId(), urlToken, totalFee, customizedTemplate, false, "14300");
                    } else if (null != lbTwPayPaymentAccount) {
                        return redirectTo("bank.id.14400", qrcodeParameter.getMchId(), urlToken, totalFee, customizedTemplate, false, "14400");
                    } else if (null != tsbTwPayPaymentAccount) {
                        return redirectTo("bank.id.15000", qrcodeParameter.getMchId(), urlToken, totalFee, customizedTemplate, false, "15000");
                    } else if (null != megaTwPayNewPaymentAccount) {
                        return redirectTo("bank.id.15600", qrcodeParameter.getMchId(), urlToken, totalFee, customizedTemplate, false, "15600");
                    }
                }
                List<String> accountName = new ArrayList<String>();
                String accountId = merchant.getAccountId();
                String accountArray = redisService.getRedis(accountId);

                if (accountArray != null) {
                    accountName = Arrays.asList(accountArray.split(" - "));
                } else {
                    List<PaymentAccount> list = paymentAccountService.listByQrcode(urlToken);
                    LOGGER.info("[Qrocde][Type5][Unknown agent]");
                    for (PaymentAccount aList : list) {
                        if (aList.getSeqBy() != null) {
                            accountName.add(aList.getBank().getName() + "," + aList.getSeqBy());
                        } else {
                            accountName.add(aList.getBank().getName() + ",99");
                        }
                    }
                    try {
                        String accountConvert = String.join(" - ", accountName);
                        redisService.setRedis(accountId, accountConvert, REDIS_TIMEOUT);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                LOGGER.info("[payment account] " + accountName);

                ModelAndView model = new ModelAndView();
                model.setViewName("paymentMethod");
                model.addObject("token", urlToken);
                model.addObject("bankName", bank == null ? "" : bank.getName());
                model.addObject("domain", API_URL);
                model.addObject("accountName", accountName);
                model.addObject("applePayHost", propertyService.get("server.domain"));
                model.addObject("merchant", qrcodeParameter.getMchId());
                model.addObject("customizedTemplate", customizedTemplate);
                model.addObject("isCustom", "1");
                model.addObject("isMultiQRCode", isMultiQRCode);
                this.setModelViewDetail(model, merchant, qrcodeParameter, response);
                model.addObject("paymentBanner", StringUtils.isNotBlank(merchant.getPaymentBanner()) ? merchant.getPaymentBanner() : "");
                model.addObject("multiQrcodeList", multiQRCodeList != null ? new Gson().toJson(multiQRCodeList) : "");

                return model;
            } else {
                LOGGER.info("[Qrocde][Type1]");
                storeOrderNoNew = qrcodeParameter.getStoreOrderNo();
                totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, ""));
            }

            integrateMchId = qrcodeParameter.getMchId();
            tradeDetail = tradeDetailService.getOne(integrateMchId, storeOrderNoNew);

        } else {
            LOGGER.info("[Qroce][isNull]");

            ModelAndView model = new ModelAndView();
            model.setViewName("invalidQrcode");
            return model;
        }
//qrcode type:1
        if (tradeDetail != null) {
            boolean flag = tradeDetail.getStatus().equals("Waiting")
                    || tradeDetail.getStatus().equals(SystemInstance.TRADE_SUCCESS)
                    || tradeDetail.getStatus().equals(SystemInstance.TRADE_FAIL);

            if (tradeDetail.getMethod().startsWith("2")) {
                ModelAndView modelAndView = new ModelAndView();
                modelAndView.setViewName("qrCodeExpired");
                if (tradeDetail.getStatus().equals(SystemInstance.TRADE_SUCCESS)) {
                    modelAndView.addObject("status", "success");
                } else {
                    modelAndView.addObject("status", "fail");
                }
                return modelAndView;

            }

            if (flag) {
                return new ModelAndView("redirect:" + tradeDetail.getOnlinePayUrl());
            }
        }

        // add last 4 code
        String body = qrcodeService.getRandomBody(qrcodeParameter);
        qrcodeParameter.setBody(body);

        try {
            if (agent.contains(env.getProperty("weixinAgent"))) {
                LOGGER.info("[Qrocde][Type1][weixinAgent]");

                long bankId = Long.valueOf(env.getProperty("bank.id.10110"));
                PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);
                if (paymentAccount == null) {
                    return noServiceReturn(urlToken);
                }
                totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "10110"));
                platformURL = weixinOnline(qrcodeParameter, storeOrderNoNew, totalFee, integrateMchId, urlToken);
            } else if (agent.contains(env.getProperty("aliAgent"))
                    && paymentAccountService.getOne(qrcodeParameter.getMchId(), 2) != null) {

                long bankId = Long.valueOf(env.getProperty("bank.id.10220"));
                PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);
                if (paymentAccount == null) {
                    return noServiceReturn(urlToken);
                }
                totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "10220"));
                platformURL = aliOnline(qrcodeParameter, storeOrderNoNew, totalFee, integrateMchId, urlToken);

            } else if (agent.contains(env.getProperty("aliAgent"))
                    && paymentAccountService.getOne(qrcodeParameter.getMchId(), 11) != null) {

                long bankId = Long.valueOf(env.getProperty("bank.id.11100"));
                PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);
                if (paymentAccount == null) {
                    return noServiceReturn(urlToken);
                }
                totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "11100"));
                platformURL = yuanTaOfflineService.createOrder(tradeDetail, qrcodeParameter, totalFee, storeOrderNoNew,
                        paymentAccount);
            } else if (agent.contains(env.getProperty("piAgent"))
                    && paymentAccountService.getOne(qrcodeParameter.getMchId(), 59) != null) {

                long bankId = Long.valueOf(env.getProperty("bank.id.15900"));
                PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);
                if (paymentAccount == null) {
                    return noServiceReturn(urlToken);
                }
                totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "15900"));
                platformURL = newPiService.getPaymentUrl(totalFee, qrcodeParameter.getShortId(), null);
            } else if (agent.contains(env.getProperty("aliAgent"))
                    && paymentAccountService.getOne(integrateMchId, 25) != null) {
                LOGGER.info("[REQUEST][ONLINE][ALISHANGHAI]");
                long bankId = Long.valueOf(env.getProperty("bank.id.12520"));
                PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);
                totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "12520"));
                platformURL = alipayShangHaiService.createOrder(tradeDetail, qrcodeParameter, Integer.valueOf(totalFee),
                        storeOrderNoNew, paymentAccount);
            } else if (agent.contains(env.getProperty("allpayAgent"))) {
                LOGGER.info("allpay redirect url ");

                long bankId = Long.valueOf(env.getProperty("bank.id.10500"));
                PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);

                if (paymentAccount == null) {
                    return noServiceReturn(urlToken);
                }
                totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "10500"));
                WeixinJsapiRequestData weixinJsapiRequestData = new WeixinJsapiRequestData();

                weixinJsapiRequestData.setOutTradeNo(storeOrderNoNew);
                weixinJsapiRequestData.setTotalFee(totalFee);
                weixinJsapiRequestData.setDeviceInfo(qrcodeParameter.getDeviceInfo());
                weixinJsapiRequestData.setBody(qrcodeParameter.getBody());

                tradeDetail = setOnlineTradeDetailRequest(weixinJsapiRequestData, integrateMchId, paymentAccount,
                        qrcodeParameter);

                ModelAndView model = new ModelAndView();
                model.setViewName("allpayCheckOut");
                model.addObject("randomId", tradeDetail.getTradeDetailRandomId().toString());
                model.addObject("orderNo", storeOrderNoNew);
                model.addObject("totalFee", (int) OnlinepayUtil.getTradeFee(qrcodeParameter, "10500"));
                // paymentAccountService.getOne(qrcodeParameter.getMchId(),Long.valueOf(env.getProperty("bank.id.10500"))).getAccount();
                model.addObject("merchant", paymentAccount.getAccount());
                model.addObject("merchantName", merchant.getName());
                model.addObject("domain", API_URL);
                model.addObject("allpayDomain", env.getProperty("host.request.allpay"));

                return model;
            } else if (agent.contains(env.getProperty("lineAgent"))) {
                LOGGER.info("line redirect url ");
                long bankId = Long.valueOf(env.getProperty("bank.id.11500"));
                PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);
                if (paymentAccount == null) {
                    return noServiceReturn(urlToken);
                }
                totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "11500"));
                platformURL = getLineUrl(qrcodeParameter, storeOrderNoNew, totalFee, integrateMchId, urlToken,
                        paymentAccount);

            } else if (agent.contains(env.getProperty("twmpAgent"))) {

                LOGGER.info("twpay redirect url ");
                PaymentAccount tobPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), co.intella.constant.Bank.TOBTWPAY.getId());
                PaymentAccount fbPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), co.intella.constant.Bank.FBTWPAY.getId());
                PaymentAccount twPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), co.intella.constant.Bank.TWPAY.getId());
                PaymentAccount lbPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), co.intella.constant.Bank.LBTWPAY.getId());
                PaymentAccount tcbPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), co.intella.constant.Bank.TCBTWPAY.getId());
                PaymentAccount megaPaymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), co.intella.constant.Bank.MEGATWPAY2.getId());

                PaymentAccount paymentAccount = null;
                if (null != tobPaymentAccount) {
                    paymentAccount = tobPaymentAccount;
                } else if (null != fbPaymentAccount) {
                    paymentAccount = fbPaymentAccount;
                } else if (null != twPaymentAccount) {
                    paymentAccount = twPaymentAccount;
                } else if (null != lbPaymentAccount) {
                    paymentAccount = lbPaymentAccount;
                } else if (null != tcbPaymentAccount) {
                    paymentAccount = tcbPaymentAccount;
                } else if (null != megaPaymentAccount) {
                    paymentAccount = megaPaymentAccount;
                }
                if (paymentAccount == null) {
                    return noServiceReturn(urlToken);
                }

                totalFee = String.valueOf((int) OnlinepayUtil.getTradeFee(qrcodeParameter, "1" + paymentAccount.getBank().getBankId() + "00"));
                String twResponse = twPayService.getPaymentUrl(totalFee, urlToken, agent);
                TwPayResponse twPayResponse = new Gson().fromJson(twResponse, TwPayResponse.class);

                platformURL = twPayResponse.getUriSchema();

            } else {
                // return new ModelAndView("paymentCheckOut");
                List<String> accountName = new ArrayList<String>();
                String accountId = merchant.getAccountId();
                String accountArray = redisService.getRedis(accountId);

                if (accountArray != null) {
                    accountName = Arrays.asList(accountArray.split(" - "));
                } else {
                    List<PaymentAccount> list = paymentAccountService.listByQrcode(urlToken);
                    for (PaymentAccount aList : list) {
                        if (aList.getSeqBy() != null) {
                            accountName.add(aList.getBank().getName() + "," + aList.getSeqBy());
                        } else {
                            accountName.add(aList.getBank().getName() + ",99");
                        }

                    }
                    try {
                        String accountConvert = String.join(" - ", accountName);
                        redisService.setRedis(accountId, accountConvert, REDIS_TIMEOUT);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                LOGGER.info("[payment account] " + accountName);

                ModelAndView model = new ModelAndView();
                model.setViewName("paymentMethod");
                model.addObject("bankName", bank == null ? "" : bank.getName());
                model.addObject("token", urlToken);
                model.addObject("domain", API_URL);
                model.addObject("applePayHost", propertyService.get("server.domain"));
                model.addObject("accountName", accountName);
                model.addObject("merchant", qrcodeParameter.getMchId());
                model.addObject("isCustom", "0"); // for codeType:5
                model.addObject("isMultiQRCode", isMultiQRCode);
                this.setModelViewDetail(model, merchant, qrcodeParameter, response);
                model.addObject("paymentBanner", StringUtils.isNotBlank(merchant.getPaymentBanner()) ? merchant.getPaymentBanner() : "");
                model.addObject("multiQrcodeList", multiQRCodeList != null ? new Gson().toJson(multiQRCodeList) : "");

                return model;
            }

            return new ModelAndView("redirect:" + platformURL);
        } catch (OrderAlreadyExistedException e) {
            LOGGER.info("[Online][TradeExist]");
            ModelAndView model = new ModelAndView();
            model.setViewName("invalidQrcode");
            return model;

        }

    }

    private String getLineUrl(QrcodeParameter qrcodeParameter, String storeOrderNoNew, String totalFee,
                              String integrateMchId, String urlToken, PaymentAccount paymentAccount) throws Exception {

        TradeDetail tradeDetail;
        String result;

        WeixinJsapiRequestData weixinJsapiRequestData = new WeixinJsapiRequestData();
        weixinJsapiRequestData.setOutTradeNo(storeOrderNoNew);
        weixinJsapiRequestData.setTotalFee(totalFee);
        weixinJsapiRequestData.setDeviceInfo(qrcodeParameter.getDeviceInfo());
        weixinJsapiRequestData.setBody(qrcodeParameter.getBody());

        LineReserveRequestData lineReserveRequestData = lineService.prepareLineReserveRequestData(qrcodeParameter,
                Integer.valueOf(totalFee), storeOrderNoNew);

//        lineReserveRequestData.setProductName(qrcodeParameter.getBody());
//        lineReserveRequestData.setProductImageUrl(API_URL+"intella-allpaypass/images.png");
//        lineReserveRequestData.setAmount(Integer.valueOf(totalFee));
//        lineReserveRequestData.setCurrency("TWD");
//        lineReserveRequestData.setConfirmUrl(API_URL+"allpaypass/api/linepay/confirm");
//        lineReserveRequestData.setCancelUrl(API_URL+"allpaypass/api/linepay/cancel?orderId="+storeOrderNoNew);
//        lineReserveRequestData.setOrderId(storeOrderNoNew);
        try {
            tradeDetail = setOnlineTradeDetailRequest(weixinJsapiRequestData, integrateMchId, paymentAccount,
                    qrcodeParameter);
        } catch (OrderAlreadyExistedException e) {
            throw new OrderAlreadyExistedException("TradeExist");
        } catch (Exception e) {
            e.printStackTrace();
            tradeDetail = null;
            LOGGER.info("[line][set trade exception]" + e.getMessage());
        }

        try {
            result = lineService.reserve(lineReserveRequestData, paymentAccount);
        } catch (Exception e) {
            e.printStackTrace();
            result = null;
            LOGGER.info("[line][reserve exception]" + e.getMessage());

        }
        LOGGER.info("[line][reserve result]" + result);

        LineResponseData lineResponseData = new Gson().fromJson(result, LineResponseData.class);

        if (lineResponseData.getReturnCode().equals("0000")) {
            return lineResponseData.getInfo().getPaymentUrl().getWeb();
        } else {
            return null;
        }

    }

    @RequestMapping(method = RequestMethod.GET, value = "/interruptTrade/{shortId}")
    public ResponseEntity<String> interruptTrade(@PathVariable String shortId) throws Exception {
//        JsonObject json = new Gson().fromJson(body, JsonObject.class);

        LOGGER.info("[online][interrupt]shoortId:" + shortId);
        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(shortId);
        if (qrcodeParameter == null) {
            LOGGER.info("[online][interrupt]Qrcode error");
            return new ResponseEntity<String>(getResponseJson("Qrcode error"), HttpStatus.OK);
        }

        if (!qrcodeParameter.getCodeType().equals("1")) {
            LOGGER.info("[online][interrupt]trade is permanent");
            return new ResponseEntity<String>(getResponseJson("Trade interrupted"), HttpStatus.OK);
        }

        TradeDetail tradeDetail = tradeDetailService.getOneByShortId(shortId);

        if (tradeDetail == null) {
            LOGGER.info("[online][interrupt]trade is null");
            return new ResponseEntity<String>(getResponseJson("order not found"), HttpStatus.OK);
        }
        LOGGER.info("[online][interrupt]trade detail:" + tradeDetail.getStatus());

        if (tradeDetail.getStatus().equals("Waiting") || tradeDetail.getStatus().equals("Creating order")) {
            tradeDetail.setStatus("Interrupted");
            tradeDetailService.save(tradeDetail);
            return new ResponseEntity<String>(getResponseJson("Trade interrupted"), HttpStatus.OK);
        } else if (tradeDetail.getStatus().equals("Trade success")) {
            return new ResponseEntity<String>(getResponseJson("Trade success"), HttpStatus.OK);
        } else {
            return new ResponseEntity<String>(getResponseJson("Trade fail"), HttpStatus.OK);
        }
    }

    private String getResponseJson(String message) {
        Map<String, String> response = new HashMap<String, String>();
        response.put("status", message);
        return new Gson().toJson(response);
    }

    private ModelAndView redirectToWebagent(String totalFee, String urlToken, String customizedTemplate,
                                            boolean isCustomizedPage, String method) {

        //有agent  這邊算折扣金額，排除已經算好折扣的qrCode(指定支付)
        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(urlToken);
        if (qrcodeParameter.getOnSaleTotalFee() == 0) {
            DiscountTrade discountTrade = discountService.checkoutTotalFree(qrcodeParameter.getMchId(),
                    Double.valueOf(qrcodeParameter.getTotalFee()).longValue(), method);
            if (Objects.nonNull(discountTrade)) {
                DiscountTradeOLPay discountTradeOLPay = new DiscountTradeOLPay();
                BeanUtils.copyProperties(discountTrade, discountTradeOLPay, "discountTradeSeq");
                discountTradeOLPay.setShortId(qrcodeParameter.getShortId());
                discountTradeOLPayService.save(discountTradeOLPay);
                totalFee = String.valueOf(discountTrade.getTradeAmount());
            }
        }

        if (isCustomizedPage) {
            LOGGER.info("[Qrocde][Type5][Known agent]");
            ModelAndView model = new ModelAndView();
            model.setViewName("customized");
            model.addObject("amount", totalFee);
            model.addObject("token", urlToken);
            model.addObject("domain", API_URL);
            model.addObject("method", method);
            model.addObject("merchantName", customizedTemplate);
            model.addObject("customizedTemplate", customizedTemplate);
            return model;
        } else if (!Strings.isNullOrEmpty(customizedTemplate)) {
            LOGGER.info("[Qrocde][Type6][Known agent]");
            ModelAndView model = new ModelAndView();
            model.setViewName("webagent");
            model.addObject("amount", totalFee);
            model.addObject("token", urlToken);
            model.addObject("domain", API_URL);
            model.addObject("method", method);
            model.addObject("merchantName", customizedTemplate);
            model.addObject("customizedTemplate", customizedTemplate);
            model.addObject("isCustom", "1");
            return model;
        } else {
            LOGGER.info("[Qrocde][Type3][Known agent]");
            ModelAndView model = new ModelAndView();
            model.setViewName("webagent");
            model.addObject("amount", totalFee);
            model.addObject("token", urlToken);
            model.addObject("domain", API_URL);
            model.addObject("method", method);
            return model;
        }
    }

    private ModelAndView noServiceReturn(String urlToken) {
        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(urlToken);
        Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());

        String isMultiQRCode = "0";
        List<MultiQrcode> multiQRCodeList = null;

        List<String> accountName = new ArrayList<String>();
        String accountId = merchant.getAccountId();
        String accountArray = redisService.getRedis(accountId);

        if (accountArray != null) {
            accountName = Arrays.asList(accountArray.split(" - "));

        } else {
            LOGGER.info("[Qrocde][Type1][No service]");
            List<PaymentAccount> list = paymentAccountService.listByQrcode(urlToken);
            LOGGER.info("[Qrocde][Type1][list service]");
            for (PaymentAccount aList : list) {
                if (aList.getSeqBy() != null) {
                    accountName.add(aList.getBank().getName() + "," + aList.getSeqBy());
                } else {
                    accountName.add(aList.getBank().getName() + ",99");
                }
            }
            try {
                String accountConvert = String.join(" - ", accountName);
                redisService.setRedis(accountId, accountConvert, REDIS_TIMEOUT);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (null != merchant.getMgtPermission() && merchant.getMgtPermission().contains(MULTI_ORDER_PERMISSION)) {
            try {
                multiQRCodeList = multiQrcodeService.findMutiQRCode(qrcodeParameter.getShortId());
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (multiQRCodeList != null && !multiQRCodeList.isEmpty()) {
                isMultiQRCode = "1";
            }
        }

        LOGGER.info("[payment account] " + accountName);
        ModelAndView model = new ModelAndView();
        model.setViewName("paymentMethod");
        model.addObject("token", urlToken);
        model.addObject("domain", API_URL);
        model.addObject("accountName", accountName);
        model.addObject("isMultiQRCode", isMultiQRCode);
        //qrcode codeType = 3不帶金額
        this.setModelViewDetail(model, merchant, qrcodeParameter, null);
        model.addObject("paymentBanner", StringUtils.isNotBlank(merchant.getPaymentBanner()) ? merchant.getPaymentBanner() : "");
        model.addObject("multiQrcodeList", multiQRCodeList != null ? new Gson().toJson(multiQRCodeList) : "");

        return model;

    }

    private String weixinOnline(QrcodeParameter qrcodeParameter, String storeOrderNoNew, String totalFee,
                                String integrateMchId, String urlToken) throws Exception {
        long bankId = Long.valueOf(env.getProperty("bank.id.10110"));
        PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);

        WeixinJsapiRequestData weixinJsapiRequestData = new WeixinJsapiRequestData();

        weixinJsapiRequestData.setVer(qrcodeParameter.getVer());
        weixinJsapiRequestData.setMchId(paymentAccount.getAccount());
        weixinJsapiRequestData.setMethod("10");
        weixinJsapiRequestData.setNonceStr(null);

        weixinJsapiRequestData.setTimeExpire(qrcodeParameter.getTimeExpire());
        weixinJsapiRequestData.setTradeType("JSAPINORETURN");
        weixinJsapiRequestData.setOutTradeNo(storeOrderNoNew);
        weixinJsapiRequestData.setBody(qrcodeParameter.getBody());
        weixinJsapiRequestData.setFeeType(qrcodeParameter.getFeeType());
        weixinJsapiRequestData.setTotalFee(totalFee);
        weixinJsapiRequestData.setDetail(qrcodeParameter.getDetail());
        weixinJsapiRequestData.setDeviceInfo(qrcodeParameter.getDeviceInfo());

        LOGGER.info(" Weixin online get request data " + new Gson().toJson(weixinJsapiRequestData));

        TradeDetail tradeDetail = setOnlineTradeDetailRequest(weixinJsapiRequestData, integrateMchId, paymentAccount,
                qrcodeParameter);
        LOGGER.info("setOnlineTradeDetailRequest fin ");

        String platformURL = weixinService.callJsApiNoReturn(weixinJsapiRequestData);
        LOGGER.info("weixin order url :" + platformURL);

        if (!platformURL.startsWith("http")) {
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
        } else {
            tradeDetail.setStatus("Waiting");
            tradeDetail.setOnlinePayUrl(platformURL);

            jobService.addJob(storeOrderNoNew, qrcodeParameter.getMchId(), paymentAccount.getAccount(), 1);
        }

        tradeDetailService.save(tradeDetail);

        return platformURL;
    }

    private String aliOnline(QrcodeParameter qrcodeParameter, String storeOrderNoNew, String amount,
                             String integrateMchId, String urlToken) throws Exception {
        LOGGER.info("create ali online url");
        long bankId = Long.valueOf(env.getProperty("bank.id.10220"));
        PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), bankId);

        WeixinJsapiRequestData weixinJsapiRequestData = new WeixinJsapiRequestData();

        weixinJsapiRequestData.setVer(qrcodeParameter.getVer());
        weixinJsapiRequestData.setMchId(paymentAccount.getAccount());
        weixinJsapiRequestData.setMethod("12");
        weixinJsapiRequestData.setNonceStr(null);

        weixinJsapiRequestData.setTimeExpire(qrcodeParameter.getTimeExpire());
        weixinJsapiRequestData.setTradeType("JSAPI");
        weixinJsapiRequestData.setOutTradeNo(storeOrderNoNew);
        weixinJsapiRequestData.setBody(qrcodeParameter.getBody());
        weixinJsapiRequestData.setFeeType(qrcodeParameter.getFeeType());
        weixinJsapiRequestData.setTotalFee(amount);
        weixinJsapiRequestData.setDetail(qrcodeParameter.getDetail());
        weixinJsapiRequestData.setDeviceInfo(qrcodeParameter.getDeviceInfo());

        LOGGER.info("request data " + new Gson().toJson(weixinJsapiRequestData));
        TradeDetail tradeDetail = setOnlineTradeDetailRequest(weixinJsapiRequestData, integrateMchId, paymentAccount,
                qrcodeParameter);

        LOGGER.info("trade detail saved");
        String platformURL = aliService.callJsapi(weixinJsapiRequestData, AliRequestBody.REFUND);
        LOGGER.info("platform url" + platformURL);

        if (!platformURL.startsWith("http")) {
            LOGGER.info("request URL failed ");
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
        } else {
            LOGGER.info("request URL success ");
            tradeDetail.setStatus("Waiting");
            tradeDetail.setOnlinePayUrl(platformURL);
        }
        tradeDetailService.save(tradeDetail);

        LOGGER.info("ali order url :" + platformURL);

        return platformURL;
    }

    private TradeDetail setOnlineTradeDetailRequest(WeixinJsapiRequestData weixinJsapiRequestData,
                                                    String integrateMchId, PaymentAccount paymentAccount, QrcodeParameter qrcodeParameter) throws Exception {
        LOGGER.info("enter trade detail request:" + new Gson().toJson(weixinJsapiRequestData));

        Merchant merchant = merchantService.getOne(integrateMchId);

        TradeDetail tradeDetail = tradeDetailService.getOne(integrateMchId, weixinJsapiRequestData.getOutTradeNo());
        if (tradeDetail != null) {
            throw new OrderAlreadyExistedException("Trade exist");
        } else {
            tradeDetail = new TradeDetail();
        }

        tradeDetail.setAccountId(integrateMchId);
        if (paymentAccount.getBank().getBankId() == 1) {
            tradeDetail.setMethod("10110");
        } else if (paymentAccount.getBank().getBankId() == 2) {
            tradeDetail.setMethod("10220");
        } else if (paymentAccount.getBank().getBankId() == 3) {
            tradeDetail.setMethod("10300");
        } else if (paymentAccount.getBank().getBankId() == 5) {
            tradeDetail.setMethod("10500");
        } else if (paymentAccount.getBank().getBankId() == 10) {
            tradeDetail.setMethod("21100");
        } else if (paymentAccount.getBank().getBankId() == 15) {
            tradeDetail.setMethod("11500");
        }

        tradeDetail.setPaymentAccount(paymentAccount);
        tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        tradeDetail.setStatus("Creating order");
        tradeDetail.setOrderId(weixinJsapiRequestData.getOutTradeNo());
        tradeDetail.setDeviceRandomId(deviceService.getOne(merchant, weixinJsapiRequestData.getDeviceInfo()));
        tradeDetail.setPayment(Long.parseLong(weixinJsapiRequestData.getTotalFee()));
        tradeDetail.setServiceType("OLPay");
        tradeDetail.setQrcodeToken(qrcodeParameter.getShortId());
        tradeDetail.setStoreInfo(weixinJsapiRequestData.getStoreId());
        tradeDetail.setDescription(weixinJsapiRequestData.getBody());
        tradeDetail.setDetail(qrcodeParameter.getDetail());
        if (qrcodeParameter.getOnSaleTotalFee() == Long.parseLong(weixinJsapiRequestData.getTotalFee())) {
            tradeDetail.setOnSale(true);
        } else {
            tradeDetail.setOnSale(false);
        }
        tradeDetail.setOriginalPrice(qrcodeParameter.getTotalFee());

        LOGGER.info("save online trade detail : " + new ObjectMapper().writeValueAsString(tradeDetail));

        tradeDetail = tradeDetailService.save(tradeDetail);
//        tradeDetail = tradeDetailService.getOne(tradeDetail.getOrderId());

        LOGGER.info("save captureOrder");
        tradeProcedureService.captureOrderSave(tradeDetail);
        LOGGER.info("save captureOrder fin");

        return tradeDetail;
    }

    private ModelAndView redirectTo(String bankCode, String mchId, String urlToken, String totalFee,
                                    String customizedTemplate, boolean isCustomizedPage, String method) {

        long bankId = Long.valueOf(env.getProperty(bankCode));
        PaymentAccount paymentAccount = paymentAccountService.getOne(mchId, bankId);
        if (paymentAccount == null) {
            return noServiceReturn(urlToken);
        }
        return redirectToWebagent(totalFee, urlToken, customizedTemplate, isCustomizedPage, method);
    }

    private void setModelViewDetail(ModelAndView model, Merchant merchant, QrcodeParameter qrcodeParameter, HttpServletResponse response) {
        ImotoToken ntuImotoToken = imotoTokenService.getOneForEffectiveByShortId(qrcodeParameter.getShortId());
        String defaultDetail = "";
        if (isNull(ntuImotoToken)) {
            defaultDetail = "<b>" + merchant.getName() + "</b><br>" + qrcodeParameter.getBody();
            if (!"3".equals(qrcodeParameter.getCodeType())) {
                defaultDetail += "<br>" + messageSource.getMessage("payment.method.detail.amount", null, locale) + "NT$" + qrcodeParameter.getTotalFee();
            }
            if (qrcodeParameter.getOnSaleTotalFee() > 0) {
                String discountAmount = new BigDecimal(qrcodeParameter.getOnSaleTotalFee()).subtract(new BigDecimal(qrcodeParameter.getTotalFee())).toString();
                defaultDetail += "<br>" + messageSource.getMessage("payment.method.detail.discountAmount", null, locale) + "NT$" + discountAmount;
            }
        } else {
            ImotoTokenLog imotoTokenLog = imotoTokenLogService.getLastLogByOutercodeAndTokenSeq(ntuImotoToken.getTokenOutercode(), ntuImotoToken.getImotoTokenSeq());
            String reg = "^09\\d{8}$";

            if (nonNull(imotoTokenLog) && StringUtils.defaultIfBlank(imotoTokenLog.getPhoneNum(), "").matches(reg) && imotoTokenLog.getAmount() > 0) {
                final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                defaultDetail = "<b>" + merchant.getName() + "</b>"
                        + "<br>" + messageSource.getMessage("payment.method.detail.token", null, locale) + imotoTokenLog.getTokenOutercode()
                        + "<br>" + messageSource.getMessage("payment.method.detail.enterTime", null, locale) + sdf.format(imotoTokenLog.getEnterTime())
                        + "<br>" + messageSource.getMessage("payment.method.detail.exitTime", null, locale) + sdf.format(imotoTokenLog.getExitTime())
                        + "<br>" + messageSource.getMessage("payment.method.detail.lastExitTime", null, locale) + sdf.format(imotoTokenLog.getLastExitTime())
                        + "<br>" + messageSource.getMessage("payment.method.detail.amount", null, locale) + "NT$" + imotoTokenLog.getAmount()
                        + "<br>" + messageSource.getMessage("payment.method.detail.promoCodeValid", null, locale) + ("Y".equals(imotoTokenLog.getPromoCodeValid()) ?
                        messageSource.getMessage("payment.method.detail.Y", null, locale) : messageSource.getMessage("payment.method.detail.N", null, locale));
            } else if (nonNull(response)) {
                try {
                    response.sendRedirect(String.format("%s%s.imoto", API_URL, qrcodeParameter.getShortId()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if ("7".equals(qrcodeParameter.getCodeType())) {
            QrType7 qrType7 = qrType7RepositoryImpl.searchTheNewest(qrcodeParameter.getShortId());
            if (qrType7 != null) {
                defaultDetail = "<b>" + merchant.getName() + "</b>"
                        + "<br>" + messageSource.getMessage("co.intella.qrType7.car", null, locale) + qrType7.getLicensePlate()
                        + "<br>" + messageSource.getMessage("co.intella.qrType7.mobile", null, locale) + qrType7.getMobile()
                        + "<br>" + messageSource.getMessage("co.intella.qrType7.note", null, locale) + qrType7.getRemark()
                        + "<br>" + messageSource.getMessage("co.intella.qrType7.price", null, locale) + qrType7.getAmount();

            }
        } else if ("8".equals(qrcodeParameter.getCodeType())) {
            QrType8 qrType8 = qrType8RepositoryImpl.searchTheNewest(qrcodeParameter.getShortId());
            if (qrType8 != null) {
                defaultDetail = "<b>" + merchant.getName() + "</b>"
                        + "<br>" + messageSource.getMessage("co.intella.qrType8.name", null, locale) + qrType8.getName()
                        + "<br>" + messageSource.getMessage("co.intella.qrType8.mobile", null, locale) + qrType8.getMobile()
                        + "<br>" + messageSource.getMessage("co.intella.qrType8.price", null, locale) + qrType8.getAmount()
                        + "<br>" + messageSource.getMessage("co.intella.qrType8.note", null, locale) + qrType8.getRemark();

            }
        }
        model.addObject("detail", defaultDetail);
    }

    /**
     * 設定 .imoto 頁面 所需 參數
     *
     * @param qrcodeParameter
     * @param modelAndView
     * @return
     */
    private ModelAndView showImoto(QrcodeParameter qrcodeParameter, ModelAndView modelAndView, String shortId) {
        modelAndView.setViewName("ntuImoto");
        if (nonNull(qrcodeParameter)) {
            ImotoToken ntuImotoToken = imotoTokenService.getOneForEffectiveByShortId(qrcodeParameter.getShortId());
            if (nonNull(ntuImotoToken)) {
                Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());
                ImotoTokenLog imotoTokenLog = imotoTokenLogService.getLastLogByOutercodeAndTokenSeq(ntuImotoToken.getTokenOutercode(), ntuImotoToken.getImotoTokenSeq());
                String phoneErrStr = "";
                if (nonNull(imotoTokenLog)) {
                    String reg = "^09\\d{8}$";
                    String phoneNum = imotoTokenLog.getPhoneNum();
                    if (StringUtils.isBlank(phoneNum)) {
                        phoneErrStr = messageSource.getMessage("co.intella.imoto.phoneNum.err1", null, locale);
                    } else if (!phoneNum.matches(reg)) {
                        phoneErrStr = messageSource.getMessage("co.intella.imoto.phoneNum.err2", null, locale);
                    } else if (imotoTokenLog.getAmount() <= 0) {
                        phoneErrStr = messageSource.getMessage("co.intella.imoto.amount.err1", null, locale);
                    }
                }
                modelAndView.addObject("parameterBody", qrcodeParameter.getBody());
                modelAndView.addObject("phoneErrStr", phoneErrStr);
                modelAndView.addObject("mchName", merchant.getCompanyName());
                modelAndView.addObject("outCode", ntuImotoToken.getTokenOutercode());
                modelAndView.addObject("shortId", ntuImotoToken.getShortId());
            } else {
                //若 掃描世曦優惠碼進來，又剛好跟shortid一樣，但沒token對照檔，當成優惠碼
                modelAndView.addObject("promoCode", shortId);
            }
        } else {
            // 掃描世曦優惠碼進來的…
            modelAndView.addObject("promoCode", shortId);
        }

        return modelAndView;
    }


    private ModelAndView showInfo(QrcodeParameter qrcodeParameter, ModelAndView modelAndView, String shortId) {
        modelAndView.setViewName("demoInfo");
        modelAndView.addObject("shortId", shortId);
        modelAndView.addObject("originBody", qrcodeParameter.getBody());
        return modelAndView;
    }

    /**
     * web post   起始入口
     *
     * @param agent
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(method = RequestMethod.GET, value = "/webPostLogin")
    public ModelAndView webPostLogin(
            @org.springframework.web.bind.annotation.RequestHeader(value = "User-Agent") String agent,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("webPostLogin");
        modelAndView.addObject("hostDomain", WEB_POST_HOST_DOMAIN);
        return modelAndView;
    }

    /**
     * 判別是否已登入過或seesion是否還在，決定導去哪裡
     *
     * @param agent
     * @param urlToken
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(method = RequestMethod.GET, value = "/webPostLogin/{urlToken}")
    public ModelAndView webPostLoginGET(
            @org.springframework.web.bind.annotation.RequestHeader(value = "User-Agent") String agent,
            @PathVariable String urlToken, HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOGGER.info("webPostLogin =>" + urlToken);
        HttpSession sessionNew = request.getSession();
        ServletContext context = sessionNew.getServletContext();
        HttpSession sessionOld = (HttpSession) context.getAttribute(urlToken);

        ModelAndView modelAndView = new ModelAndView();
        if (Objects.nonNull(sessionOld)) {
            // 有登入過 持續更新session
            String mchId = (String) sessionOld.getAttribute("mchId");
            sessionNew.setAttribute("mchName", sessionOld.getAttribute("mchName"));
            sessionNew.setAttribute("mchId", mchId);
            context.setAttribute(urlToken, sessionNew);
            EncryptType encryptType = edgeService.findEncryptType(mchId);
            modelAndView.setViewName("webPost");
            modelAndView.addObject("mchName", sessionNew.getAttribute("mchName"));
            modelAndView.addObject("mchId", mchId);

            modelAndView.addObject("edgeToken", encryptType.getToken());
            modelAndView.addObject("edgeKey", encryptType.getKey());
            modelAndView.addObject("edgeIv", encryptType.getIv());
            modelAndView.addObject("edgeType", encryptType.getEncryptType());

            modelAndView.addObject("edgeUrl", EDGE_URL);

        } else {
            //沒登入過 或已失效
            context.removeAttribute(urlToken);
            modelAndView.addObject("deleteToken", "1");
            modelAndView.setViewName("webPostLogin");
        }
        modelAndView.addObject("hostDomain", WEB_POST_HOST_DOMAIN);
        return modelAndView;
    }

    /**
     * 驗證 是否 登入資料有效
     *
     * @param webPostLogin
     * @param request
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping(value = "/webPost")
    public Map<String, String> webPost(@ModelAttribute WebPostLogin webPostLogin, HttpServletRequest request) throws Exception {
        LOGGER.info(webPostLogin.toString());
        Map<String, String> resultMap = new HashMap<>();
        String msgCode = "0000";
        String msg = "";
        Merchant merchant = merchantService.getOne(webPostLogin.getUserName());
//        LOGGER.info("merchant.getPin()  =>" + merchant.getPin());
        if (StringUtils.isBlank(webPostLogin.getUserName())
                || StringUtils.isBlank(webPostLogin.getPassword())
                || (Objects.nonNull(merchant) && !merchant.getPin().equalsIgnoreCase(this.getSHA256Hex(webPostLogin.getPassword())))) {
            msgCode = "9998";
            msg = messageSource.getMessage("api.merchant.checkPin.msg.0002", null, locale);
        }

        resultMap.put("msgCode", msgCode);
        resultMap.put("msg", msg);

        if ("0000".equals(msgCode)) {
            HttpSession session = request.getSession();
            ServletContext context = session.getServletContext();
            session.setAttribute("mchName", merchant.getCompanyName());
            session.setAttribute("mchId", merchant.getAccountId());
            context.setAttribute(session.getId(), session);
            resultMap.put("token", session.getId());
        }
        return resultMap;
    }

    /**
     * 登出
     *
     * @param agent
     * @param token
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(method = RequestMethod.GET, value = "/webPostLogout/{token}")
    public Map<String, String> webPostOut(
            @org.springframework.web.bind.annotation.RequestHeader(value = "User-Agent") String agent,
            @PathVariable String token, HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOGGER.info("webPostLogout =>" + token);
        HttpSession session = request.getSession();
        ServletContext context = session.getServletContext();
        context.removeAttribute(token);
        Map<String, String> resultMap = new HashMap<>();
        String msgCode = "0000";
        String msg = "";
        resultMap.put("msgCode", msgCode);
        resultMap.put("msg", msg);
        return resultMap;
    }

    //SHA256加密
    private String getSHA256Hex(String message) throws Exception {

        final MessageDigest digest = MessageDigest.getInstance("sha-256");
        byte[] messageHash = digest.digest(message.getBytes(StandardCharsets.UTF_8));
        String result = DatatypeConverter.printHexBinary(messageHash);
//        LOGGER.info("getSHA256Hex =>" + result);
        return result;
    }

    //    token=>accountId=>storeOrderNumber
    @ResponseBody
    @GetMapping("/token/{token}")
    public Map<String, String> getGetOrderIdByAccountId(@PathVariable("token") String token) {
        LOGGER.info("[REQUEST][token] " + token);
        String accountId = getAccountId(token);
        LOGGER.info("[REQUEST][accountId] " + accountId);
        Map<String, String> result = orderIdByAccountIdRepository.getGetOrderIdByAccountId(accountId);
        LOGGER.info("[RESPONSE] " + result);
        return result;
    }

    private String getAccountId(String token) {
        String accountId = "";
        try {
            EdgeToken edgeToken = edgeService.findToken(token);
            accountId = edgeToken == null ? "" : edgeToken.getAccountId();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return accountId;
    }


}
