package co.intella.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.intella.request.QrcodeGetImageRequest;
import co.intella.response.GeneralResponse;
import co.intella.service.qrcodeImage.QrcodeGetImageService;

@RestController
@RequestMapping("/api/qrcodeImge")
public class QrcodeImageController {
	
	@Autowired
	private QrcodeGetImageService qrcodeGetImageService;
	
	@PostMapping("/getImage")
	public GeneralResponse getImage(@RequestBody QrcodeGetImageRequest request) throws Exception {
		return qrcodeGetImageService.doService(request);
	}
}
