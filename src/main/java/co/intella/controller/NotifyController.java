package co.intella.controller;

import co.intella.domain.ali.AliReturnData;
import co.intella.domain.gama.GamaNotifyData;
import co.intella.domain.tcbank.TCBankNotifyRequest;
import co.intella.domain.twpay.*;
import co.intella.model.TradeDetail;
import co.intella.service.*;
import co.intella.utility.GeneralResponseMessageUtil;
import co.intella.utility.SystemInstance;
import co.intella.utility.TWPayUtil;
import co.intella.utility.XmlUtil;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

/**
 * This api used by third party to notify transaction result
 *
 * @author Miles
 */
@Controller
@RequestMapping("/api/notify")
public class NotifyController {

    private final Logger LOGGER = LoggerFactory.getLogger(NotifyController.class);

    @Autowired
    private MessageSource messageSource;

    private Locale locale = new Locale("zh_TW");

    @Resource
    private ECPayService ecPayService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private TwPayService twPayService;

    private TemplateEngine templateEngine;

    @Autowired
    public NotifyController(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    @Value("${fisc.parameter}")
    private String FISC_PARAMETER = "A69896AF6057EFAD836FB7EB3109B52AB896E6B9FEA7173E";

    @RequestMapping(method = RequestMethod.POST, value = "/gamapay")
    public ResponseEntity<String> notifyFromGamaPay(@RequestBody String request) {
        LOGGER.info("[REQUEST] /api/notify/gamapay ");
        Map<String, String> map = new HashMap<String, String>();
        GamaNotifyData gamaNotifyData = null;
        try {
            gamaNotifyData = convertStr2Object(request);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("Result", "NG");
            return new ResponseEntity<String>(new Gson().toJson(map), HttpStatus.OK);
        }

        TradeDetail tradeDetail = tradeDetailService.getOne(gamaNotifyData.getMerchantOrderId());
        if (tradeDetail == null) {
            map.put("Result", "NG");
            return new ResponseEntity<String>(new Gson().toJson(map), HttpStatus.OK);
        }

        tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);

        tradeDetailService.save(tradeDetail);


        map.put("Result", "OK");
        return new ResponseEntity<String>(new Gson().toJson(map), HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.POST, value = "/ecpay")
    public ResponseEntity<String> postECPay(@RequestBody String result) throws Exception {
        LOGGER.info("[REQUEST] /api/notify/ecpay " + result);

        // todo limit by ip from ecpay server

        Map<String, String> returnData = ecPayService.verifyNotification(result);
        LOGGER.info("[REQUEST] RtnCode : " + returnData.get("RtnCode") + ",accoundId : " + returnData.get("accountId") + ",orderId :" + returnData.get("orderId"));
        if ("1".equals(returnData.get("RtnCode"))) {
            // call push-notification api
            appNotificationService.notifyApp(returnData.get("accountId"), returnData.get("orderId"));
        } else {
            //todo what else should do?
        }
        return new ResponseEntity<String>(returnData.get("returnToECpay"), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/alipay")
    public ResponseEntity<String> postAliPay(@RequestBody AliReturnData request) {
        LOGGER.info("[REQUEST] /api/notify/alipay " + new Gson().toJson(request));

        TradeDetail tradeDetail = tradeDetailService.getOne(request.getOrderId());
        if (tradeDetail == null) {
            return null;
        }
        if (request.getStatus().equals("P")) {
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetailService.save(tradeDetail);
        } else {
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            tradeDetailService.save(tradeDetail);
        }

        // todo limit by ip from yuanta server
        appNotificationService.notifyApp(tradeDetail.getAccountId(), request.getOrderId());
        return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> postTcBank(@RequestParam String mchId, @RequestParam String orderId, TCBankNotifyRequest request) throws Exception {
        LOGGER.info("[REQUEST] /api/notify " + new Gson().toJson(request));

        TradeDetail tradeDetail = tradeDetailService.getOne(mchId, orderId);

        tradeDetail.setStatus(getTradeStatus(request.getResponseCode()));
        tradeDetail.setDescription(getTradeDescription(request.getResponseCode()));
        LOGGER.info("[RESPONSE] " + getTradeStatus(request.getResponseCode()) + ", " + getTradeDescription(request.getResponseCode()));

        tradeDetailService.save(tradeDetail);
        return new ResponseEntity<String>(buildContent(request), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<String> getTcBank() throws Exception {
        return new ResponseEntity<String>(SystemInstance.TRADE_SUCCESS, HttpStatus.OK);
    }

    private String getTradeStatus(String code) {
        if ("00".equals(code) || "08".equals(code) || "11".equals(code)) {
            return SystemInstance.TRADE_SUCCESS;
        } else if ("01".equals(code) || "02".equals(code) || "P1".equals(code) || "P4".equals(code) || "P6".equals(code) || "P9".equals(code) || "T4".equals(code)) {
            return SystemInstance.TRADE_FAIL;
        } else if ("03".equals(code) || "05".equals(code) || "06".equals(code) || "09".equals(code) || "12".equals(code)
                || "13".equals(code) || "14".equals(code) || "15".equals(code) || "30".equals(code) || "31".equals(code)
                || "39".equals(code) || "51".equals(code) || "55".equals(code) || "56".equals(code) || "57".equals(code)
                || "58".equals(code) || "61".equals(code) || "62".equals(code) || "63".equals(code)) {
            return SystemInstance.TRADE_FAIL;
        }

        return SystemInstance.TRADE_FAIL;
    }

    private String getTradeDescription(String code) {
        if ("00".equals(code) || "08".equals(code) || "11".equals(code)) {
            return SystemInstance.TRADE_SUCCESS;
        } else if ("01".equals(code) || "02".equals(code) || "P1".equals(code) || "P4".equals(code) || "P6".equals(code) || "P9".equals(code) || "T4".equals(code)) {
            return "Call Bank";
        } else if ("03".equals(code) || "05".equals(code) || "06".equals(code) || "09".equals(code) || "12".equals(code)
                || "13".equals(code) || "14".equals(code) || "15".equals(code) || "30".equals(code) || "31".equals(code)
                || "39".equals(code) || "51".equals(code) || "55".equals(code) || "56".equals(code) || "57".equals(code)
                || "58".equals(code) || "61".equals(code) || "62".equals(code) || "63".equals(code)) {
            return "Decline";
        }

        return "Decline";
    }

    public String buildContent(TCBankNotifyRequest request) {
        Context context = new Context();
        context.setVariable("result", getTradeMessage(request.getResponseCode()));
        context.setVariable("description", request.getResponseCode());
        return templateEngine.process("tcb-notify", context);
    }

    private String getTradeMessage(String code) {
        return messageSource.getMessage("notification.tcb.3D.result.success", null, locale);
    }

    private GamaNotifyData convertStr2Object(String result) throws Exception {
        String[] split = result.split("&");
        Map<String, String> map = new HashMap<String, String>();
        for (String element : split) {
            String[] temp = element.split("=");
            if (temp.length == 1)
                map.put(temp[0], "");
            else
                map.put(temp[0], temp[1]);
        }
        JsonElement jsonElement = new Gson().toJsonTree(map);
        return new Gson().fromJson(URLDecoder.decode(jsonElement.toString(), "UTF-8"), GamaNotifyData.class);

    }

    @RequestMapping(method = RequestMethod.POST, value = "/twpay", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> notifyTWPayTransaction(@RequestBody TwPayResponse request) {

        LOGGER.info("[/notify/twpay][request]" + new Gson().toJson(request));
        TradeDetail tradeDetail = tradeDetailService.getOneBySystemOrderId(request.getSrrn());
        if (Objects.nonNull(tradeDetail)) {
//        	tradeDetail.setMethod("12000");
            if ("0000".equals(request.getRespCode())) {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
//                    tradeDetail.setTxParams(JsonUtil.toJsonStr(request));
                    tradeDetailService.save(tradeDetail);
                    appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                }
            } else {
                LOGGER.info(String.format("[REQUEST] orderid %s %s respCode %s", request.getSrrn(), SystemInstance.TRADE_FAIL, request.getRespCode()));
                tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                tradeDetailService.save(tradeDetail);
            }
        }

        try {
            request.setTxnStatus("1"); // 交易回應
            request.setVerifyCode(null);
            request.setVerifyCode(TWPayUtil.getTwmpVerifyCode(request));
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("[/notify/twpay][setVerifyCode error]" + e.toString());
            return new ResponseEntity<String>("set verifyCode fail", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String requestJson = new Gson().toJson(request);
        LOGGER.info("[RESPONSE] /api/notify/twpay " + requestJson);
        return new ResponseEntity<String>(requestJson, HttpStatus.OK);
    }

    /**
     * 財金  金融卡回 調通知
     *
     * @param qrpNotifyReq
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/fisc/twpay", produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<String> notifyFiscTWPayTransaction(@RequestBody String body) {

        LOGGER.info("[REQUEST] /api/notify/fisc/twpay: " + body);
        QrpNotifyReq qrpNotifyReq = XmlUtil.unmarshall(body, QrpNotifyReq.class);
        TradeDetail tradeDetail = tradeDetailService.getOneBySystemOrderId(qrpNotifyReq.getOrderNumber());
        QrpNotifyResp qrpNotifyResp = twPayService.getQrpNotifyResp(qrpNotifyReq);
        if (Objects.nonNull(tradeDetail) && StringUtils.isBlank(tradeDetail.getTxParams())) {
            // 目前以收主掃交易通知為主; 退貨、取消當下交易時就會會知道
            String key = StringUtils.defaultIfBlank(tradeDetail.getPaymentAccount().getPlatformCheckCode(), this.FISC_PARAMETER);
            LOGGER.info("[REQUEST][isFiscVerifyCodeOk]: " + TWPayUtil.isFiscVerifyCodeOk(qrpNotifyReq, qrpNotifyReq.getVerifyCode(), key));

            twPayService.updateTradeDetailByQrpNotifyReq(qrpNotifyReq, tradeDetail, "7");
            LOGGER.info("[RESPONSE] /api/notify/fisc/twpay: " + XmlUtil.marshall(qrpNotifyResp));
        }

        return new ResponseEntity<String>(XmlUtil.marshall(qrpNotifyResp), HttpStatus.OK);
    }

    /**
     * 財金  信用卡回 調通知
     *
     * @param qrpNotifyReq
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.POST, value = "/fisc/twpay/webToAppNotify", produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<String> notifyFiscTWPayTransaction(@RequestBody String body, HttpServletResponse response) throws IOException {

        LOGGER.info("[REQUEST] /fisc/twpay/webToAppNotify: " + body);
        QrpEmvqrNotifyReq qrpNotifyReq = XmlUtil.unmarshall(body, QrpEmvqrNotifyReq.class);
        Gson gson = new Gson();
        Type typeOfMap = new TypeToken<Map<String, String>>() {
        }.getType();
        Map<String, String> otherInfo = gson.fromJson(qrpNotifyReq.getOtherInfo(), typeOfMap);
        String systemOrderId = otherInfo.get("tag20").trim().replaceAll("\\s","").replaceAll("\r?\n","");
        TradeDetail tradeDetail = tradeDetailService.getOneBySystemOrderId(systemOrderId.contains(",") ? systemOrderId.split(",")[1] : systemOrderId);
        QrpEmcqrNotifyResp qrpNotifyResp = twPayService.getQrpEmvqrNotifyResp(qrpNotifyReq);
        if (Objects.nonNull(tradeDetail) && StringUtils.isBlank(tradeDetail.getTxParams())) {
            // 目前以收主掃交易通知為主; 退貨、取消當下交易時就會會知道

            String key = StringUtils.defaultIfBlank(tradeDetail.getPaymentAccount().getPlatformCheckCode(), this.FISC_PARAMETER);
            boolean isFiscVerifyCodeOk = TWPayUtil.isFiscVerifyCodeOk(qrpNotifyReq, qrpNotifyReq.getVerifyCode(), key);
            LOGGER.info("[REQUEST][/fisc/twpay/webToAppNotify][isFiscVerifyCodeOk]: " + isFiscVerifyCodeOk);

            twPayService.updateTradeDetailByQrpEmvqrNotifyReq(qrpNotifyReq, tradeDetail);
//            response.sendRedirect(String.format("%sallpaypass/api/payment/order/result?orderId=%s", API_URL, EncryptUtil.encryptSimple(tradeDetail.getOrderId())));
        }
        return new ResponseEntity<String>(XmlUtil.marshall(qrpNotifyResp), HttpStatus.OK);
    }
}
