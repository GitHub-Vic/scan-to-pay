package co.intella.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import co.intella.domain.DecryptRequest;
import co.intella.domain.UserMerchantRequest;
import co.intella.model.IntegratedRequest;
import co.intella.service.TokenService;
import co.intella.service.UserMerchantService;
import co.intella.utility.AESUtil;

@RestController
@RequestMapping("/api/userMerchant")
public class UserMerchantController {
	
	Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private AESUtil aesUtil;

	@Autowired
	private UserMerchantService userMerchantService;
	
	@Autowired
    private MessageSource messageSource;

	@Autowired
	private TokenService tokenService;
    
     private Locale locale = new Locale("zh_TW");
	
    @PostMapping("/userId")
	public ResponseEntity<String> getByMerchantSeqId (@RequestBody IntegratedRequest requests) {
		Map<String, Object> result = new HashMap<String, Object>();
        DecryptRequest descryptData = null;
        String encryptData = null;
        String rawData = null;
        try {
        	descryptData = aesUtil.descryptData(requests);
        	String tokenId = null;
        	JSONObject jsonObject = new JSONObject(descryptData.getData().getAsString());
        	if (jsonObject.has("tokenId")) {
        		tokenId = jsonObject.getString("tokenId");
        	}
            if (StringUtils.isNotEmpty(tokenId)) {
        		JSONObject checkToken = tokenService.checkToken(tokenId);
        		if (!checkToken.has("userId")) {
        			result.put("msg", messageSource.getMessage("co.intella.tokenId.error", null, locale));
                    result.put("msgCode", "9995");
        		} else {
        			UserMerchantRequest userMerchant = new Gson().fromJson(descryptData.getData().getAsString(), UserMerchantRequest.class);
                    result = userMerchantService.getByUserId(userMerchant.getUserId());
        		}
            } else {
            	result.put("msg", messageSource.getMessage("co.intella.tokenId.empty", null, locale));
                result.put("msgCode", "9996");
            }
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
            result.put("msg", messageSource.getMessage("co.intella.exception.msg", null, locale));
            result.put("msgCode", "9999");
        } finally {
        	JSONObject json = new JSONObject(result);
        	rawData = json.toString();
			encryptData = aesUtil.encryptData(descryptData.getSecretKey(), rawData);
        }
        return new ResponseEntity<String>(encryptData, HttpStatus.OK);
	}
	
	@PostMapping(value="/insert")
	public ResponseEntity<String> insert(@RequestBody IntegratedRequest requests) {
        Map<String, Object> result = new HashMap<String, Object>();
        DecryptRequest descryptData = null;
        String encryptData = null;
        String rawData = null;
        try {
        	descryptData = aesUtil.descryptData(requests);
        	String tokenId = null;
        	JSONObject jsonObject = new JSONObject(descryptData.getData().getAsString());
        	if (jsonObject.has("tokenId")) {
        		tokenId = jsonObject.getString("tokenId");
        	}
            if (StringUtils.isNotEmpty(tokenId)) {
        		JSONObject checkToken = tokenService.checkToken(tokenId);
        		if (!checkToken.has("userId")) {
        			result.put("msg", messageSource.getMessage("co.intella.tokenId.error", null, locale));
                    result.put("msgCode", "9995");
        		} else {
        			UserMerchantRequest userMerchant = new Gson().fromJson(descryptData.getData().getAsString(), UserMerchantRequest.class);
                    result = userMerchantService.insert(userMerchant);
        		}
            } else {
            	result.put("msg", messageSource.getMessage("co.intella.tokenId.empty", null, locale));
                result.put("msgCode", "9996");
            }
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
            result.put("msg", messageSource.getMessage("co.intella.exception.msg", null, locale));
            result.put("msgCode", "9999");
        } finally {
        	JSONObject json = new JSONObject(result);
        	rawData = json.toString();
			encryptData = aesUtil.encryptData(descryptData.getSecretKey(), rawData);
        }
        return new ResponseEntity<String>(encryptData, HttpStatus.OK);
	}
	
	@PostMapping("/delete")
	public ResponseEntity<String> delete(@RequestBody IntegratedRequest requests) {
		Map<String, Object> result = new HashMap<String, Object>();
        DecryptRequest descryptData = null;
        String encryptData = null;
        String rawData = null;
        try {
        	descryptData = aesUtil.descryptData(requests);
        	String tokenId = null;
        	JSONObject jsonObject = new JSONObject(descryptData.getData().getAsString());
        	if (jsonObject.has("tokenId")) {
        		tokenId = jsonObject.getString("tokenId");
        	}
            if (StringUtils.isNotEmpty(tokenId)) {
        		JSONObject checkToken = tokenService.checkToken(tokenId);
        		if (!checkToken.has("userId")) {
        			result.put("msg", messageSource.getMessage("co.intella.tokenId.error", null, locale));
                    result.put("msgCode", "9995");
        		} else {
        			UserMerchantRequest userMerchant = new Gson().fromJson(descryptData.getData().getAsString(), UserMerchantRequest.class);
                    result = userMerchantService.delete(userMerchant);
        		}
            } else {
            	result.put("msg", messageSource.getMessage("co.intella.tokenId.empty", null, locale));
                result.put("msgCode", "9996");
            }
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
            result.put("msg", messageSource.getMessage("co.intella.exception.msg", null, locale));
            result.put("msgCode", "9999");
        } finally {
        	JSONObject json = new JSONObject(result);
        	rawData = json.toString();
			encryptData = aesUtil.encryptData(descryptData.getSecretKey(), rawData);
        }
        return new ResponseEntity<String>(encryptData, HttpStatus.OK);
	}
	
	@PostMapping("/update")
	public ResponseEntity<String> update(@RequestBody IntegratedRequest requests) {
		Map<String, Object> result = new HashMap<String, Object>();
        DecryptRequest descryptData = null;
        String encryptData = null;
        String rawData = null;
        try {
        	descryptData = aesUtil.descryptData(requests);
        	String tokenId = null;
        	JSONObject jsonObject = new JSONObject(descryptData.getData().getAsString());
        	if (jsonObject.has("tokenId")) {
        		tokenId = jsonObject.getString("tokenId");
        	}
            if (StringUtils.isNotEmpty(tokenId)) {
        		JSONObject checkToken = tokenService.checkToken(tokenId);
        		if (!checkToken.has("userId")) {
        			result.put("msg", messageSource.getMessage("co.intella.tokenId.error", null, locale));
                    result.put("msgCode", "9995");
        		} else {
        			UserMerchantRequest userMerchant = new Gson().fromJson(descryptData.getData().getAsString(), UserMerchantRequest.class);
                    result = userMerchantService.update(userMerchant);
        		}
            } else {
            	result.put("msg", messageSource.getMessage("co.intella.tokenId.empty", null, locale));
                result.put("msgCode", "9996");
            }
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
            result.put("msg", messageSource.getMessage("co.intella.exception.msg", null, locale));
            result.put("msgCode", "9999");
        } finally {
        	JSONObject json = new JSONObject(result);
        	rawData = json.toString();
			encryptData = aesUtil.encryptData(descryptData.getSecretKey(), rawData);
        }
        return new ResponseEntity<String>(encryptData, HttpStatus.OK);
	}
}
