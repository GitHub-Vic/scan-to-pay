package co.intella.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import co.intella.domain.DecryptRequest;
import co.intella.domain.UserRequest;
import co.intella.model.IntegratedRequest;
import co.intella.service.UserService;
import co.intella.utility.AESUtil;

@RestController
@RequestMapping("/api/user")
public class UserController {

	Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private AESUtil aesUtil;
	
	@Autowired
	private MessageSource messageSource;

	private Locale locale = new Locale("zh_TW");
	
	@Autowired
	private UserService userService;

	@PostMapping("/getToken")
	public ResponseEntity<String> getToken(@RequestBody IntegratedRequest requests) throws Exception {
		
		Map<String, Object> result = new HashMap<String, Object>();
        DecryptRequest descryptData = null;
        String encryptData = null;
        String rawData = null;
        try {
            descryptData = aesUtil.descryptData(requests);
            UserRequest userRequest = new Gson().fromJson(descryptData.getData().getAsString(), UserRequest.class);
            result = userService.getToken(userRequest.getClientId(), userRequest.getCode());
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
            result.put("msg", messageSource.getMessage("co.intella.exception.msg", null, locale));
            result.put("msgCode", "9999");
        } finally {
        	JSONObject json = new JSONObject(result);
        	rawData = json.toString();
			encryptData = aesUtil.encryptData(descryptData.getSecretKey(), rawData);
        }
        return new ResponseEntity<String>(encryptData, HttpStatus.OK);
	}
	
	@GetMapping("/getCode")
	public ModelAndView getCode(@RequestParam String code) {
		ModelAndView model = new ModelAndView();
        model.setViewName("appCode");
        model.addObject("code", code);
        return model;
	}
}
