package co.intella.controller;

import co.intella.crypto.AesCryptoUtil;
import co.intella.domain.allpay.AllpayOLPayResponse;
import co.intella.domain.allpay.AllpayReturnData;
import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import co.intella.service.*;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.crypto.SecretKey;
import javax.servlet.http.HttpSession;

import co.intella.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.crypto.SecretKey;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;
import com.sun.org.apache.xml.internal.security.utils.Base64;

/**
 * This api used by PHP module
 * @author Alex
 */
@Controller
@RequestMapping("/api/allpay")
public class AllpayPHPController {

    private final Logger LOGGER = LoggerFactory.getLogger(AllpayPHPController.class);

    @Autowired
    private Environment env;

    @Resource
	private TradeDetailService tradeDetailService;

	@Resource
	private MerchantService merchantService;

	@Resource
	private AppNotificationService appNotificationService;

	@Resource
	private PaymentAccountService paymentAccountService;

	@Resource
	private AllpayService allpayService;

	@Resource
	private RedirectService redirectService;

	@Value("${php.url}")
	private String PHP_URL;

	@Value("${api.url}")
	private String API_URL;

	@RequestMapping(method = RequestMethod.GET, value = "php/{alias}")
	public ResponseEntity<String> php(@PathVariable(value = "alias") String alias) throws IOException {

		// todo url as config
		URL url = new URL("http://localhost/intella-allpaypass/allpaypass/allpay.php?alias=" + alias);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		String token = "";

		BufferedReader in;

		if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {

			in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			token = in.readLine();

		} else {
			// todo handle connection error
			InputStream err = conn.getErrorStream();
			LOGGER.debug("[Allpay][getTradeToken]error");
		}
		LOGGER.info("[Allpay][TradeToken]" + token);
		return new ResponseEntity<String>(token, HttpStatus.OK);

	}

	@RequestMapping("/checkout/{randomId}")
	public ModelAndView checkout(HttpSession session, @PathVariable(value = "randomId") String randomId) {
		LOGGER.info("[REQUEST][ALLPAY] session : " + session.toString());

		TradeDetail tradeDetail = tradeDetailService.getOne(UUID.fromString(randomId));

		ModelAndView model = new ModelAndView();
		model.setViewName("allpayCheckOut");
		model.addObject("randomId", randomId);
		model.addObject("orderNo", tradeDetail.getOrderId());
		model.addObject("totalFee", (int) tradeDetail.getPayment());
		model.addObject("merchant", paymentAccountService.getOne(tradeDetail.getAccountId(), 5).getAccount());
		model.addObject("domain", API_URL);
		model.addObject("merchantName", merchantService.getOne(tradeDetail.getAccountId()).getName());

		try {
			model.addObject("allpayDomain", env.getProperty("host.request.allpay"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return model;
	}

	@RequestMapping(method = RequestMethod.GET, value = "redirect")
	public ModelAndView redirect(@RequestParam(value = "RtnMsg") String rtnMsg,
			@RequestParam(value = "TradeToken") String tradeToken, @RequestParam(value = "RtnCode") String rtnCode,
			@RequestParam(value = "Data", required = false) String data) throws IOException {

		LOGGER.info("[ALLPAY] redirect : ");
		LOGGER.info("[ALLPAY] RtnMsg : " + rtnMsg);
		LOGGER.info("[ALLPAY] TradeToken : " + tradeToken);
		LOGGER.info("[ALLPAY] RtnCode : " + rtnCode);
		LOGGER.info("[ALLPAY] Data : " + data);

		if (!rtnCode.equals("1")) {
			// return model;
			TradeDetail tradeDetail = tradeDetailService.getOneByTradeToken("10500", tradeToken);

			ModelAndView model = new ModelAndView();
			model.addObject("RtnCode", "99999999");
			model.setViewName("paymentResult");

			if (tradeDetail != null) {
				tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
				tradeDetailService.save(tradeDetail);
				model.addObject("MerchantID", merchantService.getOne(tradeDetail.getAccountId()).getName());
				model.addObject("domain", API_URL);
				model.addObject("MerchantTradeNo", tradeDetail.getOrderId());
				model.addObject("TradeAmt", tradeDetail.getPayment());
				model.addObject("PaymentDate", tradeDetail.getCreateDate());
				model.addObject("itemName", tradeDetail.getDescription());
			}
			model = redirectService.confirm(model);
			return model;
		}

		// data =
		// "TVwkVhVFibZF8aAP+XenLZBM0RiIkPmhESHm2jUp9dzCIVAzAxt3rCZcOgHXcq9f5WpNLG+DQqOOxLPBvwKF+DX7fCCSyRWD+T6OX3J8PJUnyTlM91BZfNgfDfQSZ6SfE5RzdaYguE1Micg0IwY+MWNsIRZY1XyWUhwVJWUEjUNUtuB6w93FcQpLIZnPFMUhpaJtsReI9rjBtJ1daVdr/WGrRNV3dwlpxXCA/RJ72blFPwXxe4mRX8/4IgEylhgaGyQZLnnHw+Vvh+KrdC0WK5Aoj5ipyDZrIsTIM3scxsk=";
//        String responseData = URLDecoder.decode(URLDecoder.decode(data,"UTF-8"),"UTF-8");
        String decryptData="";
        try {
            TradeDetail tradeDetail = tradeDetailService.getOneByTradeToken("10500",tradeToken);
            PaymentAccount paymentAccount = paymentAccountService.getOne(tradeDetail.getAccountId(),5);
            SecretKey secretKey =  AesCryptoUtil.convertSecretKey(env.getProperty("allpay.onlinehashkey").getBytes());
            //final Base64.Decoder decoder = Base64.getDecoder();
            decryptData = AesCryptoUtil.decrypt(secretKey,env.getProperty("allpay.onlinehashIV").getBytes(), Base64.decode(data));

            LOGGER.info(decryptData);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("[Exceptin][DecryptData]");

        }



        AllpayOLPayResponse allpayOLPayResponse = new Gson().fromJson(decryptData, AllpayOLPayResponse.class);
        if (allpayOLPayResponse.getRtnCode().equals("1")) {
            TradeDetail tradeDetail = tradeDetailService.getOne(allpayOLPayResponse.getMerchantTradeNo());

//            if (checkMacValue(decryptData,paymentAccountService.getOne(tradeDetail.getAccountId(),5))) {
//                updateTradeDetail(tradeDetail,allpayOLPayResponse);

			ModelAndView model = new ModelAndView();
			model.setViewName("paymentResult");
			model.addObject("titleBarColor", "allPay");
			model.addObject("domain", API_URL);
			model.addObject("MerchantID", merchantService.getOne(tradeDetail.getAccountId()).getName());
			model.addObject("MerchantTradeNo", allpayOLPayResponse.getMerchantTradeNo());
			model.addObject("TradeAmt", tradeDetail.getPayment());
			model.addObject("PaymentDate", tradeDetail.getCreateDate());
			model.addObject("RtnCode", "00");
			model.addObject("itemName", tradeDetail.getDescription());
			model = redirectService.confirm(model);
			return model;
//            }
//            return null;
        } else {
            return null;
        }

    }

    private void updateTradeDetail(TradeDetail tradeDetail, AllpayOLPayResponse allpayOLPayResponse) {
        if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())){
    		tradeDetail.setMethod("10500");
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetailService.save(tradeDetail);
            appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
        }
    }

    private boolean checkMacValue(String decryptData, PaymentAccount one) {
        Map<String,String> result =
                null;
        try {
            result = new ObjectMapper().readValue(decryptData, HashMap.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String macValue = result.get("CheckMacValue");
        result.remove("CheckMacValue");

        String cal = null;
        try {
            cal = allpayService.getCheckMacValue(result,one);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return cal.equals(macValue);

    }

    @RequestMapping(method= RequestMethod.POST, value="returnUrl" )
    public ResponseEntity<String> returnUrl (@RequestBody String body) throws Exception {
        LOGGER.info("[ALLPAY] return platform data: "+body);

        AllpayReturnData returnData = convertStr2Object(body);

        LOGGER.info("[ALLPAY] return returnData: "+ new Gson().toJson(returnData));

        TradeDetail tradeDetail = tradeDetailService.getOne(returnData.getMerchantTradeNo());
        if (tradeDetail != null) {

            if (returnData.getRtnCode().equals("1")) {
                if (returnData.getPaymentType().equals("TopUpUsed_AllPay_TopUp")){
                    tradeDetail.setType(1);
                }
                else if (returnData.getPaymentType().equals("Credit_CreditCard")){
                    tradeDetail.setType(2);
                }
                tradeDetail.setSystemOrderId(returnData.getTradeNo());
                if(!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                }
            } else {
                tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            }
            tradeDetailService.save(tradeDetail);
        }
        return new ResponseEntity<String>("1|OK", HttpStatus.OK);
    }

    private AllpayReturnData convertStr2Object(String result) throws Exception {
        String[] split = result.split("&");
        Map<String,String> map = new HashMap<String, String>();
        for (String element:split) {
            String[] temp = element.split("=");
            if (temp.length==1)
                map.put(temp[0],"");
            else
                map.put(temp[0],temp[1]);
        }
        JsonElement jsonElement = new Gson().toJsonTree(map);
        return new Gson().fromJson(URLDecoder.decode(jsonElement.toString(), "UTF-8"), AllpayReturnData.class);

    }
}
