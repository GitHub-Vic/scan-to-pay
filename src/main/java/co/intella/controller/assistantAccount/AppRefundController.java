package co.intella.controller.assistantAccount;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;

import co.intella.controller.login.SystemLoginController;
import co.intella.model.Merchant;
import co.intella.response.GeneralResponse;
import co.intella.service.MerchantService;
import co.intella.utility.GeneralUtil;

@CrossOrigin
@Controller
@RequestMapping("/api/app")
public class AppRefundController {
	
	private final Logger LOGGER = LoggerFactory.getLogger(SystemLoginController.class);
	
	@Resource
    private MerchantService merchantService;
	
	@RequestMapping(method = RequestMethod.POST, value = "/refundKeyCheck")
    public ResponseEntity<String> crmLogin(@RequestBody Map<String, String> request) throws JsonProcessingException {
		GeneralResponse response = new GeneralResponse();
		
		if (false == valid(request, response)) {
			return new ResponseEntity<String>(new Gson().toJson(response), HttpStatus.OK);
		}
		
		String childAccountId = request.get("accountId");
        String refundPin = request.get("refundPin");
        
        Merchant childMerchant = merchantService.getOne(childAccountId);
		if (childMerchant == null) {
			response.setMsgCode("9990");
	        response.setMsg(childAccountId+"查無對象");
	        response.setData(new HashMap<String, String>());
	        return new ResponseEntity<String>(new Gson().toJson(response), HttpStatus.OK);
		}
		
		Merchant parentMerchant = null;
		if (2 == childMerchant.getAccountType()) {
			String parentAccountId = childMerchant.getParentMerchantAccount();
			parentMerchant = merchantService.getOne(parentAccountId);
		}
		else if (1 == childMerchant.getAccountType()) {
			parentMerchant = childMerchant;
		}
		
		// assertion
		if (parentMerchant == null) {
			LOGGER.error("[DIRTY][Merchant] AccountId: {} AccountType: {} parentMerchant is null", childAccountId, childMerchant.getAccountType());
			defaultFailResponse(response);
	        return new ResponseEntity<String>(new Gson().toJson(response), HttpStatus.OK);
		}
		
		String hashPin = GeneralUtil.getHash(refundPin);
		if (hashPin.equals(childMerchant.getRefundPin())) {
	        HashMap<String, String> map = new HashMap<String, String>();
	        map.put("accountId", parentMerchant.getAccountId());
	        map.put("refundPin", parentMerchant.getRefundPin());
	        response.setData(map);
	        response.setMsgCode("0000");
	        response.setMsg("");
	        return new ResponseEntity<String>(new Gson().toJson(response), HttpStatus.OK);
		}
		else if (!hashPin.equals(childMerchant.getRefundPin())){
			response.setMsgCode("9990");
	        response.setMsg("Account or password is incorrect.");
	        response.setData(new HashMap<String, String>());
	        return new ResponseEntity<String>(new Gson().toJson(response), HttpStatus.OK);
		}
		
		LOGGER.error("[ERROR][refundKeyCheck] default UNHANDLED request: {} ", request);
        defaultFailResponse(response);
        return new ResponseEntity<String>(new Gson().toJson(response), HttpStatus.OK);
	}
	
	private boolean valid(Map<String, String> request, GeneralResponse response) {
		String childAccountId = request.get("accountId");
		if (StringUtils.isEmpty(childAccountId)) {
			response.setMsgCode("9990");
	        response.setMsg("accountId不可為空");
	        response.setData(new HashMap<String, String>());
			return false;
		}
		
		String refundPin = request.get("refundPin");
		if (StringUtils.isEmpty(refundPin)) {
			response.setMsgCode("9990");
	        response.setMsg("refundPin不可為空");
	        response.setData(new HashMap<String, String>());
			return false;
		}
		
		if (response.getMsgCode() == null) {
			return true;
		}
			
		defaultFailResponse(response);
		return false;
	}
	
	private GeneralResponse defaultFailResponse(GeneralResponse response) {
		response.setMsgCode("9000");
        response.setMsg("發生錯誤，請聯繫客服人員");
        response.setData(new HashMap<String, String>());
        return response;
	}

}
