package co.intella.controller;


import co.intella.model.Merchant;
import co.intella.service.MerchantService;
import co.intella.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;


@RestController
@RequestMapping("/api/order")
public class OrderController {

    private final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

    @Resource
    private OrderService orderService;

    @Resource
    private MerchantService merchantService;

    @Autowired
    private MessageSource messageSource;

    private Locale locale = new Locale("zh_TW");

    @RequestMapping(method = RequestMethod.GET, value = "/createAppOrderNo/{merchantId}")
    public Map<String, String> createAppOrderNo(@PathVariable String merchantId) {
        //新版本的手機不好拿唯一識別碼，故後來調整為傳 0000000000 (新版)，由後端拿訂單號
        LOGGER.info("[REQUEST] /api/order/createAppOrder/" + merchantId);
        Map<String, String> respMap = new HashMap<>();
        respMap.put("msg", "");
        respMap.put("orderNo", "");
        Merchant merchant = merchantService.getOne(merchantId);

        if (Objects.nonNull(merchant)) {
            Map<String, String> resultMap = orderService.createOrderIdByMchId(String.valueOf(merchant.getMerchantSeqId()));
            respMap.putAll(resultMap);
        } else {
            respMap.put("msg", messageSource.getMessage("response.7169", null, locale));
        }
        return respMap;

    }


}
