package co.intella.controller;

import org.springframework.beans.factory.annotation.Value;

public class BaseController {
	
	@Value("${api.version:1.0.0}")
	private String apiVersion;
	
	private String getApiVersion() {
		return apiVersion == null ? "0" : String.valueOf(apiVersion.charAt(0));
	}
	
}
