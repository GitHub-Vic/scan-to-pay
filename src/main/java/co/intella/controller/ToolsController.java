package co.intella.controller;

import java.io.File;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.crypto.SecretKey;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import co.intella.crypto.AesCryptoUtil;
import co.intella.crypto.KeyReader;
import co.intella.domain.weixin.ScheduledQueryJob;
import co.intella.model.IntegratedRequest;
import co.intella.model.PaymentAccount;
import co.intella.model.RefundDetail;
import co.intella.model.TradeDetail;
import co.intella.net.Constant;
import co.intella.net.HttpRequestUtil;
import co.intella.schedule.AutoRefundSchedule;
import co.intella.service.AppNotificationService;
import co.intella.service.MqttService;
import co.intella.service.PaymentAccountService;
import co.intella.service.PiService;
import co.intella.service.QueryJobService;
import co.intella.service.RefundDetailService;
import co.intella.service.TradeDetailService;
import co.intella.utility.GeneralResponseMessageUtil;
import co.intella.utility.SystemInstance;

/**
 * @author Miles
 */
@Controller
@RequestMapping("/tools")
public class  ToolsController{

    private final Logger LOGGER = LoggerFactory.getLogger(ToolsController.class);

    @Resource
    private PiService piService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private MqttService mqttService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private QueryJobService jobService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private RefundDetailService refundDetailService;

    @Resource
    private AutoRefundSchedule autoRefundSchedule;

    @Value("${merchant.key.mwd}")
    private String MWD_PARTNER_KEY;

    @Value("${host.pi.request.inapp}")
    private String PI_API_URL;

    @Value("${execution.mode}")
    private String EXEC_MODE;

    private final String EZC_HOST = "http://localhost:8080/allpaypass/api/general";

    @RequestMapping(method = RequestMethod.POST, value = "/auto/refund")
    public ResponseEntity<String> autoRefund(@RequestBody Map<String, String> request) {
	LOGGER.info("[REQUEST][TOOL][autoRefund] " + request);

	String startDate = request.get("start");
	String endDate = request.get("end");

	try {

	    autoRefundSchedule.doRefund(startDate, endDate);

	    return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage("ok"), HttpStatus.OK);
	} catch (Exception e) {
	    LOGGER.error("[EXCEPTION][TOOL]" + Arrays.toString(e.getStackTrace()));
	    return new ResponseEntity<String>(
		    GeneralResponseMessageUtil.getFailureMessage(Arrays.toString(e.getStackTrace())),
		    HttpStatus.BAD_REQUEST);
	}
    }

    @RequestMapping(method = RequestMethod.POST, value = "/mqtt/publish2")
    public ResponseEntity<String> mqttPublish2(@RequestBody Map<String, String> request) {
	LOGGER.info("[REQUEST][TOOL][mqttPublish2] " + request);

	String merchant = request.get("merchant");
	String orderId = request.get("orderId");

	try {
	    appNotificationService.notifyApp(merchant, orderId);
	    return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage("ok"), HttpStatus.OK);
	} catch (Exception e) {
	    LOGGER.error("[EXCEPTION][TOOL]" + e.toString());
	    e.printStackTrace();
	    return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("error"),
		    HttpStatus.BAD_REQUEST);
	}
    }

    @RequestMapping(method = RequestMethod.POST, value = "/mqtt/publish")
    public ResponseEntity<String> mqttPublish(@RequestBody Map<String, String> request) {
	LOGGER.info("[REQUEST][TOOL][mqttPublish] " + request);

	String clientId = request.get("clientId");
	String message = request.get("message");
	String topic = request.get("topic");

	try {
	    mqttService.publish(clientId, message, topic);
	    return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage("ok"), HttpStatus.OK);
	} catch (Exception e) {
	    LOGGER.error("[EXCEPTION][TOOL]" + e.toString());
	    e.printStackTrace();
	    return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("error"),
		    HttpStatus.BAD_REQUEST);
	}
    }

    @RequestMapping(method = RequestMethod.GET, value = "/patch/pi/{dateFrom}")
    public ResponseEntity<String> reconciliation(@PathVariable(value = "dateFrom") String dateFrom) {
	LOGGER.info("[REQUEST][PI][reconciliation] " + dateFrom);

	try {
	    patchTradeDetails(dateFrom);
	    return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage("ok"), HttpStatus.OK);
	} catch (Exception e) {
	    LOGGER.error("[EXCEPTION][TOOL]" + e.toString());
	    e.printStackTrace();
	    return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("error"),
		    HttpStatus.BAD_REQUEST);
	}
    }

    @RequestMapping(method = RequestMethod.GET, value = "/patch/pi2/{dateFrom}")
    public ResponseEntity<String> patchOrder(@PathVariable(value = "dateFrom") String dateFrom) {
	LOGGER.info("[REQUEST][PI][patchOrder] " + dateFrom);

	try {
	    List<TradeDetail> list = tradeDetailService.listByMerchantIdAndDate("mwd", dateFrom + "000000",
		    dateFrom + "235959");

	    for (TradeDetail t : list) {
		if (t.getMethod().equals("60300")) {

		    Map<String, String> requestMap = new HashMap<String, String>();
		    requestMap.put("partner_id", "UE00000000000019");
		    requestMap.put("partner_key", "KEYrn5iR9o8SXOmvWk7WhbggsGPR9JNZ");
		    requestMap.put("partner_order_id", t.getOrderId());

		    String piResponse = HttpRequestUtil.post(PI_API_URL + "payments/orders",
			    new Gson().toJson(requestMap));
		    LOGGER.info("[patchOrder][Pi] " + piResponse);
		    JsonObject json = new Gson().fromJson(piResponse, JsonObject.class);

		    if ("success".equals(json.get("result").getAsString())
			    && "1".equals(json.get("status").getAsString())) {
			LOGGER.info("[patchOrder][Pi][" + t.getOrderId() + "][PENDING] Complete."
				+ json.get("status").getAsString());
			t.setStatus(SystemInstance.TRADE_CREATING_ORDER);
			tradeDetailService.save(t);
		    } else if ("success".equals(json.get("result").getAsString())
			    && "3".equals(json.get("status").getAsString())) {
			LOGGER.info("[patchOrder][Pi][" + t.getOrderId() + "][PAY] Complete."
				+ json.get("status").getAsString());
			t.setStatus(SystemInstance.TRADE_SUCCESS);
			t.setPlatformPaidDate(json.get("paid_at").getAsString());
			tradeDetailService.save(t);
		    } else if ("success".equals(json.get("result").getAsString())
			    && "D".equals(json.get("status").getAsString())) {
			LOGGER.info("[patchOrder][Pi][" + t.getOrderId() + "][REFUND] Complete."
				+ json.get("status").getAsString());
			t.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
			t.setPlatformPaidDate(json.get("paid_at").getAsString());
			tradeDetailService.save(t);
			RefundDetail refundDetail = refundDetailService.getTopOneByStoreRefundId("mwd", t.getOrderId());
			refundDetail.setPlatformRefundDate(json.get("refunded_at").getAsString());
			refundDetailService.save(refundDetail);
		    } else {
			LOGGER.info("[patchOrder][Pi][" + t.getOrderId() + "] Do nothing." + piResponse);
		    }
		}
	    }

	    return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage("ok"), HttpStatus.OK);
	} catch (Exception e) {
	    LOGGER.error("[EXCEPTION][TOOL]" + e.toString());
	    e.printStackTrace();
	    return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("error"),
		    HttpStatus.BAD_REQUEST);
	}
    }

    @RequestMapping(method = RequestMethod.GET, value = "/addJob/{orderId}/{merchantId}/{bankId}")
    public ResponseEntity<String> addJob(@PathVariable(value = "orderId") String orderId,
	    @PathVariable(value = "merchantId") String merchantId, @PathVariable(value = "bankId") Long bankId) {
	try {
	    PaymentAccount paymentAccount = paymentAccountService.getOne(merchantId, bankId);
	    jobService.addJob(orderId, merchantId, paymentAccount.getAccount(), bankId);
	    return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage("add job done"),
		    HttpStatus.OK);
	} catch (Exception e) {
	    LOGGER.error("[EXCEPTION][" + bankId + "][addJob] " + Arrays.toString(e.getStackTrace()));
	    return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("error"),
		    HttpStatus.BAD_REQUEST);
	}
    }

    @RequestMapping(method = RequestMethod.GET, value = "/listJobs")
    public ResponseEntity<List<ScheduledQueryJob>> list() {
	try {
	    return new ResponseEntity<List<ScheduledQueryJob>>(jobService.list(), HttpStatus.OK);
	} catch (Exception e) {
	    LOGGER.error("[EXCEPTION][QUERY][JOB][LIST] " + Arrays.toString(e.getStackTrace()));
	    return new ResponseEntity<List<ScheduledQueryJob>>(new ArrayList<ScheduledQueryJob>(),
		    HttpStatus.BAD_REQUEST);
	}
    }

    @RequestMapping(method = RequestMethod.GET, value = "/ezc/trade/{times}/{deviceId}/{range}")
    public ResponseEntity<String> ezcTest(@PathVariable(value = "times") String times,
	    @PathVariable(value = "deviceId") String deviceId, @PathVariable(value = "range") String range) {
	try {

	    for (int i = 0; i < Integer.valueOf(times); i++) {
		LOGGER.info("[TOOL][EZC][AUTO] ------- " + i + " --------- START ");
		ezcContinuousTest(deviceId, Integer.valueOf(range));
		LOGGER.info("[TOOL][EZC][AUTO] ------- " + i + " --------- END ");
		TimeUnit.SECONDS.sleep(3);
	    }

	    LOGGER.info("[TOOL][EZC][AUTO] ------- CHECKOUT --------- START ");
	    doSettlement(deviceId);
	    LOGGER.info("[TOOL][EZC][AUTO] ------- CHECKOUT --------- START ");
	    return new ResponseEntity<String>("ok", HttpStatus.OK);
	} catch (Exception e) {
	    LOGGER.error("[EXCEPTION][QUERY][JOB][LIST] " + Arrays.toString(e.getStackTrace()));
	    return new ResponseEntity<String>("fail", HttpStatus.BAD_REQUEST);
	}
    }

    private void ezcContinuousTest(String deviceId, int range) throws Exception {

	Random random = new Random();
	int action = random.nextInt(3) + 1;

	Random r2 = new Random();
	int amount = r2.nextInt(range) + 1;

	if (action == 1) {
	    LOGGER.info("[TOOL][EZC][AUTO] ----------- doPayment ----------");
	    doPayment(deviceId, amount);
	}

	if (action == 2) {
	    LOGGER.info("[TOOL][EZC][AUTO] ----------- doRefund ----------");
	    doRefund(deviceId, amount);
	}

	if (action == 3) {
	    LOGGER.info("[TOOL][EZC][AUTO] ----------- doReserve ----------");
	    doReserve(deviceId, amount);
	}
    }

    private void doPayment(String deviceId, int amount) throws Exception {
	Map<String, String> requestData = new HashMap<String, String>();
	// setting header
	requestData.put("Method", "31800");
	requestData.put("ServiceType", "Payment");
	requestData.put("MchId", "super");
	requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN)); // format must be
											     // yyyyMMddHHmmss

	requestData.put("Amount", String.valueOf(amount));
	requestData.put("Retry", "0");
	requestData.put("DeviceId", deviceId);
	requestData.put("StoreOrderNo", "AUTO" + DateTime.now().toString(SystemInstance.DATE_PATTERN));
	SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
	IntegratedRequest encryptRequest = getEncryptRequest(requestData, secretKey);

	String result = HttpRequestUtil.post(EZC_HOST, new Gson().toJson(encryptRequest));
	String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
	JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);

	LOGGER.info("[TOOL][EZC][doPayment]" + json);
	JsonObject responseData = json.getAsJsonObject("Data");
	String retry = responseData.get("retry").getAsString();
	JsonObject originalRequestData = responseData.getAsJsonObject("request");
	originalRequestData.get("deviceId").getAsString();

	while (retry.equals("1")) {
	    LOGGER.info("[TOOL][EZC][retryPayment] ------ retryPayment -----" + originalRequestData.toString());
	    JsonObject retryResult = retryPayment(originalRequestData);
	    retry = retryResult.getAsJsonObject("Data").get("retry").getAsString();
	}
    }

    private void doRefund(String deviceId, int amount) throws Exception {
	Map<String, String> requestData = new HashMap<String, String>();
	// setting header
	requestData.put("Method", "31800");
	requestData.put("ServiceType", "EZCRefund");
	requestData.put("MchId", "super");
	requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN)); // format must be
											     // yyyyMMddHHmmss

	requestData.put("DeviceId", deviceId);
	requestData.put("Amount", String.valueOf(amount));
	requestData.put("Retry", "0");
	requestData.put("RefundKey", "0000");

	SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
	IntegratedRequest encryptRequest = getEncryptRequest(requestData, secretKey);

	String result = HttpRequestUtil.post(EZC_HOST, new Gson().toJson(encryptRequest));
	String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
	JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);

	LOGGER.info("[TOOL][EZC][doRefund]" + json);
	JsonObject responseData = json.getAsJsonObject("Data");
	String retry = responseData.get("retry").getAsString();
	JsonObject originalRequestData = responseData.getAsJsonObject("request");
	originalRequestData.get("deviceId").getAsString();

	while (retry.equals("1")) {
	    LOGGER.info("[TOOL][EZC][retryRefund] ------ retryRefund -----" + originalRequestData.toString());
	    JsonObject retryResult = retryRefund(originalRequestData);
	    retry = retryResult.getAsJsonObject("Data").get("retry").getAsString();
	}
    }

    private void doSettlement(String deviceId) throws Exception {
	Map<String, String> requestData = new HashMap<String, String>();
	// setting header
	requestData.put("Method", "31800");
	requestData.put("ServiceType", "Settlement");
	requestData.put("MchId", "super");
	requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN)); // format must be
											     // yyyyMMddHHmmss

	requestData.put("DeviceId", deviceId);

	SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
	IntegratedRequest encryptRequest = getEncryptRequest(requestData, secretKey);

	String result = HttpRequestUtil.post(EZC_HOST, new Gson().toJson(encryptRequest));
	String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
	JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);

	LOGGER.info("[TOOL][EZC][doSettlement]" + json);
    }

    private void doReserve(String deviceId, int amount) throws Exception {
	Map<String, String> requestData = new HashMap<String, String>();
	// setting header
	requestData.put("Method", "31800");
	requestData.put("ServiceType", "Reserve");
	requestData.put("MchId", "super");
	requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN)); // format must be
											     // yyyyMMddHHmmss

	requestData.put("DeviceId", deviceId);
	requestData.put("Amount", String.valueOf(amount));
	requestData.put("Retry", "0");
	requestData.put("StoreOrderNo", "AUTO" + DateTime.now().toString(SystemInstance.DATE_PATTERN));

	SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
	IntegratedRequest encryptRequest = getEncryptRequest(requestData, secretKey);

	String result = HttpRequestUtil.post(EZC_HOST, new Gson().toJson(encryptRequest));
	String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
	JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);

	LOGGER.info("[TOOL][EZC][doReserve]" + json);
	JsonObject responseData = json.getAsJsonObject("Data");
	String retry = responseData.get("retry").getAsString();
	JsonObject originalRequestData = responseData.getAsJsonObject("request");
	originalRequestData.get("deviceId").getAsString();

	while (retry.equals("1")) {
	    LOGGER.info("[TOOL][EZC][retryReserve] ------ retryReserve -----" + originalRequestData.toString());
	    JsonObject retryResult = retryReserve(originalRequestData);
	    retry = retryResult.getAsJsonObject("Data").get("retry").getAsString();
	}
    }

    private JsonObject retryPayment(JsonObject originalRequestData) throws Exception {
	Map<String, String> requestData = new HashMap<String, String>();
	// setting header
	requestData.put("Method", "31800");
	requestData.put("ServiceType", "Payment");
	requestData.put("MchId", "super");
	requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN)); // format must be
											     // yyyyMMddHHmmss

	requestData.put("DeviceId", originalRequestData.get("deviceId").getAsString());
	requestData.put("Amount", originalRequestData.get("amount").getAsString());
	requestData.put("Retry", "1");
	requestData.put("TerminalTXNNumber", originalRequestData.get("terminalTXNNumber").getAsString());
	requestData.put("HostSerialNumber", originalRequestData.get("hostSerialNumber").getAsString());

	SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
	IntegratedRequest encryptRequest = getEncryptRequest(requestData, secretKey);

	String result = HttpRequestUtil.post(EZC_HOST, new Gson().toJson(encryptRequest));
	String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
	return new Gson().fromJson(decryptResponse, JsonObject.class);
    }

    private JsonObject retryRefund(JsonObject originalRequestData) throws Exception {
	Map<String, String> requestData = new HashMap<String, String>();
	// setting header
	requestData.put("Method", "31800");
	requestData.put("ServiceType", "EZCRefund");
	requestData.put("MchId", "super");
	requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN)); // format must be
											     // yyyyMMddHHmmss

	requestData.put("DeviceId", originalRequestData.get("deviceId").getAsString());
	requestData.put("Amount", originalRequestData.get("amount").getAsString());
	requestData.put("Retry", "1");
	requestData.put("RefundKey", "0000");
	requestData.put("TerminalTXNNumber", originalRequestData.get("terminalTXNNumber").getAsString());
	requestData.put("HostSerialNumber", originalRequestData.get("hostSerialNumber").getAsString());

	SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
	IntegratedRequest encryptRequest = getEncryptRequest(requestData, secretKey);

	String result = HttpRequestUtil.post(EZC_HOST, new Gson().toJson(encryptRequest));
	String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
	return new Gson().fromJson(decryptResponse, JsonObject.class);
    }

    private JsonObject retryReserve(JsonObject originalRequestData) throws Exception {
	Map<String, String> requestData = new HashMap<String, String>();
	// setting header
	requestData.put("Method", "31800");
	requestData.put("ServiceType", "Reserve");
	requestData.put("MchId", "super");
	requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN)); // format must be
											     // yyyyMMddHHmmss

	requestData.put("DeviceId", originalRequestData.get("deviceId").getAsString());
	requestData.put("Amount", originalRequestData.get("amount").getAsString());
	requestData.put("Retry", "1");
	requestData.put("TerminalTXNNumber", originalRequestData.get("terminalTXNNumber").getAsString());
	requestData.put("HostSerialNumber", originalRequestData.get("hostSerialNumber").getAsString());

	SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
	IntegratedRequest encryptRequest = getEncryptRequest(requestData, secretKey);

	String result = HttpRequestUtil.post(EZC_HOST, new Gson().toJson(encryptRequest));
	String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
	return new Gson().fromJson(decryptResponse, JsonObject.class);
    }

    public IntegratedRequest getEncryptRequest(Map<String, String> requestData, SecretKey secretKey) throws Exception {

	String keyPath = EXEC_MODE.equals("stage") ? "stage-pub.der" : "pub.der";

	ClassLoader classLoader = getClass().getClassLoader();
	File file = new File(classLoader.getResource("key/" + keyPath).getFile());
	PublicKey publicKey = KeyReader.loadPublicKeyFromDER(file);

	String requestString = HttpRequestUtil.getEncryptRequestJson(requestData, publicKey, secretKey);
	return new Gson().fromJson(requestString, IntegratedRequest.class);
    }

    private void patchTradeDetails(String startDate) throws Exception {
	piService.reconciliation("UE00000000000019", "KEYrn5iR9o8SXOmvWk7WhbggsGPR9JNZ", startDate);
    }

}
