package co.intella.controller;

import co.intella.exception.InvokeAPIException;
import co.intella.service.ICashService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;


@RestController
@RequestMapping("/api/iCash")
public class IcashController {

    private final Logger LOGGER = LoggerFactory.getLogger(IcashController.class);

    @Resource
    private ICashService iCashService;

    @RequestMapping(method = RequestMethod.GET, value = "/doBlcDownload")
    public String doBlcDownload() {
        //  sh 每天 22:30  正式機在A2
        LOGGER.info("[ICASH][doBlcDownload] Start...");
        //從愛金卡公司的FTP下載最新的黑名單檔到本機端。
        //目前：從FTP下載最新的黑名單檔到/ICASH/iCashSource/iCash/NegaList/
        String result = "Fail";
        try {
            result = iCashService.doBlcDownload();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (InvokeAPIException e) {
            e.printStackTrace();
        }
        LOGGER.info("[ICASH][doBlcDownload] end ..." + result);
        return result;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/doStoreMain")
    public String doStoreMain(HttpServletRequest request) {
        //    sh  每天 00:05  正式機在A2
        LOGGER.info("[ICASH][doStoreMain] Start...");
        //每天都必需上傳門市主檔給愛金卡公司
        String result = "Fail";
        try {
            result = iCashService.doStoreMain();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (InvokeAPIException e) {
            e.printStackTrace();
        }
        LOGGER.info("[ICASH][doStoreMain] end ..." + result);
        return result;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/settlement/{createDate}")
    public boolean settlement(@PathVariable("createDate") String createDate) {
        //    sh  每天 00:30  正式機在A2
        return settlementMain(createDate, "", "00");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/settlement/{createDate}/{endDate}")
    public boolean settlement(@PathVariable("createDate") String createDate, @PathVariable("endDate") String endDate) {
        //   預留  手動補
        return settlementMain(createDate, endDate, "00");
    }

    private boolean settlementMain(String createDate, String endDate, String batchEndStr) {
        LOGGER.info("[ICash][settlementMain] settlementMain createDate = " + createDate + " , endDate = " + endDate);
        boolean c = false;
        try {
            iCashService.settlementMain(createDate, endDate, batchEndStr);
            c = true;
        } catch (Exception e) {
            LOGGER.error("[settlementMain][EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
        }
        return c;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/settlementNew/{createDate}")
    public boolean settlementNew(@PathVariable("createDate") String createDate) {
        //    sh  每天 00:50  正式機在A2
        return settlementMain(createDate, "", "01");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/settlementNew/{createDate}/{endDate}")
    public boolean settlementNew(@PathVariable("createDate") String createDate, @PathVariable("endDate") String endDate) {
        //   預留  手動補
        return settlementMain(createDate, endDate, "01");
    }
}
