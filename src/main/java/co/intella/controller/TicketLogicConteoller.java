package co.intella.controller;


import co.intella.model.TaxiTransactionDetail;
import co.intella.service.TicketLogicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/ticketLogic")
public class TicketLogicConteoller {
    private final Logger LOGGER = LoggerFactory.getLogger(TicketLogicConteoller.class);

    @Resource
    private TicketLogicService ticketLogicService;

    @RequestMapping(method = RequestMethod.GET, value = "/getAbnormalRuleMsgByReport/{startDate}/{endDate}")
    public List<TaxiTransactionDetail> getAbnormalRuleMsgByReport(@PathVariable("startDate") String startDate, @PathVariable("endDate") String endDate, @RequestParam(value = "terminalId") String terminalId) {
        LOGGER.info("[getAbnormalRuleMsgByReport] startDate = " + startDate + " , endDate = " + endDate + " , terminalId = " + terminalId);
        return ticketLogicService.getAbnormalRuleMsgByReport(startDate, endDate, terminalId);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/autoLogin")
    public Boolean autoLogin() {
        // 排成 每日  2點  5點 ，正式機 在A2
        LOGGER.info("[autoLogin] START = " + LocalDateTime.now().toString());
        boolean isOk = ticketLogicService.autoLogin();
        LOGGER.info("[autoLogin] END = " + LocalDateTime.now().toString());
        return isOk;
    }
}
