package co.intella.controller;

import co.intella.domain.StatusMessage;
import co.intella.domain.tsbank.TSBOtherResponseData;
import co.intella.domain.tsbank.TSBOtherResponseDataPara;
import co.intella.domain.tsbank.TSBPostBackPara;
import co.intella.domain.tsbank.TSBVirtualAccountData;
import co.intella.exception.InvalidDataFormatException;
import co.intella.exception.NoSuchMerchantException;
import co.intella.exception.OrderAlreadyExistedException;
import co.intella.model.InvoiceDetail;
import co.intella.model.RefundDetail;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import co.intella.service.*;
import co.intella.utility.GeneralResponseMessageUtil;
import co.intella.utility.JsonUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.*;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import co.intella.service.*;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

/**
 * @author Alex
 */
@Controller
@RequestMapping("/api/tsb")
public class TsBankCreaditCardController {

    private final Logger LOGGER = LoggerFactory.getLogger(TsBankCreaditCardController.class);

    @Autowired
    private TradeDetailService tradeDetailService;

    @Autowired
    private MerchantService merchantService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private TsBankService tsBankService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private InvoiceDetailService invoiceDetailService;

    @Resource
    private RefundDetailService refundDetailService;

    @Resource
    private CheckInputService checkInputService;

    @Autowired
    private MessageSource messageSource;

    @Resource
    private TradeProcedureService tradeProcedureService;

    private Locale locale = new Locale("zh_TW");

    @Value("${api.url}")
    private String API_URL;
    @Resource
    private RedirectService redirectService;

    @RequestMapping(value = "/credit")
    public ResponseEntity<String> credit(@RequestBody String body, HttpServletRequest request) throws IOException {
//        LOGGER.info("[/credit] received item: " + body);
        String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
        String clientIP = request.getHeader("X-FORWARDED-FOR");

        JsonObject jsonObject = new Gson().fromJson(body, JsonObject.class);
        JsonObject header = jsonObject.getAsJsonObject("Header");
        JsonPrimitive data = jsonObject.getAsJsonPrimitive("Data");

        RequestHeader requestHeader = new Gson().fromJson(header.toString(), RequestHeader.class);
        JsonObject requestData3 = new Gson().fromJson(data.getAsString(), JsonObject.class);

        String integrateMchId = requestHeader.getMerchantId();

        if (requestData3.has("Body")) {
            String bodyRandom = requestData3.get("Body").getAsString();
            if ("Payment".equals(requestHeader.getServiceType())) {
                bodyRandom += requestData3.has("CardId") ? "-" + requestData3.get("CardId").getAsString().substring(12)
                        : "";
            }
            requestData3.remove("Body");
            requestData3.addProperty("Body", bodyRandom);
            data = new JsonPrimitive(new Gson().toJson(requestData3));
        }

        if (requestHeader.getMethod().startsWith("2")) {
            if ("21100".equals(requestHeader.getMethod())) {
                // ECPay
            } else {
                LOGGER.debug("[dispatch credit][Tsbank][" + sessionId + "]");
                requestHeader.setMethod("20800");
            }
        }
        String generalResponseData = null;
        try {
            generalResponseData = tsBankService.doRequest(requestHeader, data, integrateMchId, clientIP);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("[Exception][20800]" + e.getMessage());

        }
        LOGGER.debug("[RESPONSE][20800][" + sessionId + "]" + generalResponseData);

        return new ResponseEntity<String>(generalResponseData, HttpStatus.OK);

    }

    @RequestMapping(value = "return")
    public ModelAndView postback(@RequestBody String body) throws Exception {

        LOGGER.info("[REQUEST][TSB] api/tsb/return " + body);

        TSBPostBackPara tsbPostBackPara = convertStr2Object(body);

        LOGGER.info("[RESPONSE][TSB][POST_BACK]" + new Gson().toJson(tsbPostBackPara));

        TradeDetail tradeDetail = tradeDetailService.getOne(tsbPostBackPara.getOrderNo());

        ModelAndView model = new ModelAndView();
        model.setViewName("paymentResult");
        model.addObject("MerchantID", merchantService.getOne(tradeDetail.getAccountId()).getName());
        model.addObject("MerchantTradeNo", tsbPostBackPara.getOrderNo());
        model.addObject("TradeAmt", tradeDetail.getPayment());
        model.addObject("PaymentDate", DateTime.now().toString(SystemInstance.DATE_PATTERN));
        model.addObject("itemName", tradeDetail.getDescription());
        model.addObject("titleBarColor", "tsColor");
        model.addObject("domain", API_URL);
        model.addObject("token", tradeDetail.getQrcodeToken());

        if (tsbPostBackPara.getRetCode().equals("00") ||
                (tsbPostBackPara.getRetCode().equals("-300") && SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus()))) {
            model.addObject("RtnCode", "00");
            tradeDetail.setMethod("20800");
            if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                tradeDetailService.save(tradeDetail);
                appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
            }
        } else {
            model.addObject("RtnCode", "99");
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            tradeDetailService.save(tradeDetail);
        }
        model = redirectService.confirm(model);
        return model;
    }

    @RequestMapping(value = "notify")
    public ResponseEntity<String> result(@RequestBody String body) throws IOException {
        LOGGER.info("[REQUEST][TSB] api/tsb/notify" + body);
        ObjectMapper mapper = new ObjectMapper();

        TSBOtherResponseData tsbOtherResponseData = mapper.readValue(body, TSBOtherResponseData.class);

        LOGGER.info("[RESPONSE][TSB] " + new Gson().toJson(tsbOtherResponseData));

        TradeDetail tradeDetail = tradeDetailService.getOne(tsbOtherResponseData.getParams().getOrderNo());

        if (tradeDetail != null) {
            tradeDetail.setMethod("20800");
            if (tsbOtherResponseData.getParams().getRetCode().equals("00")) {
                tradeDetail.setSystemOrderId(tsbOtherResponseData.getParams().getRrn());
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                }
                tradeDetailService.save(tradeDetail);
//                appNotificationService.notifyApp(tradeDetail,merchantService.getOne(tradeDetail.getAccountId()));

            } else {
                tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            }
            tradeDetailService.save(tradeDetail);
            //TradeDetail tradeDetail = tradeDetailService.getOneBySystemOrderId(tsbPostBackPara.getOrderNo());
        }

        return new ResponseEntity<String>("OK", HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.POST, value = "virtualAccount")
    public ResponseEntity<String> getVirtualAccount(@RequestBody String body) {

        LOGGER.info("[REQUEST][TSB] api/tsb/virtualAccount " + body);

        TSBVirtualAccountData data = new Gson().fromJson(body, TSBVirtualAccountData.class);

        DateTime serverTime = DateTime.now();

        String account = tsBankService.getVirtualAccount(String.format("%06d", new Random().nextInt(999999)), String.valueOf(serverTime.getYear() - 1).substring(3) + String.valueOf(serverTime.getDayOfYear()), data.getFee());


        try {
            checkInputService.doRequest(data);
        } catch (NoSuchMerchantException e) {
            StatusMessage status = new StatusMessage();
            status.setStatus("7132");
            status.setMessage(messageSource.getMessage("response.7132", null, locale));
            return new ResponseEntity<String>(new Gson().toJson(status), HttpStatus.OK);
        } catch (OrderAlreadyExistedException e) {
            StatusMessage status = new StatusMessage();
            status.setStatus("7000");
            status.setMessage(messageSource.getMessage("response.7000", null, locale));
            return new ResponseEntity<String>(new Gson().toJson(status), HttpStatus.OK);
        } catch (InvalidDataFormatException e) {
            StatusMessage status = new StatusMessage();
            status.setStatus("8002");
            status.setMessage(messageSource.getMessage("response.8002", null, locale));
            return new ResponseEntity<String>(new Gson().toJson(status), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        TradeDetail tradeDetail = new TradeDetail();
        tradeDetail.setOrderId(data.getOrderId());
        tradeDetail.setPayment(data.getFee());
        tradeDetail.setDescription(data.getItemName());
        //TODO virtual account method = ? and service type = ?
        tradeDetail.setMethod("20800");
        tradeDetail.setServiceType("VAccount");
        tradeDetail.setAccountId(data.getMchId());
        tradeDetail.setTradeToken(account);
        tradeDetail.setPaymentAccount(paymentAccountService.getOne(data.getMchId(), 8));
        tradeDetail.setDeviceRandomId(deviceService.getOne(merchantService.getOne(data.getMchId()), "APP-001"));
        tradeDetail.setCreateDate(data.getCreateTime());
        tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
        tradeDetail = tradeDetailService.save(tradeDetail);
        tradeProcedureService.captureOrderSave(tradeDetail);
        return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(account), HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.POST, value = "partialRefund")
    @Transactional
    public ResponseEntity<String> partialRefund(@RequestBody String body) throws Exception {

        JsonObject request = new Gson().fromJson(body, JsonObject.class);

        String orderId = request.get("orderId").getAsString();
        Double refundAmount = request.get("amount").getAsDouble();
        String createDate = request.get("createDate").getAsString();
        String merchantId = request.get("merchant").getAsString();
        String refundPin = request.get("refundKey").getAsString();

        if (!merchantService.getOne(merchantId).getRefundPin().equals(refundPin)) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("fail"), HttpStatus.OK);
        }

        String chargeAmount;

        TradeDetail tradeDetail = tradeDetailService.getOne(orderId);

        Map<String, String> requestMap = new HashMap<String, String>();
        requestMap.put("StoreOrderNo", orderId);
        //requestMap.put("DeviceInfo","A6113004");
        JsonPrimitive jsonPrimitive = new JsonPrimitive(new Gson().toJson(requestMap));

        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setServiceType("SingleOrderQuery");
        requestHeader.setMerchantId("super");

        String result = tsBankService.doRequest(requestHeader, jsonPrimitive, tradeDetail.getAccountId(), "127.0.0.1");


//        JsonObject jsonObject = new Gson().fromJson(new Gson().fromJson(result,JsonObject.class).get("Data"),JsonObject.class);
//        JsonPrimitive jsonPrimitive1 = jsonObject.getAsJsonPrimitive("PlatformRsp").getAsJsonPrimitive();
//        JsonObject jsonObject1 = JsonUtil.convertJsonPrimitiveToJsonObject(jsonPrimitive1);
        TSBOtherResponseDataPara tsResponse = getTSPara(result);//new Gson().fromJson(jsonObject1.get("params"),TSBOtherResponseDataPara.class);

        if (!tsResponse.getRetCode().equals("00")) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("fail"), HttpStatus.OK);
        }

        if (checkOrderclosed(tsResponse)) {
            //todo return
            requestHeader.setServiceType("Return");
            requestMap.put("TotalFee", String.valueOf(refundAmount));
            jsonPrimitive = new JsonPrimitive(new Gson().toJson(requestMap));
            String chargeResult = tsBankService.doRequest(requestHeader, jsonPrimitive, tradeDetail.getAccountId(), "127.0.0.1");
            TSBOtherResponseDataPara returnResponse = getTSPara(chargeResult);

            if (returnResponse.getRetCode().equals("00")) {
                RefundDetail refundDetail = new RefundDetail();
                refundDetail.setMethod("20800");
                refundDetail.setOrderId(orderId);
                refundDetail.setAmount(refundAmount.longValue());
                refundDetail.setTradeDetailRandomId(tradeDetail);
                refundDetail.setCreateTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
                refundDetail.setStoreRefundId("R" + orderId);
                refundDetailService.save(refundDetail);

                InvoiceDetail invoiceDetail = new InvoiceDetail();

                invoiceDetail.setOrderId(orderId);
                invoiceDetail.setAmount(Double.valueOf(refundAmount));
                invoiceDetail.setCreateDate(createDate);
                invoiceDetail.setRefundDetailRandomId(refundDetail);
                invoiceDetail.setTradeDetailRandomId(tradeDetail);

                invoiceDetailService.save(invoiceDetail);
                return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(), HttpStatus.OK);

            } else {
                return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage(""), HttpStatus.OK);
            }

        } else if (tsResponse.getOrderStatus().equals("02")) {
            requestHeader.setServiceType("Charge");

            chargeAmount = getchargeAmount(tradeDetail, refundAmount);

            requestMap.put("TotalFee", chargeAmount);
            jsonPrimitive = new JsonPrimitive(new Gson().toJson(requestMap));

            String chargeResult = tsBankService.doRequest(requestHeader, jsonPrimitive, tradeDetail.getAccountId(), "127.0.0.1");
//            String chargeResult ="{\"Header\":{\"StatusCode\":\"0000\",\"StatusDesc\":\"jkk\",\"Method\":\"20800\",\"ServiceType\":\"Charge\",\"MchId\":\"super\",\"ResponseTime\":\"20171016175314\"},\"Data\":{\"StoreOrderNo\":\"IntellaA170825000042\",\"PlatformRsp\":\"{\\\"ver\\\":\\\"1.0.0\\\",\\\"mid\\\":\\\"000812770060201\\\",\\\"tid\\\":\\\"T0000000\\\",\\\"pay_type\\\":1,\\\"tx_type\\\":3,\\\"ret_value\\\":\\\"0\\\",\\\"params\\\":{\\\"ret_code\\\":\\\"00\\\",\\\"auth_id_resp\\\":\\\"010154\\\",\\\"rrn\\\":\\\"728907843527\\\",\\\"order_status\\\":\\\"02\\\",\\\"auth_type\\\":\\\"SSL\\\",\\\"cur\\\":\\\"NTD\\\",\\\"purchase_date\\\":\\\"2017-10-16 15:57:22\\\",\\\"tx_amt\\\":\\\"300\\\",\\\"settle_amt\\\":\\\"100\\\",\\\"settle_seq\\\":\\\"171016948033\\\",\\\"settle_date\\\":\\\"2017-10-16\\\",\\\"refund_trans_amt\\\":\\\"0\\\",\\\"redeem_pt\\\":\\\"0\\\",\\\"redeem_amt\\\":\\\"0\\\",\\\"post_redeem_amt\\\":\\\"0\\\",\\\"post_redeem_pt\\\":\\\"0\\\",\\\"install_down_pay\\\":\\\"0\\\",\\\"install_pay\\\":\\\"0\\\"}}\"}}";
            TSBOtherResponseDataPara chargeResponse = getTSPara(chargeResult);

            if (chargeResponse.getRetCode().equals("00")) {
                RefundDetail refundDetail = new RefundDetail();
                refundDetail.setMethod("20800");
                refundDetail.setOrderId(orderId);
                refundDetail.setAmount(refundAmount.longValue());
                refundDetail.setTradeDetailRandomId(tradeDetail);
                refundDetail.setCreateTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
                refundDetail.setStoreRefundId("R" + orderId);
                refundDetailService.save(refundDetail);

                InvoiceDetail invoiceDetail = new InvoiceDetail();

                invoiceDetail.setOrderId(orderId);
                invoiceDetail.setAmount(Double.valueOf(chargeAmount));
                invoiceDetail.setCreateDate(createDate);
                invoiceDetail.setRefundDetailRandomId(refundDetail);
                invoiceDetail.setTradeDetailRandomId(tradeDetail);

                invoiceDetailService.save(invoiceDetail);
                return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(), HttpStatus.OK);

            } else {
                return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage(""), HttpStatus.OK);
            }
        } else if (tsResponse.getOrderStatus().equals("03")) {
            //todo repeat partial refund
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage(""), HttpStatus.OK);

        }
        return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage(""), HttpStatus.OK);

    }

    private TSBOtherResponseDataPara getTSPara(String result) {

        JsonObject jsonObject = new Gson().fromJson(new Gson().fromJson(result, JsonObject.class).get("Data"), JsonObject.class);
        JsonPrimitive jsonPrimitive = jsonObject.getAsJsonPrimitive("PlatformRsp").getAsJsonPrimitive();
        JsonObject jsonObject2 = JsonUtil.convertJsonPrimitiveToJsonObject(jsonPrimitive);
        return new Gson().fromJson(jsonObject2.get("params"), TSBOtherResponseDataPara.class);

    }

    private String getchargeAmount(TradeDetail tradeDetail, Double refundAmount) {
        String chargeAmount;

//        if (!tradeDetail.getRefundStatus().equals("Cancel success")) {
        chargeAmount = String.valueOf((int) (tradeDetail.getPayment() - refundAmount));
        return chargeAmount;
//        }
//        else return null;
    }

    private boolean checkOrderclosed(TSBOtherResponseDataPara result) {
        return result.getOrderStatus().equals("04");
    }

    private boolean checkOrderCharged(TradeDetail tradeDetail) {
        return false;
    }

    private TSBPostBackPara convertStr2Object(String result) throws Exception {
        String[] split = result.split("&");
        Map<String, String> map = new HashMap<String, String>();
        for (String element : split) {
            String[] temp = element.split("=");
            if (temp.length == 1) {
                map.put(temp[0], "");
            } else {
                map.put(temp[0], temp[1]);
            }
        }
        JsonElement jsonElement = new Gson().toJsonTree(map);
        return new Gson().fromJson(URLDecoder.decode(jsonElement.toString(), "UTF-8"), TSBPostBackPara.class);

    }


}
