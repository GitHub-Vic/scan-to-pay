//package co.intella.controller;
//
//import co.intella.service.TsmcService;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.Resource;
//
//@RestController
//@RequestMapping("/api/tmsc")
//public class TsmcController {
//    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
//
//    @Resource
//    private TsmcService tsmcService;
//
//    @RequestMapping(method = RequestMethod.POST, value = "/personnelNotify")
//    public ResponseEntity<String> capture(@RequestBody String body) {
//        LOGGER.info("[REQUEST] api/tmsc/personnelNotify :" + body);
//        String result = tsmcService.doPersonnelNotify(body);
//        LOGGER.info("[REPONSE] api/tmsc/personnelNotify :" + result);
//        return new ResponseEntity<String>(result, HttpStatus.OK);
//    }
//}
