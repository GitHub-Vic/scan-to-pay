package co.intella.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.intella.domain.CheckoutCtrlRequest;
import co.intella.model.RequestHeader;
import co.intella.service.CheckoutService;

/**
 * Scan2pay Api
 *
 * @author Bob
 */
@RestController
@RequestMapping("/api/checkout")
public class CheckoutController {

    private final Logger LOGGER = LoggerFactory.getLogger(CheckoutController.class);

    @Value("${api.url}")
    private String API_URL;

    @Resource
	private CheckoutService checkoutService;

    @RequestMapping(method = RequestMethod.GET, value ="/ezCardCheckout/{createDate}")
    public String ezCardCheckout(@PathVariable("createDate") String createDate, HttpServletRequest request) {
    	return checkoutMiddle("", "","", createDate);
    }
    
    @RequestMapping(method = RequestMethod.GET, value ="/ezCardCheckout/{accountId}/{createDate}")
    public String ezCardCheckout(@PathVariable("accountId") String accountId, @PathVariable("createDate") String createDate, HttpServletRequest request) {
    	return checkoutMiddle(accountId, "", "", createDate);
    }

    @RequestMapping(method = RequestMethod.GET, value ="/ezCardCheckout/{accountId}/{ownerDeviceId}/{createDate}")
    public String ezCardCheckout(@PathVariable("accountId") String accountId, @PathVariable("ownerDeviceId") String ownerDeviceId, @PathVariable("createDate") String createDate, HttpServletRequest request) {
    	return checkoutMiddle(accountId, ownerDeviceId, "", createDate);
    }

    @RequestMapping(method = RequestMethod.GET, value ="/ezCardCheckout/{accountId}/{ownerDeviceId}/{batchNo}/{createDate}")
    public String ezCardCheckout(@PathVariable("accountId") String accountId, @PathVariable("ownerDeviceId") String ownerDeviceId, @PathVariable("batchNo") String batchNo, @PathVariable("createDate") String createDate, HttpServletRequest request) {
    	return checkoutMiddle(accountId, ownerDeviceId, batchNo, createDate);
    }

	private String checkoutMiddle(String accountId, String ownerDeviceId, String batchNo, String createDate) {
		RequestHeader requestHeader = new RequestHeader();
    	LOGGER.info("[REQUEST][EZC] ezCardCheckout accountId= " + accountId + ", ownerDeviceId= " + ownerDeviceId + ", batchNo= " + batchNo + ", createDate= " + createDate);
    	requestHeader.setMerchantId(accountId);
    	requestHeader.setMethod("31800");
    	Calendar c = Calendar.getInstance();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    	try {
    		if (!createDate.equals("noDate")) {
    			c.setTime(sdf.parse(createDate));
    		}
		} catch (ParseException e) {
			LOGGER.error("[EXCEPTION] dateFormatWrong " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
			return "dateFormatWrong should be yyyyMMdd";
		}
		return checkoutService.checkout(accountId, ownerDeviceId, batchNo, requestHeader, c, createDate);
	}
    
    @RequestMapping(method = RequestMethod.GET, value="/checkoutFailSendMail")
    public String checkoutFailSendMail () {
    	return checkoutService.doSendFailEmail();
    }
    
    @PostMapping("/listByAccountId")
    public Map<String, Object> listByAccountId(@RequestBody CheckoutCtrlRequest request) {
    	return checkoutService.listByAccountId(request);
    }

}
