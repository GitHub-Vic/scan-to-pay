package co.intella.controller;

import co.intella.model.TradeDetail;
import co.intella.service.AppNotificationService;
import co.intella.service.MerchantService;
import co.intella.service.TradeDetailService;
import co.intella.utility.SystemInstance;

import javax.annotation.Resource;

import co.intella.service.RedirectService;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * @author Miles Wu
 */
@Controller
@RequestMapping("/api/alipay")
public class AliPayController {

    private final Logger LOGGER = LoggerFactory.getLogger(AliPayController.class);

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private RedirectService redirectService;

    @Value("${api.url}")
    private String API_URL;

    @RequestMapping(method = RequestMethod.GET, value = "/result")
    public ModelAndView getAliPayResult(@RequestParam(value = "STOREID") String storeId, @RequestParam(value = "ORDERID") String orderId, @RequestParam(value = "STATUS") String status, @RequestParam(value = "CCY") String ccy, @RequestParam(value = "AMT") String amt, @RequestParam(value = "SIGN") String sign, @RequestParam(value = "SIGN_CHARSET") String signCharset) {
        LOGGER.info("[REQUEST] /api/alipay/result " + storeId + " " + orderId + " " + status);
        TradeDetail tradeDetail = tradeDetailService.getOne(orderId);
        if (tradeDetail == null) {
            return null;
        }
        tradeDetail.setMethod("10220");
        if (status.equals("P")) {
            if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                tradeDetailService.save(tradeDetail);
                appNotificationService.notifyApp(tradeDetail.getAccountId(), orderId);
            }

            ModelAndView model = new ModelAndView();
            model.addObject("MerchantID", merchantService.getOne(tradeDetail.getAccountId()).getName());
            model.addObject("MerchantTradeNo", orderId);
            model.addObject("TradeAmt", amt);
            model.addObject("PaymentDate", DateTime.now().toString(SystemInstance.DATE_PATTERN));
            model.addObject("RtnCode", "0000");
            model.addObject("itemName", tradeDetail.getDescription());
            model.addObject("titleBarColor", "AlipayClient");
            model.addObject("domain", API_URL);
            model.setViewName("paymentResult");
            model = redirectService.confirm(model);
            return model;

        } else {
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            tradeDetailService.save(tradeDetail);
            ModelAndView model = new ModelAndView();
            model.addObject("MerchantID", merchantService.getOne(tradeDetail.getAccountId()).getName());
            model.addObject("MerchantTradeNo", orderId);
            model.addObject("TradeAmt", amt);
            model.addObject("PaymentDate", DateTime.now().toString(SystemInstance.DATE_PATTERN));
            model.addObject("RtnCode", "99");
            model.addObject("itemName", tradeDetail.getDescription());
            model.addObject("titleBarColor", "AlipayClient");
            model.addObject("domain", API_URL);
            model.setViewName("paymentResult");
            model = redirectService.confirm(model);
            return model;
        }
    }
}
