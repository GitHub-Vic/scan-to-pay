package co.intella.controller;

import co.intella.crypto.AesCryptoUtil;
import co.intella.crypto.KeyReader;
import co.intella.model.IntegratedRequest;
import co.intella.net.Constant;
import co.intella.net.HttpRequestUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.crypto.SecretKey;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.PublicKey;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Andy Lin
 */
@Controller
@RequestMapping("/api/healthcheck")
public class HealthCheckController {

    private final Logger LOGGER = LoggerFactory.getLogger(HealthCheckController.class);

    private final String intellaQRCodeURL = "https://a.intella.co/demo";
    private final String simulatedQRCodeURL = "https://a.intella.co/intella";
    private final String apiURL = "https://a.intella.co/allpaypass/api/general";

    @Value("${db.url}")
    private String dbURL;
    @Value("${db.username}")
    private String dbUsername;
    @Value("${db.password}")
    String dbPassword;

    private final String colorSuccess = "success";
    private final String colorWarning = "warning";
    private final String colorDanger = "danger";

    @RequestMapping(method= RequestMethod.GET, value="/list" )
    public ModelAndView showHealthStatus() throws Exception {
        ModelAndView model = new ModelAndView();
        Map<String,String> map = new HashMap<String, String>();

        qrcodeIntellaExamine(map);
        qrcodeSimulatedExamine(map);
        dbExamine(map);
        weixinExamine(map);
        allpayExamine(map);
        alipayExamine(map);

        model.addAllObjects(map);
        model.setViewName("healthCheck");
        LOGGER.info("done");
        return model;
    }

    private void dbExamine(Map<String, String> map) {
        LOGGER.info("[DB] testing ...");
        Connection conn = null;
        SQLException ex = null;
        try {
            conn = DriverManager.getConnection(dbURL, dbUsername, dbPassword);
            LOGGER.info(conn.getCatalog().toString());
        }
        catch(SQLException e) {
            ex = e;
        }
        finally {
            if(conn != null) {
                map.put("dbStatusTitle", "DB test");
                map.put("dbStatusText", "OK");
                map.put("dbStatusColor", colorSuccess);
                try {
                    conn.close();
                }
                catch(SQLException e) {
                    if(ex == null) {
                        ex = e;
                    }
                    else {
                    }
                }
            }
            if(ex != null) {
                map.put("dbStatusTitle", "DB test");
                map.put("dbStatusText", "maintaining");
                map.put("dbStatusColor", colorDanger);
            }
            LOGGER.info("[DB] testing done");
        }
    }

    private void allpayExamine(Map<String, String> map) throws Exception{
        LOGGER.info("[AllPay] testing ...");

        Map<String, String> requestData = new HashMap<String, String>();
        requestData.put("Method", "10500");
        requestData.put("ServiceType", "SingleOrderQuery");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170513080000");
        requestData.put("StoreOrderNo", "allpa0f4g3y007");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = getEncryptRequest(requestData, secretKey);
        String response = apiTest(encryptRequest.getRequestBody(), encryptRequest.getApiKey());

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, response);
        LOGGER.info("Decrypted Msg:" + decryptResponse);
//        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);

        if(response.substring(2,10).equals("Response")) {
            if(!decryptResponse.equals("")) {
                map.put("allpayStatusTitle", "AllPay");
                map.put("allpayStatusText", "OK");
                map.put("allpayStatusColor", colorSuccess);
            }else {
                map.put("allpayStatusTitle", "AllPay");
                map.put("allpayStatusText", "maintaining");
                map.put("allpayStatusColor", colorWarning);
            }
        }else {
            map.put("allpayStatusTitle", "AllPay");
            map.put("allpayStatusText", "maintaining");
            map.put("allpayStatusColor", colorDanger);
        }

        LOGGER.info("[AllPay] testing done");
    }

    private void weixinExamine(Map<String, String> map) throws Exception {
        LOGGER.info("[Weixin] testing ...");
        Map<String, String> requestData = new HashMap<String, String>();
        requestData.put("Method", "10110");
        requestData.put("ServiceType", "SingleOrderQuery");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170313080000");    // format must be yyyyMMddHHmmss
        requestData.put("StoreOrderNo", "BISNT507008D0000000003");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = getEncryptRequest(requestData, secretKey);
        String response = apiTest(encryptRequest.getRequestBody(), encryptRequest.getApiKey());

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, response);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);

        LOGGER.info("Decrypted Msg:" + decryptResponse);

        if(response.substring(2,10).equals("Response")) {
            if(!decryptResponse.equals("")) {
                map.put("weixinStatusTitle", "WeiXin");
                map.put("weixinStatusText", "OK");
                map.put("weixinStatusColor", colorSuccess);
            }else {
                map.put("weixinStatusTitle", "WeiXin");
                map.put("weixinStatusText", "maintaining");
                map.put("weixinStatusColor", colorWarning);
            }
        }else {
            map.put("weixinStatusTitle", "WeiXin");
            map.put("weixinStatusText", "maintaining");
            map.put("weixinStatusColor", colorDanger);
        }
        LOGGER.info("[Weixin] testing done");
    }

    private void alipayExamine(Map<String, String> map) throws Exception{
        LOGGER.info("[AliPay] testing ...");
        Map<String, String> requestData = new HashMap<String, String>();

        requestData.put("Method", "10220");
        requestData.put("ServiceType", "Query");
        requestData.put("MchId", "super");
        requestData.put("CreateTime", "20170419120000");    // format must be yyyyMMddHHmmss
        requestData.put("DeviceInfo", "23412342");
        requestData.put("StoreOrderNo", "AliTest20170419001");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = getEncryptRequest(requestData, secretKey);

        String response = apiTest(encryptRequest.getRequestBody(), encryptRequest.getApiKey());

        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, response);
        JsonObject json = new Gson().fromJson(decryptResponse, JsonObject.class);

        LOGGER.info("Decrypted Msg:" + decryptResponse);
        if(response.substring(2,10).equals("Response")) {
            if(!decryptResponse.equals("")) {
                map.put("alipayStatusTitle", "AliPay");
                map.put("alipayStatusText", "OK");
                map.put("alipayStatusColor", colorSuccess);
            }else {
                map.put("alipayStatusTitle", "AliPay");
                map.put("alipayStatusText", "maintaining");
                map.put("alipayStatusColor", colorWarning);
            }
        }else {
            map.put("alipayStatusTitle", "AliPay");
            map.put("alipayStatusText", "maintaining");
            map.put("alipayStatusColor", colorDanger);
        }
        LOGGER.info("[AliPay] testing done");
    }

    private String apiTest(String request, String apiKey) throws Exception{
        URL url = new URL(apiURL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setUseCaches(false);
//        conn.setInstanceFollowRedirects(true);
        conn.setRequestProperty("Content-Type","application/json");
        conn.setRequestProperty("Accept","application/json");

        JSONObject obj = new JSONObject();
        obj.put("Request",request);
        obj.put("ApiKey",apiKey);

        OutputStream out = conn.getOutputStream();
        out.write(obj.toString().getBytes());
        out.flush();
        out.close();

        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String readline = "";
        String response = "";
        while ((readline = br.readLine()) != null) {
            response += readline;
        }
        LOGGER.info(response);
        return response;
    }

    private void qrcodeIntellaExamine(Map<String,String> map) throws Exception {
        LOGGER.info("[" + intellaQRCodeURL + "] testing ...");
        URL url = new URL(intellaQRCodeURL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        String rspCode = String.valueOf(conn.getResponseCode());
        BufferedReader bf = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String readline = "";
        String response = "";
        while ((readline = bf.readLine()) != null) {
            response += readline;
        }

        boolean htmlContent = response.matches("(.*)<div class=\"keyboard\"(.*)");
        LOGGER.info(String.valueOf(rspCode));

        if("200".equals(rspCode)) {
            map.put("qrcodeIntellaStatusTitle", "QRcode Intella");
            map.put("qrcodeIntellaStatusText", " OK ");
            map.put("qrcodeIntellaStatusColor", colorSuccess);
        }else {
            map.put("qrcodeIntellaStatusTitle", "QRcode Intella");
            map.put("qrcodeIntellaStatusText", " maintaining");
            map.put("qrcodeIntellaStatusColor", colorDanger);
        }
        LOGGER.info("[" + intellaQRCodeURL + "] testing done");
    }

    private void qrcodeSimulatedExamine(Map<String,String> map) throws Exception {
        LOGGER.info("[" + simulatedQRCodeURL + "] testing ...");
        URL url = new URL(simulatedQRCodeURL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        String rspCode = String.valueOf(conn.getResponseCode());
        BufferedReader bf = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String readline = "";
        String response = "";
        while ((readline = bf.readLine()) != null) {
            response += readline;
        }

        boolean htmlContent = response.matches("(.*)<div class=\"keyboard\"(.*)");
        LOGGER.info(String.valueOf(rspCode));

        if("200".equals(rspCode)) {
            map.put("qrcodeSimulatedStatusTitle", "QRcode testing");
            map.put("qrcodeSimulatedStatusText", " OK ");
            map.put("qrcodeSimulatedStatusColor", colorSuccess);
        }else {
            map.put("qrcodeSimulatedStatusTitle", "QRcode testing");
            map.put("qrcodeSimulatedStatusText", " maintaining");
            map.put("qrcodeSimulatedStatusColor", colorDanger);
        }
        LOGGER.info("[" + simulatedQRCodeURL + "] testing done");
    }

    private IntegratedRequest getEncryptRequest(Map<String, String> requestData, SecretKey secretKey) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("key/pub.der").getFile());
        PublicKey publicKey = KeyReader.loadPublicKeyFromDER(file);
        String requestString = HttpRequestUtil.getEncryptRequestJson(requestData, publicKey, secretKey);
        return new Gson().fromJson(requestString, IntegratedRequest.class);
    }
}
