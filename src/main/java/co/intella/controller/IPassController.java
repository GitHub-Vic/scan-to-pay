package co.intella.controller;

import co.intella.model.INT_VW_IpassCheckOut;
import co.intella.service.IPassService;
import co.intella.service.IpassCheckOutService;
import co.intella.utility.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.ForkJoinPool;


@RestController
@RequestMapping("/api/iPass")
public class IPassController {

    private final Logger LOGGER = LoggerFactory.getLogger(IPassController.class);

    @Resource
    private IPassService iPassService;

    @Resource
    private IpassCheckOutService ipassCheckOutService;

    @RequestMapping(method = RequestMethod.GET, value = "/settlement/{mchAccount}/{deviceId}/{createDate}")
    public boolean settlement(@PathVariable("createDate") String createDate, @PathVariable("mchAccount") String mchAccount, @PathVariable("deviceId") String deviceId) {
        return this.settlementMain(createDate, "", deviceId, mchAccount);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/settlement/{mchAccount}/{deviceId}/{createDate}/{endDate}")
    public boolean settlement(@PathVariable("createDate") String createDate, @PathVariable("endDate") String endDate, @PathVariable("deviceId") String deviceId, @PathVariable("mchAccount") String mchAccount) {
        return this.settlementMain(createDate, endDate, deviceId, mchAccount);
    }

    private boolean settlementMain(String createDate, String endDate, String deviceId, String mchAccount) {
        LOGGER.info("[IPass][settlementMain] settlementMain deviceId = " + deviceId + " ,  mchAccount = " + mchAccount);
        LOGGER.info("[IPass][settlementMain] settlementMain createDate = " + createDate + " , endDate = " + endDate);
        boolean c = false;
        try {
            iPassService.settlementMain(createDate, endDate, deviceId, mchAccount);
            c = true;
        } catch (Exception e) {
            LOGGER.info("[Icash][settlementMain]" + e.getMessage());
        }
        return c;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/autoSettlement")
    public void autoSettlement() {
        List<INT_VW_IpassCheckOut> list = ipassCheckOutService.findAll();
        LOGGER.info("[IPass][autoSettlement]  list =>" + JsonUtil.toJsonStr(list));
        final String yesteray = LocalDate.now().minusDays(1).format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        LOGGER.info("[IPass][autoSettlement]  date =>" + yesteray);
        ForkJoinPool forkJoinPool = new ForkJoinPool(10);
        forkJoinPool.submit(() -> {
            list.parallelStream().forEach(int_vw_ipassCheckOut -> {
                boolean checkout = this.settlementMain(yesteray, "", int_vw_ipassCheckOut.getOwnerDeviceId(), int_vw_ipassCheckOut.getAccountId());
                LOGGER.info(String.format("[IPass][autoSettlement] OwnerDeviceId : %s , AccountId : %s , checkout %s",
                        int_vw_ipassCheckOut.getOwnerDeviceId(), int_vw_ipassCheckOut.getAccountId(), String.valueOf(checkout)));
            });
        });

    }


}
