package co.intella.controller;

import co.intella.exception.NoSuchMerchantException;
import co.intella.exception.OrderAlreadyExistedException;
import co.intella.exception.OtherAPIException;
import co.intella.model.QrcodeParameter;
import co.intella.service.MerchantService;
import co.intella.service.QrcodeService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;

/**
 * 原先Qrcode Controller 有設定訪問權限，所以直接另外開一個給樣板使用
 */

@RestController
@RequestMapping("/api/qrcodeTemplate")
public class QrcodeTemplateController {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Resource
    private QrcodeService qrcodeService;

    @Autowired
    private MessageSource messageSource;

    @Resource
    private MerchantService merchantService;

     private Locale locale = new Locale("zh_TW");


    @RequestMapping(method = RequestMethod.POST, value = "/save")
    public Map<String, String> savePost(@RequestBody QrcodeParameter qrcodeParameter) {

        QrcodeParameter _qrcodeParameter = null;

        String msgCode = "9995", shortId = "";
        try {
            _qrcodeParameter = qrcodeService.createQrcode(qrcodeParameter);
            if (Objects.nonNull(_qrcodeParameter)) {
                msgCode = "0000";
                shortId = _qrcodeParameter.getShortId();
            }
        } catch (NoSuchMerchantException e) {
            LOGGER.error("[savePost][NoSuchMerchantException]" + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            msgCode = "7132";
        } catch (OrderAlreadyExistedException e) {
            LOGGER.error("[savePost][OrderAlreadyExistedException]" + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            msgCode = "7000";
        } catch (Exception e) {
            LOGGER.error("[savePost][Exception]" + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
        }

        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("msgCode", msgCode);
        resultMap.put("msg", messageSource.getMessage("response." + msgCode, null, locale));
        resultMap.put("shortId", shortId);

        return resultMap;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/save")
    public Map<String, String> saveDelete(@RequestBody QrcodeParameter qrcodeParameter) {

        QrcodeParameter _qrcodeParameter = qrcodeService.getOriginOneByShortId(qrcodeParameter.getShortId());

        String msgCode = "0000";
        try {
            if (StringUtils.isBlank(qrcodeParameter.getShortId()) || Objects.isNull(_qrcodeParameter)) {
                throw new OtherAPIException(" qrcode not found. ");
            }
            qrcodeService.deleteQrcodeTemplate(_qrcodeParameter);
        } catch (Exception e) {
            LOGGER.error("[saveDelete][Exception]" + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            msgCode = "9995";
        }

        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("msgCode", msgCode);
        resultMap.put("msg", messageSource.getMessage("response." + msgCode, null, locale));

        return resultMap;
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/save")
    public Map<String, String> savePut(@RequestBody QrcodeParameter qrcodeParameter) {

        QrcodeParameter _qrcodeParameter = qrcodeService.getOneByShortIdAndMchId(qrcodeParameter.getMchId(), qrcodeParameter.getShortId());

        String msgCode = "0000", shortId = "";
        try {
            if (StringUtils.isBlank(qrcodeParameter.getShortId()) || Objects.isNull(_qrcodeParameter)) {
                throw new OtherAPIException(" qrcode not found. ");
            }
            _qrcodeParameter.setDetail(qrcodeParameter.getDetail());
            _qrcodeParameter.setBody(qrcodeParameter.getBody());
            _qrcodeParameter.setTotalFee(qrcodeParameter.getTotalFee());

            qrcodeService.save(_qrcodeParameter);
        } catch (Exception e) {
            LOGGER.error("[savePut][Exception]" + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            msgCode = "9995";
        }

        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("msgCode", msgCode);
        resultMap.put("msg", messageSource.getMessage("response." + msgCode, null, locale));

        return resultMap;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getOne/{shortId}")
    public QrcodeParameter getOne(@PathVariable String shortId) {

        QrcodeParameter _qrcodeParameter = null;

        try {
            _qrcodeParameter = qrcodeService.getOneByShortId(shortId);
            if (StringUtils.isBlank(shortId) || Objects.isNull(_qrcodeParameter)) {
                throw new OtherAPIException(" qrcode not found. ");
            }

        } catch (Exception e) {
            LOGGER.error("[getOne][Exception]" + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
        }
        return _qrcodeParameter;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getList/{accountId}")
    public List<QrcodeParameter> getList(@PathVariable String accountId) {

        List<QrcodeParameter> _qrcodeParameterList = null;

        try {
            if (Objects.isNull(merchantService.getOne(accountId))) {
                throw new NoSuchMerchantException("Merchant error");
            }

            _qrcodeParameterList = qrcodeService.qrcodeListByMchId(accountId);

        } catch (Exception e) {
            LOGGER.error("[getList][Exception]" + e.getMessage() + ", \n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
        }
        return _qrcodeParameterList;
    }
}
