package co.intella.controller.customize;

import co.intella.domain.mwd.MwdBill;
import co.intella.model.RefundDetail;
import co.intella.model.TradeDetail;
import co.intella.net.HttpRequestUtil;
import co.intella.service.RefundDetailService;
import co.intella.service.TradeDetailService;
import co.intella.utility.GeneralResponseMessageUtil;
import co.intella.utility.GeneralUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.assertj.core.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.util.*;

/**
 * Used by MWD to check their bill
 * @author Miles
 */
@Controller
@RequestMapping("/api/mwd/v1")
public class MwdController {

    private final Logger LOGGER = LoggerFactory.getLogger(MwdController.class);

    @Resource
    private TradeDetailService tradeDetailService;

    @Autowired
    private RefundDetailService refundDetailService;

    @Autowired
    private MessageSource messageSource;

    @Value("${host.pi.request.inapp}")
    private String PI_API_URL;

    private static final String MWD = "mwd";

    @RequestMapping(method= RequestMethod.GET, value="/bill/{dateFrom}/{dateTo}" )
    public ResponseEntity<String> bill(@PathVariable(value = "dateFrom") String dateFrom,@PathVariable(value = "dateTo") String dateTo) {
        try {
            LOGGER.info("[REQUEST][MWD][BILL] " + dateFrom + " ~ " + dateTo);
            List<TradeDetail> tradeDetails = tradeDetailService.listByMerchantIdAndDate(MWD, dateFrom + "000000", dateTo + "235959");

            if(tradeDetails == null || tradeDetails.isEmpty()) {
                return new ResponseEntity<String>("[]", HttpStatus.OK);
            }

            List<MwdBill> billList = convertData(tradeDetails);

            ObjectMapper objectMapper = new ObjectMapper();
            String result = objectMapper.writeValueAsString(billList);

            return new ResponseEntity<String>(result, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION][MWD][BILL] " + e.getMessage() + ", \n" + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("error"), HttpStatus.BAD_REQUEST);
        }
    }

    private List<MwdBill> convertData(List<TradeDetail> tradeDetails) throws Exception {

        List<MwdBill> billList = new ArrayList<MwdBill>();
        LOGGER.info("[REQUEST][MWD][BILL][size] " + billList.size());

        for (TradeDetail tradeDetail : tradeDetails) {
//            LOGGER.info("[REQUEST][MWD][BILL] " + tradeDetail.getOrderId());
            if(SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                MwdBill bill = new MwdBill();

                String methodName = GeneralUtil.getMethodName(tradeDetail.getMethod(), messageSource);

                bill.setMerchantId(tradeDetail.getAccountId());
                bill.setMethod(methodName);
                bill.setTradeType(methodName);
                bill.setTradeStatus(tradeDetail.getStatus());
                bill.setDeviceOs(tradeDetail.getDeviceOS());
                bill.setMoney(tradeDetail.getPayment());
                bill.setOrderId(tradeDetail.getOrderId());
                bill.setCreateDate(tradeDetail.getCreateDate());
                bill.setRefundStatus(tradeDetail.getRefundStatus());

                // storeInfo|type|name
                if(!Strings.isNullOrEmpty(tradeDetail.getStoreInfo())) {
                    String[] storeInfo = tradeDetail.getStoreInfo().split("\\|");

                    int count = 0;
                    for (String token : storeInfo) {
                        if(count == 0) {
                            bill.setStoreInfo(token);
                        }

                        if(count == 1) {
                            bill.setStoreType(token);
                        }

                        if(count == 2) {
                            bill.setStoreName(token);
                        }
                        count ++;
                    }
                }

                if(SystemInstance.TRADE_REFUND_SUCCESS.equals(tradeDetail.getRefundStatus())) {
                    RefundDetail refundDetail = refundDetailService.getOne(tradeDetail.getOrderId());

                    if(refundDetail != null) {

                        if(Strings.isNullOrEmpty(refundDetail.getPlatformRefundDate())) {
                            LOGGER.warn("[PATCH][Pi][Order] " + tradeDetail.getOrderId() + ". with empty platform refund date");
                            JsonObject json = getOrderFromPi(tradeDetail.getOrderId());
                            refundDetail.setPlatformRefundDate(json.get("refunded_at").getAsString());
                            refundDetailService.save(refundDetail);

                            bill.setRefundTime(json.get("refunded_at").getAsString());
                        } else {
                            bill.setRefundTime(convertDate(refundDetail.getPlatformRefundDate()));
                        }
                    }
                }

                if(Strings.isNullOrEmpty(tradeDetail.getPlatformPaidDate())) {
                    LOGGER.warn("[PATCH][Pi][Order] " + tradeDetail.getOrderId() + ". with empty platform trade date");
                    JsonObject json = getOrderFromPi(tradeDetail.getOrderId());
                    tradeDetail.setPlatformPaidDate(json.get("paid_at").getAsString());
                    tradeDetailService.save(tradeDetail);

                    bill.setTradeTime(json.get("paid_at").getAsString());
                } else {
                    bill.setTradeTime(convertDate(tradeDetail.getPlatformPaidDate()));
                }

                billList.add(bill);
            }
        }

        return billList;
    }

    private String convertDate(String date){
//        LOGGER.info("[REQUEST][MWD][BILL][DATE] " + date);
        String pre = date.substring(0, 10);
        String post = date.substring(10, 18);

        return pre + " " + post;
    }

    private JsonObject getOrderFromPi(String orderId) throws Exception {
        Map<String, String> requestMap = new HashMap<String, String>();
        requestMap.put("partner_id", "UE00000000000019");
        requestMap.put("partner_key", "KEYrn5iR9o8SXOmvWk7WhbggsGPR9JNZ");
        requestMap.put("partner_order_id", orderId);

        String piResponse = HttpRequestUtil.post(PI_API_URL + "payments/orders", new Gson().toJson(requestMap));
        LOGGER.info("[patchOrder][Pi] " + piResponse);
        return new Gson().fromJson(piResponse, JsonObject.class);
    }
}
