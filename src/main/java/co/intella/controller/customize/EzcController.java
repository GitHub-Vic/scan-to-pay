package co.intella.controller.customize;

import co.intella.domain.contratStore.DeviceVo;
import co.intella.model.*;
import co.intella.service.*;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author Alex
 */
@Controller
@RequestMapping("/api/web/ezc")
public class EzcController {
    private final Logger LOGGER = LoggerFactory.getLogger(AipeiController.class);

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private EzCardService ezCardService;

    @Resource
    private RedirectService redirectService;

    @Value("${api.url}")
    private String API_URL;

    @RequestMapping(method = RequestMethod.GET, value = "/payment")
    public ModelAndView ezcPay(@RequestParam(value = "amount") String amount, @RequestParam(value = "qrcode") String qrcode) {
        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(qrcode);

        Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());
        List<DeviceVo> deviceList = deviceService.listAllByMerchantAndType(merchant.getAccountId(), "ezc");
        DeviceVo device;
        if (deviceList.size()>1){
            device = deviceService.getOneByMerchantAndDeviceId("01304096",merchant.getAccountId());
        }
        else {
            device = deviceList.get(0);
        }
        LOGGER.info("[EZC][SignOn]");
        String signOnResult = singOn(qrcodeParameter,merchant,device);
        LOGGER.info("[EZC][SignOn][Result]"+signOnResult);
        if (!signOnResult.equals("0000")){
            return null;
        }
        String storeOrderNo = qrcodeService.getOrderId(qrcodeParameter);
        LOGGER.info("[EZC][payment]");

        String paymentResult = payment(storeOrderNo,merchant,device,amount);
        LOGGER.info("[EZC][payment][Result]"+paymentResult);


        ModelAndView model = new ModelAndView();
        model.setViewName("paymentResult");
        model.addObject("titleBarColor", "allPay");
        model.addObject("domain", API_URL);
        model.addObject("MerchantID", merchant.getName());
        model.addObject("MerchantTradeNo", storeOrderNo);
        model.addObject("TradeAmt", amount);
        model.addObject("PaymentDate", DateTime.now().toString(SystemInstance.DATE_PATTERN));
        if (paymentResult.equals("0000")){
            model.addObject("RtnCode", "00");
        }
        else {
            model.addObject("RtnCode", "9999");
        }
        model.addObject("itemName", qrcodeParameter.getBody());
        model = redirectService.confirm(model);
        return model;
    }

    private String payment(String storeOrderNo, Merchant merchant, DeviceVo device, String amount) {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "31800");
        requestData.put("ServiceType", "Payment");
        requestData.put("MchId", merchant.getAccountId());
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss

        requestData.put("Amount", amount);
        requestData.put("Retry", "0");
        requestData.put("DeviceId", device.getOwnerDeviceId());
        requestData.put("StoreOrderNo", storeOrderNo);

        IntegratedRequestBody body = new IntegratedRequestBody();
        String rawJson = (new Gson()).toJson(requestData);
        RequestHeader header = (RequestHeader)(new Gson()).fromJson(rawJson, RequestHeader.class);
        Map<String, String> dataMap = new HashMap();
        ArrayList<String> ignoreList = new ArrayList<String>() {
            {
                this.add("Method");
                this.add("ServiceType");
                this.add("MchId");
                this.add("CreateTime");
                this.add("TradeKey");
            }
        };
        Iterator var10 = requestData.keySet().iterator();

        String key;
        while(var10.hasNext()) {
            key = (String)var10.next();
            if(!ignoreList.contains(key)) {
                dataMap.put(key, requestData.get(key));
            }
        }

        body.setRequestData((new Gson()).toJson(dataMap));


        JsonObject requestData2 = new Gson().fromJson((new Gson()).toJson(dataMap), JsonObject.class);
        String generalResponseData = "";
        try {
            generalResponseData = ezCardService.ezRequest(header, requestData2, merchant);
        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonObject jsonObject = new Gson().fromJson(generalResponseData,JsonObject.class);

        return jsonObject.get("Header").getAsJsonObject().get("StatusCode").getAsString();
    }

    private String singOn(QrcodeParameter qrcodeParameter, Merchant merchant, DeviceVo device) {
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("Method", "31800");
        requestData.put("ServiceType", "SignOn");
        requestData.put("MchId", qrcodeParameter.getMchId());
        requestData.put("TradeKey", "0000");
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));    // format must be yyyyMMddHHmmss

        requestData.put("DeviceId", device.getOwnerDeviceId());

        IntegratedRequestBody body = new IntegratedRequestBody();
        String rawJson = (new Gson()).toJson(requestData);
        RequestHeader header = (RequestHeader)(new Gson()).fromJson(rawJson, RequestHeader.class);
        Map<String, String> dataMap = new HashMap();
        ArrayList<String> ignoreList = new ArrayList<String>() {
            {
                this.add("Method");
                this.add("ServiceType");
                this.add("MchId");
                this.add("CreateTime");
                this.add("TradeKey");
            }
        };
        Iterator var10 = requestData.keySet().iterator();

        String key;
        while(var10.hasNext()) {
            key = (String)var10.next();
            if(!ignoreList.contains(key)) {
                dataMap.put(key, requestData.get(key));
            }
        }

        body.setRequestData((new Gson()).toJson(dataMap));


        JsonObject requestData2 = new Gson().fromJson((new Gson()).toJson(dataMap), JsonObject.class);
        String generalResponseData = "";
        try {
            generalResponseData = ezCardService.ezRequest(header, requestData2, merchant);
        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonObject jsonObject = new Gson().fromJson(generalResponseData,JsonObject.class);

        return jsonObject.get("Header").getAsJsonObject().get("StatusCode").getAsString();
    }
}