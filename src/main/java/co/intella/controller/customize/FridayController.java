package co.intella.controller.customize;

import co.intella.crypto.KeyReader;
import co.intella.domain.friDay.FridayCreateOrderResponseData;
import co.intella.domain.friDay.FridayNotifyRequestData;
import co.intella.domain.pi.PiErrorResponseData;
import co.intella.domain.pi.PiNotifyRequestData;
import co.intella.model.*;
import co.intella.service.*;
import co.intella.utility.OnlinepayUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.security.PrivateKey;
import java.util.Arrays;
import java.util.HashMap;

import static co.intella.crypto.RsaCryptoUtil.decrypt;

/**
 * @author Alex
 */
@Controller
@RequestMapping("/api/friDay")
public class FridayController {

    private final Logger LOGGER = LoggerFactory.getLogger(FridayController.class);


    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private DeviceService deviceService;

    @Autowired
    private Environment env;

    @Resource
    private FridayService fridayService;

    @Resource
    private OnlinepayUtil OnlinepayUtil;

    @RequestMapping(method = RequestMethod.POST, value = "/createOrder")
    public ResponseEntity<String> createOrder(@RequestBody String encryptedBody, HttpServletRequest request) throws Exception {

        LOGGER.info("[REQUEST][friDay] " + request.getHeader("X-FORWARDED-FOR"));

        String plainStr = decryptRSA(encryptedBody);

        LOGGER.info("[REQUEST][friDay] createOrder() : " + plainStr);

        HashMap<String, String> requestData = new ObjectMapper().readValue(plainStr, HashMap.class);

        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(requestData.get("token"));

        if (qrcodeParameter == null) {

            LOGGER.error("[REQUEST][friDay] createOrder() qrcode not found. " + requestData.get("token"));
            String errorJson = new Gson().toJson(getErrorMessage("0202", "qrcode not found", requestData.get("token")));
            return new ResponseEntity<String>(errorJson, HttpStatus.BAD_REQUEST);
        }

        Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());
        if (merchant == null) {
            LOGGER.error("[REQUEST][friDay] createOrder() merchant not found. " + requestData.get("token"));
            String errorJson = new Gson().toJson(getErrorMessage("0001", "Merchant Unavailable", requestData.get("token")));
            return new ResponseEntity<String>(errorJson, HttpStatus.BAD_REQUEST);
        }

        PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 23);
        if (paymentAccount == null) {
            LOGGER.error("[REQUEST][friDay] createOrder() payment account not found. " + requestData.get("token"));
            String errorJson = new Gson().toJson(getErrorMessage("0002", "Do Not Support", requestData.get("token")));
            return new ResponseEntity<String>(errorJson, HttpStatus.BAD_REQUEST);
        }

        Device device = deviceService.getOne(merchant, "APP-001");
        if (device == null) {
            LOGGER.error("[REQUEST][friDay] createOrder() device not found. " + requestData.get("token"));
            String errorJson = new Gson().toJson(getErrorMessage("9998", "device not found", requestData.get("token")));
            return new ResponseEntity<String>(errorJson, HttpStatus.BAD_REQUEST);
        }

        int amount;
        if (qrcodeParameter.getCodeType().equals("3")) {
            amount = 0;
        } else {
            amount = new Double(OnlinepayUtil.getTradeFee(qrcodeParameter, "12300")).intValue();
        }

        String randomBody = qrcodeService.getRandomBody(qrcodeParameter);

        String orderId = qrcodeService.getOrderId(qrcodeParameter);

        FridayCreateOrderResponseData responseData = new FridayCreateOrderResponseData();
        responseData.setErrorCode("0000");
        responseData.setStoreId(paymentAccount.getAppId());
        responseData.setStoreName(merchant.getName());
        responseData.setOrderId(orderId);
        responseData.setAmount(String.valueOf(amount));
        responseData.setChannelId(paymentAccount.getAccount());
        responseData.setPartnerId(paymentAccount.getPlatformCheckCode());
        responseData.setProductId(paymentAccount.getKeyFilePath());
        responseData.setWalletStoreId(paymentAccount.getHashIV());


        try {
            LOGGER.info("[createOrder][responseData]" + new ObjectMapper().writeValueAsString(responseData));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        TradeDetail tradeDetail = new TradeDetail();
        tradeDetail.setStatus(SystemInstance.TRADE_CREATING_ORDER);
        tradeDetail.setOrderId(orderId);
        tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        tradeDetail.setAccountId(merchant.getAccountId());
        tradeDetail.setPayment(amount);
        tradeDetail.setDeviceRandomId(device);
        tradeDetail.setPaymentAccount(paymentAccount);
        tradeDetail.setMethod("12300");
        tradeDetail.setServiceType(SystemInstance.TYPE_OLPAY);
        tradeDetail.setDescription(randomBody);
        TradeDetail entity = tradeDetailService.save(tradeDetail);


        try {
            if (entity == null) {
                LOGGER.error("[ERROR] createOrder() failed. " + new Gson().toJson(requestData));
                String errorJson = new Gson().toJson(getErrorMessage("9997", "create order failed.", requestData.get("token")));
                return new ResponseEntity<String>(errorJson, HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(responseData), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            String errorJson = new Gson().toJson(getErrorMessage("9999", "internal_error", ""));
            return new ResponseEntity<String>(errorJson, HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

    @RequestMapping(method = RequestMethod.POST, value = "/notify")
    public ResponseEntity<String> notify(@RequestBody String encryptedBody, HttpServletRequest request) throws JsonProcessingException {

        LOGGER.info("[REQUEST][friDay][notify]" + request.getHeader("X-FORWARDED-FOR"));

        FridayCreateOrderResponseData responseData = new FridayCreateOrderResponseData();

        String plainStr;
        try {
            plainStr = decryptRSA(encryptedBody);
        } catch (Exception e) {
            e.printStackTrace();
            responseData.setErrorCode("8003");
            return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(responseData), HttpStatus.BAD_REQUEST);

        }

        LOGGER.info("[REQUEST][friDay] notify() : " + plainStr);

        FridayNotifyRequestData requestData = null;

        requestData = new Gson().fromJson(plainStr, FridayNotifyRequestData.class);


        if (fridayService.updateNotify(requestData)) {
            LOGGER.info("[friDay][notify][success]");
            responseData.setErrorCode("0000");
            return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(responseData), HttpStatus.OK);
        } else {
            LOGGER.info("[friDay][notify][fail]");
            responseData.setErrorCode("9999");
            return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(responseData), HttpStatus.BAD_REQUEST);
        }


    }

    private String decryptRSA(String encryptApiKey) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(env.getProperty("friday2intella.private.key")).getFile());

        PrivateKey privateKey = KeyReader.loadPrivateKeyFromDER(file);
        byte[] byteKeys = Base64.decode(encryptApiKey);
        return fridayService.RSADecrypt(byteKeys, privateKey);
    }

    private PiErrorResponseData getErrorMessage(String errorCode, String message, String qrCode) {
        PiErrorResponseData errorMessage = new PiErrorResponseData();
        errorMessage.setErrorCode(errorCode);
        errorMessage.setErrorMessage(message);
        errorMessage.setQrCode(qrCode);
        errorMessage.setPaymentGateway("friDay_wallet");
        return errorMessage;
    }
}
