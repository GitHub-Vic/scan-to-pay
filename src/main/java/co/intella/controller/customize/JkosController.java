package co.intella.controller.customize;

import co.intella.domain.jkos.JkosEntryResponseData;
import co.intella.model.INT_TB_CancelTradeDetail;
import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import co.intella.service.*;
import co.intella.utility.JsonUtil;
import co.intella.utility.SystemInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Alex
 */

@Controller
@RequestMapping("/api/jkos")
public class JkosController {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Resource
    private JkosService jkosService;

    @Autowired
    private INT_TB_CancelTradeDetailService intTbCancelTradeDetailService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private PaymentAccountService paymentAccountService;


    @Resource
    private MerchantService merchantService;

    @Value("${api.url}")
    private String API_URL;

    @RequestMapping(method = RequestMethod.GET, value = "/cancel")
    @ResponseBody
    public ResponseEntity<Void> backUrl(HttpServletRequest request, HttpServletResponse response) throws Exception {

        List<INT_TB_CancelTradeDetail> list = intTbCancelTradeDetailService.getFailList();
        list.forEach(intTbCancelTradeDetail -> {
            TradeDetail tradeDetail = tradeDetailService.getOne(intTbCancelTradeDetail.getOrderId());
            try {
                jkosService.doCancel(tradeDetail);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        return ResponseEntity.ok().build();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{urlToken}/{amt}")
    public ResponseEntity<String> onlinePay(@PathVariable String urlToken, @PathVariable String amt) throws Exception {

        LOGGER.info("[REQUEST] /api/jkospay" + urlToken + amt);

        String response = jkosService.getPaymentUrl(amt, urlToken, "");
        JkosEntryResponseData jkosEntryResponseData = JsonUtil.parseJson(response, JkosEntryResponseData.class);

        try {
            return new ResponseEntity<String>(jkosEntryResponseData.getResult_object().getPayment_url(), HttpStatus.OK);
        } catch (NullPointerException e) {
            return new ResponseEntity<String>(API_URL + "allpaypass/api/test/invalidQrcode", HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/notify")
    public ResponseEntity<String> notify(@RequestBody String request) {
        LOGGER.info("[Jkos][REQUEST] /notify :" + request);
        boolean statusFlag = false;

        Map<String, String> reqMap = JsonUtil.parseJson(request, Map.class);
        Map<String, String> transactionMap = JsonUtil.parseJson(JsonUtil.toJsonStr(reqMap.get("transaction")), Map.class);

        LOGGER.info("[Jkos][REQUEST] /notify Json Data:" + transactionMap);
        TradeDetail tradeDetail = tradeDetailService.getOne(transactionMap.get("platform_order_id"));
        if (Objects.isNull(tradeDetail)) {
            LOGGER.error("[Jkos][REQUEST] /notify tradedetail is not found.");
        } else {
            tradeDetail.setMethod("13600");
            PaymentAccount paymentAccount = paymentAccountService.getOne(tradeDetail.getAccountId(), 36);
            tradeDetail.setPaymentAccount(paymentAccount);
            tradeDetail.setTxParams(request);

            String status = String.valueOf(transactionMap.get("status"));
            if ("0".equals(status) && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
            } else if (("101".equals(status) || "1".equals(status))
                    && (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus()))) {
                //  此訂單號尚未付款 or 付款處理中
                tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
            } else if ("102".equals(status) && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {   //此訂單號不存在

                tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            }

            tradeDetail.setSystemOrderId(transactionMap.get("tradeNo"));
            if (Objects.isNull(tradeDetailService.save(tradeDetail))) {
                LOGGER.error("[Jkos] notify save tradeDetail fail.");
            } else {
                LOGGER.info("[Jkos] notify save tradeDetail succes.");
                statusFlag = true;
            }
        }

        return new ResponseEntity<String>("", statusFlag ? HttpStatus.OK : HttpStatus.BAD_REQUEST);

    }
}
