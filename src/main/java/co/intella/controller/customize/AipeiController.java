package co.intella.controller.customize;

import co.intella.domain.aipei.AipeiGetOrderDetail;
import co.intella.domain.aipei.AipeiOrder;
import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import co.intella.net.HttpRequestUtil;
import co.intella.service.*;
import co.intella.utility.GeneralResponseMessageUtil;
import co.intella.utility.GeneralUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.bouncycastle.jce.provider.symmetric.Grain128Mappings;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author Alex
 */
@Controller
@RequestMapping("/api/aipei")
public class AipeiController {

    private final Logger LOGGER = LoggerFactory.getLogger(AipeiController.class);

    @Resource
    private QrcodeService qrcodeService;

    @Autowired
    private MessageSource messageSource;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private DeviceService deviceService;

    private static String localUrl = "http://localhost:8080/scan2pay-data/";

     private Locale locale = new Locale("zh_TW");

    @RequestMapping(method = RequestMethod.GET, value = "/{urlToken}")
    public ResponseEntity<String> onlinePay(@PathVariable String urlToken){

        return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/get_order_details")
    public ResponseEntity<String> onlinePay(@RequestParam(value = "app_key") String appKey, @RequestParam(value = "order_id") String orderId) {

        TradeDetail tradeDetail;
        try {
            String responseEntity = HttpRequestUtil.httpGet(localUrl + "api/trade/" + orderId);
            tradeDetail = new Gson().fromJson(GeneralUtil.getMessage(responseEntity), TradeDetail.class);
        } catch (Exception e) {
            e.printStackTrace();
            tradeDetail = null;
        }


        PaymentAccount paymentAccount = null;

        AipeiGetOrderDetail aipeiGetOrderDetail = new AipeiGetOrderDetail();
        AipeiOrder aipeiOrder = new AipeiOrder();


        try {
            String responseEntity = HttpRequestUtil.httpGet(localUrl + "api/paymentaccount/getByAppKey/" + appKey + "/" + 14);
            paymentAccount = new Gson().fromJson(GeneralUtil.getMessage(responseEntity), PaymentAccount.class);
            if (paymentAccount == null) throw new Exception();

        } catch (Exception e) {
            e.printStackTrace();
        }
        aipeiOrder.setAppId(paymentAccount.getAppId());
        aipeiOrder.setOrderId(orderId);
        aipeiOrder.setCurrency("TWD");
        aipeiOrder.setPayableAmount((int) tradeDetail.getPayment());
        aipeiOrder.setExpiryTime((int) (DateTime.now().plusMinutes(10).getMillis() / 1000));
        aipeiOrder.setItemName(tradeDetail.getDescription() == null ? paymentAccount.getDescription() : tradeDetail.getDescription());


        aipeiGetOrderDetail.setOrder(aipeiOrder);
        aipeiGetOrderDetail.setOrderValidity(200);

        String ret = SystemInstance.EMPTY_STRING;
        try {
            ret = new ObjectMapper().writeValueAsString(aipeiGetOrderDetail);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        LOGGER.info("[Aipei][getOrderDetail]" + ret);
        return new ResponseEntity<String>(ret, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.POST, value = "/validate_order")
    public ResponseEntity<String> validateOrder(@RequestParam(value = "app_key") String appKey, @RequestParam(value = "order_id") String orderId) {

        TradeDetail tradeDetail;
        try {
            String responseEntity = HttpRequestUtil.httpGet(localUrl + "api/trade/" + orderId);
            tradeDetail = new Gson().fromJson(GeneralUtil.getMessage(responseEntity), TradeDetail.class);
        } catch (Exception e) {
            e.printStackTrace();
            tradeDetail = null;
        }


        PaymentAccount paymentAccount = null;

        AipeiGetOrderDetail aipeiGetOrderDetail = new AipeiGetOrderDetail();


        try {
            String responseEntity = HttpRequestUtil.httpGet(localUrl + "api/paymentaccount/getByAppKey/" + appKey + "/" + 14);
            paymentAccount = new Gson().fromJson(GeneralUtil.getMessage(responseEntity), PaymentAccount.class);
            if (paymentAccount == null) throw new Exception();

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (tradeDetail == null || paymentAccount == null) {
            aipeiGetOrderDetail.setOrderValidity(400);
        } else {
            aipeiGetOrderDetail.setOrderValidity(200);
        }

        String ret = SystemInstance.EMPTY_STRING;
        try {
            ret = new ObjectMapper().writeValueAsString(aipeiGetOrderDetail);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        LOGGER.info("[Aipei][validateOrder]" + ret);
        return new ResponseEntity<String>(ret, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/notify_order_status")
    public ResponseEntity<String> notifyOrderStatus(@RequestParam(value = "app_key") String appKey,
                                                    @RequestParam(value = "order_id") String orderId,
                                                    @RequestParam(value = "order_status") Integer orderStatus,
                                                    @RequestParam(value = "payment_id") String paymentId) {
        //todo black bug
        LOGGER.info("[Aipei][validateOrder]");
        Map<String, String> map = new HashMap<String, String>();
        map.put("order_id", orderId);

        if (orderStatus==200) {
            map.put("msg", SystemInstance.TRADE_SUCCESS);
        }
        else {
            map.put("msg", SystemInstance.TRADE_FAIL);
        }
        return new ResponseEntity<String>(new Gson().toJson(map), HttpStatus.OK);

    }
}