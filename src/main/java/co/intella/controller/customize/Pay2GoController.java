package co.intella.controller.customize;

import co.intella.crypto.AesCryptoUtil;
import co.intella.crypto.KeyReader;
import co.intella.domain.friDay.FridayCreateOrderResponseData;
import co.intella.domain.pi.PiErrorResponseData;
import co.intella.model.*;
import co.intella.service.*;
import co.intella.utility.GeneralUtil;
import co.intella.utility.OnlinepayUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.crypto.SecretKey;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.PrivateKey;
import java.util.*;

import static co.intella.crypto.RsaCryptoUtil.decrypt;

/**
 * @author Alex
 */
@Controller
@RequestMapping("/api/Pay2Go")
public class Pay2GoController {
    private final Logger LOGGER = LoggerFactory.getLogger(Pay2GoController.class);

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private OrderService orderService;

    @Autowired
    private Environment env;

    @Resource
    private Pay2GoService pay2GoService;

    @Resource
    private OnlinepayUtil OnlinepayUtil;

    @RequestMapping(method = RequestMethod.POST, value = "/getPay2GoUrl")
    public ResponseEntity<String> getPay2GoUrl(@RequestBody String input) {

        String android = "intent://www.ezpay.com/newTaipeiCity/#Intent;";
        String ios = "ezpay://www.pay2go.com/newTaipeiCity/index";
        String version = "1.0";

        LOGGER.info("[getPay2GoUrl]:" + input);
//
        JsonObject jsonObject = new Gson().fromJson(input, JsonObject.class);
        String totalFee = jsonObject.get("amount").getAsString();
        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(jsonObject.get("shortId").getAsString());
//
        try {
            LOGGER.info("[QrcodeParameter]:" + new ObjectMapper().writeValueAsString(qrcodeParameter));

            if (qrcodeParameter == null) {
                return null;
            }

            String storeOrderNoNew;

            if (qrcodeParameter.getCodeType().equals("1")) {
                storeOrderNoNew = qrcodeParameter.getStoreOrderNo();
                totalFee = Double.toString(qrcodeParameter.getTotalFee());
            } else {
                storeOrderNoNew = orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
            }

            Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());

            PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 24);

            // add last 4 code
            String body = qrcodeService.getRandomBody(qrcodeParameter);
            qrcodeParameter.setBody(body);

            HashMap<String, String> tradeMap = new LinkedHashMap<String, String>();
            tradeMap.put("TimeStamp", String.valueOf(DateTime.now().getMillis()));
            tradeMap.put("Version", version);
            tradeMap.put("storeId", paymentAccount.getAccount());
            tradeMap.put("storeName", URLEncoder.encode(merchant.getName(), "UTF-8"));
            tradeMap.put("orderId", storeOrderNoNew);
            tradeMap.put("amount", totalFee);

            LOGGER.info("[Pay2Go][trade Data]" + new ObjectMapper().writeValueAsString(tradeMap));

            SecretKey secretKey = AesCryptoUtil.convertSecretKey(paymentAccount.getHashKey().getBytes());
            String iv = paymentAccount.getHashIV();
            String postData = GeneralUtil.mapToUrlQuery(tradeMap);

            LOGGER.info("[Pay2Go][trade Data]" + postData);

            String tradeInfo = new BigInteger(AesCryptoUtil.encrypt(secretKey, iv.getBytes(), postData)).toString(16);
            String tradeSha = GeneralUtil
                    .getHash("HashKey=" + paymentAccount.getHashKey() + "&" + tradeInfo + "&HashIV=" + iv).toUpperCase();

            // Android
            HashMap<String, String> androidParam = new HashMap<String, String>();
            androidParam.put("scheme", "ezpay");
            androidParam.put("package", "com.pay2go.pay2go_app");
            androidParam.put("S.MerchantID", paymentAccount.getAccount());
            androidParam.put("S.Version", version);
            androidParam.put("S.TradeInfo", tradeInfo);
            androidParam.put("S.TradeSha", tradeSha);

            StringBuilder sb = new StringBuilder();
            sb.append(android);
            for (Map.Entry<String, String> entry : androidParam.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                sb.append(key).append("=").append(value).append(";");
            }
            String androidData = sb.append("end").toString();

            // IOS
            HashMap<String, String> iosParam = new HashMap<String, String>();
            iosParam.put("MerchantID", String.valueOf(DateTime.now().getMillis()));
            iosParam.put("Version", version);
            iosParam.put("TradeInfo", tradeInfo);
            iosParam.put("TradeSha", tradeSha);

            String iosData = ios + GeneralUtil.mapToUrlQuery(iosParam);
//			
            HashMap<String, String> result = new HashMap<>();
            result.put("android", androidData);
            result.put("ios", iosData);

            LOGGER.info("[Pay2Go][reserve url result]" + result);

            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);

            return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(result), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/createOrder")
    public ResponseEntity<String> createOrder(@RequestBody String encryptedBody, HttpServletRequest request)
            throws Exception {

        LOGGER.info("[REQUEST][Pay2Go] " + request.getHeader("X-FORWARDED-FOR"));

        String plainStr;
        try {
            plainStr = decryptRSA(encryptedBody);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            String errorJson = new Gson().toJson(getErrorMessage("8003", "decrypt error", ""));
            return new ResponseEntity<String>(errorJson, HttpStatus.BAD_REQUEST);
        }

        LOGGER.info("[REQUEST][Pay2Go] createOrder() : " + plainStr);

        HashMap<String, String> requestData = new ObjectMapper().readValue(plainStr, HashMap.class);

        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(requestData.get("token"));

        if (qrcodeParameter == null) {

            LOGGER.error("[REQUEST][Pay2Go] createOrder() qrcode not found. " + requestData.get("token"));
            String errorJson = new Gson().toJson(getErrorMessage("0202", "qrcode not found", requestData.get("token")));
            return new ResponseEntity<String>(errorJson, HttpStatus.BAD_REQUEST);
        }

        Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());
        if (merchant == null) {
            LOGGER.error("[REQUEST][Pay2Go] createOrder() merchant not found. " + requestData.get("token"));
            String errorJson = new Gson()
                    .toJson(getErrorMessage("0001", "Merchant Unavailable", requestData.get("token")));
            return new ResponseEntity<String>(errorJson, HttpStatus.BAD_REQUEST);
        }

        PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 24);
        if (paymentAccount == null) {
            LOGGER.error("[REQUEST][Pay2Go] createOrder() payment account not found. " + requestData.get("token"));
            String errorJson = new Gson().toJson(getErrorMessage("0002", "Do Not Support", requestData.get("token")));
            return new ResponseEntity<String>(errorJson, HttpStatus.BAD_REQUEST);
        }

        Device device = deviceService.getOne(merchant, "APP-001");
        if (device == null) {
            LOGGER.error("[REQUEST][Pay2Go] createOrder() device not found. " + requestData.get("token"));
            String errorJson = new Gson().toJson(getErrorMessage("9998", "device not found", requestData.get("token")));
            return new ResponseEntity<String>(errorJson, HttpStatus.BAD_REQUEST);
        }

        int amount;
        if (qrcodeParameter.getCodeType().equals("3")) {
            amount = 0;
        } else {
            amount = new Double(OnlinepayUtil.getTradeFee(qrcodeParameter, "12400")).intValue();
        }

        String randomBody = qrcodeService.getRandomBody(qrcodeParameter);

        String orderId = qrcodeService.getOrderId(qrcodeParameter);

        FridayCreateOrderResponseData responseData = new FridayCreateOrderResponseData();
        responseData.setErrorCode("0000");
        responseData.setStoreId(paymentAccount.getAccount());
        responseData.setStoreName(merchant.getName());
        responseData.setOrderId(orderId);
        responseData.setAmount(String.valueOf(amount));

        try {
            LOGGER.info("[createOrder][responseData]" + new ObjectMapper().writeValueAsString(responseData));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        TradeDetail tradeDetail = new TradeDetail();
        tradeDetail.setStatus(SystemInstance.TRADE_CREATING_ORDER);
        tradeDetail.setOrderId(orderId);
        tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        tradeDetail.setAccountId(merchant.getAccountId());
        tradeDetail.setPayment(amount);
        tradeDetail.setDeviceRandomId(device);
        tradeDetail.setPaymentAccount(paymentAccount);
        tradeDetail.setMethod("12400");
        tradeDetail.setServiceType(SystemInstance.TYPE_OLPAY);
        tradeDetail.setDescription(randomBody);
        TradeDetail entity = tradeDetailService.save(tradeDetail);

        try {
            if (entity == null) {
                LOGGER.error("[ERROR] createOrder() failed. " + new Gson().toJson(requestData));
                String errorJson = new Gson()
                        .toJson(getErrorMessage("9997", "create order failed.", requestData.get("token")));
                return new ResponseEntity<String>(errorJson, HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(responseData), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            String errorJson = new Gson().toJson(getErrorMessage("9999", "internal_error", ""));
            return new ResponseEntity<String>(errorJson, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @RequestMapping(method = RequestMethod.POST, value = "/notify")
    public ResponseEntity<String> notify(@RequestBody String requestJson, HttpServletRequest request)
            throws JsonProcessingException {

        LOGGER.info("[REQUEST][Pay2Go][Notify] : " + request.getHeader("X-FORWARDED-FOR") + " , " + requestJson);

        FridayCreateOrderResponseData responseData = new FridayCreateOrderResponseData();

        HashMap<String, String> requestBody = null;
        try {
            requestBody = new ObjectMapper().readValue(requestJson, HashMap.class);
        } catch (IOException e) {
            e.printStackTrace();
            responseData.setErrorCode("8003");
            return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(responseData),
                    HttpStatus.BAD_REQUEST);

        }

        List<PaymentAccount> paymentAccounts = paymentAccountService.listByPayAccount(requestBody.get("MerchantID"));
        PaymentAccount paymentAccount = paymentAccounts.get(0);

        if (!pay2GoService.verify(requestBody, paymentAccount)) {
            responseData.setErrorCode("8004");
            return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(responseData),
                    HttpStatus.BAD_REQUEST);
        }

        try {
            pay2GoService.updateNotify(requestBody, paymentAccount);
        } catch (Exception e) {
            e.printStackTrace();
            responseData.setErrorCode("9995");
            return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(responseData),
                    HttpStatus.BAD_REQUEST);
        }

        responseData.setErrorCode("0000");
        return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(responseData), HttpStatus.OK);

    }

    private String decryptRSA(String encryptApiKey) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(env.getProperty("pay2go2intella.private.key")).getFile());

        PrivateKey privateKey = KeyReader.loadPrivateKeyFromDER(file);
        byte[] byteKeys = Base64.decode(encryptApiKey);
        return decrypt(byteKeys, privateKey);
    }

    private PiErrorResponseData getErrorMessage(String errorCode, String message, String qrCode) {
        PiErrorResponseData errorMessage = new PiErrorResponseData();
        errorMessage.setErrorCode(errorCode);
        errorMessage.setErrorMessage(message);
        errorMessage.setQrCode(qrCode);
        errorMessage.setPaymentGateway("Pay2Go");
        return errorMessage;
    }
}
