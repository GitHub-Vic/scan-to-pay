package co.intella.controller.customize;

import co.intella.service.HNTwpayService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


@RestController
@RequestMapping("/api/hnTwPay")
public class HNTwpayController {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Resource
    private HNTwpayService HNTwpayService;

    @RequestMapping(method = RequestMethod.GET, value = "/{urlToken}/{amt}")
    public ResponseEntity<String> onlinePay(@PathVariable String urlToken, @PathVariable String amt) throws Exception {
        LOGGER.info("[REQUEST] /api/hnTwPay" + urlToken + amt);
        String response = HNTwpayService.getPaymentUrl(amt, urlToken, "");
        return new ResponseEntity<String>(response, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.POST, value = "/notify")
    public ResponseEntity<String> notify(@RequestBody String body) throws Exception {
        LOGGER.info("[REQUEST] /api/hnTwPay/notify => " + body);
//        String response = HNTwpayService.getPaymentUrl(amt, urlToken, "");
        return new ResponseEntity<String>("", HttpStatus.OK);
    }

}
