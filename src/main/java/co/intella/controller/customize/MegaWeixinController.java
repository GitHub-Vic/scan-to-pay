package co.intella.controller.customize;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;

import co.intella.domain.megaweixin.OrderPaymentJson;
import co.intella.service.MegaWeiXinService;

@Controller
@RequestMapping("/api/megaweixin")
public class MegaWeixinController {
	
    private final Logger LOGGER = LoggerFactory.getLogger(MegaWeixinController.class);

    @Resource
    private MegaWeiXinService megaWeiXinService;
	  
	//notify
	@RequestMapping(method = RequestMethod.POST, value = "/notify")
    public ResponseEntity<String> notify(@RequestBody String json, HttpServletResponse response) throws Exception {
	    LOGGER.info("[notify] " + json);
	    String result = "";
	    try {
	    	OrderPaymentJson orderPaymentJson = new Gson().fromJson(json, OrderPaymentJson.class);
	    	result = megaWeiXinService.doNotify(orderPaymentJson);
	    } catch(Exception e) {
	        LOGGER.info("[MegaWeixin][notify][Exception] " + e.getMessage());
	    }
        LOGGER.info("[RESPONSE][MegaWeixin][Response] " + result);
        return new ResponseEntity<String>(new Gson().toJson(result), HttpStatus.OK);
	}

}
