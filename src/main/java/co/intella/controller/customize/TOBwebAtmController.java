package co.intella.controller.customize;



import co.intella.service.*;
import co.intella.utility.EncryptUtil;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Controller
@RequestMapping("/api/TOBwebATM")
public class TOBwebAtmController {
    private final Logger LOGGER = LoggerFactory.getLogger(TOBwebAtmController.class);

    @Resource
    private TOBwebAtmService toBwebAtmService;

    @Value("${api.url}")
    private String API_URL;

    @Resource
    private TradeDetailService tradeDetailService;

    @RequestMapping(method = RequestMethod.GET, value = "/schedulingQueryTobWebAtmOrder")
    @ResponseBody
    public String schedulingQueryOrder(){
        try{

            List<String> orderIdlist = tradeDetailService.queryTobWebAtmOrder();
            String orderIds = new Gson().toJson(orderIdlist);

            LOGGER.info("[REQUEST] /api/TOBwebATM/orderId" + orderIds);
            LOGGER.info("[REQUEST] orderIdlist.size() " + orderIdlist.size());

            for(int i=0;i<orderIdlist.size();i++) {

                toBwebAtmService.schedulingQueryMerchantOrder(orderIdlist.get(i));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    //接收webagent.html
    @RequestMapping(method = RequestMethod.GET, value = "/{urlToken}/{amt}")
    public void getTOBwebATMUrl(@PathVariable String urlToken, @PathVariable String amt, HttpServletResponse response)  {
        LOGGER.info("[REQUEST] /api/TOBwebATM" + urlToken + amt);
        try {
            toBwebAtmService.getPaymentUrl(amt, urlToken,response);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    // accept TOB request
    @RequestMapping(method = RequestMethod.POST, value = "/backUrl")
    public void backUrl(@RequestParam(value = "CardPayRs") String CardPayRs,HttpServletResponse response ) throws IOException {
        String storeOrderNo = null;
        LOGGER.info("[TobWebATM][REQUEST] /backUrl:" + CardPayRs );

        try {
             storeOrderNo = toBwebAtmService.backUrl(CardPayRs);

        } catch (Exception e) {
            e.printStackTrace();
        }
        response.sendRedirect(String.format("%sallpaypass/api/payment/order/result?orderId=%s", API_URL, EncryptUtil.encryptSimple(storeOrderNo)));


    }
    @RequestMapping("/notify")  //TOBwebATM只有交易成功才有可能接收到notify
    public ResponseEntity<String> doNotify(@RequestBody String request){
        LOGGER.info("[TobWebATM][REQUEST] /notify :" + request );
        LOGGER.info("[TobWebATM][REQUEST] /notify " +
                "=================***********=======*****============"  );

        try {
            toBwebAtmService.notify(request);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseEntity<String>("200 O.K", HttpStatus.OK);
    }


}
