package co.intella.controller.customize;

import co.intella.domain.line.LineConfirmRequestData;
import co.intella.domain.line.LineInfoData;
import co.intella.domain.line.LineReserveRequestData;
import co.intella.domain.line.LineResponseData;
import co.intella.model.Merchant;
import co.intella.model.PaymentAccount;
import co.intella.model.QrcodeParameter;
import co.intella.model.TradeDetail;
import co.intella.service.*;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 * @author Alex
 */
@Controller
@RequestMapping("/api/linepay")
public class LinepayController {

    private final Logger LOGGER = LoggerFactory.getLogger(LinepayController.class);

    @Resource
    private LineService lineService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private OrderService orderService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private RedisService redisService;

    @Resource
    private RedirectService redirectService;

    @Resource
    private TradeDetailOtherInfoService tradeDetailOtherInfoService;

    @Value("${api.url}")
    private String API_URL;

    @RequestMapping(method = RequestMethod.POST, value = "/test")
    public ResponseEntity<String> test2444(@RequestBody LineResponseData input) {

        LOGGER.info("fefwfewfw");
        TradeDetail tradeDetail = tradeDetailService.getOne("Apple20170731060104");
        tradeDetail.setStatus("feff");
        tradeDetailService.save(tradeDetail);

        return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/getLineUrl")
    public ResponseEntity<String> getLineUrl(@RequestBody String input) {

        LOGGER.info("[getLineUrl]:" + input);

        JsonObject jsonObject = new Gson().fromJson(input, JsonObject.class);
        String totalFee = jsonObject.get("amount").getAsString();
        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(jsonObject.get("shortId").getAsString());
        try {
            LOGGER.info("[QrcodeParameter]:" + new ObjectMapper().writeValueAsString(qrcodeParameter));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        if (qrcodeParameter == null) {
            return null;
        } else {
            String nowStr = LocalDateTime.now().format(DateTimeFormatter.ofPattern(SystemInstance.DATE_PATTERN));
            if (StringUtils.isNotBlank(qrcodeParameter.getTimeExpire()) && Long.valueOf(nowStr) > Long.valueOf(qrcodeParameter.getTimeExpire())) {
                return null;
            }
        }

        String storeOrderNoNew;

        if (qrcodeParameter.getCodeType().equals("1")) {
            storeOrderNoNew = qrcodeParameter.getStoreOrderNo();
        } else {
            storeOrderNoNew = orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
        }

        Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());

        PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 15);

        TradeDetail tradeDetail = null;

        // add last 4 code
        String body = qrcodeService.getRandomBody(qrcodeParameter);
        qrcodeParameter.setBody(body);

        LineReserveRequestData lineReserveRequestData = lineService.prepareLineReserveRequestData(qrcodeParameter,
                Integer.valueOf(totalFee), storeOrderNoNew);
//        lineReserveRequestData.setProductName(qrcodeParameter.getBody());
//        lineReserveRequestData.setProductImageUrl(API_URL+"intella-allpaypass/images.png");
//        lineReserveRequestData.setAmount(Integer.valueOf(totalFee));
//        lineReserveRequestData.setCurrency("TWD");
//        lineReserveRequestData.setConfirmUrl(API_URL+"allpaypass/api/linepay/confirm");
//        lineReserveRequestData.setCancelUrl(API_URL+"allpaypass/api/linepay/cancel?orderId="+storeOrderNoNew);
//        lineReserveRequestData.setOrderId(storeOrderNoNew);
        try {
            tradeDetail = setOnlineTradeDetailRequest(lineReserveRequestData, merchant, paymentAccount,
                    qrcodeParameter);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("[line][set trade exception]" + e.getMessage());
        }

        String result = null;
        try {
            result = lineService.reserve(lineReserveRequestData, paymentAccount);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("[line][reserve exception]" + e.getMessage());

        }

        LOGGER.info("[line][reserve result]" + result);

        LineResponseData lineResponseData = new Gson().fromJson(result, LineResponseData.class);

        if (lineResponseData.getReturnCode().equals("0000")) {
            return new ResponseEntity<String>(lineResponseData.getInfo().getPaymentUrl().getApp(), HttpStatus.OK);
        } else {
            return null;
        }

    }

    private TradeDetail setOnlineTradeDetailRequest(LineReserveRequestData lineReserveRequestData, Merchant merchant,
                                                    PaymentAccount paymentAccount, QrcodeParameter qrcodeParameter) {
        LOGGER.info("enter trade detail request:" + new Gson().toJson(lineReserveRequestData));

        TradeDetail tradeDetail = tradeDetailService.getOne(merchant.getAccountId(),
                lineReserveRequestData.getOrderId());
        if (tradeDetail != null) {
            return tradeDetail;
        } else {
            tradeDetail = new TradeDetail();
        }

        tradeDetail.setAccountId(merchant.getAccountId());

        tradeDetail.setMethod("11500");

        tradeDetail.setPaymentAccount(paymentAccount);
        tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        tradeDetail.setStatus("Creating order");
        tradeDetail.setOrderId(lineReserveRequestData.getOrderId());
        tradeDetail.setDeviceRandomId(deviceService.getOne(merchant, "APP-001"));
        tradeDetail.setPayment(lineReserveRequestData.getAmount());
        tradeDetail.setServiceType("OLPay");
        tradeDetail.setQrcodeToken(qrcodeParameter.getShortId());
        tradeDetail.setDescription(lineReserveRequestData.getProductName());
        tradeDetail.setDetail(qrcodeParameter.getDetail());
        if (qrcodeParameter.getOnSaleTotalFee() == lineReserveRequestData.getAmount()) {
            tradeDetail.setOnSale(true);
        } else {
            tradeDetail.setOnSale(false);
        }
        tradeDetail.setOriginalPrice(qrcodeParameter.getTotalFee());

        try {
            LOGGER.info("save online trade detail : " + new ObjectMapper().writeValueAsString(tradeDetail));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            LOGGER.info("save online trade detail error!");
        }

        tradeDetail = tradeDetailService.save(tradeDetail);
        tradeProcedureService.captureOrderSave(tradeDetail);
        return tradeDetail;

    }


    @RequestMapping(method = RequestMethod.GET, value = "/cancel")
    public ModelAndView cancel(@RequestParam(value = "orderId") String orderId) {
        LOGGER.info("[LinePay][cancel] orderId: " + orderId);
        TradeDetail tradeDetail = tradeDetailService.getOne(orderId);

        ModelAndView model = new ModelAndView();
        model.addObject("RtnCode", "9999");
        model.addObject("titleBarColor", "linePayColor");
        model.setViewName("paymentResult");

        if (tradeDetail != null) {
            if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                tradeDetailService.save(tradeDetail);
            }
            model.addObject("MerchantID", merchantService.getOne(tradeDetail.getAccountId()).getName());
            model.addObject("domain", API_URL);
            model.addObject("TradeAmt", tradeDetail.getPayment());
            model.addObject("MerchantTradeNo", tradeDetail.getOrderId());
            model.addObject("PaymentDate", tradeDetail.getCreateDate());
            model.addObject("itemName", tradeDetail.getDescription());
        }
        model = redirectService.confirm(model);
        return model;

    }

    @RequestMapping(method = RequestMethod.GET, value = "/confirm")
    public ModelAndView confirm(@RequestParam(value = "orderId") String orderId,
                                @RequestParam(value = "transactionId") String transactionId) {

        LOGGER.info("[LinePay][confirm] orderId: " + orderId + ", transactionId :" + transactionId);
        TradeDetail tradeDetail = tradeDetailService.getOne(orderId);

        ModelAndView model = new ModelAndView();
        model.setViewName("paymentResult");
        model.addObject("domain", API_URL);
        model.addObject("titleBarColor", "linePayColor");
//        TradeDetailOtherInfo tradeDetailOtherInfo = tradeDetailOtherInfoService.getOneByUNHEXUuid(tradeDetail.getTradeDetailRandomId().toString());
//        Merchant merchant = merchantService.getOne(tradeDetail.getAccountId());
//
//        long delay = this.checkCallBackDelay(tradeDetailOtherInfo, merchant);
//        if (delay == 0 && StringUtils.isNotBlank(tradeDetailOtherInfo.getCallBackUrl())) {
//            return new ModelAndView("redirect:" + tradeDetailOtherInfo.getCallBackUrl());
//        } else {
        if (tradeDetail != null) {
            LOGGER.info("[LinePay][confirm] get trade ");
//                if (delay > 0) {
//                    model.addObject("callBackUrl", tradeDetailOtherInfo.getCallBackUrl());
//                    model.addObject("callBackDelay", delay);
//                }
            if (tradeDetail.getStatus().equals(SystemInstance.TRADE_SUCCESS)) {
                model.addObject("MerchantID", merchantService.getOne(tradeDetail.getAccountId()).getName());
                model.addObject("MerchantTradeNo", tradeDetail.getOrderId());
                model.addObject("TradeAmt", tradeDetail.getPayment());
                model.addObject("PaymentDate", tradeDetail.getCreateDate());
                model.addObject("RtnCode", "00");
                model.addObject("itemName", tradeDetail.getDescription());
                model = redirectService.confirm(model);
                return model;
            }

            PaymentAccount paymentAccount = paymentAccountService.getOne(tradeDetail.getAccountId(), 15);
            LineConfirmRequestData lineConfirmRequestData = new LineConfirmRequestData();
            lineConfirmRequestData.setCurrency("TWD");
            lineConfirmRequestData.setAmount((int) tradeDetail.getPayment());
            LineResponseData lineResponseData;
            LOGGER.info("[LinePay][confirm][id]" + paymentAccount.getAccount());
            try {
                String confirmResult = lineService.confirm(lineConfirmRequestData, paymentAccount, transactionId);
                LOGGER.info("[LinePay][confirm][Result]: " + confirmResult);
                lineResponseData = new Gson().fromJson(confirmResult, LineResponseData.class);
            } catch (Exception e) {
                LOGGER.info("[LinePay][confirm] exception: " + e.getMessage());
                e.printStackTrace();
                model.addObject("MerchantTradeNo", tradeDetail.getOrderId());
                model.addObject("TradeAmt", tradeDetail.getPayment());
                model.addObject("RtnCode", "9999");
                model = redirectService.confirm(model);
                return model;
            }
            /**
             *  修改 returnCode:"1172","returnMessage":"ExistingsameorderId."
             * 為了防止line重複confirm導致訂單原本成功在押到失敗
             */
            if (!lineResponseData.getReturnCode().equals("1172")) {

                setLinePayResponse(lineResponseData, tradeDetail);
            }


            model.addObject("MerchantID", merchantService.getOne(tradeDetail.getAccountId()).getName());
            model.addObject("MerchantTradeNo", tradeDetail.getOrderId());
            model.addObject("TradeAmt", tradeDetail.getPayment());
            model.addObject("PaymentDate", tradeDetail.getCreateDate());
            model.addObject("itemName", tradeDetail.getDescription());
            if (lineResponseData.getReturnCode().equals("0000") || lineResponseData.getReturnCode().equals("1172")) {
                tradeDetail = tradeDetailService.getOne(orderId);

                if (tradeDetail.getStatus().equals(SystemInstance.TRADE_SUCCESS)) {
                    model.addObject("RtnCode", "00");
                } else {
                    model.addObject("RtnCode", "9999");
                }

            } else {
                model.addObject("RtnCode", "9999");
            }
            redirectService.confirm(model);

            return model;

        } else {
            LOGGER.info("[LinePay][confirm] orderId: " + orderId + " is null ");

        }
//        }

        return null;
    }

    private void setLinePayResponse(LineResponseData lineResponseData, TradeDetail tradeDetail) {
        tradeDetail.setMethod("11500");

        LineInfoData lineInfoData = lineResponseData.getInfo();
        if (Objects.nonNull(lineInfoData) && Objects.nonNull(lineInfoData.getPayInfos())) {
            tradeDetail.setTxParams(new Gson().toJson(lineInfoData.getPayInfos()));
        }

        if (lineResponseData.getReturnCode().equals("0000") && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
            LOGGER.info("[LinePay][setLinePayResponse][Success]");
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            tradeDetail.setSystemOrderId(lineInfoData.getTransactionId().toString());
            appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
        } else if (!lineResponseData.getReturnCode().equals("0000")) {
            LOGGER.info("[LinePay][setLinePayResponse][Fail]");
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
//            tradeDetail.setSystemOrderId(lineResponseData.getInfo().getTransactionId().toString());
        }

        tradeDetailService.save(tradeDetail);

    }

//    private long checkCallBackDelay(TradeDetailOtherInfo tradeDetailOtherInfo, Merchant merchant) {
//        long delay = -1;
//        if (Objects.nonNull(tradeDetailOtherInfo)) {
//            // 有特別的Detail
//            Map<String, String> tradeDetailOtherInfoMap = JsonUtil.parseJson(tradeDetailOtherInfo.getRequestDetail(), Map.class);
//            if (StringUtils.isNotEmpty(merchant.getCallBackDelay())) {
//                delay = Long.valueOf(merchant.getCallBackDelay());
//            } else if (tradeDetailOtherInfoMap.containsKey("Delay")) {
//                delay = Long.valueOf(tradeDetailOtherInfoMap.get("Delay"));
//            } else {
//                delay = 3;
//            }
//            delay = delay > 10 ? 3 : delay;     //超過十秒 強制為3秒
//        }
//        return delay;
//    }
}
