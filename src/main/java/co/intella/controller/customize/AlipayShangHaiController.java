package co.intella.controller.customize;

import co.intella.domain.aliShangHai.AlipayPreCreateAsync;
import co.intella.model.*;
import co.intella.service.*;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

@Controller
@RequestMapping("/api/alipayshanghai")
public class AlipayShangHaiController {

    private final Logger LOGGER = LoggerFactory.getLogger(AlipayShangHaiController.class);

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private OrderService orderService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private AlipayShangHaiService alipayShangHaiService;

    @Value("${host.request.alipay.shanghai}")
    private String ALIPAY_SHANGHAI_URL;

    @Value("${api.url}")
    private String API_URL;

    @RequestMapping(method = RequestMethod.POST, value = "/getAlipayShangHaiUrl")
    public ResponseEntity<String> getAlipayShangHaiUrl(@RequestBody String input) {

        LOGGER.info("[getAlipayShangHaiUrl]:" + input);

        JsonObject jsonObject = new Gson().fromJson(input, JsonObject.class);
        LOGGER.info("[jsonObject]:" + jsonObject);
        String totalFee = jsonObject.get("amount").getAsString();
        LOGGER.info("[totalFee]:" + totalFee);
        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(jsonObject.get("shortId").getAsString());
        LOGGER.info("[qrcodeParameter]:" + qrcodeParameter);
        try {
            LOGGER.info("[QrcodeParameter]:" + new ObjectMapper().writeValueAsString(qrcodeParameter));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        if (qrcodeParameter == null) {
            return null;
        }
        TradeDetail tradeDetail = null;
        String storeOrderNoNew;
        if (qrcodeParameter.getCodeType().equals("1")) {
            storeOrderNoNew = qrcodeParameter.getStoreOrderNo();
            tradeDetail = tradeDetailService.getOne(storeOrderNoNew);
            totalFee = Integer.toString(Double.valueOf(qrcodeParameter.getTotalFee()).intValue());
        } else {
            storeOrderNoNew = orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
            LOGGER.info("[storeOrderNoNew]:" + storeOrderNoNew);
        }
        Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());

        PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 25);

        Device device = deviceService.getOne(merchant, "AlipayShangHai");
        LOGGER.info("[device]:" + device.getBatchNumber() + device.getOwner() + device.getType());

        //add last 4 code
        String body = qrcodeService.getRandomBody(qrcodeParameter);
        qrcodeParameter.setBody(body);

        String paymentUrl = null;
        try {
            paymentUrl = alipayShangHaiService.createOrder(tradeDetail, qrcodeParameter, Integer.valueOf(totalFee), storeOrderNoNew, paymentAccount);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<String>(paymentUrl, HttpStatus.OK);
    }

    //notify
    @RequestMapping(method = RequestMethod.POST, value = "/notify")
    public ResponseEntity<String> notify(@RequestBody String json) {

        LOGGER.info("[orderId] " + json);
        AlipayPreCreateAsync alipayPreCreateAsync = new Gson().fromJson(json, AlipayPreCreateAsync.class);
        LOGGER.info("[alipayPreCreateAsync] " + alipayPreCreateAsync);
        String orderIdTrans = alipayPreCreateAsync.getOutTradeNo();
        LOGGER.info("[orderIdTrans] " + orderIdTrans);

        String result;
        TradeDetail tradeDetail = tradeDetailService.getOne(orderIdTrans);
        LOGGER.info("[tradeDetail] " + tradeDetail);
        if (tradeDetail != null) {
            LOGGER.info("[AliPayShangHai][notify] get trade ");
            setAliPayShangHaiResponse(tradeDetail);
            if (tradeDetail.getStatus().equals(SystemInstance.TRADE_SUCCESS)) {
                result = "SUCCESS";
                LOGGER.info("[notify][result] " + result);
                return new ResponseEntity<String>(result, HttpStatus.OK);
            }
        } else {
            LOGGER.info("[AliPayShangHai][notify] json: " + json + " is null ");
        }
        return null;
    }

    private void setAliPayShangHaiResponse(TradeDetail tradeDetail) {
//        if (lineResponseData.getReturnCode().equals("0000")){
        LOGGER.info("[AliPayShangHai][setAliPayShangHaiResponse][Success]");
        if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
            tradeDetail.setMethod("12520");
            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
            appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
        }
//        }
//        else {
//            LOGGER.info("[AliPayShangHai][setLinePayResponse][Fail]");
//            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
////            tradeDetail.setSystemOrderId(lineResponseData.getInfo().getTransactionId().toString());
//        }
        tradeDetailService.save(tradeDetail);
    }
}
