package co.intella.controller.customize;

import co.intella.domain.twpay.QrpEmcqrNotifyResp;
import co.intella.domain.twpay.QrpEmvqrNotifyReq;
import co.intella.domain.twpay.TwPayResponse;
import co.intella.domain.twpay.WebToAppNotify;
import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import co.intella.service.*;
import co.intella.utility.EncryptUtil;
import co.intella.utility.SystemInstance;
import co.intella.utility.TWPayUtil;
import co.intella.utility.XmlUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.Objects;

/**
 * @author Alex
 */
@Controller
@RequestMapping("/api/twpay")
public class TwpayController {

    private final Logger LOGGER = LoggerFactory.getLogger(TwpayController.class);

    @Resource
    private TwPayService twPayService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private TradeProcedureService tradeProcedureService;

    @Resource
    private RedirectService redirectService;

    @Value("${api.url}")
    private String API_URL;

    @Value("${fisc.parameter}")
    private String FISC_PARAMETER = "A69896AF6057EFAD836FB7EB3109B52AB896E6B9FEA7173E";

    @RequestMapping(method = RequestMethod.POST, value = "/payment-url")
    public ResponseEntity<String> getPaymentUrl(@RequestBody String input, @RequestHeader(value = "User-Agent") String userAgent) {

        LOGGER.info("[TwpayController][getPaymentUrl]: " + input);

        JsonObject jsonObject = new Gson().fromJson(input, JsonObject.class);
        if (jsonObject.get("amount") == null) {
            return new ResponseEntity<String>("bad request; amount is required", HttpStatus.BAD_REQUEST);
        }

        String amount = jsonObject.get("amount").getAsString();
        String shortId = jsonObject.get("shortId").getAsString();

        TwPayResponse twPayResponse = null;
        try {
            String result = twPayService.getPaymentUrl(amount, shortId, userAgent);
            twPayResponse = new Gson().fromJson(result, TwPayResponse.class);
            LOGGER.info("[twpay][result]" + result);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("[twpay][exception]" + e.getMessage());
        }

        if (twPayResponse != null && "0000".equals(twPayResponse.getRespCode())) {
            return new ResponseEntity<String>(twPayResponse.getUriSchema(), HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("transaction fail", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/cancel")
    public ModelAndView cancel(@RequestParam(value = "orderId") String orderId) {

        LOGGER.info("[Twpay][cancel] orderId: " + orderId);
        TradeDetail tradeDetail = tradeDetailService.getOne(orderId);

        ModelAndView model = new ModelAndView();
        model.addObject("RtnCode", "9999");
        model.addObject("titleBarColor", "TwpayColor");
        model.setViewName("paymentResult");

        if (tradeDetail != null) {
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            tradeDetailService.save(tradeDetail);
            model.addObject("MerchantID", merchantService.getOne(tradeDetail.getAccountId()).getName());
            model.addObject("domain", API_URL);
            model.addObject("TradeAmt", tradeDetail.getPayment());
            model.addObject("MerchantTradeNo", tradeDetail.getOrderId());
            model.addObject("PaymentDate", tradeDetail.getCreateDate());
            model.addObject("itemName", tradeDetail.getDescription());
        }
        model = redirectService.confirm(model);
        return model;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/webToAppReq/{shortId}/{amount}")
    public void webToAppReq(@PathVariable("shortId") String shortId, @PathVariable("amount") String amount, HttpServletResponse response) throws Exception {

        LOGGER.info("[TwpayController][webToAppReq]: " + shortId + " \r amount = " + amount);
        twPayService.webToAppReq(shortId, amount, response);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/webToAppCallBack")
    public void webToAppCallBack(WebToAppNotify webToAppNotify, HttpServletResponse response) throws Exception {
//    public void webToAppNotify(@RequestParam(value = "acqBank") String acqBank, @RequestParam(value = "terminalId") String terminalId, @RequestParam(value = "merchantId") String merchantId,
//                               @RequestParam(value = "orderNumber") String orderNumber, @RequestParam(value = "cardPlan") String cardPlan, @RequestParam(value = "responseCode") String responseCode,
//                               @RequestParam(value = "verifyCode") String verifyCode, HttpServletResponse response) throws Exception {
        LOGGER.info("[TwpayController][webToAppCallBack]: " + webToAppNotify);
        TradeDetail tradeDetail = tradeDetailService.getOneBySystemOrderId(webToAppNotify.getOrderNumber());
        if (Objects.isNull(tradeDetail)) {
            tradeDetail = tradeDetailService.getOneByIntellaOrderId(webToAppNotify.getOrderNumber());
            if ("15200".equals(tradeDetail.getMethod())) {
                PaymentAccount paymentAccount = paymentAccountService.getOne(tradeDetail.getAccountId(), 56);
                tradeDetail.setPaymentAccount(paymentAccount);
            }
        }
        boolean isFiscVerifyCodeOk = TWPayUtil.isFiscVerifyCodeOk(webToAppNotify, webToAppNotify.getVerifyCode(), StringUtils.defaultIfEmpty(tradeDetail.getPaymentAccount().getPlatformCheckCode(), this.FISC_PARAMETER));
        LOGGER.info("[REQUEST][webToAppCallBack][isFiscVerifyCodeOk]: " + isFiscVerifyCodeOk);
        if (isFiscVerifyCodeOk) {
//            twPayService.updateTradeDetailByWebToApp(webToAppNotify, tradeDetail);
            response.sendRedirect(String.format("%sallpaypass/api/payment/order/result?orderId=%s", API_URL, EncryptUtil.encryptSimple(tradeDetail.getOrderId())));
        }
    }

    /**
     * 財金  信用卡回 調通知
     *
     * @param qrpNotifyReq
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.POST, value = "/webToAppNotify", produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<String> notifyFiscTWPayTransaction(@RequestBody String body, HttpServletResponse response) throws IOException {

        LOGGER.info("[REQUEST] /webToAppNotify: " + body);
        QrpEmvqrNotifyReq qrpNotifyReq = XmlUtil.unmarshall(body, QrpEmvqrNotifyReq.class);
        Gson gson = new Gson();
        Type typeOfMap = new TypeToken<Map<String, String>>() {
        }.getType();
        Map<String, String> otherInfo = gson.fromJson(qrpNotifyReq.getOtherInfo(), typeOfMap);
        String systemOrderId = otherInfo.get("tag20").trim().replaceAll("\\s","").replaceAll("\r?\n","");
        TradeDetail tradeDetail = tradeDetailService.getOneBySystemOrderId(systemOrderId.contains(",") ? systemOrderId.split(",")[1] : systemOrderId);
        QrpEmcqrNotifyResp qrpNotifyResp = twPayService.getQrpEmvqrNotifyResp(qrpNotifyReq);
        if (Objects.nonNull(tradeDetail) && StringUtils.isBlank(tradeDetail.getTxParams())) {
            // 目前以收主掃交易通知為主; 退貨、取消當下交易時就會會知道

            String key = StringUtils.defaultIfBlank(tradeDetail.getPaymentAccount().getPlatformCheckCode(), this.FISC_PARAMETER);
            boolean isFiscVerifyCodeOk = TWPayUtil.isFiscVerifyCodeOk(qrpNotifyReq, qrpNotifyReq.getVerifyCode(), key);
            LOGGER.info("[REQUEST][webToAppNotify][isFiscVerifyCodeOk]: " + isFiscVerifyCodeOk);

            twPayService.updateTradeDetailByQrpEmvqrNotifyReq(qrpNotifyReq, tradeDetail);
//            response.sendRedirect(String.format("%sallpaypass/api/payment/order/result?orderId=%s", API_URL, EncryptUtil.encryptSimple(tradeDetail.getOrderId())));
        }
        return new ResponseEntity<String>(XmlUtil.marshall(qrpNotifyResp), HttpStatus.OK);
    }
}