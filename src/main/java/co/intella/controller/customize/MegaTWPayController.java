package co.intella.controller.customize;

import co.intella.domain.megatwpay.NewNotifyReq;
import co.intella.domain.megatwpay.TxnNotifyHeaderRes;
import co.intella.domain.megatwpay.TxnNotifyReq;
import co.intella.domain.megatwpay.TxnNotifyRes;
import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import co.intella.service.AppNotificationService;
import co.intella.service.MerchantService;
import co.intella.service.PaymentAccountService;
import co.intella.service.TradeDetailService;
import co.intella.utility.NewMegaTWPayUtil;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Controller
@RequestMapping("/api/megatwpay")
public class MegaTWPayController {

    private final Logger LOGGER = LoggerFactory.getLogger(MegaTWPayController.class);

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Value("${api.url}")
    private String API_URL;

    //notify
    @RequestMapping(method = RequestMethod.POST, value = "/notify")
    public ResponseEntity<String> notify(@RequestBody String json) {

        LOGGER.info("[notify] " + json);
        TxnNotifyReq txnNotifyReq = new Gson().fromJson(json, TxnNotifyReq.class);
        LOGGER.info("[txnNotifyReq] " + txnNotifyReq);
        String orderIdTrans = txnNotifyReq.getTxnNotifyDataReq().getOrderNo();
        LOGGER.info("[orderIdTrans] " + orderIdTrans);

        String result;
        TxnNotifyRes txnNotifyRes = new TxnNotifyRes();
        TxnNotifyHeaderRes txnNotifyHeaderRes = new TxnNotifyHeaderRes();
        TradeDetail tradeDetail = tradeDetailService.getOne(orderIdTrans);
        LOGGER.info("[tradeDetail] " + tradeDetail);
        if (tradeDetail != null) {
            LOGGER.info("[MegaTWPay][notify] get trade ");
            String notifyType = txnNotifyReq.getTxnNotifyDataReq().getType();
            setMegaTWPayResponse(tradeDetail, notifyType);
            txnNotifyHeaderRes.setsCode("00");
            txnNotifyHeaderRes.setsDesc("執行成功");
            txnNotifyHeaderRes.setrTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
            txnNotifyRes.setTxnNotifyHeaderRes(txnNotifyHeaderRes);
            result = new Gson().toJson(txnNotifyRes);
            LOGGER.info("result :" + result);
            return new ResponseEntity<String>(result, HttpStatus.OK);

        } else {
            LOGGER.info("[MegaTWPay][notify] orderId: " + orderIdTrans + " tradeDetail is null ");
        }
        return null;
    }

    private void setMegaTWPayResponse(TradeDetail tradeDetail, String notifyType) {
        LOGGER.info("[MegaTWPay][setMegaTWPayResponse][Success]");
        tradeDetail.setMethod("13500");
        if ("P".equals(notifyType)) {
            //payment success
            if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
            }
            //todo open after test

        } else if ("R".equals(notifyType)) {
            //refund success
            tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
        } else {
            //cancel success
            tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            tradeDetail.setRefundStatus(SystemInstance.TRADE_CANCEL_SUCCESS);
        }
        tradeDetailService.save(tradeDetail);
    }


    @RequestMapping(method = RequestMethod.POST, value = "/newNotify")
    @ResponseBody
    public Map<String, String> newNotify(@RequestBody NewNotifyReq newNotifyReq) {
        LOGGER.info("[newNotify] " + newNotifyReq);

        String rtnCode = "0101";
        Map<String, String> resulMap = new HashMap<>();
        if (Double.valueOf(newNotifyReq.getTxnAmt()) > 0) {
            TradeDetail tradeDetail = tradeDetailService.getOneBySystemOrderId(newNotifyReq.getOrderNumber());
            try {
                PaymentAccount paymentAccount = paymentAccountService.getOne(tradeDetail.getAccountId(), 52);
                tradeDetail.setPaymentAccount(paymentAccount);
                tradeDetail.setMethod("15200");
                boolean check = NewMegaTWPayUtil.checkSign(newNotifyReq, paymentAccount, "2");
                if (check) {
                    if (Objects.nonNull(tradeDetail)) {
                        if ("P".equals(newNotifyReq.getTxnType()) && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                            //購物
                            tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                            tradeDetail.setCreditCardType("C".equals(newNotifyReq.getPayType()) ? "8" : "7");   //D: 金融卡 C:信用卡
                            tradeDetail.setIntellaOrderId(newNotifyReq.getOrderNumber());
                            tradeDetail.setSystemOrderId(newNotifyReq.getTxnSeqno());
                            tradeDetailService.save(tradeDetail);
                            appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                        }//"R".equals(newNotifyReq.getTxnType() ，不做事情，這種理應直接在退款時就有狀態。
                        rtnCode = "0000";
                    } else {
                        rtnCode = "0112";
                    }
                }
            } catch (Exception e) {
                LOGGER.error("[EXCEPTION][newNotify] checkSign." + e.getMessage() + ", \n"
                        + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            }
        } else {
            rtnCode = "0000";
        }
        resulMap.put("rtnCode", rtnCode);
        resulMap.put("rtnMsg", NewMegaTWPayUtil.getMegaTWPayCodeDesc(rtnCode));
        return resulMap;
    }

}
