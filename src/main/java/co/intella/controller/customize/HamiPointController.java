package co.intella.controller.customize;

import co.intella.model.IntTbLookupCode;
import co.intella.service.LookupCodeService;
import co.intella.utility.HttpUtil;
import co.intella.utility.MD5Util;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/hamiPoint")
public class HamiPointController {

    @Value("${hamiPoint.auth.api.url}")
    private String AUTH_API_URL;

    @Value("${hamiPoint.token.api.url}")
    private String TOKEN_API_URL;

    @Value("${hamiPoint.user.info.api.url}")
    private String USER_INFO_API_URL;

    @Resource
    private LookupCodeService lookupCodeService;

    @Value("${api.url}")
    private String API_URL;

    private final Logger LOGGER = LoggerFactory.getLogger(HamiPointController.class);

    @RequestMapping(method = RequestMethod.GET, value = "/check")
    public void check(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.info("[REQUEST] /api/hamiPoint/check ");

        //TODO  檢查DB 有無SN, 無的話做授權

        if (false) {

        } else {
            //無sn
            chtAccountAuth(request, response);
        }
    }

    /**
     * 取得會員基本屬性
     */
    private void authorizationToken(String jsonStr) {
        LOGGER.info("[REQUEST] [authorizationTolen]  start ");

        JsonObject jsonObject = new Gson().fromJson(jsonStr, JsonObject.class);
        String accessToken = Objects.nonNull(jsonObject.get("access_token")) && !jsonObject.get("access_token").isJsonNull() ?
                jsonObject.get("access_token").getAsString() : "";
        Map<String, String> reqMap = new HashMap<>();
        reqMap.put("access_token", accessToken);
        reqMap.put("client_id", getHamiPointAuth().getType1());

        LOGGER.info("[REQUEST] [authorizationTolen]  reqMap =  " + reqMap);
        String result = HttpUtil.doPost(USER_INFO_API_URL, reqMap);
        LOGGER.info("[REQUEST] [authorizationTolen]  result =  " + result);
        JsonObject resultObject = new Gson().fromJson(result, JsonObject.class);
        String sn = Objects.nonNull(resultObject.get("sn")) && !resultObject.get("sn").isJsonNull() ?
                resultObject.get("sn").getAsString() : "";
        LOGGER.info("[REQUEST] [authorizationTolen]  sn =  " + sn);
        if (StringUtils.isNotEmpty(sn)) {
            //TODO 存起來
        }

    }

    /**
     * 驗證會員授權
     *
     * @param request
     * @param response
     */
    @RequestMapping(method = RequestMethod.GET, value = "/Authorization")
    public void authorization(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.info("[REQUEST] /api/hamiPoint/Authorization");
        //會員授權回調必帶state參數
        String state = request.getParameter("state");
        //驗證 state ,準備取token
        String[] states = state.split(",");
        if (states.length == 2) {
            String checkState = states[0].toLowerCase() + LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
            checkState = MD5Util.getMD5(checkState);
            LOGGER.info("[REQUEST] checkState = " + checkState);
            LOGGER.info("[REQUEST] State = " + states[1]);
            if (states[1].equalsIgnoreCase(checkState)) {
                authorizationToken(chtAccountToken(request, response));
            }
        }
    }

    /**
     * 會員授權
     *
     * @param request
     */
    private void chtAccountAuth(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.info("[REQUEST] [chtAccountAuth]  start ");

        //TODO   取得手機機碼
        String deviceId = "deviceId0";
        LOGGER.info("[REQUEST] [chtAccountAuth] phone deviceId = " + deviceId);
        Map<String, String> reqMap = new HashMap<>();
        //TODO  還未完成申請 OAuth
        reqMap.put("client_id", getHamiPointAuth().getType1());
        reqMap.put("redirect_uri", API_URL + "allpaypass/api/hamiPoint/Authorization");
        reqMap.put("response_type", "code");
        reqMap.put("scope", "basic_profile");
        String state = deviceId.toLowerCase() + LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        reqMap.put("state", deviceId + "%2c" + MD5Util.getMD5(state));
        try {
            String queryStr = reqMap.entrySet().stream().map(m -> m.getKey() + "=" + m.getValue()).collect(Collectors.joining("&"));
            LOGGER.info("[REQUEST] [chtAccountAuth] queryStr = " + queryStr);
            response.sendRedirect(AUTH_API_URL + "?" + queryStr);
            LOGGER.info("[REQUEST] [chtAccountAuth]  end ");
        } catch (UnsupportedEncodingException e) {
            LOGGER.info("[REQUEST] [chtAccountAuth]  URLEncoder fail ");
            e.printStackTrace();
        } catch (IOException e) {
            LOGGER.info("[REQUEST] [chtAccountAuth]  sendRedirect fail ");
            e.printStackTrace();
        }
    }

    /**
     * 存取授權
     *
     * @param request
     * @param response
     */
    private String chtAccountToken(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.info("[REQUEST] [chtAccountToken]  start ");
        StringBuffer sb = new StringBuffer();
        String code = request.getParameter("code");
        String result = "";
        if (Objects.nonNull(code) && StringUtils.isNotEmpty(code)) {
            Map<String, String> reqMap = new HashMap<>();
            reqMap.put("grant_type", "authorization_code");
            reqMap.put("code", code);
            reqMap.put("redirect_uri", API_URL + "allpaypass/api/hamiPoint/Authorization");
            IntTbLookupCode lookupCode = getHamiPointAuth();
            reqMap.put("client_id", lookupCode.getType1());
            reqMap.put("client_secret", lookupCode.getType2());
            LOGGER.info("[REQUEST] [chtAccountToken]  reqMap " + reqMap);
            result = HttpUtil.doPost(TOKEN_API_URL, reqMap);

        } else {
            //TODO  會員授權失敗
            LOGGER.info("[REQUEST] [chtAccountToken] Auth Fail");
            LOGGER.info("[REQUEST] [chtAccountToken] cht error : " + request.getParameter("error"));
            LOGGER.info("[REQUEST] [chtAccountToken] cht error desc  : " + request.getParameter("error_description"));
        }
        LOGGER.info("[REQUEST] [chtAccountToken]  end ");
        return result;
    }

    private IntTbLookupCode getHamiPointAuth() {
        IntTbLookupCode tlc = lookupCodeService.findOne("hamiPointAuth", "OAuthInfo");
//        LOGGER.info("[HamiPoint] IntTbLookupCode = " + tlc);
        return tlc;
    }
}
