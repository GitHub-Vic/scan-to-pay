package co.intella.controller.customize;


import co.intella.domain.gama.GamaCreateOrderResponseData;
import co.intella.domain.gama.GamaNotifyData;
import co.intella.domain.gama.GamaOnlineReturn;
import co.intella.model.Merchant;
import co.intella.model.PaymentAccount;
import co.intella.model.QrcodeParameter;
import co.intella.model.TradeDetail;
import co.intella.service.*;
import co.intella.utility.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


/**
 * @author Alex
 */
@Controller
@RequestMapping("/api/gamapay")
public class GamapayController {

    private final Logger LOGGER = LoggerFactory.getLogger(GamapayController.class);

    @Resource
    private QrcodeService qrcodeService;

    @Autowired
    private MessageSource messageSource;

    @Resource
    private OrderService orderService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private GamaPayService gamaPayService;

    @Resource
    private OnlinepayUtil OnlinepayUtil;

    @Value("${api.url}")
    private String API_URL;

    @Resource
    private AppNotificationService appNotificationService;

     private Locale locale = new Locale("zh_TW");


    @RequestMapping(method = RequestMethod.GET, value = "/{urlToken}/{amt}")
    public ResponseEntity<String> onlinePay(@PathVariable String urlToken, @PathVariable String amt) throws Exception {

        LOGGER.info("[REQUEST] /api/gamapay" + urlToken + amt);

//		QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(urlToken);
//
//		if (qrcodeParameter == null) {
//			return new ResponseEntity<String>(messageSource.getMessage("response.7158", null, locale), HttpStatus.OK);
//		}
//		String storeOrderNoNew = "1".equals(qrcodeParameter.getCodeType())? qrcodeParameter.getStoreOrderNo():orderService.createOrderId(qrcodeParameter.getMchId());
//
//		String integrateMchId = qrcodeParameter.getMchId();
//		TradeDetail tradeDetail = tradeDetailService.getOne(integrateMchId, storeOrderNoNew);
//
//		// add last 4 code
//		String body = qrcodeParameter.getBody();
//		if (body.contains("-")) {
//			String[] temp = body.split("-");
//			body = temp[0];
//		}
//		String bodyRandom = body + "-" + String.format("%04d", new Random().nextInt(9999));
//
//		if (!Objects.isNull(tradeDetail)) {
//			boolean flag = tradeDetail.getStatus().equals("Waiting")
//					|| tradeDetail.getStatus().equals(SystemInstance.TRADE_SUCCESS);
//			if (flag) {
//				return new ResponseEntity<String>(tradeDetail.getOnlinePayUrl(), HttpStatus.OK);
//			}
//
//		}
//		tradeDetail = setOnlineTradeDetailRequest(qrcodeParameterrcodeParameter, integrateMchId, bodyRandom, storeOrderNoNew);
//
//		PaymentAccount paymentAccount = paymentAccountService.getOne(integrateMchId, 13);
//
//		GamaCreateOrderRequestData gamaCreateOrderRequestData = new GamaCreateOrderRequestData();
//		gamaCreateOrderRequestData.setMerchantOrderId(storeOrderNoNew);
//		gamaCreateOrderRequestData.setTransAmount(Integer.parseInt(amt));
//
//		DateTime dateTime = DateTime.now();
//		gamaCreateOrderRequestData.setMerchantId(paymentAccount.getAccount());
//		gamaCreateOrderRequestData.setTransType(10);
//		gamaCreateOrderRequestData.setTradingContent(bodyRandom);
//		gamaCreateOrderRequestData.setCurrencyCode("TWD");
//		gamaCreateOrderRequestData.setBusinessDate(dateTime.toString("yyyyMMdd"));
//		gamaCreateOrderRequestData.setPayWay(1);
//		gamaCreateOrderRequestData.setPostBackUrl(String.format("%sallpaypass/api/payment/order/result?orderId=%s", API_URL, storeOrderNoNew));
//
//
//		String result = gamaPayService.createOrder(gamaCreateOrderRequestData, paymentAccount);
//
//		GamaCreateOrderResponseData gamaCreateOrderResponseData = new Gson().fromJson(result, GamaCreateOrderResponseData.class);


        String response = gamaPayService.getPaymentUrl(amt, urlToken, "");
        GamaCreateOrderResponseData gamaCreateOrderResponseData = new Gson().fromJson(response, GamaCreateOrderResponseData.class);

        return new ResponseEntity<String>(gamaCreateOrderResponseData.getgPPUrl(), HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/backUrl")
    public void backUrl(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String storeOrderNo = request.getParameter("MerchantOrderID");
        LOGGER.info("[GamaPay][REQUEST] /backUrl :" + request.getRequestURL());
        LOGGER.info("[GamaPay][REQUEST] storeOrderNo :" + storeOrderNo);
        LOGGER.info("[GamaPay][REQUEST] forword  :" + String.format("%sallpaypass/api/payment/order/result?orderId=%s", API_URL, EncryptUtil.encryptSimple(storeOrderNo)));
        Thread.sleep(150);
        response.sendRedirect(String.format("%sallpaypass/api/payment/order/result?orderId=%s", API_URL, EncryptUtil.encryptSimple(storeOrderNo)));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/notify")
    public ResponseEntity<String> notify(HttpServletRequest request) {

        Map<String, String> map = new HashMap<String, String>();
        boolean statusFlag = false;
        HttpStatus httpStatus = null;

        GamaNotifyData gamaNotifyData = new GamaNotifyData();
        gamaNotifyData.setTransactionId(request.getParameter("TransactionID"));
        gamaNotifyData.setMerchantOrderId(request.getParameter("MerchantOrderID"));
        gamaNotifyData.setCurrencyCode(request.getParameter("CurrencyCode"));
        gamaNotifyData.setTransAmount(Double.valueOf(request.getParameter("TransAmount")).intValue());
        gamaNotifyData.setMac(request.getParameter("MAC"));
        gamaNotifyData.setStatusCode(request.getParameter("StatusCode"));
        LOGGER.info("[GamaPay][REQUEST] /notify :" + JsonUtil.toJsonStr(gamaNotifyData));
        TradeDetail tradeDetail = tradeDetailService.getOne(gamaNotifyData.getMerchantOrderId());
        if (tradeDetail == null) {
            LOGGER.error("[GamaPay][REQUEST] tradedetail is not found.");
        } else {
            tradeDetail.setMethod("11300");
        }
        PaymentAccount paymentAccount = paymentAccountService.getOne(tradeDetail.getAccountId(), 13);
        tradeDetail.setPaymentAccount(paymentAccount);
        if (!statusFlag && gamaNotifyData.getMac().equals(this.getMacValue(gamaNotifyData, tradeDetail.getPaymentAccount()))) {
            String status = gamaNotifyData.getStatusCode().toString();

            tradeDetail.setSystemOrderId(request.getParameter("TransactionID"));

            if ("100".equals(status) && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
            } else if (("0".equals(status) || "".equals(status))
                    && (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus()))) {
                tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
            } else if ("200".equals(status)
                    && SystemInstance.TRADE_REFUND_SUCCESS.equals(tradeDetail.getRefundStatus())) {
                tradeDetail.setRefundStatus(SystemInstance.TRADE_REFUND_SUCCESS);
            } else {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus()))
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
            }

            if (tradeDetailService.save(tradeDetail) == null) {
                LOGGER.error("[GamaPay][REQUEST] notify save tradeDetail fail.");
            } else {
                LOGGER.info("[GamaPay][REQUEST] notify save tradeDetail succes.");
                statusFlag = true;
            }

        } else if (!statusFlag && !gamaNotifyData.getMac().equals(this.getMacValue(gamaNotifyData, tradeDetail.getPaymentAccount()))) {
            LOGGER.error("[GamaPay][REQUEST] trading pressure code is illegal.");
        }

        if (statusFlag) {
            map.put("Result", "OK");
            httpStatus = HttpStatus.OK;
        } else {
            map.put("Result", "NG");
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        String result = new Gson().toJson(map);
        return new ResponseEntity<String>(result, httpStatus);
    }

    private String getResponseJson(QrcodeParameter qrcodeParameter, TradeDetail tradeDetail) {
        GamaOnlineReturn gamaOnlineReturn = new GamaOnlineReturn();

        gamaOnlineReturn.setCreateDate(tradeDetail.getCreateDate());
        gamaOnlineReturn.setDetail(tradeDetail.getDescription());
        gamaOnlineReturn.setOrderId(tradeDetail.getOrderId());
        gamaOnlineReturn.setPaymentAccount(tradeDetail.getPaymentAccount().getAccount());
        gamaOnlineReturn.setStoreName(merchantService.getOne(tradeDetail.getAccountId()).getName());
        if (qrcodeParameter.getCodeType().equals("3")) {
            gamaOnlineReturn.setWithFee(false);
            gamaOnlineReturn.setTotalFee("");
        } else {
            gamaOnlineReturn.setWithFee(true);
            gamaOnlineReturn.setTotalFee(String.valueOf(OnlinepayUtil.getTradeFee(qrcodeParameter, "11300")));
        }
        return new Gson().toJson(gamaOnlineReturn);
    }

    private TradeDetail setOnlineTradeDetailRequest(QrcodeParameter qrcodeParameter, String integrateMchId, String body,
                                                    String storeOrderNoNew) throws Exception {
        LOGGER.info("enter trade detail request:" + new Gson().toJson(qrcodeParameter));

        PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 13);

        Merchant merchant = merchantService.getOne(integrateMchId);


//        TradeDetail tradeDetail = tradeDetailService.getOne(integrateMchId, weixinJsapiRequestData.getOutTradeNo());
//        if (tradeDetail != null) {
//            return tradeDetail;
//        } else {
        TradeDetail tradeDetail = new TradeDetail();
//        }

        tradeDetail.setAccountId(integrateMchId);

        tradeDetail.setMethod("11300");

        tradeDetail.setPaymentAccount(paymentAccount);
        tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        tradeDetail.setStatus("Creating order");
        tradeDetail.setOrderId(storeOrderNoNew);
        tradeDetail.setDeviceRandomId(deviceService.getOne(merchant, "skb0001"));
        // tradeDetail.setPayment(Long.parseLong(weixinJsapiRequestData.getTotalFee()));
        tradeDetail.setServiceType("OLPay");
        tradeDetail.setQrcodeToken(qrcodeParameter.getShortId());
        tradeDetail.setStoreInfo(qrcodeParameter.getStoreInfo());
        tradeDetail.setDescription(body);

        if (qrcodeParameter.getCodeType().equals("2")
                && (OnlinepayUtil.getTradeFee(qrcodeParameter, "11300") == qrcodeParameter.getOnSaleTotalFee())) {
            tradeDetail.setOnSale(true);
        } else {
            tradeDetail.setOnSale(false);
        }
        tradeDetail.setOriginalPrice(qrcodeParameter.getTotalFee());
        LOGGER.info("save online trade detail : " + new ObjectMapper().writeValueAsString(tradeDetail));

        tradeDetailService.save(tradeDetail);
        return tradeDetail;
    }

    private String getMacValue(GamaNotifyData request, PaymentAccount paymentAccount) {

        String macData = String.format("%s%s%s%s%s", paymentAccount.getAccount(), request.getMerchantOrderId(),
                request.getCurrencyCode(), String.format("%014d", request.getTransAmount() * 100),
                paymentAccount.getHashIV());
        GamaPayEncryptAESUtil gamaPayEncryptAESUtil = new GamaPayEncryptAESUtil(paymentAccount.getHashIV(),
                paymentAccount.getHashKey());
        String mac = gamaPayEncryptAESUtil.encrypt(macData);
        return mac;
    }

}
