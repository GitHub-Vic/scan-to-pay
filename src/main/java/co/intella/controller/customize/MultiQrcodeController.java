package co.intella.controller.customize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;

import co.intella.model.MultiQrcode;
import co.intella.service.MultiQrcodeService;

@Controller
@RequestMapping("/api/mutiQrcode")
public class MultiQrcodeController {

	private final Logger LOGGER = LoggerFactory.getLogger(MultiQrcodeController.class);

	@Resource
	private MultiQrcodeService multiQrcodeService;

	@PostMapping("/findmergeQrcodeByPayer")
	public ResponseEntity<String> findMutiQRCodeByPayer(@RequestBody Map<String, String> requestBody, @CookieValue(value = "PayerKey", defaultValue = "") String payerKey, HttpServletResponse response) throws Exception {
		String generalResponseData = "";
		HttpStatus httpStatus = HttpStatus.OK;
		try {
			String payerId = requestBody.get("payerId");
			String token = requestBody.get("token");
			if(null == payerId && null == token && StringUtils.isNotBlank(payerKey)) {
				String[] keyArray = payerKey.split("#");
				payerId = keyArray[0];
				token = keyArray[1];
			}
			List<MultiQrcode> resList = multiQrcodeService.findMutiQRCodeByPayer(requestBody.get("shortId"), requestBody.get("email"), requestBody.get("payerName"), requestBody.get("mobileNumber"), payerId, token, response);
			if (null != resList) {
				generalResponseData = new Gson().toJson(resList);
			}
		} catch (Exception e) {
			LOGGER.info("[EXCEPTION] " + e.getMessage() + " - " + e.getStackTrace());
			deleteCookie(response);
			httpStatus = HttpStatus.FORBIDDEN;
			generalResponseData = getExceptionRes(e);
		}
		return new ResponseEntity<String>(generalResponseData, httpStatus);
	}

	@PostMapping("/findmergeQrcode")
	public ResponseEntity<String> findMutiQRCode(@RequestBody Map<String, String> requestBody, @CookieValue(value = "PayerKey", defaultValue = "") String payerKey, HttpServletResponse response) throws Exception {
		String generalResponseData = "";
		HttpStatus httpStatus = HttpStatus.OK;
		try {
			String payerId = requestBody.get("payerId");
			String token = requestBody.get("token");
			if(null == payerId && null == token && StringUtils.isNotBlank(payerKey)) {
				String[] keyArray = payerKey.split("#");
				payerId = keyArray[0];
				token = keyArray[1];
			}
			LOGGER.info("payerId " + payerId + token);
			Map<String, Object> resMap = multiQrcodeService.findMutiQRCode(requestBody.get("shortId"), payerId, token, response);
			if (null != resMap) {
				generalResponseData = new Gson().toJson(resMap);
			}
		} catch (Exception e) {
			LOGGER.info("[EXCEPTION] " + e.getMessage() + " - " + e.getStackTrace());
			deleteCookie(response);
			httpStatus = HttpStatus.FORBIDDEN;
			generalResponseData = getExceptionRes(e);
		}
		return new ResponseEntity<String>(generalResponseData, httpStatus);
	}

	@PostMapping("/mergeQrcode")
	public ResponseEntity<String> mergeQrcode(@RequestBody Map<String, String> requestBody, @CookieValue(value = "PayerKey", defaultValue = "") String payerKey, HttpServletResponse response) throws Exception {
		String shortId = requestBody.get("shortId");
		String payerId = requestBody.get("payerId");
		String token = requestBody.get("token");
		LOGGER.info("[REQUEST][MUTIQRCODE][Input] shortId = " + shortId + ", payerId = " + payerId + ", token = " + token);
		String generalResponseData = null;
		HttpStatus httpStatus = HttpStatus.OK;
		try {
			if(null == payerId && null == token && StringUtils.isNotBlank(payerKey)) {
				String[] keyArray = payerKey.split("#");
				payerId = keyArray[0];
				token = keyArray[1];
			}
			generalResponseData = multiQrcodeService.mergeQrcode(shortId, payerId, token, response);
		} catch (Exception e) {
			LOGGER.info("[EXCEPTION] " + e.getMessage() + " - " + e.getStackTrace());
			deleteCookie(response);
			httpStatus = HttpStatus.FORBIDDEN;
			generalResponseData = getExceptionRes(e);
		}
		LOGGER.info("[RESPONSE][MUTIQRCODE] " + generalResponseData);
		return new ResponseEntity<String>(generalResponseData, httpStatus);
	}

	@PostMapping("/snedCookie")
	public ResponseEntity<String> sendCookie(@RequestBody Map<String, String> request, HttpServletRequest httpServletRequest, HttpServletResponse response) throws Exception {
		HttpStatus httpStatus = HttpStatus.OK;
		String result = "";
		try {
			result = multiQrcodeService.sendCookie(request, response);
		} catch (Exception e) {
			httpStatus = HttpStatus.FORBIDDEN;
			LOGGER.info("[SEND_COOKIE][MUTIQRCODE] send cookie fail");
			result = "send cookie fail";
		}
		return new ResponseEntity<String>(result, httpStatus);
	}
	
	@PostMapping("/sendToken")
	public ResponseEntity<String> sendToken(@RequestBody Map<String, String> request) throws Exception {
		HttpStatus httpStatus = HttpStatus.OK;
		String result = "";
		try {
			multiQrcodeService.sendToken(request);
			result = "send mail or text success";
		} catch (Exception e) {
			httpStatus = HttpStatus.FORBIDDEN;
			LOGGER.info("[SEND_TOKEN][MUTIQRCODE] send Token fail" + e.getMessage() + " - " + e.getStackTrace());
			result = "send mail or text fail";
		}
		return new ResponseEntity<String>(result, httpStatus);
	}
	
	private void deleteCookie(HttpServletResponse response) {
		Cookie cookie = new Cookie("PayerKey", null);
		cookie.setMaxAge(0);
		response.addCookie(cookie);
	}
	
	private String getExceptionRes(Exception e) {
		Map<String, Object> result = new HashMap<>();
		Map<String, String> header = new HashMap<>();
		Map<String, String> data = new HashMap<>();
		header.put("msg", e.getMessage());
		header.put("msgCode", "9998");
		result.put("Header", header);
		result.put("Data", data);
		return new Gson().toJson(result);
	}
}
