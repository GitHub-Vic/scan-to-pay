package co.intella.controller.customize;

import co.intella.constant.Bank;
import co.intella.domain.newPi.NotifyRequest;
import co.intella.domain.newPi.NotifyResponse;
import co.intella.domain.pi.*;
import co.intella.model.*;
import co.intella.service.*;
import co.intella.utility.JsonUtil;
import co.intella.utility.OnlinepayUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author Alex Pi Prod. 175.99.68.152, 60.199.143.106 Pi Stage.175.99.68.154
 */
@Controller
@RequestMapping("/api/pi")
public class PiController {

    private final Logger LOGGER = LoggerFactory.getLogger(PiController.class);

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private AppNotificationService appNotificationService;

    @Resource
    private NtuImotoService ntuImotoService;

    @Resource
    private NewPiService newPiService;

    @Resource
    private OnlinepayUtil OnlinepayUtil;

    private List<String> paymentGateway = Arrays.asList("pi_wallet", "credit_card");

    private List<String> ipAllowed = Arrays.asList("175.99.68.152", "60.199.143.106", "61.221.49.115", "175.99.68.154", "175.99.68.143");

    @RequestMapping(method = RequestMethod.POST, value = "/createOrder")
    public ResponseEntity<String> createOrder(@RequestBody PiCreateOrderRequestData requestData,
                                              HttpServletRequest request) {

        LOGGER.info("[REQUEST][Pi] " + request.getHeader("X-FORWARDED-FOR"));
        String remoteIP = request.getHeader("X-FORWARDED-FOR");
        if (!remoteIP.isEmpty()) {
            remoteIP = remoteIP.split(",")[0];
        }
        if (ipAllowed.contains(remoteIP)) {
            LOGGER.info("[REQUEST][Pi] Check from Pi Server.");
        } else {
            LOGGER.error("[REQUEST][Pi] This Request is not allowed. " + remoteIP);
            String errorJson = new Gson().toJson(getErrorMessage("9995", "ip not allowed", requestData.getQrcode()));
            return new ResponseEntity<String>(errorJson, HttpStatus.BAD_REQUEST);
        }

        LOGGER.info("[REQUEST][Pi] createOrder() " + new Gson().toJson(requestData));

        if (requestData.getQrcode().matches(".*\\..*")) {
            String s = requestData.getQrcode();
            requestData.setQrcode(s.split("\\.")[0]);
        }

        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(requestData.getQrcode());

        if (qrcodeParameter == null) {

            LOGGER.error("[REQUEST][Pi] createOrder() qrcode not found. " + requestData.getQrcode());
            String errorJson = new Gson().toJson(getErrorMessage("9998", "qrcode not found", requestData.getQrcode()));
            return new ResponseEntity<String>(errorJson, HttpStatus.BAD_REQUEST);
        } else {
            String nowStr = LocalDateTime.now().format(DateTimeFormatter.ofPattern(SystemInstance.DATE_PATTERN));
            if (StringUtils.isNotBlank(qrcodeParameter.getTimeExpire()) && Long.valueOf(nowStr) > Long.valueOf(qrcodeParameter.getTimeExpire())) {
                String errorJson = new Gson().toJson(getErrorMessage("9998", "qrcode not found", requestData.getQrcode()));
                return new ResponseEntity<String>(errorJson, HttpStatus.BAD_REQUEST);
            }
        }

        Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());
        if (merchant == null) {
            LOGGER.error("[REQUEST][Pi] createOrder() merchant not found. " + requestData.getQrcode());
            String errorJson = new Gson()
                    .toJson(getErrorMessage("9998", "intella account not found", requestData.getQrcode()));
            return new ResponseEntity<String>(errorJson, HttpStatus.BAD_REQUEST);
        }

        PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 3);
        if (paymentAccount == null) {
            LOGGER.error("[REQUEST][Pi] createOrder() payment account not found. " + requestData.getQrcode());
            String errorJson = new Gson()
                    .toJson(getErrorMessage("9998", "pi account not found", requestData.getQrcode()));
            return new ResponseEntity<String>(errorJson, HttpStatus.BAD_REQUEST);
        }

        Device device = deviceService.getOne(merchant, "APP-001");
        if (device == null) {
            LOGGER.error("[REQUEST][Pi] createOrder() device not found. " + requestData.getQrcode());
            String errorJson = new Gson().toJson(getErrorMessage("9998", "device not found", requestData.getQrcode()));
            return new ResponseEntity<String>(errorJson, HttpStatus.BAD_REQUEST);
        }

        int amount;
        if (qrcodeParameter.getCodeType().equals("3")) {
            long imotoAmount = ntuImotoService.checkoutImotoAmount(requestData.getQrcode());
            amount = imotoAmount == -1 ? 0 : Long.valueOf(imotoAmount).intValue();
        } else {
            amount = new Double(OnlinepayUtil.getTradeFee(qrcodeParameter, "10300")).intValue();
        }

        String randomBody = qrcodeService.getRandomBody(qrcodeParameter);

        String orderId = qrcodeService.getOrderId(qrcodeParameter);

        PiCreateOrderResponseData responseData = new PiCreateOrderResponseData();

        responseData.setPartnerId(requestData.getPartnerId());
        responseData.setPaymentAccount(paymentAccount.getAccount());
        responseData.setOrderId(orderId);
        responseData.setStoreName(paymentAccount.getAppId());
        responseData.setCreatedAt(DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
        responseData.setTotalAmount(amount);
        responseData.setFee(0);
        responseData.setDetail(randomBody);
        responseData.setAppTradeInfo(merchant.getName());

        try {
            LOGGER.info("[createOrder][responseData]" + new ObjectMapper().writeValueAsString(responseData));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        TradeDetail tradeDetail = tradeDetailService.getOne(orderId);
        if (Objects.nonNull(tradeDetail)) {
            String errorJson = new Gson()
                    .toJson(getErrorMessage("7000", "Trade exist.", requestData.getQrcode()));
            return new ResponseEntity<String>(errorJson, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        tradeDetail = new TradeDetail();
        tradeDetail.setStatus(SystemInstance.TRADE_CREATING_ORDER);
        tradeDetail.setOrderId(orderId);
        tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        tradeDetail.setAccountId(merchant.getAccountId());
        tradeDetail.setPayment(Double.valueOf(qrcodeParameter.getTotalFee()).longValue());
        tradeDetail.setDeviceRandomId(device);
        tradeDetail.setPaymentAccount(paymentAccount);
        tradeDetail.setMethod("10300");
        tradeDetail.setUserId(requestData.getPartnerId());
        tradeDetail.setServiceType(SystemInstance.TYPE_OLPAY);
        tradeDetail.setDescription(randomBody);
        tradeDetail.setQrcodeToken(requestData.getQrcode());
        TradeDetail entity = tradeDetailService.save(tradeDetail);

        // todo add polling job to request
        // https://sdk.piapp.com.tw/enterprise/payment/result

        try {
            if (entity == null) {
                LOGGER.error("[ERROR] createOrder() failed. " + new Gson().toJson(requestData));
                String errorJson = new Gson()
                        .toJson(getErrorMessage("9997", "create order failed.", requestData.getQrcode()));
                return new ResponseEntity<String>(errorJson, HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(responseData), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            String errorJson = new Gson().toJson(getErrorMessage("9999", "internal_error", ""));
            return new ResponseEntity<String>(errorJson, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/notify")
    public ResponseEntity<String> notify(@RequestBody PiNotifyRequestData requestData) {

        LOGGER.info("[NOTIFY][Pi] " + new Gson().toJson(requestData));
        try {
            TradeDetail tradeDetail = tradeDetailService.getOne(requestData.getOrderId());
            if (tradeDetail == null) {
                LOGGER.error("[ERROR] order not found.");
                PiNotifyResponseData piNotifyResponseData = new PiNotifyResponseData();
                piNotifyResponseData.setOrderId(requestData.getOrderId());
                piNotifyResponseData.setPaymentGateway(requestData.getPaymentGateway());
                piNotifyResponseData.setResult(false);
                piNotifyResponseData.setTotalAmount(requestData.getTotalAmount());
                piNotifyResponseData.setReceivedAt(DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
                return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(piNotifyResponseData),
                        HttpStatus.OK);
            } else {
                LOGGER.info("[Pi] notify order result.");
                tradeDetail.setMethod("10300");
                if (requestData.getResult().equals("01") && !SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setType(paymentGateway.indexOf(requestData.getPaymentGateway()));
                    tradeDetail.setPlatformPaidDate(requestData.getPaidAt());
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    tradeDetail.setPayment(requestData.getTotalAmount());
                    tradeDetail.setSystemOrderId(requestData.getTxId());
                    appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                } else if (!requestData.getResult().equals("01")) {
                    if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                        tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                    }
                }
                tradeDetailService.save(tradeDetail);

            }

            PiNotifyResponseData piNotifyResponseData = new PiNotifyResponseData();
            piNotifyResponseData.setOrderId(tradeDetail.getOrderId());
            piNotifyResponseData.setPaymentGateway(requestData.getPaymentGateway());
            piNotifyResponseData.setResult(true);
            piNotifyResponseData.setTotalAmount(requestData.getTotalAmount());
            piNotifyResponseData.setReceivedAt(DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
            return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(piNotifyResponseData),
                    HttpStatus.OK);

        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + ", " + Arrays.toString(e.getStackTrace()));
            String errorJson = new Gson().toJson(getErrorMessage("9999", "internal_error", ""));
            return new ResponseEntity<String>(errorJson, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private PiErrorResponseData getErrorMessage(String errorCode, String message, String qrCode) {
        PiErrorResponseData errorMessage = new PiErrorResponseData();
        errorMessage.setErrorCode(errorCode);
        errorMessage.setErrorMessage(message);
        errorMessage.setQrCode(qrCode);
        errorMessage.setPaymentGateway("pi_wallet");
        return errorMessage;
    }


    /**
     * 每天 凌晨四點 更新
     */
    @GetMapping("/updateAuthorization")
    public void updateAuthorization() {

        LOGGER.info("[AUTO][updateAuthorization]  START");
        List<PaymentAccount> paymentAccountList = paymentAccountService.listByBankId(Bank.NEW_PI.getId());

        paymentAccountList = paymentAccountList.stream().filter(this.distinctByKey(PaymentAccount::getAccount)).collect(Collectors.toList());

        paymentAccountList.parallelStream().forEach(paymentAccount -> newPiService.updateAuthorization(paymentAccount));
        LOGGER.info("[AUTO][updateAuthorization]  END");

    }

    private <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return object -> seen.putIfAbsent(keyExtractor.apply(object), Boolean.TRUE) == null;
    }

    @PostMapping("/paymentNotify")
    public ResponseEntity<String> paymentNotify(@RequestBody NotifyRequest notifyRequest) {
        LOGGER.info("[REQUEST] [paymentNotify]" + notifyRequest);

        TradeDetail tradeDetail = tradeDetailService.getOne(notifyRequest.getBill_id());

        if (Objects.nonNull(tradeDetail)) {
            PaymentAccount paymentAccount = paymentAccountService.getOne(tradeDetail.getAccountId(), Bank.NEW_PI.getId());

            if (Objects.nonNull(paymentAccount) && paymentAccount.getPlatformCheckCode().equals(notifyRequest.getSecret_key())) {
                if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                    tradeDetail.setMethod("15900");
                    tradeDetail.setPaymentAccount(paymentAccount);
                    tradeDetail.setSystemOrderId(notifyRequest.getTransaction_id());
                    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                    tradeDetailService.save(tradeDetail);
                    appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                }
            }
        }

        NotifyResponse notifyResponse = new NotifyResponse(notifyRequest);

        return new ResponseEntity<String>(JsonUtil.toJsonStr(notifyResponse),
                HttpStatus.OK);
    }


    @GetMapping("/createNewPi/{urlToken}/{amt}")
    public void createNewPi(@PathVariable String urlToken, @PathVariable String amt, HttpServletResponse response) throws Exception {

        LOGGER.info("[REQUEST] [createNewPi]" + urlToken + "\t" + amt);
        String url = newPiService.getPaymentUrl(amt, urlToken, "");
        response.sendRedirect(url);

    }
}
