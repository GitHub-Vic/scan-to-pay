package co.intella.controller.customize;

import co.intella.service.UUPayService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/api/UUPay")
public class UUPayController {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private UUPayService uuPayService;

    @GetMapping("/{totalFee}/{token}")
    public void doOLpay(HttpServletResponse response, @PathVariable String totalFee, @PathVariable String token) throws IOException {
        String url = null;
        try {
            url = uuPayService.doOLPay(totalFee, token);
        } catch (Exception e) {
            e.printStackTrace();
        }
        response.sendRedirect(url);
    }

    @PostMapping("/notify")
    public void doNotify(@RequestBody String request,@RequestHeader("Authorization") String authorization) {
        try {
            uuPayService.doNotify(request,authorization);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
