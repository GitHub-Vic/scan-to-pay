package co.intella.controller.customize;

import co.intella.model.Merchant;
import co.intella.model.PaymentAccount;
import co.intella.model.QrcodeParameter;
import co.intella.model.TradeDetail;
import co.intella.service.*;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

@Controller
@RequestMapping("/api/yuantaoffline")
public class YuanTaOfflineController {

    private final Logger LOGGER = LoggerFactory.getLogger(YuanTaOfflineController.class);

    @Resource
    private YuanTaOfflineService yuanTaOfflineService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private OrderService orderService;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private AppNotificationService appNotificationService;

    @Value("${api.url}")
    private String API_URL;

    @RequestMapping(method = RequestMethod.POST, value = "/getYuanTaOfflineUrl")
    public ResponseEntity<String> getLineUrl(@RequestBody String input) {

        LOGGER.info("[getYuanTaOfflineUrl]:" + input);

        JsonObject jsonObject = new Gson().fromJson(input, JsonObject.class);
        String totalFee = jsonObject.get("amount").getAsString();
        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(jsonObject.get("shortId").getAsString());
        try {
            LOGGER.info("[QrcodeParameter]:" + new ObjectMapper().writeValueAsString(qrcodeParameter));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        if (qrcodeParameter == null) {
            return null;
        }
        TradeDetail tradeDetail = null;
        String storeOrderNoNew;

        if (qrcodeParameter.getCodeType().equals("1")) {
            storeOrderNoNew = qrcodeParameter.getStoreOrderNo();
            totalFee = Integer.toString(Double.valueOf(qrcodeParameter.getTotalFee()).intValue());
        } else {
            storeOrderNoNew = orderService.createOrderId(qrcodeParameter.getMchId(), qrcodeParameter);
            LOGGER.info("[storeOrderNoNew]:" + storeOrderNoNew);
        }
        Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());

        PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 11);

        // add last 4 code
        String body = qrcodeService.getRandomBody(qrcodeParameter);
        qrcodeParameter.setBody(body);

        String precreateUrl = null;
        try {
            precreateUrl = yuanTaOfflineService.createOrder(tradeDetail, qrcodeParameter, totalFee, storeOrderNoNew, paymentAccount);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<String>(precreateUrl, HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/notify")
    public ResponseEntity<String> notify(@RequestParam(value = "Action") String action, @RequestParam(value = "Notify_time") String notifyTime,
                                         @RequestParam(value = "Notify_id") String notifyId, @RequestParam(value = "OrderNo") String orderId,
                                         @RequestParam(value = "Status") String status, @RequestParam(value = "hashDigest") String hashDigest) {
        LOGGER.info("[REQUEST] /api/yuantaoffline/result " + action + " " + orderId + " " + status);

        String result;
        String parametersResponse = action + notifyTime + notifyId + orderId + status;
        String checkHashDigest = null;
        try {
            checkHashDigest = yuanTaOfflineService.encryptSHA256(parametersResponse);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("[YuanTaOffline][notify]" + e.getMessage());
        }
        if (hashDigest.equals(checkHashDigest)) {
            TradeDetail tradeDetail = tradeDetailService.getOne(orderId);
            tradeDetail.setMethod("11100");
            LOGGER.info("[tradeDetail] " + tradeDetail);
            if (tradeDetail != null) {
                if (status.equals("S")) {
                    if (!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
                        tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
                        appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
                    }
                } else {
                    tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
                }
                tradeDetailService.save(tradeDetail);

                LOGGER.info("[YuanTaOffline][notify] get trade ");
                if (tradeDetail.getStatus().equals(SystemInstance.TRADE_SUCCESS)) {
                    result = "SUCCESS";
                    LOGGER.info("[YuanTaOffline][notify][result] " + result);
                } else {
                    result = "FAIL";
                    LOGGER.info("[YuanTaOffline][notify][result] " + result);
                }
            } else {
                LOGGER.info("[YuanTaOffline][notify] orderId: " + orderId + " is null ");
                result = "FAIL";
            }
        } else {
            //校驗碼有誤
            result = "FAIL";
        }
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }
}
