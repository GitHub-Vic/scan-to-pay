package co.intella.controller.customize;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;

import javax.annotation.Resource;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;

import co.intella.service.*;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import co.intella.domain.luckypay.LuckyPayCreateOrderResponseData;
import co.intella.domain.luckypay.LuckyPayErrorResponseData;
import co.intella.domain.luckypay.LuckyPayNotifyRequestData;
import co.intella.domain.luckypay.LuckyPayNotifyResponseData;
import co.intella.model.Device;
import co.intella.model.Merchant;
import co.intella.model.PaymentAccount;
import co.intella.model.QrcodeParameter;
import co.intella.model.TradeDetail;
import co.intella.utility.OnlinepayUtil;
import co.intella.utility.SystemInstance;

/**
 * @author Alex
 */
@Controller
@RequestMapping("/api/luckypay")
public class LuckypayController {

	private final Logger LOGGER = LoggerFactory.getLogger(LuckypayController.class);

	private String NOTIFY_SUCCESS = "1";

	private String NOTIFY_FAIL = "0";

	@Value("${luckypayintella.aes.key}")
	private String LUCKYPAY_AES_KEY;

	@Value("${luckypayintella.aes.iv}")
	private String LUCKYPAY_AES_IV;

	@Resource
	private LuckyPayService luckyPayService;

	@Resource
	private TradeDetailService tradeDetailService;

	@Resource
	private AppNotificationService appNotificationService;

	@Resource
	private QrcodeService qrcodeService;

	@Resource
	private MerchantService merchantService;

	@Resource
	private PaymentAccountService paymentAccountService;

	@Resource
	private DeviceService deviceService;

	@Resource
	private OnlinepayUtil OnlinepayUtil;

	@RequestMapping(method = RequestMethod.POST, value = "/createorder")
	public ResponseEntity<String> createOrder(@RequestBody String encryptedBody, HttpServletRequest request)
			throws JsonGenerationException, JsonMappingException, IOException {
		String cKey = LUCKYPAY_AES_KEY;
		byte[] bytes = LUCKYPAY_AES_IV.getBytes(StandardCharsets.UTF_8);
		try {
			LOGGER.info("[REQUEST][Luckypay]" + "[" + request.getHeader("X-FORWARDED-FOR") + "]");
			LOGGER.info("[REQUEST][Luckypay] request:" + encryptedBody);

			String decryptStr = null;
			String currencyTime = LocalDateTime.now().toString("yyyyMMddHHmmss");
			HashMap<String, String> requestData = new HashMap<String, String>();

			decryptStr = decrypt(cKey, bytes, encryptedBody);

			if ("decrypt error".equals(decryptStr)) {
				LOGGER.error("[ERROR] createOrder() data format error " + new Gson().toJson(requestData));
				String errorJson = new Gson()
						.toJson(getErrorMessage("8003", "Decryption Error.", requestData.get("token")));
				LuckyPayErrorResponseData luckyPayErrorResponseData = new Gson().fromJson(errorJson,
						LuckyPayErrorResponseData.class);
				LOGGER.info("[ERROR] responseData Encrypt:"
						+ encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(luckyPayErrorResponseData)));
				return new ResponseEntity<String>(
						encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(luckyPayErrorResponseData)),
						HttpStatus.BAD_REQUEST);
			}

			try {
				requestData = new ObjectMapper().readValue(decryptStr, HashMap.class);
			} catch (Exception e) {
				LOGGER.error("[ERROR] createOrder() data format error " + new Gson().toJson(requestData));
				String errorJson = new Gson().toJson(getErrorMessage("8002", "Data Format Error.", ""));
				LuckyPayErrorResponseData luckyPayErrorResponseData = new Gson().fromJson(errorJson,
						LuckyPayErrorResponseData.class);
				LOGGER.info("[ERROR] responseData Encrypt:"
						+ encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(luckyPayErrorResponseData)));
				return new ResponseEntity<String>(
						encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(luckyPayErrorResponseData)),
						HttpStatus.BAD_REQUEST);
			}
			LOGGER.info("[REQUEST][Luckypay] createOrder() request data : " + decryptStr);
			QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(requestData.get("token"));
			if (qrcodeParameter == null) {
				LOGGER.error("[REQUEST][Luckypay] createOrder() qrcode not found. " + requestData.get("token"));
				String errorJson = new Gson()
						.toJson(getErrorMessage("0202", "qrcode not found", requestData.get("token")));
				LuckyPayErrorResponseData luckyPayErrorResponseData = new Gson().fromJson(errorJson,
						LuckyPayErrorResponseData.class);
				LOGGER.info("[ERROR] responseData Encrypt:"
						+ encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(luckyPayErrorResponseData)));
				return new ResponseEntity<String>(
						encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(luckyPayErrorResponseData)),
						HttpStatus.BAD_REQUEST);
			} else {
				if (qrcodeParameter.getTimeExpire() != null && !"3".equals(qrcodeParameter.getCodeType()) && Long.valueOf(qrcodeParameter.getTimeExpire()) < Long.valueOf(currencyTime)) {
					LOGGER.error("[REQUEST][Luckypay] createOrder() QR Code Expired. " + requestData.get("token"));
					String errorJson = new Gson()
							.toJson(getErrorMessage("0201", "qrcode expired", requestData.get("token")));
					LuckyPayErrorResponseData luckyPayErrorResponseData = new Gson().fromJson(errorJson,
							LuckyPayErrorResponseData.class);
					LOGGER.info("[ERROR] responseData Encrypt:"
							+ encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(luckyPayErrorResponseData)));
					return new ResponseEntity<String>(
							encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(luckyPayErrorResponseData)),
							HttpStatus.BAD_REQUEST);
				}
			}

			Merchant merchant = merchantService.getOne(qrcodeParameter.getMchId());
			if (merchant == null) {
				LOGGER.error(
						"[REQUEST][Luckypay] createOrder() merchant merchant not found. " + requestData.get("token"));
				String errorJson = new Gson()
						.toJson(getErrorMessage("0001", "Merchant Unavailable", requestData.get("token")));
				LOGGER.info("[createOrder][responseData] responseData Encrypt:"
						+ encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(errorJson)));
				LuckyPayErrorResponseData luckyPayErrorResponseData = new Gson().fromJson(errorJson,
						LuckyPayErrorResponseData.class);
				LOGGER.info("[ERROR] responseData Encrypt:"
						+ encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(luckyPayErrorResponseData)));
				return new ResponseEntity<String>(
						encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(luckyPayErrorResponseData)), HttpStatus.BAD_REQUEST);
			}

			PaymentAccount paymentAccount = paymentAccountService.getOne(qrcodeParameter.getMchId(), 30);
			if (paymentAccount == null) {
				LOGGER.error(
						"[REQUEST][Luckypay] createOrder() payment account not found. " + requestData.get("token"));
				String errorJson = new Gson()
						.toJson(getErrorMessage("0002", "Do Not Support", requestData.get("token")));
				LuckyPayErrorResponseData luckyPayErrorResponseData = new Gson().fromJson(errorJson,
						LuckyPayErrorResponseData.class);
				LOGGER.info("[ERROR] responseData Encrypt:"
						+ encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(luckyPayErrorResponseData)));
				return new ResponseEntity<String>(
						encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(luckyPayErrorResponseData)), HttpStatus.BAD_REQUEST);
			}

			Device device = deviceService.getOne(merchant, "skb0001");
			if (device == null) {
				LOGGER.error("[REQUEST][Luckypay] createOrder() device not found. " + requestData.get("token"));
				String errorJson = new Gson()
						.toJson(getErrorMessage("9998", "device not found.", requestData.get("token")));
				LuckyPayErrorResponseData luckyPayErrorResponseData = new Gson().fromJson(errorJson,
						LuckyPayErrorResponseData.class);
				LOGGER.info("[ERROR] responseData Encrypt:"
						+ encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(luckyPayErrorResponseData)));
				return new ResponseEntity<String>(
						encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(luckyPayErrorResponseData)), HttpStatus.BAD_REQUEST);
			}

			int amount;
			if (qrcodeParameter.getCodeType().equals("3")) {
				amount = 0;
			} else {
				amount = new Double(OnlinepayUtil.getTradeFee(qrcodeParameter, "13000")).intValue();
				if (amount > 30000) {
					LOGGER.error("[REQUEST][Luckypay] createOrder() amount exceeds limit. " + requestData.get("token"));
					String errorJson = new Gson()
							.toJson(getErrorMessage("0102", "Amount Exceeds Limit.", requestData.get("token")));
					LuckyPayErrorResponseData luckyPayErrorResponseData = new Gson().fromJson(errorJson,
							LuckyPayErrorResponseData.class);
					LOGGER.info("[ERROR] responseData Encrypt:"
							+ encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(luckyPayErrorResponseData)));
					return new ResponseEntity<String>(
							encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(luckyPayErrorResponseData)),
							HttpStatus.BAD_REQUEST);
				}
			}

			String randomBody = qrcodeService.getRandomBody(qrcodeParameter);
			String orderId = qrcodeService.getOrderId(qrcodeParameter);
			String returnCode = "0000";

			LuckyPayCreateOrderResponseData responseData = new LuckyPayCreateOrderResponseData();
			responseData.setStoreId(paymentAccount.getAccount());
			responseData.setRegisterId(paymentAccount.getHashKey());
			responseData.setStoreName(merchant.getName());
			responseData.setTerminalId(paymentAccount.getAppId());
			responseData.setOrderId(orderId);
			responseData.setAmount(String.valueOf(amount * 100));
			responseData.setReturnCode(returnCode);
			String responseDataEncrypt = encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(responseData));
			LOGGER.info("[createOrder][responseData]" + new ObjectMapper().writeValueAsString(responseData));
			LOGGER.info("[createOrder][responseData] responseData Encrypt:" + responseDataEncrypt);
			TradeDetail tradeDetail = new TradeDetail();
			tradeDetail.setStatus(SystemInstance.TRADE_WAITING);
			tradeDetail.setOrderId(orderId);
			tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
			tradeDetail.setAccountId(merchant.getAccountId());
			tradeDetail.setPayment(amount);
			tradeDetail.setDeviceRandomId(device);
			tradeDetail.setPaymentAccount(paymentAccount);
			tradeDetail.setMethod("13000");
			tradeDetail.setServiceType(SystemInstance.TYPE_OLPAY);
			tradeDetail.setDescription(randomBody);

			TradeDetail entity = tradeDetailService.save(tradeDetail);
			if (entity == null) {
				LOGGER.error("[ERROR] createOrder() failed. " + new Gson().toJson(requestData));
				String errorJson = new Gson()
						.toJson(getErrorMessage("9997", "create order failed.", requestData.get("token")));
				LuckyPayErrorResponseData luckyPayErrorResponseData = new Gson().fromJson(errorJson,
						LuckyPayErrorResponseData.class);
				LOGGER.info("[ERROR] responseData Encrypt:"
						+ encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(luckyPayErrorResponseData)));
				return new ResponseEntity<String>(
						encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(luckyPayErrorResponseData)),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
			return new ResponseEntity<String>(encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(responseData)),
					HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("[EXCEPTION] " + e.getMessage());
			String errorJson = new Gson().toJson(getErrorMessage("9995", "Service Temporarily Unavailable.", ""));
			LuckyPayErrorResponseData luckyPayErrorResponseData = new Gson().fromJson(errorJson,
					LuckyPayErrorResponseData.class);
			LOGGER.info("[ERROR] responseData Encrypt:"
					+ encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(luckyPayErrorResponseData)));
			return new ResponseEntity<String>(encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(luckyPayErrorResponseData)),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/notify")
	public ResponseEntity<String> notify(@RequestBody String encryptedBody, HttpServletRequest request)
			throws JsonParseException, JsonMappingException, IOException {
		byte[] bytes = LUCKYPAY_AES_IV.getBytes(StandardCharsets.UTF_8);
		LuckyPayNotifyResponseData responseData = new LuckyPayNotifyResponseData();
		LuckyPayNotifyRequestData requestData = new LuckyPayNotifyRequestData();
		LOGGER.info("[REQUEST][LuckyPay][notify]" + request.getHeader("X-FORWARDED-FOR"));
		LOGGER.info("[REQUEST][LuckyPay][notify] encryptedBody :" + encryptedBody);
		String cKey = LUCKYPAY_AES_KEY;
		String decryptStr = null;
		String resultCode = null;
		int amount = 0;

		try {
			decryptStr = decrypt(cKey, bytes, encryptedBody);
			LOGGER.info("[REQUEST][LuckyPay][notify] decryptStr :" + decryptStr);
			requestData = new Gson().fromJson(decryptStr, LuckyPayNotifyRequestData.class);
		} catch (Exception e) {
			LOGGER.error("[REQUEST][Luckypay] notify() Decryption Error. ");
		}

		if (!(requestData.getResultCode() == null || requestData.getAmount() == null)) {
			resultCode = requestData.getResultCode();
			amount = Integer.parseInt(requestData.getAmount());
			amount = amount / 100;
		} else {
			LOGGER.error("[REQUEST][Luckypay] notify() resultCode FAIL. ");
			resultCode = "0";
		}

		TradeDetail tradeDetail = tradeDetailService.getOne(requestData.getOrderNo());
        tradeDetail.setMethod("13000");
		if (NOTIFY_SUCCESS.equals(resultCode) && !(tradeDetail == null) && !(requestData.getTransactionId() == null)) {
			LOGGER.info("[LuckyPay][notify][success]");
			if(!SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
			    tradeDetail.setStatus(SystemInstance.TRADE_SUCCESS);
			    tradeDetail.setSystemOrderId(requestData.getTransactionId());
			    tradeDetail.setPayment(Long.parseLong(Integer.toString(amount)));
			    tradeDetailService.save(tradeDetail);
				appNotificationService.notifyApp(tradeDetail, merchantService.getOne(tradeDetail.getAccountId()));
			}

			responseData.setReturnCode("0000");
			responseData.setAmount(Integer.toString(amount));
			responseData.setOrderId(requestData.getOrderNo());
			responseData.setStore(requestData.getStore());
			LOGGER.info("[notify][responseData]" + new ObjectMapper().writeValueAsString(responseData));
			return new ResponseEntity<String>(encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(responseData)),
					HttpStatus.OK);
		} else if (NOTIFY_FAIL.equals(resultCode) && !(tradeDetail == null)) {
			LOGGER.info("[LuckyPay][notify][fail]");
			tradeDetail.setStatus(SystemInstance.TRADE_FAIL);
			tradeDetailService.save(tradeDetail);
			responseData.setStore(requestData.getStore());
			responseData.setOrderId(requestData.getOrderNo());
			responseData.setReturnCode("9999");
			LOGGER.info("[notify][responseData]" + new ObjectMapper().writeValueAsString(responseData));
			return new ResponseEntity<String>(encrypt(cKey, bytes, new ObjectMapper().writeValueAsString(responseData)),
					HttpStatus.BAD_REQUEST);
		} else {
			LOGGER.info("[LuckyPay][notify][fail] incorrect parameter or no such order_no");
			return new ResponseEntity<String>(encrypt(cKey, bytes, "Incorrect parameter or no such order_no"),
					HttpStatus.BAD_REQUEST);
		}
	}

	private LuckyPayErrorResponseData getErrorMessage(String errorCode, String message, String qrCode) {
		LuckyPayErrorResponseData errorMessage = new LuckyPayErrorResponseData();
		errorMessage.setReturnCode(errorCode);
		errorMessage.setErrorMessage(message);
		errorMessage.setQrCode(qrCode);
		errorMessage.setPaymentGateway("luckypay");
		return errorMessage;
	}

	public static String decrypt(String key, byte[] initVector, String encrypted) {
		try {

			IvParameterSpec iv = new IvParameterSpec(initVector);
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

			Base64.Decoder decode = Base64.getDecoder();

			byte[] original = cipher.doFinal(

					decode.decode(encrypted));

			return new String(original);
		} catch (Exception ex) {
			return "decrypt error";
		}
	}

	public static String encrypt(String key, byte[] initVector, String value) {
		try {
			IvParameterSpec iv = new IvParameterSpec(initVector);
			SecretKeySpec skeySpec = new SecretKeySpec((key.getBytes("UTF-8")), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");

			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

			byte[] encrypted = cipher.doFinal(value.getBytes());

			Base64.Encoder encode = Base64.getEncoder();

			return encode.encodeToString(encrypted);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}

}
