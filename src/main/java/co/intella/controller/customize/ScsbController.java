package co.intella.controller.customize;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import co.intella.controller.BaseController;
import co.intella.model.RequestHeader;
import co.intella.service.MerchantService;
import co.intella.service.OrderService;
import co.intella.service.QrcodeService;
import co.intella.service.ScsbService;

@Controller
@RequestMapping("/api/scsb/")
public class ScsbController extends BaseController {

	private final Logger LOGGER = LoggerFactory.getLogger(ScsbController.class);

	@Resource
	private MerchantService merchantService;

	@Resource
	private QrcodeService qrcodeService;

	@Resource
	private OrderService orderService;

	@Resource
	private ScsbService scsbService;

	@Value("${api.url}")
	private String API_URL;

	@RequestMapping(value = "/credit")
	public ResponseEntity<String> credit(@RequestBody String body, HttpServletRequest request) throws IOException {
		LOGGER.debug("[credit][scsb] body =>" + body);
		String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
		String clientIP = request.getHeader("X-FORWARDED-FOR");

		JsonObject jsonObject = new Gson().fromJson(body, JsonObject.class);
		JsonObject header = jsonObject.getAsJsonObject("Header");
		JsonPrimitive data = jsonObject.getAsJsonPrimitive("Data");

		RequestHeader requestHeader = new Gson().fromJson(header.toString(), RequestHeader.class);
		JsonObject requestJson = new Gson().fromJson(data.getAsString(), JsonObject.class);

		String integrateMchId = requestHeader.getMerchantId();

		if (requestJson.has("Body")) {
			String bodyRandom = requestJson.get("Body").getAsString();
			if ("Payment".equals(requestHeader.getServiceType())) {
				bodyRandom += requestJson.has("CardId") ? "-" + requestJson.get("CardId").getAsString().substring(12) : "";
			}
			requestJson.remove("Body");
			requestJson.addProperty("Body", bodyRandom);
			data = new JsonPrimitive(new Gson().toJson(requestJson));
		}

		LOGGER.debug("[dispatch credit][scsb][" + sessionId + "]");
		requestHeader.setMethod("22600");

		String generalResponseData = null;

		try {
			generalResponseData = scsbService.creditAuth(requestHeader, data, integrateMchId, clientIP);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.debug("[Exception][22600]" + e.getMessage());
		}

		LOGGER.debug("[RESPONSE][22600][" + sessionId + "]" + generalResponseData);

		return new ResponseEntity<String>(generalResponseData, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/credit3D")
	public ResponseEntity<String> credit3D(@RequestBody String body, HttpServletRequest request) throws IOException {
		
		String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
		String clientIP = request.getHeader("X-FORWARDED-FOR");

		JsonObject jsonObject = new Gson().fromJson(body, JsonObject.class);
		JsonObject header = jsonObject.getAsJsonObject("Header");
		JsonPrimitive data = jsonObject.getAsJsonPrimitive("Data");

		RequestHeader requestHeader = new Gson().fromJson(header.toString(), RequestHeader.class);
		JsonObject requestJson = new Gson().fromJson(data.getAsString(), JsonObject.class);

		String integrateMchId = requestHeader.getMerchantId();

		if (requestJson.has("Body")) {
			String bodyRandom = requestJson.get("Body").getAsString();
			if ("Payment".equals(requestHeader.getServiceType())) {
				bodyRandom += requestJson.has("CardId") ? "-" + requestJson.get("CardId").getAsString().substring(12) : "";
			}
			requestJson.remove("Body");
			requestJson.addProperty("Body", bodyRandom);
			data = new JsonPrimitive(new Gson().toJson(requestJson));
		}

		LOGGER.debug("[dispatch credit][scsb3D][" + sessionId + "]");
		requestHeader.setMethod("23900");

		String generalResponseData = null;

		try {
			generalResponseData = scsbService.creditAuth3D(requestHeader, data, integrateMchId, clientIP);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.debug("[Exception][23900]" + e.getMessage());
		}

		LOGGER.debug("[RESPONSE][23900][" + sessionId + "]" + generalResponseData);

		return new ResponseEntity<String>(generalResponseData, HttpStatus.OK);
	}
	
	@RequestMapping(method= RequestMethod.POST, value = "/credit3D/Notify")
	public void notify(HttpServletRequest request, HttpServletResponse response) throws Exception {
	    LOGGER.info("[REQUEST][scsb3D][Input] /api/scsb/credit3D/Notify   orderId: " + request.getParameter("lidm"));
		LOGGER.info("[REQUEST][scsb3D][Input] /api/scsb/credit3D/Notify   errcode: " + request.getParameter("errcode"));
		String generalResponseData = null;
		try {
			generalResponseData = scsbService.scsb3DNotify(request.getParameter("lidm"), request.getParameter("errcode"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		LOGGER.info("[RESPONSE][scsb3D][Redirect] " + generalResponseData);
		response.sendRedirect(generalResponseData);
	}


	@RequestMapping(method= RequestMethod.POST, value = "/upopNotify")
	public void upopNotify(HttpServletRequest request, HttpServletResponse response , @RequestBody String body) throws Exception {
		LOGGER.info("[REQUEST][scsb3D][Input] /api/scsb/upopNotify " +  body);

//		String generalResponseData = null;
//		try {
//			generalResponseData = scsbService.scsb3DNotify(request.getParameter("lidm"), request.getParameter("errcode"));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		LOGGER.info("[RESPONSE][scsb3D][Redirect] " + generalResponseData);
//		response.sendRedirect(generalResponseData);
	}
}