package co.intella.controller.customize;

import co.intella.model.RequestHeader;
import co.intella.service.FuBonBankService;
import co.intella.service.MerchantService;
import co.intella.service.PaymentService;
import co.intella.service.QrcodeService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/api/fubonbank")
public class FubonBankController {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Resource
    private MerchantService merchantService;

    @Resource
    private QrcodeService qrcodeService;

    @Resource
    private FuBonBankService fubonBankService;

    @Resource
    private PaymentService fuBonService;

    @RequestMapping(value = "/credit")
    public ResponseEntity<String> credit(@RequestBody String body, HttpServletRequest request, HttpServletResponse response) throws Exception {

        String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
        String clientIP = request.getHeader("X-FORWARDED-FOR");

        JsonObject jsonObject = new Gson().fromJson(body, JsonObject.class);
        JsonObject header = jsonObject.getAsJsonObject("Header");
        JsonPrimitive data = jsonObject.getAsJsonPrimitive("Data");

        RequestHeader requestHeader = new Gson().fromJson(header.toString(), RequestHeader.class);
        JsonObject requestJson = new Gson().fromJson(data.getAsString(), JsonObject.class);

        String integrateMchId = requestHeader.getMerchantId();

        if (requestJson.has("Body")) {
            String bodyRandom = requestJson.get("Body").getAsString();
            if ("Payment".equals(requestHeader.getServiceType())) {
                bodyRandom += requestJson.has("CardId") ? "-" + requestJson.get("CardId").getAsString().substring(12) : "";
            }
            requestJson.remove("Body");
            requestJson.addProperty("Body", bodyRandom);
            data = new JsonPrimitive(new Gson().toJson(requestJson));
        }

        LOGGER.info("[dispatch credit][fubonbank3D][" + sessionId + "]");
        requestHeader.setMethod("23800");

        String generalResponseData = null;

        try {
            generalResponseData = fubonBankService.creditAuth(requestHeader, data, integrateMchId, clientIP);
        } catch (Exception e) {
            e.printStackTrace();

        }

        LOGGER.info("[RESPONSE][23800][" + sessionId + "]" + generalResponseData);
        return new ResponseEntity<String>(generalResponseData, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/Notify")
    public void notify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOGGER.info("[REQUEST][FubonBank3D][Input] /api/fubonbank/Notify " + request.getParameter("orderID"));

        String generalResponseData = null;
        try {
            generalResponseData = fubonBankService.fubonBankNotify(request.getParameter("orderID"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        LOGGER.info("[RESPONSE][FubonBank3D][Redirect] " + generalResponseData);
        response.sendRedirect(generalResponseData);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/doECCapture")
    public void doECCapture(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOGGER.info("[doECCapture] START");
        fuBonService.doECCapture();
        LOGGER.info("[doECCapture] END");
    }
}
