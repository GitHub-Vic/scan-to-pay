package co.intella.controller.salesCRM;

import co.intella.domain.crm.TicketReviewData;
import co.intella.model.*;
import co.intella.model.SalesManage.Status;
import co.intella.model.SalesManage.WorkflowConfig;
import co.intella.service.CRM.TicketHistoryService;
import co.intella.service.CRM.TicketService;
import co.intella.service.CRM.WorkflowService;
import co.intella.service.MerchantService;
import co.intella.service.SaleService;
import co.intella.service.TradeDetailOtherInfoService;
import co.intella.service.TradeDetailService;
import co.intella.utility.GeneralResponseMessageUtil;
import co.intella.utility.WorkflowAction;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Alex
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@Controller
@RequestMapping("/api/ticket")
public class TicketController {

    private final Logger LOGGER = LoggerFactory.getLogger(TicketController.class);

    @Autowired
    private SaleService saleService;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private WorkflowService workflowService;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private TicketHistoryService ticketHistoryService;

    @Autowired
    private TradeDetailService tradeDetailService;

    @Resource
    private TradeDetailOtherInfoService tradeDetailOtherInfoService;

    @RequestMapping(method = RequestMethod.POST, value = "/create")
    public ResponseEntity<String> createTicket(@RequestBody String json) throws Exception {
        JsonObject jsonObject = new Gson().fromJson(json, JsonObject.class);

        String salesId = jsonObject.get("sales").getAsString();
        String merchantId = jsonObject.get("merchant").getAsString();

        LOGGER.info("[ticket][Create] sales:" + salesId + "merchantId" + merchantId);


        Sale sales = saleService.getOneById(Long.valueOf(salesId));
        if (sales == null) {
            return new ResponseEntity<String>("No such sales", HttpStatus.OK);
        }
        Merchant merchant = merchantService.getOne(merchantId);
        if (merchant == null) {
            return new ResponseEntity<String>("No such merchant", HttpStatus.OK);
        }

        Ticket ticket = ticketService.getOneIsActive(merchant);
        if (ticket != null) {
            throw new Exception("Ticket already exists");
        }
        ticket = workflowService.createTicket(merchant, sales);

        return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage("Ticket ID :" + ticket.getId()), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/reset")
    public ResponseEntity<String> resetTicket(@RequestBody String json) throws Exception {
        JsonObject jsonObject = new Gson().fromJson(json, JsonObject.class);

        String salesId = jsonObject.get("sales").getAsString();
        String merchantId = jsonObject.get("merchant").getAsString();

        Sale sales = saleService.getOneById(Long.valueOf(salesId));
        if (sales == null) {
            return new ResponseEntity<String>("No such sales", HttpStatus.OK);
        }
        Merchant merchant = merchantService.getOne(merchantId);
        if (merchant == null) {
            return new ResponseEntity<String>("No such merchant", HttpStatus.OK);
        }

        Ticket ticket = ticketService.getOneIsActive(merchant);

        ticket = workflowService.resetTicket(ticket, sales);

        return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage("Ticket ID " + ticket.getId() + " is reset"), HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.POST, value = "/toNextStep")
    public ResponseEntity<String> toNextStep(@RequestBody String json) throws Exception {
        JsonObject jsonObject = new Gson().fromJson(json, JsonObject.class);

        String ticketId = jsonObject.get("ticketId").getAsString();
        String salesId = jsonObject.get("sales").getAsString();
        String action = jsonObject.get("action").getAsString();
        String message = jsonObject.has("message") ? jsonObject.get("message").getAsString() : null;


        LOGGER.info("[toNextStep] ticketId:" + ticketId + " salesId:" + salesId + " action:" + action);

        Ticket ticket = ticketService.getOneById(Long.valueOf(ticketId));
        if (ticket == null) {
            throw new Exception("Ticket is not existed");
        }

        LOGGER.info("[ticket Status][Now]:" + ticket.getStatus());

        Sale sales = saleService.getOneById(Long.valueOf(salesId));

        if (action.equals("APPROVE")) {
            ticket = workflowService.nextStep(ticket, WorkflowAction.APPROVE, sales, null);
            Merchant merchant = merchantService.getOne(ticket.getMerchant().getAccountId());
            if (ticket.getStatus().equals(3)) {
                merchant.setStatus("active");
                merchantService.save(merchant);
            }

        } else if (action.equals("REJECT")) {
            ticket = workflowService.nextStep(ticket, WorkflowAction.REJECT, sales, message);
            Merchant merchant = merchantService.getOne(ticket.getMerchant().getAccountId());
            merchant.setStatus("rejected");
            merchantService.save(merchant);
        } else if (action.equals("RETURN")) {
            ticket = workflowService.nextStep(ticket, WorkflowAction.RETURN, sales, message);
        }

        LOGGER.info("[ticket Status][After]:" + ticket.getStatus());

        return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage("Ticket is " + action + "ED"), HttpStatus.OK);


    }


    @RequestMapping(method = RequestMethod.GET, value = "/getHistoryList/{salesId}")
    public ResponseEntity<String> getHistoryList(@PathVariable String salesId) throws JsonProcessingException {
        LOGGER.info("[getHistoryList]");

        Sale sales = saleService.getOneById(Long.valueOf(salesId));
        LOGGER.info("[getSales]" + sales.getAccountId());

        List<TicketHistory> a = ticketHistoryService.listBySales(sales);

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

        String requestJson = ow.writeValueAsString(a);

        //List<Ticket> tickets = ticketService.listHistory(sales);
        return new ResponseEntity<String>(requestJson, HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/getReviewList/{salesId}")
    public ResponseEntity<String> getReviewList(@PathVariable String salesId) throws JsonProcessingException {

        LOGGER.info("[getReviewList]");

        Sale sales = saleService.getOneById(Long.valueOf(salesId));
        LOGGER.info("[getSales]" + sales.getAccountId());


        List<TicketReviewData> ticketReviewDataList = new ArrayList<TicketReviewData>();
        List<Ticket> tickets = ticketService.listForReview(sales);

        WorkflowConfig config = workflowService.getCONFIG();
        LOGGER.info("[getConfig]");
        List<Status> statuses = config.getStatuses();

        for (Ticket t : tickets) {
            TicketReviewData ticketReviewData = new TicketReviewData();
            ticketReviewData.setTicketId(t.getId());
            ticketReviewData.setCreator(t.getCreator().getName());
            ticketReviewData.setMerchantName(t.getMerchant().getName());
            ticketReviewData.setAccountId(t.getMerchant().getAccountId());
            ticketReviewData.setStatus(statuses.get(t.getStatus()).getName());
            ticketReviewData.setSystime(new DateTime(t.getCreateDate()).toString("yyyy/MM/dd HH:mm:ss"));
            ticketReviewDataList.add(ticketReviewData);

        }

        String result = new ObjectMapper().writeValueAsString(ticketReviewDataList);

        return new ResponseEntity<String>(result, HttpStatus.OK);

    }

    @ResponseBody
    @GetMapping("/cardNumberForPrint")
    public String getCardIdByOrderId(@RequestParam("orderId") String orderId) {
        LOGGER.info("[cardNumberForPrint]  orderId => " + orderId);
        TradeDetail tradeDetail = tradeDetailService.getOne(orderId);
//        String result = "";
//        if (Objects.nonNull(tradeDetail)) {
//            TradeDetailOtherInfo tradeDetailOtherInfo = tradeDetailOtherInfoService.getOneByUNHEXUuid(tradeDetail.getTradeDetailRandomId().toString());
//            if (Objects.nonNull(tradeDetailOtherInfo)) {
//                result = StringUtils.defaultString(tradeDetailOtherInfo.getCardNumberForPrint(), "");
//            }
//        }
        return tradeDetail.getUserId();
    }

    @ResponseBody
    @GetMapping("/cardId")
    public String getCardIdByCardNumberForPrint(@RequestParam("cardNumberForPrint") String cardNumberForPrint) {
        LOGGER.info("[getCardIdByCardNumberForPrint]  cardNumberForPrint => " + cardNumberForPrint);
        return StringUtils.defaultIfBlank(tradeDetailOtherInfoService.getCardIdByCardNumberForPrint(cardNumberForPrint), "");
    }

}
