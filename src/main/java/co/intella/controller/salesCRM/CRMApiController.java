package co.intella.controller.salesCRM;

import co.intella.domain.contratStore.PaymentAccountIncome;
import co.intella.domain.crm.CrmMerchantData;
import co.intella.domain.crm.CrmRoleData;
import co.intella.domain.crm.CrmSaleData;
import co.intella.model.*;
import co.intella.service.*;
import co.intella.utility.GeneralResponseMessageUtil;
import co.intella.utility.SystemInstance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * @author Andy Lin
 */
@CrossOrigin
@Controller
@RequestMapping("/api/crm")
public class CRMApiController {

    private final Logger LOGGER = LoggerFactory.getLogger(CRMApiController.class);

    @Resource
    private PermissionService permissionService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private SaleService saleService;

    @Resource
    private RoleService roleService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private BankService bankService;

    @RequestMapping( method= RequestMethod.POST, value="/bank/list" )
    public ResponseEntity<String> createOrUpdate(@RequestBody String request) throws JsonProcessingException {
        LOGGER.info(SystemInstance.REQUEST_STRING + "api/crm/bank/list. " + request);
        String response;

        List<Bank> list = bankService.listAll();
        response = new ObjectMapper().writeValueAsString(list);

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @RequestMapping( method= RequestMethod.POST, value="/bank/save" )
    public ResponseEntity<String> savePaymentAccount(@RequestBody PaymentAccountIncome input) {
        LOGGER.info("[TEST]" + new Gson().toJson(input));
        Merchant merchant = merchantService.getOne(input.getMerchantId());

        if(merchant == null) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Merchant not exists."), HttpStatus.OK);
        }

        Bank bank = bankService.getOne(input.getBankId());

        if(bank == null) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Payment platform not exists."), HttpStatus.OK);
        }

        PaymentAccount paymentAccount = paymentAccountService.getOne(merchant.getAccountId(), bank.getBankId());

        if(paymentAccount == null) {
            paymentAccount = new PaymentAccount();
        }

        paymentAccount.setDescription(bank.getDescription());
        paymentAccount.setAccount(input.getAccount());
        paymentAccount.setBank(bank);
        paymentAccount.setMerchant(merchant);
        paymentAccount.setDefault(true);

        PaymentAccount entity = paymentAccountService.save(paymentAccount);
        return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(new Gson().toJson(entity)), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/role/setpermission")
    public ResponseEntity<String> setPermission(@RequestBody String request) {

        LOGGER.info(SystemInstance.REQUEST_STRING + "/role/setpermission. " + request);

        JsonObject jsonObject = new Gson().fromJson(request, JsonObject.class);

        try{
            JsonArray jsonArray = jsonObject.get("roleList").getAsJsonArray();

            for (JsonElement jsonElement:jsonArray) {
                JsonElement jsonElementId = jsonElement.getAsJsonObject().get("roleId");
                JsonElement jsonElementPermissions = jsonElement.getAsJsonObject().get("permissions");
                Role role;
                List<Permission> listOfPermission;

                if(jsonElementId == null || jsonElementPermissions == null) {
                    LOGGER.info("[setPermission]" + "'roleId' or 'permissions' is null.");
                } else {
                    role = roleService.getOneById(jsonElementId.getAsLong());
                    if(role == null) {
                        LOGGER.info("[setPermission]" + "No Such roleId :" + jsonElementId.getAsString());
                    } else {
                        //todo: recognizes all name of the privilege
                        listOfPermission = permissionService.getListByRoleId(role.getId());
                        boolean isPermissionExist;
                        boolean isRedundant;
                        String action;
                        for (Permission permission: listOfPermission ) {
                            isRedundant = true;
                            for (JsonElement jsonElement2:jsonElementPermissions.getAsJsonArray() ) {
                                action = jsonElement2.getAsJsonObject().get("action").getAsString();
                                if(action.equals(permission.getAction())) {
                                    isRedundant = false;
                                    break;
                                }
                            }
                            if(isRedundant) {
                                permissionService.delete(permission);
                            }
                        }
                        for (JsonElement jsonElement2:jsonElementPermissions.getAsJsonArray() ) {
                            action = jsonElement2.getAsJsonObject().get("action").getAsString();
                            isPermissionExist = false;
                            for (Permission permission: listOfPermission ) {
                                if(action.equals(permission.getAction())) {
                                    isPermissionExist = true;
                                    break;
                                }
                            }
                            if(!isPermissionExist) {
                                Permission newPermission = new Permission();
                                newPermission.setRoleId(role.getId());
                                newPermission.setAction(action);
                                permissionService.save(newPermission);
                            }
                        }
                    }
                }
            }

            return new ResponseEntity<String>("Permission-setting success.", HttpStatus.OK);

        } catch(Exception e) {
            return new ResponseEntity<String>("Permission-setting failed.", HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/merchant/query")
    public ResponseEntity<String> queryMerchant(@RequestBody Map<String, String> request) {
        String response;
        LOGGER.info(SystemInstance.REQUEST_STRING + "api/crm/merchant/query. " + request);

        String accountId = request.get("accountId") == null ? "":request.get("accountId");
        String name = request.get("name") == null ? "":request.get("name");

        List<Merchant> list;

        list = merchantService.queryWithLike(accountId, name);

        List<CrmMerchantData> list2 = new ArrayList<CrmMerchantData>();

        for (Merchant merchant:list) {
            list2.add(getMerchantFormat(merchant));
        }

        try {
            response = new ObjectMapper().writeValueAsString(list2);
        } catch (JsonProcessingException e) {
            LOGGER.error("[EXCEPTION] queryMerchant() " + e.toString());
            e.printStackTrace();
            return new ResponseEntity<String>("JsonProcessingException", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/merchant/create")
    public ResponseEntity<String> createMerchant(@RequestBody Map<String, String> request) {
        LOGGER.info(SystemInstance.REQUEST_STRING + "api/crm/merchant/create. " + request);
        String response;

        try{
            Merchant newMerchant = new Merchant();
            Sale sale = saleService.getOneById(Long.valueOf(request.get("saleId")));

            if (merchantService.getOne(request.get("accountId")) != null) {
                response = "Account already exists.";
            }else if (sale == null) {
                response = "The saleId not Exists.";
            }else {
                String activateToken = UUID.randomUUID().toString().substring(0, 15);

                newMerchant.setName(request.get("name"));
                newMerchant.setEmail(request.get("email"));
                newMerchant.setCreator(Long.valueOf(request.get("creator")));
                newMerchant.setPin(request.get("pin"));
                newMerchant.setAccountId(request.get("accountId"));
                newMerchant.setComId(request.get("comId"));
                newMerchant.setPhone(request.get("phone"));
                newMerchant.setSignDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
                newMerchant.setStatus("unverified");
                newMerchant.setPaycodeToken(request.get("paycodeToken") == null? "XXXXXX" : request.get("paycodeToken"));
                newMerchant.setActivateToken(activateToken);
                newMerchant.setRefundPin(request.get("refundPin") == null? "0000" : request.get("refundPin"));
                newMerchant.setSaleId(sale);
                Merchant merchant1 = merchantService.save(newMerchant);

                response = merchant1 == null? "Merchant creates failed.":"Merchant creates successfully.";
            }

            return new ResponseEntity<String>(response, HttpStatus.OK);
        } catch(NullPointerException e){
            LOGGER.error("[EXCEPTION]" + e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<String>("Null Data is detected.", HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION]" + e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<String>("Merchant creates failed.", HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/merchant/edit")
    public ResponseEntity<String> getEditMerchantInfo(@RequestBody String request) throws JsonProcessingException {
        LOGGER.info(SystemInstance.REQUEST_STRING + "api/crm/merchant/edit. " + request);
        String response;

        JsonObject jsonObject = new Gson().fromJson(request, JsonObject.class);
        String accountId = jsonObject.get("accountId") == null ? "0":jsonObject.get("accountId").getAsString();


        if("0".equals(accountId)) {
            response = "'accountId' is null";
        }else {
            Merchant merchant = merchantService.getOne(accountId);
            if(merchant == null) {
                response = "No Such merchant-accountId :" + accountId;
            }else {
                response = new ObjectMapper().writeValueAsString(getMerchantFormat(merchant));
            }
        }

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    //todo:  modify password function
    @RequestMapping(method = RequestMethod.POST, value = "/merchant/update")
    public ResponseEntity<String> updateMerchant(@RequestBody String request) {

        LOGGER.info(SystemInstance.REQUEST_STRING + "api/crm/merchant/update. " + request);
        String response;

        JsonObject jsonObject = new Gson().fromJson(request, JsonObject.class);
        String accountId = jsonObject.get("accountId") == null ? "0":jsonObject.get("accountId").getAsString();
        Merchant merchant = merchantService.getOne(accountId);

        try{
            if(jsonObject.get("accountId") == null) {
                response = "'accountId' is null";
            }else {
                if(merchant == null) {
                    response = "No Such merchant-account :" + accountId;
                }else {

                    merchant.setName(jsonObject.get("name").getAsString());
                    merchant.setEmail(jsonObject.get("email").getAsString());
                    merchant.setComId(jsonObject.get("comId").getAsString());
                    merchant.setPhone(jsonObject.get("phone").getAsString());
                    merchant.setPaycodeToken(jsonObject.get("paycodeToken") == null? "XXXXXX" : jsonObject.get("paycodeToken").getAsString());
                    merchant.setRefundPin(jsonObject.get("refundPin") == null? "0000" : jsonObject.get("refundPin").getAsString());

                    Merchant merchant1 = merchantService.save(merchant);
                    response = merchant1 == null? "Merchant updates failed.":"Merchant updates successfully.";
                }
            }

            return new ResponseEntity<String>(response, HttpStatus.OK);
        }catch(NullPointerException e){
            LOGGER.error("[EXCEPTION] " + e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<String>("Null Data is detected.", HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/merchant/list")
    public ResponseEntity<String> listMerchant(@RequestBody String request) throws JsonProcessingException {
        LOGGER.info(SystemInstance.REQUEST_STRING + "api/crm/merchant/list. " + request);
        String response;

        List<Merchant> list;
        List<CrmMerchantData> list2 = new ArrayList<CrmMerchantData>();
        list = merchantService.listAll();

        for (Merchant merchant : list) {
            list2.add(getMerchantFormat(merchant));
        }
        response = new ObjectMapper().writeValueAsString(list2);
        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/role/setrole")
    public ResponseEntity<String> setRole(@RequestBody String request) {
        String log = " api/crm/role/setrole : ";
        LOGGER.info(SystemInstance.REQUEST_STRING + log + request);
        String response = "";

        JsonObject jsonObject = new Gson().fromJson(request, JsonObject.class);
        JsonElement jsonElementId = jsonObject.get("id");
        JsonElement jsonElementRoles = jsonObject.get("roles");
        Sale sale;

        try{
            if(jsonElementId == null || jsonElementRoles == null) {
                response = "'id' or 'roles' is null.";
            }else {
                sale = saleService.getOneById(jsonElementId.getAsLong());
                if(sale == null) {
                    response = "No Such sale-Id :" + jsonElementId.getAsString();
                }else {
                    List<Role> checkList = sale.getRoles();
                    for (JsonElement jsonElement: jsonElementRoles.getAsJsonArray()) {
                        Role targetRole = roleService.getOneById(jsonElement.getAsJsonObject().get("id").getAsLong());
                        if(targetRole != null) {
                            String name = targetRole.getName();
                            boolean isRoleExist = false;
                            for (Role element : checkList) {
                                if(name.equals(element.getName())) {
                                    isRoleExist = true;
                                    break;
                                }
                            }
                            if(!isRoleExist) {
                                sale.getRoles().add(targetRole);
                            }
                        }
                    }
                    Sale sale1 = saleService.save(sale);
                    response = sale1 == null? "Role-setting failed.":"Role-setting success.";
                }
            }

            return new ResponseEntity<String>(response, HttpStatus.OK);
        } catch(NullPointerException e) {
            LOGGER.error("[EXCEPTION]" + e.toString());
            e.printStackTrace();
            return new ResponseEntity<String>("Null data is detected. Check the Request.", HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/role/create")
    public ResponseEntity<String> createRole(@RequestBody String request) throws JsonProcessingException {
        LOGGER.info(SystemInstance.REQUEST_STRING + "api/crm/role/create. " + request);

        JsonObject jsonObject = new Gson().fromJson(request, JsonObject.class);
        Role role = new Role();
        role.setName(jsonObject.get("name").getAsString());
        role.setIsEnable(jsonObject.get("isEnable").getAsInt());
        role.setSales(new ArrayList<Sale>());
        return new ResponseEntity<String>(new Gson().toJson(role), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/role/list")
    public ResponseEntity<String> listRoles(@RequestBody String request) throws JsonProcessingException {
        LOGGER.info(SystemInstance.REQUEST_STRING + "api/crm/role/list. " + request);
        String response;

        JsonObject jsonObject = new Gson().fromJson(request, JsonObject.class);
        List<Role> list;
        if(jsonObject.get("isPersonal") == null) {
            list = roleService.getListAll();
        }else {
            list = roleService.getListByIsPersonal(jsonObject.get("isPersonal").getAsInt());
        }

        List<CrmRoleData> roleList = new ArrayList<CrmRoleData>();
        List<Permission> permissions;
        for (Role role: list) {
            permissions = permissionService.getListByRoleId(role.getId());
            roleList.add(getRoleFormat(role, permissions));
        }

        response = new ObjectMapper().writeValueAsString(roleList);
        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/sales/list")
    public ResponseEntity<String> listSales(@RequestBody Map<String, String> request) throws JsonProcessingException {
        String response = "No data.";
        LOGGER.info(SystemInstance.REQUEST_STRING + "api/crm/sale/list. " + request);

        // todo: determine the count of  1 page
        String type = request.get("type") == null ? "":request.get("type");
        String roleId = request.get("roleId") == null ? "":request.get("roleId");

        List<Sale> list = new ArrayList<Sale>();
        Role role;

        if(!"".equals(type) && !"0".equals(type)) {
            list = saleService.getListByType(Integer.parseInt(type));
            if(list.size() == 0) {
                response = "No Such type: " + type;
            }
        }else if(!"".equals(roleId) && !"0".equals(roleId)) {
            role = roleService.getOneById(Long.valueOf(request.get("roleId")));
            if(role == null) {
                response = "No Such roleId :" + request.get("roleId");
            }else {
                list = role.getSales();
            }
        }else {
            list = saleService.getListAll();
        }

        if(list.size() > 0) {
            List<CrmSaleData> list1 = new ArrayList<CrmSaleData>();

            for (Sale sale:list) {
                list1.add(getSaleFormat(sale));
            }
            response = new ObjectMapper().writeValueAsString(list1);
        }

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/sales/query")
    public ResponseEntity<String> querySales(@RequestBody String request) {
        String response;
        LOGGER.info(SystemInstance.REQUEST_STRING + "api/crm/sale/query. " + request);

        JsonObject jsonObject = new Gson().fromJson(request, JsonObject.class);
        String accountId = jsonObject.get("accountId") == null ? "":jsonObject.get("accountId").getAsString();
        String name = jsonObject.get("name") == null ? "":jsonObject.get("name").getAsString();

        List<Sale> list = saleService.queryWithLike(accountId, name);

        List<CrmSaleData> list1 = new ArrayList<CrmSaleData>();

        for (Sale sale:list) {
            list1.add(getSaleFormat(sale));
        }

        try {
            response = new ObjectMapper().writeValueAsString(list1);
        } catch (JsonProcessingException e) {
            LOGGER.error("[EXCEPTION] querySales() " + e.toString());
            e.printStackTrace();
            return new ResponseEntity<String>("JsonProcessingException", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/sales/enable")
    public ResponseEntity<String> setIsEnable(@RequestBody String request) throws JsonProcessingException {
        String response;
        LOGGER.info(SystemInstance.REQUEST_STRING + "api/crm/sale/enable. " + request);

        JsonObject jsonObject = new Gson().fromJson(request, JsonObject.class);
        String id = jsonObject.get("id") == null ? "0":jsonObject.get("id").getAsString();
        int isEnable = jsonObject.get("isEnable") == null ? 99:jsonObject.get("isEnable").getAsInt();
        Sale sale;

        if(jsonObject.get("isEnable") == null) {
            response = "'isEnable' is null";
        }else {
            sale = saleService.getOneById(Long.parseLong(id));
            if(sale == null) {
                response = "No Such sale-id :" + id;
            }else {
                sale.setIsEnable(isEnable);
                saleService.save(sale);
                response = "sale-id " + id + " 'isEnable' has been set to " + sale.getIsEnable();
            }
        }

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/sales/create")
    public ResponseEntity<String> createSales(@RequestBody String request) {
        LOGGER.info(SystemInstance.REQUEST_STRING + "api/crm/sale/create. " + request);
        String response;
        int isPersonal = 1;

        try{
            JsonObject jsonObject = new Gson().fromJson(request, JsonObject.class);
            Sale sale = new Sale();
            Role role = new Role();

            if (roleService.getOneByName(jsonObject.get("accountId").getAsString()) != null) {
                response = "accountId already exists.";
            }else {
                role.setName(jsonObject.get("accountId").getAsString());
                role.setIsEnable(1);
                role.setIsPersonal(isPersonal);
                Role role2 = roleService.save(role);
                Sale manager = saleService.getOneById(jsonObject.get("manager").getAsLong());
                if(role2 == null) {
                    response = "Role-Creating failed.";
                }else if(manager == null){
                    response = "Manager is Empty.";
                }else {
                    sale.getRoles().add(role2);
                    sale.setAccountId(jsonObject.get("accountId").getAsString());
                    sale.setPin(jsonObject.get("pin").getAsString());
                    sale.setIsEnable(1);
                    sale.setType(jsonObject.get("type").getAsInt());
                    sale.setName(jsonObject.get("name").getAsString());
                    sale.setMobile(jsonObject.get("mobile").getAsString());
                    sale.setEmail(jsonObject.get("email").getAsString());
                    sale.setManager(manager);
                    sale.setCreateTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
                    sale.setModifiedTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

                    Sale sale1 = saleService.save(sale);

                    response = sale1 == null? "Item create failed.":"Item created successfully.";
                }
            }

            return new ResponseEntity<String>(response, HttpStatus.OK);
        } catch(NullPointerException e){
            LOGGER.error("[EXCEPTION]" + e.toString());
            e.printStackTrace();
            return new ResponseEntity<String>("Null Data is detected.", HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/sales/update")
    public ResponseEntity<String> updateSales(@RequestBody String request) throws JsonProcessingException {
        String response;
        LOGGER.info(SystemInstance.REQUEST_STRING + "api/crm/sale/update. " + request);

        JsonObject jsonObject = new Gson().fromJson(request, JsonObject.class);
        String id = jsonObject.get("id") == null ? "0":jsonObject.get("id").getAsString();
        Sale sale = saleService.getOneById(Long.parseLong(id));
        Sale sale1;

        try {
            if(jsonObject.get("id") == null) {
                response = "'id' is null";
            }else {
                if(sale == null) {
                    response = "No Such sale-id :" + id;
                }else {

                    sale.setIsEnable(jsonObject.get("isEnable").getAsInt());
                    sale.setType(jsonObject.get("type").getAsInt());
                    sale.setName(jsonObject.get("name").getAsString());
                    sale.setMobile(jsonObject.get("mobile").getAsString());
                    sale.setEmail(jsonObject.get("email").getAsString());
                    sale.setModifiedTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
                    sale1 = saleService.save(sale);
                    response = sale1 == null ? " update failed.": "sale-id " + id + " is updated." + new ObjectMapper().writeValueAsString(getSaleFormat(sale1));
                }
            }

            return new ResponseEntity<String>(response, HttpStatus.OK);
        } catch (NullPointerException e) {
            LOGGER.error("[EXCEPTION]" + e.toString());
            e.printStackTrace();
            return new ResponseEntity<String>("Null Data is detected.", HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/sales/edit")
    public ResponseEntity<String> editSalesPage(@RequestBody String request) throws JsonProcessingException {

        String response;
        LOGGER.info( SystemInstance.REQUEST_STRING + "api/crm/sale/edit. " + request);

        JsonObject jsonObject = new Gson().fromJson(request, JsonObject.class);
        String id = jsonObject.get("id") == null ? "0":jsonObject.get("id").getAsString();
        Sale sale;

        if(jsonObject.get("id") == null) {
            response = "'id' is null";
        }else {
            sale = saleService.getOneById(Long.parseLong(id));
            if(sale == null) {
                response = "No Such sale-id :" + id;
            }else {
                response = new ObjectMapper().writeValueAsString(getSaleFormat(sale));
            }
        }

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    private CrmMerchantData getMerchantFormat(Merchant merchant) {

        Sale sale = merchant.getSaleId();

        CrmMerchantData crmMerchantData = new CrmMerchantData();
        crmMerchantData.setName(merchant.getName());
        crmMerchantData.setEmail(merchant.getEmail());
        crmMerchantData.setCreator(merchant.getCreator());
        crmMerchantData.setAccountId(merchant.getAccountId());
        crmMerchantData.setComId(merchant.getComId());
        crmMerchantData.setPhone(merchant.getPhone());
        crmMerchantData.setSignDate(merchant.getSignDate());
        crmMerchantData.setStatus(merchant.getStatus());
        crmMerchantData.setPaycodeToken(merchant.getPaycodeToken());

        if(sale == null) {
            crmMerchantData.setSaleId(0);
            crmMerchantData.setSaleAccountId("pastMerchant");
        }else {
            crmMerchantData.setSaleId(sale.getId());
            crmMerchantData.setSaleAccountId(sale.getAccountId());
        }

        crmMerchantData.setActivateToken(merchant.getActivateToken());
        crmMerchantData.setRefundPin(merchant.getRefundPin());

        return crmMerchantData;
    }

    private CrmRoleData getRoleFormat(Role role, List<Permission> permissions) {

        CrmRoleData crmRoleData = new CrmRoleData();
        crmRoleData.setId(role.getId());
        crmRoleData.setName(role.getName());
        crmRoleData.setIsPersonal(role.getIsPersonal());
        crmRoleData.setIsEnable(role.getIsEnable());
        crmRoleData.setPermissions(permissions);

        return crmRoleData;
    }

    private CrmSaleData getSaleFormat(Sale sale) {

        Sale saleManager = sale.getManager();
        String roles = "";
        CrmSaleData crmSaleData = new CrmSaleData();
        crmSaleData.setId(sale.getId());
        crmSaleData.setAccountId(sale.getAccountId());
        crmSaleData.setName(sale.getName());
        crmSaleData.setIsEnable(sale.getIsEnable());
        crmSaleData.setType(sale.getType());
        crmSaleData.setEmail(sale.getEmail());
        crmSaleData.setMobile(sale.getMobile());

        for (Role role:sale.getRoles()) {
            if( role.getIsPersonal() == 0) {
                roles += role.getName() + ",";
            }
        }

        if(roles.length() > 0) {
            roles = roles.substring(0,roles.length() -1);
        }
        crmSaleData.setRoles(roles);

        if(saleManager != null) {
            crmSaleData.setManagerId(saleManager.getId());
            crmSaleData.setManagerName(saleManager.getName());
            crmSaleData.setManagerAccountId(saleManager.getAccountId());
        }

        return crmSaleData;
    }

    private String getHash(String pin){
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return byte2Hex(digest.digest(pin.getBytes()));

    }

    private String byte2Hex(byte[] inputBytes) {
        String result = "";
        for (byte currentByte : inputBytes) result += Integer.toString((currentByte & 0xff) + 0x100, 16).substring(1);
        return result;
    }

}
