package co.intella.controller.login;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.context.Context;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import co.intella.domain.contratStore.DeviceVo;
import co.intella.domain.contratStore.MerchantAppLogin;
import co.intella.domain.contratStore.MerchantIncome;
import co.intella.domain.contratStore.ResetPasswordMail;
import co.intella.model.IntTbLookupCode;
import co.intella.model.MailInformation;
import co.intella.model.Merchant;
import co.intella.model.MerchantResetToken;
import co.intella.model.Permission;
import co.intella.model.Recipient;
import co.intella.model.Role;
import co.intella.model.Sale;
import co.intella.service.DeviceService;
import co.intella.service.LookupCodeService;
import co.intella.service.MailService;
import co.intella.service.MerchantResetTokenService;
import co.intella.service.MerchantService;
import co.intella.service.PaymentAccountService;
import co.intella.service.PermissionService;
import co.intella.service.RecipientService;
import co.intella.service.SaleService;
import co.intella.service.TicketLogicService;
import co.intella.service.TokenService;
import co.intella.utility.GeneralResponseMessageUtil;
import co.intella.utility.SystemInstance;

/**
 * @author Miles
 */
@CrossOrigin
@Controller
@RequestMapping("/api/system")
public class SystemLoginController {

    private final Logger LOGGER = LoggerFactory.getLogger(SystemLoginController.class);

    @Resource
    @Qualifier("authenticationManager")
    private AuthenticationManager authenticationManager;

    @Resource
    private DeviceService deviceService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private PermissionService permissionService;

    @Resource
    private SaleService saleService;

    @Resource
    private RecipientService recipientService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private MerchantResetTokenService merchantResetTokenService;

    @Resource
    private LookupCodeService lookupCodeService;

    @Value("${api.url}")
    private String API_URL;

    @Resource
    private MailService mailService;

    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    private TokenService tokenService;

    private Locale locale = new Locale("zh_TW");

    @RequestMapping(method = RequestMethod.POST, value = "/app/logout")
    public ResponseEntity<String> logout(@RequestBody MerchantIncome input) {

        Merchant merchant = merchantService.getOne(input.getAccountId());

        if (merchant == null) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage(messageSource.getMessage("response.0001", null, locale)), HttpStatus.OK);
        }

        if (merchant.getStatus().equals("active")) {
            if (input.getSerialId() != null) {
                recipientService.delete(input.getSerialId(), input.getAppId());
            }

            return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(), HttpStatus.OK);
        } else if (merchant.getStatus().equals("banned")) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage(messageSource.getMessage("response.0002", null, locale)), HttpStatus.OK);

        } else if (merchant.getStatus().equals("unverified")) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage(messageSource.getMessage("response.0003", null, locale)), HttpStatus.OK);
        } else {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage(messageSource.getMessage("response.0004", null, locale)), HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{systemName}/login")
    public ResponseEntity<String> crmLogin(@RequestBody Map<String, String> request, @PathVariable String systemName, HttpServletRequest httpServletRequest, HttpServletResponse response) throws JsonProcessingException {

        if ("crm".equals(systemName)) {


            String username = request.get("username");
            String password = request.get("password");

            LOGGER.info("[AUTH][CRM] starting..." + username);

            Sale sale = saleService.login(username, password);

            String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();

            if (sale == null) {
                LOGGER.info("[AUTH][FAILED] " + username);
                CrmLoginStatus loginStatus = new CrmLoginStatus(false, username, -1, null, null);
                return new ResponseEntity<String>(new Gson().toJson(loginStatus), HttpStatus.OK);
            }

            Cookie cookie = new Cookie("SESSION", sessionId);
            cookie.setHttpOnly(true);
            cookie.setMaxAge(3600);
            cookie.setPath("/allpaypass/");
            response.addCookie(cookie);

            List<Role> roles = new ArrayList<Role>();
            roles.add(new Role(SystemInstance.ROLE_CRM_USER));
            roles.add(new Role(SystemInstance.ROLE_ANONYMOUS));
            roles.add(new Role(SystemInstance.ROLE_USER));

            User details = new User(sale.getAccountId(), sale.getPin(), roles);

            Collection<? extends GrantedAuthority> authorities = details.getAuthorities();
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password, authorities);
            token.setDetails(details);

            LOGGER.info("[AUTH][SUCCESS] " + username);
            try {
                Authentication auth = authenticationManager.authenticate(token);
                SecurityContextHolder.getContext().setAuthentication(auth);

                Set<String> permissionSet = collectPermissions(sale.getRoles());
                CrmLoginStatus loginStatus = new CrmLoginStatus(auth.isAuthenticated(), auth.getName(), sale.getId(), sessionId, permissionSet);

                return new ResponseEntity<String>(new Gson().toJson(loginStatus), HttpStatus.OK);
            } catch (BadCredentialsException e) {
                LOGGER.info("[AUTH][FAILED] " + username + ", " + e.toString());
                CrmLoginStatus loginStatus = new CrmLoginStatus(false, null, -1, null, null);
                return new ResponseEntity<String>(new Gson().toJson(loginStatus), HttpStatus.OK);
            }
        } else if ("app".equals(systemName)) {
            try {
            	// 帳號密碼登入時帶入
                String accountId = request.get("accountId");
                String pin = request.get("pin");
                
                // sso登入時帶入
                String tokenId = request.get("tokenId");
                String userId = request.get("userId");

                // ios app才有
                String serialId = request.get("serialId");
                
                // 區別app
                String appId = request.get("appId");
                
                // 區別ios android
                String platform = request.get("platform");
                
                String channel;
                boolean isEzc = false;

                LOGGER.info("[AUTH][APP] starting..." + accountId);
                LOGGER.info("[AUTH][serialId] ..." + serialId);
                LOGGER.info("[AUTH][tokenId] ..." + tokenId);
                LOGGER.info("[AUTH][appId] ..." + appId);
                LOGGER.info("[AUTH][userId] ..." + userId);
                LOGGER.info("[AUTH][platform] ..." + platform);


                MerchantAppLogin merchant = merchantService.getLoginMerchantByLoginId(accountId);

                JSONObject json = merchantService.getAppImg(accountId);
                String appImgString = null;
                if ("null" != json.getString("appImg")) {
                	appImgString = new String(new Base64().decode(json.getString("appImg")));
                }
                if (merchant == null) {
                    LOGGER.error("[AUTH][APP] merchant not found." + accountId);

                    AppLoginStatus loginStatus = new AppLoginStatus(false, accountId, "Account or password is incorrect.", null, null, null, false, null, null, null);

                    return new ResponseEntity<String>(new Gson().toJson(loginStatus), HttpStatus.OK);
                }

                if (merchant.getStatus().equals("active")) {
                	
                	if (StringUtils.isNotEmpty(tokenId)) {
                		JSONArray jsonArray = tokenService.getMerchantByTokenIdAndAccountId(tokenId, accountId);
                		if (jsonArray.length()  == 0) {
                			AppLoginStatus loginStatus = new AppLoginStatus(false, null, messageSource.getMessage("co.intella.tokenId.error", null, locale), null, null, null, false, null, null, null);

                            return new ResponseEntity<String>(new Gson().toJson(loginStatus), HttpStatus.OK);
                		} else {
                			pin = merchant.getPin();
                		}
                	} else if (!merchant.getPin().equals(pin)) {

                        AppLoginStatus loginStatus = new AppLoginStatus(false, accountId, "Account or password is incorrect.", null, null, null, false, null, null, null);

                        return new ResponseEntity<String>(new Gson().toJson(loginStatus), HttpStatus.OK);
                    }

                } else if (merchant.getStatus().equals("banned")) {

                    AppLoginStatus loginStatus = new AppLoginStatus(false, accountId, "Account or password is incorrect.", null, null, null, false, null, null, null);
                    return new ResponseEntity<String>(new Gson().toJson(loginStatus), HttpStatus.OK);
                } else if (merchant.getStatus().equals("unverified")) {
                    AppLoginStatus loginStatus = new AppLoginStatus(false, accountId, "Account or password is incorrect.", null, null, null, false, null, null, null);

                    return new ResponseEntity<String>(new Gson().toJson(loginStatus), HttpStatus.OK);
                }

                MerchantAppLogin channelMerchant = null;
                if (merchant.getAccountType() != null && merchant.getAccountType() == 2) {
                    channel = merchant.getParentMerchant() != null ? merchant.getParentMerchant() : "null";
                    channelMerchant = merchantService.getLoginMerchantByLoginId(channel);
                } else {
                    channel = merchant.getAccountId();
                    channelMerchant = merchant;
                }
                
                if (StringUtils.isNotEmpty(platform) && platform.equals("IOS")) {
                	LOGGER.info("[AUTH][APP] start deal with serialId. new ");

                    recipientService.delete(serialId, appId);

                    Recipient recipient = new Recipient();
                    recipient.setSerialId(serialId);
                    recipient.setCrDate(new Date());
                    recipient.setDateStamp(new Date());
                    recipient.setMerchant(merchant);
                    recipient.setAppId(appId);
                    recipient.setUserId(userId);
                    recipientService.save(recipient);
                    LOGGER.info("[AUTH][APP] end deal with serialId. new ");
                } else if (serialId != null && !"ANDROID".equals(platform)) {
                	// 兼容舊app
                    LOGGER.info("[AUTH][APP] start deal with serialId. old");

                    recipientService.deleteNullAppId(serialId);

                    Recipient recipient = new Recipient();
                    recipient.setSerialId(serialId);
                    recipient.setCrDate(new Date());
                    recipient.setDateStamp(new Date());
                    recipient.setMerchant(merchant);
                    recipientService.save(recipient);
                    LOGGER.info("[AUTH][APP] end deal with serialId. old");
                }

                isEzc = checkTerminalEZC(merchant);

                LOGGER.info("[AUTH][APP] start deal with session and auth.");
                String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();

                Cookie cookie = new Cookie("SESSION", sessionId);
                cookie.setHttpOnly(true);
                cookie.setMaxAge(3600);
                cookie.setPath("/allpaypass/");
                response.addCookie(cookie);

                List<Role> roles = new ArrayList<Role>();
                roles.add(new Role(SystemInstance.ROLE_APP_USER));
                roles.add(new Role(SystemInstance.ROLE_ANONYMOUS));
                roles.add(new Role(SystemInstance.ROLE_USER));

                User details = new User(accountId, pin, roles);
                Collection<? extends GrantedAuthority> authorities = details.getAuthorities();
                UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(accountId, pin, authorities);
                token.setDetails(details);

                Authentication auth = authenticationManager.authenticate(token);
                SecurityContextHolder.getContext().setAuthentication(auth);
                LOGGER.info("[AUTH][APP] start deal with session and auth.");

                AppLoginStatus loginStatus = new AppLoginStatus(auth.isAuthenticated(), auth.getName(), "LOGIN_SUCCESS", merchant.getName(), channelMerchant.getTradePin(), channel, isEzc, merchant.getRandomId(), tokenId, appImgString);

                LOGGER.info("[AUTH][APP][PASS] granted authorities.");
                return new ResponseEntity<String>(new Gson().toJson(loginStatus), HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                LOGGER.error("[EXCEPTION][AUTH][APP][NOT PASS] " + e.toString());

                AppLoginStatus loginStatus = new AppLoginStatus(false, "", "BAD_AUTH", null, null, null, false, null, null, null);

                return new ResponseEntity<String>(new Gson().toJson(loginStatus), HttpStatus.UNAUTHORIZED);
            }
        } else {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("login failed."), HttpStatus.OK);
        }
    }

    private boolean checkTerminalEZC(Merchant merchant) {

        boolean checkout = true;
        List<DeviceVo> deviceVoList = deviceService.listAllByMerchantAndType(merchant.getAccountId(), "ezc");
        List<DeviceVo> taxiDeviceVoList = deviceService.listAllByMerchantAndType(merchant.getAccountId(), "taxi");

        if (CollectionUtils.isEmpty(deviceVoList) && CollectionUtils.isEmpty(taxiDeviceVoList)) {
            checkout = false;
            return checkout;
        }
        long posCount = paymentAccountService.listAllByAccount(merchant.getAccountId()).parallelStream()
                .filter(pa -> Boolean.valueOf(pa.getIsDefault()) && TicketLogicService.ticketBankList.contains(pa.getBank().getBankId())).count();
        if (posCount < 1) {
            checkout = false;
        }
        return checkout;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/forgetPassword")
    public ResponseEntity<String> forgetPassword(@RequestBody String body) throws Exception {

        JsonObject json = new Gson().fromJson(body, JsonObject.class);

        if (!json.has("merchant")) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("fail"), HttpStatus.OK);
        }
        String merchantId = json.get("merchant").getAsString();

        Merchant merchant = merchantService.getOne(merchantId);
        String resetUrl = null;

        if (merchant != null) {
            SecureRandom random = new SecureRandom();
            String token = new BigInteger(130, random).toString(32);

            MerchantResetToken merchantResetToken = new MerchantResetToken();
            merchantResetToken.setAccountId(merchantId);
            merchantResetToken.setResetTimeout(DateTime.now().plusMinutes(15).toString("yyyyMMddHHmmss"));
            merchantResetToken.setResetToken(token);
            merchantResetToken.setUsed(false);
            merchantResetTokenService.save(merchantResetToken);
            resetUrl = API_URL + "allpaypass/api/system/resetPasswordUrl/" + merchantId + "/" + token;
            ResetPasswordMail resetPasswordMail = new ResetPasswordMail();

            resetPasswordMail.setMessage(resetUrl);
            resetPasswordMail.setEmail(merchant.getEmail());
            resetPasswordMail.setSubject("Reset PASSWORD");
            resetPasswordMail.setUserName(merchant.getName());
            resetPasswordMail.setMerchant(merchantId);

            try {
                sendResetMail(resetPasswordMail);
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
        return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(resetUrl), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/resetPasswordUrl/{accountId}/{token}")
    public ModelAndView resetPasswordUrl(@PathVariable String accountId, @PathVariable String token) {

        LOGGER.info("[resetPasswordUrl]");
        MerchantResetToken merchantResetToken = merchantResetTokenService.getOne(accountId, token);
        ModelAndView model = new ModelAndView();

        if (merchantResetToken != null) {
            //merchantResetTokenService.save(merchantResetToken);

            model.setViewName("resetPasswordPage");
            model.addObject("account", accountId);
            model.addObject("token", token);
            model.addObject("domain", API_URL);
            return model;
        }
        //todo link fail
        else {
            model.setViewName("resetPasswordFail");
            return model;
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/resetPasswordUrl/{account}/resetPassword")
    public ResponseEntity<String> resetPassword(@RequestBody String body) {

        LOGGER.info("resetpw " + body);

        JsonObject json = new Gson().fromJson(body, JsonObject.class);

        String accountId = json.get("merchant").getAsString();
        String token = json.get("token").getAsString();
        String pin = json.get("pw").getAsString();

        MerchantResetToken merchantResetToken = merchantResetTokenService.getOne(accountId, token);

        if (merchantResetToken != null) {
            LOGGER.info("[start][resetpw] " + body);

            merchantResetToken.setUsed(true);
            merchantResetTokenService.save(merchantResetToken);

            Merchant merchant = merchantService.getOne(accountId);
            merchant.setPin(getHash(pin));
            merchantService.editPassword(merchant);
            LOGGER.info("[end][resetpw] " + body);


            return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage("success"), HttpStatus.OK);
        } else {
            LOGGER.info("[fail][resetpw] " + body);
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("fail"), HttpStatus.OK);
        }


    }

    @RequestMapping(method = RequestMethod.GET, value = "/resetPasswordSuccess")
    public ModelAndView resetPasswordSuccess() {
        ModelAndView model = new ModelAndView();
        model.setViewName("resetPasswordSuccess");
        return model;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/app/version")
    public ResponseEntity<String> getVersion(@RequestBody Map<String, String> request) {
        try {
        	final String TYPE = request.get("type");
        	final String APP_VERSION = request.get("appversion");
        	String APP_ID = request.get("appId");
        	LOGGER.info("[/app/version][getVersion] TYPE = " + TYPE + " APP_VERSION = " + APP_VERSION + " APP_ID = " + APP_ID);
        	if (null == APP_ID) {
        		APP_ID = "taxi";
        	}
        	IntTbLookupCode lookupCode = lookupCodeService.findOne("appversion", APP_ID);
        	final String MIN_VERSION;
        	if (TYPE.equals("ANDROID")) {
        		MIN_VERSION = lookupCode.getType1();
        	} else {
        		MIN_VERSION = lookupCode.getType2();
        	}
        	
        	String msgCode = "";
        	String msg = "";
            String updateversion = "";
            String minVersion = "";

            if (Double.parseDouble(MIN_VERSION) > Double.parseDouble(APP_VERSION)) {
                msgCode = "0001";
                msg = "須更新版本";
                updateversion = "true";
                minVersion = MIN_VERSION;
            } else {
                msgCode = "0000";
                updateversion = "false";
                minVersion = MIN_VERSION;
            }
            Map<String, String> response = new HashMap<String, String>();
            response.put("msgCode", msgCode);
            response.put("msg", msg);
            response.put("updateversion", updateversion);
            response.put("minVersion", minVersion);
            LOGGER.info("[REQUEST] api/android/version");
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(new Gson().toJson(response)), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("https://intella.co", HttpStatus.OK);
        }
    }

    private Set<String> collectPermissions(List<Role> roles) {
        Set<String> permissionSet = new HashSet<String>();
        List<Permission> list;

        for (Role role : roles) {
            list = permissionService.getListByRoleId(role.getId());
            for (Permission permission : list) {
                permissionSet.add(permission.getAction());
            }
        }
        return permissionSet;
    }

    private void sendResetMail(ResetPasswordMail input) throws MessagingException {
        MailInformation mailInformation = new MailInformation();
        mailInformation.setMailTo(new String[]{input.getEmail()});
        mailInformation.setSubject(input.getSubject());

        Context ctx = new Context();
        ctx.setVariable("username", input.getUserName());
        ctx.setVariable("email", input.getEmail());
        ctx.setVariable("message", input.getMessage());
        ctx.setVariable("merchant", input.getMerchant());

        mailService.send(mailInformation, ctx, "resetPasswordMail");
    }


    private String getHash(String pin) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return byte2Hex(digest.digest(pin.getBytes()));
    }

    private String byte2Hex(byte[] inputBytes) {
        String result = "";
        for (byte currentByte : inputBytes) result += Integer.toString((currentByte & 0xff) + 0x100, 16).substring(1);
        return result;
    }

    public class CrmLoginStatus {

        private final boolean loggedIn;

        private final String username;

        private final long id;

        private final String session;

        private Set<String> permissionSet = new HashSet<String>();

        CrmLoginStatus(boolean loggedIn, String username, long id, String session, Set<String> permissionSet) {
            this.loggedIn = loggedIn;
            this.username = username;
            this.id = id;
            this.session = session;
            this.permissionSet = permissionSet;
        }

        public boolean isLoggedIn() {
            return loggedIn;
        }

        public String getUsername() {
            return username;
        }

        public long getId() {
            return id;
        }

        public String getSession() {
            return session;
        }

        public Set<String> getPermissionSet() {
            return permissionSet;
        }
    }

    private class AppLoginStatus {

        private final boolean loggedIn;

        private final String username;

        private String message;

        private String storeName;

        private String tradeKey;

        private String channel;

        private String randomid;

        private boolean ezc;
        
        private String appImg;

        private String tokenId;
        
        AppLoginStatus(boolean loggedIn, String username, String message, String storeName, String tradeKey, String channel, boolean ezc, String randomid, String tokenId, String appImg) {
            this.loggedIn = loggedIn;
            this.username = username;
            this.message = message;
            this.storeName = storeName;
            this.tradeKey = tradeKey;
            this.channel = channel;
            this.ezc = ezc;
            this.randomid = randomid;
            this.tokenId = tokenId;
            this.appImg = appImg;
        }

        public boolean isLoggedIn() {
            return loggedIn;
        }

        public String getUsername() {
            return username;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getTradeKey() {
            return tradeKey;
        }

        public void setTradeKey(String tradeKey) {
            this.tradeKey = tradeKey;
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
        }

        public boolean isEzc() {
            return ezc;
        }

        public String getRandomid() {
            return randomid;
        }

        public void setRandomid(String randomid) {
            this.randomid = randomid;
        }

        public void setEzc(boolean ezc) {
            this.ezc = ezc;
        }

		public String getAppImg() {
			return appImg;
		}
		public String getTokenId() {
			return tokenId;
		}

		public void setTokenId(String tokenId) {
			this.tokenId = tokenId;
		}   
        
		public void setAppImg(String appImg) {
			this.appImg = appImg;
		}
    }
}
