package co.intella.controller;

import co.intella.domain.android.AndroidPayRequest;
import co.intella.utility.GeneralResponseMessageUtil;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * This api used by apple pay transaction
 * @author Miles
 */
@Controller
@RequestMapping("/api/android")
public class AndroidPayController {

    private final Logger LOGGER = LoggerFactory.getLogger(AndroidPayController.class);

    @RequestMapping("/page")
    public ModelAndView page()  {
        LOGGER.info("[REQUEST] api/android/page");
        ModelAndView model = new ModelAndView();
        model.setViewName("android-pay");
        return model;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/pay")
    public ResponseEntity<String> pay(@RequestBody AndroidPayRequest input) {
        try {
            LOGGER.info("[REQUEST] api/android/pay" + new Gson().toJson(input));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage("ok"), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("https://intella.co", HttpStatus.OK);
        }
    }

}
