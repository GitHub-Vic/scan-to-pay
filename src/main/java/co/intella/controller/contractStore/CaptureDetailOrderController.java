package co.intella.controller.contractStore;

import co.intella.domain.contratStore.CustomizePageRequest;
import co.intella.model.CaptureOrderDetail;
import co.intella.service.CaptureOrderDetailService;
import co.intella.utility.GeneralResponseMessageUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * @author Miles Wu
 */
@Controller
@RequestMapping("/api/captureOrder")
public class CaptureDetailOrderController {

    private final Logger LOGGER = LoggerFactory.getLogger(CaptureDetailOrderController.class);

    @Resource
    private CaptureOrderDetailService captureOrderDetailService;

    @RequestMapping( method= RequestMethod.POST, value="/pageCount" )
    public ResponseEntity<String> pageableCount(@RequestBody CustomizePageRequest pageRequest) {

        if(Strings.isNullOrEmpty(pageRequest.getAccountId())) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("AccountId should not be null."), HttpStatus.BAD_REQUEST);
        }

        long size = captureOrderDetailService.pageCount(pageRequest);

        return new ResponseEntity<String>(String.valueOf(size), HttpStatus.OK);
    }

    @RequestMapping( method= RequestMethod.POST, value="/pageList" )
    public ResponseEntity<String> pageable(@RequestBody CustomizePageRequest pageRequest) {

        if(Strings.isNullOrEmpty(pageRequest.getAccountId())) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("AccountId should not be null."), HttpStatus.BAD_REQUEST);
        }

        List<CaptureOrderDetail> list = captureOrderDetailService.pageList(pageRequest);

        try {
            return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(list), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            LOGGER.error("[EXCEPTION] json processing error. " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("JsonProcessingException"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping( method= RequestMethod.POST, value="/list" )
    public ResponseEntity<String> list(@RequestBody CustomizePageRequest pageRequest) {

        if(Strings.isNullOrEmpty(pageRequest.getAccountId())) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("AccountId should not be null."), HttpStatus.BAD_REQUEST);
        }

        List<CaptureOrderDetail> list = captureOrderDetailService.list(pageRequest.getAccountId(), pageRequest.getStartDate(), pageRequest.getEndDate(), pageRequest.getStatus());

        try {
            return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(list), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            LOGGER.error("[EXCEPTION] json processing error. " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("JsonProcessingException"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
