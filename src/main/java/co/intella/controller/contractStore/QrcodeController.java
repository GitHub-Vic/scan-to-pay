package co.intella.controller.contractStore;

import co.intella.domain.contratStore.CustomizePageRequest;
import co.intella.domain.contratStore.QrcodeAlias;
import co.intella.model.QrcodeParameter;
import co.intella.service.QrcodeService;
import co.intella.utility.GeneralResponseMessageUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;


/**
 * @author Alex
 */
@Controller
@RequestMapping("/api/qrcode")
public class QrcodeController {

    private final Logger LOGGER = LoggerFactory.getLogger(QrcodeController.class);

    @Resource
    private QrcodeService qrcodeService;

    @RequestMapping(method = RequestMethod.GET, value = "/exists/{id}")
    public ResponseEntity<String> isExists(@PathVariable(value = "id") String id) {
        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortId(id);
        if (qrcodeParameter != null) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage("exits"), HttpStatus.OK);
        } else {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage("not found"), HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/alias")
    public ResponseEntity<String> modifyShortly(@RequestBody QrcodeAlias input) {

        if (Strings.isNullOrEmpty(input.getAliasCurrent()) || Strings.isNullOrEmpty(input.getMerchant()) || Strings.isNullOrEmpty(input.getAliasNew())) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Argument wrong. MerchantId, alias name and new name should not be null."), HttpStatus.OK);
        }

        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortIdAndMchId(input.getMerchant(), input.getAliasCurrent());

        if (qrcodeParameter == null) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("QRCode not found."), HttpStatus.OK);
        } else {

            QrcodeParameter modified = qrcodeService.modifyShortId(qrcodeParameter, input.getAliasNew());
            if (modified == null) {
                LOGGER.warn("[WARNING] Modify qrcode alias failed. " + new Gson().toJson(input));
                return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Modify alias failed."), HttpStatus.OK);
            }
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(new Gson().toJson(modified)), HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/modify")
    public ResponseEntity<String> modify(@RequestBody QrcodeAlias input) {

        if (Strings.isNullOrEmpty(input.getAliasCurrent()) || Strings.isNullOrEmpty(input.getMerchant())) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Argument wrong."), HttpStatus.OK);
        }

        QrcodeParameter qrcodeParameter = qrcodeService.getOneByShortIdAndMchId(input.getMerchant(), input.getAliasCurrent());

        if (qrcodeParameter == null) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("QRCode not found"), HttpStatus.OK);
        } else {

            // todo modify more field.

            qrcodeParameter.setBody(input.getName());
            qrcodeParameter.setStoreOrderNo(input.getStoreOrderNo());
            QrcodeParameter modifiedEntity = qrcodeService.save(qrcodeParameter);
            if (modifiedEntity != null) {
                return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(), HttpStatus.OK);
            } else {
                LOGGER.warn("[WARNING] Modify qrcode failed. " + new Gson().toJson(input));
                return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Modify failed."), HttpStatus.OK);
            }
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/save")
    public ResponseEntity<String> save(@RequestBody QrcodeParameter input) {

        LOGGER.info("[Qrcode][save][input]: " + new Gson().toJson(input));

        if (Strings.isNullOrEmpty(input.getMchId()) || Strings.isNullOrEmpty(input.getShortId())) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Arguments error."), HttpStatus.OK);
        }

        QrcodeParameter qrcodeParameter = qrcodeService.save(input);

        if (qrcodeParameter != null) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(), HttpStatus.OK);
        } else {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("save failed."), HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/list")
    public ResponseEntity<String> list(@RequestBody QrcodeParameter input) {

        try {
            List<QrcodeParameter> list = qrcodeService.listAll(input.getMchId());

            if (list == null) {
                return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("no such code"), HttpStatus.OK);
            }

            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(list);
            return new ResponseEntity<String>(json, HttpStatus.OK);
        } catch (JsonProcessingException e) {
            LOGGER.error("[EXCEPTION] list : " + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Exception"), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] list : " + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Exception"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/pageCount")
    public ResponseEntity<String> pageableCount(@RequestBody CustomizePageRequest pageRequest) {

        if (Strings.isNullOrEmpty(pageRequest.getAccountId())) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("AccountId should not be null."), HttpStatus.BAD_REQUEST);
        }

        long size = qrcodeService.pageCount(pageRequest);

        return new ResponseEntity<String>(String.valueOf(size), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/pageList")
    public ResponseEntity<String> pageable(@RequestBody CustomizePageRequest pageRequest) {

        if (Strings.isNullOrEmpty(pageRequest.getAccountId())) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("AccountId should not be null."), HttpStatus.OK);
        }

        List<QrcodeParameter> list = qrcodeService.pageList(pageRequest);

        try {
            return new ResponseEntity<String>(new ObjectMapper().writeValueAsString(list), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            LOGGER.error("[EXCEPTION] pageable : " + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("JsonProcessingException"), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] pageable : " + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Exception"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
