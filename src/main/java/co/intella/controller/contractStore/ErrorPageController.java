package co.intella.controller.contractStore;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Vita
 */
@Controller
public class ErrorPageController implements ErrorController {

    private static final String ERROR_PATH = "/error";

    @RequestMapping(value = ERROR_PATH)
    public String handleError() {
        return "notFoundDefault";
    }

    public String getErrorPath() {
        // TODO Auto-generated method stub
        return ERROR_PATH;
    }
}

