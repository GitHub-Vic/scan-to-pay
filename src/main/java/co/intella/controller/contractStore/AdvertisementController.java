package co.intella.controller.contractStore;

import co.intella.controller.OnlineController;
import co.intella.model.PaymentAccount;
import co.intella.service.PaymentAccountService;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vita on 2017/11/20.
 */

@Controller
@RequestMapping("/api/advertise")
public class AdvertisementController {

    private final Logger LOGGER = LoggerFactory.getLogger(OnlineController.class);

    @Value("${api.url}")
    private String API_URL;

    @Resource
    private PaymentAccountService paymentAccountService;



    @RequestMapping("/airAds/{token}")
    public ModelAndView airAdsQrcode(@PathVariable(value = "token") String token) {
        ModelAndView model = new ModelAndView();
        List<PaymentAccount> list = paymentAccountService.listByQrcode(token);
        List<String> accountName = new ArrayList<String>();
        for (PaymentAccount aList : list) {
            accountName.add(aList.getBank().getName());
        }
        LOGGER.info("[payment] " + accountName);
        model.addObject("accountName", accountName);
        model.addObject("domain", API_URL);
        model.setViewName("airAdsQrcode");
        return model;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/qrcode/previewFineBack/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> viewQRCodeImageFineBack(@PathVariable("id") String id) {

        String myCodeText = API_URL + id;
        int size = 650;

        final HttpHeaders headers = new HttpHeaders();
        InputStream is;
        byte[] result = null;

        try {

            Map<EncodeHintType, Object> hintMap = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");

            hintMap.put(EncodeHintType.MARGIN, 1);
            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix byteMatrix = qrCodeWriter.encode(myCodeText, BarcodeFormat.QR_CODE, size, size, hintMap);
            int width = byteMatrix.getWidth();
            BufferedImage image = new BufferedImage(width, width, BufferedImage.TYPE_INT_RGB);
            image.createGraphics();

            Graphics2D graphics = (Graphics2D) image.getGraphics();
            graphics.setColor(Color.WHITE);
            graphics.fillRect(0, 0, width, width);
            graphics.setColor(Color.BLACK);

            for (int i = 0; i < width; i++) {
                for (int j = 0; j < width; j++) {
                    if (byteMatrix.get(i, j)) {
                        graphics.fillRect(i, j, 1, 1);
                    }
                }
            }
            //Add back ground/home/cs/back.jpg
            ClassLoader classLoader = getClass().getClassLoader();
            final BufferedImage backImage = ImageIO.read( new File(classLoader.getResource("static/images/airAdsQrcode/fine-back.png").getFile()));
            Graphics backGraphics=backImage.getGraphics();
            backGraphics.drawImage(image,backImage.getWidth()/2-image.getWidth()/2,backImage.getHeight()/2-image.getHeight()/2-65,null);
            //Add text
            backGraphics.setColor(Color.BLACK);
            backGraphics.setFont(new Font("Microsoft JhengHei",Font.PLAIN,136));
            backGraphics.drawString("intella",156,backImage.getHeight()-42);

            ClassLoader centerLogoLoader = getClass().getClassLoader();
            final BufferedImage centerLogo = ImageIO.read( new File(centerLogoLoader.getResource("static/images/airAdsQrcode/center-qrcode.png").getFile()));
            backGraphics.drawImage(centerLogo,backImage.getWidth()/2-centerLogo.getWidth()/2,backImage.getHeight()/2-centerLogo.getHeight()/2-65,null);

            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(backImage, "png", os);
            is = new ByteArrayInputStream(os.toByteArray());
            result = IOUtils.toByteArray(is);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (WriterException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<byte[]>(result, headers, HttpStatus.CREATED);
    }
}
