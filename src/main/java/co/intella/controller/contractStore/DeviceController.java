package co.intella.controller.contractStore;

import co.intella.domain.contratStore.DeviceIncome;
import co.intella.model.*;
import co.intella.service.*;
import co.intella.utility.GeneralResponseMessageUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * @author Alex
 */
@Controller
@RequestMapping("/api/device")
public class DeviceController {

    private final Logger LOGGER = LoggerFactory.getLogger(DeviceController.class);

    @Resource
    private MerchantService merchantService;

    @Resource
    private DeviceService deviceService;

    @RequestMapping( method= RequestMethod.POST, value="/create" )
    public ResponseEntity<String> create(@RequestBody DeviceIncome input) {
        //todo insert logic

        Device device = new Device();

        device.setName(input.getName());
        device.setDescription(input.getDescription());
        device.setOwner(merchantService.getOne(input.getOwner()));
        device.setOwnerDeviceId(input.getDeviceId());
        device.setType(input.getType());


        deviceService.save(device);

        return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(), HttpStatus.OK);
    }


//device get one
    @RequestMapping( method= RequestMethod.GET, value="/get" )
    public ResponseEntity<String> list(@RequestParam(value = "accountId") String id, @RequestParam(value = "deviceId") String deviceId) {

        Merchant merchant = merchantService.getOne(id);

        Device device = deviceService.getOne(merchant,deviceId);

        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(device);
        } catch (JsonProcessingException e) {
            LOGGER.error("[EXCEPTION] json processing error. " + e);
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("JsonProcessingException"), HttpStatus.INTERNAL_SERVER_ERROR);
        }


        return new ResponseEntity<String>(json, HttpStatus.OK);
    }

    @RequestMapping( method= RequestMethod.GET, value="/list/{accountId}" )
    public ResponseEntity<String> list(@PathVariable String accountId) {



        List<Device> list = deviceService.listAllByMerchant(merchantService.getOne(accountId));

        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(list);
        } catch (JsonProcessingException e) {
            LOGGER.error("[EXCEPTION] json processing error. " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("JsonProcessingException"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<String>(json, HttpStatus.OK);
    }

    //todo update
    //todo delete

}
