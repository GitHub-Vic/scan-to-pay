package co.intella.controller.contractStore;

import co.intella.domain.contratStore.CustomizePageRequest;
import co.intella.domain.contratStore.TradeDetailIncome;
import co.intella.model.*;
import co.intella.service.*;
import co.intella.utility.GeneralResponseMessageUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.assertj.core.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Alexchiu on 2017/3/20.
 */
@Controller
@RequestMapping("/api/trade")
public class TradeDetailController {

    private final Logger LOGGER = LoggerFactory.getLogger(TradeDetailController.class);

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private PaymentAccountService paymentAccountService;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private TradeDetailService tradeDetailService;

    @RequestMapping( method= RequestMethod.POST, value="/pageCount" )
    public ResponseEntity<String> pageableCount(@RequestBody CustomizePageRequest pageRequest) {

        LOGGER.info("[REQUEST] /api/trade/pageCount : " + new Gson().toJson(pageRequest));

        if(Strings.isNullOrEmpty(pageRequest.getAccountId())) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("AccountId should not be null."), HttpStatus.BAD_REQUEST);
        }

        long size = tradeDetailService.pageCount(pageRequest);

        LOGGER.info("[RESPONSE] /api/trade/pageableCount : " + size);

        return new ResponseEntity<String>(String.valueOf(size), HttpStatus.OK);
    }

    @RequestMapping( method= RequestMethod.POST, value="/pageList" )
    public ResponseEntity<String> pageable(@RequestBody CustomizePageRequest pageRequest) {

        LOGGER.info("[REQUEST] /api/trade/pageList : " + new Gson().toJson(pageRequest));

        if(Strings.isNullOrEmpty(pageRequest.getAccountId())) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("AccountId should not be null."), HttpStatus.BAD_REQUEST);
        }

        List<TradeDetail> list = tradeDetailService.pageList(pageRequest);

        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(list);
        } catch (JsonProcessingException e) {
            LOGGER.error("[EXCEPTION] json processing error. " + e);
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("JsonProcessingException"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        LOGGER.info("[RESPONSE] /api/trade/pageList : " + list.size());
        return new ResponseEntity<String>(json, HttpStatus.OK);
    }

    @RequestMapping( method= RequestMethod.POST, value="/create" )
    public ResponseEntity<String> create(@RequestBody TradeDetailIncome input) {

        LOGGER.info("[REQUEST] /api/trade/create : " + new Gson().toJson(input));
        //todo Lisa pass the bankId here? what are  method and type mean?
        long bankId = 0;
        if (input.getMethod().equals("10110"))
        {
            bankId = 1;
        }

        Merchant merchant = merchantService.getOne(input.getAccountId());

        if(merchant == null) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage("Merchant not found."), HttpStatus.OK);
        }

        TradeDetail trade = new TradeDetail();

        trade.setMethod(input.getMethod());
        trade.setAccountId(input.getAccountId());
        trade.setOrderId(input.getOrderId());
        trade.setPaymentAccount(paymentAccountService.getOne(input.getAccountId(), bankId));
        trade.setDeviceRandomId( deviceService.getOne(merchant, input.getDeviceId()));
        trade.setPayCode(input.getPayCode());
        trade.setPayment(input.getPayment());
        trade.setCreateDate(input.getCreateDate().toString());//DateTime.now().toString("yyyyMMddHHmmss")
        trade.setStatus("Waiting");
        trade.setType(input.getType());
        trade.setMethod(input.getMethod());

        tradeDetailService.save(trade);

        return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(), HttpStatus.OK);
    }

    @RequestMapping( method= RequestMethod.GET, value="/list/{accountId}" )
    public ResponseEntity<String> listByAccountId(@PathVariable String accountId) {
        try {

            List<TradeDetail> list = tradeDetailService.listByAccountId(accountId);

            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(list);

            return new ResponseEntity<String>(json, HttpStatus.OK);
        } catch (JsonProcessingException e) {
            LOGGER.error("[EXCEPTION] listByAccountId : " + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("JsonProcessingException"), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] listByAccountId : " + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Exception"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @RequestMapping( method= RequestMethod.GET, value="/list/{accountId}/{orderId}" )
    public ResponseEntity<String> getOne(@PathVariable String accountId, @PathVariable String orderId) {

        try {
            TradeDetail tradeDetail =  tradeDetailService.getOne(accountId,orderId);

            ObjectMapper mapper = new ObjectMapper();

            String json = mapper.writeValueAsString(tradeDetail);
            return new ResponseEntity<String>(json, HttpStatus.OK);
        } catch (JsonProcessingException e) {
            LOGGER.error("[EXCEPTION] getOne : " + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("JsonProcessingException"), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] getOne : " + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Exception"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @RequestMapping( method= RequestMethod.GET, value="/account" )
    public ResponseEntity<String> list1(@RequestParam(value = "id") String id, @RequestParam(value = "bankId") long bankId) {

        Merchant merchant = merchantService.getOne(id);

        if(merchant == null) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage("Merchant not found."), HttpStatus.OK);
        }

        PaymentAccount paymentAccount = paymentAccountService.getOne(id, bankId);

        return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(paymentAccount.getPaymentAccountRandomId().toString()), HttpStatus.OK);
    }

    @RequestMapping( method= RequestMethod.POST, value="/countByDate" )
    public ResponseEntity<String> countByDate(@RequestBody CustomizePageRequest request) {
        if(Strings.isNullOrEmpty(request.getAccountId())) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage("Argument error."), HttpStatus.OK);
        }

        long sum = tradeDetailService.countPaymentByCreateDate(request);
        return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(String.valueOf(sum)), HttpStatus.OK);
    }

    @RequestMapping( method= RequestMethod.POST, value="/comment" )
    public ResponseEntity<String> editComment(@RequestBody String request) {

        JsonObject jsonObject = new Gson().fromJson(request, JsonObject.class);

        String orderId = jsonObject.get("orderId").getAsString();
        String comment = jsonObject.get("comment").getAsString();

        TradeDetail tradeDetail = tradeDetailService.getOne(orderId);

        tradeDetail.setComment(comment);

        tradeDetailService.save(tradeDetail);

        return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage("Comment has been added"), HttpStatus.OK);
    }

}
