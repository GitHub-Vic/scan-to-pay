package co.intella.controller.contractStore;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import co.intella.domain.contratStore.PaymentAccountIncome;
import co.intella.model.Bank;
import co.intella.model.Merchant;
import co.intella.model.PaymentAccount;
import co.intella.service.BankService;
import co.intella.service.MerchantService;
import co.intella.service.PaymentAccountService;
import co.intella.service.TokenService;
import co.intella.utility.GeneralResponseMessageUtil;

/**
 * @author Alex
 */
@Controller
@RequestMapping("/api/paymentaccount")
public class PaymentAccountController {

    private final Logger LOGGER = LoggerFactory.getLogger(PaymentAccountController.class);

    @Resource
    private MerchantService merchantService;

    @Resource
    private BankService bankService;

    @Resource
    private PaymentAccountService paymentAccountService;
    
    @Autowired
    private MessageSource messageSource;

     private Locale locale = new Locale("zh_TW");

    @RequestMapping(method= RequestMethod.GET, value="/get/{merchantId}/{bankId}")
    public ResponseEntity<String> get(@PathVariable String merchantId, @PathVariable long bankId) {

        Merchant merchant = merchantService.getOne(merchantId);
        if(merchant == null) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage("Merchant not exists."), HttpStatus.OK);
        }

        PaymentAccount paymentAccount = paymentAccountService.getOne(merchantId, bankId);
        return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(new Gson().toJson(paymentAccount)), HttpStatus.OK);
    }

    @RequestMapping( method= RequestMethod.POST, value="/save" )
    public ResponseEntity<String> createOrUpdate(@RequestBody PaymentAccountIncome input) {

        Merchant merchant = merchantService.getOne(input.getMerchantId());

        if(merchant == null) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Merchant not exists."), HttpStatus.OK);
        }

        Bank bank = bankService.getOne(input.getBankId());

        if(bank == null) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Payment platform not exists."), HttpStatus.OK);
        }

        PaymentAccount paymentAccount = paymentAccountService.getOne(merchant.getAccountId(), bank.getBankId());

//        if(paymentAccount != null) {
//
//            if(!Strings.isNullOrEmpty(input.getAccount()) && !input.getAccount().equals(paymentAccount.getDescription())) {
//                paymentAccount.setAccount(input.getAccount());
//            }
//
//            if(!Strings.isNullOrEmpty(input.getDescription()) && !input.getDescription().equals(paymentAccount.getDescription())) {
//                paymentAccount.setDescription(input.getDescription());
//            }
//
//        } else {

            paymentAccount = new PaymentAccount();

            paymentAccount.setDescription(input.getDescription());
            paymentAccount.setAccount(input.getAccount());
            paymentAccount.setBank(bankService.getOne(input.getBankId()));
            paymentAccount.setMerchant(merchantService.getOne(input.getMerchantId()));
            paymentAccount.setDefault(true);
//        }

        PaymentAccount entity = paymentAccountService.save(paymentAccount);
        return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(new Gson().toJson(entity)), HttpStatus.OK);
    }

    @RequestMapping( method= RequestMethod.GET, value="/list" )
    public ResponseEntity<String> list(@RequestParam(value = "merchantId") String merchantId, @RequestParam(value = "bankId") long bankId) {

        try {
            Merchant merchant = merchantService.getOne(merchantId);

            if(merchant == null) {
                return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage("Merchant not found."), HttpStatus.OK);
            }

            PaymentAccount paymentAccount =  paymentAccountService.getOne(merchantId, bankId);

            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(paymentAccount);

            return new ResponseEntity<String>(json, HttpStatus.OK);
        } catch (JsonProcessingException e) {
            LOGGER.error("[EXCEPTION] list : " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("JsonProcessingException"), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] list : " + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Exception"), HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

    @RequestMapping( method= RequestMethod.GET, value="/listAll" )
    public ResponseEntity<String> list(@RequestParam(value = "merchantId") String merchantId) {

        try {
            List<PaymentAccount> paymentAccount =  paymentAccountService.listAllByAccount(merchantId);

            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(paymentAccount);

            return new ResponseEntity<String>(json, HttpStatus.OK);
        } catch (JsonProcessingException e) {
            LOGGER.error("[EXCEPTION] listAll : " + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("JsonProcessingException"), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] listAll : " + e.toString() + ", " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Exception"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}