package co.intella.controller.contractStore;

import co.intella.crypto.AesCryptoUtil;
import co.intella.crypto.KeyReader;
import co.intella.domain.StatusMessage;
import co.intella.domain.contratStore.MerchantIncome;
import co.intella.model.IntegratedRequest;
import co.intella.model.Merchant;
import co.intella.net.Constant;
import co.intella.service.MerchantService;
import co.intella.utility.GeneralResponseMessageUtil;
import co.intella.utility.GeneralUtil;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.sun.org.apache.xml.internal.security.utils.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.apache.commons.lang.StringUtils;
import org.joda.time.*;

import javax.annotation.Resource;
import javax.crypto.SecretKey;
import javax.servlet.http.HttpServletRequest;

import static co.intella.crypto.RsaCryptoUtil.decrypt;

import java.io.File;
import java.security.PrivateKey;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

/**
 * @author Miles Wu
 */
@Controller
@RequestMapping("/api/merchant")
public class MerchantController {

    private final Logger LOGGER = LoggerFactory.getLogger(MerchantController.class);

    @Resource
    private MerchantService merchantService;
    
    @Autowired
    private MessageSource messageSource;
    
     private Locale locale = new Locale("zh_TW");
    
    @Autowired
    private Environment env;

    @Value("${api.url}")
    private String API_URL;

    @RequestMapping( method= RequestMethod.GET, value="/check/{merchantId}" )
    public ResponseEntity<String>  checkExists(@PathVariable String merchantId) {
        Merchant user = merchantService.getOne(merchantId);

        if(user != null) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(), HttpStatus.OK);
        } else {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Account not found."), HttpStatus.OK);
        }
    }

    @RequestMapping( method= RequestMethod.GET, value="/list" )
    public ResponseEntity<String> list() {

        List<Merchant> list = merchantService.listAll();

        ObjectMapper mapper = new ObjectMapper();
        String json;
        try {
            json = mapper.writeValueAsString(list);
        } catch (JsonProcessingException e) {
            LOGGER.error("[EXCEPTION] json processing error. " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("JsonProcessingException"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<String>(json, HttpStatus.OK);
    }

    @RequestMapping( method= RequestMethod.GET, value="/get/{uid}" )
    public ResponseEntity<String>  get(@PathVariable String uid) {

        Merchant user = merchantService.getOne(uid);

        ObjectMapper mapper = new ObjectMapper();
        String json ;
        try {
            json = mapper.writeValueAsString(user);
        } catch (JsonProcessingException e) {
            LOGGER.error("[EXCEPTION] json processing error. " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("JsonProcessingException"), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<String>(json, HttpStatus.OK);
    }



    @RequestMapping( method= RequestMethod.GET, value="/count" )
    public ResponseEntity<String> count() {

        long count = merchantService.count();

        ObjectMapper mapper = new ObjectMapper();
        String json;
        try {
            json = mapper.writeValueAsString(count);
        } catch (JsonProcessingException e) {
            LOGGER.error("[EXCEPTION] json processing error. " + Arrays.toString(e.getStackTrace()));
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("JsonProcessingException"), HttpStatus.INTERNAL_SERVER_ERROR);
        }


        return new ResponseEntity<String>(json, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/create")
    public ResponseEntity<String> create (@RequestBody MerchantIncome input){

        if (merchantService.getOne(input.getAccountId()) != null) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Account already exists."), HttpStatus.OK);
        }

        Merchant newAccount = new Merchant();

        newAccount.setName(input.getName());
        newAccount.setEmail(input.getEmail());
        newAccount.setCreator(input.getCreator());
        newAccount.setPin(GeneralUtil.getHash(input.getPin()));
        newAccount.setAccountId(input.getAccountId());
        newAccount.setComId(input.getComId());
        newAccount.setPhone(input.getPhone());
        DateTime.now();
        newAccount.setSignDate(DateTime.now().toString("yyyyMMddHHmmss"));
        newAccount.setStatus("unverified");
        newAccount.setPaycodeToken(input.getToken());
        String activateToken = UUID.randomUUID().toString().substring(0, 15);
        newAccount.setActivateToken(activateToken);
        newAccount.setRefundPin(input.getRefundPin());

        merchantService.create(newAccount);

        return new ResponseEntity<String>(getSuccessMessage(input.getAccountId(), activateToken), HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.POST, value = "/editMerchant")
    public ResponseEntity<String> editMerchant (@RequestBody MerchantIncome input){

        Merchant merchant = merchantService.getOne(input.getAccountId());

        if (merchant != null) {
            merchant.setName(input.getName());
            merchant.setEmail(input.getEmail());
            merchant.setAccountId(input.getAccountId());
            merchant.setComId(input.getComId());
            merchant.setPhone(input.getPhone());
            merchantService.create(merchant);
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(), HttpStatus.OK);
        } else {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("account is not existed."), HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/editPassword")
    public ResponseEntity<String> editPassword (@RequestBody MerchantIncome input){

        Merchant merchant = merchantService.getOne(input.getAccountId());

        if (merchant == null || !merchant.getPin().equals(GeneralUtil.getHash(input.getPin()))) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Password incorrect."), HttpStatus.OK);
        }
        merchant.setPin(GeneralUtil.getHash(input.getNewPin()));
        merchantService.editPassword(merchant);
        return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(), HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.POST, value = "/editRefundPassword")
    public ResponseEntity<String> editRefundPassword (@RequestBody MerchantIncome input){

        Merchant merchant = merchantService.getOne(input.getAccountId());
        //String hashed = BCrypt.hashpw(input.getRefundPin(), BCrypt.gensalt());
        merchant.setRefundPin(input.getRefundPin());
        merchantService.editPassword(merchant);
        return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(), HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/validate")
    public ResponseEntity<String> activateAccount (@RequestParam(value = "id") String id, @RequestParam(value = "token") String activateToken){

        // todo merchant notify
        boolean result = merchantService.activateAccount(id, activateToken);
        if (!result) {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Account activate fail."), HttpStatus.OK);
        } else {
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getSuccessMessage(), HttpStatus.OK);
        }
    }
    
    private String getSuccessMessage(String accountId, String token) {
        StatusMessage status = new StatusMessage();
        status.setStatus("ok");
        status.setMessage("success");
        status.setValidateURL(API_URL+"allpaypass/api/merchant/validate?id="+accountId+"&token="+token);
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        return gson.toJson(status);
    }
    

}
