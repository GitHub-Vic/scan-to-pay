package co.intella.controller;

import co.intella.domain.pi.PiInAppOrderQueryRequestData;
import co.intella.domain.pi.PiInAppOrderQueryResponse;
import co.intella.model.PaymentAccount;
import co.intella.model.TradeDetail;
import co.intella.service.MailService;
import co.intella.service.QueryJobService;
import co.intella.service.TradeDetailService;
import co.intella.utility.GeneralResponseMessageUtil;
import co.intella.utility.GeneralUtil;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.util.Arrays;

/**
 * @author Miles
 */
@Controller
@RequestMapping("/api/pi")
public class PiInAppController {

    private final Logger LOGGER = LoggerFactory.getLogger(PiInAppController.class);

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private QueryJobService jobService;

    @Resource
    private MailService mailService;

    @Value("${emergency.mail.notify.staff}")
    private String[] mailToStaff;

    /**
     * This api used by Pi
     * @param PiInAppOrderQueryRequestData input
     * @return ResponseEntity<String>
     */
    @RequestMapping( method= RequestMethod.POST, value="/order" )
    public ResponseEntity<String> get(@RequestBody PiInAppOrderQueryRequestData input) {

        LOGGER.info("[REQUEST] /api/pi/order " + new Gson().toJson(input));

        try {
            TradeDetail tradeDetail =  tradeDetailService.getOneByPartnerUserIdAndOrderId(input.getPartnerUserId(), input.getPartnerOrderId());

            if(null == tradeDetail) {
                return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Order Not Found."), HttpStatus.OK);
            }

            PiInAppOrderQueryResponse response = new PiInAppOrderQueryResponse();


            PaymentAccount paymentAccount = tradeDetail.getPaymentAccount();

            if(null == paymentAccount) {
                return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Account Not Found."), HttpStatus.OK);
            }

            response.setMoney(tradeDetail.getPayment());
            response.setPartnerUserId(tradeDetail.getUserId());
            response.setPartnerId(paymentAccount.getAccount());
            response.setPaid(!tradeDetail.getStatus().equals(SystemInstance.TRADE_WAITING));
            response.setPartnerName(paymentAccount.getMerchant().getName());
            response.setPartnerOrderId(tradeDetail.getOrderId());

            String result = new Gson().toJson(response);
            LOGGER.info("[RESPONSE] order check for pi. " + new Gson().toJson(input));

            jobService.addJob(tradeDetail.getOrderId(), tradeDetail.getAccountId(), paymentAccount.getAccount(), 7);

            return new ResponseEntity<String>(result, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] /api/pi/order " + e.getMessage() + ", \n" + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
            sendEmergencyMail(e.getMessage() + "\n" + Arrays.toString(e.getStackTrace()).replaceAll(",", ",</br>"), "/api/pi/order");
            return new ResponseEntity<String>(GeneralResponseMessageUtil.getFailureMessage("Internal Error."), HttpStatus.OK);
        }

    }

    private void sendEmergencyMail(String input, String emailTitle) {
        try {
            mailService.send(GeneralUtil.getMailInformation(mailToStaff, emailTitle), GeneralUtil.getMailContext(input), "emergencyMail");
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION][reconciliation] sendEmergencyMail."  + e.getMessage() + ", \n" + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
        }
    }
}
