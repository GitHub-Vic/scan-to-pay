package co.intella.controller;


import co.intella.service.IcashPayService;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Controller
@RequestMapping("/api/IcashPay")
public class IcashPayController {
    private final Logger LOGGER = LoggerFactory.getLogger(IcashPayController.class);
    @Resource
    private IcashPayService icashPayService;

    @Value("${api.url}")
    private String API_URL;

    @RequestMapping(method = RequestMethod.GET, value = "/{urlToken}/{amt}")
    public ResponseEntity<String> onlinePay(@PathVariable String urlToken, @PathVariable String amt) throws Exception {
        LOGGER.info("[REQUEST]/api/IcashPay: " + "urlToken=" + urlToken + "amt=" + amt);
        String result = null;
        try {
            result = icashPayService.getPaymentUrl(amt, urlToken);

        } catch (Exception e) {
            e.getStackTrace();
        }

        return new ResponseEntity<String>(result, HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.POST, value = "/notify/{mchId}")
    public ResponseEntity<String> donotify(@RequestHeader Map<String, String> headers, @RequestBody String body, @PathVariable String mchId) {
        String notifly = "0";  //回傳值為純字串並無參數名稱若回傳值第一碼值為 1 時為成功。 若回傳值第一碼值為 0 時為失敗
        headers.forEach((key, value) -> {
            LOGGER.info("[IcashPay]/notify[Header]: " + String.format("Header '%s' = %s", key, value));
        });
            LOGGER.info("[IcashPay]/notify[body]: " + body);
        try {
            String signature = headers.get("x-icp-signature");
            notifly = icashPayService.notifly(signature, body, mchId);

        } catch (Exception e) {
            e.getStackTrace();
        }
        return new ResponseEntity<String>(notifly, HttpStatus.OK);

    }


    @GetMapping("/backUrl")
    public ResponseEntity<String> backUrl(@RequestParam Map<String, String> allParams, @RequestParam(name = "rtnCode") String rtnCode) {

        LOGGER.info("[icashpay][REQUEST] /backUrl: " + "rtnCode :" + rtnCode);
        try {

            for (Map.Entry entry : allParams.entrySet()) {

                LOGGER.info("[icashpay][REQUEST] /backUrl: " + "key: " + entry.getKey() + "; value: " + entry.getValue());
            }

        } catch (Exception e) {
            e.getStackTrace();
        }
        return null;
    }


}
