package co.intella.controller;

import co.intella.domain.ecpay.*;
import co.intella.service.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author Miles
 */
@Controller
@RequestMapping("/api/ecpay")
public class ECPayController {

    private final Logger LOGGER = LoggerFactory.getLogger(ECPayController.class);

    @Resource
    private ECPayService ecPayService;

    @RequestMapping(method = RequestMethod.POST, value = "/gettransaction")
    public ResponseEntity<String> getTransaction(@RequestBody String request) throws JsonProcessingException {
        LOGGER.info("[REQUEST] api/ecpay/gettransaction :" + request);
        JsonObject jsonObject = new Gson().fromJson(request, JsonObject.class);
        String ss = jsonObject.get("orderId").getAsString();
        String result = ecPayService.queryTransaction(ss);
        LOGGER.info("[RESPONSE] api/ecpay/gettransaction :" + result);

        return new ResponseEntity<String>(result, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/credittrade/{spToken}")
    public ModelAndView creditTrade(@PathVariable String spToken) throws JsonProcessingException {
        LOGGER.info("[REQUEST] api/ecpay/credittrade/" + spToken);
        return ecPayService.doTransaction("2000132", spToken);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/doaction")
    public ResponseEntity<String> doAction(@RequestBody ECPayDoAction ecPayDoAction) throws JsonProcessingException {
        LOGGER.info("[REQUEST] api/ecpay/doaction :" + new Gson().toJson(ecPayDoAction));
        ECPayDoActionResponse ecPayDoActionResponse = ecPayService.doAction(ecPayDoAction);
        LOGGER.info("[RESPONSE] api/ecpay/doaction :" + new Gson().toJson(ecPayDoActionResponse));

        return new ResponseEntity<String>(new Gson().toJson(ecPayDoActionResponse), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/capture")
    public ResponseEntity<String> capture(@RequestBody ECPayCapture ecPayCapture) throws JsonProcessingException {
        LOGGER.info("[REQUEST] api/ecpay/capture :" + new Gson().toJson(ecPayCapture));
        ECPayCaptureResponse ecPayCaptureResponse = ecPayService.capture(ecPayCapture);
        LOGGER.info("[REPONSE] api/ecpay/capture :" + new Gson().toJson(ecPayCaptureResponse));

        return new ResponseEntity<String>(new Gson().toJson(ecPayCaptureResponse), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/singlequery")
    public ResponseEntity<String> queryOrder(@RequestBody ECPayQuery ecPayQuery) throws JsonProcessingException {
        ecPayQuery.setTimeStamp(Long.toString(new Date().getTime()));
        ecPayQuery.setPlatformID("");

        LOGGER.info("[REQUEST] api/ecpay/queryorder :" + new Gson().toJson(ecPayQuery));
        ECPayQueryResponse ecPayQueryResponse = ecPayService.queryOrder(ecPayQuery);
        LOGGER.info("[RESPONSE] api/ecpay/queryorder :" + new Gson().toJson(ecPayQueryResponse));

        return new ResponseEntity<String>(new Gson().toJson(ecPayQueryResponse), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/createorder")
    public ResponseEntity<String> creditOrder(@RequestBody ECPayOrder ecPayOrder) throws JsonProcessingException {
        LOGGER.info("[REQUEST] api/ecpay/createorder :" + new Gson().toJson(ecPayOrder));
        ECPayOrderResponse ecPayOrderResponse = ecPayService.createOrder(ecPayOrder);
        LOGGER.info("[RESPONSE] api/ecpay/createorder :" + new Gson().toJson(ecPayOrderResponse));

        if(ecPayOrderResponse.getReturnCode() != 1) {
            LOGGER.info("[ECPay] Creating Order failed.");
            String string = unicodeToChinese(ecPayOrderResponse.getReturnMessage());
            ecPayOrderResponse.setReturnMessage(string);
        }

        return new ResponseEntity<String>(new Gson().toJson(ecPayOrderResponse), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/posttrade")
    public ModelAndView creditTradePost(@RequestBody ECPayOrder ecPayOrder) throws JsonProcessingException {
        ECPayOrderResponse ecPayOrderResponse = ecPayService.createOrder(ecPayOrder);
        LOGGER.info("[REQUEST] api/ecpay/createtrade :" + new Gson().toJson(ecPayOrderResponse));
        return ecPayService.doTransaction(ecPayOrderResponse.getMerchantId(), ecPayOrderResponse.getSpToken());
    }

    private String unicodeToChinese(String string) {
        String str = string.replace("\\u", ",");
        String[] s2 = str.split(",");
        String s1 = "";
        for (int i = 1; i < s2.length; i++) {
            s1 = s1 + (char) Integer.parseInt(s2[i], 16);
        }
        return s1;
    }

}
