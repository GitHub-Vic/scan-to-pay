package co.intella;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import java.io.File;

/**
 * @author Miles Wu
 */
@SpringBootApplication
@ComponentScan
@EnableScheduling
@EnableAsync
public class AllPayApplication extends SpringBootServletInitializer {

    public static String APNS_PD;

    @Value("${intella.apns.pd}")
    public void setApnsPd(String apnsPd) {
        APNS_PD = apnsPd;
    }

    public static File APNS_KEY;

    public static String APPLE_PUSH_SERVER;

    @Value("${intella.apns.key}")
    public void setApnsKey(String apnsKey) {
        ClassLoader classLoader = getClass().getClassLoader();

        APNS_KEY = new File(classLoader.getResource(apnsKey).getFile());
    }

    @Value("${apple.push.server}")
    public void setApplePushServer(String url) {
        APPLE_PUSH_SERVER = url;
    }


    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(AllPayApplication.class);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }


//    static {
//        disableSslVerification();
//    }
//
//    private static void disableSslVerification() {
//        try{
//            // Create a trust manager that does not validate certificate chains
//            TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
//                public X509Certificate[] getAcceptedIssuers() {
//                    return null;
//                }
//                public void checkClientTrusted(X509Certificate[] certs, String authType) {
//                }
//                public void checkServerTrusted(X509Certificate[] certs, String authType) {
//                }
//            }
//            };
//
//            // Install the all-trusting trust manager
//            SSLContext sc = SSLContext.getInstance("SSL");
//            sc.init(null, trustAllCerts, new java.security.SecureRandom());
//            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
//
//            // Create all-trusting host name verifier
//            HostnameVerifier allHostsValid = new HostnameVerifier() {
//                public boolean verify(String hostname, SSLSession session) {
//                    return true;
//                }
//            };
//
//            // Install the all-trusting host verifier
//            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (KeyManagementException e) {
//            e.printStackTrace();
//        }
//    }


    public static void main(String[] args) throws Exception {
        SpringApplication.run(AllPayApplication.class, args);
    }

}
