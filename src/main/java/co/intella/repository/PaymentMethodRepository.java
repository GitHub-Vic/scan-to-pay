package co.intella.repository;

import co.intella.model.PaymentMethod;

public interface PaymentMethodRepository  {
	public PaymentMethod getOne(String method);
}
