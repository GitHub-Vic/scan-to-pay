package co.intella.repository;

import co.intella.model.Recipient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Miles
 */
@Component
public interface RecipientRepository extends JpaRepository<Recipient, Long>, QueryDslPredicateExecutor<Recipient> {
    @Modifying
    @Transactional
    @Query("delete from Recipient where serialId = ?1 and appId = ?2")
    void deleteUsersBySerialId(String serialId, String appId);

    @Modifying
    @Transactional
    @Query("delete from Recipient where serialId = ?1 and appId is null")
	void deleteUsersBySerialIdNullAppId(String serialId);
}
