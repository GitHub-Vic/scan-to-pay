package co.intella.repository;

import co.intella.model.RefundDetail;
import co.intella.model.TradeDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

/**
 * @author Miles Wu
 */
@Component
public interface RefundDetailRepository extends JpaRepository<RefundDetail, Long>, QueryDslPredicateExecutor<RefundDetail> {

    @Query("SELECT SUM(t.payment) FROM TradeDetail t WHERE t.accountId = :id AND t.createDate BETWEEN :startDate AND :endDate")
    Long countPaymentByCreateDate(@Param("id") String id, @Param("startDate") String startDate, @Param("endDate") String endDate);

}
