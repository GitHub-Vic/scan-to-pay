package co.intella.repository;

import co.intella.model.CaptureDetail;
import co.intella.model.CaptureOrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Component;

/**
 * @author Andy Lin
 */
@Component
public interface CaptureDetailRepository  {
}
