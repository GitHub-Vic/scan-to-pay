package co.intella.repository;

import co.intella.model.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Component;

/**
 * @author Alex
 */
@Component
public interface TicketRepository extends JpaRepository<Ticket, Long>, QueryDslPredicateExecutor<Ticket> {
}
