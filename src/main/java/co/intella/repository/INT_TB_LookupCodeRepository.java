package co.intella.repository;

import java.util.List;

import co.intella.model.IntTbLookupCodeNoFK;


/**
 * @author Mustang
 */
public interface INT_TB_LookupCodeRepository {
	
	List<IntTbLookupCodeNoFK> findAllByLookType(String type);
	
	IntTbLookupCodeNoFK getByLookTypeAndLookupCode(String lookupType, String lookupCode);
}
