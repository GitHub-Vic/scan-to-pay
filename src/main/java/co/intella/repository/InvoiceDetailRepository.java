package co.intella.repository;

import co.intella.model.InvoiceDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Component;

/**
 * @author Alex
 */
@Component
public interface InvoiceDetailRepository extends JpaRepository<InvoiceDetail, Long>, QueryDslPredicateExecutor<InvoiceDetail> {
}
