package co.intella.repository;

import co.intella.model.ConsumerToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * @author Miles
 */
public interface ConsumerTokenRepository extends JpaRepository<ConsumerToken, Long>, QueryDslPredicateExecutor<ConsumerToken> {
}
