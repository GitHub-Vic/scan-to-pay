package co.intella.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Component;

import co.intella.model.INT_TB_CheckoutCtrl;

/**
 * @author Nick Lian
 */
@Component
public interface INT_TB_CheckoutCtrlRepository extends JpaRepository<INT_TB_CheckoutCtrl,Long>, QueryDslPredicateExecutor<INT_TB_CheckoutCtrl>{

}
