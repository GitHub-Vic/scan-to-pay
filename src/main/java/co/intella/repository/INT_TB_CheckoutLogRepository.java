package co.intella.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Component;

import co.intella.model.INT_TB_CheckoutLog;

/**
 * @author Nick Lian
 */
@Component
public interface INT_TB_CheckoutLogRepository extends JpaRepository<INT_TB_CheckoutLog, Long>, QueryDslPredicateExecutor<INT_TB_CheckoutLog>{

}
