package co.intella.repository;

import co.intella.model.TicketOperator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Component;

/**
 * @author Alex
 */
@Component
public interface TicketOperatorRepository extends JpaRepository<TicketOperator, Long>, QueryDslPredicateExecutor<TicketOperator> {
}
