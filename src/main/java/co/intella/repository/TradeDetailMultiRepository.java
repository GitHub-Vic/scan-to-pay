package co.intella.repository;

import co.intella.request.TradeDetailMultiRequest;

import java.util.List;
import java.util.Map;

public interface TradeDetailMultiRepository {
    List<Map<String, Object>> list(TradeDetailMultiRequest request);

    List<Integer> getAccountIdsForListTradeDetail(TradeDetailMultiRequest request);
}
