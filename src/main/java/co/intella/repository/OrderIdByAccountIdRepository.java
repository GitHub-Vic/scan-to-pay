package co.intella.repository;

import java.util.Map;

public interface OrderIdByAccountIdRepository {

	public Map<String, String> getGetOrderIdByAccountId(String accountId);

}
