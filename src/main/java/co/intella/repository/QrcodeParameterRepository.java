package co.intella.repository;

import co.intella.model.QrcodeParameter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Component;

/**
 * @author Alex
 */
@Component
public interface QrcodeParameterRepository extends JpaRepository<QrcodeParameter, Long>, QueryDslPredicateExecutor<QrcodeParameter> {
}
