package co.intella.repository;

public interface PaymentAccountCustomRepository {

	Integer checkQrcodePayment(String status);

	Integer checkTicketPayment(String status);

}
