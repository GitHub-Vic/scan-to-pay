package co.intella.repository;

import co.intella.model.Sale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * @author Andy Lin
 */
public interface SaleRepository extends JpaRepository<Sale, Long>, QueryDslPredicateExecutor<Sale> {
}
