package co.intella.repository;

import co.intella.model.PayerInfo;

public interface PayerInfoRepository {
    public PayerInfo findPayerInfo(String email, String mobileNumber, String shortId);
    public int updateToken(String payerId, String token);
    public PayerInfo findByToken(String payerId, String token);
}
