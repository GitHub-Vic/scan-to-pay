package co.intella.repository;

import co.intella.model.PaymentAccount;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Component;

/**
 * @author Miles Wu
 */
@Component
public interface PaymentAccountRepository extends JpaRepository<PaymentAccount, Long>, QueryDslPredicateExecutor<PaymentAccount> {

  //@Query("SELECT"+ "new com.intella.model.PaymentAccount"+" FROM PaymentAccount p where p.spID is not null group by p.spID,p.systemID,p.sftpUser,p.sftpPasswd")
  @Query("SELECT " +
          "    new PaymentAccount(p.systemID, p.spID, p.sftpUser, p.sftpPasswd) " +
          "FROM " +
          "    PaymentAccount p " +
          "WHERE " +
          "  p.bank.bankId = 21 and  p.spID is not null and length(trim(p.spID)) > 0  and p.isDefault = 1 " +
          "GROUP BY " +
          "    p.systemID,p.spID,p.sftpUser,p.sftpPasswd")
  List<PaymentAccount> groupAcerTexiIpass();

}
