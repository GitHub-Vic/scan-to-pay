package co.intella.repository;

import co.intella.model.INT_TB_CancelTradeDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Component;

@Component
public interface INT_TB_CancelTradeDetailRepository extends JpaRepository<INT_TB_CancelTradeDetail, Long>, QueryDslPredicateExecutor<INT_TB_CancelTradeDetail> {
}
