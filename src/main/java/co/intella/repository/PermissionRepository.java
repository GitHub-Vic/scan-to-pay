package co.intella.repository;

import co.intella.model.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Component;

/**
 * @author Andy Lin
 */
@Component
public interface PermissionRepository extends JpaRepository<Permission, Long>, QueryDslPredicateExecutor<Permission> {
}
