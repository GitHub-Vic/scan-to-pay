package co.intella.repository;

import co.intella.model.TicketHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Component;

/**
 * @author Alex
 */
@Component
public interface TicketHistoryRepository extends JpaRepository<TicketHistory, Long>, QueryDslPredicateExecutor<TicketHistory> {
}
