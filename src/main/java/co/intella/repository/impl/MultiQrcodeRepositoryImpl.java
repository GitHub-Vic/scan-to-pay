package co.intella.repository.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.intella.model.MultiQrcode;
import co.intella.repository.MultiQrcodeRespository;

@Repository
public class MultiQrcodeRepositoryImpl implements MultiQrcodeRespository {
	
	@Autowired
    private EntityManager entityManager;
	
	@Override
	public List<MultiQrcode> findMutiQRCodeByPayer(String shortId, String email, String payerName, String mobileNumber) {
	    String sql = "SELECT distinct qp.shortId , q.totalFee , q.body , q.detail, qp.mergeShortId FROM allpaypass_new.INT_TB_QRCodePayer qp " + 
	    		"inner join allpaypass_new.QrcodeParameter q on q.shortId = qp.shortId " + 
	    		"inner join allpaypass_new.INT_TB_PayerInfo p on p.payerId = qp.payerId " + 
	    		"left join TradeDetail t on t.qrcodeToken = qp.shortId " + 
	    		"where qp.payerId in (select payerId from INT_TB_QRCodePayer where " + 
	    		"(ifnull(:shortId,'') = '' or shortId like CONCAT('%',:shortId,'%')  ) " + 
	    		"and (ifnull(:email,'') = '' or p.email like CONCAT('%',:email,'%')  ) " + 
	    		"and (ifnull(:payerName,'') = '' or p.payerName like CONCAT('%',:payerName,'%')  ) " + 
	    		"and (ifnull(:mobileNumber,'') = '' or p.mobileNumber like CONCAT('%',:mobileNumber,'%') ) " + 
	    		"and (ifnull(t.status,'') = '' or t.status !='Trade Success') and q.status = 1";
	    Query query = entityManager.createNativeQuery(sql).setParameter("shortId", shortId)
	    		                                          .setParameter("email", email)
                                                          .setParameter("payerName", payerName)
	    		                                          .setParameter("mobileNumber", mobileNumber); 
	    query.unwrap(SQLQuery.class).addScalar("shortId", StandardBasicTypes.STRING)
	                                .addScalar("totalFee", StandardBasicTypes.STRING)
	                                .addScalar("body", StandardBasicTypes.STRING)
	                                .addScalar("detail", StandardBasicTypes.STRING)
	                                .addScalar("mergeShortId", StandardBasicTypes.STRING)
	                                .setResultTransformer(Transformers.aliasToBean(MultiQrcode.class));
	    @SuppressWarnings("unchecked")
		List<MultiQrcode> resultList = query.getResultList();
	    return resultList;
	}
	
	@Override
	public List<MultiQrcode> findMutiQRCodeByPayer(String shortId, String email, String payerName, String mobileNumber, String payerId) {
	    String sql = "SELECT distinct qp.shortId , q.totalFee , q.body , q.detail, qp.mergeShortId, qp.payerId FROM allpaypass_new.INT_TB_QRCodePayer qp " + 
	    		"inner join allpaypass_new.QrcodeParameter q on q.shortId = qp.shortId " + 
	    		"inner join allpaypass_new.INT_TB_PayerInfo p on p.payerId = qp.payerId " + 
	    		"left join TradeDetail t on t.qrcodeToken = qp.shortId " + 
	    		"where qp.payerId = :payerId " +
	    		"and (ifnull(:email,'') = '' or p.email like CONCAT('%',:email,'%')  ) " + 
	    		"and (ifnull(:payerName,'') = '' or p.payerName like CONCAT('%',:payerName,'%')  ) " + 
	    		"and (ifnull(:mobileNumber,'') = '' or p.mobileNumber like CONCAT('%',:mobileNumber,'%') ) " + 
	    		"and (ifnull(t.status,'') = '' or t.status !='Trade Success') and q.status = 1";
	    Query query = entityManager.createNativeQuery(sql).setParameter("email", email)
                                                          .setParameter("payerName", payerName)
	    		                                          .setParameter("mobileNumber", mobileNumber)
	    		                                          .setParameter("payerId", payerId); 
	    query.unwrap(SQLQuery.class).addScalar("shortId", StandardBasicTypes.STRING)
	                                .addScalar("totalFee", StandardBasicTypes.STRING)
	                                .addScalar("body", StandardBasicTypes.STRING)
	                                .addScalar("detail", StandardBasicTypes.STRING)
	                                .addScalar("mergeShortId", StandardBasicTypes.STRING)
	                                .addScalar("payerId", StandardBasicTypes.STRING)
	                                .setResultTransformer(Transformers.aliasToBean(MultiQrcode.class));
	    @SuppressWarnings("unchecked")
		List<MultiQrcode> resultList = query.getResultList();
	    return resultList;
	}
	
	@Override
	public List<MultiQrcode> findMutiQRCode(String shortId) {
	    String sql = "SELECT distinct qp.shortId , q.totalFee, q.body , q.detail, qp.mergeShortId FROM allpaypass_new.INT_TB_QRCodePayer qp " + 
	    		"inner join allpaypass_new.QrcodeParameter q on q.shortId = qp.shortId " + 
	    		"inner join allpaypass_new.INT_TB_PayerInfo p on p.payerId = qp.payerId " + 
	    		"left join TradeDetail t on t.qrcodeToken = qp.shortId " + 
	    		"where qp.payerId in (select payerId from INT_TB_QRCodePayer where shortId = :shortId) " + 
	    		"and q.totalFee != 0.00 " +
	    		"and (ifnull(t.status,'') = '' or t.status != 'Trade Success') and q.status = 1";
	    Query query = entityManager.createNativeQuery(sql).setParameter("shortId", shortId);
	    query.unwrap(SQLQuery.class).addScalar("shortId", StandardBasicTypes.STRING)
	                                .addScalar("totalFee", StandardBasicTypes.STRING)
	                                .addScalar("body", StandardBasicTypes.STRING)
	                                .addScalar("detail", StandardBasicTypes.STRING)
	                                .addScalar("mergeShortId", StandardBasicTypes.STRING)
	                                .setResultTransformer(Transformers.aliasToBean(MultiQrcode.class));
	    @SuppressWarnings("unchecked")
		List<MultiQrcode> resultList = query.getResultList();
	    return resultList;
	}
	
	@Override
	public List<MultiQrcode> findMutiQRCode(String shortId, String payerId) {
		System.out.println(payerId);
	    String sql = "SELECT distinct qp.shortId , q.totalFee, q.body , q.detail, qp.mergeShortId, qp.payerId FROM allpaypass_new.INT_TB_QRCodePayer qp " + 
	    		"inner join allpaypass_new.QrcodeParameter q on q.shortId = qp.shortId " + 
	    		"inner join allpaypass_new.INT_TB_PayerInfo p on p.payerId = qp.payerId " + 
	    		"left join TradeDetail t on t.qrcodeToken = qp.shortId " + 
	    		"where p.payerId = :payerId " + 
	    		"and q.totalFee != 0.00 " +
	    		"and (ifnull(t.status,'') = '' or t.status != 'Trade Success') and q.status = 1";
	    Query query = entityManager.createNativeQuery(sql).setParameter("payerId", payerId);
	    query.unwrap(SQLQuery.class).addScalar("shortId", StandardBasicTypes.STRING)
	                                .addScalar("totalFee", StandardBasicTypes.STRING)
	                                .addScalar("body", StandardBasicTypes.STRING)
	                                .addScalar("detail", StandardBasicTypes.STRING)
	                                .addScalar("mergeShortId", StandardBasicTypes.STRING)
	                                .addScalar("payerId", StandardBasicTypes.STRING)
	                                .setResultTransformer(Transformers.aliasToBean(MultiQrcode.class));
	    @SuppressWarnings("unchecked")
		List<MultiQrcode> resultList = query.getResultList();
	    return resultList;
	}
	
	@Override
	public MultiQrcode findOneByShortId(String shortId) {
	    String sql = "SELECT qp.shortId, qp.mergeShortId, qp.payerId FROM allpaypass_new.INT_TB_QRCodePayer qp where shortId = :shortId";
	    Query query = entityManager.createNativeQuery(sql).setParameter("shortId", shortId);
	    query.unwrap(SQLQuery.class).addScalar("shortId", StandardBasicTypes.STRING)
	                                .addScalar("mergeShortId", StandardBasicTypes.STRING)
	                                .addScalar("payerId", StandardBasicTypes.STRING)
	                                .setResultTransformer(Transformers.aliasToBean(MultiQrcode.class));	    
	    return (MultiQrcode) query.getSingleResult();
	}
	
	@Override
	public List<MultiQrcode> findListByMergeShortId(String mergeShortId) {
	    String sql = "SELECT qp.shortId, qp.mergeShortId, qp.payerId FROM allpaypass_new.INT_TB_QRCodePayer qp where mergeShortId = :mergeShortId";
	    Query query = entityManager.createNativeQuery(sql).setParameter("mergeShortId", mergeShortId);
	    query.unwrap(SQLQuery.class).addScalar("shortId", StandardBasicTypes.STRING)
	                                .addScalar("mergeShortId", StandardBasicTypes.STRING)
	                                .addScalar("payerId", StandardBasicTypes.STRING)
	                                .setResultTransformer(Transformers.aliasToBean(MultiQrcode.class));	    
	    @SuppressWarnings("unchecked")
		List<MultiQrcode> resultList = query.getResultList();
	    return resultList;
	}

	@Override
	public int updateOriginQrcode(String shortId, String mergeShortId) {
	    String sql = "update allpaypass_new.INT_TB_QRCodePayer set mergeShortId = :mergeShortId, megerDate = NOW() where shortId = :shortId";
	    return entityManager.createNativeQuery(sql).setParameter("mergeShortId", mergeShortId).setParameter("shortId", shortId).executeUpdate();
	}
	
	@Override
	public int updateCleanOriginQrcodeMergeShortId(String mergeShortId) {
	    String sql = "update allpaypass_new.INT_TB_QRCodePayer set mergeShortId = null, megerDate = null where mergeShortId = :mergeShortId";
	    return entityManager.createNativeQuery(sql).setParameter("mergeShortId", mergeShortId).setParameter("mergeShortId", mergeShortId).executeUpdate();
	}
	
	@Override
	public String queryMergeShortId(String shortId) {
	    String sql = "SELECT mergeShortId FROM allpaypass_new.INT_TB_QRCodePayer WHERE shortId = :shortId";
	    Query query = entityManager.createNativeQuery(sql).setParameter("shortId", shortId);
	    query.unwrap(SQLQuery.class).addScalar("mergeShortId", StandardBasicTypes.STRING);
		return (String) query.getSingleResult();
	}
	
	@Override
	public String checkPayerId(String shortId) {
	    String sql = "SELECT payerId FROM allpaypass_new.INT_TB_QRCodePayer WHERE shortId = :shortId";
	    Query query = entityManager.createNativeQuery(sql).setParameter("shortId", shortId);
	    query.unwrap(SQLQuery.class).addScalar("payerId", StandardBasicTypes.STRING);
		return (String) query.getSingleResult();
	}
	
	@Override
	public BigDecimal queryQrcodeTotalFee(String shortId) {
	    String sql = "SELECT totalFee FROM allpaypass_new.QrcodeParameter WHERE shortId = :shortId";
	    Query query = entityManager.createNativeQuery(sql).setParameter("shortId", shortId);
	    query.unwrap(SQLQuery.class).addScalar("totalFee", StandardBasicTypes.LONG);
	    return new BigDecimal((Long) query.getSingleResult());
	}
	
	@Override
	public int updateQrcode(String shortId) {
	    String sql = "update allpaypass_new.QrcodeParameter set status = 0, timeExpire = NOW() where shortId = :shortId";
	    return entityManager.createNativeQuery(sql).setParameter("shortId", shortId).executeUpdate();
	}
	
	@Override
	public String checkSelfOrderFinished(String shortId) {
	    String sql = "SELECT (CASE t.status " + 
	    		"WHEN 'Trade success' THEN '1' " + 
	    		"ELSE '0' " + 
	    		"END) status " + 
	    		"FROM allpaypass_new.TradeDetail t WHERE " + 
	    		"qrcodeToken = :shortId";
	    Query query = entityManager.createNativeQuery(sql).setParameter("shortId", shortId);
	    query.unwrap(SQLQuery.class).addScalar("status", StandardBasicTypes.STRING);
	    return (String) query.getSingleResult();
	}
	
	@Override
	public String checkOrderFinished(String shortId) {
	    String sql = "SELECT (CASE t.status " + 
	    		"WHEN 'Trade success' THEN '1' " + 
	    		"ELSE '0' " + 
	    		"END) status " + 
	    		"FROM allpaypass_new.TradeDetail t WHERE " + 
	    		"qrcodeToken = (SELECT mergeShortId FROM allpaypass_new.INT_TB_QRCodePayer WHERE shortId = :shortId)";
	    Query query = entityManager.createNativeQuery(sql).setParameter("shortId", shortId);
	    query.unwrap(SQLQuery.class).addScalar("status", StandardBasicTypes.STRING);
	    return (String) query.getSingleResult();
	}
	
}
