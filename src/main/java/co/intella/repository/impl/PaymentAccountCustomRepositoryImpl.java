package co.intella.repository.impl;

import java.math.BigInteger;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.intella.repository.PaymentAccountCustomRepository;

@Repository
public class PaymentAccountCustomRepositoryImpl implements PaymentAccountCustomRepository{
	
	@Autowired
	private EntityManager entityManager;
	
	@Override
	public Integer checkQrcodePayment(String accountId) {
		String sql = "select count(*) from PaymentAccount p " + 
				"inner join Merchant m on p.merchantSeqId = m.merchantSeqId " + 
				"where m.accountId = :accountId and bankId not in (18,21,28) ";
		BigInteger count = (BigInteger)entityManager.createNativeQuery(sql)
							.setParameter("accountId", accountId)
							.getSingleResult();
		return count.intValue();
	}
	
	@Override
	public Integer checkTicketPayment(String accountId) {
		String sql = 
			"select count(*) from PaymentAccount p " + 
			"inner join Merchant m on p.merchantSeqId = m.merchantSeqId " + 
			"inner join Device d on m.merchantSeqId = d.owner " + 
			"where m.accountId = :accountId and bankId in (18,21,28) and d.type = 'taxi'";
		BigInteger count = (BigInteger)entityManager.createNativeQuery(sql)
				.setParameter("accountId", accountId)
				.getSingleResult();
		return count.intValue();
	}
	
	

}
