package co.intella.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.intella.model.PayerInfo;
import co.intella.repository.PayerInfoRepository;

@Repository
public class PayerInfoRepositoryImpl implements PayerInfoRepository {

	@Autowired
    private EntityManager entityManager;
	
	@Override
	public PayerInfo findPayerInfo(String email, String mobileNumber, String shortId) {
		String sql = null;
		Query query = null;
		if ((StringUtils.isNotBlank(email))) {
	        sql = "SELECT payerId, accountId, email, payerName, mobileNumber, payerTag, payerType, sendType, isDelete FROM allpaypass_new.INT_TB_PayerInfo " + 
	    	      "WHERE email = :email";
		    query = entityManager.createNativeQuery(sql).setParameter("email", email); 
		} else if (StringUtils.isNotBlank(mobileNumber)) { 
	        sql = "SELECT payerId, accountId, email, payerName, mobileNumber, payerTag, payerType, sendType, isDelete FROM allpaypass_new.INT_TB_PayerInfo " + 
	    	      "WHERE mobileNumber = :mobileNumber";
		    query = entityManager.createNativeQuery(sql).setParameter("mobileNumber", mobileNumber); 
	    } else if (StringUtils.isBlank(shortId)) {
			sql = "SELECT payerId, accountId, email, payerName, mobileNumber, payerTag, payerType, sendType, isDelete " +
		          "FROM allpaypass_new.INT_TB_PayerInfo " +
				  "WHERE payerId in (SELECT payerId FROM allpaypass_new.INT_TB_QRCodePayer where shortId = ':shortId')";
		    query = entityManager.createNativeQuery(sql).setParameter("shortId", shortId);
		} else {
			return null;
		}

	    query.unwrap(SQLQuery.class).addScalar("payerId", StandardBasicTypes.STRING)
	                                .addScalar("accountId", StandardBasicTypes.STRING)
	                                .addScalar("email", StandardBasicTypes.STRING)
	                                .addScalar("payerName", StandardBasicTypes.STRING)
	                                .addScalar("mobileNumber", StandardBasicTypes.STRING)
	                                .addScalar("payerTag", StandardBasicTypes.STRING)
	                                .addScalar("payerType", StandardBasicTypes.STRING)
	                                .addScalar("sendType", StandardBasicTypes.STRING)
	                                .addScalar("isDelete", StandardBasicTypes.STRING)
	                                .setResultTransformer(Transformers.aliasToBean(PayerInfo.class));
	    PayerInfo payerInfo = (PayerInfo) query.getSingleResult();
	    return payerInfo;
	}

	@Override
	public int updateToken(String payerId, String token) {
	    String sql = "update allpaypass_new.INT_TB_PayerInfo set token = :token, timeExpired = (NOW() + INTERVAL 30 MINUTE) where payerId = :payerId";
	    return entityManager.createNativeQuery(sql).setParameter("payerId", payerId).setParameter("token", token).executeUpdate();
	}

	@Override
	public PayerInfo findByToken(String payerId, String token) {
	    String sql = "SELECT payerId, accountId, email, payerName, mobileNumber, payerTag, payerType, sendType, isDelete FROM allpaypass_new.INT_TB_PayerInfo " + 
	    	         "WHERE payerId = :payerId " +
	    		     "AND token = :token " +
	    	         "AND timeExpired > NOW()";
	    Query query = entityManager.createNativeQuery(sql).setParameter("payerId", payerId)
	    	                                              .setParameter("token", token);
		query.unwrap(SQLQuery.class).addScalar("payerId", StandardBasicTypes.STRING)
            .addScalar("accountId", StandardBasicTypes.STRING)
            .addScalar("email", StandardBasicTypes.STRING)
            .addScalar("payerName", StandardBasicTypes.STRING)
            .addScalar("mobileNumber", StandardBasicTypes.STRING)
            .addScalar("payerTag", StandardBasicTypes.STRING)
            .addScalar("payerType", StandardBasicTypes.STRING)
            .addScalar("sendType", StandardBasicTypes.STRING)
            .addScalar("isDelete", StandardBasicTypes.STRING)
            .setResultTransformer(Transformers.aliasToBean(PayerInfo.class));
        PayerInfo payerInfo = (PayerInfo) query.getSingleResult();
		return payerInfo;
	}

}
