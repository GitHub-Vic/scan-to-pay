package co.intella.repository.impl;

import co.intella.controller.QrcodePaymentController;
import co.intella.model.QQrcodeParameter;
import co.intella.model.QrType7;
import co.intella.model.QrType8;
import co.intella.model.QrcodeParameter;
import co.intella.repository.QrcodeParameterRepository;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class QrType8RepositoryImpl {
	@Autowired
	EntityManager entityManager;
	
	 @Autowired
	private QrcodeParameterRepository qrcodeParameterRepository;
	
	private final Logger LOGGER = LoggerFactory.getLogger(QrcodePaymentController.class);

	public QrType8 searchTheNewest(String shortId) {
		QrType8 qrType8 = null;
		String sql = "SELECT qrType8Sql, orderId, accountId, shortId, `name`, mobile, amount, remark, sysTime FROM allpaypass_new.INT_TB_QrType8 where shortId= :shortId and UNIX_TIMESTAMP(now()) - UNIX_TIMESTAMP(sysTime)<=300 order by sysTime desc limit 0,1";
		Query query = entityManager.createNativeQuery(sql).setParameter("shortId", shortId);
		query.unwrap(SQLQuery.class).setResultTransformer(Transformers.aliasToBean(QrType8.class));
		
		try {
			qrType8 = (QrType8) query.getSingleResult();
			
			LocalDateTime now = LocalDateTime.now();
	    	DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
	    	
	    	QQrcodeParameter qQrcodeParameter = QQrcodeParameter.qrcodeParameter;
    		BooleanExpression predicate = qQrcodeParameter.shortId.eq(shortId);
            QrcodeParameter qrcodeParameter = qrcodeParameterRepository.findOne(predicate);
	    	
            if(qrcodeParameter==null||Duration.between(LocalDateTime.parse(qrcodeParameter.getTimeExpire(), dtf2), now).toMinutes() >= 5) {
            	qrType8=null;
            }
		} catch (NoResultException e) {
			LOGGER.info("[QrType7][QrType7RepositoryImpl]  no data");
		} catch(Exception e) {
			LOGGER.error(e.getMessage());
		}
		return qrType8;

	}

}
