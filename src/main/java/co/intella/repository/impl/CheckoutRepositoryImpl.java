package co.intella.repository.impl;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import co.intella.domain.CheckoutCtrlVo;
import co.intella.domain.contratStore.DeviceVo;
import co.intella.model.INT_TB_CheckoutCtrl;
import co.intella.repository.CheckoutRepository;

@Repository
@Transactional
public class CheckoutRepositoryImpl implements CheckoutRepository {

	@Autowired
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<CheckoutCtrlVo> getCheckoutList(String accountId, String ownerDeviceId, String batchNo, String createDateStart, String createDateEnd, String bankId) {
		String sql =
				"select cc.accountId ," +
						"           cc.type ," +
						"             cc.batchNo ," +
						"             cc.comment ,  " +
						"             cc.ownerDeviceId , "+
						"             cc.method , " +
						"             'R' as status ,  " +
						"             'CC' as source,  " +
						"		      cc.ccSeqId ,  " +
						"             cc.crDate " +
						"      from INT_TB_CheckoutCtrl cc  " +
						"      where cc.status != 'COMP' and cc.status != 'ERR'  " +
						"      and (cc.accountId = :accountId or :accountId = '')  " +
						"      and (cc.ownerDeviceId = :ownerDeviceId or :ownerDeviceId = '')  " +
						"      and (cc.batchNo = :batchNo or :batchNo = '')  " +
						"	   and (cc.method = :method or :method = '')  " +
						"union all " +
						"    select aa.* , " +
						"     'I' as status ,  " +
						"           'TD' as source, " +
						"           0 as ccSeqId , " +
						"           sysdate() from ( " +
						"    select distinct a.* from  " +
						"    ( " +
						"    SELECT DISTINCT  " +
						"            t.accountId ,  " +
						"            d.type,        " +
						"            t.batchNo ,  " +
						"            t.comment ,  " +
						"            d.ownerDeviceId,"+
						"  			 t.method " +
						"    FROM TradeDetail t   " +
						"    INNER JOIN Device d on t.deviceRandomId=d.deviceRandomId   " +
						"    WHERE t.status = 'Trade success' and t.method = :method  " +
						"    and (accountId = :accountId or :accountId = '')  " +
						"    and (ownerDeviceId = :ownerDeviceId or :ownerDeviceId = '')  " +
						"    and (batchNo = :batchNo or :batchNo = '')  " +
						"    and (createDate > :createDateStart or :createDateStart = '' ) and (createDate < :createDateEnd or :createDateEnd = '' )   " +
						"    and t.batchNo IS NOT NULL " +
						"    union all  " +
						"    select DISTINCT  " +
						"            r.accountId ,  " +
						"            d.type,        " +
						"            r.batchNumber ,  " +
						"            concat(p.account,d.ownerDeviceId) comment ,  " +
						"            d.ownerDeviceId, " +
						"  			 r.method " +
						"    FROM ReserveDetail r   " +
						"    INNER JOIN Device d on r.deviceRandomId=d.deviceRandomId   " +
						"    inner join PaymentAccount p on d.owner = p.merchantSeqId and p.bankId = :bankId " +
						"    WHERE r.status = '0' and r.method = :method  " +
						"    and (r.accountId = :accountId or :accountId = '')  " +
						"    and (d.ownerDeviceId = :ownerDeviceId or :ownerDeviceId = '')  " +
						"    and (r.batchNumber = :batchNo or :batchNo = '')  " +
						"    and (createTime > :createDateStart or :createDateStart = '' ) and (createTime < :createDateEnd or :createDateEnd = '' )   " +
						"    and r.batchNumber IS NOT NULL  " +
						"    union all " +
						"    select DISTINCT  " +
						"            r.accountId ,  " +
						"            d.type,        " +
						"            r.batchNo ,  " +
						"            concat(p.account,d.ownerDeviceId) comment ,  " +
						"            d.ownerDeviceId, " +
						"            r.method " +
						"    FROM RefundDetail r   " +
						"    INNER JOIN Device d on r.deviceRandomId=d.deviceRandomId   " +
						"    inner join PaymentAccount p on d.owner = p.merchantSeqId and p.bankId = :bankId " +
						"    WHERE r.status = 'Refund success' and r.method = :method  " +
						"    and (r.accountId = :accountId or :accountId = '')  " +
						"    and (d.ownerDeviceId = :ownerDeviceId or :ownerDeviceId = '')  " +
						"    and (r.batchNo = :batchNo or :batchNo = '')  " +
						"    and (createTime > :createDateStart or :createDateStart = '' ) and (createTime < :createDateEnd or :createDateEnd = '' )   " +
						"    and r.batchNo IS NOT NULL  " +
						"    order by batchNo     " +
						"    ) a " +
						"    left join INT_TB_CheckoutCtrl b on a.accountId = b.accountId and a.batchNo = b.batchNo and a.comment = b.comment  " +
						"    where b.accountId is null ) aa;";
		Query query = entityManager.createNativeQuery(sql)
				.setParameter("accountId", accountId)
				.setParameter("ownerDeviceId", ownerDeviceId)
				.setParameter("batchNo", batchNo)
				.setParameter("createDateStart", createDateStart)
				.setParameter("createDateEnd", createDateEnd)
				.setParameter("method", "3" + bankId + "00")
				.setParameter("bankId", bankId);
		query.unwrap(SQLQuery.class).setResultTransformer(Transformers.aliasToBean(CheckoutCtrlVo.class));
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<INT_TB_CheckoutCtrl> getCheckoutList(String status) {
		String sql = "select ccSeqId, accountId, batchNo, type, comment, ownerDeviceId, status, crDate, dateStamp from INT_TB_CheckoutCtrl where status = :status";
		Query query = entityManager.createNativeQuery(sql)
				.setParameter("status", status);
		query.unwrap(SQLQuery.class).setResultTransformer(Transformers.aliasToBean(INT_TB_CheckoutCtrl.class));
		return query.getResultList();
	}

    public DeviceVo getDeviceByReserveId(String reserveOrderId) {
        String sql = "select d.* from ReserveDetail r " +
                "inner join Device d on r.deviceRandomId = d.deviceRandomId " +
                "where r.reserveOrderId = :reserveOrderId ";
        Query query = entityManager.createNativeQuery(sql)
                .setParameter("reserveOrderId", reserveOrderId);
        query.unwrap(SQLQuery.class).setResultTransformer(Transformers.aliasToBean(DeviceVo.class));
        return (DeviceVo) query.getSingleResult();
    }
    
    /**
     * count(dateStamp) where dateStamp between curdate() and now()
     * 
     * @see co.intella.repository.CheckoutRepository#countDateStamp()
     * @author mustang
     */
    public Integer countDateStamp() {
        String sql = "SELECT count(dateStamp) FROM INT_TB_CheckoutCtrl where dateStamp between curdate() and now()";
        Query query = entityManager.createNativeQuery(sql);
        BigInteger result = (BigInteger)query.getSingleResult();
        return Integer.valueOf(result.intValue());
    }
}
