package co.intella.repository.impl;


import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import co.intella.model.PaymentMethod;
import co.intella.repository.PaymentMethodRepository;

@Repository
@Transactional
public class PaymentMethodRepositoryImpl implements PaymentMethodRepository {

	@Autowired
	private EntityManager entityManager;

	@Override
	public PaymentMethod getOne(String method) {
		String sql = "select method,name,paymentAccountBankId from allpaypass_new.INT_TB_PaymentMethod where method = :method ";
        Query query = entityManager.createNativeQuery(sql)
                .setParameter("method", method);
        query.unwrap(SQLQuery.class).setResultTransformer(Transformers.aliasToBean(PaymentMethod.class));
        return (PaymentMethod) query.getSingleResult();
	}

}
