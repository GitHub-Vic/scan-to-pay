package co.intella.repository.impl;

import co.intella.model.IntTbLookupCodeNoFK;
import co.intella.model.TradeDetail;
import co.intella.repository.TradeDetailMultiRepository;
import co.intella.request.TradeDetailMultiRequest;
import com.jayway.jsonpath.internal.Utils;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Map;

@Repository
public class TradeDetailMultiRepositoryImpl implements TradeDetailMultiRepository {

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Map<String, Object>> list(TradeDetailMultiRequest request) {
        String sql = "select  " +
                "serviceType ServiceType, accountId AccountId , amount Amount , brandName BrandName , method Method , methodCode MethodCode , " +
                "itemName ItemName , StoreOrderNo , SysOrderNo , status Status , storeType StoreType , " +
                "TranDate , TranDayTime , UserId , name Name , l6.dscr LinePayMethod " +
                "from ( " +
                "select  " +
                "t.serviceType, " +
                "t.accountId,  " +
                "t.payment amount,  " +
                "m.storeName brandName,  " +
                "m.name, " +
                "pm.name method, pm.method methodCode , " +
                "t.description itemName, t.orderId StoreOrderNo, t.systemOrderId as SysOrderNo,  " +
                "l1.dscr status, " +
                "l4.dscr storeType,  " +
                "DATE_FORMAT(t.createDate,'%Y%m%d') TranDate, DATE_FORMAT(t.createDate,'%H%i%S') TranDayTime,  " +
                "CASE l3.lookupCode WHEN null THEN null ELSE t.userId END UserId, " +
                "t.createDate, " +
                "case when t.method = '11500' && JSON_VALID(t.txParams) = 1  then txParams ->>'$[0].method' ELSE null end txParams " +
                "from TradeDetail t " +
                "left join INT_TB_LookupCode l1 on l1.lookupType = 'tradeStatus' and l1.lookupCode = t.status " +
                "left join INT_TB_PaymentMethod pm on pm.method = t.method " +
                "left join INT_TB_LookupCode l3 on l3.lookupType = 'ticketType' and l3.type2 = t.method " +
                "inner join Merchant m on m.accountId = t.accountId " +
                "left join INT_TB_LookupCode l4 on l4.lookupType = 'storeType' and l4.lookupCode = m.storeType " +
                "where 1 = 1 " +
                (StringUtils.isEmpty(request.getStoreType()) ? "" : "AND m.storeType = :storeType ") +
                (StringUtils.isEmpty(request.getAccountId()) ? "" : "AND t.accountId = :accountId ") +
                (CollectionUtils.isEmpty(request.getMerchantSeqIds()) ? "" : "AND m.merchantSeqId in (:merchantSeqIds) ") +
                (StringUtils.isEmpty(request.getStartDate()) ? "" : "AND t.createDate > :startDate ") +
                (StringUtils.isEmpty(request.getEndDate()) ? "" : "AND t.createDate < :endDate ") +
                "AND t.status = 'Trade success' " +
                "union all  " +
                "select  " +
                "t.serviceType, " +
                "t.accountId,  " +
                "t.payment * -1 amount,  " +
                "m.storeName brandName, " +
                "m.name, " +
                "pm.name method,  pm.method methodCode , " +
                "t.description itemName, t.orderId StoreOrderNo, IFNULL(r1.systemRefundId, t.systemOrderId) as SysOrderNo,  " +
                "l5.dscr status, " +
                "l4.dscr storeType,  " +
                "DATE_FORMAT(r1.sysTime,'%Y%m%d') tranDate, DATE_FORMAT(r1.sysTime,'%H%i%S') tranDayTime,  " +
                "CASE l3.lookupCode WHEN null THEN null ELSE t.userId END userId, " +
                "DATE_FORMAT(r1.sysTime,'%Y%m%d%H%i%s') createDate, " +
                "case when t.method = '11500' && JSON_VALID(t.txParams) = 1  then txParams ->>'$[0].method' ELSE null end txParams " +
                "from ( " +
                "select orderId, max(sysTime) sysTime, max(systemRefundId) systemRefundId " +
                "from RefundDetail r " +
                "group by r.orderId ) r1 " +
                "inner join TradeDetail t on r1.orderId = t.orderId " +
                (StringUtils.isEmpty(request.getStartDate()) ? "" : "AND r1.sysTime > STR_TO_DATE(:startDate, '%Y%m%d%H%i%s') ") +
                (StringUtils.isEmpty(request.getEndDate()) ? "" : "AND r1.sysTime < STR_TO_DATE(:endDate, '%Y%m%d%H%i%s') ") +
                "left join INT_TB_LookupCode l1 on l1.lookupType = 'tradestatus' and l1.lookupCode = t.status " +
                "left join INT_TB_PaymentMethod pm on pm.method = t.method " +
                "left join INT_TB_LookupCode l3 on l3.lookupType = 'ticketType' and l3.type2 = t.method " +
                "inner join Merchant m on m.accountId = t.accountId " +
                "left join INT_TB_LookupCode l4 on l4.lookupType = 'storeType' and l4.lookupCode = m.storeType " +
                "left join INT_TB_LookupCode l5 on l5.lookupType = 'tradeStatus' and l5.lookupCode = t.refundStatus " +
                "where 1 = 1 " +
                (StringUtils.isEmpty(request.getStoreType()) ? "" : "AND m.storeType = :storeType ") +
                (StringUtils.isEmpty(request.getAccountId()) ? "" : "AND t.accountId = :accountId ") +
                (CollectionUtils.isEmpty(request.getMerchantSeqIds()) ? "" : "AND m.merchantSeqId in (:merchantSeqIds) ") +
                "AND t.refundStatus in ('Refund success', 'Cancel success') " +
                ") a  " +
                "left join INT_TB_LookupCode l6 on l6.lookupCode = a.txParams and l6.lookupType = 'linePayMethod' " +
                "order by a.createDate desc";
        Query query = entityManager.createNativeQuery(sql)
                .setParameter("startDate", request.getStartDate())
                .setParameter("endDate", request.getEndDate());
        if (!StringUtils.isEmpty(request.getStoreType())) {
            query.setParameter("storeType", request.getStoreType());
        }
        if (!StringUtils.isEmpty(request.getAccountId())) {
            query.setParameter("accountId", request.getAccountId());
        }
        if (!StringUtils.isEmpty(request.getMerchantSeqIds())) {
            query.setParameter("merchantSeqIds", request.getMerchantSeqIds());
        }
        query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        return query.getResultList();
    }

    @Override
    public List<Integer> getAccountIdsForListTradeDetail(TradeDetailMultiRequest request) {
        String sql = "select m1.merchantSeqId " +
                "from Merchant m1 " +
                "inner join Merchant m2 on m2.merchantSeqId = m1.parentMerchant and m2.accountId = :mchId " +
                "where m1.accountType = 1 " +
                "union all " +
                (StringUtils.isEmpty(request.getStoreType()) ?
                        "select merchantSeqId " +
                        "from Merchant " +
                        "where accountId = :mchId " :
                        "select merchantSeqId " +
                        "from Merchant m2 " +
                        "where m2.accountId = :mchId and storeType = :storeType "
                );
        Query query = entityManager.createNativeQuery(sql)
                .setParameter("mchId", request.getMchId());
        if (!StringUtils.isEmpty(request.getStoreType())) {
            query.setParameter("storeType", request.getStoreType());
        }
        return query.getResultList();
    }
}
