package co.intella.repository.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.springframework.stereotype.Repository;

import co.intella.repository.OrderIdByAccountIdRepository;


@Repository
public class OrderIdByAccountIdRepositoryImpl implements OrderIdByAccountIdRepository {
 @Resource
private EntityManager entityManager;
	@Override
	public Map<String, String> getGetOrderIdByAccountId(String accountId) {
		StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("INT_SP_GetOrderIdByAccountId");

		storedProcedure.registerStoredProcedureParameter("PI_accountId", String.class, ParameterMode.IN);
		storedProcedure.registerStoredProcedureParameter("PO_msgCode", String.class, ParameterMode.OUT);
		storedProcedure.registerStoredProcedureParameter("PO_msg", String.class, ParameterMode.OUT);
		storedProcedure.registerStoredProcedureParameter("PO_orderId", String.class, ParameterMode.OUT);
		storedProcedure.setParameter("PI_accountId", accountId);
		storedProcedure.execute();

		String msgCode = (String) storedProcedure.getOutputParameterValue("PO_msgCode");
		String msg = (String) storedProcedure.getOutputParameterValue("PO_msg");
		String orderId = (String) storedProcedure.getOutputParameterValue("PO_orderId");
		Map<String, String> result = new HashMap<String, String>();
		result.put("msgCode", msgCode);
		result.put("msg", msg);
		result.put("orderId", orderId);
		return result;
	}

}
