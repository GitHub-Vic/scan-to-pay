package co.intella.repository.impl;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.querydsl.core.types.dsl.BooleanExpression;

import co.intella.controller.QrcodePaymentController;
import co.intella.model.QQrcodeParameter;
import co.intella.model.QrType7;
import co.intella.model.QrcodeParameter;
import co.intella.repository.QrType7Repository;
import co.intella.repository.QrcodeParameterRepository;


public class QrType7RepositoryImpl {
	@Autowired
	EntityManager entityManager;
	
	 @Autowired
	private QrcodeParameterRepository qrcodeParameterRepository;
	
	private final Logger LOGGER = LoggerFactory.getLogger(QrcodePaymentController.class);

	public QrType7 searchTheNewest(String shortId) {
		QrType7 qrType7 = null;
		String sql = "SELECT qrType7Sql, orderId, accountId, shortId, licensePlate, mobile, amount, remark, sysTime FROM allpaypass_new.INT_TB_QrType7 where shortId= :shortId and UNIX_TIMESTAMP(now()) - UNIX_TIMESTAMP(sysTime)<=300 order by sysTime desc limit 0,1";
		Query query = entityManager.createNativeQuery(sql).setParameter("shortId", shortId);
		query.unwrap(SQLQuery.class).setResultTransformer(Transformers.aliasToBean(QrType7.class));
		
		try {
			qrType7 = (QrType7) query.getSingleResult();
			
			LocalDateTime now = LocalDateTime.now();
	    	DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
	    	
	    	QQrcodeParameter qQrcodeParameter = QQrcodeParameter.qrcodeParameter;
    		BooleanExpression predicate = qQrcodeParameter.shortId.eq(shortId);
            QrcodeParameter qrcodeParameter = qrcodeParameterRepository.findOne(predicate);
	    	
            if(qrcodeParameter==null||Duration.between(LocalDateTime.parse(qrcodeParameter.getTimeExpire(), dtf2), now).toMinutes() >= 5) {
            	qrType7=null;
            }
		} catch (NoResultException e) {
			LOGGER.info("[QrType7][QrType7RepositoryImpl]  no data");
		} catch(Exception e) {
			LOGGER.error(e.getMessage());
		}
		return qrType7;

	}

}
