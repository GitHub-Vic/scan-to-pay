package co.intella.repository.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.intella.model.IntTbLookupCodeNoFK;
import co.intella.repository.INT_TB_LookupCodeRepository;

/**
 * @author Mustang
 */
@Repository
public class INT_TB_LookupCodeRepositoryImpl implements INT_TB_LookupCodeRepository {
	@Autowired
    private EntityManager entityManager;

	@Override
	@SuppressWarnings("unchecked")
	public List<IntTbLookupCodeNoFK> findAllByLookType(String lookupType) {
//		List<IntTbLookupCode> list = new ArrayList<IntTbLookupCode>();
//		String sql = "SELECT lookupType, lookupCode, dscr, value, type1, type2, type3, crUser, crDate, userStamp, dateStamp FROM INT_TB_LookupCode where lookupType = :lookupType";
		String sql = "SELECT lookupType, lookupCode, dscr, type1, type2, type3, crUser, crDate, userStamp, dateStamp FROM INT_TB_LookupCode where lookupType = :lookupType";
		Query query = entityManager.createNativeQuery(sql)
				.setParameter("lookupType", lookupType);
		query.unwrap(SQLQuery.class).setResultTransformer(Transformers.aliasToBean(IntTbLookupCodeNoFK.class));
		List<IntTbLookupCodeNoFK> resultList = query.getResultList();
		
//		for (IntTbLookupCodeNoFK intTbLookupCodeNoFK : resultList) {
//			IntTbLookupType intTbLookupType = new IntTbLookupType();
//			intTbLookupType.setLookupType(intTbLookupCodeNoFK.getLookupType());
//			IntTbLookupCode lookupCode = new IntTbLookupCode();
//			lookupCode.setLookupType(intTbLookupType);
//			lookupCode.setLookupCode(intTbLookupCodeNoFK.getLookupCode());
//			lookupCode.setType1(intTbLookupCodeNoFK.getType1());
//			lookupCode.setType2(intTbLookupCodeNoFK.getType2());
//			lookupCode.setType3(intTbLookupCodeNoFK.getType3());
//			list.add(lookupCode);
//		}
		return resultList;
	}

	@Override
	public IntTbLookupCodeNoFK getByLookTypeAndLookupCode(String lookupType, String lookupCode) {
		String sql = "SELECT lookupType, lookupCode, dscr, type1, type2, type3, crUser, crDate, userStamp, dateStamp FROM INT_TB_LookupCode where lookupType = :lookupType and lookupCode = :lookupCode";
		Query query = entityManager.createNativeQuery(sql)
				.setParameter("lookupType", lookupType)
				.setParameter("lookupCode", lookupCode);
		query.unwrap(SQLQuery.class).setResultTransformer(Transformers.aliasToBean(IntTbLookupCodeNoFK.class));
		IntTbLookupCodeNoFK result = (IntTbLookupCodeNoFK) query.getSingleResult();
		return result;
	}
}
