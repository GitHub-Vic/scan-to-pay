package co.intella.repository;

import co.intella.model.MerchantResetToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * @author Alex
 */

public interface MerchantResetTokenRepository extends JpaRepository<MerchantResetToken, Long>, QueryDslPredicateExecutor<MerchantResetToken> {
}
