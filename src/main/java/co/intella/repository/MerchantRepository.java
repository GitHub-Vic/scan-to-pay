package co.intella.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import co.intella.model.Merchant;

/**
 * @author Miles Wu
 */
@Component
public interface MerchantRepository extends JpaRepository<Merchant, Long>, QueryDslPredicateExecutor<Merchant> {

//    @Query(nativeQuery = true,
//	  value = 
//	  		"select cc.accountId , " + 
//	  		"       cc.batchNo , " + 
//	  		"       cc.comment , " + 
//	  		"       cc.ownerDeviceId , " + 
//	  		"       'R' as status , " + 
//	  		"       'CC' as source, " + 
//	  		"		cc.ccSeqId ," +
//	  		"        cc.crDate " + 
//	  		"from allpaypass_new.INT_TB_CheckoutCtrl cc " + 
//	  		"where cc.status != 'COMP' and cc.status != 'ERR' " + 
//	  		"and (cc.accountId = :accountId or :accountId = '') " + 
//	  		"and (cc.ownerDeviceId = :ownerDeviceId or :ownerDeviceId = '')"+
//			"union all " + 
//			"select a.*, sysdate() as crDate from  " + 
//			"(SELECT DISTINCT " + 
//			"        t.accountId , " + 
//			"        t.batchNo , " + 
//			"        t.comment , " + 
//			"        d.ownerDeviceId , " + 
//			"        'I' as status ,  " + 
//			"        'TD' as source, " +
//			"        0 as ccSeqId " + 
//			"FROM TradeDetail t  " + 
//			"INNER JOIN Device d on d.type = 'taxi'  AND  " + 
//			"t.deviceRandomId=d.deviceRandomId  " + 
//			"WHERE t.status = 'Trade success' and t.method = '31800' " + 
//			"and (accountId = :accountId or :accountId = '') " + 
//			"and (ownerDeviceId = :ownerDeviceId or :ownerDeviceId = '') " + 
//			"AND t.batchNo IS NOT NULL order by batchNo) a " + 
//			"left join allpaypass_new.INT_TB_CheckoutCtrl b on a.accountId = b.accountId and a.batchNo = b.batchNo and a.comment = b.comment " + 
//			"where b.accountId is null ;"
//    		)
//    public List<Object[]> getTaxi(@Param(value = "accountId") String accountId , @Param(value = "ownerDeviceId") String ownerDeviceId);
}
