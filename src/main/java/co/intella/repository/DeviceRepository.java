package co.intella.repository;

import co.intella.model.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Component;

/**
 * @author Miles Wu
 */
@Component
public interface DeviceRepository extends JpaRepository<Device, Long>, QueryDslPredicateExecutor<Device> {
}
