package co.intella.repository;

import co.intella.model.QrType7;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

/**
 * @author Austin
 */
public interface QrType7Repository extends JpaRepository<QrType7, Long>, QueryDslPredicateExecutor<QrType7> {

//	@Query("SELECT qrType7Sql, orderId, accountId, shortId, licensePlate, mobile, amount, remark, sysTime FROM allpaypass_new.INT_TB_QrType7 where shortId= :shortId order by sysTime)")
//	List<QrType7> searchTheNewest(@Param(value = "shortId") String shortId);

}
