package co.intella.repository;

import java.util.List;

import co.intella.domain.CheckoutCtrlVo;
import co.intella.domain.contratStore.DeviceVo;
import co.intella.model.INT_TB_CheckoutCtrl;

public interface CheckoutRepository {

	List<CheckoutCtrlVo> getCheckoutList(String accountId , String ownerDeviceId, String batchNo, String createDateStart, String createDateEnd,String method);

	public List<INT_TB_CheckoutCtrl> getCheckoutList(String status);
	
	public DeviceVo getDeviceByReserveId(String reserveOrderId);
	
	public Integer countDateStamp();
}
