package co.intella.repository;

import java.math.BigDecimal;
import java.util.List;
import co.intella.model.MultiQrcode;

public interface MultiQrcodeRespository {
	public List<MultiQrcode> findMutiQRCodeByPayer(String shortId, String email, String payerName, String mobileNumber);
	public List<MultiQrcode> findMutiQRCodeByPayer(String shortId, String email, String payerName, String mobileNumber, String payerId);
	public List<MultiQrcode> findMutiQRCode(String shortId);
	public List<MultiQrcode> findMutiQRCode(String shortId, String payerId);
	public int updateOriginQrcode(String shortId, String mergeShortId);
	public String queryMergeShortId(String shortId);
	public int updateQrcode(String shortId);
	public BigDecimal queryQrcodeTotalFee(String shortId);
	public int updateCleanOriginQrcodeMergeShortId(String mergeShortId);
	public String checkPayerId(String shortId);
	public String checkOrderFinished(String shortId);
	public String checkSelfOrderFinished(String shortId);
	public MultiQrcode findOneByShortId(String shortId);
	public List<MultiQrcode> findListByMergeShortId(String mergeShortId);
}
