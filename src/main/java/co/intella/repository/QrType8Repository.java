package co.intella.repository;

import co.intella.model.QrType8;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * @author Austin
 */
public interface QrType8Repository extends JpaRepository<QrType8, Long>, QueryDslPredicateExecutor<QrType8> {

//	@Query("SELECT qrType7Sql, orderId, accountId, shortId, licensePlate, mobile, amount, remark, sysTime FROM allpaypass_new.INT_TB_QrType7 where shortId= :shortId order by sysTime)")
//	List<QrType7> searchTheNewest(@Param(value = "shortId") String shortId);

}
