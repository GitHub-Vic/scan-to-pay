package co.intella.exception;

/**
 * @author Miles
 */
public class DeviceNotFoundException extends Exception {

    public DeviceNotFoundException(String message) {
        super(message);
    }

}
