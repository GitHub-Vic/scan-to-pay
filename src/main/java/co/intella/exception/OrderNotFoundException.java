package co.intella.exception;

/**
 * @author Andy Lin
 */
public class OrderNotFoundException extends Exception {
    public OrderNotFoundException(String message) {
        super(message);
    }
}
