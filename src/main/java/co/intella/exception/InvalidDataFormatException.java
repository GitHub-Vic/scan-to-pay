package co.intella.exception;

/**
 * @author Andy Lin
 */
public class InvalidDataFormatException extends Exception{
    public InvalidDataFormatException(String message) {
        super(message);
    }
}
