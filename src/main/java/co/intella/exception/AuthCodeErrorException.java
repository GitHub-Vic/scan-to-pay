package co.intella.exception;

/**
 * @author Alex
 */

public class AuthCodeErrorException extends Exception {
    public AuthCodeErrorException(String message) {
        super(message);
    }

}
