package co.intella.exception;


public class OrderNoIsBlankException extends Exception {
    public OrderNoIsBlankException(String message) {
        super(message);
    }

}
