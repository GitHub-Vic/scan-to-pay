package co.intella.exception;

/**
 * @author Alex
 */

public class TradeKeyErrorException extends Exception {
    public TradeKeyErrorException(String message) {
        super(message);
    }

}
