package co.intella.exception;

/**
 * @author Miles
 */
public class RefundKeyException extends Exception {

    public RefundKeyException(String message) {
        super(message);
    }

}
