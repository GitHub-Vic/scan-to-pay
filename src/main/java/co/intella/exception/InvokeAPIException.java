package co.intella.exception;

public class InvokeAPIException extends Exception {

    public InvokeAPIException(String message) {
        super(message);
    }

    public InvokeAPIException(String methodCode, String className, String message) {
        super(methodCode + "  className : " + className + "，Error : " + message);
    }

    public InvokeAPIException(String className, String message) {
        super("className : " + className + "，Error : " + message);
    }
}
