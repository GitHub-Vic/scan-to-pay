package co.intella.exception;

/**
 * @author Alex
 */

public class OrderAlreadyRefundedException extends Exception{
    public OrderAlreadyRefundedException(String message) {
        super(message);
    }

}
