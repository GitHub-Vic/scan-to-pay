package co.intella.exception;

/**
 * @author Alex
 */

public class TimeExpireException extends Exception {
    public TimeExpireException(String message) {
        super(message);
    }

}
