package co.intella.exception;

public class AlreadyRetryException extends Exception {
    public AlreadyRetryException(String message) {
        super(message);
    }
}
