package co.intella.exception;

/**
 * @author Miles
 */
public class ServiceTypeNotSupportException extends Exception {

    public ServiceTypeNotSupportException(String message) {
        super(message);
    }

}
