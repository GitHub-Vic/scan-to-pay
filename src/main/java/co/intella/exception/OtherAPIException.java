package co.intella.exception;

public class OtherAPIException extends Exception {

    public OtherAPIException(String message) {
        super(message);
    }

}