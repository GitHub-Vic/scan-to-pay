package co.intella.exception;

public class TicketReadCardException extends Exception {
    public TicketReadCardException(String message) {
        super(message);
    }
}
