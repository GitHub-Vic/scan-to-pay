package co.intella.exception;

/**
 * @author Andy Lin
 */
public class OrderAlreadyExistedException extends Exception{

    public OrderAlreadyExistedException(String message) {
        super(message);
    }
}
