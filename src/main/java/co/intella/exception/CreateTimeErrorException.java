package co.intella.exception;

/**
 * @author Alex
 */

public class CreateTimeErrorException extends Exception {
    public CreateTimeErrorException(String message) {
        super(message);
    }
}
