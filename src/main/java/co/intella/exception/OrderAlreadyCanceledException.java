package co.intella.exception;

/**
 * @author Alex
 */

public class OrderAlreadyCanceledException extends Exception{
    public OrderAlreadyCanceledException(String message) {
        super(message);
    }

}
