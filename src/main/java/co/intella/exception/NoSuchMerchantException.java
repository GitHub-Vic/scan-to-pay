package co.intella.exception;

/**
 * @author Alex
 */

public class NoSuchMerchantException extends Exception {
    public NoSuchMerchantException(String message) {
        super(message);
    }

}
