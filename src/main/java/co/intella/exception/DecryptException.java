package co.intella.exception;

/**
 * @author Alex
 */

public class DecryptException extends Exception {
    public DecryptException(String message) {
        super(message);
    }
}
