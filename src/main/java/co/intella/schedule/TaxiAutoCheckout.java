package co.intella.schedule;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import co.intella.model.RequestHeader;
import co.intella.service.CheckoutService;

/**
 * @author Nick Lian
 */

@Component
public class TaxiAutoCheckout {
	
	private final Logger LOGGER = LoggerFactory.getLogger(TaxiAutoCheckout.class);
	
//	@Resource
//	private CheckoutService checkoutService;
	
//	@Scheduled(cron="0 0 23,1 * * *")
//	public void autoTaxiCheckout() {
//    	RequestHeader requestHeader = new RequestHeader();
//    	requestHeader.setMethod("31800");
//		checkoutService.checkout("","" , requestHeader, null);
//	}

}
