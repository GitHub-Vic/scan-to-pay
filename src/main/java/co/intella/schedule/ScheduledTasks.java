package co.intella.schedule;

import co.intella.domain.pi.PiReconciliationRequest;
import co.intella.exception.InvokeAPIException;
import co.intella.model.PaymentAccount;
import co.intella.service.*;
import co.intella.utility.SystemInstance;
import com.ibm.icu.util.Calendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author Andy Lin
 */
@Component
public class ScheduledTasks {

    private final Logger LOGGER = LoggerFactory.getLogger(ScheduledTasks.class);

    @Resource
    private CaptureService captureService;

    @Resource
    private PiService piService;

    @Resource
    private PaymentAccountService paymentAccountService;

    @Resource
    private QueryJobService queryJobService;

    @Resource
    private ICashService iCashService;

    @Autowired
    private Environment env;

    @Value("${execution.mode}")
    private String EXECUTION_MODE;

    @Scheduled(cron = "0 0 2 * * *")
    public void piReconciliate() {

        try {
            String endDate = convertDateFormat(getNowTime(), 8);
            String startDate = convertDateFormat(dateCalculate(-1, endDate), 8);
            LOGGER.info("[Pi] Start Reconciliating Date " + startDate);

            List<PaymentAccount> list = paymentAccountService.listByBankId(Long.parseLong(env.getProperty("bank.id.60300")));

            for (PaymentAccount paymentAccount : list) {
                LOGGER.info("[Reconciliation] payment account :" + paymentAccount.getAccount() + ", merchant :" + paymentAccount.getMerchant().getAccountId());
                // todo: Warning!  partner-key is fixed now, since PaymentAccount doesn't include the column of mchKey(or partnerKey)
                PiReconciliationRequest piReconciliationRequest = new PiReconciliationRequest();
                piReconciliationRequest.setPartnerId(paymentAccount.getAccount());
                piReconciliationRequest.setPartnerKey(env.getProperty("merchant.key.mwd"));
                piReconciliationRequest.setDate(startDate);

                String result = piService.reconciliation(piReconciliationRequest.getPartnerId(), piReconciliationRequest.getPartnerKey(), piReconciliationRequest.getDate());
                LOGGER.info("[Reconciliation] Done! payment account :" + paymentAccount.getAccount() + ", merchant :" + paymentAccount.getMerchant().getAccountId());
            }
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION][" + EXECUTION_MODE + "][Schedule][Pi][reconcile] " + e.getMessage() + ", \n" + Arrays.toString(e.getStackTrace()).replaceAll(",", ",\n"));
        }
    }

    @Scheduled(cron = "0 0 2 * * *")
    public String weixinCapture() throws ParseException, IOException {
        LOGGER.info("[WeiXin] Start capture...");
        String mchId = "super";
        String to = convertDateFormat(getNowTime(), 8);
        String from = convertDateFormat(dateCalculate(-1, to), 8);

        return captureService.weixinCapture(mchId, from, to);
    }

    @Scheduled(fixedDelay = 5000)
    public void doJobs() {
        queryJobService.doJobs();
    }

    @Scheduled(cron = "0 0 2 * * *")
    public String allpayCapture() throws ParseException, IOException {
        LOGGER.info("[AllPay] Start capture...");
        //todo: get Merchants corresponding to ALLPAY from DB
        String mchId = "super";

        String endDate = convertDateFormat(getNowTime(), 8);
        String startDate = convertDateFormat(dateCalculate(-1, endDate), 8);
        return captureService.allpayCapture(mchId, startDate, endDate);
    }

    @Scheduled(cron = "0 0 2 * * *")
    public String alipayCapture() throws ParseException, IOException {
        LOGGER.info("[AliPay] Start capture...");
        //todo: get Merchants corresponding to ALIPAY from DB
        String mchId = "super";

        String endDate = convertDateFormat(getNowTime(), 8);
        String startDate = convertDateFormat(dateCalculate(-1, endDate), 8);
        return captureService.alipayCapture(mchId, startDate, endDate);
    }

    @Scheduled(cron = "0 0 23 * * *")
    public String tsbCapture() throws ParseException, IOException {
        LOGGER.info("[TsBank] Start capture...");
        //todo: get Merchants corresponding to TsBank from DB
        String mchId = "super";

        String endDate = convertDateFormat(getNowTime(), 8);
        String startDate = convertDateFormat(dateCalculate(-1, endDate), 8);
        return captureService.tsbCapture(mchId, startDate, endDate);
    }

    private String convertDateFormat(String dateTime, int to) {
        String pattern1 = dateFormatParameter(dateTime.length());
        String pattern2 = dateFormatParameter(to);

        SimpleDateFormat sdf1 = new SimpleDateFormat(pattern1);
        SimpleDateFormat sdf2 = new SimpleDateFormat(pattern2);
        String str = "";
        try {
            Date d = sdf1.parse(dateTime);
            str = sdf2.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private String dateCalculate(int days, String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date dt = sdf.parse(date);
        Calendar cd = Calendar.getInstance();
        cd.setTime(dt);
        cd.add(Calendar.DAY_OF_YEAR, days);
        Date dt1 = cd.getTime();
        String str = sdf.format(dt1);

        return str;
    }

    private String dateFormatParameter(int ref) {
        String pattern = "";
        if (ref == 14) {
            pattern = SystemInstance.DATE_PATTERN;
        } else if (ref == 12) {
            pattern = "yyyyMMddhhmm";
        } else if (ref == 8) {
            pattern = "yyyyMMdd";
        } else if (ref == 6) {
            pattern = "yyMMdd";
        }
        return pattern;
    }

    private String getNowTime() {
        SimpleDateFormat sdf1 = new SimpleDateFormat(SystemInstance.DATE_PATTERN);
        String str = "";
        str = sdf1.format(new Date());
        return str;
    }
}
