package co.intella.schedule;

import co.intella.crypto.AesCryptoUtil;
import co.intella.crypto.KeyReader;
import co.intella.domain.contratStore.CustomizePageRequest;
import co.intella.model.IntegratedRequest;
import co.intella.model.Merchant;
import co.intella.model.TradeDetail;
import co.intella.net.Constant;
import co.intella.net.HttpRequestUtil;
import co.intella.service.MerchantService;
import co.intella.service.TradeDetailService;
import co.intella.utility.SystemInstance;
import com.google.gson.Gson;
import org.assertj.core.util.Strings;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.crypto.SecretKey;
import java.io.File;
import java.security.PublicKey;
import java.util.*;

/**
 * @author Miles
 */
@Component
public class AutoRefundSchedule {

    private final Logger LOGGER = LoggerFactory.getLogger(AutoRefundSchedule.class);

    @Value("${execution.mode}")
    private String EXECUTION_MODE;

    @Value("${host.scan2pay.api}")
    private String HOST_API;

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private MerchantService merchantService;

    @Scheduled(cron="0 0 1 * * *")
    public void autoRefund() {

        String startDate = DateTime.now().minusDays(1).toString("yyyyMMdd") + "000000";
        String endDate = DateTime.now().minusDays(1).toString("yyyyMMdd") + "235959";

        doRefund(startDate, endDate);
    }

    public void iPassNonTransactionRequest(String serviceType){

        Map<String, String> requestData = new HashMap<String, String>();

        requestData.put("Method", "32100");
        requestData.put("ServiceType", serviceType);
        requestData.put("MchId", "acertest004");
        requestData.put("TradeKey", "9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");
        requestData.put("CreateTime", "20190319235900");


        requestData.put("DeviceId", "01304162");
        requestData.put("Retry", "0");

    }

    public void doRefund(String startDate, String endDate) {
        LOGGER.info("[Scheduled][AUTO_REFUND] " + startDate + " ~ " + endDate);

        try {
            List<Merchant> merchantAccounts = merchantService.listByHierarchy("00000");
            List<String> ids = new ArrayList<String>();

            for (Merchant merchant : merchantAccounts) {
                ids.add(merchant.getAccountId());
            }

            CustomizePageRequest request = new CustomizePageRequest();
            request.setMerchantsIdList(ids);
            request.setStartDate(startDate);
            request.setEndDate(endDate);

            List<TradeDetail> list = tradeDetailService.listByMerchants(request);

            for(TradeDetail tradeDetail : list) {
                boolean isNotRefund = Strings.isNullOrEmpty(tradeDetail.getRefundStatus()) || !tradeDetail.getRefundStatus().equals(SystemInstance.TRADE_REFUND_SUCCESS);
                LOGGER.info("\t" + tradeDetail.getOrderId() + " -> " + tradeDetail.getRefundStatus());
                if(tradeDetail.getStatus().equals(SystemInstance.TRADE_SUCCESS) && isNotRefund) {
                    LOGGER.info("REFUND : " + tradeDetail.getMethod() + " -> " + tradeDetail.getOrderId());
                    doRefund(tradeDetail);
                }
            }
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
        }

        LOGGER.info("[Scheduled][AUTO_REFUND] END");
    }

    private void doRefund(TradeDetail tradeDetail) throws Exception {

        if(tradeDetail.getMethod().equals("31800")) {
            return;
        }

        String serviceType = "Refund";

        if(tradeDetail.getMethod().equals("20840") || tradeDetail.getMethod().equals("20800") || tradeDetail.getMethod().equals("21100")) {
            serviceType = "Cancel";
        }

        LOGGER.info("do" + serviceType + " -> " + tradeDetail.getMethod() + " -> " + tradeDetail.getOrderId());
        Map<String, String> requestData = new HashMap<String, String>();
        // setting header
        requestData.put("MchId", "super");
        requestData.put("Method", tradeDetail.getMethod());
        requestData.put("ServiceType", serviceType);
        requestData.put("TradeKey", "9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");
        requestData.put("StoreOrderNo", tradeDetail.getOrderId());
        requestData.put("StoreRefundNo", "R" + tradeDetail.getOrderId());
        requestData.put("CreateTime", DateTime.now().toString(SystemInstance.DATE_PATTERN));
        requestData.put("DeviceInfo", "T0000000");
        requestData.put("Body", "AUTO_REFUND");
        requestData.put("TotalFee", String.valueOf(tradeDetail.getPayment()));
        requestData.put("FeeType", SystemInstance.CURRENCY_TWD);
        requestData.put("Detail", "AUTO_REFUND");
        requestData.put("StoreInfo", "AUTO_REFUND");
        requestData.put("Cashier", "SYSTEM");
        requestData.put("BarcodeMode", "barcodeMode");
        requestData.put("Extended", "");
        requestData.put("RefundKey", "9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0");

        SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
        IntegratedRequest encryptRequest = getEncryptRequest(requestData, secretKey);

        String result = HttpRequestUtil.post(HOST_API, new Gson().toJson(encryptRequest));
        String decryptResponse = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, result);
        LOGGER.info("doRefund : " + decryptResponse);
    }

    public IntegratedRequest getEncryptRequest(Map<String, String> requestData, SecretKey secretKey) throws Exception {

        String keyPath = EXECUTION_MODE.equals("stage") ? "stage-pub.der" : "pub.der";

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("key/" + keyPath).getFile());
        PublicKey publicKey = KeyReader.loadPublicKeyFromDER(file);

        String requestString = HttpRequestUtil.getEncryptRequestJson(requestData, publicKey, secretKey);
        return new Gson().fromJson(requestString, IntegratedRequest.class);
    }
}
