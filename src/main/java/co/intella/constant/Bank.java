package co.intella.constant;

public enum Bank {

	SKB(1),
	YUANTA(2),
	PI(3),
	TCBANK(4),
	ALLPAY(5),
	SP(6),
	PIINAPP(7),
	TSBANK(8),
	JKOS(9),
	ECPAY(10),
	YUANTA_OFFLINE(11),
	APPLE_PAY(12),
	GAMAPAY(13),
	AIPEI(14),
	LINEPAY(15),
	LINEPAY_OFFLINE(16),
	AIPEI_OFFLINE(17),
	EZC(18),
	ALLPAY_OFFLINE(19),
	TWPAY(20),
	IPASS(21),
	GOOGLE(22),
	FRIDAY(23),
	PAY2GO(24),
	ALIPAY_SHANG_HAI(25),
	SCSB(26),
	CTBC_WEIXIN(27),
	ICASH(28),
	SCSB_ALIPAY(29),
	LUCKY_PAY(30),
	DG_PAY(31),
	FUBON(32),
	TOBTWPAY(42),
	FBTWPAY(43),
	LBTWPAY(44),
	HNTWPAY(45),
	TCBTWPAY(50),
	NEW_PI(59),
	MEGATWPAY2(56);

	
    private final int id;

    private Bank(int bankId) {
        this.id = bankId;
    }

    public int getId() {
        return id;
    }

}
