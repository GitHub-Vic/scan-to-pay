package co.intella.constant;

import co.intella.utility.SystemInstance;

public enum TicketPayment {
    EZCARD_EZC("Payment", "31800", "ezc", "SmallPayments"),
    EZCARD_TAXI(SystemInstance.ServiceType.TICKET_LOGIC, "31800", "taxi", "Taxi"),
    EZCARD_EDC("EZCardEDCAPayment", "31800", "traffic", "Parking"),

    IPASS_EZC("Payment", "32100", "ezc", "SmallPayments"),
    IPASS_TAXI(SystemInstance.ServiceType.TICKET_LOGIC, "32100", "taxi", "Taxi"),
    IPASS_EDC("Payment", "32100", "traffic", "SmallPayments"),

    ICASH_EZC("Payment", "32800", "ezc", "SmallPayments"),
    ICASH_TAXI("Payment", "32800", "taxi", "SmallPayments"),
    ICASH_EDC("Payment", "32800", "traffic", "SmallPayments");

    private String serviceType;
    private String method;
    private String deviceType;
    private String serviceTxType;

    TicketPayment(String serviceType, String method, String deviceType, String serviceTxType) {
        this.serviceType = serviceType;
        this.method = method;
        this.deviceType = deviceType;
        this.serviceTxType = serviceTxType;
    }

    public static TicketPayment getTicketPayment(String method, String deviceType) {
        TicketPayment ticketPayment = null;
        for (TicketPayment t : TicketPayment.values()) {
            if (t.getMethod().equals(method) && t.getDeviceType().equals(deviceType))
                ticketPayment = t;
        }
        return ticketPayment;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getServiceTxType() {
        return serviceTxType;
    }

    public void setServiceTxType(String serviceTxType) {
        this.serviceTxType = serviceTxType;
    }
}
