package co.intella.constant;

public enum BankNameEnum {// db的bankName轉成PaymentMethodController的方法
	twpay("twPay"), TOBtwpay("twPay"), FBtwpay("twPay"), LBtwpay("twPay"), YuanTaOffline("yuanTaOffline"),
	GamaPay("gamaPay"), AlipayShangHai("alipayShangHai"), UUPay("uuPay"), ezcard("ezcard"), LinePay("linePay"),
	scsb3D("getScsb3D"), scsb("getScsb"), FubonBank("funbonBank"), TSBank("tsBank"), EC("ecPay"),TOBwebATM("tOBwebATM"),IcashPay("icashPay");


	private String bankName;

	BankNameEnum(String bankName) {//
		this.bankName = bankName;
	}

	public String getBankName() {
		return bankName;
	}
}