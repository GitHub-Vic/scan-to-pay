package co.intella.constant;

import java.util.Arrays;
import java.util.List;

public class Payment {

    public static final String CREDIT_CARD = "creditcard";

    public static final List<Long> CREDIT_CARD_BANKID = Arrays.asList(8L, 4L, 6L, 26L, 39L, 11L, 38L);

    public static final List<Long> TWPAY_BANKID = Arrays.asList(42L, 43L, 20L, 44L, 50L, 56L);
}
