package co.intella.constant;

public interface OrderStatus {
	
	public static final String PROCESSING = "0";
	public static final String SUCCESS = "1";
	public static final String FAIL = "2";
	public static final String REFUND = "3";


}
