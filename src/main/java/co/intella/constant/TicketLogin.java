package co.intella.constant;

public enum TicketLogin {


    IPASS_TAXI("TaxiLogin", "21", "32100", "taxi"),
    IPASS_EZC("TermAuth", "21", "32100", "ezc"),
    IPASS_EZD("TermAuth", "21", "32100", "traffic"),

    EZCard_TAXI("TaxiLogin", "18", "31800", "taxi"),
    EZCard_EZC("SignOn", "18", "31800", "ezc"),
    EZCard_EZD("EDCASignOn", "18", "31800", "traffic"),

    ICASH_EZC("TermAuth", "28", "32800", "ezc"),
    ICASH_TAXI("TermAuth", "28", "32800", "taxi"),
    ICASH_EZD("TermAuth", "28", "32800", "traffic");


    private String serivceType;
    private String bankId;
    private String methodId;
    private String deviceType;

    TicketLogin(String serivceType, String bankId, String methodId, String deviceType) {

        this.serivceType = serivceType;
        this.bankId = bankId;
        this.methodId = methodId;
        this.deviceType = deviceType;
    }

    public static TicketLogin getTicketLogin(String bankId, String deviceType) {
        TicketLogin taxiLogin = null;
        for (TicketLogin t : TicketLogin.values()) {
            if (t.getBankId().equals(bankId) && t.getDeviceType().equals(deviceType))
                taxiLogin = t;
        }
        return taxiLogin;
    }

    public static TicketLogin getTicketLogin(String serivceType) {
        TicketLogin taxiLogin = null;
        for (TicketLogin t : TicketLogin.values()) {
            if (t.getSerivceType().equals(serivceType))
                taxiLogin = t;
        }
        return taxiLogin;
    }

    public String getMethodId() {
        return methodId;
    }

    public void setMethodId(String methodId) {
        this.methodId = methodId;
    }

    public String getSerivceType() {
        return serivceType;
    }

    public void setSerivceType(String serivceType) {
        this.serivceType = serivceType;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    @Override
    public String toString() {
        return this.serivceType + "_" + this.bankId + "_" + methodId;
    }

}
