package co.intella.constant;

public enum MethodToBankCode {

    TCB_BANK("15000", "006"),    //合庫
    MEGA_BANK("15200", "017"),    //兆豐
    SCSB_BANK("12000", "011")    //上海

    ;


    private String method;
    private String bankCode;

    MethodToBankCode(String method, String bankCode) {
        this.method = method;
        this.bankCode = bankCode;
    }

    /**
     * 將method 代表的支付收單銀行轉換為原本收單銀行的代碼，請自行新增對應資料
     *
     * @param method
     * @return
     */
    public static MethodToBankCode findMethodToBankCode(String method) {
        MethodToBankCode methodToBankCode = null;
        for (MethodToBankCode t : MethodToBankCode.values()) {
            //TODO  黃的明顯  這邊用的是 contains     這邊用的是 contains     這邊用的是 contains   ，為了怕有多個METHOD 對一家銀行！！！
            if (t.getMethod().contains(method)) {
                methodToBankCode = t;
            }
        }
        return methodToBankCode;
    }


    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Override
    public String toString() {
        return "MethodToBankCode{" +
                "method='" + method + '\'' +
                ", bankCode='" + bankCode + '\'' +
                '}';
    }
}
