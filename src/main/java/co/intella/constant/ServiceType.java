package co.intella.constant;

public interface ServiceType {

	public static final String OLPAY = "OLPay";
	public static final String MICROPAY = "Micropay";
	public static final String PAYMENT = "Payment";
	public static final String REFUND = "Refund";
	public static final String CANCEL = "Cancel";
	public static final String SINGLE_ORDER_QUERY = "SingleOrderQuery";
	public static final String ORDER_QUERY = "OrderQuery";
	public static final String INVOICE_QUERY = "InvoiceQuery";

}
