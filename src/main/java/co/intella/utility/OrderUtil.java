package co.intella.utility;


import co.intella.constant.Payment;
import co.intella.model.Merchant;
import co.intella.model.PaymentAccount;
import co.intella.model.QrcodeParameter;
import co.intella.model.TradeDetail;
import co.intella.service.DeviceService;
import co.intella.service.OrderService;
import co.intella.service.TradeDetailService;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

@Component
public class OrderUtil {

    @Resource
    private TradeDetailService tradeDetailService;

    @Resource
    private OrderService orderService;

    @Resource
    private DeviceService deviceService;

    public TradeDetail setOnlineTradeDetailRequest(String amount, Merchant merchant,
                                                   PaymentAccount paymentAccount, QrcodeParameter qrcodeParameter, String method) {
        Map<String, String> resultMap = orderService.createOrderIdByMchId(String.valueOf(merchant.getMerchantSeqId()));
        String systemOrderId = resultMap.get("orderNo");
        if (StringUtils.isBlank(systemOrderId) || Payment.TWPAY_BANKID.contains(paymentAccount.getBank().getBankId())) {
            systemOrderId = orderService.createOrderIdBySequence(qrcodeParameter.getMchId());
        } else {
            systemOrderId += StringUtils.defaultIfBlank(qrcodeParameter.getStoreInfo(), "");
        }
        String storeOrderNo = "1".equals(qrcodeParameter.getCodeType()) ?
                qrcodeParameter.getStoreOrderNo() : systemOrderId;

        TradeDetail tradeDetail = tradeDetailService.getOne(merchant.getAccountId(), storeOrderNo);
        if (tradeDetail != null) {
            return tradeDetail;
        } else {
            tradeDetail = new TradeDetail();
        }
        tradeDetail.setAccountId(merchant.getAccountId());
        tradeDetail.setMethod(method);
        tradeDetail.setPaymentAccount(paymentAccount);
        tradeDetail.setCreateDate(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        tradeDetail.setStatus("Creating order");

        tradeDetail.setOrderId(storeOrderNo);
        tradeDetail.setSystemOrderId(systemOrderId);
        tradeDetail.setDeviceRandomId(deviceService.getOne(merchant, "APP-001"));
        tradeDetail.setPayment(Long.valueOf(amount));
        tradeDetail.setServiceType("OLPay");
        tradeDetail.setQrcodeToken(qrcodeParameter.getShortId());
        tradeDetail.setDescription(qrcodeParameter.getBody());
        tradeDetail.setDetail(qrcodeParameter.getDetail());
        tradeDetail.setOnSale(false);
        tradeDetail.setOriginalPrice(qrcodeParameter.getTotalFee());

        tradeDetailService.save(tradeDetail);

        return tradeDetailService.getOne(merchant.getAccountId(), storeOrderNo);
    }
}
