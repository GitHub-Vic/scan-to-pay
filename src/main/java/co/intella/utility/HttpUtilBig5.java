package co.intella.utility;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

import static co.intella.net.Constant.*;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.net.ssl.SSLContext;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;


public class HttpUtilBig5 {

    private static final int TIME_OUT = 100 * 1000;

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpUtilBig5.class);



    /**
     * @param url
     * @return
     * @throws Exception
     */
    public static String doGet(String url) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        String result = "";
        try {
            LOGGER.info("doGet :" + url);
            HttpGet httpget = new HttpGet(url.trim());
            httpget.setConfig(HttpUtilBig5.getRequestConfig());

            response = httpclient.execute(httpget);
//解析返结果
            result = getResponseStr(response);
//            LOGGER.info("doGet result:" + result);
        } catch (IOException e) {
            LOGGER.info(e.getMessage());
        } finally {
            close(httpclient, response);
        }
        return result;
    }

    public static String post(String url, String data) throws Exception {
        HttpURLConnection conn = (HttpURLConnection) new URL(url.trim())
                .openConnection();

        conn.setRequestMethod(POST);
        conn.setRequestProperty(CONTENT_TYPE, APPLICATION_JSON);

        conn.setDoOutput(true);
        conn.setDoInput(true);

        conn.getOutputStream().write(data.getBytes("Big5"));

        conn.getOutputStream().flush();
        conn.getOutputStream().close();
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream(), "Big5"));
        String line;
        StringBuilder text = new StringBuilder();
        while ((line = rd.readLine()) != null) {
            text.append(line);
            if (line.length() < 1 && line.charAt(0) == '\uFEFF') {
                text.deleteCharAt(0);
            }
        }

        return text.toString();
    }

    /**
     * 用form表單送出
     *
     * @param url
     * @param paramMap
     * @return
     */
    public static String doPost(String url, Map<String, ?> paramMap) throws Exception {

//        System.setProperty("jsse.enableSNIExtension", "false");//忽略憑證
//        CloseableHttpClient httpclient = buildSSLCloseableHttpClient();//忽略憑證

        CloseableHttpClient httpclient = HttpClients.createDefault();

        CloseableHttpResponse response = null;
        String result = "";
        try {
            LOGGER.info("doPost " + url);
            HttpPost httpPost = new HttpPost(url.trim());
            httpPost.setConfig(HttpUtilBig5.getRequestConfig());

            httpPost.addHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded;charset=Big5");

            //参数
            List<NameValuePair> formparams = paramMap.entrySet().stream()
                    .map(es -> new BasicNameValuePair(es.getKey(), (Objects.nonNull(es.getValue()) ? es.getValue().toString() : "null")))
                    .collect(Collectors.toList());
            UrlEncodedFormEntity uefEntity = new UrlEncodedFormEntity(formparams, "Big5");
            httpPost.setEntity(uefEntity);
            response = httpclient.execute(httpPost);
            result = getResponseStr(response);

        } catch (UnsupportedEncodingException e) {
            LOGGER.info(e.getMessage());
        } catch (IOException e) {
            LOGGER.info(e.getMessage());
        } finally {
            close(httpclient, response);
        }

        return result;
    }

    /**
     * 用Json送出
     *
     * @param url
     * @param jsonStr
     * @return
     */
    public static String doPostJson(String url, String jsonStr) throws Exception {

        Map<String, String> header = new HashMap<>();
        header.put(HTTP.CONTENT_TYPE, "application/json;charset=Big5");
        return doPost(header, url, jsonStr);
    }

    /**
     * 用Json送出
     *
     * @param url
     * @param jsonStr
     * @return
     */
    public static String doPostJson(@Nonnull Map<String, String> header, String url, String jsonStr) throws Exception {
        if (!header.containsKey((HTTP.CONTENT_TYPE))) {
            header.put(HTTP.CONTENT_TYPE, "application/json;charset=Big5");
        }
        return doPost(header, url, jsonStr);
    }

    /**
     * 用Xml送出
     *
     * @param url
     * @param jsonStr
     * @return
     */
    public static String doPostXml(String url, String jsonStr) throws Exception {
        Map<String, String> header = new HashMap<>();
        header.put(HTTP.CONTENT_TYPE, "application/xml;charset=Big5");
        return doPost(header, url, jsonStr);
    }

    /**
     * 用Xml送出
     *
     * @param url
     * @param jsonStr
     * @return
     */
    public static String doPostXml(@Nonnull Map<String, String> header, String url, String jsonStr) throws Exception {
        if (!header.containsKey((HTTP.CONTENT_TYPE))) {
            header.put(HTTP.CONTENT_TYPE, "application/xml;charset=Big5");
        }
        return doPost(header, url, jsonStr);
    }

    /**
     * @param header
     * @param url
     * @param str
     * @return
     */
    public static String doPost(@Nonnull Map<String, String> header, String url, String str) throws Exception {



//       System.setProperty("jsse.enableSNIExtension", "false");//忽略憑證
//        CloseableHttpClient httpclient = buildSSLCloseableHttpClient();//忽略憑證

        CloseableHttpClient httpclient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        String result = "";
        try {

            LOGGER.info("doPost :" + url);
            LOGGER.info("doPost data  :" + str);
            HttpPost httpPost = new HttpPost(url.trim());
            httpPost.setConfig(HttpUtilBig5.getRequestConfig());

            for (Map.Entry<String, String> entry : header.entrySet()) {
                httpPost.addHeader(entry.getKey(), entry.getValue());
                LOGGER.info("addHeader ==>  " + entry.getKey() + ":" + entry.getValue());
            }

            StringEntity se = new StringEntity(str);
            se.setContentEncoding("Big5");
            se.setContentType(StringUtils.defaultIfEmpty(header.get(HTTP.CONTENT_TYPE), "text/html"));

            httpPost.setEntity(se);
            response = httpclient.execute(httpPost);
            result = getResponseStr(response);

        } catch (UnsupportedEncodingException e) {
            LOGGER.info(e.getMessage());
        } catch (IOException e) {
            LOGGER.info(e.getMessage());
        } finally {
            close(httpclient, response);
        }
        return result;
    }

    private static String getResponseStr(CloseableHttpResponse response) throws IOException {
        String result = "";
        if (Objects.nonNull(response)) {
            HttpEntity resEntity = response.getEntity();
            if (Objects.nonNull(resEntity)) {
                result = EntityUtils.toString(resEntity, "UTF-8");
            }
        }
        LOGGER.info("[getResponseStr]  Response : " + result);
        return result;
    }

    private static void close(CloseableHttpClient httpclient, CloseableHttpResponse response) {
        try {
            if (Objects.nonNull(httpclient))
                httpclient.close();
            if (Objects.nonNull(response))
                response.close();
        } catch (IOException e) {
            LOGGER.info(e.getMessage());
        }
    }

    /**
     * 取得請求TIME_OUT 設定
     *
     * @return
     */
    private static RequestConfig getRequestConfig() {
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(TIME_OUT).setConnectionRequestTimeout(TIME_OUT)
                .setSocketTimeout(TIME_OUT).build();
        return requestConfig;
    }


    /**
     * 寫一個form表單給客戶端跳轉
     *
     * @param response
     * @param url
     * @param paramMap
     */
    public static void sendForm(HttpServletResponse response, String url, Map<String, ? > paramMap) {
        PrintWriter out = null;
        try {
            StringBuffer sb = new StringBuffer();
            sb.append("<!DOCTYPE html><html><form name='form' action='" + url + "' method='POST' accept-charset='Big5' >");
            String param = paramMap.entrySet().stream().filter(es -> Objects.nonNull(es.getValue())).map(es ->
                    "<input type=\"hidden\" name=\"" + es.getKey() + "\" value=\"" + es.getValue().toString() + "\">"
            ).collect(Collectors.joining(""));
            sb.append(param).append("</form></html>").append("<script>document.forms[0].submit();</script>");

            out = response.getWriter();
            response.setContentType("text/html;charset=Big5");
            String formStr = sb.toString();
            LOGGER.info("send form =>" + formStr);
            out.println(formStr);
            out.flush();
        } catch (IOException e) {
            LOGGER.info(e.getMessage());
        } finally {
            if (Objects.nonNull(out)) {
                out.close();
            }
        }

    }




    /**
     * ============忽略证书
     */
    private static CloseableHttpClient buildSSLCloseableHttpClient()
            throws Exception {
        // 信任所有
        SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null,
                (chain, authType) -> true).build();
        // ALLOW_ALL_HOSTNAME_VERIFIER:这个主机名验证器基本上是关闭主机名验证的,实现的是一个空操作，并且不会抛出javax.net.ssl.SSLException异常。
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                sslContext, new String[] { "TLSv1.2" }, null,
                SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        return HttpClients.custom().setSSLSocketFactory(sslsf).build();
    }




}
