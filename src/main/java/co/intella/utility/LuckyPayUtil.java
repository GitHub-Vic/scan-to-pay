package co.intella.utility;

import java.util.Locale;

import org.joda.time.DateTime;
import org.springframework.context.MessageSource;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.model.RequestHeader;

public class LuckyPayUtil {

	private Locale locale = new Locale("zh_TW");
	
	public ResponseGeneralHeader getResponseHeader(String result, RequestHeader requestHeader, MessageSource messageSource) {
		JsonObject json = new Gson().fromJson(result, JsonObject.class);
		ResponseGeneralHeader responseGeneralHeader = null;
		String returnCode = json.get("return_code").getAsString();
		if (json.has("Header")) {
			responseGeneralHeader = new Gson().fromJson(json.get("Header").toString(), ResponseGeneralHeader.class);
		} else {
			responseGeneralHeader = new ResponseGeneralHeader();
			if(returnCode.equals("000000")){
				responseGeneralHeader.setStatusCode("0000");
				responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
			}else if(returnCode.equals("000001")||returnCode.equals("000004")||returnCode.equals("000100")||returnCode.equals("000101")||returnCode.equals("000102")||returnCode.equals("000103")) {
				responseGeneralHeader.setStatusCode("8002");
				responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.8002", null, locale));
			}else if(returnCode.equals("000002")) {
				responseGeneralHeader.setStatusCode("7147");
				responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7147", null, locale));
			}else if(returnCode.equals("000003")) {
				responseGeneralHeader.setStatusCode("7124");
				responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7124", null, locale));
			}else if(returnCode.equals("000100")) {
				responseGeneralHeader.setStatusCode("8002");
				responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.8002", null, locale));
			}else if(returnCode.equals("400003")) {
				responseGeneralHeader.setStatusCode("7004");
				responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7004", null, locale));
			}else if(returnCode.equals("300000")) {
				responseGeneralHeader.setStatusCode("7002");
				responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7002", null, locale));
			}else if(returnCode.equals("000104")||returnCode.equals("000105")) {
				responseGeneralHeader.setStatusCode("8004");
				responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.8004", null, locale));
			}else if(returnCode.equals("100000")||returnCode.equals("100001")) {
				responseGeneralHeader.setStatusCode("7201");
				responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7201", null, locale));
			}else if(returnCode.equals("999999")) {
				responseGeneralHeader.setStatusCode("9999");
				responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.9999", null, locale));
			}else if(returnCode.equals("100104")) {
				responseGeneralHeader.setStatusCode("7143");
				responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7143", null, locale));
			}else {
				responseGeneralHeader.setStatusCode("7404");
				responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7404", null, locale));
			}
			
		}
		responseGeneralHeader.setServiceType(requestHeader.getServiceType());
		responseGeneralHeader.setMethod(requestHeader.getMethod());
		responseGeneralHeader.setMchId(requestHeader.getMerchantId());
		responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

		return responseGeneralHeader;
	}
}
