package co.intella.utility;

import java.util.Locale;

import org.joda.time.DateTime;
import org.springframework.context.MessageSource;

import co.intella.domain.integration.IntegratedRefundRequestData;
import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.domain.pay2go.Pay2GoRefundResult;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;

/**
 * @author Alex
 */

public class Pay2GoUtil {

    private Locale locale = new Locale("zh_TW");

    public ResponseGeneralHeader getResponseHeader(String code, String integrateMchId, RequestHeader requestHeader, MessageSource messageSource) {

        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();

        String statusCode;

        if (code.equals("SUCCESS") ){
            statusCode = "0000";
        }else if (code.equals("MTR01002") || code.equals("MTR01005") || code.equals("MTR01006")){
            statusCode = "7132";
        }else if (code.equals("MTR01003")){
            statusCode = "8004";
        }else if (code.equals("MTR01010")){
            statusCode = "7139";
        }else if (code.equals("MTR01018")){
            statusCode = "7112";
        }else if (code.equals("MTR01012") || code.equals("MTR01013")){
            statusCode = "7127";
        }else if (code.equals("MTR01015")){
            statusCode = "7113";
        }else if (code.equals("MTR01021")){
            statusCode = "7138";
        }else {
            statusCode = "9998";
        }
        responseGeneralHeader.setStatusCode(statusCode);
        responseGeneralHeader.setStatusDesc(messageSource.getMessage("response."+statusCode, null, locale));
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        responseGeneralHeader.setMchId(integrateMchId);
        return responseGeneralHeader;
    }

    public IntegratedRefundResponseData getIntegratedRefundResponseData(String code, Pay2GoRefundResult refundResult, RequestHeader requestHeader, TradeDetail tradeDetail, IntegratedRefundRequestData integratedRefundRequestData) {
        IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();

        if (code.equals("SUCCESS")) {
            integratedRefundResponseData.setStoreOrderNo(tradeDetail.getOrderId());
            integratedRefundResponseData.setStoreRefundNo(integratedRefundRequestData.getOutRefundNo());
            integratedRefundResponseData.setSysRefundNo(refundResult.getTradeNo());
            integratedRefundResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
            integratedRefundResponseData.setRefundedAt(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        }

        return integratedRefundResponseData;

    }
    
}
