package co.intella.utility;

import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.model.RequestHeader;
import org.joda.time.DateTime;
import org.springframework.context.MessageSource;

import java.util.Locale;

/**
 * @author Andy Lin
 */
public class AliShangHaiUtil {

    private Locale locale = new Locale("zh_TW");

    public ResponseGeneralHeader getResponseHeader(String code, String integrateMchId, RequestHeader requestHeader, MessageSource messageSource , String resultCode) {

        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();

        String statusCode;

        if (code.equals("0000") && "SUCCESS".equals(resultCode)){
            statusCode = "0000";
        }else if (code.equals("9999")){
            statusCode = "9995";
        }else {
            statusCode = "9998";
//            if(requestHeader.getServiceType().equals("Refund"))
//                statusCode = "7138";
//            else if(requestHeader.getServiceType().equals("SingleOrderQuery"))
//                statusCode = "7141";
//            else
//                statusCode ="9998";
        }

        responseGeneralHeader.setMethod("12520");
        responseGeneralHeader.setStatusCode(statusCode);
        responseGeneralHeader.setStatusDesc(messageSource.getMessage("response."+statusCode, null, locale));
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        responseGeneralHeader.setMchId(integrateMchId);
        return responseGeneralHeader;
    }
}
