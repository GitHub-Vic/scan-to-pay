package co.intella.utility;

import co.intella.domain.notify.ApnsMessage;
import co.intella.domain.notify.TransactionMessage;
import co.intella.model.TradeDetail;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.context.MessageSource;

/**
 * @author Miles
 */
public class AppNotificationUtil {

    public static TransactionMessage getNotifyMessages(TradeDetail tradeDetail, MessageSource messageSource) {
        TransactionMessage transactionMessage = new TransactionMessage();
        transactionMessage.setOrderId(tradeDetail.getOrderId());
        transactionMessage.setMoney(String.valueOf(tradeDetail.getPayment()));
        transactionMessage.setDate(tradeDetail.getCreateDate());
        transactionMessage.setDescription(tradeDetail.getDescription());
        transactionMessage.setPaymentType(GeneralUtil.getMethodName(tradeDetail.getMethod(), messageSource));
        transactionMessage.setOfficial(false);
        transactionMessage.setSystemOrderId(tradeDetail.getSystemOrderId());
        transactionMessage.setMerchantId(tradeDetail.getAccountId());
        transactionMessage.setDetail(StringUtils.defaultIfEmpty(tradeDetail.getDetail(), ""));

        return transactionMessage;
    }

    public static ApnsMessage getApnsMessages(TradeDetail tradeDetail, MessageSource messageSource) {
        ApnsMessage apnsMessage = new ApnsMessage();
        apnsMessage.setTitle("Intella");
        String msg = DateTime.now().toString("MM/dd") + "\u6536\u5230" + GeneralUtil.getMethodName(tradeDetail.getMethod(), messageSource) + tradeDetail.getPayment() + "\u5143\u6b3e\u9805";

        apnsMessage.setMsg(msg);


        return apnsMessage;
    }


}
