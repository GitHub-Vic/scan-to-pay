package co.intella.utility;

/**
 * @author Miles
 */
public interface SystemInstance {

    String ROLE_CRM_USER = "ROLE_CRM_USER";

    String ROLE_APP_USER = "ROLE_APP_USER";

    String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";

    String ROLE_USER = "ROLE_USER";

    String REQUEST_STRING = "[REQUEST]";

    String EMPTY_STRING = "";

    String STATUS_SUCCESS = "0000";

    String STATUS_SUCCESS_0001 = "0001";

    String STATUS_FAIL = "9997";

    String STATUS_DESCRIPTION_SUCCESS = "Success";

    String STATUS_DESCRIPTION_FAIL = "fail";
    String STATUS_DESCRIPTION_FAIL2 = "Fail";

    String DATE_PATTERN = "yyyyMMddHHmmss";

    String DATE_YYPATTERN = "yyMMddHHmmss";

    String DATE_PI_PATTERN = "yyyy-MM-dd HH:mm:ss";

    String DATE_TIME_PATTERN = "HHmmss";

    String DATE_DATE_PATTERN = "yyyyMMdd";

    String DATE_YYDATE_PATTERN = "yyMMdd";

    String CURRENCY_TWD = "TWD";

    String EXCEPTION_MESSAGE_DATA_FORMAT = "Invalid Data Format.";

    String ORDER_NOT_FOUND = "Order Not Found.";

    String TRADE_SUCCESS = "Trade success";

    String TRADE_FAIL = "Trade fail";

    String TRADE_REFUND_SUCCESS = "Refund success";

    String TRADE_REFUND_FAIL = "Refund fail";

    String TRADE_REFUND_INIT = "Refunding";

    String TRADE_CANCEL_SUCCESS = "Cancel success";

    String TRADE_CANCEL_FAIL = "Cancel fail";

    String TRADE_CANCEL_INIT = "Canceling";

    String TRADE_WAITING = "Waiting";

    String TRADE_CREATING_ORDER = "Creating order";

    String TRADE_INTERRUPTED = "Interrupted";

    String FIELD_NAME_STORE_ORDER_NO = "StoreOrderNo";

    String FIELD_NAME_AMOUNT = "Amount";

    String TYPE_MICROPAY = "Micropay";

    String TYPE_OLPAY = "OLPay";

    String TYPE_INAPP = "InApp";

    String TYPE_PAYMENT = "Payment";

    interface ServiceType {

        String TICKET_LOGIC = "TicketLogic";

        String FIND_CARD = "FindCard";
    }

}


