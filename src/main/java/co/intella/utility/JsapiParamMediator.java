package co.intella.utility;

import co.intella.domain.weixin.WeixinBasicRequestData;
import com.google.gson.annotations.SerializedName;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author Alex
 */

public class JsapiParamMediator extends WeixinBasicRequestData {



    @SerializedName("trade_type")
    @JsonProperty(value = "trade_type")
    private String tradeType;
    @JsonProperty(value = "time_expire")
    private String timeExpire;
    @JsonProperty(value = "out_trade_no")
    private String outTradeNo;
    private String body;
    @JsonProperty(value = "fee_type")
    private String feeType;
    @JsonProperty(value = "total_fee")
    private String totalFee;
    private String detail;
    @JsonProperty(value = "device_info")
    private String deviceInfo;


    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getTimeExpire() {
        return timeExpire;
    }

    public void setTimeExpire(String timeExpire) {
        this.timeExpire = timeExpire;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }
}
