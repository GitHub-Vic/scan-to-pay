package co.intella.utility;

import static co.intella.crypto.RsaCryptoUtil.decrypt;

import java.io.File;
import java.security.Key;
import java.security.PrivateKey;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.sun.org.apache.xml.internal.security.utils.Base64;

import co.intella.crypto.AesCryptoUtil;
import co.intella.crypto.KeyReader;
import co.intella.domain.DecryptRequest;
import co.intella.domain.integration.IntegratedResponse;
import co.intella.model.IntegratedRequest;
import co.intella.net.Constant;


@Service
public class AESUtil {
	
	Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	@Autowired
    private Environment env;
	
	private SecretKey decryptSecretKey(String encryptApiKey) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(env.getProperty("intella.private.key")).getFile());
        PrivateKey privateKey = KeyReader.loadPrivateKeyFromDER(file);
        byte[] byteKeys = Base64.decode(encryptApiKey);
        String aesKey = decrypt(byteKeys, privateKey);
        byte[] decode = Base64.decode(aesKey);
        return AesCryptoUtil.convertSecretKey(decode);
    }
	
	public DecryptRequest descryptData (IntegratedRequest requests) throws Exception {
		String requestBody = requests.getRequestBody();
        String encryptApiKey = requests.getApiKey();
        String body = "";
        SecretKey secretKey = null;
        secretKey = decryptSecretKey(encryptApiKey);
        body = AesCryptoUtil.decrypt(secretKey, Constant.IV, Base64.decode(requestBody));
        JsonObject jsonObject = new JsonParser().parse(body).getAsJsonObject();
        JsonObject header = jsonObject.getAsJsonObject("Header");
        JsonPrimitive data = jsonObject.getAsJsonPrimitive("Data");
        DecryptRequest decryptRequest = new DecryptRequest();
        decryptRequest.setData(data);
        decryptRequest.setHeader(header);
        decryptRequest.setSecretKey(secretKey);
        return decryptRequest;
	}
	
	public String encryptData (SecretKey secretKey, String data) {
		try {
			LOGGER.info("encryptData data" + data);
			byte[] encryptBytes = AesCryptoUtil.encrypt(secretKey, Constant.IV, data);
			IntegratedResponse integratedResponse = new IntegratedResponse();
			integratedResponse.setResult(Base64.encode(encryptBytes));
			return new Gson().toJson(integratedResponse);
		} catch (Exception e) {
			LOGGER.error("[EXCEPTION] " + e.getMessage() + " , " + Arrays.toString(e.getStackTrace()));
		}
		return "encrypt error";
	}
	
    public String useRfc2898DeriveBytesToEncrypt(String data, String fPassword, String fSalt) throws Exception{
    	SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
    	PBEKeySpec pbeKeySpec = new PBEKeySpec(fPassword.toCharArray(), fSalt.getBytes("UTF-8"), 1000, 384);
    	Key secretKey = factory.generateSecret(pbeKeySpec);
    	byte[] key = new byte[32];
    	byte[] iv = new byte[16];
    	System.arraycopy(secretKey.getEncoded(), 0, key, 0, 32);
    	System.arraycopy(secretKey.getEncoded(), 32, iv, 0, 16);

    	SecretKeySpec secret = new SecretKeySpec(key,"AES");
    	AlgorithmParameterSpec ivSpec = new IvParameterSpec(iv);
    	Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
    	cipher.init(Cipher.ENCRYPT_MODE, secret, ivSpec);
    	byte[] result = cipher.doFinal(data.getBytes("UTF-8"));
    	return java.util.Base64.getEncoder().encodeToString(result);
    }
}
