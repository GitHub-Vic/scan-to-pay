package co.intella.utility;

import co.intella.domain.integration.IntegratedMicropayResponseData;
import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.IntegratedSingleOrderQueryResponseData;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.domain.megatwpay.*;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import org.joda.time.DateTime;
import org.springframework.context.MessageSource;

import java.util.Locale;

public class MegaTWPayUtil {

    private static Locale locale = new Locale("zh_TW");


    public static ResponseGeneralHeader getResponseHeader(
            TxnPaymentRes responseObj, RequestHeader requestHeader,
            String integrateMchId, MessageSource messageSource) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        String intellaCode;

        if (("00").equals(responseObj.getTxnPaymentHeaderRes().getsCode()) && ("00")
                .equals(responseObj.getTxnPaymentDataRes().getTxnResult())) {
            intellaCode = "0000";
        } else if (responseObj.getTxnPaymentHeaderRes().getsCode().equals("10")) {
            intellaCode = "8002";
        } else if (responseObj.getTxnPaymentHeaderRes().getsCode().equals("82")) {
            intellaCode = "8004";
        } else if (responseObj.getTxnPaymentHeaderRes().getsCode().equals("99")) {
            intellaCode = "7404";
        } else {
            intellaCode = "9998";
        }
        responseGeneralHeader.setStatusCode(intellaCode);
        responseGeneralHeader
                .setStatusDesc(messageSource.getMessage("response." + intellaCode, null, locale));
        responseGeneralHeader.setMethod(requestHeader.getMethod());
        responseGeneralHeader.setMchId(integrateMchId);
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        return responseGeneralHeader;
    }

    public static ResponseGeneralHeader getResponseHeader(
            SingleOrderRes responseObj, RequestHeader requestHeader,
            String integrateMchId, MessageSource messageSource) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        String intellaCode;

        if (("00").equals(responseObj.getSingleOrderHeaderRes().getsCode()) && ("00")
                .equals(responseObj.getSingleOrderDataRes().getOrderStatus())) {
            intellaCode = "0000";
        } else if (responseObj.getSingleOrderHeaderRes().getsCode().equals("10")) {
            intellaCode = "8002";
        } else if (responseObj.getSingleOrderHeaderRes().getsCode().equals("82")) {
            intellaCode = "8004";
        } else if (responseObj.getSingleOrderHeaderRes().getsCode().equals("99")) {
            intellaCode = "7404";
        } else {
            intellaCode = "9998";
        }
        responseGeneralHeader.setStatusCode(intellaCode);
        responseGeneralHeader
                .setStatusDesc(messageSource.getMessage("response." + intellaCode, null, locale));
        responseGeneralHeader.setMethod(requestHeader.getMethod());
        responseGeneralHeader.setMchId(integrateMchId);
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        return responseGeneralHeader;
    }

    public static ResponseGeneralHeader getResponseHeader(
            TxnCancelRes responseObj, RequestHeader requestHeader,
            String integrateMchId, MessageSource messageSource) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        String intellaCode;

        if (("00").equals(responseObj.getTxnCancelHeaderRes().getsCode()) && ("00")
                .equals(responseObj.getTxnCancelDataRes().getTxnResult())) {
            intellaCode = "0000";
        } else if (responseObj.getTxnCancelHeaderRes().getsCode().equals("10")) {
            intellaCode = "8002";
        } else if (responseObj.getTxnCancelHeaderRes().getsCode().equals("82")) {
            intellaCode = "8004";
        } else if (responseObj.getTxnCancelHeaderRes().getsCode().equals("99")) {
            intellaCode = "7404";
        } else {
            intellaCode = "9998";
        }
        responseGeneralHeader.setStatusCode(intellaCode);
        responseGeneralHeader
                .setStatusDesc(messageSource.getMessage("response." + intellaCode, null, locale));
        responseGeneralHeader.setMethod(requestHeader.getMethod());
        responseGeneralHeader.setMchId(integrateMchId);
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        return responseGeneralHeader;
    }

    public static IntegratedMicropayResponseData getIntegratedResponseData(
            TxnPaymentRes txnPaymentRes, TxnPaymentReq txnPaymentReq) {
        IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();

        if ("00".equals(txnPaymentRes.getTxnPaymentDataRes().getTxnResult())) {
            integratedMicropayResponseData
                    .setSysOrderNo(txnPaymentRes.getTxnPaymentDataRes().getTxnSeqNo());
            integratedMicropayResponseData
                    .setTotalFee(txnPaymentRes.getTxnPaymentDataRes().getAmt());
        } else {
            integratedMicropayResponseData.setSysOrderNo(SystemInstance.EMPTY_STRING);
            integratedMicropayResponseData.setTotalFee(SystemInstance.EMPTY_STRING);
        }
        integratedMicropayResponseData
                .setStoreOrderNo(txnPaymentRes.getTxnPaymentDataRes().getOrderNo());
        integratedMicropayResponseData
                .setAuthCode(txnPaymentReq.getTxnPaymentDataReq().getTxnData());
        return integratedMicropayResponseData;
    }

    public static IntegratedSingleOrderQueryResponseData getIntegratedResponseData(
            SingleOrderRes singleOrderRes, TradeDetail tradeDetail, PaymentAccount paymentAccount) {
        IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();

//    if (!"00".equals(singleOrderRes.getSingleOrderDataRes().getOrderStatus())) {
//
//    } else {
        integratedSingleOrderQueryResponseData
                .setStoreOrderNo(singleOrderRes.getSingleOrderDataRes().getOrderNo());
        integratedSingleOrderQueryResponseData
                .setSysOrderNo(singleOrderRes.getSingleOrderDataRes().getTxnSeqNo());
        integratedSingleOrderQueryResponseData
                .setFeeType(singleOrderRes.getSingleOrderDataRes().getAmt());
        integratedSingleOrderQueryResponseData.setDeviceInfo(paymentAccount.getAccountTerminalId());
        integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());
        integratedSingleOrderQueryResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
        integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());
        integratedSingleOrderQueryResponseData.setPaidAt(tradeDetail.getCreateDate());
        integratedSingleOrderQueryResponseData.setRefundedMsg(tradeDetail.getRefundStatus());
//    }

        if (SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())
                && SystemInstance.TRADE_REFUND_SUCCESS.equals(tradeDetail.getRefundStatus())) {
            integratedSingleOrderQueryResponseData.setOrderStatus("3");
        } else if (SystemInstance.TRADE_SUCCESS.equals(tradeDetail.getStatus())) {
            integratedSingleOrderQueryResponseData.setOrderStatus("1");
        } else if (SystemInstance.TRADE_FAIL.equals(tradeDetail.getStatus())) {
            integratedSingleOrderQueryResponseData.setOrderStatus("2");
        } else if (SystemInstance.TRADE_WAITING.equals(tradeDetail.getStatus())) {
            integratedSingleOrderQueryResponseData.setOrderStatus("0");
        }
        return integratedSingleOrderQueryResponseData;
    }

    public static IntegratedRefundResponseData getIntegratedResponseData(
            TradeDetail tradeDetail, String storeRefundNo) {
        IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();

        integratedRefundResponseData.setStoreOrderNo(tradeDetail.getOrderId());
        integratedRefundResponseData.setStoreRefundNo(storeRefundNo);
        integratedRefundResponseData.setSysOrderNo(storeRefundNo);
        integratedRefundResponseData
                .setRefundedAt(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        return integratedRefundResponseData;
    }


    public static TxnPaymentReq generatePaymentReq(TradeDetail tradeDetail,
                                                   PaymentAccount paymentAccount) {
        TxnPaymentReq txnPaymentReq = new TxnPaymentReq();
        TxnPaymentHeaderReq txnPaymentHeaderReq = new TxnPaymentHeaderReq();
        TxnPaymentDataReq txnPaymentDataReq = new TxnPaymentDataReq();
        txnPaymentHeaderReq.settStamp(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        txnPaymentDataReq.setPscode(paymentAccount.getAccount());
        txnPaymentDataReq.setOrderNo(tradeDetail.getOrderId());
        txnPaymentDataReq.setAmt(Long.toString(tradeDetail.getPayment()));
        txnPaymentDataReq.setTxnData(tradeDetail.getBarcode());
        txnPaymentDataReq.setAcctDate(DateTime.now().toString(SystemInstance.DATE_DATE_PATTERN));
        txnPaymentDataReq.setTerminalNo(paymentAccount.getAccountTerminalId());
        txnPaymentReq.setTxnPaymentHeaderReq(txnPaymentHeaderReq);
        txnPaymentReq.setTxnPaymentDataReq(txnPaymentDataReq);
        return txnPaymentReq;
    }

    public static SingleOrderReq generateSingleOrderReq(TradeDetail tradeDetail,
                                                        PaymentAccount paymentAccount) {
        SingleOrderReq singleOrderReq = new SingleOrderReq();
        SingleOrderHeaderReq singleOrderHeaderReq = new SingleOrderHeaderReq();
        SingleOrderDataReq singleOrderDataReq = new SingleOrderDataReq();
        singleOrderHeaderReq.settStamp(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        singleOrderDataReq.setPscode(paymentAccount.getAccount());
        singleOrderDataReq.setOrderNo(tradeDetail.getOrderId());
        singleOrderReq.setSingleOrderHeaderReq(singleOrderHeaderReq);
        singleOrderReq.setSingleOrderDataReq(singleOrderDataReq);
        return singleOrderReq;
    }

    public static TxnCancelReq generateTxnCancelReq(TradeDetail tradeDetail,
                                                    PaymentAccount paymentAccount) {
        TxnCancelReq txnCancelReq = new TxnCancelReq();
        TxnCancelHeaderReq txnCancelHeaderReq = new TxnCancelHeaderReq();
        TxnCancelDataReq txnCancelDataReq = new TxnCancelDataReq();
        txnCancelHeaderReq.settStamp(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        txnCancelDataReq.setPscode(paymentAccount.getAccount());
        txnCancelDataReq.setOrderNo(tradeDetail.getOrderId());
        txnCancelDataReq.setAmt(Long.toString(tradeDetail.getPayment()));
        txnCancelDataReq.setTxnSeqNo(tradeDetail.getSystemOrderId());
        txnCancelDataReq.setCancelAmt(Long.toString(tradeDetail.getPayment()));
        txnCancelDataReq.setTerminalNo(paymentAccount.getAccountTerminalId());
        txnCancelReq.setTxnCancelHeaderReq(txnCancelHeaderReq);
        txnCancelReq.setTxnCancelDataReq(txnCancelDataReq);
        return txnCancelReq;
    }

}
