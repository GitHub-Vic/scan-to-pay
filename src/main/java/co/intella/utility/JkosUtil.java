package co.intella.utility;

import co.intella.domain.integration.IntegratedMicropayResponseData;
import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.domain.jkos.JkosBasicResponse;
import co.intella.domain.jkos.JkosRefundResponseData;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import org.joda.time.DateTime;
import org.springframework.context.MessageSource;

import java.util.Locale;
import java.util.Objects;

/**
 * @author Alex
 */
//@Transactional
public class JkosUtil {

    private Locale locale = new Locale("zh_TW");

    public ResponseGeneralHeader getJkosResponseHeader(JkosBasicResponse result, RequestHeader requestHeader, String integrateMchId, MessageSource messageSource) {

        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        if (Objects.isNull(result)) {
            responseGeneralHeader.setStatusCode("9998");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.9998", null, locale));
        } else if (!"000".equals(result.getStatusCode())) {
            if (result.getStatusCode().equals("902")) {
                responseGeneralHeader.setStatusCode("7158");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7158", null, locale));
            } else if (result.getStatusCode().equals("903")) {
                responseGeneralHeader.setStatusCode("8003");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.8003", null, locale));
            } else if (result.getStatusCode().equals("904")) {
                responseGeneralHeader.setStatusCode("8004");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.8004", null, locale));
            } else if (result.getStatusCode().equals("905")) {
                responseGeneralHeader.setStatusCode("8002");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.8002", null, locale));
            } else if (result.getStatusCode().equals("906")) {
                responseGeneralHeader.setStatusCode("7123");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7123", null, locale));
            } else if (result.getStatusCode().equals("916")) {
                responseGeneralHeader.setStatusCode("7002");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7002", null, locale));
            } else if (result.getStatusCode().equals("917")) {
                responseGeneralHeader.setStatusCode("7138");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7138", null, locale));
            } else if (result.getStatusCode().equals("918")) {
                responseGeneralHeader.setStatusCode("7138");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7138", null, locale));
            } else if (result.getStatusCode().equals("919")) {
                responseGeneralHeader.setStatusCode("9995");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.9995", null, locale));
            } else if (result.getStatusCode().equals("921")) {
                responseGeneralHeader.setStatusCode("7345");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7345", null, locale));
            } else if (result.getStatusCode().equals("922")) {
                responseGeneralHeader.setStatusCode("7111");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7111", null, locale));
            } else if (result.getStatusCode().equals("923")) {
                responseGeneralHeader.setStatusCode("7138");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7138", null, locale));
            } else {
                responseGeneralHeader.setStatusCode(SystemInstance.STATUS_FAIL);
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.9997", null, locale));
            }
        } else {
            responseGeneralHeader.setStatusCode(SystemInstance.STATUS_SUCCESS);
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        }

        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setMethod(requestHeader.getMethod());
        responseGeneralHeader.setMchId(integrateMchId);
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        return responseGeneralHeader;

    }

    public IntegratedMicropayResponseData getGeneralResponseData(JkosBasicResponse jkosBasicResponse, RequestHeader requestHeader) {

        IntegratedMicropayResponseData integratedMicropayResponseData = new IntegratedMicropayResponseData();
        if (Objects.nonNull(jkosBasicResponse) && "000".equals(jkosBasicResponse.getStatusCode())) {
            integratedMicropayResponseData.setSysOrderNo(jkosBasicResponse.getTradeNo());
            integratedMicropayResponseData.setTotalFee(String.valueOf(jkosBasicResponse.getTradeAmount()));
        } else {
            integratedMicropayResponseData.setSysOrderNo(SystemInstance.EMPTY_STRING);
            integratedMicropayResponseData.setTotalFee(SystemInstance.EMPTY_STRING);
        }
        integratedMicropayResponseData.setStoreOrderNo(jkosBasicResponse.getMerchantTradeNo());
        //todo get auth code
        integratedMicropayResponseData.setAuthCode("");
//        integratedMicropayResponseData.setPlatformRsp(new Gson().toJson(jkosBasicResponse));

        return integratedMicropayResponseData;
    }

    public IntegratedRefundResponseData getRefundGeneralResponseData(JkosBasicResponse jkosBasicResponse, RequestHeader requestHeader, String integrateMchId, TradeDetail tradeDetail) {
        IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();
        JkosRefundResponseData jkosRefundResponseData = (JkosRefundResponseData) jkosBasicResponse;
        if (jkosBasicResponse.getStatusCode().equals("000")) {
            integratedRefundResponseData.setSysRefundNo(((JkosRefundResponseData) jkosBasicResponse).getRefundTradeNo());
            integratedRefundResponseData.setRefundedAt(((JkosRefundResponseData) jkosBasicResponse).getRefundTradeTime());
        } else {
            integratedRefundResponseData.setSysRefundNo(SystemInstance.EMPTY_STRING);
            integratedRefundResponseData.setRefundedAt(SystemInstance.EMPTY_STRING);


        }

        //TradeDetail tradeDetail = tradeDetailService.getOne(integrateMchId,jkosBasicResponse.getMerchantTradeNo());

        integratedRefundResponseData.setStoreRefundNo(tradeDetail.getStoreRefundId());
        integratedRefundResponseData.setStoreOrderNo(jkosBasicResponse.getMerchantTradeNo());
        integratedRefundResponseData.setSysOrderNo(jkosBasicResponse.getTradeNo());
//        integratedRefundResponseData.setPlatformRsp(new Gson().toJson(jkosRefundResponseData));

        return integratedRefundResponseData;

    }

}