package co.intella.utility;

/**
 * @author Alex
 */

public enum WorkflowAction {
    APPROVE,
    RETURN,
    REJECT,
    RESET
}
