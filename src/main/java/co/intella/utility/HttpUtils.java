package co.intella.utility;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

/**
 * 
 * ClassName: HttpUtils <br/>  
 * Description: http工具 <br/>  
 * Copyright: Copyright (c) 2019 <br/>  
 * Company: intella.co <br/>  
 * @author Bob  
 * @version 1.0 
 * @Date 2019/07/19
 */
public class HttpUtils {
	
	
	/**
	 * 
	 * doPost: post請求. <br/> 
	 * @author Bob 
	 * @param url
	 * @param requestProperties
	 * @param urlParameters
	 * @return
	 * @throws Exception
	 * @date Create Time: 2019/07/19<br/>
	 */
	public static String doPost(String url, Map<String, String> requestProperties, String urlParameters) throws Exception {
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		for (String key: requestProperties.keySet()) {
			con.setRequestProperty(key, requestProperties.get(key));
		}
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		
		int responseCode = con.getResponseCode();
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		return response.toString();
	}
	
	/**
	 * 
	 * doGet:get 請求. <br/> 
	 * 
	 * @author Bob 
	 * @param url
	 * @return
	 * @throws Exception
	 * @date Create Time: 2019/07/19<br/>
	 */
	public static String doGet(String url) throws Exception {
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setDoOutput(true);
		
		int responseCode = con.getResponseCode();
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		return response.toString();
	}
}
