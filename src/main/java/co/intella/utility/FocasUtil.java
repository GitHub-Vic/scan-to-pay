package co.intella.utility;

import java.util.Locale;

import org.springframework.context.MessageSource;

import co.intella.domain.focas.FOCASPayResponse;
import co.intella.domain.integration.ResponseGeneralHeader;

public class FocasUtil {
	
    private Locale locale =new Locale("zh_TW");
    
    public ResponseGeneralHeader getFOCASPayResponseHeader(FOCASPayResponse response, MessageSource messageSource) {

        ResponseGeneralHeader responseHeader = new ResponseGeneralHeader();
        if ("0000".equals(response.getErrCode())) {
            responseHeader.setStatusCode("0000");
            responseHeader.setStatusDesc(messageSource.getMessage("response.0000" , null, locale));
        }
        else if ("-67".equals(response.getErrCode())){
            responseHeader.setStatusCode("7316");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7316" , null, locale));
        }
        else if ("82".equals(response.getErrCode()) || "N7".equals(response.getErrCode())){
            responseHeader.setStatusCode("7347");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7347" , null, locale));
        }
        else if ("14".equals(response.getErrCode())){
            responseHeader.setStatusCode("7314");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7314" , null, locale));
        }
        else if ("-51".equals(response.getErrCode())){
            responseHeader.setStatusCode("7127");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7127" , null, locale));
        }
        else if ("-321".equals(response.getErrCode())){
            responseHeader.setStatusCode("7303");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7303" , null, locale));
        }
        else if ("-300".equals(response.getErrCode())){
            responseHeader.setStatusCode("7000");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7000" , null, locale));
        }
        else if("6105".equals(response.getErrCode())) {
            responseHeader.setStatusCode("7002");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7002" , null, locale));
        } else if("6141".equals(response.getErrCode())) {
            responseHeader.setStatusCode("7138");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7138" , null, locale));
        }
        else {
            responseHeader.setStatusCode("9998");
            responseHeader.setStatusDesc(messageSource.getMessage("response.9998" , null, locale));
        }

        return responseHeader;
    }

}
