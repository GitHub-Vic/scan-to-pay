package co.intella.utility;

import co.intella.domain.megatwpay.NewNotifyReq;
import co.intella.model.PaymentAccount;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class NewMegaTWPayUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(NewMegaTWPayUtil.class);

    public static String getMegaTWPayCodeDesc(String code) {
        String result;
        switch (code) {
            case "0000":
                result = "成功";
                break;
            case "0101":
                result = "資料簽章值錯誤";
                break;
            case "0102":
                result = "訊息格式或內容編輯錯誤";
                break;
            case "0103":
                result = "目前無法查詢，請稍後再試";
                break;
            case "0104":
                result = "訊息發生一般檢核錯誤";
                break;
            case "0105":
                result = "交易已退過款，無法重複退款";
                break;
            case "0106":
                result = "欲退款之交易未完成/失敗，請檢查交易是否可以進行退款";
                break;
            case "0107":
                result = "無交易紀錄(單筆交易查詢)";
                break;
            case "0108":
                result = "無交易紀錄(多筆交易查詢)";
                break;
            case "0109":
                result = "不支援全國繳特店";
                break;
            case "0110":
                result = "轉帳購物交易不能退款";
                break;
            case "0111":
                result = "固定費率購物交易不能退款";
                break;
            case "0112":
                result = "查詢結果有0筆訂單交易";
                break;
            case "0113":
                result = "查詢結果有多筆訂單交易";
                break;
            case "0114":
                result = "該公司未啟用";
                break;
            case "0115":
                result = "該特店未啟用";
                break;
            case "0116":
                result = "該分店未啟用";
                break;
            case "0117":
                result = "該端末未啟用";
                break;
            case "0118":
                result = "特店支援收款類型不支援此筆交易";
                break;
            case "0119":
                result = "端末裝機型態不支援此筆交易";
                break;
            case "0120":
                result = "退款金額大於原始交易金額";
                break;
            case "0121":
                result = "非促銷商品金額大於商品金額";
                break;
            case "0122":
                result = "非一維被掃信用卡交易";
                break;
            case "4201":
                result = "交易金額超過限額";
                break;
            case "5000":
                result = "交易失敗";
                break;
            case "5004":
                result = "目前不支援此支付工具";
                break;
            case "5007":
                result = "訂單編號已重覆";
                break;
            case "5010":
                result = "部分退款次數已達上限，無法進行退款";
                break;
            case "5011":
                result = "部分退款總金額已超出原交易金額";
                break;
            case "5014 ":
                result = "特店不允許撥款日前退貨";
                break;
            case "5015":
                result = "特店不允許撥款日後退貨";
                break;
            case "5016":
                result = "已超過可退款期限，不允許退貨";
                break;
            case "5017":
                result = "不允許沖正退貨";
                break;
            case "5018":
                result = "特店不支援反掃QRcode功能";
                break;
            case "5020":
                result = "連線主機逾時";
                break;
            case "5035":
                result = "撥款後退貨餘額不足。";
                break;
            case "5113":
                result = "信用卡一維被掃收單服務尚未開啟";
                break;
            case "6000":
                result = "條碼格式錯誤";
                break;
            case "6001":
                result = "無效的QRCode";
                break;
            case "8000":
                result = "店家暫停營業";
                break;
            case "8001":
                result = "查無特店";
                break;
            case "9401":
                result = "認證錯誤";
                break;
            case "9403":
                result = "權限不足";
                break;
            case "9404":
                result = "資源不存在";
                break;
            case "9910":
                result = "系統暫停服務";
                break;
            default:
            case "9999":
                result = "系統忙碌中，請稍後再試";
                break;
        }

        return result;
    }

    public static boolean checkSign(NewNotifyReq newNotifyReq, PaymentAccount paymentAccount, String model) throws Exception {
        String str = JsonUtil.toJsonStr(newNotifyReq);
        boolean check = false;
        Map<String, String> signMap = new TreeMap<>();
        signMap.putAll(JsonUtil.parseJson(str, Map.class));
        String sign = signMap.remove("sign").replaceAll("-", "+").replaceAll("_", "/");
        String signStr = signMap.entrySet().stream().filter(en -> StringUtils.isNotBlank(en.getValue())).map(m -> m.getKey() + "=" + m.getValue()).collect(Collectors.joining("&"));
        LOGGER.info("NewMegaTWPayUtil  checkSign  signStr = " + signStr);
        if ("2".equals(model)) {
            Signature signature = Signature.getInstance("SHA256withRSA");
            PublicKey publicKey = loadPublicKeyFromSring(paymentAccount);
            signature.initVerify(publicKey);
            signature.update(signStr.getBytes());
            check = signature.verify(com.sun.org.apache.xml.internal.security.utils.Base64.decode(sign.getBytes()));
        } else {
            signStr = getSHA256Hex(signStr);
            LOGGER.info("checkSign  signStr getSHA256Hex = " + signStr);
            check = signStr.equalsIgnoreCase(sign);
        }

        return check;
    }

    private static PublicKey loadPublicKeyFromSring(PaymentAccount paymentAccount) throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] publicKeyDER = Base64.getDecoder().decode(paymentAccount.getHashKey());
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePublic(new X509EncodedKeySpec(publicKeyDER));
    }

    //SHA256加密
    private static String getSHA256Hex(String message) throws Exception {

        final MessageDigest digest = MessageDigest.getInstance("sha-256");
        byte[] messageHash = digest.digest(message.getBytes(StandardCharsets.UTF_8));
        return DatatypeConverter.printHexBinary(messageHash).toUpperCase();
    }

    private static PrivateKey loadPrivateFromString(PaymentAccount paymentAccount) throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] privateKeyDER = Base64.getDecoder().decode(paymentAccount.getHashIV());
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePrivate(new PKCS8EncodedKeySpec(privateKeyDER));
    }
}
