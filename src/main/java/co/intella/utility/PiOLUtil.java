package co.intella.utility;

import co.intella.domain.allpay.AllpayBasicResponseData;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.domain.pi.PiOLRefundRequestData;
import co.intella.domain.pi.PiOLRefundResponseData;
import co.intella.model.RequestHeader;
import org.joda.time.DateTime;
import org.springframework.context.MessageSource;

import java.util.Locale;

/**
 * @author Alex
 */

public class PiOLUtil {
    private Locale locale = new Locale("zh_TW");

    public ResponseGeneralHeader getPiRefundResponseHeader(PiOLRefundResponseData result, RequestHeader requestHeader, MessageSource messageSource) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();

        if (result.getResult().equals("success")){
            responseGeneralHeader.setStatusCode(SystemInstance.STATUS_SUCCESS);
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        }
        else {
            if (result.getErrorCode()== 1022){
                responseGeneralHeader.setStatusCode("7138");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7138", null, locale));
            }
            else if(result.getErrorCode()==60023){
                responseGeneralHeader.setStatusCode("7110");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7110", null, locale));
            }
            else {
                responseGeneralHeader.setStatusCode("7138");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7138", null, locale));
            }

        }
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setMethod(requestHeader.getMethod());
        responseGeneralHeader.setMchId(requestHeader.getMerchantId());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        return responseGeneralHeader;

    }

}
