package co.intella.utility;

public class IPassUtil {

    public static String getErrorMsg(String errorcode) {
        switch (errorcode) {
            case "900000":
                return  " 其他錯誤";
            case "900001":
            case "900002":
                return  " 命令格式錯誤";
            case "900003":
                return  " 關閉自動加值";
            case "900004":
                return  " 開啟自動加值";
            case "900005":
                return  " 資料長度錯誤";
            case "900006":
                return  " 讀卡機尚未認證";
            case "900007":
                return  " 讀卡機已鎖";
            case "900008":
                return  " SAM 卡尚未認證成功";
            case "900009":
                return  " 資料接收長度溢位";
            case "900033":
                return  " 讀卡機狀態錯誤";
            case "900034":
                return  " 無法取得讀卡機編號";
            case "900065":
                return  " 無法取得讀卡機編號";
            case "900066":
                return  " R1 長度錯誤";
            case "900067":
                return  " Auth Code 長度錯誤";
            case "900097":
                return  " 電源重置失敗";
            case "900098":
                return  " Select AID 失敗";
            case "900099":
                return  " GetData 失敗";
            case "90009A":
                return  " GetRandom 失敗";
            case "90009B":
                return  " SAM 認證失敗";
            case "90009C":
                return  " Get SessionKey 失敗";
            case "90009D":
                return  " 無 SAM 卡錯誤";
            case "90009E":
                return  " 押 TAC 錯誤";
            case "9000A1":
                return  " Challenge 失敗";
            case "9000A2":
                return  " AuthenticateC 失敗";
            case "9000A3":
                return  " ReaderIDAuth 失敗";
            case "9000A4":
                return  " GetRandomR2 失敗";
            case "9000A5":
                return  " AuthenticateP 失敗";
            case "9000A6":
                return  " GetSAMType 失敗";
            case "9000A7":
                return  " GetMifareKey 失敗";
            case "9000C9":
                return  " 詢卡失敗(無卡)";
            case "9000CA":
                return  " 多卡錯誤";
            case "9000FB":
                return  " Auth. S0 Error";
            case "9000FC":
                return  " Auth. S1 Error";
            case "9000FD":
                return  " Auth. S2 Error";
            case "9000FE":
                return  " Auth. S3 Error";
            case "9000FF":
                return  " Auth. S4 Error";
            case "900100":
                return  " Auth. S5 Error";
            case "900101":
                return  " Auth. S6 Error";
            case "900102":
                return  " Auth. S7 Error";
            case "900103":
                return  " Auth. S8 Error";
            case "900104":
                return  " Auth. S9 Error";
            case "900105":
                return  " Auth. S10 Error";
            case "900106":
                return  " Auth. S11 Error";
            case "900107":
                return  " Auth. S12 Error";
            case "900108":
                return  " Auth. S13 Error";
            case "900109":
                return  " Auth. S14 Error";
            case "90010A":
                return  " Auth. S15 Error";
            case "90010B":
                return  " ReadBlock S0B0 Error";
            case "90010C":
                return  " ReadBlock S0B1 Error";
            case "90010D":
                return  " ReadBlock S0B2 Error";
            case "90010E":
                return  " ReadBlock S0B3 Error";
            case "90010F":
                return  " ReadBlock S1B0 Error";
            case "900110":
                return  " ReadBlock S1B1 Error";
            case "900111":
                return  " ReadBlock S1B2 Error";
            case "900112":
                return  " ReadBlock S1B3 Error";
            case "900113":
                return  " ReadBlock S2B0 Error";
            case "900114":
                return  " ReadBlock S2B1 Error";
            case "900115":
                return  " ReadBlock S2B2 Error";
            case "900116":
                return  " ReadBlock S2B3 Error";
            case "900117":
                return  " ReadBlock S3B0 Error";
            case "900118":
                return  " ReadBlock S3B1 Error";
            case "900119":
                return  " ReadBlock S3B2 Error";
            case "90011A":
                return  " ReadBlock S3B3 Error";
            case "90011B":
                return  " ReadBlock S4B0 Error";
            case "90011C":
                return  " ReadBlock S4B1 Error";
            case "90011D":
                return  " ReadBlock S4B2 Error";
            case "90011E":
                return  " ReadBlock S4B3 Error";
            case "90011F":
                return  " ReadBlock S5B0 Error";
            case "900120":
                return  " ReadBlock S5B1 Error";
            case "900121":
                return  " ReadBlock S5B2 Error";
            case "900122":
                return  " ReadBlock S5B3 Error";
            case "900123":
                return  " ReadBlock S6B0 Error";
            case "900124":
                return  " ReadBlock S6B1 Error";
            case "900125":
                return  " ReadBlock S6B2 Error";
            case "900126":
                return  " ReadBlock S6B3 Error";
            case "900127":
                return  " ReadBlock S7B0 Error";
            case "900128":
                return  " ReadBlock S7B1 Error";
            case "900129":
                return  " ReadBlock S7B2 Error";
            case "90012A":
                return  " ReadBlock S7B3 Error";
            case "90012B":
                return  " ReadBlock S8B0 Error";
            case "90012C":
                return  " ReadBlock S8B1 Error";
            case "90012D":
                return  " ReadBlock S8B2 Error";
            case "90012E":
                return  " ReadBlock S8B3 Error";
            case "90012F":
                return  " ReadBlock S9B0 Error";
            case "900130":
                return  " ReadBlock S9B1 Error";
            case "900131":
                return  " ReadBlock S9B2 Error";
            case "900132":
                return  " ReadBlock S9B3 Error";
            case "900133":
                return  " ReadBlock S10B0 Error";
            case "900134":
                return  " ReadBlock S10B1 Error";
            case "900135":
                return  " ReadBlock S10B2 Error";
            case "900136":
                return  " ReadBlock S10B3 Error";
            case "900137":
                return  " ReadBlock S11B0 Error";
            case "900138":
                return  " ReadBlock S11B1 Error";
            case "900139":
                return  " ReadBlock S11B2 Error";
            case "90013A":
                return  " ReadBlock S11B3 Error";
            case "90013B":
                return  " ReadBlock S12B0 Error";
            case "90013C":
                return  " ReadBlock S12B1 Error";
            case "90013D":
                return  " ReadBlock S12B2 Error";
            case "90013E":
                return  " ReadBlock S12B3 Error";
            case "90013F":
                return  " ReadBlock S13B0 Error";
            case "900140":
                return  " ReadBlock S13B1 Error";
            case "900141":
                return  " ReadBlock S13B2 Error";
            case "900142":
                return  " ReadBlock S13B3 Error";
            case "900143":
                return  " ReadBlock S14B0 Error";
            case "900144":
                return  " ReadBlock S14B1 Error";
            case "900145":
                return  " ReadBlock S14B2 Error";
            case "900146":
                return  " ReadBlock S14B3 Error";
            case "900147":
                return  " ReadBlock S15B0 Error";
            case "900148":
                return  " ReadBlock S15B1 Error";
            case "900149":
                return  " ReadBlock S15B2 Error";
            case "90014A":
                return  " ReadBlock S15B3 Error";
            case "90014B":
                return  " WriteBlock S0B0 Error";
            case "90014C":
                return  " WriteBlock S0B1 Error";
            case "90014D":
                return  " WriteBlock S0B2 Error";
            case "90014E":
                return  " WriteBlock S0B3 Error";
            case "90014F":
                return  " WriteBlock S1B0 Error";
            case "900150":
                return  " WriteBlock S1B1 Error";
            case "900151":
                return  " WriteBlock S1B2 Error";
            case "900152":
                return  " WriteBlock S1B3 Error";
            case "900153":
                return  " WriteBlock S2B0 Error";
            case "900154":
                return  " WriteBlock S2B1 Error";
            case "900155":
                return  " WriteBlock S2B2 Error";
            case "900156":
                return  " WriteBlock S2B3 Error";
            case "900157":
                return  " WriteBlock S3B0 Error";
            case "900158":
                return  " WriteBlock S3B1 Error";
            case "900159":
                return  " WriteBlock S3B2 Error";
            case "90015A":
                return  " WriteBlock S3B3 Error";
            case "90015B":
                return  " WriteBlock S4B0 Error";
            case "90015C":
                return  " WriteBlock S4B1 Error";
            case "90015D":
                return  " WriteBlock S4B2 Error";
            case "90015E":
                return  " WriteBlock S4B3 Error";
            case "90015F":
                return  " WriteBlock S5B0 Error";
            case "900160":
                return  " WriteBlock S5B1 Error";
            case "900161":
                return  " WriteBlock S5B2 Error";
            case "900162":
                return  " WriteBlock S5B3 Error";
            case "900163":
                return  " WriteBlock S6B0 Error";
            case "900164":
                return  " WriteBlock S6B1 Error";
            case "900165":
                return  " WriteBlock S6B2 Error";
            case "900166":
                return  " WriteBlock S6B3 Error";
            case "900167":
                return  " WriteBlock S7B0 Error";
            case "900168":
                return  " WriteBlock S7B1 Error";
            case "900169":
                return  " WriteBlock S7B2 Error";
            case "90016A":
                return  " WriteBlock S8B0 Error";
            case "90016B":
                return  " WriteBlock S8B1 Error";
            case "90016C":
                return  " WriteBlock S8B2 Error";
            case "90016D":
                return  " WriteBlock S8B3 Error";
            case "90016E":
                return  " WriteBlock S9B0 Error";
            case "90016F":
                return  " WriteBlock S9B1 Error";
            case "900170":
                return  " WriteBlock S9B2 Error";
            case "900171":
                return  " WriteBlock S9B3 Error";
            case "900172":
                return  " WriteBlock S10B0 Error";
            case "900173":
                return  " WriteBlock S10B1 Error";
            case "900174":
                return  " WriteBlock S10B2 Error";
            case "900175":
                return  " WriteBlock S10B3 Error";
            case "900176":
                return  " WriteBlock S11B0 Error";
            case "900177":
                return  " WriteBlock S11B1 Error";
            case "900178":
                return  " WriteBlock S11B2 Error";
            case "900179":
                return  " WriteBlock S11B3 Error";
            case "90017A":
                return  " WriteBlock S12B0 Error";
            case "90017B":
                return  " WriteBlock S12B1 Error";
            case "90017C":
                return  " WriteBlock S12B2 Error";
            case "90017D":
                return  " WriteBlock S12B3 Error";
            case "90017E":
                return  " WriteBlock S13B0 Error";
            case "90017F":
                return  " WriteBlock S13B1 Error";
            case "900180":
                return  " WriteBlock S13B2 Error";
            case "900181":
                return  " WriteBlock S13B3 Error";
            case "900182":
                return  " WriteBlock S14B0 Error";
            case "900183":
                return  " WriteBlock S14B1 Error";
            case "900184":
                return  " WriteBlock S14B2 Error";
            case "900185":
                return  " WriteBlock S14B3 Error";
            case "900186":
                return  " WriteBlock S15B0 Error";
            case "900187":
                return  " WriteBlock S15B1 Error";
            case "900188":
                return  " WriteBlock S15B2 Error";
            case "900189":
                return  " WriteBlock S15B3 Error";
            case "90018A":
                return  " EV BEV Format Error";
            case "90018B":
                return  " 讀取六筆交易紀錄最新一筆錯誤";
            case "90018C":
                return  " Decrement S2B0 Error";
            case "90018D":
                return  " Restore S2B0 to S2B1 Error";
            case "90018E":
                return  " Increment S2B1 Error";
            case "90018F":
                return  " Restore S2B1 to S2B0 Error";
            case "900190":
                return  " 寫入六筆交易紀錄最新一筆錯誤";
            case "900191":
                return  " 詢卡無卡錯誤";
            case "900192":
                return  " 詢卡多卡錯誤";
            case "9001F5":
                return  " 發卡單位非 0x09";
            case "9001F6":
                return  " S1B0 卡片有效日期逾期";
            case "9001F7":
                return  " S1B0 卡片版本非 0x02";
            case "9001F8":
                return  " 卡片已鎖卡";
            case "9001F9":
                return  " 主備電子票值不符";
            case "9001FA":
                return  " 交易指標錯誤";
            case "9001FB":
                return  " 個人效期逾期";
            case "9001FC":
                return  " 主備電子票證負值";
            case "9001FD":
                return  " 同卡號電子票證復原失敗";
            case "9001FE":
                return  " 無法交易的票卡";
            case "900200":
                return  " 卡片無法讀取";
            case "900259":
                return  " 未執行讀卡前無法寫入";
            case "90025A":
                return  " 卡號不同無法寫入";
            case "90025B":
                return  " 票卡餘額不足";
            case "90025C":
                return  " 交易重複寫入";
            case "90025D":
                return  " 要取消加值之金額不正確";
            case "90025E":
                return  " 已無法加值(授權期限已過期)";
            case "90025F":
                return  " 已無法加值(授權餘額已不足)";
            case "900261":
                return  " 訊息封包格式錯誤";
            case "900262":
                return  " 訊息封包檢查碼錯誤";
            case "900267":
                return  " 不允許非可扣點卡種扣點";
            case "900300":
                return  " 卡片資料讀取錯誤";
            case "900301":
                return  " 電子票值重讀失敗";
            case "900400":
                return  " 卡片寫入錯誤";
            case "900500":
                return  " 非一卡通票卡";
            case "900501":
                return  " 票卡有效日期逾期";
            case "900502":
                return  " 票卡版本異常";
            case "900503":
                return  " 票卡狀態異常(已鎖卡)";
            case "900504":
                return  " 票卡防偽驗證錯誤";
            case "900505":
                return  " 電子票值異常";
            case "900506":
                return  " 交易記錄指標錯誤";
            case "900507":
                return  " 個人有效日期逾期";
            case "900508":
                return  " 非儲值型票卡無法進行交易";
            case "900509":
                return  " 交易金額超過 1000 元上限";
            case "900510":
                return  " 無自動加值權限";
            case "900511":
                return  " 超過當日累計交易金額上限 3000 元";
            case "900512":
                return  " 使用非原加值交易票卡進行取消加值交易";
            case "900513":
                return  " 不合法的交易類型";
            case "900514":
                return  " 授權驗證碼錯誤";
            case "900515":
                return  " 使用不同票卡進行同一交易(卡號錯誤)";
            case "900516":
                return  " 使用不同票卡進行同一交易(交易序號錯誤)";
            case "900517":
                return  " 黑名單鎖卡";
            case "900518":
                return  " 票卡餘額不足";
            case "900519":
                return  " 電子票值格式錯誤";
            case "900520":
                return  " 動態防偽驗證碼錯誤";
            case "900521":
                return  " 非自動加值票值倍數";
            case "900522":
                return  " 交易金額不可小於或等於 0";
            case "900523":
                return  " 加值後餘額不可超過1萬元上限";
            case "900600":
                return  " 詢卡無卡錯誤";
            case "900601":
                return  " 請勿多張票卡";
            case "900700":
                return  " WRONG_TAG";
            case "900701":
                return  " WRONG_ATRC_ATAC";
            case "900702":
                return  " WRONG_AAC";
            case "900703":
                return  " DB_EXCEPTION";
            case "900704":
                return  " 設備未註冊";
            case "900705":
                return  " 黑名單鎖卡";
            case "900706":
                return  " 無此服務";
            case "900707":
                return  " 與請求交易不符";
            case "900708":
                return  " 交易逾時";
            case "900709":
                return  " 設備未註冊";
            case "900710":
                return  " 回應逾時";
            case "900711":
                return  " 無效簽章命令長度";
            case "900712":
                return  " 無效簽章內容長度";
            case "900713":
                return  " 無此簽章命令";
            case "900714":
                return  " 無此簽章訊息";
            case "900715":
                return  " 簽章加/解密錯誤";
            case "900716":
                return  " 自動加值授權失敗";
            case "900717":
                return  " 無此銀行代碼";
            case "900800":
                return  " 寫入讀卡機錯誤";
            case "900801":
                return  " 讀卡機讀取資料逾時";
            case "900802":
                return  " 讀卡機檢查碼錯誤";
            case "900803":
                return  " 讀卡機回傳執行錯誤";
            case "900804":
                return  " SAM 回傳執行錯誤";
            case "900900":
                return  " 讀取 TxLog 錯誤";
            case "900905":
                return  " 尚未端末開機";
            case "900908":
                return  " 無此交易類型";
            case "900911":
                return  " Input 長度錯誤";
            case "900917":
                return  " Input 格式錯誤";
            case "900919":
                return  " 加密設定錯誤";
            case "90FFF7":
                return  " SAM 卡尚未認證成功";
            case "90FFF8":
                return  " 讀卡機已鎖";
            case "90FFF9":
                return  " 讀卡機尚未認證";
            case "90FFFA":
                return  " 資料長度錯誤";
            case "90FFFB":
                return  " 命令接收逾時";
            case "90FFFC":
                return  " BCC 錯誤";
            case "90FFFD":
                return  " 命令類別錯誤";
            case "90FFFE":
                return  " 命令格式錯誤";
            case "90FFFF":
                return  " 不明錯誤";
            case "910003":
                return  " 解AESKEY失敗";
            case "910004":
                return  " 交易中發生TimeOut";
            case "910005":
                return  " 交易中Socket TimeOut or 沒資料";
            case "920201":
                return  " Multi Card Error";
            case "920202":
                return  " Type A Active from idle fail";
            case "920203":
                return  " Type A Active from halt fail";
            case "920204":
                return  " Type A halt fail";
            case "920205":
                return  " RATS fail";
            case "920207":
                return  " IPASS First Key fail";
            case "920208":
                return  " Mifare get key fail";
            case "990000":
                return  " 找不到相關ServiceType";
            case "990001":
                return  " 無法建立ServiceType";
            case "990002":
                return  " Reader使用中";
            case "990003":
                return  " 資料格式錯誤";
            case "990004":
                return  " 黑名單檔不存在";
            case "990005":
                return  " 無法連接MqttService";
            case "990006":
                return  " Socket無法連接";
            case "999001":
                return  " 資料格式錯誤";
            case "999002":
                return  " 轉換json資料格式錯誤";
            case "999003":
                return  " TerminalID資料格式錯誤";
            case "999004":
                return  " DeviceID資料格式錯誤";
            case "999005":
                return  " SystemID資料格式錯誤";
            case "999006":
                return  " SPID資料格式錯誤";
            case "690000":
                return  " 黑名單下載失敗";
            case "690001":
                return  " 資料格式錯誤";
            case "690002":
                return  " 轉換json資料格式錯誤";
            case "690003":
                return  " Count資料格式錯誤";
            case "690004":
                return  " 沒有任何資料";
            case "690005":
                return  " 沒有下一筆資料";
            case "690006":
                return  " 資料格式錯誤";
            case "790000":
                return  " 其他錯誤";
            case "790001":
                return  " 資料格式錯誤";
            case "790002":
                return  " 轉換json資料格式錯誤";
            case "790003":
                return  " Count資料格式錯誤";
            case "790004":
                return  " 資料格式錯誤";
            case "790005":
                return  " 沒有任何資料";
            case "790006":
                return  " 沒有下一筆資料";
            case "790007":
                return  " 資料格式錯誤";
            case "790008":
                return  " 相關路徑檔設定錯誤";
            case "890000":
                return  " 部份檔案上傳FTP失敗";
            case "890001":
                return  " 資料格式錯誤";
            case "890002":
                return  " 轉換json資料格式錯誤";
            case "890003":
                return  " Count資料格式錯誤";
            case "890004":
                return  " 沒有任何資料";
            case "890005":
                return  " 沒有下一筆資料";
            case "890006":
                return  " 資料格式錯誤";
            case "890007":
                return  " 相關路徑檔設定錯誤";
            default:
                return errorcode;
        }
    }
}
