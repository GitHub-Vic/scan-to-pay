package co.intella.utility;

import org.apache.commons.lang3.StringUtils;

import co.intella.domain.fubon.FuBonParamBean;

public class FubonUtil {
	
	/**
	 * 必須同時滿足以下條件
	 * ResponseCode 欄位值為 000、001、或 006
	 * ResponseMsg 欄位值前八碼為 Approved 或 APPROVED
	 * ApproveCode 欄位必須有值
	 * @param fuBonParamRes
	 * @return
	 */
	public static boolean isPaymentApproved(FuBonParamBean fuBonParamRes) {

		if (("000".equals(fuBonParamRes.getResponseCode()) 
				|| "001".equals(fuBonParamRes.getResponseCode())
				|| "006".equals(fuBonParamRes.getResponseCode()))
				&& "Approved".equalsIgnoreCase(fuBonParamRes.getResponseMsg().substring(0, 8)) 
				&& !StringUtils.isBlank(fuBonParamRes.getApproveCode())) {
			return true;
		} else {
			return false;
		}
	}

}
