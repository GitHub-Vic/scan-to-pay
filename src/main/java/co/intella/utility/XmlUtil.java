package co.intella.utility;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class XmlUtil {

	public static String marshall(Object object) {

		try {
			StringWriter stringWriter = new StringWriter();
			JAXBContext context = JAXBContext.newInstance(object.getClass());
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);
			marshaller.marshal(object, stringWriter);
			return stringWriter.toString().replace("<ns2:", "<").replace("</ns2:", "</").replace("xmlns:ns2=", "xmlns=");
		} catch (JAXBException e) {
			e.printStackTrace();
			System.err.println(String.format("Catch a exception while marshalling: %s", e.getMessage()));
		}
		return "";
	}

	public static <T> T unmarshall(String xml, Class<T> clazz) {
		StringReader stringReader;
		T object = null;
		try {
			JAXBContext ctx = JAXBContext.newInstance(clazz);
			Unmarshaller unmarshaller = ctx.createUnmarshaller();
			stringReader = new StringReader(xml);
			object = clazz.cast(unmarshaller.unmarshal(stringReader));
			stringReader.close();
		} catch (JAXBException e) {
			e.printStackTrace();
			System.err.println(String.format("Catch a exception while unmarshalling: %s", e.getMessage()));
		}
		return object;
	}
	
}
