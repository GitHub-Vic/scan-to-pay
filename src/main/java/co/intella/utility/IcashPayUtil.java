package co.intella.utility;


import org.apache.tomcat.util.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;


import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.validation.constraints.NotNull;
import java.security.*;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class IcashPayUtil {
    private static final String CHARSET_NAME = "UTF-8";
    private static final String AES_NAME = "AES";
    // encryption mode
    public static final String ALGORITHM = "AES/CBC/PKCS7Padding";

    static {
        Security.addProvider(new BouncyCastleProvider());
    }


    public String encrypt(@NotNull String content, String hashKey, String hashIV) {
        byte[] result = null;
        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            SecretKeySpec keySpec = new SecretKeySpec(hashKey.getBytes(CHARSET_NAME), AES_NAME);
            AlgorithmParameterSpec paramSpec = new IvParameterSpec(hashIV.getBytes());
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, paramSpec);
            result = cipher.doFinal(content.getBytes(CHARSET_NAME));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Base64.encodeBase64String(result);
    }


    public String decrypt(@NotNull String content, String hashKey, String hashIV) {
        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            SecretKeySpec keySpec = new SecretKeySpec(hashKey.getBytes(CHARSET_NAME), AES_NAME);
            AlgorithmParameterSpec paramSpec = new IvParameterSpec(hashIV.getBytes());
            cipher.init(Cipher.DECRYPT_MODE, keySpec, paramSpec);
            return new String(cipher.doFinal(Base64.decodeBase64(content)), CHARSET_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "no decrypt";
    }
    /**
     * 簽名
     *
     * @param privateKey  私鑰
     * @param encdata    密文
     *
     */
    public String makeSign(String encdata, String privateKey)throws Exception {


        byte[] data = encdata.getBytes();
        byte[] keyBytes = java.util.Base64.getDecoder().decode(privateKey);
        java.security.Security.addProvider(
                new org.bouncycastle.jce.provider.BouncyCastleProvider()
        );
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);

            KeyFactory keyFactory = KeyFactory.getInstance("RSA");

            PrivateKey priKey = keyFactory.generatePrivate(pkcs8KeySpec);

            Signature signature = Signature.getInstance("SHA256withRSA");
            signature.initSign(priKey);
            signature.update(data);
            return Base64.encodeBase64String(signature.sign());


    }
    /**
     * 驗籤
     *
     * @param pubKey  公鑰
     * @param data    明文
     * @param signed  簽名
     */
    public  boolean verifySign(String pubKey, String data, String signed) {


        boolean SignedSuccess = false;
        try {
            //return publicKey
            byte[] pubKeybyte = java.util.Base64.getDecoder().decode(pubKey);
            X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(pubKeybyte);
            KeyFactory factory = KeyFactory.getInstance("RSA");
            PublicKey publicKey = factory.generatePublic(x509EncodedKeySpec);

            //return data
            byte[] databyte = data.getBytes();

            //return sign
            byte[] resXICPde = java.util.Base64.getDecoder().decode(signed);


            Signature verifySign = Signature.getInstance("SHA256withRSA");
            verifySign.initVerify(publicKey);
            verifySign.update(databyte);
            SignedSuccess = verifySign.verify(resXICPde);
            System.out.println("驗證---" + SignedSuccess);

        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException  | InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return SignedSuccess;
    }


}

//    private String ALGO = "AES";
//    private String ALGO_MODE = "AES/CBC/NoPadding";
//    private String akey = "Q88dNDnZUTh19lbPokDVP1KAClOP8PRu";
//    private String aiv = "LivoAluIi0zL5X9F";
//
//    public static void main(String[] args) throws Exception {
//        IcashPayUtil icpUtil = new IcashPayUtil();
//        JSONObject data = new JSONObject();
//
//        data.put("haha", "hehe");
//        System.out.println("字串"+data.toString());
//        String rstData = pkcs7padding(data.toString());
//        String passwordEnc = icpUtil.encrypt(rstData);
//        String passwordDec = icpUtil.decrypt(passwordEnc);
//        System.out.println("加密"+passwordEnc);
//        System.out.println("解密"+passwordDec);
//    }
//
//    public String encrypt(String Data) throws Exception {
//        try {
//            byte[] iv = aiv.getBytes();
//            Cipher cipher = Cipher.getInstance(ALGO_MODE);
//            int blockSize = cipher.getBlockSize();
//            byte[] dataBytes = Data.getBytes();
//            int plaintextLength = dataBytes.length;
//            if (plaintextLength % blockSize != 0) {
//                plaintextLength = plaintextLength + (blockSize - (plaintextLength % blockSize));
//            }
//            byte[] plaintext = new byte[plaintextLength];
//            System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);
//            SecretKeySpec keyspec = new SecretKeySpec(akey.getBytes("utf-8"), ALGO);
//            IvParameterSpec ivspec = new IvParameterSpec(iv);
//            cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
//            byte[] encrypted = cipher.doFinal(plaintext);
//            String EncStr = (new BASE64Encoder()).encode(encrypted);
//            return EncStr ;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    public String decrypt(String encryptedData) throws Exception {
//        try {
//            byte[] encrypted1 = (new BASE64Decoder()).decodeBuffer(encryptedData);
//            byte[] iv = aiv.getBytes();
//            Cipher cipher = Cipher.getInstance(ALGO_MODE);
//            SecretKeySpec keyspec = new SecretKeySpec(akey.getBytes("utf-8"), ALGO);
//            IvParameterSpec ivspec = new IvParameterSpec(iv);
//            cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);
//            byte[] original = cipher.doFinal(encrypted1);
//            String originalString = new String(original);
//            return originalString.trim();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    public static String pkcs7padding(String data) {
//        int bs = 16;
//        int padding = bs - (data.length() % bs);
//        String padding_text = "";
//        for (int i = 0; i < padding; i++) {
//            padding_text += (char)padding;
//        }
//        return data+padding_text;
//    }

