package co.intella.utility;

import org.apache.tomcat.util.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

/**
 * 
 * ClassName: AESUtil <br/>  
 * Description: AES加密工具 <br/>  
 * Copyright: Copyright (c) 2019 <br/>  
 * Company: intella.co <br/>  
 * @author Bob  
 * @version 1.0 
 * @Date 2019/07/19
 */
public class DonateAESUtil {
	// key
    private static final String KEY = "xxxxxxxxxxxxxxxx";
 
    //參數分別代表 算法名稱/加密模式/數據填充方式
    private static final String ALGORITHMSTR = "AES/ECB/PKCS5Padding";
 
    /**
     * 
     * encrypt: 執行AES加密. <br/> 
     * 
     * @author Bob 
     * @param content
     * @param encryptKey
     * @return
     * @throws Exception
     * @date Create Time: 2019/07/19<br/>
     */
    public static String encrypt(String content, String encryptKey) throws Exception {
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        kgen.init(128);
        Cipher cipher = Cipher.getInstance(ALGORITHMSTR);
        cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(encryptKey.getBytes(), "AES"));
        byte[] b = cipher.doFinal(content.getBytes("utf-8"));
        // 采用base64算法进行转码,避免出现中文乱码
        return Base64.encodeBase64String(b);
    }
 
    /**
     * 
     * decrypt: 執行aes解密. <br/> 
     * 
     * @author Bob 
     * @param encryptStr
     * @param decryptKey
     * @return
     * @throws Exception
     * @date Create Time: 2019/07/19<br/>
     */
    public static String decrypt(String encryptStr, String decryptKey) throws Exception {
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        kgen.init(128);
        Cipher cipher = Cipher.getInstance(ALGORITHMSTR);
        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(decryptKey.getBytes(), "AES"));
        // 采用base64算法进行转码,避免出现中文乱码
        byte[] encryptBytes = Base64.decodeBase64(encryptStr);
        byte[] decryptBytes = cipher.doFinal(encryptBytes);
        return new String(decryptBytes, "UTF-8");
    }
 
    /**
     * 
     * encrypt: aes 加密. <br/> 
     * 
     * @author Bob 
     * @param content
     * @return
     * @throws Exception
     * @date Create Time: 2019/07/19<br/>
     */
    public static String encrypt(String content) throws Exception {
        return encrypt(content, KEY);
    }
    
    /**
     * 
     * decrypt: aes 解密. <br/> 
     * 
     * @param encryptStr
     * @return
     * @throws Exception
     * @date Create Time: 2019/07/19<br/>
     */
    public static String decrypt(String encryptStr) throws Exception {
        return decrypt(encryptStr, KEY);
    }
}
