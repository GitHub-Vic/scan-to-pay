package co.intella.utility;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang.math.NumberUtils;
import org.bouncycastle.util.encoders.Base64;
import org.springframework.context.MessageSource;
import org.thymeleaf.context.Context;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import co.intella.domain.PaymentHeader;
import co.intella.domain.PaymentRequest;
import co.intella.domain.PaymentRequestData;
import co.intella.model.MailInformation;
import co.intella.model.RequestHeader;

/**
 * @author Alex
 */

public class GeneralUtil {

    public static String getMethodName(String method, MessageSource messageSource) {
        return messageSource.getMessage("payment.method." + method, null, new Locale("zh_TW"));
    }

    public static String getHash(String pin) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return byte2Hex(digest.digest(pin.getBytes()));

    }

    public static String getHashBase64(String pin) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            return new String(Base64.encode(digest.digest(pin.getBytes())), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }

    }

    public static String byte2Hex(byte[] inputBytes) {
        String result = "";
        for (byte currentByte : inputBytes) result += Integer.toString((currentByte & 0xff) + 0x100, 16).substring(1);
        return result;
    }

    public static byte[] hex2Byte(String hexString) {
        byte[] bytes = new byte[hexString.length() / 2];
        for (int i=0 ; i<bytes.length ; i++)
            bytes[i] = (byte) Integer.parseInt(hexString.substring(2 * i, 2 * i + 2), 16);
        return bytes;
    }

    public static Context getMailContext(String input) {
        Context ctx = new Context();
        ctx.setVariable("errorMessage", input);
        return ctx;
    }

    public static MailInformation getMailInformation(String[] mailToStaff, String emailTitle) {
        MailInformation mailInformation = new MailInformation();

        mailInformation.setMailTo(mailToStaff);
        mailInformation.setSubject("[Scan2 Pay] EMERGENCY MAIL !!! -  " + emailTitle);
        return mailInformation;
    }

    public static String getMessage(String response) {
        return new Gson().fromJson(response, JsonObject.class).get("message").getAsString();

    }

    public static String getMD5(String pin) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return byte2Hex(digest.digest(pin.getBytes()));

    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static String getHmacSha256(String msg, byte[] keyByte) throws Exception {
        String digest = null;
        try {
            SecretKeySpec key = new SecretKeySpec(keyByte, "HmacSHA256");
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(key);

            byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));
            byte2Hex(bytes);
            StringBuffer hash = new StringBuffer();
            for (byte aByte : bytes) {
                String hex = Integer.toHexString(0xFF & aByte);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (UnsupportedEncodingException e) {
        } catch (InvalidKeyException e) {
        } catch (NoSuchAlgorithmException e) {
        }
        return digest;

    }
    
    /**
     * split body string and append with 4 digit number
     * @param body
     * @return
     */
	public static String getBodyWithRandomNumber(String body) {
		
		if (body != null && body.contains("-")) {
			body = body.split("-")[0];
		}

		return String.format("%s-%04d", body, new Random().nextInt(9999));
	}

	
	public static String maskCreditCardNumber(String cardNumber) {
		
		if(cardNumber == null || cardNumber.isEmpty()) { 
			return "*************";
		} else {
			return cardNumber.substring(0,6) + "*************" + cardNumber.substring(cardNumber.length()-4);
		}
	}

	public static String mapToUrlQuery(Map<String,String> map) {
		
	     StringBuilder checkStr = new StringBuilder();

	        for(Map.Entry<String,String> entry : map.entrySet()) {
	            String key = entry.getKey();
	            String value = entry.getValue();

	            checkStr.append(key).append("=").append(value).append("&");
	        }
	        checkStr.deleteCharAt(checkStr.length()-1);

		return checkStr.toString();
	}
	
	
	public static String getOrderResultUrl(String apiUrl, String orderId) {

		return String.format("%sallpaypass/api/payment/order/result?orderId=%s", apiUrl,
				EncryptUtil.encryptSimple(orderId));

	}
	
	public static PaymentRequest convertToPaymentRequest(RequestHeader requestHeader, JsonPrimitive requestData) {
		
		PaymentRequest paymentRequest = new PaymentRequest();
		PaymentHeader paymentHeader = paymentRequest.getHeader();
		PaymentRequestData paymentRequestData = paymentRequest.getData();
		
		paymentHeader.setMchId(requestHeader.getMerchantId());
		paymentHeader.setMethod(paymentHeader.getMethod());
		paymentHeader.setServiceType(requestHeader.getServiceType());
		
        JsonObject data = new Gson().fromJson(requestData.getAsString(), JsonObject.class);
        paymentRequestData.setStoreRefundNo(String.valueOf(data.get("StoreRefundNo")));
        paymentRequestData.setStoreOrderNo(String.valueOf(data.get("StoreOrderNo")));
        paymentRequestData.setDeviceInfo(String.valueOf(data.get("DeviceInfo")));
        paymentRequestData.setRefundKey(String.valueOf(data.get("RefundKey")));
        paymentRequestData.setRefundFee(NumberUtils.toInt(data.get("RefundFee").getAsString(), 0));
		
        return paymentRequest;
	}

}
