package co.intella.utility;

import co.intella.domain.integration.IntegratedRefundRequestData;
import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.IntegratedSingleOrderQueryResponseData;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.domain.line.LineQueryResponseData;
import co.intella.domain.line.LineResponseData;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import org.joda.time.DateTime;
import org.springframework.context.MessageSource;

import java.util.Locale;

/**
 * @author Alex
 */

public class LinePayUtil {

    private Locale locale = new Locale("zh_TW");

    public ResponseGeneralHeader getResponseHeader(String returnCode, String returnMessage, RequestHeader requestHeader, MessageSource messageSource) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();

        if (returnCode.equals("0000") || returnCode.equals("1150")) {
            responseGeneralHeader.setStatusCode("0000");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        } else if (returnCode.equals("1133")) {
            responseGeneralHeader.setStatusCode("7123");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7123", null, locale));
        } else if (returnCode.equals("1124")) {
            responseGeneralHeader.setStatusCode("7126");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7126", null, locale));
        } else if (returnCode.equals("1104") || returnCode.equals("1105")) {
            responseGeneralHeader.setStatusCode("7132");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7132", null, locale));
        } else if (returnCode.equals("1106")) {
            responseGeneralHeader.setStatusCode("7158");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7158", null, locale));
        } else if (returnCode.equals("1155")) {
            responseGeneralHeader.setStatusCode("7138");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7138", null, locale));
        } else if (returnCode.equals("1163")) {
            responseGeneralHeader.setStatusCode("7155");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7155", null, locale));
        } else if (returnCode.equals("1164")) {
            responseGeneralHeader.setStatusCode("7112");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7112", null, locale));
        } else if (returnCode.equals("1165")) {
            responseGeneralHeader.setStatusCode("7112");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7112", null, locale));
        } else if (returnCode.equals("1282")) {
            responseGeneralHeader.setStatusCode("7342");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7342", null, locale));
        } else if (returnCode.equals("1288")) {
            responseGeneralHeader.setStatusCode("7345");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7345", null, locale));
        } else {
            responseGeneralHeader.setStatusCode("9998");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.9998", null, locale));
        }
        if (requestHeader.getMethod().equals("61500")) {
            responseGeneralHeader.setMethod("61500");
        } else {
            responseGeneralHeader.setMethod("11500");
        }
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        return responseGeneralHeader;
    }


    public IntegratedRefundResponseData getIntegratedRefundResponseData(LineResponseData lineResponseData, RequestHeader requestHeader, TradeDetail tradeDetail, IntegratedRefundRequestData integratedRefundRequestData) {
        IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();


        if (lineResponseData.getReturnCode().equals("0000")) {
            integratedRefundResponseData.setStoreOrderNo(tradeDetail.getOrderId());
            integratedRefundResponseData.setStoreRefundNo(integratedRefundRequestData.getOutRefundNo());
            integratedRefundResponseData.setSysRefundNo(lineResponseData.getInfo().getRefundTransactionId().toString());
            integratedRefundResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
            integratedRefundResponseData.setRefundedAt(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        }

        return integratedRefundResponseData;
    }

    public IntegratedSingleOrderQueryResponseData getIntegratedSingleOrderQueryResponseData(LineQueryResponseData lineQueryResponseData, TradeDetail tradeDetail) {
        IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();

        integratedSingleOrderQueryResponseData.setStoreOrderNo(tradeDetail.getOrderId());
        integratedSingleOrderQueryResponseData.setSysOrderNo("");
        integratedSingleOrderQueryResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
        integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());
        integratedSingleOrderQueryResponseData.setPaidAt(tradeDetail.getCreateDate());
        integratedSingleOrderQueryResponseData.setRefundedMsg(tradeDetail.getRefundStatus());
        if (lineQueryResponseData.getReturnCode().equals("0000")) {

            integratedSingleOrderQueryResponseData.setStoreOrderNo(lineQueryResponseData.getInfo().get(0).getOrderId());
            integratedSingleOrderQueryResponseData.setSysOrderNo(lineQueryResponseData.getInfo().get(0).getTransactionId().toString());
            integratedSingleOrderQueryResponseData.setFeeType(SystemInstance.CURRENCY_TWD);
            integratedSingleOrderQueryResponseData.setBody(lineQueryResponseData.getInfo().get(0).getProductName());
            integratedSingleOrderQueryResponseData.setTotalFee(String.valueOf(lineQueryResponseData.getInfo().get(0).getPayInfos().get(0).getAmount()));

            if (lineQueryResponseData.getInfo().get(0).getTransactionType().equals("PAYMENT")) {
                integratedSingleOrderQueryResponseData.setOrderStatus("1");
            } else if (lineQueryResponseData.getInfo().get(0).getTransactionType().equals("PAYMENT_REFUND")) {
                integratedSingleOrderQueryResponseData.setOrderStatus("3");
                integratedSingleOrderQueryResponseData.setRefundedAt((lineQueryResponseData.getInfo().get(0).getTransactionDate()));

            }

        } else if (lineQueryResponseData.getReturnCode().equals("1150")) {
            integratedSingleOrderQueryResponseData.setOrderStatus("0");
        } else {
        }


        return integratedSingleOrderQueryResponseData;
    }

}