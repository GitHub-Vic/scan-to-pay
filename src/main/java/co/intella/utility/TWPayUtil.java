package co.intella.utility;

import co.intella.constant.OrderStatus;
import co.intella.domain.integration.IntegratedRefundRequestData;
import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.IntegratedSingleOrderQueryResponseData;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.domain.twpay.*;
import co.intella.model.IntTbLookupCode;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.payneteasy.tlv.BerTag;
import com.payneteasy.tlv.BerTlv;
import com.payneteasy.tlv.BerTlvParser;
import com.payneteasy.tlv.HexUtil;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.context.MessageSource;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class TWPayUtil {

    private static final String TWMP_PARAMETER = "5275C6450F50E13499EC6FBB1B17FF9BFDB9E558030BF344";
    private static final String FISC_PARAMETER = "A69896AF6057EFAD836FB7EB3109B52AB896E6B9FEA7173E";
    private static final String TWMP_IV = "TWMPQRCD";
    private static final String WEB_TO_APP_IV = "FOCASAPIFOCASAPI";
    private static Locale locale = new Locale("zh_TW");

    private static String encryptForTWMP(String message) throws Exception {
        byte[] paramBytes = Hex.decodeHex(TWMP_PARAMETER.toCharArray());
        final SecretKey key = new SecretKeySpec(paramBytes, "DESede");
        final IvParameterSpec iv = new IvParameterSpec(TWMP_IV.getBytes());
        final Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key, iv);
        final MessageDigest digest = MessageDigest.getInstance("sha-256");
        byte[] messageHash = digest.digest(message.getBytes(StandardCharsets.UTF_8));
        final byte[] cipherText = cipher.doFinal(messageHash);
        final String hexString = DatatypeConverter.printHexBinary(cipherText);
        return StringUtils.substring(hexString, 0, 64);
    }

    private static String getSHA256Hex(String... messages) throws Exception {
        String message = "";
        for (String str : messages) {
            message += str;
        }
        final MessageDigest digest = MessageDigest.getInstance("sha-256");
        byte[] messageHash = digest.digest(message.getBytes(StandardCharsets.UTF_8));
        return DatatypeConverter.printHexBinary(messageHash);
    }

    private static String concatInputDataAlphabetical(Object object) {
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> map = objectMapper.convertValue(object, TreeMap.class);
        return joinValueAlphabetByKey(map);
    }

    private static String joinValueAlphabetByKey(Map<String, Object> map) {
        StringBuffer sb = new StringBuffer();
        for (String key : map.keySet()) {
            if (map.get(key) != null && !"verifyCode".equals(key)) {
                sb.append(map.get(key));
            }
        }
        System.out.println(sb.toString());
        return sb.toString();
    }

    public static String getTwmpVerifyCode(Object object) {

        try {
            return encryptForTWMP(concatInputDataAlphabetical(object));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getFiscVerifyCode(Object object) {

        try {
            return getSHA256Hex(concatInputDataAlphabetical(object), FISC_PARAMETER);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getFiscVerifyCode(Object object, String parameter) {

        try {
            return getSHA256Hex(concatInputDataAlphabetical(object), parameter);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static boolean isFiscVerifyCodeOk(Object object, String verifyCode) {

        if (verifyCode == null) return false;

        try {
            return verifyCode.equals(getFiscVerifyCode(object));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean isFiscVerifyCodeOk(Object object, String verifyCode, String parameter) {

        if (verifyCode == null) return false;

        try {
            return verifyCode.equals(getFiscVerifyCode(object, parameter));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static TWPayRefundRequest generateRefund2543Req(TradeDetail tradeDetail) throws Exception {

        QrpNotifyReq qrpNotifyReq = XmlUtil.unmarshall(tradeDetail.getTxParams(), QrpNotifyReq.class);
        if (Objects.isNull(qrpNotifyReq)) {
            throw new Exception("unmarshall fail");
        }
        TWPayRefundRequest refundRequest = new TWPayRefundRequest();
        BeanUtils.copyProperties(qrpNotifyReq, refundRequest, "systemDateTime", "localTime", "localDate");

        refundRequest.setMti("0200");
        refundRequest.setProcessingCode("002543");
        refundRequest.setLocalDate(DateTime.now().toString("yyyyMMdd"));
        refundRequest.setLocalTime(DateTime.now().toString("HHmmss"));

        OrgTxnData orgTxnData = new OrgTxnData();
        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        Map<String, String> orgTxnDataMap = new Gson().fromJson(qrpNotifyReq.getOrgTxnData(), type);
        orgTxnData.setMti(orgTxnDataMap.get("tag01"));
        orgTxnData.setProcessingCode(orgTxnDataMap.get("tag02"));
        orgTxnData.setAmt(qrpNotifyReq.getAmt());
        orgTxnData.setSystemDateTime(qrpNotifyReq.getSystemDateTime());
        orgTxnData.setSrrn(qrpNotifyReq.getSrrn());
        refundRequest.setTxnCurrencyCode("901");
        if ("8".equals(tradeDetail.getCreditCardType())) {
            refundRequest.setPosEntryMode("071");
        } else {
            refundRequest.setPosEntryMode("091");
        }
        refundRequest.setPosConditionCode("77");
        refundRequest.setOrgTxnData(orgTxnData);

        refundRequest.setHostId(qrpNotifyReq.getHostId());

        String traceNumber = String.format("%06d", new Random().nextInt(999999));
        refundRequest.setTraceNumber(traceNumber);

        orgTxnDataMap.put("tag02","002543");
        qrpNotifyReq.setOrgTxnData(JsonUtil.toJsonStr(orgTxnDataMap));
        qrpNotifyReq.setTraceNumber(traceNumber);
        qrpNotifyReq.setLocalDate(refundRequest.getLocalDate());
        qrpNotifyReq.setLocalTime(refundRequest.getLocalTime());
        tradeDetail.setTxParams(XmlUtil.marshall(qrpNotifyReq));

        return refundRequest;
    }

    public static QrpInqueryReq generateQrpInqueryReq(PaymentAccount paymentAccount, TradeDetail tradeDetail, String hostId) {

        QrpNotifyReq qrpNotifyReq = XmlUtil.unmarshall(tradeDetail.getTxParams(), QrpNotifyReq.class);
        QrpInqueryReq qrpInqueryReq = new QrpInqueryReq();
        BeanUtils.copyProperties(qrpNotifyReq, qrpInqueryReq, "systemDateTime", "localTime", "localDate");

        qrpInqueryReq.setMti("0800");
        qrpInqueryReq.setProcessingCode("000170");
        qrpInqueryReq.setLocalDate(DateTime.now().toString("yyyyMMdd"));
        qrpInqueryReq.setLocalTime(DateTime.now().toString("HHmmss"));
        qrpInqueryReq.setMerchantId(paymentAccount.getAccount());
        qrpInqueryReq.setTerminalId(paymentAccount.getAccountTerminalId());
        qrpInqueryReq.setHostId(hostId);

        OrgTxnData orgTxnData = new OrgTxnData();
        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        Map<String, String> orgTxnDataMap = new Gson().fromJson(qrpNotifyReq.getOrgTxnData(), type);
        orgTxnData.setMti(orgTxnDataMap.get("tag01"));
        orgTxnData.setProcessingCode(orgTxnDataMap.get("tag02"));
        orgTxnData.setAmt(qrpNotifyReq.getAmt());
        orgTxnData.setTraceNumber(qrpNotifyReq.getTraceNumber());
        orgTxnData.setLocalDate(qrpNotifyReq.getLocalDate());
        orgTxnData.setLocalTime(qrpNotifyReq.getLocalTime());
        qrpInqueryReq.setOrgTxnData(orgTxnData);

        return qrpInqueryReq;
    }

    public static ResponseGeneralHeader getResponseHeader(TwPayRequestData responseObj, RequestHeader requestHeader,
                                                          String integrateMchId, MessageSource messageSource) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        String intellaCode;

        if (responseObj.getResponseCode().equals("0000")) {
            intellaCode = "0000";
        } else if (responseObj.getResponseCode().equals("0012")) {
            intellaCode = "9999";
        } else if (responseObj.getResponseCode().equals("0013")) {
            intellaCode = "7313";
        } else if (responseObj.getResponseCode().equals("0018")) {
            intellaCode = "8004";
        } else {
            intellaCode = "9998";
        }
        responseGeneralHeader.setStatusCode(intellaCode);
        responseGeneralHeader.setStatusDesc(messageSource.getMessage("response." + intellaCode, null, locale));

//		if (requestHeader.getMethod().equals("62000")) {
//			responseGeneralHeader.setMethod("62000");
//		} else {
//			responseGeneralHeader.setMethod("12000");
//		}
        responseGeneralHeader.setMchId(integrateMchId);
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setMethod(requestHeader.getMethod());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        return responseGeneralHeader;
    }

    public static ResponseGeneralHeader getResponseHeader(QrpInqueryResp qrpInqueryResp, RequestHeader requestHeader,
                                                          String integrateMchId, MessageSource messageSource, Gson gson) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        String intellaCode;

        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        Map<String, String> otherInfo = gson.fromJson(qrpInqueryResp.getOtherInfo(), type);
        if (Objects.isNull(otherInfo)) {
            intellaCode = "0004";
        } else {
            if ("0000".equals(otherInfo.get("tag08"))) {
                intellaCode = "0000";
            } else if ("0012".equals(otherInfo.get("tag08"))) {
                intellaCode = "9999";
            } else if ("0013".equals(otherInfo.get("tag08"))) {
                intellaCode = "7313";
            } else if ("0018".equals(otherInfo.get("tag08"))) {
                intellaCode = "8004";
            } else if ("4202".equals(otherInfo.get("tag08"))) {
                intellaCode = "7324";
            } else {
                intellaCode = "9998";
            }
        }

        responseGeneralHeader.setStatusCode(intellaCode);
        responseGeneralHeader.setStatusDesc(messageSource.getMessage("response." + intellaCode, null, locale));

//		if (requestHeader.getMethod().equals("62000")) {
//			responseGeneralHeader.setMethod("62000");
//		} else {
//			responseGeneralHeader.setMethod("12000");
//		}
        responseGeneralHeader.setMchId(integrateMchId);
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        return responseGeneralHeader;
    }

    public static IntegratedRefundResponseData getIntegratedRefundResponseData(TwPayRequestData responseObj, RequestHeader requestHeader, TradeDetail tradeDetail, IntegratedRefundRequestData integratedRefundRequestData) {

        IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();
        if ("0000".equals(responseObj.getResponseCode())) {
            integratedRefundResponseData.setStoreOrderNo(tradeDetail.getOrderId());
            integratedRefundResponseData.setStoreRefundNo(integratedRefundRequestData.getOutRefundNo());
            integratedRefundResponseData.setSysRefundNo(tradeDetail.getSystemOrderId());
            integratedRefundResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
            integratedRefundResponseData.setRefundedAt(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        }

        return integratedRefundResponseData;
    }

    public static IntegratedSingleOrderQueryResponseData getIntegratedSingleOrderQueryResponseData(QrpInqueryResp qrpInqueryResp, TradeDetail tradeDetail, Gson gson) {

        IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();

        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        Map<String, String> otherInfo = gson.fromJson(qrpInqueryResp.getOtherInfo(), type);

        integratedSingleOrderQueryResponseData.setStoreOrderNo(tradeDetail.getOrderId());
        integratedSingleOrderQueryResponseData.setSysOrderNo(StringUtils.defaultIfEmpty(tradeDetail.getSystemOrderId(), ""));
        integratedSingleOrderQueryResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
        integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());
        integratedSingleOrderQueryResponseData.setPaidAt(tradeDetail.getCreateDate());
        integratedSingleOrderQueryResponseData.setRefundedMsg(tradeDetail.getRefundStatus());

        if ("0000".equals(qrpInqueryResp.getResponseCode())) {

            integratedSingleOrderQueryResponseData.setStoreOrderNo(tradeDetail.getOrderId());
            integratedSingleOrderQueryResponseData.setSysOrderNo(StringUtils.defaultIfEmpty(tradeDetail.getSystemOrderId(), ""));
            integratedSingleOrderQueryResponseData.setFeeType(SystemInstance.CURRENCY_TWD);
            integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());

            if ("0000".equals(otherInfo.get("tag08"))) { // 成功
                integratedSingleOrderQueryResponseData.setOrderStatus(OrderStatus.SUCCESS);
            } else if ("0083".equals(otherInfo.get("tag08"))) { // 此訂單交易已取消
                integratedSingleOrderQueryResponseData.setOrderStatus(OrderStatus.REFUND);
                integratedSingleOrderQueryResponseData.setRefundedAt("");
            } else if ("0027".equals(otherInfo.get("tag08"))) { // 原交易授權失敗
                integratedSingleOrderQueryResponseData.setOrderStatus(OrderStatus.FAIL);
            } else if ("0029".equals(otherInfo.get("tag08"))) { // 處理中
                integratedSingleOrderQueryResponseData.setOrderStatus(OrderStatus.PROCESSING);
            }
        }

        return integratedSingleOrderQueryResponseData;
    }

    private static BerTlv decodeAuthCodeToBerTlv(String authCode, int aFirstByte) {

        try {
            byte[] bytes = HexUtil.parseHex(Hex.encodeHexString(Base64.getDecoder().decode(authCode)));
            BerTlvParser parser = new BerTlvParser();
            return parser.parse(bytes, 0, bytes.length).find(new BerTag(aFirstByte));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    public static String decode2541PassiveQrCodeAsText(String authCode) {

        BerTlv tlv = decodeAuthCodeToBerTlv(authCode, 0xC1);
        if (tlv != null) {
            return tlv.getTextValue();
        }
        return "";
    }

    public static byte[] decode2541PassiveQrCodeAsHexBytes(String authCode) {

        BerTlv tlv = decodeAuthCodeToBerTlv(authCode, 0xC1);
        if (tlv != null) {
            tlv.getHexValue();
            return tlv.getBytesValue();
        }
        return new byte[0];
    }

    public static String getFiscVerifyCodeParam() {
        return FISC_PARAMETER;
    }


    /**
     * 一維條碼 解密，並補齊資料！
     *
     * @param authCode
     * @return
     */
    public static BarCodeData decode2541PassiveBarCodeAsText(String authCode) {

        Base64.Decoder decoder = Base64.getDecoder();
        byte[] authCodeBtye = decoder.decode(authCode.substring(2));
        String hexStr = DatatypeConverter.printHexBinary(authCodeBtye);
        BarCodeData barCodeData = new BarCodeData();
        if (hexStr.startsWith("0")) {
            String itemStr = hexStr.substring(1);
            barCodeData.setBankCode(itemStr.substring(4, 7) + "00000");
//            barCodeData.setTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH")) + itemStr.substring(0, 4));
            barCodeData.setTime(getTime(itemStr.substring(0, 4)));
            barCodeData.setDate(getDate());
            barCodeData.setCardNum(itemStr.substring(7, 23));
            barCodeData.setTSN("0000" + itemStr.substring(23, 25));
            barCodeData.setTAC(itemStr.substring(25, 29).getBytes());
            barCodeData.setKeep("00");

        } else if (hexStr.startsWith("1")) {
            String item1_16bit = hexStr.substring(1, 10);      //資訊1
            String item2_16bit = hexStr.substring(10, 24);     //資訊2
            String item3 = hexStr.substring(24);        //保留欄位+OTP

            String item1_10bit = String.format("%011d", Long.parseLong(item1_16bit, 16));
            String item2_10bit = String.format("%016d", Long.parseLong(item2_16bit, 16));
            barCodeData.setBankCode(item1_10bit.substring(4, 7) + "00000");
//            barCodeData.setTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH")) + item1_10bit.substring(0, 4));
            barCodeData.setTime(getTime(item1_10bit.substring(0, 4)));
            barCodeData.setDate(getDate());
            barCodeData.setCardNum(item2_10bit);
            barCodeData.setTSN("0000" + item1_10bit.substring(7));
            barCodeData.setTAC(item3.substring(2).getBytes());
            barCodeData.setKeep(item3.substring(0, 2));
        }
        return barCodeData;
    }

    private static String getDate() {
        return java.time.LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    }

    /**
     * 補合理小時值，誤差五分鐘內
     *
     * @param mmss
     * @return
     */
    private static String getTime(String mmss) {
        String time;
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime codeTime = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), now.getHour(), Integer.valueOf(mmss.substring(0, 2)), Integer.valueOf(mmss.substring(2)));
        if (Duration.between(now, codeTime).toMinutes() > 5) {
            time = codeTime.minusHours(1).format(DateTimeFormatter.ofPattern("HHmmss"));
        } else {
            time = codeTime.format(DateTimeFormatter.ofPattern("HHmmss"));
        }
        return time;
    }

    /**
     * 把16進位制字串轉換成位元組陣列
     *
     * @param hex
     * @return byte[]
     */
    public static byte[] hexStringToByte(String hex) {
        int len = (hex.length() / 2);
        byte[] result = new byte[len];
        char[] achar = hex.toCharArray();
        for (int i = 0; i < len; i++) {
            int pos = i * 2;
            result[i] = (byte) (toByte(achar[pos]) << 4 | toByte(achar[pos + 1]));
        }
        return result;
    }

    private static int toByte(char c) {
        byte b = (byte) "0123456789ABCDEF".indexOf(c);
        return b;
    }

    private final static String mHexStr = "0123456789ABCDEF";

    public static byte[] hexStr2Btye(String hexStr) {
        hexStr = hexStr.toString().trim().replace(" ", "").toUpperCase(Locale.US);
        char[] hexs = hexStr.toCharArray();
        byte[] bytes = new byte[hexStr.length() / 2];
        int iTmp = 0x00;

        for (int i = 0; i < bytes.length; i++) {
            iTmp = mHexStr.indexOf(hexs[2 * i]) << 4;
            iTmp |= mHexStr.indexOf(hexs[2 * i + 1]);
            bytes[i] = (byte) (iTmp & 0xFF);
        }
        return bytes;
    }


    public static WalletRefundRequest generateWalletRefundRequest(TradeDetail tradeDetail) {

        QrpNotifyReq qrpNotifyReq = XmlUtil.unmarshall(tradeDetail.getTxParams(), QrpNotifyReq.class);
        WalletRefundRequest refundRequest = new WalletRefundRequest();
        BeanUtils.copyProperties(qrpNotifyReq, refundRequest, "systemDateTime", "localTime", "localDate");

        refundRequest.setMti("0200");
        refundRequest.setProcessingCode("003010");
        refundRequest.setLocalDate(DateTime.now().toString("yyyyMMdd"));
        refundRequest.setLocalTime(DateTime.now().toString("HHmmss"));

        OrgTxnData orgTxnData = new OrgTxnData();
        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        Map<String, String> orgTxnDataMap = new Gson().fromJson(qrpNotifyReq.getOrgTxnData(), type);
        orgTxnData.setMti(orgTxnDataMap.get("tag01"));
        orgTxnData.setProcessingCode(orgTxnDataMap.get("tag02"));
        orgTxnData.setAmt(qrpNotifyReq.getAmt());
        orgTxnData.setSystemDateTime(qrpNotifyReq.getSystemDateTime());
        orgTxnData.setSrrn(qrpNotifyReq.getSrrn());
        orgTxnData.setTraceNumber(qrpNotifyReq.getTraceNumber());

        refundRequest.setCountryCode("158");
        refundRequest.setPosEntryMode("071");
        refundRequest.setPosConditionCode("77");
        refundRequest.setTxnCurrencyCode("901");
        refundRequest.setMcc("5999");       //TODO  不知道是啥 放文檔範例上的
        refundRequest.setOrgTxnData(orgTxnData);
        refundRequest.setMerchantName("INTELLA                Taipei        TWN");

        refundRequest.setHostId(qrpNotifyReq.getHostId());


        return refundRequest;
    }

    /**
     * web to app 加密
     *
     * @return
     */
    public static String encodeWeb2App(String str, String key, String type, String baseCodeVersion) throws Exception {

        System.err.println("URL encode = " + str);
//        System.err.println("URL encode length = " + str.length());
        //長度 10進 轉16進
        StringBuilder dataLen = new StringBuilder(Integer.toHexString(str.getBytes("Big5").length).toUpperCase());
        if ("Qrcode".equals(type)) {        //QRcode長度2
            while (dataLen.length() < 4) {
                dataLen.insert(0, "0");
            }
        } else {       //URL長度3
            while (dataLen.length() < 6) {
                dataLen.insert(0, "0");
            }
        }
        System.err.println("dataLen = " + dataLen);

        //加密內容(已經URL encode) getbyte  轉16進
        String data = DatatypeConverter.printHexBinary(str.getBytes("Big5"));
        System.err.println("data = " + data);

        String dataStr = dataLen.append(data).toString();
        System.err.println("dataStr.length() = " + dataStr.length());
        System.err.println("dataStr = " + dataStr);
        //AES with CBC
        String encDataStrHex = getTerminalVerifyCode(dataStr, key);
        System.err.println(encDataStrHex);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(GeneralUtil.hex2Byte(StringUtils.defaultIfBlank(baseCodeVersion, "02")));
        outputStream.write(GeneralUtil.hex2Byte(encDataStrHex));

        String base64Data = Base64.getEncoder().encodeToString(outputStream.toByteArray());
//        System.err.println(base64Data);

        return URLEncoder.encode(base64Data, "UTF-8");
    }


    public static void main(String[] args) throws Exception {
        String req = "TWQRP://社團法人中華民國軍人之友社台北國軍英雄館/158/01/V1?D1=100&D2=000000244625&D3=AYY721Dv9hj+&D10=901&D11=00,01701727189147000100010001;01,01701727189147000100010001";
        System.err.println(URLEncoder.encode(req, "UTF-8"));
        System.err.println(TWPayUtil.encodeWeb2App(URLEncoder.encode(req, "UTF-8"), "1C0F04617E890C297056AFD3C15DCB6F", "Qrcode", "01"));
//        String ss = "0002010102120216416798543903796404155190215439037961316315815543903796735960012tw.com.twqrp0176V1158AfwnpQVDx6Kk00,01101154390379600190010001;01,0110115439037960019001000152040037530390154041.005802TW5904test6006Taipei6103114621201081234567864130002ZH0103英特拉63044D36";
//        System.out.println(DatatypeConverter.printHexBinary(ss.getBytes()));

//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
////        if (StringUtils.isNotBlank(baseCodeVersion) && "1".equals(baseCodeVersion)) {
//        outputStream.write(GeneralUtil.hex2Byte("01"));
//        String base64Data = Base64.getEncoder().encodeToString(outputStream.toByteArray());
//        System.out.println(base64Data);
//        outputStream = new ByteArrayOutputStream();
//        outputStream.write(0x01);
//        base64Data = Base64.getEncoder().encodeToString(outputStream.toByteArray());
//        System.out.println(base64Data);

    }

    public static String getTerminalVerifyCode(String data, String verifyCodeParam) {
        try {

            byte[] key = GeneralUtil.hex2Byte(verifyCodeParam);

//            byte[] key;
//            if (byteKey.length == 16) {
//                key = new byte[24];
//                System.arraycopy(byteKey, 0, key, 0, 16);
//                System.arraycopy(byteKey, 0, key, 16, 8);
//            } else {
//                key = byteKey;
//            }

            SecretKey secretKey = new SecretKeySpec(key, 0, key.length, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            IvParameterSpec ips = new IvParameterSpec(WEB_TO_APP_IV.getBytes());
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, ips);
            byte[] dataBtyes = GeneralUtil.hex2Byte(data);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(dataBtyes);
            for (int i = dataBtyes.length % 16 == 0 ? 16 : dataBtyes.length % 16; 16 - i > 0; i++) {
                outputStream.write(0x00);
            }
            byte[] bOut = cipher.doFinal(outputStream.toByteArray());

            return DatatypeConverter.printHexBinary(bOut);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public static String generateQrPaymentInfoString(PaymentAccount paymentAccount, String amount, String twPayOrderId) {


        StringBuffer sb = new StringBuffer();
        sb.append("TWQRP://").append(paymentAccount.getAppId()) // 名稱
                .append("/158") // 國別碼; 158 台灣
                .append("/01") // 01:購物、02:轉帳、03:繳費
                .append("/V1") // 版本
                .append("?D1=").append(amount) // 交易金額
                .append("&D2=").append(twPayOrderId) // 訂單號碼
                .append("&D3=").append(paymentAccount.getHashKey()) // 安全碼
                .append("&D10=901") // 交易幣別; 901台幣
                .append("&D11=").append("00,").append(getAcquirerInfo(paymentAccount)) // 收單行資訊
                .append(";").append("01,").append(getAcquirerInfo(paymentAccount));
        //.append("&D12=").append(getQRExpirydate()); // QR Code 效期

        try {
            return URLEncoder.encode(sb.toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }

    private static String getAcquirerInfo(PaymentAccount paymentAccount) {

        StringBuffer sb = new StringBuffer();
        sb.append(paymentAccount.getBankCode())
                .append(paymentAccount.getAccount())
                .append(paymentAccount.getAccountTerminalId());

        return sb.toString();
    }

    public static String encryptForWebToApp(Object object, String key) {
        try {
            return getSHA256Hex(concatInputDataAlphabetical(object), key);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String generateEMVCode(PaymentAccount paymentAccount, String amount, String twPayOrderId, IntTbLookupCode lookupCode) {
        StringBuilder generateEMVCodeSb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        String tag35Data = "";
        if (StringUtils.isNotEmpty(paymentAccount.getHashKey())) {
            String tag35Data01 = sb2.append("V1158").append(paymentAccount.getHashKey()).append("00,").append(getAcquirerInfo(paymentAccount))
                    .append(";").append("01,").append(getAcquirerInfo(paymentAccount)).toString();
            sb2.delete(0, sb2.length());

            tag35Data = sb2.append("0012tw.com.twqrp01").append(String.format("%02d", tag35Data01.length())).append(tag35Data01).toString();
            sb2.delete(0, sb2.length());
        }
        String tag54Data = sb2.append(Long.valueOf(amount) / 100).append(".00").toString();
        sb2.delete(0, sb2.length());

        String tag62Data = sb2.append("01").append(String.format("%02d", twPayOrderId.length())).append(twPayOrderId).toString();
        sb2.delete(0, sb2.length());

        String tag64Data = sb2.append("0002zh01").append(String.format("%02d", paymentAccount.getAppId().length())).append(paymentAccount.getAppId()).toString();
        sb2.delete(0, sb2.length());

        String tag02Data = sb2.append(lookupCode.getType1()).append(paymentAccount.getSystemID()).toString();
        tag02Data = sb2.append(getLuhn(tag02Data)).toString();
        sb2.delete(0, sb2.length());

        String tag04Data = sb2.append(lookupCode.getType2()).append(paymentAccount.getSystemID()).toString();
//        tag04Data = sb2.append(getLuhn(tag04Data)).toString();
        sb2.delete(0, sb2.length());

        String tag13Data = sb2.append(lookupCode.getType3()).append(paymentAccount.getSystemID()).toString();
        tag13Data = sb2.append(getLuhn(tag13Data)).toString();
        sb2.delete(0, sb2.length());

        generateEMVCodeSb.append("000201010212")
                .append("02").append(String.format("%02d", tag02Data.length())).append(tag02Data)
                .append("04").append(String.format("%02d", tag04Data.length())).append(tag04Data)
                .append("13").append(String.format("%02d", tag13Data.length())).append(tag13Data);
        if (StringUtils.isNotEmpty(tag35Data)) {
            generateEMVCodeSb.append("35").append(String.format("%02d", tag35Data.length())).append(tag35Data);
        }
        generateEMVCodeSb.append("5204").append(paymentAccount.getHashIV()).append("5303901")
                .append("54").append(String.format("%02d", tag54Data.length())).append(tag54Data)
                .append("5802TW").append("59").append(String.format("%02d", paymentAccount.getSftpUser().length())).append(paymentAccount.getSftpUser())
                .append("6006Taipei")
                .append("62").append(String.format("%02d", tag62Data.length())).append(tag62Data)
                .append("64").append(String.format("%02d", tag64Data.length())).append(tag64Data)
                .append("6304");
        System.out.println("crc16 Str =>" + generateEMVCodeSb.toString());
        int crc16 = crc16(generateEMVCodeSb.toString().getBytes());
        StringBuilder crc16Hex = new StringBuilder(Integer.toHexString(crc16));
        System.out.println("crc16 enStr =>" + crc16Hex.toString());
        while (crc16Hex.length() < 4) {
            crc16Hex.insert(0, "0");
        }
        generateEMVCodeSb.append(crc16Hex.toString());
        return generateEMVCodeSb.toString();
    }

    private static int crc16(final byte[] buffer) {
        int crc = 0xFFFF;

        for (int j = 0; j < buffer.length; j++) {
            crc = ((crc >>> 8) | (crc << 8)) & 0xffff;
            crc ^= (buffer[j] & 0xff);//byte to int, trunc sign
            crc ^= ((crc & 0xff) >> 4);
            crc ^= (crc << 12) & 0xffff;
            crc ^= ((crc & 0xFF) << 5) & 0xffff;
        }
        crc &= 0xffff;
        return crc;

    }

    private static String getLuhn(String mchPan) {
        int count = 0;
        int mchPanLength = mchPan.length();
        for (int i = 0; i < mchPanLength; ++i) {
            char a = mchPan.charAt(mchPanLength - 1 - i);
            Integer aNum = Integer.valueOf(String.valueOf(a));
            if (i % 2 == 0) {
                aNum = aNum * 2;
                if (aNum >= 10) {
                    aNum = aNum / 10 + aNum % 10;
                }
            }
            count += aNum;
        }

        count = count * 9;
        count = count % 10;
        return String.valueOf(count);
    }

    public static TWPayEmvqrRefundRequest generateEmvqrRefund(TradeDetail tradeDetail) throws Exception {
        QrpEmvqrNotifyReq qrpNotifyReq = XmlUtil.unmarshall(tradeDetail.getTxParams(), QrpEmvqrNotifyReq.class);
        TWPayEmvqrRefundRequest emvqrRefundRequest = new TWPayEmvqrRefundRequest();
        BeanUtils.copyProperties(qrpNotifyReq, emvqrRefundRequest, "systemDateTime", "localTime", "localDate");

        emvqrRefundRequest.setMti("0200");
        emvqrRefundRequest.setProcessingCode("200000");
        emvqrRefundRequest.setLocalDate(DateTime.now().toString("yyyyMMdd"));
        emvqrRefundRequest.setLocalTime(DateTime.now().toString("HHmmss"));
        emvqrRefundRequest.setPosEntryMode("010");

//        String[] mchNames = qrpNotifyReq.getMerchantName().split(" ");
//        for (int i = 0; i <= 2; ++i) {
//            StringBuilder sb = new StringBuilder();
//            sb.append(mchNames[i]);
//            if (i == 0) {
//                while (sb.length() < 22) {
//                    sb.append(" ");
//                }
//
//            } else if (i == 1) {
//                while (sb.length() < 13) {
//                    sb.append(" ");
//                }
//            } else if (i == 2) {
//                while (sb.length() < 3) {
//                    sb.append(" ");
//                }
//            }
//            mchNames[i] = sb.toString();
//        }
//        StringBuilder sb = new StringBuilder();
//        sb.append(mchNames[0]).append(" ").append(mchNames[1]).append(" ").append(mchNames[2]);
//        emvqrRefundRequest.setMerchantName(sb.toString());
        emvqrRefundRequest.setMerchantName(qrpNotifyReq.getMerchantName());

        Type type = new TypeToken<Map<String, String>>() {
        }.getType();

        Map<String, String> reqOtherInfoMap = new Gson().fromJson(qrpNotifyReq.getOtherInfo(), type);
        Map<String, String> otherInfoMap = new HashMap<>();
        if (reqOtherInfoMap.containsKey("tag101"))
            otherInfoMap.put("tag101", reqOtherInfoMap.get("tag101"));      //交易卡片別
        if (reqOtherInfoMap.containsKey("tag102"))
            otherInfoMap.put("tag102", reqOtherInfoMap.get("tag102"));      //merchant Pan
        if (reqOtherInfoMap.containsKey("tag103"))
            otherInfoMap.put("tag103", reqOtherInfoMap.get("tag103"));      //銀連 voucher number
        otherInfoMap.put("tag105", "0");                                //TODO  第一次發送為0，重送要改為重送次數，最高9 (超過一樣)，要改！要存！
        emvqrRefundRequest.setOtherInfo(JsonUtil.toJsonStr(otherInfoMap));

        Map<String, String> reqOrgTxnDataMap = new Gson().fromJson(qrpNotifyReq.getOrgTxnData(), type);
        Map<String, String> orgTxnDataMap = new HashMap<>();
        if (reqOrgTxnDataMap.containsKey("tag01"))
            orgTxnDataMap.put("tag01", reqOrgTxnDataMap.get("tag01"));      //原交易類型
        if (reqOrgTxnDataMap.containsKey("tag02"))
            orgTxnDataMap.put("tag02", reqOrgTxnDataMap.get("tag02"));      //原處理代碼
        orgTxnDataMap.put("tag03", emvqrRefundRequest.getAmt());        //原交易金額
        orgTxnDataMap.put("tag07", qrpNotifyReq.getLocalDate());        //原交易日期
        orgTxnDataMap.put("tag08", qrpNotifyReq.getSrrn());             //原係統統追蹤號
        emvqrRefundRequest.setOrgTxnData(JsonUtil.toJsonStr(orgTxnDataMap));

        String traceNumber = String.format("%06d", new Random().nextInt(999999));
        emvqrRefundRequest.setTraceNumber(traceNumber);
        qrpNotifyReq.setTraceNumber(traceNumber);
        qrpNotifyReq.setLocalDate(emvqrRefundRequest.getLocalDate());
        qrpNotifyReq.setLocalTime(emvqrRefundRequest.getLocalTime());
        tradeDetail.setTxParams(XmlUtil.marshall(qrpNotifyReq));

        return emvqrRefundRequest;

    }
}

