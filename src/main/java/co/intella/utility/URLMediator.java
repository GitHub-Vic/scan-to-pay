package co.intella.utility;

/**
 * @author Alex
 */

public class URLMediator {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
