package co.intella.utility;

import co.intella.controller.IntegratedController;
import co.intella.crypto.AesCryptoUtil;
import co.intella.domain.StatusMessage;
import co.intella.domain.integration.IntegratedResponse;
import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralData;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.model.RequestHeader;
import co.intella.net.Constant;
import com.google.gson.Gson;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.crypto.SecretKey;

/**
 * @author Miles
 */
public class GeneralResponseMessageUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(IntegratedController.class);

    public static String getSuccessMessage() {
        StatusMessage status = new StatusMessage();
        status.setStatus("ok");
        status.setMessage("success");
        return new Gson().toJson(status);
    }

    public static String getSuccessMessage(String message) {
        StatusMessage status = new StatusMessage();
        status.setStatus("ok");
        status.setMessage(message);
        return new Gson().toJson(status);
    }

    public static String getFailureMessage(String message) {
        StatusMessage status = new StatusMessage();
        status.setStatus("fail");
        status.setMessage(message);
        return new Gson().toJson(status);
    }

    public static ResponseEntity<String> getResponseOfException(SecretKey secretKey, String body, String statusCode, String message) {
        byte[] encryptBytes;
        try {
            encryptBytes = AesCryptoUtil.encrypt(secretKey, Constant.IV, getFailureMessage(statusCode, message));
        } catch (Exception e) {
            LOGGER.error("[EXCEPTION][9995] " + e +" <" + statusCode +" "+ message + ">" + " [REQUEST] " + body);
            return new ResponseEntity<String> ("9995", HttpStatus.OK);
        }
        IntegratedResponse integratedResponse = new IntegratedResponse();
        integratedResponse.setResult(Base64.encode(encryptBytes).replaceAll("\r?\n","").trim());

        String encryptResponse = new Gson().toJson(integratedResponse);
        LOGGER.error("[getResponseOfException] " + encryptResponse);
        return new ResponseEntity<String> (encryptResponse, HttpStatus.OK);
    }

    public static ResponseGeneralHeader composeHeader(boolean isSuccess, RequestHeader header) {

        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setStatusCode(isSuccess ? SystemInstance.STATUS_SUCCESS : SystemInstance.STATUS_FAIL);
        responseGeneralHeader.setStatusDesc(isSuccess ? SystemInstance.STATUS_DESCRIPTION_SUCCESS : SystemInstance.STATUS_DESCRIPTION_FAIL);

        responseGeneralHeader.setServiceType(header.getServiceType());
        responseGeneralHeader.setMethod(header.getMethod());
        responseGeneralHeader.setMchId(header.getMerchantId());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        return responseGeneralHeader;
    }

    private static String getFailureMessage(String statusCode, String message) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setStatusCode(statusCode);
        responseGeneralHeader.setStatusDesc(message);
        ResponseGeneralData responseGeneralData = new ResponseGeneralData();
        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        responseGeneralBody.setHeader(responseGeneralHeader);
        responseGeneralBody.setData(responseGeneralData);
        return new Gson().toJson(responseGeneralBody);
    }

}
