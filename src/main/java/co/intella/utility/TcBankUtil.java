package co.intella.utility;

import co.intella.domain.integration.*;
import co.intella.domain.tcbank.TCBCreditCardCancelRequestData;
import co.intella.domain.tcbank.TCBCreditCardOrderQueryRequestData;
import co.intella.domain.tcbank.TCBCreditCardRequestData;
import co.intella.domain.tcbank.TCBCreditCardResponseData;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import com.google.gson.Gson;
import com.google.gson.JsonPrimitive;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import java.io.*;
import java.net.URL;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Miles
 */
public class TcBankUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(TcBankUtil.class);

    private static SSLSocketFactory sslSocketFactory = null;

    public static String convertTcbankCancelResponse(String result, TCBCreditCardCancelRequestData requestData, RequestHeader requestHeader, JsonPrimitive data) {
        TCBCreditCardResponseData tcbCreditCardResponseData = new Gson().fromJson(result, TCBCreditCardResponseData.class);

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();

        ResponseGeneralHeader header = new ResponseGeneralHeader();
        header.setMethod("20400");
        header.setServiceType("Cancel");
        header.setMchId(requestHeader.getMerchantId());
        header.setResponseTime(tcbCreditCardResponseData.getTransTime());
        header.setStatusCode(SystemInstance.STATUS_SUCCESS);
        header.setStatusDesc(SystemInstance.STATUS_DESCRIPTION_SUCCESS);

        IntegratedPaymentResponseData responseData = new IntegratedPaymentResponseData();
        responseData.setStoreOrderNo(tcbCreditCardResponseData.getOrderId());
        responseData.setTotalFee(tcbCreditCardResponseData.getCreaditAmt());
        responseData.setEstablishedAt(requestHeader.getCreateTime());
        responseData.setPaidAt(tcbCreditCardResponseData.getTransTime());
        responseData.setExtended(tcbCreditCardResponseData.getPrivateData());
        responseData.setApproveCode(tcbCreditCardResponseData.getApproveCode());
        responseData.setSecureStatus(tcbCreditCardResponseData.getSecureStatus());

        responseGeneralBody.setHeader(header);
        responseGeneralBody.setData(responseData);

        return new Gson().toJson(responseGeneralBody);
    }

    public static String convertTcbankOrderQueryResponse(String result, TCBCreditCardOrderQueryRequestData requestData, RequestHeader requestHeader, JsonPrimitive data, TradeDetail tradeDetail) {
        TCBCreditCardResponseData tcbCreditCardResponseData = new Gson().fromJson(result, TCBCreditCardResponseData.class);

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();

        ResponseGeneralHeader header = new ResponseGeneralHeader();
        header.setMethod("20400");
        header.setServiceType(requestHeader.getServiceType());
        header.setMchId(requestHeader.getMerchantId());
        header.setResponseTime(tcbCreditCardResponseData.getTransTime());

        header.setStatusCode(SystemInstance.STATUS_SUCCESS);
        header.setStatusDesc(SystemInstance.STATUS_DESCRIPTION_SUCCESS);

        IntegratedSingleOrderQueryResponseData responseData = new IntegratedSingleOrderQueryResponseData();
        responseData.setStoreOrderNo(tcbCreditCardResponseData.getOrderId());
        responseData.setSysOrderNo(tcbCreditCardResponseData.getOrderId());
        responseData.setTotalFee(tcbCreditCardResponseData.getCreaditAmt());
        responseData.setDeviceInfo(requestData.getTerminalId());
        responseData.setEstablishedAt(requestHeader.getCreateTime());
        responseData.setPaidAt(tcbCreditCardResponseData.getTransDate() + tcbCreditCardResponseData.getTransTime());
        responseData.setExtended(tcbCreditCardResponseData.getPrivateData());
        responseData.setApproveCode(tcbCreditCardResponseData.getApproveCode());
        responseData.setSecureStatus(tcbCreditCardResponseData.getSecureStatus());
        responseData.setFeeType(SystemInstance.CURRENCY_TWD);
        responseData.setRedeemBalance(tcbCreditCardResponseData.getRedeemBalance());
        responseData.setRedeemType(tcbCreditCardResponseData.getRedeemType());
        responseData.setRiskMask(tcbCreditCardResponseData.getRiskMark());
        responseData.setSysOrderNo(tcbCreditCardResponseData.getOrderId());

        // todo set response message as real
        String orderStatus = getTradeStatusCode(tradeDetail.getStatus());
        responseData.setOrderStatus(orderStatus);
        responseData.setOrderStatusDesc(tradeDetail.getStatus());

        responseGeneralBody.setHeader(header);
        responseGeneralBody.setData(responseData);

        return new Gson().toJson(responseGeneralBody);
    }

    public static String convertTcbankResponse(String result, TCBCreditCardRequestData requestData, RequestHeader requestHeader, JsonPrimitive data) throws JSONException {

        TCBCreditCardResponseData tcbCreditCardResponseData = new Gson().fromJson(result, TCBCreditCardResponseData.class);
        JSONObject integratedData = new JSONObject(data.getAsString());
        String integratedStatusCode = StatusManipulation.getCodeOfTCBank(tcbCreditCardResponseData.getResponseCode());
        String integratedStatusDesc = StatusManipulation.getDescOfTCBank(integratedStatusCode);

        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();

        ResponseGeneralHeader header = new ResponseGeneralHeader();
        header.setMethod("20400");
        header.setServiceType("Payment");
        header.setMchId(requestHeader.getMerchantId());
        header.setResponseTime(tcbCreditCardResponseData.getTransTime());
        header.setStatusCode(integratedStatusCode);
        header.setStatusDesc(integratedStatusDesc);

        IntegratedPaymentResponseData responseData = new IntegratedPaymentResponseData();
        responseData.setStoreOrderNo(tcbCreditCardResponseData.getOrderId());
        responseData.setTotalFee(tcbCreditCardResponseData.getCreaditAmt());
        responseData.setFeeType(SystemInstance.CURRENCY_TWD);
        responseData.setStoreFeeRate("defined");
        responseData.setDeviceInfo(requestData.getTerminalId());
        responseData.setBody(integratedData.get("Body").toString());
        responseData.setBarcodeMode(!integratedData.has("BarcodeMode") ? "": integratedData.get("BarcodeMode").toString());
        responseData.setDetail(!integratedData.has("Detail") ? "": integratedData.get("Detail").toString());
        responseData.setStoreInfo(!integratedData.has("StoreInfo")  ? "": integratedData.get("StoreInfo").toString());
        responseData.setCashier(!integratedData.has("Cashier") ? "":integratedData.get("Cashier").toString());
        responseData.setReconcileTime("");
        responseData.setSettlementTime("");
        responseData.setAppropriationTime("");
        responseData.setEstablishedAt(requestHeader.getCreateTime());
        responseData.setPaidAt(tcbCreditCardResponseData.getTransTime());
        responseData.setAcceptedAt("");
        responseData.setRejectedAt("");
        responseData.setRefundedAt("");
        responseData.setRefundedMsg("");
        responseData.setExtended(tcbCreditCardResponseData.getPrivateData());
        responseData.setApproveCode(tcbCreditCardResponseData.getApproveCode());
        responseData.setTransMode(tcbCreditCardResponseData.getTransMode());
        responseData.setSecureStatus(tcbCreditCardResponseData.getSecureStatus());
        responseData.setPlatformRsp(tcbCreditCardResponseData.getHtml());
        responseData.setToken(requestData.getToken());
        responseData.setSerialNumber(requestData.getTokenSeq());

        responseGeneralBody.setHeader(header);
        responseGeneralBody.setData(responseData);

        return new Gson().toJson(responseGeneralBody);
    }

    public static String upload(String mode, String filepath, String domain, String merchantId) throws IOException {

        HttpsURLConnection urlc = null;
        URL url = null;
        byte[] buffer = new byte[1024];
        int bytesRead = 100;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "---------------------------7d737331110542";
        BufferedReader br = null;
        FileInputStream fileInputStream = null;
        DataOutputStream dos = null;
        String sCHANNEL = "I";
        String fileName = null;
        String sMERCHANTID = null;
        String sDomain = null;
        String sMODE = "internet";
        String str = null;
        String strRsp = "";
        String sApp = "/mbec";
        File file = null;
        String protocol = "TLSv1.2";
        String apiVersion = "MBEC-FU-JavaApi_v1.2";
        String detailVersion = "";
        String acquirerURL = "";
        String acquirerIP = "";

        try {

            if(sMODE.trim().length() == 0) {
                LOGGER.info("Usage: MbecFileUpload Internet MerchantId Domain-name");
                System.exit(99);
            }

            if("internet".equalsIgnoreCase(sMODE)) {
                sCHANNEL = "I";
                sDomain = acquirerURL;
            } else if("leased".equalsIgnoreCase(sMODE)) {
                sCHANNEL = "L";
                sDomain = acquirerIP;
            } else {
                LOGGER.info("Usage: MbecFileUpload Internet MerchantId Domain-name");
                System.exit(99);
            }


            sMERCHANTID = merchantId;
            fileName = filepath + ".dat";

            file = new File(fileName);
            if(!file.exists()) {
                LOGGER.info("The file [" + fileName + "] does not exist.");
                System.exit(98);
            }

            if(domain.indexOf("http") > 0) {
                LOGGER.info("Format error with Domain-name/IP. " + domain);
                System.exit(97);
            }

            if(domain.indexOf(".nccc.com.tw") < 0) {
                if("internet".equals(sMODE)) {
                    LOGGER.info("Format error with Internet user Domain-name. " + domain);
                    System.exit(97);
                }
            } else if("leased".equals(sMODE)) {
                LOGGER.info("IP format error of leased line userage. " + domain);
                System.exit(97);
            }

            if(domain.indexOf(acquirerURL) >= 0) {
                sApp = "/mbec";
            } else if(acquirerIP.equals(domain)) {
                sApp = "/mbec";
            }

            sDomain = domain;

            boolean ex = false;

            try {
                Security.addProvider((Provider)Class.forName("com.sun.net.ssl.internal.ssl.Provider").newInstance());
                ex = true;
            } catch (Exception var51) {
                LOGGER.info("USE com.sun.net.ssl.internal.ssl.Provider FAILE");
            }

            if(!ex) {
                Security.addProvider((Provider)Class.forName("com.ibm.jsse.IBMJSSEProvider").newInstance());
            }

            detailVersion = apiVersion + "|Internet|" + protocol;
            TcBankUtil.MyTrustManager xtm = new TcBankUtil.MyTrustManager();
            TrustManager[] mytm = new TrustManager[]{xtm};
            SSLContext sslc = SSLContext.getInstance(protocol);
            sslc.init((KeyManager[])null, mytm, (SecureRandom)null);
            SSLSocketFactory sslSocketFactory = sslc.getSocketFactory();
            HttpsURLConnection.setDefaultHostnameVerifier(new TcBankUtil.MyVerified());
            url = new URL("https://" + sDomain + sApp + "/FileTransfer?template=FileTransfer&perform=APIUPLOAD&CHANNEL=" + sCHANNEL + "&MERCHANTID=" + sMERCHANTID + "&VERSION=" + detailVersion);
            urlc = (HttpsURLConnection)url.openConnection();
            urlc.setSSLSocketFactory(sslSocketFactory);
            urlc.setDoInput(true);
            urlc.setDoOutput(true);
            urlc.setUseCaches(false);
            urlc.setRequestMethod("POST");
            urlc.setRequestProperty("Connection", "Keep-Alive");
            urlc.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            dos = new DataOutputStream(urlc.getOutputStream());
            dos.writeBytes(lineEnd + twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"file\"; filename=\"" + fileName + "\"");
            dos.writeBytes(lineEnd);
            dos.writeBytes("Content-Type: application/octet-stream");
            dos.writeBytes(lineEnd + lineEnd);
            fileInputStream = new FileInputStream(file);

            while(bytesRead > 0) {
                if((bytesRead = fileInputStream.read(buffer, 0, 1024)) > 0) {
                    dos.write(buffer, 0, bytesRead);
                }
            }

            dos.writeBytes(lineEnd + twoHyphens + boundary + twoHyphens + lineEnd + lineEnd);
            dos.flush();

        } catch (Exception var56) {
            LOGGER.info("File upload failed with: " + var56.getMessage());
            var56.printStackTrace(System.out);
            System.exit(96);
        } finally {
            if(fileInputStream != null) {
                fileInputStream.close();
            }

            if(dos != null) {
                dos.close();
            }

        }

        try {
            br = new BufferedReader(new InputStreamReader(urlc.getInputStream(), "UTF-8"));

            while((str = br.readLine()) != null) {
                LOGGER.info(str);
                if(str != null) {
                    strRsp += str;
                }
            }
        } catch (Exception var54) {
            LOGGER.info("File upload failed with: " + var54.getMessage());
            var54.printStackTrace(System.out);
            System.exit(96);
        } finally {
            try {
                if(br != null) {
                    br.close();
                }

                if(urlc != null) {
                    urlc.disconnect();
                }
            } catch (Exception var50) {
                ;
            }
        }
        return strRsp;
    }

    public static String download(String mode, String filePath, String merchantId, String domain, String date) {
        boolean rtnCode = false;
        Object list = null;
        String sMODE = null;
        Object fileName = null;
        String sMERCHANTID = null;
        String sDomain = null;
        String sCHANNEL = null;
        String sApp = "/mbec";
        String sRspFileDate = "";
        String protocol = "TLSv1.2";
        String apiVersion = "MBEC-FD-JavaApi_v1.2";
        String detailVersion = "";
        String acquirerURL = "";
        String acquirerIP = "";

        sMODE = mode.toLowerCase();

        if(sMODE.trim().length() == 0) {
            LOGGER.info("Usage: MbecFileDownload Internet merchantId Domain-name");
            System.exit(99);
        }

        if("internet".equalsIgnoreCase(sMODE)) {
            sCHANNEL = "I";
            sDomain = acquirerURL;
        } else if("leased".equalsIgnoreCase(sMODE)) {
            sCHANNEL = "L";
            sDomain = acquirerIP;
        } else {
            LOGGER.info("Usage: MbecFileDownload Internet merchantId Domain-name");
            System.exit(99);
        }

        sMERCHANTID = merchantId;
        if(sMERCHANTID.trim().length() != 10) {
            LOGGER.info("Usage: MbecFileDownload Internet merchantId Domain-name");
            System.exit(99);
        }

        if(domain.indexOf("http") > 0) {
            LOGGER.info("Format error with Domain-name/IP. " + domain);
            System.exit(97);
        }

        if(domain.indexOf(".nccc.com.tw") < 0) {
            if("internet".equals(sMODE)) {
                LOGGER.info("Format error with Internet user Domain-name. " + domain);
                System.exit(97);
            }
        } else if("leased".equals(sMODE)) {
            LOGGER.info("IP format error of leased line userage. " + domain);
            System.exit(97);
        }

        if(domain.indexOf("nccnet-ecnew") >= 0) {
            sApp = "/mbec";
        } else if(acquirerIP.equals(domain)) {
            sApp = "/mbec";
        }

        sDomain = domain;

        if(date != null) {
            try {
                sRspFileDate = date.trim();
                SimpleDateFormat ex = new SimpleDateFormat("yyyyMMdd");
                ex.setLenient(false);
                ex.parse(sRspFileDate);
            } catch (Exception var24) {
                LOGGER.info("Date format error. " + date);
                sRspFileDate = "";
            }
        }

        try {
            boolean ex1 = false;

            try {
                Security.addProvider((Provider)Class.forName("com.sun.net.ssl.internal.ssl.Provider").newInstance());
                ex1 = true;
            } catch (Exception var22) {
                LOGGER.info("USE com.sun.net.ssl.internal.ssl.Provider FAILE");
            }

            if(!ex1) {
                Security.addProvider((Provider)Class.forName("com.ibm.jsse.IBMJSSEProvider").newInstance());
            }

            detailVersion = apiVersion + "|Internet|" + protocol;
            TcBankUtil.MyTrustManager xtm = new TcBankUtil.MyTrustManager();
            TrustManager[] mytm = new TrustManager[]{xtm};
            SSLContext sslc = SSLContext.getInstance(protocol);
            sslc.init((KeyManager[])null, mytm, (SecureRandom)null);
            sslSocketFactory = sslc.getSocketFactory();
            HttpsURLConnection.setDefaultHostnameVerifier(new TcBankUtil.MyVerified());

            try {
                int rtnCode1 = connectServer("https://" + sDomain + sApp + "/FileTransfer?template=FileTransfer&perform=APIDOWNLOAD&CHANNEL=" + sCHANNEL + "&MERCHANTID=" + sMERCHANTID + "&FILENAME=" + fileName + "&RSPFILEDATE=" + sRspFileDate + "&VERSION=" + detailVersion, true, filePath);
                LOGGER.info("result : " + rtnCode1);
                return Integer.toString(rtnCode1);
            } catch (Exception var21) {
                var21.printStackTrace(System.out);
            }
        } catch (Exception var23) {
            LOGGER.info("Down load failed with:" + var23.getMessage());
            var23.printStackTrace(System.out);
            System.exit(96);
        }
        return "";
    }

    private static String getTradeStatusCode(String status) {

        if(SystemInstance.TRADE_SUCCESS.equals(status)) {
            return "1";
        } else if ("Waiting".equals(status)) {
            return "0";
        } else if (SystemInstance.TRADE_FAIL.equals(status)) {
            return "2";
        } else if ("Creating order".equals(status)) {
            return "0";
        }

        return "2";
    }

    private static int connectServer(String urlString, boolean download, String filePath) throws Exception {

        StringBuffer responseMsg = new StringBuffer();

        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "---------------------------7d737331110542";
        URL url = null;
        HttpsURLConnection urlc = null;
        byte[] buffer = new byte[4096];
        boolean rtnCode = false;
        DataOutputStream dos = null;
        DataInputStream dis = null;
        String fileName = null;
        String sHEADER = null;
        String string = null;
        BufferedReader br = null;
        long len = 0L;
        url = new URL(urlString);
        urlc = (HttpsURLConnection)url.openConnection();
        urlc.setSSLSocketFactory(sslSocketFactory);
        urlc.setDoInput(true);
        urlc.setDoOutput(true);
        urlc.setUseCaches(false);
        urlc.setRequestMethod("POST");
        urlc.setRequestProperty("Connection", "Keep-Alive");
        if(download) {
            urlc.setRequestProperty("Content-Type", "multipart/form-data;boundary=---------------------------7d737331110542");
        }

        sHEADER = "application/octet-stream";
//        sHEADER = urlc.getContentType();
        responseMsg.setLength(0);
        byte rtnCode1;
        if("application/octet-stream".equalsIgnoreCase(sHEADER)) {
            sHEADER = urlc.getHeaderField("Content-Disposition");
            int pos;
            if("".equals(sHEADER) || sHEADER == null) {
                return 99;
            }else if((pos = sHEADER.lastIndexOf("filename=")) > 0) {
                fileName = sHEADER.substring(pos + 9);
            }

            try {
                dis = new DataInputStream(urlc.getInputStream());
                dos = new DataOutputStream(new FileOutputStream(filePath + fileName));
                len = 0L;

                int bytesRead;
                while((bytesRead = dis.read(buffer, 0, 4096)) > 0) {
                    len += (long)bytesRead;
                    dos.write(buffer, 0, bytesRead);
                }

                dos.flush();
                rtnCode1 = 0;
                LOGGER.info("Download [" + fileName + "] with (" + len + " bytes) successfully.");
            } catch (Exception var37) {
                LOGGER.info("Download failed with:" + var37.getMessage());
                var37.printStackTrace(System.out);
                rtnCode1 = 9;
                System.exit(96);
            } finally {
                if(dis != null) {
                    dis.close();
                }

                if(dos != null) {
                    dos.close();
                }

            }
        } else {
            try {
                br = new BufferedReader(new InputStreamReader(urlc.getInputStream(), "UTF-8"));

                while((string = br.readLine()) != null) {
                    responseMsg.append(string + "\n");
                }

                LOGGER.info("EC system responses:\n" + responseMsg.toString());
                rtnCode1 = 1;
            } catch (Exception var35) {
                LOGGER.info("Error occured while receiving response message with:" + var35.getMessage());
                var35.printStackTrace(System.out);
                rtnCode1 = 8;
            } finally {
                try {
                    if(br != null) {
                        br.close();
                    }

                    if(urlc != null) {
                        urlc.disconnect();
                    }
                } catch (Exception var34) {
                    ;
                }

            }
        }

        return rtnCode1;
    }

    private static List getAttribute(String xmlString) throws Exception {
        String name = "FILENAME=";
        int beginPos = 0;
        boolean endPos = false;
        ArrayList list = new ArrayList();

        while((beginPos = xmlString.indexOf("FILENAME=", beginPos)) >= 0) {
            beginPos += "FILENAME=".trim().length() + 1;
            int endPos1;
            if((endPos1 = xmlString.indexOf("\" ", beginPos)) >= 0) {
                list.add(xmlString.substring(beginPos, endPos1));
            }
        }

        return list;
    }

    private static class MyTrustManager implements X509TrustManager {
        MyTrustManager() {
        }

        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }

    private static class MyVerified implements HostnameVerifier {
        private MyVerified() {
        }

        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

}
