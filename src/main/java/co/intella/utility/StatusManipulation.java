package co.intella.utility;

/**
 * @author Andy Lin
 */
public class StatusManipulation {

    public static String getCodeOfECPay(String code, String serviceType) {
        String statusCode = "9998";
        if("1".equals(code) || "1|OK".equals(code)) {
            statusCode = "0000";
        }else {
            statusCode = "9998";
        }
        return statusCode;
    }

    public static String getCodeOfSpGateway(String code, String serviceType) {
        String statusCode = "9995";
        if("SUCCESS".equals(code)) {
            statusCode = "0000";
        }else if("TRA10001".equals(code) || "TRA10009".equals(code) || "TRA10012".equals(code)) {
            statusCode = "7132";
        }else if("TRA10003".equals(code)) {
            statusCode = "7125";
        }else if("TRA10008".equals(code)) {
            statusCode = "8003";
        }else if("TRA10013".equals(code)) {
            statusCode = "7300";
        }else if("TRA10015".equals(code)) {
            statusCode = "8015";
        }else if("TRA10016".equals(code)) {
            statusCode = "7342";
        }else if("TRA10021".equals(code)) {
            statusCode = "7002";
        }else if("TRA10037".equals(code)) {
            statusCode = "7102";
        }else if("TRA10042".equals(code)) {
            statusCode = "7314";
        }else if("TRA10043".equals(code)) {
            statusCode = "7316";
        }else if("TRA10044".equals(code)) {
            statusCode = "7347";
        }else if("TRA10045".equals(code)) {
            statusCode = "7137";
        }else if("TRA10052".equals(code)) {
            statusCode = "7344";
        }else if("TRA10053".equals(code)) {
            statusCode = "7345";
        }else if("TRA10054".equals(code)) {
            statusCode = "8004";
        }else if("TRA10058".equals(code)) {
            statusCode = "7346";
        }else if("TRA10060".equals(code)) {
            statusCode = "7343";
        }else if("TRA10067".equals(code)) {
            statusCode = "7348";
        }else if("TRA10075".equals(code)) {
            statusCode = "7142";
        }else if("TRA10105".equals(code)) {
            statusCode = "7309";
        }else if("TRA10106".equals(code)) {
            statusCode = "7349";
        }else if("TRA10107".equals(code)) {
            statusCode = "7301";
        }else if("TRA20004".equals(code)) {
            statusCode = "7146";
        }else if("TRA20011".equals(code)) {
            statusCode = "7325";
        }else if("TRA10036".equals(code) || "TRA40008".equals(code) || "MEM40008".equals(code) || "TRA40012".equals(code) || "MEM40012".equals(code) || "TRA40013".equals(code) || "MEM40013".equals(code)) {
            statusCode = "8002";
        }else if("TRA40014".equals(code) || "MEM40014".equals(code)) {
            statusCode = "7313";
        }
        return statusCode;
    }

    public static String getCodeOfPi(String code, String serviceType) {
        String statusCode = "9995";
        if(serviceType.equals(SystemInstance.TYPE_MICROPAY)) {
            if (code.equals("1001")) {
                statusCode = "7120";
            } else if(code.equals("1002")) {
                statusCode = "7121";
            } else if(code.equals("1003")) {
                statusCode = "7122";
            } else if(code.equals("1004")) {
                statusCode = "7123";
            } else if(code.equals("1005")) {
                statusCode = "7124";
            } else if(code.equals("1006")) {
                statusCode = "7125";
            } else if(code.equals("1007")) {
                statusCode = "7126";
            } else if(code.equals("1008")) {
                statusCode = "7000";
            } else if(code.equals("1009")) {
                statusCode = "7127";
            } else if(code.equals("1010")) {
                statusCode = "7128";
            } else if(code.equals("1011")) {
                statusCode = "7129";
            } else if(code.equals("1012")) {
                statusCode = "7130";
            } else if(code.equals("1013")) {
                statusCode = "7131";
            } else if(code.equals("1014")) {
                statusCode = "9998";
            } else if(code.equals("1015")) {
                statusCode = "7132";
            } else {
                statusCode = "9996";
            }
        }else if(serviceType.equals("Refund")) {
            if(code.equals("1005")) {
                statusCode = "7124";
            }else if(code.equals("1006")) {
                statusCode = "7127";
            }else if(code.equals("1007")) {
                statusCode = "7127";
            }else if(code.equals("1008")) {
                statusCode = "7135";
            }else if(code.equals("1009")) {
                statusCode = "7135";
            }else if(code.equals("1010")) {
                statusCode = "7135";
            }else if(code.equals("1011")) {
                statusCode = "7136";
            }else if(code.equals("1012")) {
                statusCode = "7137";
            }else if(code.equals("1013")) {
                statusCode = "7107";
            }else if(code.equals("1014")) {
                statusCode = "7139";
            }else if(code.equals("1015")) {
                statusCode = "7110";
            }else if(code.equals("20005")) {
                statusCode = "7002";
            }else if(code.equals("20001")) {
                statusCode = "8002";
            }else if(code.equals("20004")) {
                statusCode = "7300";
            }else if(code.equals("20006")) {
                statusCode = "7110";
            }else if(code.equals("20007")) {
                statusCode = "7109";
            }
        }else if(serviceType.equals("SingleOrderQuery")) {
            if(code.equals("1005")) {
                statusCode = "7124";
            }else if(code.equals("1006")) {
                statusCode = "7127";
            }else if(code.equals("1007")) {
                statusCode = "7127";
            }else if(code.equals("1009")) {
                statusCode = "7135";
            }else if(code.equals("1010")) {
                statusCode = "7135";
            }else if(code.equals("1011")) {
                statusCode = "7140";
            }else if(code.equals("1013")) {
                statusCode = "7141";
            }
        }else if(serviceType.equals("Cancel")) {
            if(code.equals("1001")) {
                statusCode = "7127";
            }else if(code.equals("1002")) {
                statusCode = "7002";
            }else if(code.equals("1003")) {
                statusCode = "7129";
            }else if(code.equals("1004")) {
                statusCode = "7129";
            }else if(code.equals("1005")) {
                statusCode = "7130";
            }else if(code.equals("1006")) {
                statusCode = "7130";
            }else if(code.equals("1007")) {
                statusCode = "7133";
            }else if(code.equals("1008")) {
                statusCode = "7134";
            }else if(code.equals("1009")) {
                statusCode = "7132";
            }else if(code.equals("1010")) {
                statusCode = "7132";
            }
        }else if(serviceType.equals("Reconciliate")) {
            if(code.equals("0")) {
                statusCode = "0000";
            }else if(code.equals("20001")) {
                statusCode = "8002";
            }else {
                statusCode = "9998";
            }
        }
        return statusCode;
    }

    public static String getCodeOfTCBank(String code) {
        String statusCode = "9999";

        if("".equals(code)) {
            // 3D html
            statusCode = "7341";
        }

        if(code.equals("00") || code.equals("08") || code.equals("11")) {
            statusCode = "0000";
        }else if(code.equals("01") || code.equals("02") || code.equals("P1") || code.equals("P4") || code.equals("P6") || code.equals("P9") || code.equals("T4")) {
            statusCode = "7300";
        }else if(code.equals("03") || code.equals("05") || code.equals("06") || code.equals("09") || code.equals("12") || code.equals("13") || code.equals("14") || code.equals("15") || code.equals("30") || code.equals("31") || code.equals("39") || code.equals("51") || code.equals("55") || code.equals("56") || code.equals("57") || code.equals("58") || code.equals("61") || code.equals("62") || code.equals("63") || code.equals("65") || code.equals("68") || code.equals("7592") || code.equals("94") || code.equals("N1") || code.equals("N2") || code.equals("N3") || code.equals("N4") || code.equals("N5") || code.equals("N6") || code.equals("N7")|| code.equals("N8") || code.equals("N9") || code.equals("O0") || code.equals("O1") || code.equals("O2") || code.equals("O3") || code.equals("O4") || code.equals("O5") || code.equals("O6") || code.equals("O7") || code.equals("O8") || code.equals("O9") || code.equals("P0") || code.equals("P2") || code.equals("P3") || code.equals("P5") || code.equals("P7") || code.equals("P8") || code.equals("Q0") || code.equals("Q1") || code.equals("Q2") || code.equals("Q3") || code.equals("Q4") || code.equals("Q6") || code.equals("Q7") || code.equals("Q8") || code.equals("Q9") || code.equals("R0") || code.equals("R1") || code.equals("R2") || code.equals("R3") || code.equals("R4") || code.equals("R5") || code.equals("R6") || code.equals("R7") || code.equals("R8") || code.equals("S4") || code.equals("S5") || code.equals("S6") || code.equals("S7") || code.equals("S8") || code.equals("S9") || code.equals("T5") || code.equals("A3") || code.equals("A9")) {
            statusCode = "7301";
        }else if(code.equals("04") || code.equals("07") || code.equals("34") || code.equals("35") || code.equals("36") || code.equals("37") || code.equals("38") || code.equals("41") || code.equals("43") || code.equals("Q5")) {
            statusCode = "7302";
        }else if(code.equals("33") || code.equals("54")) {
            statusCode = "7303";
        }else if(code.equals("T2")) {
            statusCode = "7304";
        }else if(code.equals("AY")) {
            statusCode = "7305";
        }else if(code.equals("A9")) {
            statusCode = "7306";
        }else if(code.equals("AD") || code.equals("AE")) {
            statusCode = "7307";
        }else if(code.equals("AI")) {
            statusCode = "7308";
        }else if(code.equals("AH") || code.equals("AG")) {
            statusCode = "7309";
        }else if(code.equals("AK") || code.equals("AL")) {
            statusCode = "7310";
        }else if(code.equals("TIMEOUT") || code.equals("801") || code.equals("802") || code.equals("803") || code.equals("804") || code.equals("805") || code.equals("931")) {
            statusCode = "9998";
        }else if(code.equals("900")) {
            statusCode = "8002";
        }else if(code.equals("901")) {
            statusCode = "7132";
        }else if(code.equals("902")) {
            statusCode = "7130";
        }else if(code.equals("903")) {
            statusCode = "7127";
        }else if(code.equals("904")) {
            statusCode = "7125";
        }else if(code.equals("905")) {
            statusCode = "7165";
        }else if(code.equals("906")) {
            statusCode = "7311";
        }else if(code.equals("907")) {
            statusCode = "7312";
        }else if(code.equals("908") || code.equals("909")) {
            statusCode = "7132";
        }else if(code.equals("910")) {
            statusCode = "7166";
        }else if(code.equals("911")) {
            statusCode = "7000";
        }else if(code.equals("912")) {
            statusCode = "7130";
        }else if(code.equals("913")) {
            statusCode = "7167";
        }else if(code.equals("914") || code.equals("915")) {
            statusCode = "7404";
        }else if(code.equals("916")) {
            statusCode = "9999";
        }else if(code.equals("917")) {
            statusCode = "8002";
        }else if(code.equals("918") || code.equals("919")) {
            statusCode = "7313";
        }else if(code.equals("920")) {
            statusCode = "7314";
        }else if(code.equals("921")) {
            statusCode = "9996";
        }else if(code.equals("922")) {
            statusCode = "7315";
        }else if(code.equals("923")) {
            statusCode = "7316";
        }else if(code.equals("925") || code.equals("926")) {
            statusCode = "7317";
        }else if(code.equals("927")) {
            statusCode = "7318";
        }else if(code.equals("928")) {
            statusCode = "7319";
        }else if(code.equals("929")) {
            statusCode = "7320";
        }else if(code.equals("933")) {
            statusCode = "7002";
        }else if(code.equals("934")) {
            statusCode = "7321";
        }else if(code.equals("935")) {
            statusCode = "7322";
        }else if(code.equals("936")) {
            statusCode = "7323";
        }else if(code.equals("937")) {
            statusCode = "7324";
        }else if(code.equals("939")) {
            statusCode = "7325";
        }else if(code.equals("940")) {
            statusCode = "7326";
        }else if(code.equals("941")) {
            statusCode = "7327";
        }else if(code.equals("942")) {
            statusCode = "7127";
        }else if(code.equals("943")) {
            statusCode = "7328";
        }else if(code.equals("944")) {
            statusCode = "7329";
        }else if(code.equals("945")) {
            statusCode = "7340";
        }else if(code.equals("589")) {
            statusCode = "7404";
        }else if(code.equals("998")) {
            statusCode = "7341";
        }else if(code.equals("999")) {
            statusCode = "9995";
        }
        return statusCode;
    }

    public static String getDescOfTCBank(String code) {
        String statusDesc = "System maintaining";
        //todo: The whole spec of status-code and status-message to be given
        if(code.equals("0000")) {
            statusDesc = "Success";
        } else if("7341".equals(code)) {
            statusDesc = "3D-verify";
        } else {
            statusDesc = "Decline";
        }
        return statusDesc;
    }
}
