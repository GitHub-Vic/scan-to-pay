package co.intella.utility;

import co.intella.domain.integration.IntegratedRefundRequestData;
import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import com.google.gson.JsonObject;
import org.joda.time.DateTime;
import org.springframework.context.MessageSource;

import java.util.Locale;

/**
 * @author Alex
 */

public class FridayUtil {
    private Locale locale = new Locale("zh_TW");

    public ResponseGeneralHeader getResponseHeader(String code, String integrateMchId, RequestHeader requestHeader, MessageSource messageSource) {

        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();

        String statusCode;

        if (code.equals("E0000") ){
            statusCode = "0000";
        }else if (code.equals("E3003")){
            statusCode = "7112";
        }else if (code.equals("E3055")){
            statusCode = "7112";
        }else if (code.equals("E3047")){
            statusCode = "7127";
        }else if (code.equals("E3052")){
            statusCode = "7127";
        }else if (code.equals("E3056")){
            statusCode = "7109";
        }else if (code.equals("E3057")){
            statusCode = "7111";
        }else {
            if(requestHeader.getServiceType().equals("Refund"))
                statusCode = "7138";
            else if(requestHeader.getServiceType().equals("SingleOrderQuery"))
                statusCode = "7141";
            else
                statusCode ="9998";
        }
        responseGeneralHeader.setStatusCode(statusCode);
        responseGeneralHeader.setStatusDesc(messageSource.getMessage("response."+statusCode, null, locale));
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        responseGeneralHeader.setMchId(integrateMchId);
        return responseGeneralHeader;
    }

    public IntegratedRefundResponseData getIntegratedRefundResponseData(JsonObject headerJson, RequestHeader requestHeader, TradeDetail tradeDetail, IntegratedRefundRequestData integratedRefundRequestData) {
        IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();
        String code =headerJson.get("code").getAsString();

        if (code.equals("E0000")) {
            JsonObject dataJson = JsonUtil.convertJsonPrimitiveToJsonObject(headerJson.getAsJsonPrimitive("responseData"));
            integratedRefundResponseData.setStoreOrderNo(tradeDetail.getOrderId());
            integratedRefundResponseData.setStoreRefundNo(integratedRefundRequestData.getOutRefundNo());
            integratedRefundResponseData.setSysRefundNo(dataJson.get("refundTXID").getAsString());
            integratedRefundResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
            integratedRefundResponseData.setRefundedAt(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        }

        return integratedRefundResponseData;

    }
}
