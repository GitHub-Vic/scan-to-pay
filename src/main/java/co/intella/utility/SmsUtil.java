package co.intella.utility;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class SmsUtil {
	
	Logger LOGGER = LoggerFactory.getLogger(getClass());
	 
    @Value("${sms.url}")
	private String smsUrl;
	 
	public boolean sendSms(String dstaddr, String smbody) throws Exception {
        Map<String, String> map = new HashMap<String, String>();
        LOGGER.info("dstaddr = " + dstaddr + ", smbody = " + smbody);
        map.put("dstaddr", dstaddr);
        map.put("smbody", smbody);
        LOGGER.info("smsUrl = " + smsUrl);
        String responseString = HttpUtil.post(smsUrl, new ObjectMapper().writeValueAsString(map));
        LOGGER.info("response = " + responseString);
        JSONObject responseJson = new JSONObject(responseString);
        if (responseJson.getString("msgCode").equals("000000")) {
            return true;
        } else {
            return false;
        }
    }
}
