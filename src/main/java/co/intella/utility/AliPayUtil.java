package co.intella.utility;

import co.intella.domain.ali.AliResponseBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.model.RequestHeader;
import org.joda.time.DateTime;
import org.springframework.context.MessageSource;

import java.util.Locale;

/**
 * @author Miles
 */
public class AliPayUtil {

    private Locale locale =new Locale("zh_TW");

    public ResponseGeneralHeader getAliResponseHeader(AliResponseBody result, RequestHeader requestHeader,MessageSource messageSource) {

        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        if("F".equals(result.getStatus())) {
            if (result.getErrorCode().equals("2")){
                responseGeneralHeader.setStatusCode("9999");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.9999" , null, locale));
            }
            else if (result.getErrorCode().equals("4")){
                responseGeneralHeader.setStatusCode("8003");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.8003" , null, locale));

            }
            else if (result.getErrorCode().equals("5")){
                responseGeneralHeader.setStatusCode("8004");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.8004" , null, locale));

            }
            else if (result.getErrorCode().equals("6")){
                responseGeneralHeader.setStatusCode("8005");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.8005" , null, locale));

            }
            else if (result.getErrorCode().equals("7")){
                responseGeneralHeader.setStatusCode("8006");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.8006" , null, locale));

            }
            else if (result.getErrorCode().equals("8")){
                responseGeneralHeader.setStatusCode("8007");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.8007" , null, locale));

            }
            else if (result.getErrorCode().equals("9")){
                responseGeneralHeader.setStatusCode("8008");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.8008" , null, locale));

            }
            else if (result.getErrorCode().equals("10")){
                responseGeneralHeader.setStatusCode("8009");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.8009" , null, locale));

            }
            else if (result.getErrorCode().equals("12")){
                responseGeneralHeader.setStatusCode("8011");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.8011" , null, locale));

            }
            else if (result.getErrorCode().equals("13")){
                responseGeneralHeader.setStatusCode("8012");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.8012" , null, locale));

            }
            else if (result.getErrorCode().equals("14")){
                responseGeneralHeader.setStatusCode("8013");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.8013" , null, locale));

            }else if (result.getErrorCode().equals("200")){
                responseGeneralHeader.setStatusCode("7002");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7002" , null, locale));

            }
            else if (result.getErrorCode().equals("203")){
                responseGeneralHeader.setStatusCode("7134");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7134" , null, locale));

            }else if (result.getErrorCode().equals("204")){
                responseGeneralHeader.setStatusCode("7146");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7146" , null, locale));

            }else if (result.getErrorCode().equals("203")){
                responseGeneralHeader.setStatusCode("7134");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7134" , null, locale));

            }else if (result.getErrorCode().equals("206")){
                responseGeneralHeader.setStatusCode("7132");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7132" , null, locale));

            }else if (result.getErrorCode().equals("207")){
                responseGeneralHeader.setStatusCode("7130");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7130" , null, locale));

            }else if (result.getErrorCode().equals("209")){
                responseGeneralHeader.setStatusCode("7127");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7127" , null, locale));

            }else if (result.getErrorCode().equals("210")){
                responseGeneralHeader.setStatusCode("7160");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7160" , null, locale));

            }else if (result.getErrorCode().equals("211")){
                responseGeneralHeader.setStatusCode("7125");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7125" , null, locale));

            }else if (result.getErrorCode().equals("220")){
                responseGeneralHeader.setStatusCode("7110");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7110" , null, locale));

            }else if (result.getErrorCode().equals("207")){
                responseGeneralHeader.setStatusCode("7130");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7130" , null, locale));

            }else if (result.getErrorCode().equals("207")){
                responseGeneralHeader.setStatusCode("7130");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7130" , null, locale));

            }
            else {
                responseGeneralHeader.setStatusCode(SystemInstance.STATUS_FAIL);
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.9997" , null, locale));

            }
        }
        else{
            responseGeneralHeader.setStatusCode(SystemInstance.STATUS_SUCCESS);
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000" , null, locale));

        }

        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setMethod(requestHeader.getMethod());
        responseGeneralHeader.setMchId(requestHeader.getMerchantId());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        return responseGeneralHeader;
    }

}
