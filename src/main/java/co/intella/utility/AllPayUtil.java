package co.intella.utility;

import co.intella.domain.allpay.AllpayBasicResponseData;
import co.intella.domain.allpay.AllpayMicropayRequestData;
import co.intella.domain.allpay.AllpayOLCreditRefundResponse;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.model.RequestHeader;
import com.google.gson.Gson;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.env.Environment;

import java.util.Locale;

/**
 * @author Miles
 */
public class AllPayUtil {


    private Locale locale = new Locale("zh_TW");

    public ResponseGeneralHeader getAllpayResponseHeader(AllpayBasicResponseData result, RequestHeader requestHeader, MessageSource messageSource) {

        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        if (!"1".equals(result.getRtnCode())) {
            if (result.getRtnCode().equals("10100001")) {
                responseGeneralHeader.setStatusCode("7158");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7158", null, locale));
            } else if (result.getRtnCode().equals("10100002")) {
                responseGeneralHeader.setStatusCode("9998");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.9998", null, locale));

            } else if (result.getRtnCode().equals("10100002")) {
                responseGeneralHeader.setStatusCode("7138");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7138", null, locale));

            } else if (result.getRtnCode().equals("10100020")) {
                responseGeneralHeader.setStatusCode("7342");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7342", null, locale));

            } else if (result.getRtnCode().equals("10100050")) {
                responseGeneralHeader.setStatusCode("8002");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.8002", null, locale));

                //responseGeneralHeader.setStatusDesc("Parameter Error.");
            } else if (result.getRtnCode().equals("10100052")) {
                responseGeneralHeader.setStatusCode("7132");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7132", null, locale));

            } else if (result.getRtnCode().equals("10100054")) {
                responseGeneralHeader.setStatusCode("7000");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7000", null, locale));

            } else if (result.getRtnCode().equals("10100055")) {
                responseGeneralHeader.setStatusCode("7142");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7142", null, locale));

            } else if (result.getRtnCode().equals("10100056")) {
                responseGeneralHeader.setStatusCode("7135");
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7135", null, locale));

            } else {
                responseGeneralHeader.setStatusCode(SystemInstance.STATUS_FAIL);
                responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.9997", null, locale));

            }
        } else {
            responseGeneralHeader.setStatusCode(SystemInstance.STATUS_SUCCESS);
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));

        }

        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setMethod(requestHeader.getMethod());
        responseGeneralHeader.setMchId(requestHeader.getMerchantId());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        return responseGeneralHeader;
    }

    public ResponseGeneralHeader getAllpayInAppResponseHeader(AllpayMicropayRequestData result, RequestHeader requestHeader, MessageSource messageSource) {

        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        responseGeneralHeader.setStatusCode("0000");
        responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));

        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setMethod(requestHeader.getMethod());
        responseGeneralHeader.setMchId(requestHeader.getMerchantId());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        return responseGeneralHeader;

    }

    public ResponseGeneralHeader getAllpayAioRefundResponseHeader(String  result, RequestHeader requestHeader, MessageSource messageSource) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        if (result.startsWith("1")){
            responseGeneralHeader.setStatusCode(SystemInstance.STATUS_SUCCESS);
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        }
        else {
            String[] temp = result.split("-");
            String errCode = temp[0].split("\\|")[1];

            responseGeneralHeader.setStatusCode(errCode);
            responseGeneralHeader.setStatusDesc(temp[1]);

        }

        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setMethod(requestHeader.getMethod());
        responseGeneralHeader.setMchId(requestHeader.getMerchantId());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        return responseGeneralHeader;

    }
    public ResponseGeneralHeader getAllpayOLCreditRefundResponseHeader(String  result, RequestHeader requestHeader, MessageSource messageSource) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        AllpayOLCreditRefundResponse allpayOLCreditRefundResponse = new Gson().fromJson(result,AllpayOLCreditRefundResponse.class);
        if (allpayOLCreditRefundResponse.getRtnCode().equals("1")){
            responseGeneralHeader.setStatusCode(SystemInstance.STATUS_SUCCESS);
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        }
        else {
            responseGeneralHeader.setStatusCode(SystemInstance.STATUS_FAIL);
            responseGeneralHeader.setStatusDesc(allpayOLCreditRefundResponse.getRtnMsg());

        }

        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setMethod(requestHeader.getMethod());
        responseGeneralHeader.setMchId(requestHeader.getMerchantId());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        return responseGeneralHeader;

    }
}