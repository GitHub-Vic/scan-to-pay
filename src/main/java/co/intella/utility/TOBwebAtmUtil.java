package co.intella.utility;

import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.domain.tobWebAtm.TOBwebATMCreateOrderResponseData;
import co.intella.domain.tobWebAtm.TOBwebATMQueryResponse;
import co.intella.domain.tsbank.TSBOtherResponseData;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Locale;


public class TOBwebAtmUtil {

    private Locale locale =new Locale("zh_TW");

    public ResponseGeneralHeader getTobWebATMQueryResponseHeader(TOBwebATMQueryResponse tOBwebATMQueryRes, MessageSource messageSource) {
        ResponseGeneralHeader responseHeader = new ResponseGeneralHeader();
        if (tOBwebATMQueryRes.getrC().equals("0")) {
            responseHeader.setStatusCode("0000");
            responseHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        } else  {
            responseHeader.setStatusCode("9998");
            responseHeader.setStatusDesc(messageSource.getMessage("response.9998", null, locale));
        }

        return responseHeader;
    }

    //    public static void main(String[] args) throws Exception {
//
//
//        String hashkey = "mspma7bh";
//
//        String strFront=hashkey.substring(0,4);
//        String strBack=hashkey.substring(4);
//
//        System.out.println(strFront+hashkey+strBack);
//
//
//        String enkey = "A000120191209000000020000983004666667100";
//        String sha256 = sha(enkey);
//        AES des = new AES("1234123456785678");//自定義密鑰
//        System.out.println("加密前的字符：" + enkey);
//        System.out.println("加密後的字符：" + sha256);
//        System.out.println("加密後的字符：" + des.encrypt(sha256, "0000000000000000"));
//        Base64.Encoder encoder = Base64.getEncoder();
//        byte[] encode = encoder.encode(des.hex2Byte(des.encrypt(sha256, "0000000000000000")));
//        String ascii = new String(encode, "ASCII");
//        System.out.println("加密後的字符：" + ascii);
//        System.out.println("sucess");
//    }
    public String queryRequestEnMethod(String oNO, String mID, String hashkey) throws Exception {

        String enkey = mID + oNO;
        String sha256 = sha(enkey);

        String strFront = hashkey.substring(0, 4);
        String strBack = hashkey.substring(4);


        AES des = new AES(strFront + hashkey + strBack);//自定義密鑰

        Base64.Encoder encoder = Base64.getEncoder();
        byte[] encode = encoder.encode(des.hex2Byte(des.encrypt(sha256, "0000000000000000")));
        String ascii = new String(encode, "ASCII");

        return ascii;
    }

    public String requestEnMethod(String mID, String oNO, String accIdTo, Integer curAmt, String hashkey) throws Exception {
        String s = Integer.toString(curAmt);
        String enkey = mID + oNO + accIdTo + s;
        String sha256 = sha(enkey);

        String strFront = hashkey.substring(0, 4);
        String strBack = hashkey.substring(4);


        AES des = new AES(strFront + hashkey + strBack);//自定義密鑰

        Base64.Encoder encoder = Base64.getEncoder();
        byte[] encode = encoder.encode(des.hex2Byte(des.encrypt(sha256, "0000000000000000")));
        String ascii = new String(encode, "ASCII");

        return ascii;
    }

    public String responseEnMethod(TOBwebATMCreateOrderResponseData responseData, String hashKey) throws Exception {

        String enkey = responseData.getrC() +
                responseData.getmID() +
                responseData.getoNO() +
                responseData.getAccIdTo() +
                responseData.getCurAmt();
        String sha256 = sha(enkey);

        String strFront = hashKey.substring(0, 4);
        String strBack = hashKey.substring(4);


        AES des = new AES(strFront + hashKey + strBack);//自定義密鑰

        Base64.Encoder encoder = Base64.getEncoder();
        byte[] encode = encoder.encode(des.hex2Byte(des.encrypt(sha256, "0000000000000000")));
        String ascii = new String(encode, "ASCII");

        return ascii;
    }

    public String queryResponseEnMethod(TOBwebATMQueryResponse responseData, String hashKey) throws Exception {

        String enkey = responseData.getrC() +
                responseData.getmID() +
                responseData.getoNO() +
                responseData.getAccIdTo() +
                responseData.getCurAmt();
        String sha256 = sha(enkey);

        String strFront = hashKey.substring(0, 4);
        String strBack = hashKey.substring(4);


        AES des = new AES(strFront + hashKey + strBack);//自定義密鑰

        Base64.Encoder encoder = Base64.getEncoder();
        byte[] encode = encoder.encode(des.hex2Byte(des.encrypt(sha256, "0000000000000000")));
        String ascii = new String(encode, "ASCII");

        return ascii;
    }

    //    static String sha(String sha) {
//        String sha256 = "";
//        // With the java libraries
//        try {
//            MessageDigest digest = MessageDigest.getInstance("SHA-256");
//            digest.reset();
//            digest.update(sha.getBytes("utf8"));
//            sha256 = String.format("%040x", new BigInteger(1, digest.digest()));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return sha256.toUpperCase() ;
//    }
    /*
    上面長度不一致，改用下面方法
     */
    private static String sha(String str) {
        MessageDigest messageDigest;
        String encdeStr = "";
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
            byte[] hash = messageDigest.digest(str.getBytes("UTF-8"));
            encdeStr = Hex.encodeHexString(hash);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return encdeStr;
    }
}

class AES {
    private final Logger LOGGER = LoggerFactory.getLogger(AES.class);
    private static String strDefaultKey = null; //預設的金鑰
    private StringBuffer objSb = null;
    private Cipher objCipher = null;
    private Key objKey = null;
    private int intStringLength = 0;
    private int intTemp = 0;

    //預設建構子
    public AES() throws Exception {
        this(strDefaultKey);
    }

    //自訂密鑰
    public AES(String arg_strKey) throws Exception {

        byte[] bytes = arg_strKey.getBytes();
        setKey(bytes);
        objCipher = Cipher.getInstance("AES/CBC/NoPadding");
    }

    private void setKey(byte[] arg_strPrivateKey) throws Exception {
        SecretKeySpec secretKeySpec = new SecretKeySpec(arg_strPrivateKey, "AES");
        // 設定密鑰
        objKey = secretKeySpec;
    }

    //將byte陣列轉換16進制值的字串，如：byte[]{1,18}轉換為：0112
    public String byte2Hex(byte[] arg_bteArray) throws Exception {
        intStringLength = arg_bteArray.length;
        objSb = new StringBuffer(intStringLength * 2);
        for (int i = 0; i < intStringLength; i++) {
            intTemp = (int) arg_bteArray[i];
            //負數需要轉成正數
            if (intTemp < 0) {
                intTemp = intTemp + 256;
            }
            // 小於0F需要補0
            if (intTemp < 16) {
                objSb.append("0");
            }
            objSb.append(Integer.toString(intTemp, 16));
        }
        return objSb.toString();
    }

    //將16進制值的字串轉成byte陣列
    /*
    改用GeneralUtil做hex2Byte
     */
//    public byte[] hex2Byte(String arg_strHexString) throws Exception {
//        byte[] arrByteDAta = arg_strHexString.getBytes();
//        intStringLength = arrByteDAta.length;
//        byte[] aryRetuenData = new byte[intStringLength / 2];
//        for (int i = 0; i < intStringLength; i = i + 2) {
//            aryRetuenData[i / 2] = (byte) Integer.parseInt(new String(arrByteDAta, i, 2), 16);
//        }
//        return aryRetuenData;
//    }
    public byte[] hex2Byte(String hexString) {
//        LOGGER.info("[hexString] :"+ hexString);
//        LOGGER.info("[hexString.length] :"+ hexString.length());
        byte[] bytes = new byte[hexString.length() / 2];
        for (int i = 0; i < bytes.length; i++)
            bytes[i] = (byte) Integer.parseInt(hexString.substring(2 * i, 2 * i + 2), 16);
        return bytes;
    }

    //加密字串
    public byte[] doEncrypt(byte[] arg_bteArray, String ivString) throws Exception {
        byte[] iv = ivString.getBytes();
        byte[] arrTempByteArray = new byte[16];
// 將原始字元陣列轉換為8位
        for (int i = 0; i < iv.length && i < arrTempByteArray.length; i++) {
            arrTempByteArray[i] = iv[i];
        }

        IvParameterSpec ivKey = new IvParameterSpec(arrTempByteArray);
        objCipher.init(Cipher.ENCRYPT_MODE, objKey, ivKey);
        return objCipher.doFinal(arg_bteArray);
    }

    public String encrypt(String arg_strToEncriptString, String ivString) throws Exception {
        return byte2Hex(doEncrypt(hex2Byte(arg_strToEncriptString), ivString));
    }

}

