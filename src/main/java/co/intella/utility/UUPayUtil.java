package co.intella.utility;

import co.intella.domain.integration.ResponseGeneralBody;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.model.PaymentAccount;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.joda.time.DateTime;
import org.springframework.context.MessageSource;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

public class UUPayUtil {
    private static final String ALG = "HmacSHA256";
    private static Locale locale = new Locale("zh_TW");

    /**
     * 悠遊付 訂單狀態
     *
     * @param uupayOrderStatus
     * @return
     */
    public static String transformUUpayStatus(String uupayOrderStatus) {
//TODO  退款失敗 什麼狀態？
        String result = SystemInstance.TRADE_WAITING;

        switch (uupayOrderStatus) {
            case "PAYMENT_RECEIVED": //已付款
            case "COMPLETED": //已完成
                result = SystemInstance.TRADE_SUCCESS;
                break;
            case "ORDER_VOID":  //訂單逾期
            case "PREORDER_VOID":  //訂單逾期已作廢
            case "TRADE_FAIL":  //交易失敗
                result = SystemInstance.TRADE_FAIL;
                break;
            case "SELLER_CANCELLED":    //TODO  取消交易 == 退款？
            case "REFUND_PROCESSING":
            case "REFUND_COMPLETED":    //退款完成
                result = SystemInstance.TRADE_REFUND_SUCCESS;
                break;
//            case "SELLER_CANCELLED":  //賣家已取消   //TODO  取消交易 == 退款？
//                result = SystemInstance.TRADE_CANCEL_SUCCESS;
//                break;
        }
        return result;
    }


    public static String encyption(String json, PaymentAccount paymentAccount) throws NoSuchAlgorithmException, InvalidKeyException, org.apache.commons.codec.DecoderException, UnsupportedEncodingException {
        String encodeStr = getSign(json, paymentAccount);
        StringBuilder sb = new StringBuilder();
        encodeStr = sb.append("HmacSHA256 ").append(paymentAccount.getAccount()).append(";1;").append(encodeStr).toString();

        return encodeStr;
    }

    public static String getSign(String json, PaymentAccount paymentAccount) throws NoSuchAlgorithmException, InvalidKeyException, DecoderException, UnsupportedEncodingException {
        byte[] input = json.getBytes("ISO-8859-1");
        byte[] scId = Hex.decodeHex(paymentAccount.getHashKey().toCharArray());

        Mac sha256Hmac = Mac.getInstance(ALG);
        SecretKeySpec secretCode = new SecretKeySpec(scId, ALG);
        sha256Hmac.init(secretCode);
        byte[] mac = sha256Hmac.doFinal(input);
        String encodeStr = DatatypeConverter.printHexBinary(mac).toLowerCase();

        return encodeStr;
    }

    public static ResponseGeneralBody getResponseBody(RequestHeader requestHeader, TradeDetail tradeDetail, String returnCode, MessageSource messageSource) {
        ResponseGeneralBody responseGeneralBody = new ResponseGeneralBody();
        ResponseGeneralHeader header = new ResponseGeneralHeader();

        String intellaCode = transformUUpayReturnCode(returnCode);
        header.setStatusCode(intellaCode);
        header.setStatusDesc(messageSource.getMessage("response." + intellaCode, null, locale));
        header.setMethod(tradeDetail.getMethod());
        header.setMchId(tradeDetail.getPaymentAccount().getMerchant().getAccountId());
        header.setServiceType(requestHeader.getServiceType());
        header.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        responseGeneralBody.setHeader(header);

        return responseGeneralBody;
    }

    private static String transformUUpayReturnCode(String returnCode) {
        String result = "9998";

        switch (returnCode) {
            case "00000":
                result = "0000";
                break;
            case "20121":
            case "20122":
                result = "7120";
                break;
            case "20120":
                result = "7123";
                break;
            case "20132":
                result = "7002";
                break;
        }

        return result;
    }

    public static void main(String[] args) throws Exception {
        PaymentAccount paymentAccount = new PaymentAccount();
        paymentAccount.setHashKey("6b87baeb124bb5ed5d531ddc5cdd3727532ebf45c2e81b0b19270e5afc13c7d2");
        paymentAccount.setAccount("129438503938");

        System.err.println(UUPayUtil.encyption("{\"a\":\"1\",\"b\":\"2\"}", paymentAccount));
    }
}
