package co.intella.utility;

import co.intella.domain.aipei.AipeiPaymentResponseData;
import co.intella.domain.aipei.AipeiPaymentResponseParaData;
import co.intella.domain.integration.IntegratedRefundRequestData;
import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.IntegratedSingleOrderQueryResponseData;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import com.google.gson.Gson;
import org.joda.time.DateTime;
import org.springframework.context.MessageSource;

import java.util.Locale;

/**
 * @author Alex
 */

public class AipeiUtil {
    private Locale locale = new Locale("zh_TW");

    public ResponseGeneralHeader getResponseHeader(String errorCode, String data, RequestHeader requestHeader, MessageSource messageSource) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();
        if (errorCode.equals("")) {

            responseGeneralHeader.setStatusCode("0000");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));

        } else if (errorCode.equals("INVALID_BARCODE")) {
            responseGeneralHeader.setStatusCode("7120");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7120", null, locale));
        } else {
            responseGeneralHeader.setStatusCode("9998");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.9998", null, locale));
        }


        responseGeneralHeader.setMethod("11500");
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));

        return responseGeneralHeader;
    }


    public IntegratedSingleOrderQueryResponseData getIntegratedSingleOrderQueryResponseData(AipeiPaymentResponseData aipeiPaymentResponseData, TradeDetail tradeDetail) {
        IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();

        if (aipeiPaymentResponseData.getErrorCode().equals("")) {
            AipeiPaymentResponseParaData data = new Gson().fromJson(aipeiPaymentResponseData.getData(), AipeiPaymentResponseParaData.class);
            integratedSingleOrderQueryResponseData.setStoreOrderNo(tradeDetail.getOrderId());
            integratedSingleOrderQueryResponseData.setSysOrderNo("");
            integratedSingleOrderQueryResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
            integratedSingleOrderQueryResponseData.setPaidAt(tradeDetail.getCreateDate());
            integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());
            integratedSingleOrderQueryResponseData.setRefundedMsg(tradeDetail.getRefundStatus());

            if (data.getApTransResult().equals("SUCCESSFUL")) {
                integratedSingleOrderQueryResponseData.setSysOrderNo(data.getApTransId());
                integratedSingleOrderQueryResponseData.setFeeType(SystemInstance.CURRENCY_TWD);
                integratedSingleOrderQueryResponseData.setTotalFee(String.valueOf(data.getTransAmount() / 100));

                integratedSingleOrderQueryResponseData.setOrderStatus("1");

            } else if (data.getApTransResult().equals("UNKNOWN")) {
                integratedSingleOrderQueryResponseData.setOrderStatus("0");

            } else if (data.getApTransResult().equals("REFUNDED")) {

                integratedSingleOrderQueryResponseData.setSysOrderNo(data.getApTransId());
                integratedSingleOrderQueryResponseData.setFeeType(SystemInstance.CURRENCY_TWD);
                integratedSingleOrderQueryResponseData.setTotalFee(String.valueOf(data.getTransAmount() / 100));
                integratedSingleOrderQueryResponseData.setOrderStatus("3");

            }
        }
        return integratedSingleOrderQueryResponseData;

    }

    public IntegratedRefundResponseData getIntegratedRefundResponseData(String dataString, RequestHeader requestHeader, TradeDetail tradeDetail, IntegratedRefundRequestData integratedRefundRequestData) {
        AipeiPaymentResponseParaData data = new Gson().fromJson(dataString, AipeiPaymentResponseParaData.class);
        IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();

        if (data.getApTransResult().equals("REFUNDED")) {
            integratedRefundResponseData.setStoreOrderNo(tradeDetail.getOrderId());
            integratedRefundResponseData.setStoreRefundNo(integratedRefundRequestData.getOutRefundNo());
            integratedRefundResponseData.setSysRefundNo(data.getApTransId());
            integratedRefundResponseData.setSysOrderNo(tradeDetail.getSystemOrderId());
            integratedRefundResponseData.setRefundedAt(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        }
        return integratedRefundResponseData;
    }
}
