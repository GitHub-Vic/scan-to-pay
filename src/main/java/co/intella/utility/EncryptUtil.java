package co.intella.utility;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;


public class EncryptUtil {

    private final static String UTF_8_ENCODING = StandardCharsets.UTF_8.toString();
    private final static String SEED = "96e01e079f95v2";

    public static String encryptSimple(String context) {

        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(SEED);
        try {
            return Base64.getEncoder().encodeToString(encryptor.encrypt(context).getBytes(UTF_8_ENCODING));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return context;
        }
    }

    public static String decryptSimple(String context) {

        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(SEED);
        try {
            byte[] asBytes = Base64.getDecoder().decode(context);
            return encryptor.decrypt(new String(asBytes, UTF_8_ENCODING));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return context;
        }
    }
}
