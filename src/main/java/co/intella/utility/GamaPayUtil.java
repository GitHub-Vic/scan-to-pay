package co.intella.utility;

import co.intella.domain.gama.GamaResponseData;
import co.intella.domain.integration.IntegratedRefundRequestData;
import co.intella.domain.integration.IntegratedRefundResponseData;
import co.intella.domain.integration.IntegratedSingleOrderQueryResponseData;
import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.model.RequestHeader;
import co.intella.model.TradeDetail;
import org.joda.time.DateTime;
import org.springframework.context.MessageSource;

import java.util.Locale;

/**
 * @author Alex
 */

public class GamaPayUtil {
    private Locale locale = new Locale("zh_TW");

    public ResponseGeneralHeader getResponseHeader(String returnCode, String statusCode, RequestHeader requestHeader,
                                                   MessageSource messageSource) {
        ResponseGeneralHeader responseGeneralHeader = new ResponseGeneralHeader();

        if (returnCode.equals("0000")) {

            responseGeneralHeader.setStatusCode("0000");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.0000", null, locale));
        } else if (returnCode.equals("0003")) {
            responseGeneralHeader.setStatusCode("8002");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.8002", null, locale));
        } else if (returnCode.equals("0006")) {
            responseGeneralHeader.setStatusCode("9999");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.9999", null, locale));
        } else if (returnCode.equals("0007")) {
            responseGeneralHeader.setStatusCode("7158");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7158", null, locale));
        } else if (returnCode.equals("1203") || returnCode.equals("1204")) {
            responseGeneralHeader.setStatusCode("8007");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.8007", null, locale));
        } else if (returnCode.equals("2001")) {
            responseGeneralHeader.setStatusCode("7160");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7160", null, locale));
        } else if (returnCode.equals("2007")) {
            responseGeneralHeader.setStatusCode("7323");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7323", null, locale));
        } else if (returnCode.equals("2011")) {
            responseGeneralHeader.setStatusCode("7130");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7130", null, locale));
        } else if (returnCode.equals("2012")) {
            responseGeneralHeader.setStatusCode("7120");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7120", null, locale));
        } else if (returnCode.equals("2013")) {
            responseGeneralHeader.setStatusCode("7313");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7313", null, locale));
        } else if (returnCode.equals("2015")) {
            responseGeneralHeader.setStatusCode("7126");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.7126", null, locale));
        } else {
            responseGeneralHeader.setStatusCode("9998");
            responseGeneralHeader.setStatusDesc(messageSource.getMessage("response.9998", null, locale));
        }
        responseGeneralHeader.setMethod("11300");
        responseGeneralHeader.setServiceType(requestHeader.getServiceType());
        responseGeneralHeader.setResponseTime(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        return responseGeneralHeader;
    }

    public IntegratedRefundResponseData getIntegratedRefundResponseData(GamaResponseData gamaResponseData,
                                                                        RequestHeader requestHeader, TradeDetail tradeDetail,
                                                                        IntegratedRefundRequestData integratedRefundRequestData) {
        IntegratedRefundResponseData integratedRefundResponseData = new IntegratedRefundResponseData();

        if (gamaResponseData.getResultCode().equals("0000")) {
            integratedRefundResponseData.setStoreOrderNo(gamaResponseData.getMerchantOrderId());
            integratedRefundResponseData.setStoreRefundNo(tradeDetail.getStoreRefundId());
            integratedRefundResponseData.setSysRefundNo(gamaResponseData.getTransactionId());
            integratedRefundResponseData.setRefundedAt(DateTime.now().toString(SystemInstance.DATE_PATTERN));
        }

        return integratedRefundResponseData;
    }

    public IntegratedSingleOrderQueryResponseData getIntegratedSingleOrderQueryResponseData(
            GamaResponseData gamaResponseData, TradeDetail tradeDetail) {
        IntegratedSingleOrderQueryResponseData integratedSingleOrderQueryResponseData = new IntegratedSingleOrderQueryResponseData();

        if (gamaResponseData.getResultCode().equals("0000")) {

            integratedSingleOrderQueryResponseData.setStoreOrderNo(gamaResponseData.getMerchantOrderId());
            integratedSingleOrderQueryResponseData.setSysOrderNo(gamaResponseData.getTransactionId());
            integratedSingleOrderQueryResponseData.setFeeType(gamaResponseData.getCurrencyCode());
            integratedSingleOrderQueryResponseData.setBody(tradeDetail.getDescription());
            integratedSingleOrderQueryResponseData.setRefundedMsg(tradeDetail.getRefundStatus());
            integratedSingleOrderQueryResponseData.setTotalFee(String.valueOf(tradeDetail.getPayment()));
            integratedSingleOrderQueryResponseData.setPaidAt(tradeDetail.getCreateDate());

            if (gamaResponseData.getStatusCode().equals("100")) {
                integratedSingleOrderQueryResponseData.setOrderStatus("1");
            } else if (gamaResponseData.getStatusCode().equals("0") || gamaResponseData.getStatusCode().equals("1")) {
                integratedSingleOrderQueryResponseData.setOrderStatus("0");

            } else if (gamaResponseData.getStatusCode().equals("200")) {
                integratedSingleOrderQueryResponseData.setOrderStatus("3");
                integratedSingleOrderQueryResponseData.setRefundedAt(gamaResponseData.getTransDate());

            } else {
                integratedSingleOrderQueryResponseData.setOrderStatus("2");
            }

        } else {
        }
        return integratedSingleOrderQueryResponseData;
    }
}
