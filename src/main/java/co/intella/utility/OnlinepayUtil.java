package co.intella.utility;

import co.intella.model.DiscountTrade;
import co.intella.model.DiscountTradeOLPay;
import co.intella.model.QrcodeParameter;
import co.intella.service.DiscountService;
import co.intella.service.DiscountTradeOLPayService;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @author Alex
 */
@Component
public class OnlinepayUtil {

    @Resource
    private DiscountService discountService;

    @Resource
    private DiscountTradeOLPayService discountTradeOLPayService;

    public double getTradeFee(QrcodeParameter qrcodeParameter, String method) {

        if (qrcodeParameter.getOnSaleStart() != null && qrcodeParameter.getOnSaleEnd() != null) {
            DateTime startDate = DateTime.parse(qrcodeParameter.getOnSaleStart(), DateTimeFormat.forPattern("yyyyMMdd"));
            DateTime endDate = DateTime.parse(qrcodeParameter.getOnSaleEnd(), DateTimeFormat.forPattern("yyyyMMdd"));
            if (startDate.isBeforeNow() && endDate.plusDays(1).isAfterNow()) {
                return qrcodeParameter.getOnSaleTotalFee();
            }
        }
        double amount = qrcodeParameter.getTotalFee();
//        if (qrcodeParameter.getOnSaleTotalFee() == 0 && StringUtils.isNotBlank(method)) {
            DiscountTrade discountTrade = discountService.checkoutTotalFree(qrcodeParameter.getMchId(),
                    Double.valueOf(qrcodeParameter.getTotalFee()).longValue(), method);

            if (Objects.nonNull(discountTrade)) {
                DiscountTradeOLPay discountTradeOLPay = new DiscountTradeOLPay();
                BeanUtils.copyProperties(discountTrade, discountTradeOLPay, "discountTradeSeq");
                discountTradeOLPay.setShortId(qrcodeParameter.getShortId());
                discountTradeOLPayService.save(discountTradeOLPay);
                amount = discountTrade.getTradeAmount().doubleValue();
            }
//        }

        return amount;
    }

    public static boolean checkQrcodeExpired(QrcodeParameter qrcodeParameter) {
        if (qrcodeParameter.getCodeType().equals("4")) {
            if (DateTime.parse(qrcodeParameter.getCreateDate(), DateTimeFormat.forPattern(SystemInstance.DATE_PATTERN)).isBeforeNow()) {
                return true;
            }
        }
        return false;
    }
}
