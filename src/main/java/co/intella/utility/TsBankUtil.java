package co.intella.utility;

import co.intella.domain.integration.ResponseGeneralHeader;
import co.intella.domain.tsbank.TSBCreditCardResponseData;
import co.intella.domain.tsbank.TSBOtherResponseData;
import org.springframework.context.MessageSource;

import java.util.Locale;

/**
 * @author Alex
 */

public class TsBankUtil {

    private Locale locale =new Locale("zh_TW");

    public ResponseGeneralHeader getTsbankOtherResponseHeader(TSBOtherResponseData tsResponse,MessageSource messageSource) {
        ResponseGeneralHeader responseHeader = new ResponseGeneralHeader();
        if (tsResponse.getParams().getRetCode().equals("00")) {
            responseHeader.setStatusCode("0000");
            responseHeader.setStatusDesc(messageSource.getMessage("response.0000" , null, locale));
        }else if (tsResponse.getParams().getRetCode().equals("-67")){
            responseHeader.setStatusCode("7316");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7316" , null, locale));
        }
        else if (tsResponse.getParams().getRetCode().equals("82") || tsResponse.getParams().getRetCode().equals("N7")){
            responseHeader.setStatusCode("7347");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7347" , null, locale));
        }
        else if (tsResponse.getParams().getRetCode().equals("14")){
            responseHeader.setStatusCode("7314");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7314" , null, locale));
        }
        else if (tsResponse.getParams().getRetCode().equals("-51")){
            responseHeader.setStatusCode("7127");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7127" , null, locale));
        }
        else if (tsResponse.getParams().getRetCode().equals("-321")){
            responseHeader.setStatusCode("7303");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7303" , null, locale));
        }
        else if (tsResponse.getParams().getRetCode().equals("-300")){
            responseHeader.setStatusCode("7000");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7000" , null, locale));
        }
        else if (tsResponse.getParams().getRetCode().equals("-318")){
            responseHeader.setStatusCode("7133");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7133" , null, locale));
        }
        else if (tsResponse.getParams().getRetCode().equals("-323")){
            responseHeader.setStatusCode("0000");
            responseHeader.setStatusDesc(messageSource.getMessage("response.0000" , null, locale));
        }
        else {
            responseHeader.setStatusCode("9998");
            responseHeader.setStatusDesc(messageSource.getMessage("response.9998" , null, locale));
        }
        return responseHeader;

    }

    public ResponseGeneralHeader getTsbankResponseHeader(TSBCreditCardResponseData tsResponse, MessageSource messageSource) {

        ResponseGeneralHeader responseHeader = new ResponseGeneralHeader();
        if (tsResponse.getParams().getRetCode().equals("00")) {
            responseHeader.setStatusCode("0000");
            responseHeader.setStatusDesc(messageSource.getMessage("response.0000" , null, locale));
        }
        else if (tsResponse.getParams().getRetCode().equals("-67")){
            responseHeader.setStatusCode("7316");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7316" , null, locale));
        }
        else if (tsResponse.getParams().getRetCode().equals("82") || tsResponse.getParams().getRetCode().equals("N7")){
            responseHeader.setStatusCode("7347");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7347" , null, locale));
        }
        else if (tsResponse.getParams().getRetCode().equals("14")){
            responseHeader.setStatusCode("7314");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7314" , null, locale));
        }
        else if (tsResponse.getParams().getRetCode().equals("-51")){
            responseHeader.setStatusCode("7127");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7127" , null, locale));
        }
        else if (tsResponse.getParams().getRetCode().equals("-321")){
            responseHeader.setStatusCode("7303");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7303" , null, locale));
        }
        else if (tsResponse.getParams().getRetCode().equals("-300")){
            responseHeader.setStatusCode("7000");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7000" , null, locale));
        }
        else {
            responseHeader.setStatusCode("9998");
            responseHeader.setStatusDesc(messageSource.getMessage("response.9998" , null, locale));
        }

        return responseHeader;
    }

    public ResponseGeneralHeader getPartiaRefundHeader(String status, MessageSource messageSource) {
        ResponseGeneralHeader responseHeader = new ResponseGeneralHeader();
        if (status.equals("S")) {
            responseHeader.setStatusCode("0000");
            responseHeader.setStatusDesc(messageSource.getMessage("response.0000" , null, locale));
        }
        else {
            responseHeader.setStatusCode("7138");
            responseHeader.setStatusDesc(messageSource.getMessage("response.7138" , null, locale));
        }
        return responseHeader;

    }
}