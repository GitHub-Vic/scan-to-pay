package co.intella.utility;

import java.math.BigInteger;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class GamaPayEncryptAESUtil {

    private static final String ALGORITHM = "AES";
    private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
    private static final Base64.Decoder decoder = Base64.getDecoder();
    private static final Base64.Encoder encoder = Base64.getEncoder();
    private Key key;
    private IvParameterSpec iv;
    private Cipher cipher;

    public GamaPayEncryptAESUtil(String IV, String KEY) {
        this.key = new SecretKeySpec(decoder.decode(KEY), ALGORITHM);
        this.iv = new IvParameterSpec(decoder.decode(IV));
        try {
            cipher = Cipher.getInstance(TRANSFORMATION);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    public String encrypt(final String str) {
        try {
            byte[] bytes = str.getBytes("UTF-16LE");
            return encrypt(bytes);
        } catch (final Exception ex) {

            throw new RuntimeException(ex.getMessage());
        }
    }

    private String encrypt(final byte[] data) {

        try {
            cipher.init(Cipher.ENCRYPT_MODE, key, iv);
            final byte[] encryptData = cipher.doFinal(data);
            return new String(encoder.encode(getHash("SHA-256", encoder.encodeToString(encryptData))), "UTF-8");
        } catch (final Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }

    private static byte[] getHash(final String algorithm, final String text) {
        try {
            return getHash(algorithm, text.getBytes("UTF-8"));
        } catch (final Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }

    private static byte[] getHash(final String algorithm, final byte[] data) {
        try {
            final MessageDigest digest = MessageDigest.getInstance(algorithm);
            digest.update(data);
            return digest.digest();
        } catch (

                final Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }


    public static String getSHAStr(final String algorithm, final String text) {

        String format = "SHA-512".equalsIgnoreCase(algorithm) ? "%0128x" : "%064x";
        return String.format(format, new BigInteger(1, getHash(algorithm, text)));
    }
}
