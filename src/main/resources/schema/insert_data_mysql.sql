INSERT INTO Merchant (name, email, pin) VALUES ('miles', 'miles@intella.co', 'miles');
INSERT INTO Merchant (name, email, pin) VALUES ('benson', 'benson@intella.co', 'benson');
INSERT INTO Merchant (name, email, pin) VALUES ('alex', 'alex@intella.co', 'alex');
INSERT INTO Merchant (name, email, pin) VALUES ('lex', 'lex@intella.co', 'lex');
INSERT INTO Merchant (name, email, pin) VALUES ('andy', 'andy@intella.co', 'andy');

INSERT INTO Bank (name, type) VALUES ('SKB', 'B');
INSERT INTO Bank (name, type) VALUES ('YUANTA', 'B');
INSERT INTO Bank (name, type) VALUES ('PI', 'T');

INSERT INTO Device (deviceId, type) VALUES ('40326e78-f7df-11e6-bc64-92361f002671', 'HB-001');
INSERT INTO Device (deviceId, type) VALUES ('bdbd641e-d749-4741-8701-d7f66e6ef182', 'HB-001');
INSERT INTO Device (deviceId, type) VALUES ('a1fddf29-2a4f-40ca-a5c6-cdfdd830b596', 'HB-001');

INSERT INTO PaymentAccount (paymentAccountId, merchantId, bankId, account) VALUES ('b92ff36d-6f41-48a5-aa18-5c57419c885f', 1, 1, '001-123456789');
INSERT INTO PaymentAccount (paymentAccountId, merchantId, bankId, account) VALUES ('f3135231-5e2c-4686-8d26-105d82784c1d', 1, 2, '002-123456789');
INSERT INTO PaymentAccount (paymentAccountId, merchantId, bankId, account) VALUES ('a691424c-844e-465d-904e-b89082e68891', 2, 2, '003-123456789');

INSERT INTO TradeDetail (id, orderId, paymentAccountId, deviceId, payment) VALUES ('15b7846d-af5b-4b08-bc6a-3748844f3050', '33456789', 'b92ff36d-6f41-48a5-aa18-5c57419c885f', '40326e78-f7df-11e6-bc64-92361f002671', 1000);
INSERT INTO TradeDetail (id, orderId, paymentAccountId, deviceId, payment) VALUES ('63b7aa2e-0292-462c-bbce-6502b50209bc', '33456789', 'b92ff36d-6f41-48a5-aa18-5c57419c885f', 'bdbd641e-d749-4741-8701-d7f66e6ef182', 500);
INSERT INTO TradeDetail (id, orderId, paymentAccountId, deviceId, payment) VALUES ('d90631c8-d8d9-4533-b4c5-59f144ee0d33', '33456789', 'f3135231-5e2c-4686-8d26-105d82784c1d', '40326e78-f7df-11e6-bc64-92361f002671', 10);
