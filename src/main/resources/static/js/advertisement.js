/**
 * Created by Yin-Hsuan Hsieh on 2017/3/16.
 */
// $(function () {
//         $("#test-click").click(function(){
//             $(".content").toggle("fast");
//         });
// });

$(function () {

    $(".card").click(function () {
        var $idis = $(this).children('div').eq(1).attr("id");
        $("#" + $idis).slideToggle();
        $("div.content").not("#" + $idis).slideUp();
    });

});