/**
 * @author Miles
 */

$(function () {

    $('#pay').click(function () {
        console.log('go!');
        var json = {
            cardId:$('#cardId').val(), expireDate:$('#expireDate').val(), extendNo:$('#extendNo').val(),
            orderId:$('#oid').text(), amount:$('#amount').text(), mch:$('#mch').text(), detail:$('#detail').text(), time:$('#time').text(),
            redeem:$('input[name=redeem]:checked').val(), install:$('#install').val()
        };

        $.ajax({
            type: "POST",
            url: "tsbAjaxTest",
            dataType: "json",
            contentType: "application/json",
            success: function (msg) {
                console.log('s', msg);
                window.location.href = msg.url;
            },
            error: function(msg) {
                console.log('e', msg);
            },
            data: JSON.stringify(json)
        });

    });


    $('#Cancel').click(function () {
        console.log('Cancel go!');
        var json = {
            cardId:"Cancel",orderId:$('#refundOrderNo').val()
        };

        $.ajax({
            type: "POST",
            url: "tsbAjaxTest2",
            dataType: "json",
            contentType: "application/json",
            success: function (msg) {
                console.log('s', JSON.stringify(msg));

            },
            error: function(msg) {
                console.log('e', msg);
            },
            data: JSON.stringify(json)
        });

    });

    $('#Query').click(function () {
        console.log('SingleOrderQuery go!');
        var json = {
            cardId:"SingleOrderQuery",orderId:$('#refundOrderNo').val()
        };

        $.ajax({
            type: "POST",
            url: "tsbAjaxTest2",
            dataType: "json",
            contentType: "application/json",
            success: function (msg) {
                console.log('s', JSON.stringify(msg));
                alert(JSON.stringify(msg.params));
            },
            error: function(msg) {
                console.log('e', msg);
            },
            data: JSON.stringify(json)
        });

    });

});