$(function () {
    $('img').bind('contextmenu', function (e) {
        return false;
    });
    var ios = /iphone|ipad/i.test(navigator.userAgent);
    if (ios) {
        document.addEventListener('gesturestart', function (e) {
            e.preventDefault();
        }, false);
    }
    cooperate();
    // keyboard
    var price = "";
    $(".keyboard button").click(function () {
        var zoom = $(this).find(".zoom");
        var topPX = Math.floor($(this).height() / 2 - 45);
        zoom.animate({width: "90px", height: "90px", top: topPX + "px"}, 200)
            .animate({width: "0px", height: "0px", top: "50%"}, 200);

        if (!ios) {
            navigator.vibrate(30);
        }
        var value = $(this).attr("value");
        if (value === "clear") {
            price = "";
            priceAddCommas(price);
        } else if (value === "sure") {
            if ($("#price").val() !== "") {
                // price = $("#price").val();
                $(".all-price-show").text(price);
                $(".cny").text((price / 5).toFixed(2));
                $(".usd").text((price / 5 / 6.85).toFixed(2));
                detectBrowser();
            } else {
                price = "";
                priceAddCommas(price);
            }
        } else if (price === "" && value === "0") {
            price = "";
            priceAddCommas(price);
        } else {
            price = price + value;
            priceAddCommas(price);
        }
        togglePadding(price);
    });

    var timeout, intr;
    $("#return-num").on('touchstart', function (e) {
        price = price.substring(0, price.length - 1);
        priceAddCommas(price);
        togglePadding(price);
        timeout = setTimeout(function () {
            var funcRepeat = function () {
                intr = setInterval(function () {
                    price = price.substring(0, price.length - 1);
                    priceAddCommas(price);
                    if (price.length == 0) {
                        clearInterval(intr);
                        togglePadding(price);
                    }
                }, 150)
            }
            funcRepeat();
        }, 400);
    });
    $("#return-num").on('touchend', function (e) {
        e.preventDefault();
        clearTimeout(timeout);
        clearInterval(intr);
    });

    // allpay
    var date = new Date();
    $(".current-date").text(dateFormat(date));

    $("#checkout_btn").click(function () {
        $("#allpay-infro").hide();
        $("#allpay-check").show();
    });

    $("#back-to-keyboard").click(function () {
        price = "";
        $("#price").val(price);
        togglePadding(price);
        $("#allpay-check").hide();
        $("#allpay").hide();
    });

    $("#show-alert-btn").click(function () {
        $("#allpay-alert").show();
        $("#bg-opacity").show();
    });
    $("#alert-btn-ok").click(function () {
        $("#allpay-check").hide();
        $("#allpay-alert").hide();
        $("#bg-opacity").hide();
        $("#allpay-paid").show();
    });

    $("#alert-btn-cancel").click(function () {
        $("#bg-opacity").hide();
        $("#allpay-alert").hide();
    });

    $("#paid-success-btn").click(function () {
        $("#allpay-paid").hide();
        $("#allpay-success").show();
    });

    // weixin
    $("#wx-order-btn").click(function () {
        $("#wx-loading").show();
        $("#wx-order-btn").hide();
        setTimeout(function () {
            $("#wx-loading").hide();
            $("#anti-theft").show();
            $(".pass-keyin").show();
            $("#wx-bg-opacity").show();
        }, 1000);
    });

    $("#close-anti-theft").click(function () {
        price = "";
        $("#price").val(price);
        togglePadding(price);
        $("#weixin").hide();
        $("#anti-theft").hide();
        $(".pass-keyin").hide();
        $("#wx-bg-opacity").hide();
        $("#wx-order-btn").show();
    });

    $("#disable-btn").prop('disabled', true);

// weixin pass
    var pass = "";
    $(".pass-keyin button.num").click(function () {
        var value = $(this).attr("value");
        pass = pass + value;
        $("#anti-pass-input").val(pass);
        if (pass.length === 6) {
            setTimeout(function () {
                $("#anti-theft").hide();
                $(".pass-keyin").hide();
                $("#wx-bg-opacity").hide();
                $("#wx-order").hide();
                $("#wx-success").show();
            }, 300);
        }
    });
    $("#wx-back-btn").click(function () {
        pass = pass.substring(0, pass.length - 1);
        $("#anti-pass-input").val(pass);
    });

    // ali
    $("#ali-check-btn").click(function () {
        var aliPass = $("#ali-password").val();
        if (aliPass === "") {
            alert("請輸入密碼");
        } else {
            $("#ali-mycounter").hide();
            $("#ali-loading").show();
            setTimeout(function () {
                $("#ali-loading").hide();
                $("#ali-success").show();
            }, 1000);
        }
    });

});

function cooperate() {
    var ua = navigator.userAgent.toLowerCase();
    if (/MicroMessenger/.test(ua)) {
    //     if ( /safari/.test(ua)) {
        $(".header").html('<img class="title-logo" src="/allpaypass/images/title-intella-wx.png" />');
    } else if (/AlipayClient/.test(ua)) {
        $(".header").html('<img class="title-logo" src="/allpaypass/images/title-intella-ali.png" />');
    } else if (/allPay/.test(ua)) {
        $(".header").html('<img class="title-logo" src="/allpaypass/images/title-intella-all.png" />');
    }
}

function detectBrowser() {
    var ua = navigator.userAgent.toLowerCase();
    if (/MicroMessenger/.test(ua)) {
        // if ( /safari/.test(ua)) {
        $("#weixin").show();
        // $("#allpay").show();
        // $("#allpay-infro").show();
    } else if (/AlipayClient/.test(ua)) {
        $("#ali").show();
    } else if (/allPay/.test(ua)) {
        $("#allpay").show();
        $("#allpay-infro").show();
    }
}

function priceAddCommas(num) {
    num = num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ( num === "" ? '' : '.00');
    $("#price").val(num);
    $("#space").html(num);
}

function dateFormat(d) {
    var mm = d.getMonth() + 1;
    var dd = d.getDate();
    var hh = d.getHours();
    var minutes = d.getMinutes();
    return [d.getFullYear(),
            (mm > 9 ? '' : '0') + mm,
            (dd > 9 ? '' : '0') + dd
        ].join('/') + " " +
        (hh > 9 ? '' : '0') + hh + ":" +
        (minutes > 9 ? '' : '0') + minutes;
}

function togglePadding(price) {
    if (price !== "") {
        $("#return-num").show();
    } else {
        $("#return-num").hide();
    }

    if (price.length > 5) {
        $(".optional-price").css("font-size", "28px");
        $(".cursor").css("font-size", "28px");
    } else {
        $(".optional-price").css("font-size", "38px");
        $(".cursor").css("font-size", "38px");
    }
}