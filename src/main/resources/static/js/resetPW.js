/**
 * Created by Alexchiu on 2017/7/20.
 */
$(function () {

    $('#check').click(function () {
        console.log("domain "+domain);
        var json = {
            merchant:account, token:token, pw:$('#password').val()

        };


        $.ajax({
            type: "POST",
            url: "resetPassword",
            dataType: "json",
            contentType: "application/json",
            async:false,
            success: function (msg) {
                window.location.replace(domain+'allpaypass/api/system/resetPasswordSuccess');

            },
            error: function(msg) {
                window.location.replace(domain+'allpaypass/api/system/resetPasswordFail');
            },
            data: JSON.stringify(json)
        });

    });

});