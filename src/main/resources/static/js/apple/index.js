/*
 Copyright (C) 2016 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information

 Abstract:
 The main client-side JS. Handles displaying the Apple Pay button and requesting a payment.
 */

function checkApplePayBrowse() {
    if (window.ApplePaySession) {
        if (ApplePaySession.canMakePayments) {
            var check = document.getElementById("paymentMethod").getElementsByTagName("p")[0].innerHTML;
            if(check == "ec" || check == "ts"|| check == "linepay" || check == "other" || check == "twpay"){
                document.getElementsByClassName("price-title")[0].style.height ="22vh";
                document.getElementById("keyboard").style.height ="50vh";
                document.getElementsByClassName("safari-btn-style")[0].style.height = "100%";
                document.getElementsByClassName("child-flex")[0].style.flex = "2";
            }
            showApplePayButton();
            var clickBtn = document.getElementById("show-prompt");
            if (clickBtn) {
                clickBtn.addEventListener("click",function () {
                    var qrcode = document.getElementById("token").innerHTML;
                    var applePayHost = document.getElementById("applePayHost").innerHTML;
                    window.location.href = "https://" + applePayHost + "/allpaypass/api/payment/apple/" + qrcode;
                })
            }
        }
    } else {
        var clickBtn = document.getElementById("show-prompt");
        if (clickBtn) {
            clickBtn.addEventListener("click",function () {
                var ua = navigator.userAgent || navigator.vendor || window.opera;
                var isChrome = ua.indexOf('Chrome') > -1 || ua.indexOf('CriOS') > -1;
                var isFacebook = ua.indexOf("FBAN") > -1 || ua.indexOf("FBAV") > -1 ;
                document.getElementsByClassName("dialog-opacity-bg")[0].style.display = "block"
                if(isFacebook){
                    document.getElementById("move-to-safari").style.display = "inline-block";
                    document.getElementById("move-to-safari").style.height ="225px";
                    document.getElementById("all-tips").style.display = "none";
                    document.getElementById("common-copy").style.display = "none";
                    document.getElementById("common-no-copy").style.display = "inline-block";
                    document.getElementById("facebook-tips").style.display = "inline-block";
                }else if (isChrome) {
                    $("#chrome-to-safari").show();
                }else {
                    document.getElementById("move-to-safari").style.display = "inline-block";
                    document.getElementById("all-tips").style.display = "inline-block";
                    document.getElementById("common-copy").style.display = "inline-block";
                    document.getElementById("common-no-copy").style.display = "none";
                    document.getElementById("facebook-tips").style.display = "none";
                }
            })
        }
    }
}
function showApplePayButton() {
    HTMLCollection.prototype[Symbol.iterator] = Array.prototype[Symbol.iterator];
    const buttons = document.getElementsByClassName("apple-pay-button");
    for (let button of buttons) {
        button.className += " visible";
        document.getElementsByClassName("price-title")[0].style.height ="22vh";
        document.getElementById("keyboard").style.height ="50vh";
        document.getElementById("apple-pay").style.height = "100%";
        document.getElementsByClassName("child-flex")[0].style.flex = "2";
    }
}

function applePayButtonClicked() {

    var qrcode = document.getElementById("token").innerHTML;
    var applePayHost = document.getElementById("applePayHost").innerHTML;
    var amount = document.getElementById("price").value.split(",").join("");
    var merchant = document.getElementById("merchant").innerHTML;

    const paymentRequest = {
        countryCode: 'TW',
        currencyCode: 'TWD',

        total: {
            label: 'ApplePay付款',
            amount: amount
        },

        supportedNetworks:[ 'mastercard', 'visa'],
        merchantCapabilities: [ 'supports3DS' ]
    };

    const session = new ApplePaySession(1, paymentRequest);

    session.onvalidatemerchant = (event) => {
        getApplePaySession(event.validationURL, merchant).then(function(response) {
            session.completeMerchantValidation(response);
        });
    };

    session.onpaymentauthorized = (event) => {
        const payment = event.payment;
        decryptPaymentToken(JSON.stringify(payment.token.paymentData), qrcode, applePayHost, merchant)
	        .then(function(response) {
	        	window.location.href = response.data.callbackUrl;
	        });
        session.completePayment(ApplePaySession.STATUS_SUCCESS);
    }
    session.begin();
}
