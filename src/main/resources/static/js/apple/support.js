/*
 Copyright (C) 2016 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information

 Abstract:
 A helper function that requests an Apple Pay merchant session using a promise.
 */

function getApplePaySession(url, merchant) {
    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/getApplePaySession');
        xhr.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                resolve(JSON.parse(xhr.response));
            } else {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            }
        };

        xhr.onerror = function () {
            reject({
                status: this.status,
                statusText: xhr.statusText
            });
        };
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(JSON.stringify({url: url,merchant:merchant}));
    });
}

function decryptPaymentToken(paymentTokenData, qrcode, applePayHost, merchant) {
    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        
        xhr.open('POST', `https://${applePayHost}/allpaypass/applepay/qrcode/${qrcode}/merchant/${merchant}`, true);
        xhr.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                resolve(JSON.parse(xhr.response));
            } else {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            }
        };
        xhr.onerror = function () {
            reject({
                status: this.status,
                statusText: xhr.statusText
            });
        };
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(paymentTokenData);
    });
}
