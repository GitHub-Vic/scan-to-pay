/**
 * Created by Alexchiu on 2017/12/25.
 */
document.write('<script src="/allpaypass/js/aes/require.js" defer async="true" ></script>');

document.write('<script src="/allpaypass/js/aes/require.js" data-main="/allpaypass/js/aes/components/core"></script>');
document.write('<script src="/allpaypass/js/aes/require.js" data-main="/allpaypass/js/aes/components/aes"></script>');
document.write('<script src="/allpaypass/js/aes/require.js" data-main="/allpaypass/js/rsa/jsencrypt"></script>');


// document.write('<script src="/allpaypass/js/aes/components/core.js"></script>');
// document.write('<script src="/allpaypass/js/aes/rollups/aes.js"></script>');
// document.write('<script src="/allpaypass/js/rsa/jsencrypt.js"></script>');


    function randomStr() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0;  16 > i; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
}


var seed =randomStr();
console.log(seed);

var key = CryptoJS.enc.Utf8.parse(seed);

var aesEncryDecry = {
    decryptStringAES: function (strEncryptText) {
        var decrypted = CryptoJS.AES.decrypt(strEncryptText, key, {
            keySize: 128 / 8,
            iv:  CryptoJS.enc.Utf8.parse('8651731586517315') ,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        return decrypted.toString(CryptoJS.enc.Utf8);
    },
    encryptStringAES: function (strOrignText) {
        var encrypted = CryptoJS.AES.encrypt(strOrignText, key,{
            keySize: 128 / 8,
            iv: CryptoJS.enc.Utf8.parse('8651731586517315'),
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        return encrypted.toString();
    }
};

function getAESKey() {

    var key = CryptoJS.enc.Utf8.parse(seed);  //需要和伺服器端一致，否則… 無法解密
    return key;
}

function aesEncrypt(input) {

    return aesEncryDecry.encryptStringAES(input);

}

function aesDecrypt(input) {
    return aesEncryDecry.decryptStringAES(input);

}
