function onBuyClicked() {
    if (!window.PaymentRequest) {
        // PaymentRequest API is not available. Forwarding to
        // legacy form based experience.
        location.href = '/checkout';
        return;
    }

    // Supported payment methods
    var supportedInstruments = [
        {
            supportedMethods: ['basic-card'],
            data: {
                supportedNetworks: ['mastercard','visa', 'jcb'],
                supportedTypes: ['credit']
            }
        }, {
            supportedMethods: ['https://android.com/pay'],
            data: {
                //merchant ID obtained from Google that maps to your origin,
                //The Android Pay Merchant ID you obtained by signing up to Android Pay. https://androidpay.developers.google.com/signup
                merchantId: '02510116604241796260',
                //Add this if you are testing with Android Pay. The generated token will be invalid. For production environment, remove this line.
                environment: 'TEST',
                allowedCardNetworks: ['MASTERCARD', 'VISA'],
                paymentMethodTokenizationParameters: {
                    tokenizationType: 'NETWORK_TOKEN',
                    parameters: {
                        //public key to encrypt response from Android Pay
                        //https://developers.google.com/android-pay/integration/payment-token-cryptography#retrieving-the-encrypted-payload
                        publicKey: 'BC9u7amr4kFD8qsdxnEfWV7RPDR9v4gLLkx3jfyaGOvxBoEuLZKE0Tt5O/2jMMxJ9axHpAZD2Jhi4E74nqxr944='
                    }
                }
            }
        }
    ];



    // Checkout details
    var details = {
        displayItems: [{
            label: 'Original donation amount',
            amount: { currency: 'TWD', value: '1.00' }
        }, {
            label: 'Friends and family discount',
            amount: { currency: 'TWD', value: '0.00' }
        }],
        total: {
            label: 'Total due',
            amount: { currency: 'TWD', value : '1.00' }
        }
    };

    // 1. Create a `PaymentRequest` instance
    var request = new PaymentRequest(supportedInstruments, details);

    console.log('PaymentRequest', request);
    // 2. Show the native UI with `.show()`
    request.show()
    // 3. Process the payment
        .then(result => {

            console.log('request ??? ', JSON.stringify(result.toJSON()));
        // POST the payment information to the server
            return fetch('/allpaypass/api/android/pay', {
                method: 'POST',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(result.toJSON())
            }).then(response => {
                // 4. Display payment results
                if (response.status === 200) {
                // Payment successful
                    console.log('request', JSON.stringify(result.toJSON()));
                    return result.complete('success');
                } else {
                // Payment failure
                    return result.complete('fail');
                }
            }).catch(() => {
                return result.complete('fail');
            });
        });
}
