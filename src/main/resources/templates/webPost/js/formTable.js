const tableTitle = ['商店名稱','交易時間','訂單編號','交易單號','支付平台','交易金額','交易狀態','卡號',
]

Vue.component("page",{
    props:["currentPage","totalPage"],
    data (){
        return {
            displayCount: 5,
            input: ""
        }
    
    },
    computed: {
        firstCount() {
            let first =
                this.currentPage - Math.round(this.displayCount / 2);
            if (first < 0) {
                return 0;
            }
            if (first + this.displayCount > this.totalPage) {
                return this.totalPage - this.displayCount;
            }
            return first;
        }
    },
    methods: {
        clickEvent(val) {
            this.$emit('do-search', val);
        },
        inputEvent() {
            if (this.input <= this.totalPage && this.input > 0) {
                this.$emit('do-search', this.input);
                return;
            } 
            this.input = "";
        }
    },
    template:`
    <div class="page__box">
        <span>前往
            <input
            type="number"
            class="page__box__input"
            v-model.number="input"
            v-on:keydown.enter="inputEvent"
            /> 頁
        </span>
        <a
            v-for="n in displayCount"
            :value="n+firstCount"
            :class="n+firstCount===currentPage ? 'page__count current':'page__count'"
            v-on:click="clickEvent(n+firstCount)"
            >{{n+firstCount}}</a>
        <span>{{currentPage}} / {{totalPage}}</span>
    </div>
    `
})

const vm = new Vue({
    el: "#page__information",
    data: {
        tableTitle: tableTitle,
        autoUpdate: false,
        datas: [],
        display__information: false,
        updateInterval: 30, // 間隔時間秒數
        countDown: 30, //剩餘更新秒
        pageSize: 10,
        totalPage: 15,
        changeCurrentPage: 1,
        currentPage: 1,
        lastUpdateTime: moment().format('YYYY / MM / DD / HH:mm:ss'),
        startDate: moment({hour: 0, minute: 0, seconds: 0}).subtract(0, 'months').format('YYYYMMDDHHmmss'),
        endDate: moment({hour: 23, minute: 59, seconds: 59}).format('YYYYMMDDHHmmss'),
    },
    methods:{
        tab__information(state){ // 外部控制頁籤
            this.display__information = state;
        },
        startTime(){
            this.countDown += -1; 
            if (this.countDown === 0) this.api();
            this.auto = setTimeout(() => {
                this.startTime();
            },  1000);
            return false;
        },
        stopTime(){
            clearTimeout(this.auto);
            this.auto = false;
        },
        clickUpdataBtn(){
            this.countDown = this.updateInterval;
            this.checkAutoUpdate();
        }
        ,
        checkAutoUpdate(status = true){ //判斷是否更新
            this.stopTime();
            if (!status) {
                this.countDown = this.updateInterval;
                logTest('立即停止更新');
                return;
            }
            this.api();
            if (this.autoUpdate) {
                this.startTime();
                logTest('開始連續更新');
            } else {
                logTest('只更新一次');
            }
        },
        doSearch(val){
            this.changeCurrentPage = val;
            if (val !== 1) this.autoUpdate = false;
            this.checkAutoUpdate();
        },
        api(){
            logTest('api');
            this.countDown = this.updateInterval;

            let data = {
                "Header": {
                    "Method": "00000",
                    "ServiceType": "PageQuery",
                    "MchId": mchId,
                    "CreateTime": generateTimeReqestNumber()
                },
                "Data": "{\"Index\":\""+`${this.changeCurrentPage}`+"\",\"PageSize\":\""+`${this.pageSize}`+"\",\"StartDate\":\""+`${this.startDate}`+"\",\"EndDate\":\""+`${this.endDate}`+"\"}"
            };
            //console.log(JSON.stringify(data));
            data = enCode(JSON.stringify(data), edgeType);
            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                data: data,
                url: edgeUrl,
                headers: {
                    "token": edgeToken,
                    "requestId": "12345",
                    "encryptType": edgeType,
                    "Access-Control-Allow-Origin": "*"
                },
                success: function (result) {
                    let {Header, Data} = JSON.parse(aesDecrypt(result.data))
                    // console.log(Header, Data)
                    if(!Header || Header.StatusCode != "0000") {
                        console.log(Header.StatusDesc);
                        return
                    };
                    if (!Data) {
                        return
                    };
                    this.datas = JSON.parse(JSON.parse(aesDecrypt(result.data)).Data.list);
                    console.log(this.datas);
                    this.lastUpdateTime = moment(lastUpdateTime,'YYYYMMDDHHmmss').format('YYYY / MM / DD  HH:mm:ss');

                    this.changeCurrentPage = 1;
                    
                },
                error: function (jqXHR, exception) {
                    $('.toggle-logout').css('display', 'none');
                    if (jqXHR.status !== 200) {
                        $('.alert').text('連線錯誤請洽客服人員');
                    }
                    ;
                    $('.loading').css('display', 'none');
                    $('.dilaog-container').css('display', 'block');
                    if (exception === 'timeout') {
                        $('.alert').text('逾時');
                        $('.handleQuery').css('display', 'inline-block')
                        nodata()
                        $('#tradeStatus').text('逾時');
                    } else {
                        $('.alert').text(exception);
                    };
                },
                timeout: 30000 
            });
            // $.ajax({
            //     type: "post",
            //     dataType: 'json',
            //     contentType: 'application/json',
            //     data: JSON.stringify(data),
            //     // url: edgeUrl,
            //     url: edgeUrl,
            //     headers: {
            //         // "token": edgeToken,
            //         // "encryptType": edgeType,
            //         "requestId": "12345",
            //         "Access-Control-Allow-Origin": "*"
            //     },
            //     success:(r) => {
            //         let {content, totalPage, currentPage, lastUpdateTime} = r.data;
            //         this.datas = [...content];
            //         this.totalPage = totalPage;
            //         this.currentPage = currentPage;
            //         this.lastUpdateTime = moment(lastUpdateTime,'YYYYMMDDHHmmss').format('YYYY / MM / DD  HH:mm:ss');

            //         this.changeCurrentPage = 1;
            //     },
            //     error: function (jqXHR, exception) {
            //         console.log('no',jqXHR);
            //         console.log('no two', exception);

            //         console.log('false');
            //         if (exception === 'timeout') { // 超過30秒交易 進來這裡
            //             console.log('交易逾時，請點選頁面下方重新查詢按鈕');
            //             return;
            //         }
            //         if (jqXHR.status !== 200) { //非兩百會進來這邊
            //             console.log('連線錯誤請洽客服人員', exception);
            //             return;
            //         }
            //         console.log('訊息', exception);
            //     },
            //     timeout: 30000
            // })
        }
    },
    watch:{
        autoUpdate(){
            this.checkAutoUpdate(this.autoUpdate);
        },
        display__information: {
            immediate: true,
            handler (){
                this.checkAutoUpdate(this.display__information);
            }
        }
    }
});
function  logTest(val) {
    // console.log(val);
}
