// data
// 傳給後端的模式
let data = {
    startDate: '20200802000000', // 開始日
    endDate: '20200802000000', // 結束日
    currentPage: 1, // 預設是 1（當前的值）
    pageSize: 10, // 一頁的請求筆數
}
// 接回前端的值
let response = {
    msg: '',
    msgCode: '0000',
    data:{
        currentPage: '1', // 現在頁數
        totalPage: '15',
        lastUpdateTime: '20200802150000', // 最後更新時間
        content: [{ // table 裡面的值 判定
            "name": "123",
            "time": "2020-01-01",
            "orders": "123456",
            "dealOrders": "456789",
            "platform": "line pay",
            "Amount": "600",
            "status": "yes",
            "cardNumber": "1000"
        }]
    } 
}

$.ajax({
    type: "POST",
    dataType: 'json',
    contentType: 'application/json',
    data: data,
    url: edgeUrl,
    headers: {
        "token": edgeToken,
        "requestId": "12345",
        "encryptType": edgeType,
        "Access-Control-Allow-Origin": "*"
    },
    success: function (result) {
        
        let {msgCode} = result;
        if (msgCode === '0000') {
            //console.log(result);
            let {data} = result;
            data = aesDecrypt(data);
            data = JSON.parse(data);
            //console.log(data);
            // 成功呼叫
        }
    },
    error: function (jqXHR, exception) {
        if (exception === 'timeout') { // 超過30秒交易 進來這裡
            $('.alert').text('交易逾時，請點選頁面下方重新查詢按鈕');
            // nodata();
            // $('#tradeStatus').text('交易逾時'); // 這邊重新給交易結果原因
        } else if (jqXHR.status !== 200) { //非兩百會進來這邊
            $('.alert').text('連線錯誤請洽客服人員');
            //  $('.alert').text(exception); //這邊可以寫傳回來的exception
        } else {
            $('.alert').text(exception);
        }
    },
    timeout: 30000 // 30秒
});
