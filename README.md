## 指令說明

### 本地開發執行指令
```bash
gradle cleanCompileJava bootRun
```

### 本地端開發除錯 (default port 5005)
```bash
gradle bootRun --debug-jvm
```

### 產生 QueryDSL 程式碼

```bash
gradle clean generateQueryDSL
```

### 根據 gradle/env/dev.gradle 設定打包 war
```bash
gradle cleanCompileJava -Penv=prod war
```

### 注意事項
1. 新增功能請建立 branch 進行開發
2. 版本發布前會將發布版本 tag 並建立 branch 進行發布

### 待辦事項
相關 project 程式碼整合
1. 將 scan2pay-data 程式碼合併於此專案
2. 將 scan2pay-ui 程式碼合併於此專案

2018/11/13 for ctbc weixin private key
ALTER TABLE PaymentAccount MODIFY hashIV VARCHAR(3000);
