FROM tomcat:8.5.15-jre8
ADD build/libs/allpaypass.war /usr/local/tomcat/webapps/allpaypass.war
CMD ["catalina.sh", "run"]